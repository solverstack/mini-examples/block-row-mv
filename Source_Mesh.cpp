#include "Header.h"
 
using namespace std;
/* Definition de la classe Sommet */
Sommet::Sommet() {
	X = NULL;
	ref = 0;
}
void Sommet::print_info() {
	cout << "'nref = " << ref << " : X = " << X[0] << ", Y = " << X[1] << ", Z = " << X[2]  << ", addresse X = " << X << endl;
}
void Sommet::set_X(double* A) {
	X = A;
}
double* Sommet::get_X() {
	return X;
}
void Sommet::set_ref(MKL_INT N) {
	ref = N;
}
MKL_INT Sommet::get_ref() {
	return ref;
}
void Sommet::Allocate_X() {
  X = new double[3];
}
void Sommet::set_val(double a, double b, double c) {
  X[0] = a; X[1] = b; X[2] = c;
}

void affectation(Face &G, Face &F) {
  MKL_INT i, j;
  double *X;
  for(i = 0; i < 3; i++) {
    
    if (G.noeud[i] == NULL){
      G.noeud[i] = F.noeud[i];
      G.noeud[i]->set_ref(F.noeud[i]->get_ref());
    }
    
    else {
      G.noeud[i]->set_val((F.noeud[i]->get_X())[0], (F.noeud[i]->get_X())[1], (F.noeud[i]->get_X())[2]);
      G.noeud[i]->set_ref(F.noeud[i]->get_ref());
    }
    
  }
  
  for(i = 0; i < 3; i++) {
    G.XG[i] = F.XG[i];
  }
  
  G.voisin1 = F.voisin1;
  G.voisin2 = F.voisin2;
  G.Aire = F.Aire;
  
  G.set_ref(F.get_ref());
  
}


void affectation(Onde_plane &P1, Onde_plane &P2) {
  MKL_INT i;
  
  P1.P = P2.P;

  for (i = 0; i < 3; i++) {
    P1.V[i] = P2.V[i];
    P1.K[i] = P2.K[i];
    P1.d[i] = P2.d[i];
    P1.Ad[i] = P2.Ad[i];
    P1.AK[i] = P2.AK[i];
  }

  P1.dAd = P2.dAd;
  P1.ksurOmega = P2.ksurOmega;
  P1.set_normk(P2.get_normk());
}

/*Definition de la classe Maillage */
Maillage::Maillage() {
	MKL_INT print = 0;
	MKL_INT taille_loc;
	sld = 1.2124;
	slg = 0.5196;

	X0[0] = 0.5;
	X0[1] = 0.5;
	X0[2] = -2;

	omega = 2 * M_PI;

	ww = 1.0 / omega;

	a1 = 1.0; a2 = 2.0; xi1 = 1.0; xi2 = 2.0;
	x_I = 0.5;

	Y_omega = 1.0;

	/********************************************* quadrature de Gauss-Legendre *******************************************/

	//1D
	n_gL = 12;
	
	X1d = new double[n_gL];
	W1d = new double[n_gL];
	quad_Gauss_1D();

	//2D

	Qd2D.ordre = n_gL;
	
	Qd2D.Allocate_quad_2D();
	
	Qd2D.quad_gauss_triangle(X1d, W1d);

	//3D
	Qd.ordre = n_gL;
	
	Qd.Allocate_quad();
	
	Qd.quad_gauss_tetra(X1d, W1d);

	/**********************************************************************************************************************/
	
	n_i = 17;

	cout<<"lecture du fichier des points "<<endl;
	// lecture du fichier maillage sommets.txt
	points = Read_Points(&N_sommets);
	cout<<"lecture réussi pour les points "<<endl;

	
	// lecture du fichier maillage tetra.txt
	cout<<"lecture du tableau des tetra "<<endl;
	tetra = Read_Tetra(&N_elements);
	cout<<"lecture réussi pour les tetra "<<endl;
	  
	// lecture du fichier maillage voisin.txt
	cout<<" lecture du tableau des voisins "<<endl;
	voisin = Read_Neighbours_Tetra();
	cout<<"lecture réussi pour les voisins des tetra "<<endl;

	
	//lecture du tableau des directions sur un sphère
	cout<<" lecture du tanleau des directions dans la sphere unité "<<endl;
	DirSph = Read_PointsSphere(&npS);
	cout<<"lecture réussi pour les directions du sphere "<<endl;
	//variable permettant de définir le nombre d'ondes planes par élément
	ordre_onde = 1;
	//ndof = 6;
	
	//nombre d'ondes planes par élément
	ndof =  (2 * ordre_onde + 1) * (2 * ordre_onde + 1) * (2 * ordre_onde + 1) -  (2 * ordre_onde - 1) * (2 * ordre_onde - 1) * (2 * ordre_onde - 1);
      
	//initialisation du tableau Dir
	Dir = new double[ndof * 3];

	//remplissage du tableau des directions sur un cube
	directionInCube(Dir, ordre_onde);
	
	// Dir[3*0] = 1.0;
	// Dir[3*0+1] = 0.0;
	// Dir[3*0+2] = 0.0;
	
	// Dir[3*1] = -1.0;
	// Dir[3*1+1] = 0.0;
	// Dir[3*1+2] = 0.0;
	
	// Dir[3*2] = 0.0;
	// Dir[3*2+1] = 1.0;
	// Dir[3*2+2] = 0.0;

	// Dir[3*3] = 0.0;
	// Dir[3*3+1] = -1.0 ;
	// Dir[3*3+2] = 0.0;

	// Dir[3*4] = 0.0;
	// Dir[3*4+1] = 0.0;
	// Dir[3*4+2] = 1.0;

	// Dir[3*5] = 0.0;
	// Dir[3*5+1] = 0.0;
	// Dir[3*5+2] = -1.0;


	eps_0 = 1E-7;
	SIZE = new MKL_INT[N_elements];
	SIZE_LOC = new MKL_INT[N_elements];	
	cout << "creation et affichage du tableau contenant la liste des sommets" << endl;
	Create_Xsommets(points);
	cout << "creation du tableau avec la liste des voisins" << endl;
	Create_ListeVoisin(voisin);
	cout << "Creation du tableau loc to glob" << endl;
	Create_Elem2sommet(tetra); 
	cout << "creation de l'objet qui permet de lire facilement la liste des sommets" << endl;
	build_sommet();
	cout << "creation de l'objet qui permet de lire facilement la liste des elements" << endl;
	
	tools_to_define_sorted_mesh();

	build_NewListeVoisin();

        build_newloc2glob();
	
	build_element();
	if (print == 1) {
		print_info();
		print_loc2glob();
		print_ListeVoisin();
		test_build_sommet();
		test_build_element();
	}
	build_face();

       
	
}
void Maillage::print_info() {
	cout << "N_sommets = " << N_sommets << ", N_elements = " << N_elements << endl;
}
void Maillage::set_N_sommets(MKL_INT N){
	N_sommets = N;
}
MKL_INT Maillage::get_N_sommets() {
	return N_sommets;
}
void Maillage::set_N_elements(MKL_INT N) {
	N_elements = N;
}
MKL_INT Maillage::get_N_elements() {
	return N_elements;
}
void Maillage::Create_Xsommets(double *points)
{
	Xsommet = points;
}
void Maillage::print_Xsommets(){
	for (MKL_INT i = 0; i < N_sommets; i++) {
		cout << "Numero du sommet = " << i << " : X1 = " << Xsommet[i] << " : X2 = " << Xsommet[i + 1] << " : X3 = " << Xsommet[i + 2] << endl;
	}
	cout << endl;
}
void Maillage::Create_Elem2sommet(MKL_INT *tetra)
{

  oldloc2glob = tetra;
}
void Maillage::print_loc2glob() {
	for (MKL_INT i = 0; i < N_elements; i++) {
		cout << "Numero de l element = " << i << " : i0 = " << oldloc2glob[4 * i] << " : i1 = " << oldloc2glob[4 * i + 1] << " : i2 = " << oldloc2glob[4 * i + 2] << " : i3 = " << oldloc2glob[4 * i + 3] << endl;
	}
	cout << endl;
}
void Maillage::Create_ListeVoisin(MKL_INT *voisin)
{
	OldListeVoisin = voisin;
}
void Maillage::print_ListeVoisin() {
	for (MKL_INT i = 0; i < N_elements; i++) {
		cout << "Numero de l element = " << i << " : Voisin 0 = " << OldListeVoisin[4 * i] << " : Voisin 1 = " << OldListeVoisin[4 * i +1] << " : Voisin 2 = " << OldListeVoisin[4 * i+2] << " : Voisin 3 = " << OldListeVoisin[4 * i+3] << endl;
	}
	cout << endl;
}

void Maillage::build_sommet()
{
	sommets = (Sommet*)malloc(N_sommets*sizeof(Sommet));

	for (MKL_INT i = 0; i < N_sommets; i++) {
		sommets[i].set_X(Xsommet+3*i);
		sommets[i].set_ref(i);
	}
}
void Maillage::test_build_sommet()
{
	for (MKL_INT i = 0; i < N_sommets; i++) {
		sommets[i].print_info();
	}
}
void Maillage::build_face()
{
       double *XX = nullptr, *YY = nullptr, *ZZ = nullptr;//pour faciliter le calcul
	                                                   //d'aire des triangle
	XX = new double[3];  YY = new double[3];  ZZ = new double[3];
	cout << "Construction de la liste des faces" << endl;
	N_Face_double = N_elements * 4;
	face_double = (Face*)malloc(N_Face_double * sizeof(Face));
	MKL_INT l = 0;
	for (MKL_INT i = 0; i < N_elements; i++) {
		for (MKL_INT j = 0; j < 4; j++) {
			face_double[l] = elements[i].face[j];
			l++;
		}
	}
	cout<<" nbre Elem: "<<N_elements<<endl;
	Tri = (MKL_INT*)malloc(N_Face_double * sizeof(MKL_INT));
	for (l = 0; l < N_Face_double; l++) {
		Tri[l] = l;
	}
	// Determination de l'ordre des faces
	Tri_rapid(0, N_Face_double-1);

	N_Face = 1	; // Compteur du nombre de faces distinctes
	MKL_INT test;
	for (MKL_INT i = 0; i < N_Face_double-1; i++) {
		 test = ordre(face_double[Tri[i]].XG, face_double[Tri[i + 1]].XG, 3);
		 if (test == 2) N_Face++;
	}
	face = (Face*)malloc(N_Face*sizeof(Face));
	face[0] = face_double[Tri[0]];
       
	face_double[Tri[0]].set_ref(0);
	MKL_INT compteur = 0;
	MKL_INT n_elt;
	MKL_INT n_face;
	for (MKL_INT i = 0; i < N_Face_double - 1; i++) {
		test = ordre(face_double[Tri[i]].XG, face_double[Tri[i + 1]].XG, 3);
		if (test == 2) {
			compteur++;
		}
		if (test == 1) {
			cout << "BUG dans Source_Mesh.cpp : problème dans le tri des faces" << endl;
		}
		face_double[Tri[i + 1]].set_ref(compteur);
		if (test == 2) {
			face[compteur] = face_double[Tri[i + 1]];
		}

//		n_elt = Tri[i]/4;
//		n_face = Tri[i]%4;
//		elements[n_elt].face[n_face].set_ref(compteur);
	}
	l = 0;
   
	for (MKL_INT i = 0; i < N_elements; i++) {
	  for (MKL_INT j = 0; j < 4; j++) {
	    face_double[l].compute_Aire_triangle(XX, YY, ZZ);
	    elements[i].face[j]= face_double[l] ;
	    l++;
	  } 
	}

	for (MKL_INT i = 0; i < N_elements; i++) {
	  MKL_INT i1, i2;
	  for(MKL_INT j = 0; j < 4; j++){
	    if (elements[i].voisin[j] >=0){
	      
	      i1 = i;
	      i2 = elements[i].voisin[j];
	      
	      // cout<<"i1 = "<<i1<<" i2 = "<<i2<<endl;
	      
	      double TmL = (elements[i1].Y[j] - elements[i2].Y[j]);
	      double TpL = (elements[i1].Y[j] + elements[i2].Y[j]);
	    //double pp = 
	      
	      elements[i].TL_VV[j] = TmL / (4 * elements[i1].Y[j] * TpL);
	      
	      elements[i].LT_VV[j] = 1.0 / (2 * TpL);
	      
	      elements[i].TL_VP[j] = - TmL / (4 * TpL);
	      
	      elements[i].LT_VP[j] = - elements[i1].Y[j] / (2 * TpL);
	      
	      elements[i].TL_PV[j] = TmL / (4 * TpL);
	      
	      elements[i].LT_PV[j] = elements[i2].Y[j] / (2 * TpL);
	      
	      elements[i].TL_PP[j] = - (elements[i1].Y[j] * TmL) / (4 * TpL);
	      
	      elements[i].LT_PP[j] = - (elements[i1].Y[j] * elements[i2].Y[j]) / (2 * TpL);

	      //Coeffs de la formulation issue du systeme d'ordre 1
	      elements[i].T_PP[j] = (elements[i1].Y[j] * elements[i2].Y[j]) / TpL;
	      elements[i].T_PV[j] = - elements[i2].Y[j] / TpL;
	      elements[i].T_VP[j] = - elements[i1].Y[j] / TpL;
	      elements[i].T_VV[j] = 1.0 / TpL;

	      elements[i].L_PP[j] = - (elements[i1].Y[j] * elements[i2].Y[j]) / TpL;
	      elements[i].L_PV[j] =  elements[i2].Y[j] / TpL;
	      elements[i].L_VP[j] = - elements[i1].Y[j] / TpL;
	      elements[i].L_VV[j] = 1.0 / TpL;

	      
	    }
	    else {
	      i1 = i;
	      i2 = i;
	      
	      double TpL = (elements[i1].Y[j] + Y_omega);
	      double TmL = (elements[i1].Y[j] - Y_omega);

	      //coeff de réflexion sur une interface extérieure
	      elements[i].Q_T = (elements[i1].Y[j] - Y_omega) / TpL;
	      
	      elements[i].betaPP[j] = - (elements[i1].Y[j] * TmL) / (4 * TpL);
	      elements[i].betaPV[j] = TmL / (4 * TpL);
	      elements[i].betaVP[j] = - TmL / (4 * TpL);
	      elements[i].betaVV[j] = TmL / (4 * elements[i1].Y[j] * TpL);

	      elements[i].T_PP[j] = (elements[i1].Y[j] * Y_omega) / TpL;
	      elements[i].T_PV[j] = - Y_omega / TpL;
	      elements[i].T_VP[j] = - elements[i1].Y[j] / TpL;
	      elements[i].T_VV[j] = 1.0 / TpL;

	      //alpha_PincP[j] = (Y[j] * Y[j]) / 2
	      elements[i].alpha_PincP[j] = (elements[i1].Y[j] * Y_omega) / TpL;
	      
	      //alpha_PincV[j] = - Y[j] / 2
	      elements[i].alpha_PincV[j] = - Y_omega / TpL;
	      
	      //alpha_VincP[j] = Y[j] / 2
	      elements[i].alpha_VincP[j] = - elements[i1].Y[j] / TpL;
	      
	      //alpha_VincV[j] = Y[j] / 2
	      elements[i].alpha_VincV[j] =  1.0 / TpL;
	    }
	  }

	}
	  /*----------------------- remplissage de YT, YL-------------------------------------*/
	
	//	test_build_element();
	//	test_build_face();
	cout<<" construction de la liste face reussie "<<endl;
	delete [] XX; XX = 0;
	delete [] YY; YY = 0;
	delete [] ZZ; ZZ = 0;
}
void Maillage::test_build_face() {
  for (MKL_INT i = 0; i < N_Face ; i++) {
    face[i].print_info();
  }
}
void Maillage::Tri_rapid(MKL_INT i1, MKL_INT i2) {
	MKL_INT tampon;
	MKL_INT pivot = Tri[i1];
//	Tri[(i1 + i2) / 2] = Tri[i1];
//	Tri[i1]=pivot;
	MKL_INT idebut = i1+1;
	MKL_INT ifin = i2;
//	cout << i1 << i2 << endl;
	if (i1<i2){
		for (MKL_INT i = i1; i < i2; i++)
		{ 
				//	cout << face_double[i].XG[0] << " " << face_double[i].XG[1] << " " << face_double[i].XG[2] << endl;
				//	cout << face_double[pivot].XG[0] << " " << face_double[pivot].XG[1] << " " << face_double[pivot].XG[2] << endl;
				if (ordre(face_double[Tri[idebut]].XG, face_double[pivot].XG, 3) == 2)
				{
					Tri[idebut-1] = Tri[idebut];
					idebut++;
				}
				else {
					tampon = Tri[ifin];
					Tri[ifin] = Tri[idebut];
					Tri[idebut] = tampon;
					ifin--;
				}
		}
		Tri[idebut-1] = pivot;
		Tri_rapid(i1, idebut -2 );
		Tri_rapid(idebut, i2);
	}
/*
	cout << i1 << " " << idebut-2 << " " << idebut-1 << " " << ifin << " " << i2 << endl;
	for (MKL_INT i = i1; i < i2 + 1; i++) {
		if (ordre(face_double[Tri[i]].XG, face_double[pivot].XG, 3) == 2)
		{
			cout << i << " " << idebut - 1 << " plus grand"<< endl;
		}
		if (ordre(face_double[Tri[i]].XG, face_double[pivot].XG, 3) == 1)
		{
			cout << i << " " << idebut - 1 << "plus petit" << endl;
		}
		if (ordre(face_double[Tri[i]].XG, face_double[pivot].XG, 3) == 0)
		{
			cout << i << " " << idebut - 1 << "egal" << endl;
		}
	}
	MKL_INT ll = Tri[628];
	cout << face_double[ll].XG[0] << " " << face_double[ll].XG[1] << " " << face_double[ll].XG[2] << endl;
	ll = Tri[647];
	cout << face_double[ll].XG[0] << " " << face_double[ll].XG[1] << " " << face_double[ll].XG[2] << endl;
	ll = Tri[642];
	cout << face_double[ll].XG[0] << " " << face_double[ll].XG[1] << " " << face_double[ll].XG[2] << endl;
	*/
}


void Maillage::tools_to_define_sorted_mesh() {
  MKL_INT i, j;
  Z_XG = new double[N_elements];

  for(i = 0; i < N_elements; i++) {
    Z_XG[i] = 0.0;
  }

  //cout<<" 3e composante des barycentres des elements "<<endl;
  for(i = 0; i < N_elements; i++) {
    for(j = 0; j < 4; j++) {
      Z_XG[i] += (sommets[oldloc2glob[4 * i + j]].get_X())[2];
    }
    Z_XG[i] = Z_XG[i] / 4.0;
  }

  new2old = new MKL_INT[N_elements];
  old2new = new MKL_INT[N_elements];

  tri_elements(N_elements, new2old, Z_XG);

  for(i = 0; i < N_elements; i++){
    old2new[new2old[i]] = i;
  }
  /*
  FILE* fic;
  
  if( !(fic = fopen("data/tools/coord.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  for(i = 0; i < N_elements; i++) {
    j = old2new[i];
    fprintf(fic,"%d\t %d\t %10.15e\n",i, j, Z_XG[j]);
  }
  fclose(fic);*/
}


void Maillage::build_NewListeVoisin(){
  MKL_INT e_old, e_new, j, e_old_v, e_new_v;

  NewListeVoisin = new MKL_INT[N_elements * 4];
  
  for(e_old = 0; e_old < N_elements; e_old++) {
    
    e_new = old2new[e_old];
    
    for(j = 0; j < 4; j++) {
      e_old_v = OldListeVoisin[4 * e_old + j];
      
      if (e_old_v == -1){
	e_new_v = -1;
      }
      else{
	e_new_v = old2new[e_old_v];
      }
      
      NewListeVoisin[4 * e_new + j] = e_new_v;
    }
 
  }
  
}


void Maillage::build_newloc2glob(){
  MKL_INT e_old, e_new, j, x;

  newloc2glob = new MKL_INT[4 * N_elements];
  
  for(e_old = 0; e_old < N_elements; e_old++) {
    
    e_new = old2new[e_old];

    for(j = 0; j < 4 ; j++) {
      
      x = oldloc2glob[e_old * 4 + j];
      
      newloc2glob[e_new * 4 + j] = x;
    }
  }

  
  for(e_old = 0; e_old < N_elements; e_old++) {
    cout<< oldloc2glob[4 * e_old]<<" "<<oldloc2glob[4 * e_old + 1]<<" "<<oldloc2glob[4 * e_old + 2]<<" "<<oldloc2glob[4 * e_old + 3]<<endl;
  }
  cout<<endl;

  for(e_new = 0; e_new < N_elements; e_new++) {
    cout<< newloc2glob[4 * e_new]<<" "<<newloc2glob[4 * e_new + 1]<<" "<<newloc2glob[4 * e_new + 2]<<" "<<newloc2glob[4 * e_new + 3]<<endl;
  }
  cout<<endl;
  
}



void Maillage::build_element()
{
	MKL_INT ll;
	elements = (Element*)malloc(N_elements * sizeof(Element));
	for (MKL_INT e_new = 0; e_new < N_elements; e_new++) {
		elements[e_new].set_ref(e_new);
	}
	for (MKL_INT e_new = 0; e_new < N_elements; e_new++) {
		for (MKL_INT j = 0; j < 4; j++) {
		       elements[e_new].noeud[j] = sommets + newloc2glob[4 * e_new + j];
			elements[e_new].voisin[j] = NewListeVoisin[4 * e_new + j];
			elements[e_new].face[j].set_ref(-1);
		
			if (e_new > NewListeVoisin[4 * e_new + j])
			{
				elements[e_new].face[j].voisin1 = e_new;
				elements[e_new].face[j].voisin2 = NewListeVoisin[4 * e_new + j];
			}
			if (e_new < NewListeVoisin[4 * e_new + j])
			{
				elements[e_new].face[j].voisin1 = NewListeVoisin[4 * e_new + j];
				elements[e_new].face[j].voisin2 = e_new; 
			}
			if (e_new == NewListeVoisin[4 * e_new + j])
			{
			  //print_ListeVoisin();
				cout << "voisin 1 =" << endl;
				cout << "BUG Dans Source_Mesh.cpp affectation voisin1 et voisin2"<<endl;
				exit(1);
			}
			ll = 0;
			for (MKL_INT l = 0; l < 4; l++) {
				if (j != l) {
				  elements[e_new].face[j].noeud[ll] = sommets + newloc2glob[4 * e_new + l];
					ll++;
				}
			}
			elements[e_new].face[j].compute_XG();
		}

		//calcule du centre de gravité du tetra e
		elements[e_new].compute_XG();

		//calcule du volume du tetra e
		elements[e_new].compute_volume_tetra();

		//définition du seuil de filtrage des valeurs propres sur chaque élément
		elements[e_new].compute_eps_max_For_Eigenvalue_Filtering(eps_0, 1);
		
		choisir_A_et_xi(elements[e_new], sld, slg, 2);
		//choisir_A_et_xi(elements[i], x_I, a1, a2, xi1, xi2);
		double *Bc = new double[9];
		double *w = new double [3];
		Compute_EigenValue(elements[e_new].Ac.value,Bc, w);
		elements[e_new].Ac.eigen[0] = w[0];
		elements[e_new].Ac.eigen[1] = w[1];
		elements[e_new].Ac.eigen[2] = w[2];
		prodMatMatDiag_positive(Bc,w,elements[e_new].Ac.ADP);
		prodMatMatDiag_negative(Bc,w,elements[e_new].Ac.ADN);


		/*-------------------- definition des ondes planes -------------------------*/
		//directions des ondes planes dans un cube
		elements[e_new].OP = new Onde_plane[ndof];
		MKL_INT s;
		for (s = 0; s < ndof; s++){
		  elements[e_new].OP[s].define_d(Dir[3*s], Dir[3*s+1], Dir[3*s+2]);
		  elements[e_new].OP[s].compute_Ad_and_dAd(elements[e_new].Ac, elements[e_new].xi);
		  
		  //calculer le vecteur K de Hélène, Sebastien, Julien et Ben
		  elements[e_new].OP[s].compute_K(elements[e_new].xi, omega);
		}
		
		get_exteriorNormalToElem(elements[e_new]);
		
		//calculer A*n, n*A*n, alphaPP, alphaPV, alphaVP et alphaVV
		  for(s = 0; s < 4; s++){
		    elements[e_new].compute_An_and_nAn(s);
		  }

		  elements[e_new].Nred = 0;
		  elements[e_new].Nres = 0;
		
		delete [] w; w =  nullptr;
		delete [] Bc; Bc =  nullptr;

		
	}
	
	//Define_index_Of_reflected_wave();

	//compute_R_and_T();
	
}


void Maillage::test_build_element()
{
  //information sur les éléments du maillage
	cout << "-----------------------------" << endl;
	for (MKL_INT i = 0; i < N_elements; i++) {
		elements[i].print_info();
		for(MKL_INT s = 0; s < ndof; s++) {
		  elements[i].OP[s].print_info();
		}
	}
	cout << "-----------------------------" << endl;
}


void Maillage::Define_index_Of_reflected_wave(){
  MKL_INT j;
  
  for(j = 0; j < ndof; j++){
    if (( Dir[3*j] !=  Dir[3*n_i]) && ( Dir[3*j+1] ==  Dir[3*n_i+1]) && ( Dir[3*j+2] ==  Dir[3*n_i+2])){
      n_r = j;
    }
  }

  
}



void Maillage::print_new_mesh(double distance){
  MKL_INT i, j, l, f;
  double r , t = -1; 
  FILE* fic;
  
  if( !(fic = fopen("data/TEST_new_mesh.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  for(i = 0; i < N_elements; i++) {
    
    fprintf(fic,"(%lg, %lg, %lg)\t",elements[i].XG[0],elements[i].XG[1],elements[i].XG[2]);
    
    for(j = 0; j < 4; j++) {
      if (elements[i].voisin[j] >= 0){
	
	f = elements[i].voisin[j];
	
	r = compute_XG(elements[i], elements[f]);
       
	if (r > distance) {
	  cout<<" Bug au tetra "<<i<<" voisin "<<j<<endl;
	  cout<< newloc2glob[4 * f]<<" "<<newloc2glob[4 * f + 1]<<" "<<newloc2glob[4 * f + 2]<<" "<<newloc2glob[4 * f + 3]<<" r = "<<r<<endl;
	  exit(1);
	}
	
	fprintf(fic, "(%lg, %lg, %lg)\t %lg\t",elements[f].XG[0],elements[f].XG[1],elements[f].XG[2], r);
      }
      else {
	fprintf(fic,"%lg\t %lg",t, t);
      }
    }
    fprintf(fic,"\n");
  }
  fclose(fic);
}



//destructeur de la classe Maillage
Maillage::~Maillage(){
  MKL_INT i;
  delete [] points; points = 0;
  delete [] voisin; voisin = 0;
  delete [] tetra; tetra = 0;
  free(Tri); Tri = 0;
  delete [] NewListeVoisin; NewListeVoisin = 0;
  delete [] newloc2glob; newloc2glob = 0;

  for (i = 0; i < N_elements; i++) {
    delete [] elements[i].OP; elements[i].OP = 0;
  }
  free(face_double); face_double = 0;
  free(face); face = 0;
  free(sommets); sommets = 0;
  delete [] DirSph; DirSph = 0;
  delete [] Dir; Dir = 0;
  i = 0;
  cout<<" elements["<<i<< "].xi : "<<elements[i].xi<<endl;
  free(elements); elements = 0;
  delete [] X1d; X1d = 0;
  delete [] W1d; W1d = 0;
  delete [] SIZE; SIZE = 0;
  delete [] SIZE_LOC; SIZE_LOC = 0;
  delete [] old2new; old2new = 0;
  delete [] new2old; new2old = 0;
  delete [] Z_XG; Z_XG = 0;
} 



//Constructeur de la classe élément
Element::Element(){
	ref = 0 ;
	xi = 0;
	for (MKL_INT i = 0; i < 4; i++) {
		voisin[i] = 0;
	}
	for (MKL_INT i = 0; i < 4; i++){
	  An1[i] = 0.0;
	  An2[i] = 0.0;
	  An3[i] = 0.0;
	  An4[i] = 0.0;
	}
}
MKL_INT Element::get_ref() {
	return ref;
}
void Element::set_ref(MKL_INT N) {
	ref = N;
}

// Element::~Element() {
//   free(OP); OP = 0;
// }

void Element::compute_XG(){
  //Cette calcule le barycentre de la classe élément
  for (MKL_INT j = 0; j < 3; j++) {
    XG[j] = 0.0;
    for (MKL_INT i = 0; i < 4; i++) {
      XG[j] += (noeud[i]->get_X())[j];
    }
    XG[j] *= 0.25;
  }
}



void Element::compute_An_and_nAn(MKL_INT l){
  //Cette fonction calcule A * n et n * A * n avec n vecteur normal à la face numero l
  //Elle calcule également l'impédence Y[l]
  MKL_INT lda = 3;
  MKL_INT inc = 1;
  double zeta = 1.0;
  double yota = 0.0;

  if (l == 0) {
  //An1 = A * n1 avec A = Ac.value
  cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.invmat, lda, n1, inc, yota, An1, inc);
  
  //Y[l] = sqrt(n1\cdot An1) 
  Y[l] = cblas_ddot(lda, n1, inc, An1, inc) ;
  Y[l] = sqrt(Y[l] / xi);
  
  //alphaPP[l] = Y[l] / 4;
  alphaPP[l] = 0.25 * Y[l] ;

  //alphaPV[l] = alphaVP[l] = 0.25
  alphaPV[l] = - 0.25;
  alphaVP[l] = - 0.25;

  //alphaVV[l] = 1/ (4*Y[l])
  alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  else if (l == 1) {
  //An1 = A * n1 avec A = Ac.value
  cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.invmat, lda, n2, inc, yota, An2, inc);
  
  //Y[l] = n1\cdot An1
  Y[l] = cblas_ddot(lda, n2, inc, An2, inc) ;
  Y[l] = sqrt(Y[l] / xi);

  //alphaPP[l] = Y[l] / 4;
  alphaPP[l] = 0.25 * Y[l];

  //alphaPV[l] = alphaVP[l] = 0.25
  alphaPV[l] = - 0.25;
  alphaVP[l] = - 0.25;

  //alphaVV[l] = 1 / (4 * Y[l])
  alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  else if (l == 2) {
  //An1 = A * n1 avec A = Ac.value
  cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.invmat, lda, n3, inc, yota, An3, inc);
  
  //Y[l] = n1\cdot An1
  Y[l] = cblas_ddot(lda, n3, inc, An3, inc) ;
  Y[l] = sqrt(Y[l] / xi);

  //alphaPP[l] =  Y[l] / 4;
  alphaPP[l] = 0.25 * Y[l];

  //alphaPV[l] = alphaVP[l] = 0.25
  alphaPV[l] =  - 0.25;
  alphaVP[l] =  - 0.25;

  //alphaVV[l] = 1 / (4 * Y[l])
  alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  else if (l == 3) {
  //An1 = A * n1 avec A = Ac.value
  cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.invmat, lda, n4, inc, yota, An4, inc);
  
  //Y[l] = n1\cdot An1
  Y[l] = cblas_ddot(lda, n4, inc, An4, inc) ;
  Y[l] = sqrt(Y[l] / xi);

  //alphaPP[l] = Y[l] / 4;
  alphaPP[l] =  0.25 * Y[l];

  //alphaPV[l] = alphaVP[l] = 0.25;
  alphaPV[l] =  - 0.25;
  alphaVP[l] =  - 0.25;

  //alphaVV[l] = 1 / (4 * Y[l])
  alphaVV[l] = 1.0 / (4 * Y[l]);
  }
}



void Element::compute_An_and_nAn(MKL_INT l, double Z){
  //Cette fonction calcule A * n et n * A * n avec n vecteur normal à la face numero l
  //Elle calcule également l'impédence Y[l]
  MKL_INT lda = 3;
  MKL_INT inc = 1;
  double zeta = 1.0;
  double yota = 0.0;

  if (l == 0) {
    
    if (voisin[l] < 0){
      Y[l] = Z;
    }
    else {
      //An1 = A * n1 avec A = Ac.value
      cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.value, lda, n1, inc, yota, An1, inc);
      
      //Y[l] = sqrt(n1\cdot An1)
      Y[l] = cblas_ddot(lda, n1, inc, An1, inc) ;
      Y[l] = sqrt(xi * Y[l]);
    }
    
    //alphaPP[l] = Y[l] / 4;
    alphaPP[l] = 0.25 * Y[l] ;
    
    //alphaPV[l] = alphaVP[l] = 0.25
    alphaPV[l] = - 0.25;
    alphaVP[l] = - 0.25;
    
    //alphaVV[l] = 1/ (4*Y[l])
    alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  
  else if (l == 1) {
    
    if (voisin[l] < 0){
      Y[l] = Z;
    }
    else {
      
      //An1 = A * n1 avec A = Ac.value
      cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.value, lda, n2, inc, yota, An2, inc);
      
      //Y[l] = n1\cdot An1
      Y[l] = cblas_ddot(lda, n2, inc, An2, inc) ;
      Y[l] = sqrt(xi * Y[l]);
    }
    
    //alphaPP[l] = Y[l] / 4;
    alphaPP[l] = 0.25 * Y[l];
    
    //alphaPV[l] = alphaVP[l] = 0.25
    alphaPV[l] = - 0.25;
    alphaVP[l] = - 0.25;
    
    //alphaVV[l] = 1 / (4 * Y[l])
    alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  
  else if (l == 2) {
    
    if (voisin[l] < 0){
      Y[l] = Z;
    }
    else {
      //An1 = A * n1 avec A = Ac.value
      cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.value, lda, n3, inc, yota, An3, inc);
      
      //Y[l] = n1\cdot An1
      Y[l] = cblas_ddot(lda, n3, inc, An3, inc) ;
      Y[l] = sqrt(xi * Y[l]);
    }
    
    //alphaPP[l] =  Y[l] / 4;
    alphaPP[l] = 0.25 * Y[l];
    
    //alphaPV[l] = alphaVP[l] = 0.25
    alphaPV[l] =  - 0.25;
    alphaVP[l] =  - 0.25;
    
    //alphaVV[l] = 1 / (4 * Y[l])
    alphaVV[l] = 1.0 / (4 * Y[l]);
  }
  
  else if (l == 3) {
    
    if (voisin[l] < 0){
      Y[l] = Z;
    }
    else {
      //An1 = A * n1 avec A = Ac.value
      cblas_dgemv(CblasRowMajor, CblasNoTrans, lda, lda, zeta, Ac.value, lda, n4, inc, yota, An4, inc);
      
      //Y[l] = n1\cdot An1
      Y[l] = cblas_ddot(lda, n4, inc, An4, inc) ;
      Y[l] = sqrt(xi * Y[l]);
    }
    
    //alphaPP[l] = Y[l] / 4;
    alphaPP[l] =  0.25 * Y[l];
    
    //alphaPV[l] = alphaVP[l] = 0.25;
    alphaPV[l] =  - 0.25;
    alphaVP[l] =  - 0.25;
    
    //alphaVV[l] = 1 / (4 * Y[l])
    alphaVV[l] = 1.0 / (4 * Y[l]);
  }
}



void Element::xyz(MKL_INT i, MKL_INT j, MKL_INT k, MKL_INT Ndiv, double *x, double *y, double *z) {
  double *x1, *x2, *x3, *x4;
  MKL_INT l = Ndiv - i - j - k;
  x1 = noeud[0]->get_X();
  x2 = noeud[1]->get_X();
  x3 = noeud[2]->get_X();
  x4 = noeud[3]->get_X();
  *x = i * x1[0] + j * x2[0] + k * x3[0] + l * x4[0];
  *x /= Ndiv;
  *y = i * x1[1] + j * x2[1] + k * x3[1] + l * x4[1];
  *y /= Ndiv;
  *z = i * x1[2] + j * x2[2] + k * x3[2] + l * x4[2];
  *z /= Ndiv;
}


double compute_norm(double *X) {
  double r = cblas_ddot(3, X, 1, X, 1);
  r = sqrt(r);
  return r;
}


void Element::compute_hmax(){

  double s1, s2, s3, s4, s5, s6;
  double *x1, *x2, *x3, *x4;
  x1 = noeud[0]->get_X();
  x2 = noeud[1]->get_X();
  x3 = noeud[2]->get_X();
  x4 = noeud[3]->get_X();

  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  double *x3x2 = new double[3];
  double *x4x2 = new double[3];
  double *x4x3 = new double[3];

  for (MKL_INT j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1

    x3x2[j] = x3[j] - x2[j];//x3 - x2
    x4x2[j] = x4[j] - x2[j];//X4 - x2
    x4x3[j] = x4[j] - x3[j];//x4 - x3
  }

  s1 = compute_norm(x2x1);// ||x2 - x1||
  s2 = compute_norm(x3x1);// ||x3 - x1||
  s3 = compute_norm(x4x1);// ||x4 - x1||
  s4 = compute_norm(x3x2);// ||x3 - x2||
  s5 = compute_norm(x4x2);// ||x4 - x2||
  s6 = compute_norm(x4x3);// ||x4 - x3||

  hmax = max(s1, s2); hmin = min(s1, s2);

  hmax = max(hmax,s3); hmin = min(hmin, s3);

  hmax = max(hmax,s4); hmin = min(hmin, s4);

  hmax = max(hmax, s5); hmin = min(hmin, s5);

  hmax = max(hmax, s6); hmin = min(hmin, s6);
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] x3x2; x3x2 = 0;
  delete [] x4x2; x4x2 = 0;
  delete [] x4x3; x4x3 = 0;
}


void Element::compute_hmax(double *x2x1, double *x3x1, double *x4x1, double *x3x2, double *x4x2, double *x4x3){

  double s1, s2, s3, s4, s5, s6;
  double *x1 = nullptr, *x2 = nullptr, *x3 = nullptr, *x4 = nullptr;
  x1 = noeud[0]->get_X();
  x2 = noeud[1]->get_X();
  x3 = noeud[2]->get_X();
  x4 = noeud[3]->get_X();

  for (MKL_INT j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1

    x3x2[j] = x3[j] - x2[j];//x3 - x2
    x4x2[j] = x4[j] - x2[j];//X4 - x2
    x4x3[j] = x4[j] - x3[j];//x4 - x3
  }

  s1 = compute_norm(x2x1);// ||x2 - x1||
  s2 = compute_norm(x3x1);// ||x3 - x1||
  s3 = compute_norm(x4x1);// ||x4 - x1||
  s4 = compute_norm(x3x2);// ||x3 - x2||
  s5 = compute_norm(x4x2);// ||x4 - x2||
  s6 = compute_norm(x4x3);// ||x4 - x3||

  hmax = max(s1, s2); hmin = min(s1, s2);

  hmax = max(hmax,s3); hmin = min(hmin, s3);

  hmax = max(hmax,s4); hmin = min(hmin, s4);

  hmax = max(hmax, s5); hmin = min(hmin, s5);

  hmax = max(hmax, s6); hmin = min(hmin, s6);
}



void Element::combinaison_solution(MKL_INT ndof, MKL_INT e, VecN &ALPHA, double *X){
  //Cette fonction calcul la solution locale asscoiée à l'élément e en X et la stocke en u = \sum_{iloc = 1}{ndof} ALPHA[i] * exp(i K X) (pour la pression) et en Vexacte pour la vitesse
  /*-------------------------------------------------------------- input ----------------------------------------
    ndof     : le nombre de degré de liberté en chaque élément
    e        : le numero de l'élément 
    ALPHA    : les coordonnées de la solution
    X        : la variable est évalué la solution
    /*------------------------------------------------------------- ouput ---------------------------------------
    Pexacte  : la pression
    Vexacte  : la vitesse
    /*-----------------------------------------------------------------------------------------------------------*/
  Pexacte = cplx(0.0,0.0);
  
  for (MKL_INT i = 0; i < 3; i++)
    Vexacte[i] = cplx(0.0,0.0);
  

  cplx *alpha_iloc;//pour stocker ALPHA[e] = solution locale sur e
  
  ALPHA.get_F_elem(ndof, e, &alpha_iloc);
  
  for(MKL_INT iloc = 0; iloc < ndof; iloc++){

    //calcul de P et V en X. P est stockée dans OP[iloc].P et V dans OP[iloc].V
    OP[iloc].compute_P_and_V(X);

    //P = P + alpha_iloc * P_iloc avec P_iloc = OP[iloc].P
    Pexacte += alpha_iloc[iloc] * OP[iloc].P;  
    
    //V = V + alpha_iloc * V_iloc avec V_iloc = OP[iloc].V
    for(MKL_INT i = 0; i < 3; i++){
   
      Vexacte[i] += alpha_iloc[iloc] * OP[iloc].V[i];
    }
    
  }
}





void Element::print_info() {
  //information sur la classe élément
	cout << "ref = " << ref << ", ref noeud 0 = " << noeud[0]->get_ref() << ", ref noeud 1 = " << noeud[1]->get_ref() << ", ref noeud 2 = " << noeud[2]->get_ref() << ", ref noeud 3 = " << noeud[3]->get_ref() << endl;
	cout << "voisin 0 = " << voisin[0] << " voisin 1 = " << voisin[1] << " voisin 2 = " << voisin[2] << " voisin 3 = " << voisin[3] << endl;
	cout<<"**************************************************** de l'element : *******************************************"<<endl;
	cout<<"XG(0) = "<<XG[0]<<" XG(1) ="<<XG[1]<<" XG(2) = "<<XG[2]<<" norm = "<< sqrt(XG[0] * XG[0] + XG[1] * XG[1] + XG[2] * XG[2])<<endl;
		cout<<"**************************************************** autres infos : *******************************************"<<endl;
	cout << "noeud 0 : ";
	noeud[0]->print_info();
	cout<<"n1 = ["<<n1[0]<<" ,"<<n1[1]<<" ,"<<n1[2]<<"]"<<endl;
	cout << "noeud 1 : ";
	noeud[1]->print_info();
	cout<<"n2 = ["<<n2[0]<<" ,"<<n2[1]<<" ,"<<n2[2]<<"]"<<endl;
	cout << "noeud 2 : " ;
	noeud[2]->print_info();
	cout<<"n3 = ["<<n3[0]<<" ,"<<n3[1]<<" ,"<<n3[2]<<"]"<<endl;
	cout << "noeud 3 : " ;
	noeud[3]->print_info();
	cout<<"n4 = ["<<n4[0]<<" ,"<<n4[1]<<" ,"<<n4[2]<<"]"<<endl;
//
	cout << "face 0 : ";
	face[0].print_info();
	cout << "face 1 : ";
	face[1].print_info();
	cout << "face 2 : ";
	face[2].print_info();
	cout << "face 3 : ";
	face[3].print_info();
	cout<<"coeff physiques : "<<endl;
	cout<<"xi = "<<xi<<endl;
	cout<<"Q_T = "<<Q_T<<endl;
	cout<<"alphaPP(0) = "<<alphaPP[0]<<" alphaPP(1) = "<<alphaPP[1]<<" alphaPP(2) = "<<alphaPP[2]<<" alphaPP(3) = "<<alphaPP[3]<<endl;
	cout<<"alphaPV(0) = "<<alphaPV[0]<<" alphaPV(1) = "<<alphaPV[1]<<" alphaPV(2) = "<<alphaPV[2]<<" alphaPV(3) = "<<alphaPV[3]<<endl;
	cout<<"alphaVP(0) = "<<alphaVP[0]<<" alphaVP(1) = "<<alphaVP[1]<<" alphaVP(2) = "<<alphaVP[2]<<" alphaVP(3) = "<<alphaVP[3]<<endl;
	cout<<"alphaVV(0) = "<<alphaVV[0]<<" alphaVV(1) = "<<alphaVV[1]<<" alphaVV(2) = "<<alphaVV[2]<<" alphaVV(3) = "<<alphaVV[3]<<endl;

	cout<<"betaPP(0) = "<<betaPP[0]<<" betaPP(1) = "<<betaPP[1]<<" betaPP(2) = "<<betaPP[2]<<" betaPP(3) = "<<betaPP[3]<<endl;
	cout<<"betaPV(0) = "<<betaPV[0]<<" betaPV(1) = "<<betaPV[1]<<" betaPV(2) = "<<betaPV[2]<<" betaPV(3) = "<<betaPV[3]<<endl;
	cout<<"betaVP(0) = "<<betaVP[0]<<" betaVP(1) = "<<betaVP[1]<<" betaVP(2) = "<<betaVP[2]<<" betaVP(3) = "<<betaVP[3]<<endl;
	cout<<"betaVV(0) = "<<betaVV[0]<<" betaVV(1) = "<<betaVV[1]<<" betaVV(2) = "<<betaVV[2]<<" betaVV(3) = "<<betaVV[3]<<endl;

	cout<<"TL_PP(0) = "<<TL_PP[0]<<" TL_PP(1) = "<<TL_PP[1]<<" TL_PP(2) = "<<TL_PP[2]<<" TL_PP(3) = "<<TL_PP[3]<<endl;
	cout<<"TL_PV(0) = "<<TL_PV[0]<<" TL_PV(1) = "<<TL_PV[1]<<" TL_PV(2) = "<<TL_PV[2]<<" TL_PV(3) = "<<TL_PV[3]<<endl;
	cout<<"TL_VP(0) = "<<TL_VP[0]<<" TL_VP(1) = "<<TL_VP[1]<<" TL_VP(2) = "<<TL_VP[2]<<" TL_VP(3) = "<<TL_VP[3]<<endl;
	cout<<"TL_VV(0) = "<<TL_VV[0]<<" TL_VV(1) = "<<TL_VV[1]<<" TL_VV(2) = "<<TL_VV[2]<<" TL_VV(3) = "<<TL_VV[3]<<endl;

	cout<<"LT_PP(0) = "<<LT_PP[0]<<" LT_PP(1) = "<<LT_PP[1]<<" LT_PP(2) = "<<LT_PP[2]<<" LT_PP(3) = "<<LT_PP[3]<<endl;
	cout<<"LT_PV(0) = "<<LT_PV[0]<<" LT_PV(1) = "<<LT_PV[1]<<" LT_PV(2) = "<<LT_PV[2]<<" LT_PV(3) = "<<LT_PV[3]<<endl;
	cout<<"LT_VP(0) = "<<LT_VP[0]<<" LT_VP(1) = "<<LT_VP[1]<<" LT_VP(2) = "<<LT_VP[2]<<" LT_VP(3) = "<<LT_VP[3]<<endl;
	cout<<"LT_VV(0) = "<<LT_VV[0]<<" LT_VV(1) = "<<LT_VV[1]<<" LT_VV(2) = "<<LT_VV[2]<<" LT_VV(3) = "<<LT_VV[3]<<endl;
	cout<<" Y[0] = "<<Y[0]<<" Y[1] = "<<Y[1]<<" Y[2] = "<<Y[2]<<" Y[3] = "<<Y[3]<<endl;


	cout<<"T_PP(0) = "<<T_PP[0]<<" T_PP(1) = "<<T_PP[1]<<" T_PP(2) = "<<T_PP[2]<<" T_PP(3) = "<<T_PP[3]<<endl;
	cout<<"T_PV(0) = "<<T_PV[0]<<" T_PV(1) = "<<T_PV[1]<<" T_PV(2) = "<<T_PV[2]<<" T_PV(3) = "<<T_PV[3]<<endl;
	cout<<"T_VP(0) = "<<T_VP[0]<<" T_VP(1) = "<<T_VP[1]<<" T_VP(2) = "<<T_VP[2]<<" T_VP(3) = "<<T_VP[3]<<endl;
	cout<<"T_VV(0) = "<<T_VV[0]<<" T_VV(1) = "<<T_VV[1]<<" T_VV(2) = "<<T_VV[2]<<" T_VV(3) = "<<T_VV[3]<<endl;

	cout<<"L_PP(0) = "<<L_PP[0]<<" L_PP(1) = "<<L_PP[1]<<" L_PP(2) = "<<L_PP[2]<<" L_PP(3) = "<<L_PP[3]<<endl;
	cout<<"L_PV(0) = "<<L_PV[0]<<" L_PV(1) = "<<L_PV[1]<<" L_PV(2) = "<<L_PV[2]<<" L_PV(3) = "<<L_PV[3]<<endl;
	cout<<"L_VP(0) = "<<L_VP[0]<<" L_VP(1) = "<<L_VP[1]<<" L_VP(2) = "<<L_VP[2]<<" L_VP(3) = "<<L_VP[3]<<endl;
	cout<<"L_VV(0) = "<<L_VV[0]<<" L_VV(1) = "<<L_VV[1]<<" L_VV(2) = "<<L_VV[2]<<" L_VV(3) = "<<L_VV[3]<<endl;


	cout<<"alpha_PincP(0) = "<<alpha_PincP[0]<<" alpha_PincP(1) = "<<alpha_PincP[1]<<" alpha_PincP(2) = "<<alpha_PincP[2]<<" alpha_PincP(3) = "<<alpha_PincP[3]<<endl;

	cout<<"alpha_PincV(0) = "<<alpha_PincV[0]<<" alpha_PincV(1) = "<<alpha_PincV[1]<<" alpha_PincV(2) = "<<alpha_PincV[2]<<" alpha_PincV(3) = "<<alpha_PincV[3]<<endl;

	cout<<"alpha_VincP(0) = "<<alpha_VincP[0]<<" alpha_VincP(1) = "<<alpha_VincP[1]<<" alpha_VincP(2) = "<<alpha_VincP[2]<<" alpha_VincP(3) = "<<alpha_VincP[3]<<endl;

	cout<<"alpha_VincV(0) = "<<alpha_VincV[0]<<" alpha_VincV(1) = "<<alpha_VincV[1]<<" alpha_VincV(2) = "<<alpha_VincV[2]<<" alpha_VincV(3) = "<<alpha_VincV[3]<<endl;
	
	cout<<"Ac = "<<endl;
	Ac.print_info();

}

/* Constructeur de la classe Face */
Face::Face() {
	ref = 0 ;
	Aire = 0.0;
	for (MKL_INT j=0 ; j<3 ; j++ ){
		XG[j] = 0;
	}
}


void Face::print_info() {
  //information sur la classe Face
	cout << "ref = " << ref << endl;
	cout << "XG = (" << XG[0] << "," << XG[1] << "," << XG[2] << ")" << endl;
	noeud[0]->print_info();
	noeud[1]->print_info();
	noeud[2]->print_info();
	cout << "voisin1 = " << voisin1 << endl ;
	cout << "voisin2 = " << voisin2 << endl;
	if (voisin1 == voisin2) {
		cout << "Gros bug de voisin dans Source_Mesh.cpp Face::print_info()" << endl;
		exit(1);
	}
}
void Face::compute_XG() {
  //Cette fonction calcule le barycentre XG de la face correspondante
	for (MKL_INT j = 0; j < 3; j++) {
		XG[j] = 0;
		for (MKL_INT i = 0; i < 3; i++) {
			XG[j] += (noeud[i]->get_X())[j];
		}
		XG[j] /= 3.0;
	}
}
void Face::set_ref(MKL_INT N) {
	ref = N;
}
MKL_INT Face::get_ref() {
	return ref ;
}

void Face::compute_Aire_triangle() {
  //Cette fonction calcule l'aire du triangle associé à la face F
  /*----------------------------------------------------------- input -------------------------------------------
    F    : objet de type Face
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Aire   : l'aire du triangle
    /*-----------------------------------------------------------------------------------------------------------------*/
  
  double *X1 =nullptr;
  double *X2 =nullptr;
  double *X3=nullptr;
  
  X1 = noeud[0]->get_X();
  X2 = noeud[1]->get_X();
  X3 = noeud[2]->get_X();
  double *X2X3=nullptr, *X1X3=nullptr;
  X2X3 = new double[3];
  X1X3 = new double[3];

  
  for (MKL_INT i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
  }

  //computing the volume
  
  double *prod_Vecto = produit_vectoriel(X1X3, X2X3);

  Aire = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
  Aire = sqrt(Aire);
  
  delete [] X2X3; X2X3 = nullptr;
  delete [] X1X3; X1X3 = nullptr;
  delete [] prod_Vecto; prod_Vecto = nullptr;
 

}


void Face::compute_Aire_triangle(double *X1X3, double *X2X3, double *prod_Vecto) {
  //Cette fonction calcule l'aire du triangle associé à la face F
  /*----------------------------------------------------------- input -------------------------------------------
    F    : objet de type Face
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Aire   : l'aire du triangle
    /*-----------------------------------------------------------------------------------------------------------------*/
  
  double *X1 =nullptr;
  double *X2 =nullptr;
  double *X3=nullptr;
  
  X1 = noeud[0]->get_X();
  X2 = noeud[1]->get_X();
  X3 = noeud[2]->get_X();
  
  for (MKL_INT i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
  }

  //computing the volume
  
  produit_vectoriel(X1X3, X2X3, prod_Vecto);

  Aire = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
  Aire = sqrt(Aire);

}



/*
void Face::set_voisin(MKL_INT N) {
	voisin = N;
}
MKL_INT Face::get_voisin() {
	return voisin;
}
*/
void Export_Points(MKL_INT np, double *points){
  //ecriture des noeuds du maillage sur un fichier
  string const chaine("points_MSphere.txt");
  ofstream fic(chaine.c_str());
  if(fic)
    {
      MKL_INT address;
      //writing the number of nodes
      fic<<np<<"\n";
      
      //writing the nodes
      for(MKL_INT i = 0 ; i < np; i++){
	address = 3*i;
	fic<<points[address]<<" "<<points[address+1]<<" "<<points[address+2]<<"\n";
      }
    }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }
}


void Export_PointsSphere(MKL_INT np, double *points){
  //ecriture des vecteurs directionnels de la sphère unité sur un fichier
  MKL_INT l = 0;
  string const chaine("pointsSphere_075.txt");
  ofstream fic(chaine.c_str());
  if(fic)
    {
      MKL_INT address;
      //writing the number of nodes
     
      //writing the nodes
      for(MKL_INT i = 0 ; i < np; i++){
	address = 3*i;
	double norm = sqrt(points[address] * points[address] + points[address+1] * points[address+1] + points[address+2] * points[address+2]);
	if ((norm >=0.9) && (norm <=1)){
	  fic<<points[address]<<" "<<points[address+1]<<" "<<points[address+2]<<"\n";
	  l++;
	}
      }
    }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }

  string const chaine2("taillepointsSphere_075.txt");
  ofstream gic(chaine2.c_str());
  if(gic){
    gic<<l;
  }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }
}



void Export_Tetra(MKL_INT ne, MKL_INT *tetra){
  //ecriture des tetras sur un fichier
  string const chaine("tetra_MSphere.txt");
  ofstream fic(chaine.c_str());
  if(fic)
    {
      MKL_INT address;
      //writing the number of tetra
      fic<< ne<<"\n";
      
      //writing tetra
      for(MKL_INT i = 0 ; i < ne; i++){
	address = 4*i;
	fic<<tetra[address]<<" "<<tetra[address+1]<<" "<<tetra[address+2]<<" "<<tetra[address+3]<<"\n";
      }
    }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }
}

void Export_Voisin_Tetra(MKL_INT ne, MKL_INT *voisin){
  //ecriture des voisins des tetras sur un fichier
  string const chaine("voisin_MSphere.txt");
  ofstream fic(chaine.c_str());
  if(fic)
    {
      MKL_INT address;
      //writing the number of tetra
      fic<<ne<<"\n";
      
      //writing the neighbours of tetra
      for(MKL_INT i = 0 ; i < ne; i++){
	address = 4*i;
	fic<<voisin[address]<<" "<<voisin[address+1]<<" "<<voisin[address+2]<<" "<<voisin[address+3]<<"\n";
      }
    }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }
}


void Export_Loc2glob(MKL_INT ne){
  
  string const chaine("loc2glob_MSphere.txt");
  ofstream fic(chaine.c_str());
  if(fic)
    {
      MKL_INT address;
      //writing the number of tetra
      fic<<4*ne<<"\n";
      
      //writing the neighbours of tetra
      for(MKL_INT i = 0 ; i < ne; i++){
	address = 4*i;
	fic<<address<<" "<<address+1<<" "<<address+2<<" "<<address+3<<"\n";
      }
    }
  else{
    cout<<"probleme d'ouverture avec le fichier"<<"\n";
  }
}


//En domaine normale, utiliser les fichiers L03_05_10 .

double* Read_Points(MKL_INT *np){
  //lecteur du fichier contenant les noeuds du maillage 
  MKL_INT i, n, l;
  double x1, x2, x3, *P = NULL;
  ifstream fic("data/points_6tetra.txt");
  if(fic) {
      fic>>n;
      *np = n;
      P = new double[3 * n]; 
      for(i = 0; i<n; i++){
		fic >> x1 >> x2 >> x3 ;
		l = 3*i;
		P[l] = x1; 
		P[l+1] = x2; 
		P[l+2] = x3;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier points"<<"\n";
  }
  return P;
}


double* Read_Points2(MKL_INT *np){
  //lecteur du fichier contenant les noeuds du maillage 
  MKL_INT i, n, l;
  double x1, x2, x3, *P = NULL;
  ifstream fic("data/points_MSphere_0125.txt");
  if(fic) {
      fic>>n;
      *np = n;
      P = new double[3 * n]; 
      for(i = 0; i<n; i++){
		fic >> x1 >> x2 >> x3 ;
		l = 3*i;
		P[l] = x1; 
		P[l+1] = x2; 
		P[l+2] = x3;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier points"<<"\n";
  }
  return P;
}



 

double* Read_PointsSphere(MKL_INT *npS){
  //lecteur des vecteurs directionnels de la sphère unité
  MKL_INT i, n, l;
  double x1, x2, x3, *P = NULL;
  /*
  ifstream gic("data/taillepointsSphere_075.txt");
  if(gic) {
    gic>>n;
    *npS = n;
  }
  else{
    cout<<"probleme d''ouverture du fichier"<<"\n";
  }
  */ 
  ifstream fic("data/direction_phase_05_22.txt");
  if(fic) {
     fic>>n;
    *npS = n;
      P = new double[3 * n]; 
      for(i = 0; i<n; i++){
		fic >> x1 >> x2 >> x3 ;
		l = 3*i;
		P[l] = x1; 
		P[l+1] = x2; 
		P[l+2] = x3;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier"<<"\n";
  }
  return P;
}


MKL_INT* Read_Tetra(MKL_INT *ne){
  //lecture des tetras à partir d'un fichier
  MKL_INT i, n, l;
  MKL_INT x1, x2, x3, x4, *T = NULL;
  ifstream fic("data/tetra_6tetra.txt");
  if(fic)
    {
      fic>>n;
      *ne = n;
      T = new MKL_INT[4 * n]; 
      for(i = 0; i<n; i++){
	fic>>x1>>x2>>x3>>x4;
	l = 4*i;
	T[l] = x1-1; T[l+1] = x2-1; T[l+2] = x3-1; T[l+3] = x4-1;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier tetra"<<"\n";
  }
  return T;
}


MKL_INT* Read_Tetra2(MKL_INT *ne){
  //lecture des tetras à partir d'un fichier
  MKL_INT i, n, l;
  MKL_INT x1, x2, x3, x4, *T = NULL;
  ifstream fic("data/tetra_MSphere_0125.txt");
  if(fic)
    {
      fic>>n;
      *ne = n;
      T = new MKL_INT[4 * n]; 
      for(i = 0; i<n; i++){
	fic>>x1>>x2>>x3>>x4;
	l = 4*i;
	T[l] = x1-1; T[l+1] = x2-1; T[l+2] = x3-1; T[l+3] = x4-1;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier tetra"<<"\n";
  }
  return T;
}


MKL_INT* Read_Neighbours_Tetra(){
  //lecture des voisins des tetras à partir d'un fichier
  MKL_INT i, n, l;
  MKL_INT i1, i2, i3, i4, *T = NULL;
  ifstream fic("data/voisin_6tetra.txt");
  if(fic)
    {
      fic>>n;
      T =  new MKL_INT[4 * n];
      for(i = 0; i<n; i++){
	fic>>i1>>i2>>i3>>i4;
	l = 4*i;
	T[l] = i1-1; T[l+1] = i2-1; T[l+2] = i3-1; T[l+3] = i4-1;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier voisin"<<"\n";
  }
  return T;
}


MKL_INT* Read_Neighbours_Tetra2(){
  //lecture des voisins des tetras à partir d'un fichier
  MKL_INT i, n, l;
  MKL_INT i1, i2, i3, i4, *T = NULL;
  ifstream fic("data/voisin_MSphere_0125.txt");
  if(fic)
    {
      fic>>n;
      T =  new MKL_INT[4 * n];
      for(i = 0; i<n; i++){
	fic>>i1>>i2>>i3>>i4;
	l = 4*i;
	T[l] = i1-1; T[l+1] = i2-1; T[l+2] = i3-1; T[l+3] = i4-1;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier voisin"<<"\n";
  }
  return T;
}


MKL_INT* Read_Loc2glob(MKL_INT *taille){
  MKL_INT i, n, l;
  MKL_INT j1, j2, j3, j4;
  MKL_INT *loctoglob = NULL;
  ifstream fic("data/loc2glob_0125_05.txt");
  if(fic)
    {
      fic>>n;
      *taille = n;
      loctoglob =  new MKL_INT[4 * n]; 
      for(i = 0; i<n; i++){
	fic>>j1>>j2>>j3>>j4;
	l = 4*i;
	loctoglob[l] = j1; loctoglob[l+1] = j2; loctoglob[l+2] = j3; loctoglob[l+3] = j4;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier loc2glob"<<"\n";
  }
  return loctoglob;
}




MKL_INT ordre(double* X, double* Y, MKL_INT taille) {
  //Cette compare les vecteurs X et Y

	// retourne 1 si X >Y
	// retourne 2 si Y>X
	// retourne 0 si Y=X
  	double epsi = .000000001;
	if (X[0] > Y[0] + epsi) // X1 >Y1
	{
		return 1;
	}
	else
	{
		if (X[0] < Y[0] - epsi) { //X1 <Y1
			return 2;
		}
		else
		{// X1=Y1
			if (X[1] > Y[1] + epsi) // X1 = Y1 et X2 > Y2
			{
				return 1;
			}
			else {
				if (X[1] < Y[1] - epsi) { // X1 = Y1 et X2 < Y2
					return 2;
				}
				else { // X1 = Y1 et X2 = Y2
					if (X[2] > Y[2] + epsi) // X1 = Y1 et X2 = Y2 et X3>Y3
					{
						return 1;
					}
					else {
						if (X[2] < Y[2] - epsi) { // X1 = Y1 et X2 = Y2 et X3<Y3
							return 2;
						}
						else {
							return 0;
						}
					}

				}
			}
		}
	}
}



MKL_INT ordonner(double *X, double *Y){
  //Cette compare les vecteurs X et Y suivant la composante en z

	// retourne 1 si X >Y
	// retourne 2 si Y>X
	// retourne 0 si Y=X
        double epsi = .000000001;
	if (X[2] > Y[2] + epsi) // X3 >Y3
	{
		return 1;
	}
	else
	{
		if (X[2] < Y[2] - epsi) { //X3 <Y3
			return 2;
		}
		else
		{// X3 = Y3
			if (X[1] > Y[1] + epsi) // X3 = Y3 et X2 > Y2
			{
				return 1;
			}
			else {
				if (X[1] < Y[1] - epsi) { // X3 = Y3 et X2 < Y2
					return 2;
				}
				else { // X3 = Y3 et X2 = Y2
					if (X[0] > Y[0] + epsi) // X3 = Y3 et X2 = Y2 et X1>Y1
					{
						return 1;
					}
					else {
						if (X[0] < Y[0] - epsi) { // X3 = Y3 et X2 = Y2 et X1<Y1
							return 2;
						}
						else {
							return 0;
						}
					}

				}
			}
		}
	}
}


void tri_recursive(MKL_INT debut, MKL_INT fin, Maillage &Mesh) {
  //tri recursive par Etienne Poulin – ISN – Novembre 2013  
  const MKL_INT pivot = Mesh.old2new[debut];
  MKL_INT pos = debut;
  MKL_INT i;
  if (debut < fin)
    {
      
      /* cette boucle permet de placer le pivot (début du tableau à trier)
	 au bon endroit dans le tableau
	 avec toutes les valeurs plus petites avant
	 et les valeurs plus grandes après
	 à la fin, la valeur pivot se trouve dans le tableau à tab[pos]
      */
      for (i = debut; i < fin; i++)
	{
	  if (Mesh.elements[Mesh.old2new[i]].XG[2] < Mesh.elements[pivot].XG[2]) {
	    
	    Mesh.old2new[pos] = Mesh.old2new[i];
	    pos++;
	    Mesh.old2new[i] = Mesh.old2new[pos];
	    Mesh.old2new[pos] = pivot;
	  }
	}
      
      /* Il ne reste plus qu'à rappeler la procédure de tri
	 sur le début du tableau jusquà pos (exclu) : tab[pos-1]
      */
      tri_recursive(debut, pos, Mesh);
  
      /* et de rappeler la procédure de tri
	 sur la fin du tableau à partir de la première valeur après le pivot
	 tab[pos+1]
      */
      tri_recursive(pos+1, fin, Mesh);
    }
}

void tri_selection(double *tab, MKL_INT debut, MKL_INT fin) {
  
  MKL_INT i,j;
  double c;
  
  for(i = debut ; i < fin-1 ; i++){
    for(j = i+1 ; j < fin ; j++){
      if ( tab[i] > tab[j] ) {
 	c = tab[i];
 	tab[i] = tab[j];
 	tab[j] = c;
      }   
    }    
  }

}


void tri_selection(MKL_INT debut, MKL_INT fin, Maillage &Mesh) {
  
  MKL_INT i,j;
  MKL_INT c;
  
  for(i = debut ; i < fin-1 ; i++){
    for(j = i+1 ; j < fin ; j++){
      if ( Mesh.elements[Mesh.old2new[i]].XG[2] > Mesh.elements[Mesh.old2new[j]].XG[2] ) {
 	c = Mesh.old2new[i];
 	Mesh.old2new[i] = Mesh.old2new[j];
 	Mesh.old2new[j] = c;
      }   
    }    
  }

}


void tri_maillage(Maillage &Mesh) {

  MKL_INT i;
  cout<<"Initialisation de old2new: "<<endl;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    Mesh.old2new[i] = i;
    cout<<"old2new["<<i<<"] = "<<Mesh.old2new[i]<<endl;
  }


  cout<<"tri du tableau old2new de taille "<<Mesh.get_N_elements()<<" : "<<endl;
  //tri_selection(0, Mesh.get_get_N_elements()(), Mesh);
  tri_recursive(0, Mesh.get_N_elements(), Mesh);
}


/*void Maillage::tri_maillage() {
  //Cette fonction initialise et tri le tableau old2new des indices des éléments en fonction des barycentres des éléments suivant l'axe oz
  MKL_INT i;
  cout<<"Initialisation de old2new: "<<endl;
  for (i = 0; i < get_get_N_elements()(); i++) {
    old2new[i] = i;
  }

  cout<<"tri du tableau old2new : "<<endl;
  tri_recursive(0, get_N_elements(), *this);
}
*/


void choisir_A_et_xi(Element &Elem, double sld, double slg, MKL_INT choice) {
  //Cette fonction permet de définir les coeffs A et xi de l'equation Helmholtz sur un élément
  /*----------------------------------------------------------------------------------------
    Elem       : classe d'un élément
    sld et slg : servent de seuil pour définir un mixte d'homogène et d'hétérogène
    choice     : permet de choisir notre milieu selon choice = 0 pour homogène,
                  choice = 1 un milieu mixte et choice = 2 pour un milieu hétérogène
    ---------------------------------------------------------------------------------------*/
  if (choice == 0) {
    Elem.xi = 1.0;
    Elem.Ac.define(1.0,0.0,0.0,
		   0.0,1.0,0.0,
		   0.0,0.0,1.0);
   
  }
  else if (choice == 1) {
    double r = sqrt(Elem.XG[0] * Elem.XG[0] + Elem.XG[1] * Elem.XG[1] + Elem.XG[2] * Elem.XG[2]);
    if ((r > sld) || (r < slg)) {
      Elem.xi = 1.0;
      Elem.Ac.define(1.0,0.0,0.0,
		     0.0,1.0,0.0,
		     0.0,0.0,1.0);
    }
    else if ((r <= sld) || (r >= slg)){
      Elem.xi = 0.5;
      Elem.Ac.define(3.0,1.0,1.0,
		     1.0,3.0,1.0,
		     1.0,1.0,3.0);
    }
  }
  else if (choice == 2){
    Elem.xi = 1.0 + abs(Elem.XG[0]) + abs(Elem.XG[1]) + abs(Elem.XG[2]);
    MKL_INT l = 0;
    for (MKL_INT i = 0; i < 3; i++){
      for (MKL_INT j = 0; j < 3; j++){
	if (i == j){
	  Elem.Ac.value[l] = 1.0 + Elem.XG[i] * Elem.XG[j];
	}
	else {
	  Elem.Ac.value[l] =  Elem.XG[i] * Elem.XG[j];
	}
	l++;
      }
    }
    
  }
  // else if (choice == 2){
  //   MKL_INT a = 0;
  //   for (MKL_INT s = 0; s < 4; s++) {
      
  //     a += determine_position(Elem.face[s], L);
      
  //   }

  //   if (a < 0) {
  //     Elem.xi = 1.0 + sqrt(Elem.XG[0] * Elem.XG[0] + Elem.XG[1] * Elem.XG[1] + Elem.XG[2] * Elem.XG[2]);
  //     MKL_INT l = 0;
  //     for (MKL_INT i = 0; i < 3; i++){
  // 	for (MKL_INT j = 0; j < 3; j++){
  // 	  if (i == j){
  // 	    Elem.Ac.value[l] = 1.0 + Elem.XG[i] * Elem.XG[j];
  // 	  }
  // 	  else {
  // 	    Elem.Ac.value[l] =  Elem.XG[i] * Elem.XG[j];
  // 	  }
  // 	  l++;
  // 	}
  //     }
      
  //   }
    
  //   else {
      
  //     Elem.xi = 1.0 + abs(Elem.XG[0]) + abs(Elem.XG[1]) + abs(Elem.XG[2]);
  //     MKL_INT l = 0;
  //     for (MKL_INT i = 0; i < 3; i++){
  // 	for (MKL_INT j = 0; j < 3; j++){
  // 	  if (i == j){
  // 	    Elem.Ac.value[l] = 1.0 + Elem.XG[i];
  // 	  }
  // 	  else {
  // 	    Elem.Ac.value[l] =  Elem.XG[i] * Elem.XG[j];
  // 	  }
  // 	  l++;
  // 	}
  //     }
      
  //   }
    
  // }
}



/*void definir_A(Element &Elem) {
  MKL_INT i, j, l;
  l = 0;
  for (i = 0; i < 3; i++){
    for (j = 0; j < 3; j++){
      Elem.Ac.value[l] = 1.0 + Elem.XG[i] * Elem.XG[j];
      l++;
    }
  }
}
*/


double * produit_vectoriel(double *U, double *V){
  //Ici on calcule le produit vectoriel de deux vecteurs U et W de type double
  double *W = new double[3];//Le résultat est stocké dans ce vecteur
  W[0] = U[1] * V[2] - U[2] * V[1];
  W[1] = U[2] * V[0] - U[0] * V[2];
  W[2] = U[0] * V[1] - U[1] * V[0];
  return W;
}

void produit_vectoriel(double *U, double *V, double *W){
  //Ici on calcule le produit vectoriel de deux vecteurs U et W de type double
  //Le résultat est stocké dans le vecteur W
  W[0] = U[1] * V[2] - U[2] * V[1];
  W[1] = U[2] * V[0] - U[0] * V[2];
  W[2] = U[0] * V[1] - U[1] * V[0];
}




void get_normalToface(Face &F, double *n0){
//Ici on définit la normale à la face F avec F de type Face
  /*-----------------------------------------------------------------------------------------------------------------------------------
                                                   To get the normal to a face

   /*-------------------------------------------------------- input  -----------------------------------------------------------------------
   F.noeud[0] : coordinates of the first node associated to the triangle

   F.noeud[1] : coordinates of the second node associated to the triangle

   F.noeud[2] : coordinates of the third node associated to the triangle

   /*--------------------------------------------------------- input-ouput------------------------------------------------------------------------------
   n0 : where to store the normal
   /*------------------------------------------------------------MKL_INTermediates -----------------------------------------------------------------------------
   X2X1 : F.noeud[1] - F.noeud[0]
   X2X3 : F.noeud[1] - F.noeud[2]
   norm : norm ( X2X1 x X3X1) (x : refer to vectoriel product)
   /*--------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  double *X2X1 = new double[3];
  double *X2X3 = new double[3];
  //double *prod_Vecto = (double *)malloc(3*sizeof(double));
  double norm;
  MKL_INT i;
  for (i = 0; i < 3; i++){
    X2X1[i] = (F.noeud[1]->get_X())[i] - (F.noeud[0]->get_X())[i];
    
    X2X3[i] = (F.noeud[1]->get_X())[i] - (F.noeud[2]->get_X())[i];
   
  }
  
  double *prod_Vecto = produit_vectoriel(X2X1, X2X3);


  norm = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
  norm = sqrt(norm);
  
  norm = 1.0 / norm;
  
  for (i = 0; i < 3; i++){
  n0[i] = norm * prod_Vecto[i];
  }

  delete [] X2X1; X2X1 = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] prod_Vecto; prod_Vecto = 0;
}




void get_exteriorNormalToElem(Element &Elem){
//Cette fonction sert à orienter les vecteurs normaux à un élément
  double *X2_X1 = new double[3];

  double *X4_X2 = new double[3];

  double *X1_X3 = new double[3];

  double *X3_X4 = new double[3];

  

  double beta_1, beta_2, beta_3, beta_4;

  for( MKL_INT s = 0; s < 3; s++){
    //X2_X1[s] = X2[s] - X1[s];
    X2_X1[s] = (Elem.noeud[1]->get_X())[s] - (Elem.noeud[0]->get_X())[s];
    
    //X3_X4[s] = X3[s] - X4[s];
    X3_X4[s] = (Elem.noeud[2]->get_X())[s] - (Elem.noeud[3]->get_X())[s];
    
    //X4_X2[s] = X4[s] - X2[s];
    X4_X2[s] = (Elem.noeud[3]->get_X())[s] - (Elem.noeud[1]->get_X())[s];

    //X1_X3[s] = X1[s] - X3[s];
    X1_X3[s] = (Elem.noeud[0]->get_X())[s] - (Elem.noeud[2]->get_X())[s];
  }

  //orienting the normal n1 to face (X2 X3 X4)
  get_normalToface(Elem.face[0], Elem.n1);

  beta_1 = cblas_ddot(3, X2_X1, 1, Elem.n1, 1);
  //cout<<"beta_1 = "<<beta_1<<"\n";
  if (beta_1 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n1[s] = -Elem.n1[s];
    }
  }
  //orienting the normal n2 to face (X1 X3 X4)
  get_normalToface(Elem.face[1], Elem.n2);

  beta_2 = cblas_ddot(3, X4_X2, 1, Elem.n2, 1);
  //cout<<"beta_2 = "<<beta_2<<"\n";
  if (beta_2 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n2[s] = -Elem.n2[s];
    }
  }
  //orienting the normal n3 to face (X1 X2 X4)
  get_normalToface(Elem.face[2], Elem.n3);

  beta_3 = cblas_ddot(3, X1_X3, 1, Elem.n3, 1);
  // cout<<"beta_3 = "<<beta_3<<"\n";
  if (beta_3 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n3[s] = -Elem.n3[s];
    }
  }
  //orienting the normal n4 to face (X1 X2 X3)
  get_normalToface(Elem.face[3], Elem.n4);

  beta_4 = cblas_ddot(3, X3_X4, 1, Elem.n4, 1);
  //cout<<"beta_4 = "<<beta_4<<"\n";
  if (beta_4 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n4[s] = -Elem.n4[s];
    }
  }

  //free memory
  delete [] X2_X1; X2_X1 = 0;
  delete [] X4_X2; X4_X2 = 0;
  delete [] X1_X3; X1_X3 = 0;
  delete [] X3_X4; X3_X4 = 0;
}


void get_exteriorNormalToElem(Element &Elem, double *X2_X1, double *X4_X2, double *X1_X3, double *X3_X4){
//Cette fonction sert à orienter les vecteurs normaux à un élément  

  double beta_1, beta_2, beta_3, beta_4;

  for( MKL_INT s = 0; s < 3; s++){
    //X2_X1[s] = X2[s] - X1[s];
    X2_X1[s] = (Elem.noeud[1]->get_X())[s] - (Elem.noeud[0]->get_X())[s];
    
    //X3_X4[s] = X3[s] - X4[s];
    X3_X4[s] = (Elem.noeud[2]->get_X())[s] - (Elem.noeud[3]->get_X())[s];
    
    //X4_X2[s] = X4[s] - X2[s];
    X4_X2[s] = (Elem.noeud[3]->get_X())[s] - (Elem.noeud[1]->get_X())[s];

    //X1_X3[s] = X1[s] - X3[s];
    X1_X3[s] = (Elem.noeud[0]->get_X())[s] - (Elem.noeud[2]->get_X())[s];
  }

  //orienting the normal n1 to face (X2 X3 X4)
  get_normalToface(Elem.face[0], Elem.n1);

  beta_1 = cblas_ddot(3, X2_X1, 1, Elem.n1, 1);
  //cout<<"beta_1 = "<<beta_1<<"\n";
  if (beta_1 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n1[s] = -Elem.n1[s];
    }
  }
  //orienting the normal n2 to face (X1 X3 X4)
  get_normalToface(Elem.face[1], Elem.n2);

  beta_2 = cblas_ddot(3, X4_X2, 1, Elem.n2, 1);
  //cout<<"beta_2 = "<<beta_2<<"\n";
  if (beta_2 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n2[s] = -Elem.n2[s];
    }
  }
  //orienting the normal n3 to face (X1 X2 X4)
  get_normalToface(Elem.face[2], Elem.n3);

  beta_3 = cblas_ddot(3, X1_X3, 1, Elem.n3, 1);
  // cout<<"beta_3 = "<<beta_3<<"\n";
  if (beta_3 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n3[s] = -Elem.n3[s];
    }
  }
  //orienting the normal n4 to face (X1 X2 X3)
  get_normalToface(Elem.face[3], Elem.n4);

  beta_4 = cblas_ddot(3, X3_X4, 1, Elem.n4, 1);
  //cout<<"beta_4 = "<<beta_4<<"\n";
  if (beta_4 > 0){
    for( MKL_INT s = 0; s < 3; s++){
      Elem.n4[s] = -Elem.n4[s];
    }
  }
}



cplx eval_exp(double x){
  //Cette fonction donne l'approximation polynomiale de la fonction (exp(ix) - 1 - ix) / x 
  cplx res = cplx(-x / 2.0,0.0);
  cplx tn = cplx(1.0,0.0);
  if (abs(x) < 0.1) {
  for(MKL_INT n = 10; n >=3; n--){
    tn = cplx(1.0,0.0) + (cplx(0.0,x) / (double) n) * tn;
  }
  tn = res * tn;
  }
  else {
    tn = (exp(cplx(0.0,x)) - cplx(1.0,x)) / x;
  }
  return tn;
}



cplx integrale_triangle(Face &F, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction représente l'intégrale sur face du produit de deux ondes planes
  /*------------------------------------------------------------------ input -----------------------------------------------------------------------------

    F      : objet de type Face. Elle contient la face sur laquelle l'intégrale est effectuée
    P1, P2 : objets de type Onde_plane

    /*---------------------------------------------------------------- ouput ----------------------------------------------------------------------------

    I   : le résultat de l'intégrale 

    /*--------------------------------------------------------------------------------------------------------------------------------------------------*/
  cplx I;
  double  beta_1, beta_2, beta_3, X1dk, X2dk, X3dk;
  
  double *X1;
  double *X2;
  double *X3;
  
  X1 = F.noeud[0]->get_X();//neoud numero 1 de la face
  X2 = F.noeud[1]->get_X();//............ 2 ..........
  X3 = F.noeud[2]->get_X();//............ 3 ..........
  
  double *X3X1 = new double[3];// X3 - X1
  double *X2X3 = new double[3];// X2 - X3
  double *X1X2 = new double[3];// X1 - X2
  double *X1X3 = new double[3];// X1 - X3
  double *k = new double[3];

  MKL_INT i;

   for (i = 0; i < 3; i++){
     k[i] = P2.K[i] - P1.K[i];
   }
  
  double epsi = 1E-6;
  for (i = 0; i < 3; i++){
    
    X3X1[i] = X3[i] - X1[i];
    
    X1X3[i] = - X3X1[i];
    
    X2X3[i] = X2[i] - X3[i];
    
    X1X2[i] = X1[i] - X2[i];
    
  }
    
  beta_1 = cblas_ddot(3, X2X3, 1, k, 1);//X2X3 \cdot k
  
  beta_2 = cblas_ddot(3, X3X1, 1, k, 1);//X3X1 \cdot k
  
  beta_3 = cblas_ddot(3, X1X2, 1, k, 1);//X1X2 \cdot k
 

   if (abs(beta_1)/epsi < 1){
    if (abs(beta_2)/epsi < 1 ){
      //cas beta_1 = 0 et beta_2 = 0
      X3dk = cblas_ddot(3, X3, 1, k, 1);
     
      I =  0.5 * F.Aire * exp(cplx(0.0,X3dk));
      // cout<<"X3dk = "<<X3dk<<" I = "<<I<<"\n";
    }
    else{
      //cas beta_1 = 0 et beta_2 neq 0
      //X1dk = cblas_ddot(3, X1, 1, k, 1);

  
      X3dk = cblas_ddot(3, X3, 1, k, 1);
      
      I = (F.Aire * exp(cplx(0.0,X3dk)) / beta_2) * eval_exp(beta_3);
    }
  }
  else{
    if (abs(beta_2)/epsi < 1){
      //cas beta_1 neq 0 et beta_2 = 0
      X1dk = cblas_ddot(3, X1, 1, k, 1);

      //X2dk = cblas_ddot(3, X2, 1, k, 1);

      I =  (F.Aire * exp(cplx(0.0,X1dk)) / beta_3) * eval_exp(beta_1); 
    }
    else{
      if (abs(beta_3)/epsi < 1 ){
	//cas beta_1 neq 0 et beta_2 neq 0 et beta_3 = 0
	X2dk = cblas_ddot(3, X2, 1, k, 1);
	
	//X3dk = cblas_ddot(3, X3, 1, k, 1);
	
	I = (F.Aire * exp(cplx(0.0,X2dk)) / beta_1) * eval_exp(beta_2); 
      }
      else{
	//cas beta_1 neq 0 et beta_2 neq 0 et beta_3 neq 0
	X1dk = cblas_ddot(3, X1, 1, k, 1);
	
	//X2dk = cblas_ddot(3, X2, 1, k, 1);
	
	//X3dk = cblas_ddot(3, X3, 1, k, 1);
	
	I = (F.Aire * exp(cplx(0.0,X1dk)) / beta_1) * (eval_exp(beta_2) - eval_exp(-beta_3));
      }
      
    }
  }
   
   //libération de la mémoire
   delete [] X1X2; X1X2 = 0;
   delete [] X1X3; X1X3 = 0;
   delete [] X3X1; X3X1 = 0;
   delete [] X2X3; X2X3 = 0;
   delete [] k; k = 0;

   
   return I;
}



cplx dot_c(cplx *u, double *v) {
  cplx r = conj(u[0]) * v[0] + conj(u[1]) * v[1] + conj(u[2]) * v[2];
  return r;
}


cplx dot_c(cplx *u, cplx *v) {
  cplx r;
  //cblas_zdotc_sub(3, u, 1, v, 1, &r) ;
  r = conj(u[0]) * v[0] + conj(u[1]) * v[1] + conj(u[2]) * v[2];
  return r;
}


cplx dot_u(cplx *u, double *v) {
  cplx r = u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
  return r;
}



cplx dot_u(cplx *u, cplx *v) {
  cplx r;
  cblas_zdotu_sub(3, u, 1, v, 1, &r) ;
  return r;
}




// /************************************************************ | Intégrales pour X * conj(X') |*********************************************************/

// /************************************************************* integrales traduisant l'interaction d'un élément avec lui meme ************************************************************************/
cplx integrale_triangle_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
  //Cette fonction calcule la somme des intégrales sur les faces du produit de deux ondes planes associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de la fonction-test
    j      : le numero de la solution
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                              
  for (l = 0; l < 4; l++) {//boucle sur les faces                      
    I += Mesh.elements[e].alphaPP[l] * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
  
  return I;
}


cplx integrale_triangle_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une onde plane et d'une vitesse d'onde plane associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse
    j      : le numero de la solution
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker dj \cdot n_i avec n_i la normale à la face i               
                                              
  for (l = 0; l < 4; l++) {//boucle sur les faces de l'élément                       
    
    if (l == 0) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
  
    }
    else if (l == 1) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
    
    }
    else if (l == 2) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
     
    }
    else if (l == 3) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
   
    }
    
    I += Mesh.elements[e].alphaPV[l] * dn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
 
  return I;
}



cplx integrale_triangle_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une onde plane et d'une vitesse d'onde plane associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de la fonction-test onde plane
    j      : numero de l'onde associée à la solution vitesse
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker dj \cdot n_i avec n_i la normale à la face i
                                          
  for (l = 0; l < 4; l++) {//boucle sur les faces
    
    if (l == 0) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;
      dn = dn * Mesh.elements[e].OP[j].ksurOmega;
      
    }
    else if (l == 1) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
      dn = dn * Mesh.elements[e].OP[j].ksurOmega;
     
    }
    else if (l == 2) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
      dn = dn * Mesh.elements[e].OP[j].ksurOmega;
     
    }
    else if (l == 3) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
      dn = dn * Mesh.elements[e].OP[j].ksurOmega;
      
    }
    
    I += Mesh.elements[e].alphaVP[l] * dn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
 
  return I;
}



cplx integrale_triangle_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit de deux vitesses d'ondes planes associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse
    j      : numero de l'onde associée à la solution vitesse 
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double din;//pour stocker di \cdot n_l avec n_l la normale à la face l
  double djn;//pour stocker di \cdot n_l avec n_l la normale à la face l
  //di la direction de l'onde i et dj celle de l'onde j
  
  for (l = 0; l < 4; l++) {
    
    if (l == 0) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;

      djn = djn * Mesh.elements[e].OP[j].ksurOmega;

      din = din * Mesh.elements[e].OP[i].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
      
      djn = djn * Mesh.elements[e].OP[j].ksurOmega;

      din = din * Mesh.elements[e].OP[i].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
      
      djn = djn * Mesh.elements[e].OP[j].ksurOmega;

      din = din * Mesh.elements[e].OP[i].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
      
      djn = djn * Mesh.elements[e].OP[j].ksurOmega;

      din = din * Mesh.elements[e].OP[i].ksurOmega;
      
    }
    
    I += Mesh.elements[e].alphaVV[l] * din * djn * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
 
  return I;
}


/****************************************************** | integrales traduisant l'interaction d'un élément avec son voisin | *****************************************************************/ 


cplx integrale_triangle_PP_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une onde plane associée à un élément avec une autre onde plane associée au voisin de l'élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de la fonction-test ou de l'onde plane associée au voisin de l'élément
    j      : le numero de la solution mais aussi de l'onde associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);

  MKL_INT l;
  if (Mesh.elements[e].voisin[c] >= 0) {//test si le voisin c de l'élément existe ou pas
   
    //tester pour obtenir les faces voisines des éléments
    I = Mesh.elements[e].alphaPP[c] * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
 
  return I;
}




cplx integrale_triangle_PV_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une onde plane associée à un élément avec une vitesse d'onde plane associée au voisin de l'élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse. C'est l'onde plane du voisin
    j      : le numero de la solution mais aussi de l'onde associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);

  MKL_INT l;
  if (Mesh.elements[e].voisin[c] >= 0) {//teste l'existance du voisin de l'élément (< 0 si pas de voisin)
    MKL_INT lda = 3, inc = 1;
  
    double dn;//pour stocker dj \cdot n_i avec n_i la normale à la face i
   

    if (c == 0) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 1) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 2) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
      //cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 3) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
      dn = dn  * Mesh.elements[e].OP[i].ksurOmega;
      //cout<<l<<" dn:"<<dn<<endl;
    }
    
    I = Mesh.elements[e].alphaPV[c] * dn * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
  

  return I;
}



cplx integrale_triangle_VP_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une vitesse d'onde plane associée à un élément avec une onde plane associée au voisin de l'élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de la fonction-test ou de l'onde plane associée au voisin de l'élément
    j      : numero de l'onde associée à la solution vitesse. C'est l'onde plane de l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
  if (Mesh.elements[e].voisin[c] >= 0) {//teste l'existance du voisin
    MKL_INT lda = 3, inc = 1;
  
    double dn;//pour stocker dj \cdot n_i avec n_i la normale à la face i

    if (c == 0) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;
      dn = dn  * Mesh.elements[e].OP[j].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 1) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
      dn = dn  * Mesh.elements[e].OP[j].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 2) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
      dn = dn  * Mesh.elements[e].OP[j].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }
    else if (c == 3) {
      dn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
      dn = dn  * Mesh.elements[e].OP[j].ksurOmega;
      // cout<<l<<" dn:"<<dn<<endl;
    }

    I = Mesh.elements[e].alphaVP[c] * dn * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
  }
 
  return I;
}




cplx integrale_triangle_VV_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces du produit d'une vitesse d'onde plane associée à un élément avec une autre vitesse d'onde plane associée au voisin de l'élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse. C'est l'onde plane du voisin
    j      : numero de l'onde associée à la solution vitesse. C'est l'onde plane de l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  if (Mesh.elements[e].voisin[c] >= 0) {//teste l'existance du voisin de l'élément
    MKL_INT lda = 3, inc = 1;
  
    double din, djn;

    if (c == 0) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;

      din = din  * Mesh.elements[e].OP[i].ksurOmega;

      djn = djn  * Mesh.elements[e].OP[j].ksurOmega;
      
    }
    else if (c == 1) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;

      din = din  * Mesh.elements[e].OP[i].ksurOmega;

      djn = djn  * Mesh.elements[e].OP[j].ksurOmega;
      
    }
    else if (c == 2) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;

       din = din  * Mesh.elements[e].OP[i].ksurOmega;

      djn = djn  * Mesh.elements[e].OP[j].ksurOmega;
      
    }
    else if (c == 3) {
      din = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
      
      djn = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;

      din = din  * Mesh.elements[e].OP[i].ksurOmega;

      djn = djn  * Mesh.elements[e].OP[j].ksurOmega;
      
    }
      
    I = Mesh.elements[e].alphaVV[c] * din * djn * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j]);
  }
  
  return I;
}
// /***************************************************************************************************************************************************/


void prodMatMatComplex(MKL_INT m_a, MKL_INT n_a, MKL_INT m_b, MKL_INT n_b, cplx *a, cplx *b, cplx *c) {
  MKL_INT i, j, k;
  if (n_a == m_b){
    for (i = 0; i < m_a; i++) { 
      for (j = 0; j < n_b; j++) {
	c[i * n_b + j] = cplx(0.0,0.0);
	for (k = 0; k < n_a; k++) {
	  c[i * n_b + j] += a[i * n_a + k] * b[k * n_b + j];
	}
      }
    }
  }
  else{
    cout<<"------------------------------------------------------------------- probleme de taille ----------------------------------------------------------"<<endl; 
  }
}


void prodMatMatComplexNoInit(MKL_INT m_a, MKL_INT n_a, MKL_INT m_b, MKL_INT n_b, cplx *a, cplx *b, cplx *c) {
  MKL_INT i, j, k;
  if (n_a == m_b){
    for (i = 0; i < m_a; i++) { 
      for (j = 0; j < n_b; j++) {
	for (k = 0; k < n_a; k++) {
	  c[i * n_b + j] += a[i * n_a + k] * b[k * n_b + j];
	}
      }
    }
  }
  else{
    cout<<"------------------------------------------------------------------- probleme de taille ----------------------------------------------------------"<<endl; 
  }
}



void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A_skyline &P_red,  Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC;
  
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    JLOC = 0;
    for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
      
      a[iloc * Mesh.elements[e].Nred + JLOC] = PP_red[iloc * Mesh.ndof + jloc];
      
      b[JLOC * Mesh.ndof + iloc] = conj(PP_red[iloc * Mesh.ndof + jloc]);
      JLOC++;
    }      
  }
}


void store_P_and_transpose_P(MKL_INT e, cplx *a, A_skyline &P_red,  const string & arg, Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra soit la restriction de 
                P_red , soit la restriction de l'adjoint de P_red sur l'élément e
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    arg       : chaîne de caractères, elle détermine l'opération à faire selon :
                arg == "selfP", stocke la restriction de P_red à l'élément e dans a
		arg == "adjtP", ....................... l'adjoint de P_red à l'élément e dans b
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a    :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC;
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  if (arg == "selfP"){
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
	a[iloc * Mesh.elements[e].Nred + JLOC] = PP_red[iloc * Mesh.ndof + jloc];
	
	JLOC++;
      }      
    }
  }
  
  else  if (arg == "adjtP"){
    
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
	a[JLOC * Mesh.ndof + iloc] = conj(PP_red[iloc * Mesh.ndof + jloc]);
	
	JLOC++;
      }      
    }
  }
  
  else {
    cout<<"probleme de mot clef"<<endl;
  }
  
}

/*----------------------------------------------------------------------- P_red and (P_red)^* -----------------------------------------------------*/

void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, cplx *b, A_skyline &P_red, Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    j         : le numero local du voisin de e
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur le voisin de l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PPP_red    : contient la restriction de P_red au voisin de e mais pas en taille réelle
    PP_red     : ....................... de l'adjoint de P_red sur e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC, f;

  f = Mesh.elements[e].voisin[j];
  
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *PPP_red = P_red.Tab_A_loc[f].get_Aloc();
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    JLOC = 0;
    for (jloc = Mesh.elements[f].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
      a[iloc * Mesh.elements[f].Nred + JLOC] = PPP_red[iloc * Mesh.ndof + jloc];
      JLOC++;
    }      
  }

  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    JLOC = 0;
    for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	      
      b[JLOC * Mesh.ndof + iloc] = conj(PP_red[iloc * Mesh.ndof + jloc]);
      JLOC++;
    }      
  }
  
}				  



void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, A_skyline &P_red, const string & arg, Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    j         : le numero local de voisin de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra soit la restriction de 
                 P_red au voisin de e, soit la restriction de l'adjoint de P_red sur e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    arg       : chaîne de caractères, elle détermine l'opération à faire selon :
                arg == "Pj", stocke la restriction de P_red au voisin de e sur a
		arg == "Pi", ....................... l'adjoint de P_red à l'élément e sur a
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a    :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PPP_red    : contient la restriction de P_red au voisin de e mais pas en taille réelle
    PP_red     : ....................... de l'adjoint de P_red sur e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC, f;

  f = Mesh.elements[e].voisin[j];

  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *PPP_red = P_red.Tab_A_loc[f].get_Aloc();
  
  if (arg == "pj"){ 
    
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[f].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
	a[iloc * Mesh.elements[f].Nred + JLOC] = PPP_red[iloc * Mesh.ndof + jloc];
	JLOC++;
      }      
    }
  }
  
  else if (arg == "pi"){

    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++) { // numero de la colonne	      
	
	a[JLOC * Mesh.ndof + iloc] = conj(PP_red[iloc * Mesh.ndof + jloc]);
	JLOC++;
      }      
    }
  }

  else {
    cout<<"probleme de mot clef"<<endl;
  }
}



void prodMatVecComplex(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c) {
  MKL_INT i, j;
  if (n_a == n_b){
    for (i = 0; i < m_a; i++) {
      c[i] = cplx(0.0,0.0);
      for (j = 0; j < n_b; j++) {
	c[i] += a[i * n_a + j] * b[j];
      }
    }
  }
  else{
    cout<<"------------------------------------------------------------------- probleme de taille ----------------------------------------------------------"<<endl; 
  }
}


void prodMatVecComplexWithoutInit(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c) {
  MKL_INT i, j;
  if (n_a == n_b){
    for (i = 0; i < m_a; i++) {
  
      for (j = 0; j < n_b; j++) {
	c[i] += a[i * n_a + j] * b[j];
      }
    }
  }
  else{
    cout<<"------------------------------------------------------------------- probleme de taille ----------------------------------------------------------"<<endl; 
  }
}



void compute_residul(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c) {
  MKL_INT i, j;
  if (n_a == n_b){
    for (i = 0; i < m_a; i++) {
   
      for (j = 0; j < n_b; j++) {
	c[i] -= a[i * n_a + j] * b[j];
      }
    }
  }
  else{
    cout<<"------------------------------------------------------------------- probleme de taille ----------------------------------------------------------"<<endl; 
  }
}




void compute_sizeBlocRed(MKL_INT e, MKL_INT l, cplx *AA, double *w, A_skyline &A_glob, Maillage &Mesh) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* (avec P une matrice orthonormale et D contient les valeurs propres) et calcule la taille de la nouvelle base réduite de Galerkin associée à l'élément e
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Nred : la taille de la nouvelle base réduite de Galerkin
    Nres : ndof - Nred
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;

  Mesh.elements[e].Nred = 0;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      AA[iloc * Mesh.ndof + jloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[iloc * Mesh.ndof + jloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_ROW_MAJOR , 'V', 'L', Mesh.ndof, AA, Mesh.ndof, w);


  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    if (w[iloc] > Mesh.elements[e].eps_max){
      Mesh.elements[e].Nred++;
    }
  }

  Mesh.elements[e].Nres = Mesh.ndof - Mesh.elements[e].Nred;
  
}



MKL_INT compute_sizeBlocRed(A_skyline &A_glob, Maillage &Mesh) {
  //Cette fonction calcule la taille réduite de la grande matrice de passage P_red correspondante à la nouvelle base de Galerkin discrète
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille = 0;
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  double *w = new double[Mesh.ndof];//pour stocker les valeurs propres; w = D
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
    compute_sizeBlocRed(e, 0, AA, w, A_glob, Mesh);
    MKL_INT r = A_glob.VerifyDecompositionPMP(e, 0, AA, w, Mesh);
    
    if (r == 1) {
      Mesh.SIZE[e] = taille;
      taille += Mesh.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
  }
  
  delete [] AA; AA = 0;
  delete [] w; w = 0;
  
  return taille;
}

MKL_INT compute_sizeForA_red(Maillage &Mesh){
  //Cette fonction calcule la taille réduite de la grande matrice A_red  = P_red^* A P_red
  //Faire d'abord appel à la fonction compute_sizeBlocRed(A_skyline &A_glob, Maillage &Mesh) pour définir la taille de chaque bloc P_red
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de A_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille = 0;
  
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
   
    taille += Mesh.elements[e].Nred * Mesh.elements[e].Nred;
  }

  MKL_INT f;
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
    for (MKL_INT j = 0; j < 4; j++){
      if (Mesh.elements[e].voisin[j] >= 0){
	
      f = Mesh.elements[e].voisin[j];
      taille += Mesh.elements[e].Nred * Mesh.elements[f].Nred;
      }
      else{
	
	taille += Mesh.elements[e].Nred * Mesh.elements[e].Nred;
      }
    }
  }
  
  return taille;
}


void DecompositionDeBloc(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, Maillage &Mesh) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    P_red  : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    P         : ........... P_red ...................
    V         : ........... LAMBDA ..................
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC;

  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  cplx *P = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *V;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      AA[iloc * Mesh.ndof + jloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[iloc * Mesh.ndof + jloc];
    }
  }
  
  
  double *w = new double[Mesh.ndof];//pour stocker les valeurs propres; w = D
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_ROW_MAJOR , 'V', 'L', Mesh.ndof, AA, Mesh.ndof, w);
  
  //stockage de la base réduite associée à l'élément e
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = Mesh.elements[e].Nres; jloc < Mesh.ndof; jloc++){
      P[iloc * Mesh.ndof + jloc] = AA[iloc * Mesh.ndof + jloc];
    }
  }
  
  //clônage du tableau LAMBDA sur l'élément e
  get_F_elemRed(e, &V, LAMBDA, Mesh);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc =  Mesh.elements[e].Nres; iloc < Mesh.ndof; iloc++){
      V[ILOC] = w[iloc];
      ILOC++;
  }
  
 
  delete [] w; w = 0;
  delete [] AA; AA = 0;
}


void DecompositionGlobaleDesBlocs(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, Maillage &Mesh){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P_red : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
    DecompositionDeBloc(e, 0, A_glob, P_red, LAMBDA, Mesh);
  }
}



void min_and_max_eigenvalues(VecN &LAMBDA, double *Wmin, double *Wmax, Maillage &Mesh) {
  cplx *V;

  for (MKL_INT iloc = 0; iloc < Mesh.elements[0].Nred; iloc++){
    Wmin[iloc] = 1000.0;
    Wmax[iloc] = 0.0;
  }

  for (MKL_INT iloc = 0; iloc < Mesh.elements[0].Nred; iloc++){
    for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
      LAMBDA.get_F_elem(Mesh.elements[e].Nred, e, &V);
   
      Wmin[iloc] = min(Wmin[iloc], real(V[iloc]));
      Wmax[iloc] = max(Wmax[iloc], real(V[iloc]));
    }
  }

}


void min_and_max_eigenvalues_ESP(VecN &LAMBDA, Maillage &Mesh) {
  cplx *V;

  double  Wmin = 1000.0;
  double Wmax = 0.0;

  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
    get_F_elemRed(e, &V, LAMBDA, Mesh);
    
      Wmin = min(Wmin, real(V[0]));
      Wmax = max(Wmax, real(V[Mesh.elements[e].Nred-1]));
  }

  cout<<"eps_0 = "<<Mesh.eps_0<<" min = "<<Wmin<<" max = "<<Wmax<<endl;
}





MKL_INT CheckMredAndLAMBDA(MKL_INT e, MKL_INT l, A_skylineRed &A_red, VecN &LAMBDA, Maillage &Mesh) {
  
  MKL_INT r, s, iloc, jloc, IGLOB;
  
  cplx *A_loc = A_red.Tab_A_locRed[e * A_red.get_n_inter() + l].get_Aloc();

  s = 0; r = 0;
  
  for (iloc = 0; iloc < Mesh.elements[e].Nred; iloc++) { 
    for (jloc = 0; jloc < Mesh.elements[e].Nred; jloc++) {
      if (iloc == jloc){
	IGLOB = local_to_globRed(iloc, e, Mesh);
	if (abs(A_loc[iloc * Mesh.elements[e].Nred + jloc] - LAMBDA.value[IGLOB]) < 1E-10){
	  s++;
	}
      }
    }
  }

  if (s == Mesh.elements[e].Nred){ r = 1;}
  
  return r;
}


MKL_INT CheckMredAndLAMBDA(A_skylineRed &A_red, VecN &LAMBDA, Maillage &Mesh) {

  MKL_INT e, r, s;
  s = 0; r = 0;
  for (e = 0; e < Mesh.get_N_elements(); e++){
    s += CheckMredAndLAMBDA(e, 0, A_red, LAMBDA, Mesh);
  }
  if (s == Mesh.get_N_elements()) { r = 1;}
  
  return r;
}


// // void positivite_PP(Maillage &Mesh, MKL_INT e) {
// //   MKL_INT iloc, jloc;
// //   cplx *AA = (cplx *)malloc( Mesh.ndof * Mesh.ndof * sizeof(cplx));
// //   for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
// //     for (jloc = 0; jloc < Mesh.ndof; jloc++) {
// //       AA[iloc * Mesh.ndof + jloc] =  integrale_triangle_PP(Mesh, e, iloc, jloc);
// //     }
// //   }
// //   double maxx = 0;
// //   double minn = 1000;
// //   double *w = (double *)malloc(Mesh.ndof * sizeof(double));
// //   MKL_INT lwork = (2 * Mesh.ndof - 1), info, lda = Mesh.ndof;
// //   cplx *work = (cplx *)malloc( (2 * Mesh.ndof - 1) * sizeof(cplx));
// //   double *rwork = (double *)malloc((3 * Mesh.ndof - 1)*sizeof(double));
// //   // MKL_INT tt = LAPACKE_zheev( 101, CblasNoTrans, 'L', data.nbre_ondes, BB, data.nbre_ondes, w);
// //   zheev("N", "U", &lda, AA, &lda, w, work, &lwork, rwork, &info );
// //   maxx = max(maxx,w[Mesh.ndof - 1]);
// //   minn = min(minn,w[0]);
// //   cout<<"max = "<<maxx<<" min = "<<minn<<endl;
// //   free(w); w = NULL;
// //   free(AA); AA = NULL;
// // }



// // void positivite_VV(Maillage &Mesh, MKL_INT e) {
// //   MKL_INT iloc, jloc;
// //   cplx *AA = (cplx *)malloc( Mesh.ndof * Mesh.ndof * sizeof(cplx));
// //   for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
// //     for (jloc = 0; jloc < Mesh.ndof; jloc++) {
// //       AA[iloc * Mesh.ndof + jloc] =  integrale_triangle_VV(Mesh, e, iloc, jloc);
// //     }
// //   }
// //   double maxx = 0;
// //   double minn = 1000;
// //   double *w = (double *)malloc(Mesh.ndof * sizeof(double));
// //   MKL_INT lwork = (2 * Mesh.ndof - 1), info, lda = Mesh.ndof;
// //   cplx *work = (cplx *)malloc( (2 * Mesh.ndof - 1) * sizeof(cplx));
// //   double *rwork = (double *)malloc((3 * Mesh.ndof - 1)*sizeof(double));
// //   // MKL_INT tt = LAPACKE_zheev( 101, CblasNoTrans, 'L', data.nbre_ondes, BB, data.nbre_ondes, w);
// //   zheev("N", "U", &lda, AA, &lda, w, work, &lwork, rwork, &info );
// //   maxx = max(maxx,w[Mesh.ndof - 1]);
// //   minn = min(minn,w[0]);
// //   cout<<"max = "<<maxx<<" min = "<<minn<<endl;
// //   free(w); w = NULL;
// //   free(AA); AA = NULL;
// // }

// /************************************************************ | Intégrales pour Fx * conj(Uy') |*********************************************************/

/************************************************************ integrales traduisant l'interaction d'un élément avec lui meme (C'est des intégrales sur les faces intérieures) ************************************************************/

cplx integrale_triangle_FxUy_interieure_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit de deux ondes planes associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de la fonction-test
    j      : le numero de la solution
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;

  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] >=0){
   
      I += Mesh.elements[e].TL_PP[l] * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }

  return I;
}



cplx integrale_triangle_FxUy_interieure_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une onde plane et d'une vitesse d'onde plane. Les deux ondes planes sont associées au meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse
    j      : le numero de la solution 
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc;
  lda = 3, inc = 1;
 
  double dnT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1
  
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] >=0){
    
      if(l == 0){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 1){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 2){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 3){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      //cout<<l<<" dnT: "<<dnT<< " PV(0) = "<<Elem.face[l].TL_PV[0]<<endl;
      
      I += Mesh.elements[e].TL_PV[l] * dnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
  
  return I;
}



cplx integrale_triangle_FxUy_interieure_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une onde plane et d'une vitesse d'onde plane. Les deux ondes planes sont associées au meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de la fonction-test 
    j      : numero de l'onde associée à la solution vitesse
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l, lda, inc;
  lda = 3, inc = 1;

  double dnT;//dnT = d_j \cdot nT avec d_j la direction de l'onde j et n_T la normale au voisin 1
 
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] >=0){
     
      if(l == 0){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 1){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 2){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 3){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
	//	cout<<l<<" dnT: "<<dnT<<endl;
      
      I += Mesh.elements[e].TL_VP[l] * dnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
  
  return I;
}


cplx integrale_triangle_FxUy_interieure_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit de deux vitesses d'ondes planes. Les deux ondes planes sont associées au meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage 
    e      : le numero de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse
    j      : numero de l'onde associée à la solution vitesse
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc;
  lda = 3, inc = 1;
  
  double dinT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1

  double djnT;//dnT = d_j \cdot nT avec d_j la direction de l'onde j et n_T la normale au voisin 1
 
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] >=0){
     
      if(l == 0){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;

	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 1){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;
	
	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 2){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;
	
	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      if(l == 3){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;
	
	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
	
      I += Mesh.elements[e].TL_VV[l] * dinT * djnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
  
  return I;
}
// /*************************************************************** | integrales traduisant l'interaction d'un élément avec son voisin (C'est des intégrales sur les faces intérieures) | *********************************************/


cplx integrale_triangle_FxUy_interieure_PP_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une onde plane associée à un élément avec une autre onde plane associée au voisin de l'élément
    /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage  
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de la fonction-test ou de l'onde plane associée au voisin de l'élément
    j      : le numero de la solution mais aussi de l'onde associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, h;
  if (Mesh.elements[e].voisin[c] >=0) {//teste l'existance du voisin (< 0 si pas de voisin)
    h = Mesh.elements[e].voisin[c];
   
    I = Mesh.elements[e].LT_PP[c] * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[h].OP[j], chaine);
  }

  return I;
}



cplx integrale_triangle_FxUy_interieure_PV_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une onde plane associée à un élément avec une vitesse d'onde plane associée au voisin de l'élément
    /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage  
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse. L' onde plane est associée au voisin 
    j      : le numero de la solution ou de l'onde associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc, h;
  lda = 3, inc = 1;
  if (Mesh.elements[e].voisin[c] >= 0){//teste l'existance du voisin
    h = Mesh.elements[e].voisin[c];
   
    double dnT;//dnT = d_i \cdot nL ................................. et n_L .................... 2
    if(c == 0){
      dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;

      dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
    }
    else if(c == 1){
      dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;

      dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
    }
    else if(c == 2){
      dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;

      dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
    }
    else if(c == 3){
      dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;

      dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
    }
    //cout<<e<<" dnT: "<<dnT<< " PV(1) = "<<Mesh.elements[e].LT_PV[c]<<endl;
      
    I = Mesh.elements[e].LT_PV[c] * dnT * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[h].OP[j], chaine);
  }
  
  return I;
}



cplx integrale_triangle_FxUy_interieure_VP_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une vitesse d'onde plane associée à un élément avec une onde plane associée au voisin de l'élément
    /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage  
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de la fonction-test ou de l'onde plane associée au voisin de l'élément
    j      : numero de l'onde associée à la solution vitesse. L'onde plane est associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l, lda, inc, h;
  lda = 3, inc = 1;
  if (Mesh.elements[e].voisin[c] >= 0) {
    h = Mesh.elements[e].voisin[c];
    
    double dnL;//dnL = d_j \cdot nL ................................. et n_L .................... 2
    if (c == 0){
    dnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;
    dnL = dnL * Mesh.elements[h].OP[j].ksurOmega;
    }
    else if (c == 1){
    dnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
    dnL = dnL * Mesh.elements[h].OP[j].ksurOmega;
    }
    else if (c == 2){
    dnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
    dnL = dnL * Mesh.elements[h].OP[j].ksurOmega;
    }
    else if (c == 3){
    dnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
    dnL = dnL * Mesh.elements[h].OP[j].ksurOmega;
    }
    //cout<<e<<" dnL: "<<dnL<<" VP(1) = "<<Elem.face[e].TL_VP[1]<<endl;
    
    I = - Mesh.elements[e].LT_VP[c] * dnL * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[h].OP[j], chaine);
  }
  
  return I;
}



cplx integrale_triangle_FxUy_interieure_VV_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]) {
  //Cette fonction calcule la somme des intégrales sur les faces intérieures du produit d'une vitesse d'onde plane associée à un élément avec une autre vitesse d'onde plane associée au voisin de l'élément
    /*------------------------------------------------------- input ---------------------------------------------------
    Mesh   : le maillage  
    e      : le numero de l'élément
    c      : le numero local du voisin de l'élément
    i      : numero de l'onde associée à la fonction-test vitesse.L'onde plane est associée au voisin 
    j      :  numero de l'onde associée à la solution vitesse. L'onde plane est associée à l'élément
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc, h;
  lda = 3, inc = 1;
  if (Mesh.elements[e].voisin[c] >= 0) {//teste l'existance du voisin de l'élément
    h = Mesh.elements[e].voisin[c];
   
    double dinT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1
    double djnL;//dnT = d_j \cdot nL ................................. et n_L .................... 2
    if (c == 0){
    dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
    
    djnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;

    dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

    djnL = djnL * Mesh.elements[h].OP[j].ksurOmega;
    
    }
    else if (c == 1){
    dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
    
    djnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;

    dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

    djnL = djnL * Mesh.elements[h].OP[j].ksurOmega;    
    
    }
    else if (c == 2){
    dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
    
    djnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;

    dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

    djnL = djnL * Mesh.elements[h].OP[j].ksurOmega;
    
    }
    else if (c == 3){
    dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
    
    djnL = cblas_ddot(lda, Mesh.elements[h].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;

    dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

    djnL = djnL * Mesh.elements[h].OP[j].ksurOmega;
    
    }
    
    I = - Mesh.elements[e].LT_VV[c] * dinT * djnL * integrale_triangle(Mesh.elements[e].face[c],   Mesh.elements[e].OP[i], Mesh.elements[h].OP[j], chaine);
  }
 
  return I;
}


// /*************************************************************** | integrales traduisant l'interaction d'un élément avec son voisin (C'est des intégrales sur les faces extérieures) | *********************************************/


cplx integrale_triangle_FxUy_boundary_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces extérieures du produit de deux ondes planes associées à un meme élément
   /*------------------------------------------------------- input ---------------------------------------------
    Mesh  : structure maillage
    e     : numero de l'élément
    i     : numero de l'onde pour la fonction-test
    j     : ......................... solution
    /*------------------------------------------------------ ouput --------------------------------------------
    I     : le résultat
    /*-----------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
 
  
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      I += Mesh.elements[e].betaPP[l] * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
 
  return I;
}


cplx integrale_triangle_FxUy_boundary_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces extérieures du produit d'une onde plane et d'une vitesse d'onde plane associée à un élément
   /*-----------------------------------------------------------------------------------------
    Mesh  : structure maillage
    e     : numero de l'élément
    i     : numero de l'onde associée à la fonction-test vitesse
    j     : .............................. solution
    -----------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc;
  lda = 3, inc = 1;
  
  double dnT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1
  
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      if(l == 0){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 1){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 2){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
      else if(l == 3){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dnT = dnT * Mesh.elements[e].OP[i].ksurOmega;
      }
   
      I += Mesh.elements[e].betaPV[l] * dnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
 
  return I;
}



cplx integrale_triangle_FxUy_boundary_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
   //Cette fonction calcule la somme des intégrales sur les faces extérieures du produit d'une vitesse d'onde plane et d'une autre onde plane associées à un meme élément
   /*-----------------------------------------------------------------------------------------
    Mesh  : structure maillage
    e     : numero de l'élément
    i     : numero de l'onde pour la fonction-test
    j     : ............... associée à la solution vitesse
    -----------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l, lda, inc;
  lda = 3, inc = 1;
  
  double dnT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1
 
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      if(l == 0){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 1){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 2){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 3){
	dnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;
	dnT = dnT * Mesh.elements[e].OP[j].ksurOmega;
      }
    
      I += Mesh.elements[e].betaVP[l] * dnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
  
  return I;
}


cplx integrale_triangle_FxUy_boundary_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]) {
    //Cette fonction calcule la somme des intégrales sur les faces extérieures du produit de deux vitesses d'ondes planes associées à un meme élément
   /*-----------------------------------------------------------------------------------------
    Mesh  : structure maillage
    e     : numero de l'élément
    i     : numero de l'onde pour la fonction-test vitesse
    j     : ............... associée à la solution vitesse
    -----------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);

  MKL_INT l, lda, inc;
  lda = 3, inc = 1;
  
   double dinT;//dnT = d_i \cdot nT avec d_i la direction de l'onde i et n_T la normale au voisin 1

  double djnT;//dnT = d_j \cdot nT avec d_j la direction de l'onde j et n_T la normale au voisin 1
 
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      if(l == 0){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n1, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n1, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 1){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n2, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n2, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      else if(l == 2){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n3, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n3, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }
      if(l == 3){
	dinT = cblas_ddot(lda, Mesh.elements[e].OP[i].Ad, inc, Mesh.elements[e].n4, inc) ;
	
	djnT = cblas_ddot(lda, Mesh.elements[e].OP[j].Ad, inc, Mesh.elements[e].n4, inc) ;

	dinT = dinT * Mesh.elements[e].OP[i].ksurOmega;

	djnT = djnT * Mesh.elements[e].OP[j].ksurOmega;
      }

      I += Mesh.elements[e].betaVV[l] * dinT * djnT * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[i], Mesh.elements[e].OP[j], chaine);
    }
  }
  
  return I;
}



// double Aire_face(Face &F) {
//   double Jac;
  
//   double *X1;
//   double *X2;
//   double *X3;
  
//   X1 = F.noeud[0]->get_X();
//   X2 = F.noeud[1]->get_X();
//   X3 = F.noeud[2]->get_X();
//   double *X2X3 = (double *)malloc(3 * sizeof(double));
//   double *X1X3 = (double *)malloc(3 * sizeof(double));
//   // double *prod_Vecto = (double *)malloc(3 * sizeof(double));

  
//   for (MKL_INT i = 0; i < 3; i++){
    
//     X1X3[i] = X1[i] - X3[i];
    
//     X2X3[i] = X2[i] - X3[i];
//   }

//   //computing the volume
//   double *prod_Vecto = produit_vectoriel(X1X3, X2X3);

//   Jac = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
//   Jac = 0.5 * sqrt(Jac);
//   free(X2X3); X2X3 = 0;
//   free(X1X3); X1X3 = 0;
//   free(prod_Vecto); prod_Vecto = 0;
//   return Jac;
// }

// /*
// double Somme_Aire_Faces(Element &Elem) {
//   double S = 0.0;
//   for (MKL_INT l = 0; l < 4; l++) {
//     S += Aire_face(Elem.face[l]);
//   }
//   return S;
// }








void Maillage::quad_Gauss_1D() {
  //Cette fonction calcule les points de Gauss-Legendre 1D
  /*---------------------------------------------------------------------------------------------------------
    A et D des matrices qui contiennent les polynômes de Gauss-Legendre
    X1d : les points de Gauss-Legendre
    W1d : les poids asscoiés
    /*---------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *D = new double[n_gL * n_gL];
  double *A = new double[n_gL * n_gL];
  //double *F = (double *)malloc(n * sizeof(double));
  for (i = 0; i < n_gL; i++){
    if (i % 2 == 0) {
      W1d[i] = 2.0 / (double) (i+1);
    }
    else{
      W1d[i] = 0.0;
    }
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = 0.0;
      D[i * n_gL + j] = 0.0;
    }
  }

  for (i = 0; i < n_gL-1; i++){
    A[i*n_gL + (i+1)] = i+1;
    A[(i+1)*n_gL + i] = i+1;
    D[i * n_gL + i] = 2 * (i+1) - 1;
  }
  D[(n_gL-1) * n_gL + (n_gL-1)] = 2 * n_gL - 1;

  double *alphar = new double[ n_gL];
  double *alphai = new double[ n_gL];
  double *beta = new double[ n_gL];

  double *work = new double[ n_gL];
  MKL_INT lwork = 8 * n_gL ;
  MKL_INT info, ldr, ldl;
  ldl = n_gL;
  ldr = n_gL;

  double *vsl = new double[ldl * n_gL];
  double *vsr = new double[ldr * n_gL];

  //Calcul des valeurs propres de (A,D)
  info = LAPACKE_dggev(CblasRowMajor, 'V', 'V', n_gL, A, n_gL, D, n_gL, alphar, alphai, beta, vsl, n_gL, vsr, n_gL);

  cout<<"info = "<<info<<endl;

  //calcul des points de Gauss
  cout<<"eigen(A,D) : "<<endl;
  for (i = 0; i < n_gL; i++){
    X1d[i] = alphar[i] / beta[i];
  }


  for (i = 0; i < n_gL; i++){
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = pow(X1d[j],i);
    }
  }


   //calcul des poids de Gauss
   MKL_INT *ipiv = new MKL_INT[n_gL];
   info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n_gL, n_gL, A, n_gL, ipiv);

   MKL_INT nrhs = 1;
   if (info != 0) {
     cout << "une erreur est renvoyée : "<< info << endl << endl;
   }
   else{
     cout << "résolution du système..."<< endl << endl;
     info = LAPACKE_dgetrs(LAPACK_ROW_MAJOR, 'N', n_gL, nrhs, A, n_gL, ipiv, W1d, n_gL);
   }

   cout<<"affichage des poids : "<<endl;

  
   //adaption à l'intervalle [0 1]
   for (i = 0; i < n_gL; i++){
     X1d[i] = (X1d[i] + 1) * 0.5;
     W1d[i] *= 0.5;
     
   }

   //liberation de la mémoire
  delete [] work; work = 0;
  delete [] beta; beta = 0;
  delete [] alphar; alphar = 0;
  delete [] alphai; alphai = 0;
  delete [] ipiv; ipiv = 0;
  delete [] vsr; vsr = 0;
  //delete [] vsl; vsl = 0;

  delete [] A; A = 0;
  delete [] D; D = 0;
  
}



Quad_2D::Quad_2D() {
  ordre = -1;
  Xtriangle = 0;
  Wtriangle = 0;
}


void Quad_2D::Allocate_quad_2D() {
  Xtriangle = new double[2 * ordre * ordre];
  Wtriangle = new double[ordre * ordre];
}


void Quad_2D::Allocate_quad_2D(double *X, double *W) {
  Xtriangle = X;
  Wtriangle = W;
}


void Quad_2D::quad_gauss_triangle(double *X1d, double *W1d) {
  //Cette fonction calcule la quadrature de Gauss-Legendre en 2D à partir de la quadrature de Gauss-Legendre 1D
  /*-------------------------------------------------------------- input ---------------------------------------------
    X1d   : points de Gauss-Legendre 1D
    W1d   : poids de Gauss-Legendre 1D
  /*--------------------------------------------------------------- ouput --------------------------------------------------------
    Xtriangle : les points de Gauss 2D
    Wtriangle : les poids de Gauss 2D
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, s;

  s = 0;
  for (i = 0; i < ordre; i++) {
    for (j = 0; j < ordre; j++) {
      Xtriangle[2 * s] = X1d[i];//pour l'axe ox
      Xtriangle[2 * s + 1] = (1 - X1d[i]) * X1d[j];// pour l'axe oy
      Wtriangle[s] = (1 - X1d[i]) * W1d[i] * W1d[j];// pour les poids
      s++;
    }
  }
}

void Quad_2D::test_zero() {
  double X3, X1, X2;
  for (MKL_INT i = 0; i < ordre * ordre ; i++) {
    X1 =  Xtriangle[2*i] ;
    X2 =  Xtriangle[2*i+1];
    X3 = 1 - Xtriangle[2*i] - Xtriangle[2*i+1];
    if (X1<0){cout << "GROS PB";}
    if (X2<0){cout << "GROS PB";}
    if (X3<0){cout << "GROS PB";}
    if (X1>1){cout << "GROS PB";}
    if (X2>1){cout << "GROS PB";}
    if (X3>1){cout << "GROS PB";}
  }
}


void Quad_2D::test_One() {
  double r = 0.0;
  for (MKL_INT i = 0; i < ordre * ordre ; i++) {
    r += Wtriangle[i];
    
  }
  
  cout<<" r = "<<r<<endl;
}


void Quad_2D::test_XYZ(){
  double rx, ry, rz;
  rx = 0.0;
  ry = 0.0;
  rz = 0.0;
  for (MKL_INT i = 0; i < ordre * ordre ; i++) {
    rx += Wtriangle[i] * Xtriangle[2*i];
    ry += Wtriangle[i] * Xtriangle[2*i+1];
    rz += Wtriangle[i] * (1 - Xtriangle[2*i] - Xtriangle[2*i+1]);
  }
  cout<<"rx = "<<rx<<endl;
  cout<<"ry = "<<ry<<endl;
  cout<<"rz = "<<rz<<endl;
}


void Quad_2D::print_info() {
  cout<<"ordre = "<<ordre<<endl;
  for (MKL_INT i = 0; i < ordre * ordre; i++) {
    cout<<"Xtriangle["<<2*i<<"] = "<<Xtriangle[2*i]<<" Xtriangle["<<2*i+1<<"] = "<<Xtriangle[2*i+1]<<endl;
    cout<<"Wtriangle["<<i<<"] = "<<Wtriangle[i]<<endl;
  }
}

/*
Quad_2D::~Quad_2D() {
  delete [] Xtriangle; Xtriangle =  nullptr;
  delete [] Wtriangle; Wtriangle =  nullptr;
}
*/


double compute_Aire_triangle(double *X1X3, double *X2X3) {
  //Cette fonction calcule l'aire du triangle associé aux noeuds X1, X2 et X3
  /*----------------------------------------------------------- input -------------------------------------------
    X1X3 = X1 - X3
    X2X3 = X2 - X3
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Aire   : l'aire du triangle
    /*-----------------------------------------------------------------------------------------------------------------*/


  //computing the volume
  
  double *prod_Vecto = produit_vectoriel(X1X3, X2X3);

  double Aire = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
  Aire = sqrt(Aire);

  delete [] prod_Vecto; prod_Vecto = 0;
  return Aire;

 

}



double compute_Aire_triangle(Face &F) {
  //Cette fonction calcule l'aire du triangle associé à la face F
  /*----------------------------------------------------------- input -------------------------------------------
    F    : objet de type Face
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Aire   : l'aire du triangle
    /*-----------------------------------------------------------------------------------------------------------------*/

  double Aire;
  
  double *X1;
  double *X2;
  double *X3;
  
  X1 = F.noeud[0]->get_X();
  X2 = F.noeud[1]->get_X();
  X3 = F.noeud[2]->get_X();
  
  double *X2X3 = new double[3];
  double *X1X3 = new double[3];

  
  for (MKL_INT i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
  }

  //computing the volume
  
  double *prod_Vecto = produit_vectoriel(X1X3, X2X3);

  Aire = cblas_ddot(3, prod_Vecto, 1, prod_Vecto, 1);
  
  Aire = sqrt(Aire);
  
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] prod_Vecto; prod_Vecto = 0;
  return Aire;

}


Quad::Quad() {
  ordre = -1;
  Xtetra = nullptr;
  Wtetra = nullptr;
}


void Quad::Allocate_quad() {
  Xtetra = new double[3 * ordre * ordre * ordre];
  Wtetra = new double[ordre * ordre * ordre];
}


void Quad::Allocate_quad(double *X3d, double *W3d) {
  Xtetra = X3d;
  Wtetra = W3d;
}


void Quad::quad_gauss_tetra(double *X1d, double *W1d) {
  //Cette fonction calcule la quadrature de Gauss-Legendre en 3D à partir de la quadrature de Gauss-Legendre 1D
  /*--------------------------------------------------------------------------------------------------------------------------
    Xtetra : les points de Gauss 3D
    Wtetra : les poids de Gauss 3D
    X1d    : les points de Gauss 1D
    W1d    : les poids de Gauss 1D
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, k, s;

  s = 0;
   for (i = 0; i < ordre; i++) {
    for (j = 0; j < ordre; j++) {
      for (k = 0; k < ordre; k++) {
	Xtetra[3 * s] = X1d[i];
	Xtetra[3 * s + 1] = (1 - X1d[i]) * X1d[j];
	Xtetra[3 * s + 2] = (1 - X1d[i]) * (1 - X1d[j]) * X1d[k];
	Wtetra[s] = (1 - X1d[i]) * (1 - X1d[i]) * (1 - X1d[j]) * W1d[i] * W1d[j] * W1d[k];
	s++;
      }
    }
   }
}


void Quad::print_info() {
  cout<<"ordre = "<<ordre<<endl;
  for (MKL_INT i = 0; i < ordre * ordre * ordre; i++) {
    cout<<"Xtetra["<<3*i<<"] = "<<Xtetra[3*i]<<" Xtetra["<<3*i+1<<"] = "<<Xtetra[3*i+1]<<" Xtetra["<<3*i+2<<"] = "<<Xtetra[3*i+2]<<endl;
    cout<<" Wtetra["<<i<<"] = "<<Wtetra[i]<<endl;
  }
}


void Quad::test_zero() {
  double X4, X3, X1, X2;
  for (MKL_INT i = 0; i < ordre * ordre * ordre; i++) {
    X1 =  Xtetra[3*i] ;
    X2 =  Xtetra[3*i+1];
    X3 = Xtetra[3*i+2];
    X4 = 1 - X1 - X2 - X3;
    if (X1<0){cout << "GROS PB";}
    if (X2<0){cout << "GROS PB";}
    if (X3<0){cout << "GROS PB";}
    if (X4<0){cout << "GROS PB";}
    if (X1>1){cout << "GROS PB";}
    if (X2>1){cout << "GROS PB";}
    if (X3>1){cout << "GROS PB";}
    if (X4>1){cout << "GROS PB";}
  }
}


void Quad::test_One() {
  double r = 0.0;
  for (MKL_INT i = 0; i < ordre * ordre * ordre; i++) {
    r += Wtetra[i];
    
  }
  
  cout<<" r = "<<r<<endl;
}


void Quad::test_X1X2X3X4(){
  double rx1, rx2, rx3, rx4;
  rx1 = 0.0;
  rx2 = 0.0;
  rx3 = 0.0;
  rx4 = 0.0;
  for (MKL_INT i = 0; i < ordre * ordre * ordre; i++) {
    rx1 += Wtetra[i] * Xtetra[3*i];
    rx2 += Wtetra[i] * Xtetra[3*i+1];
    rx3 += Wtetra[i] * Xtetra[3*i+2];
    rx4 += Wtetra[i] * (1 - Xtetra[3*i] - Xtetra[3*i+1] - Xtetra[3*i+2]);
  }
  cout<<"rx1 = "<<rx1<<endl;
  cout<<"rx2 = "<<rx2<<endl;
  cout<<"rx3 = "<<rx3<<endl;
  cout<<"rx4 = "<<rx4<<endl;
}


/*
Quad::~Quad() {
  delete [] Xtetra; Xtetra =  nullptr;
  delete [] Wtetra; Wtetra =  nullptr;
}
*/


cplx integrale_triangle(Face &F, Quad_2D &Qd2D, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un triangle
  /*------------------------------------------------------------ input -----------------------------------------------------
    F      : objet de type Face. il contient tout ce qui est en rapport à une face
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];
  
  X1 = F.noeud[0]->get_X();
  X2 = F.noeud[1]->get_X();
  X3 = F.noeud[2]->get_X();
  
  
  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
  
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * P2.pression(Xquad);
    
  }
  
  I = I * F.Aire;
    
  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;

  return I;
  
}

cplx integrale_triangle_test(Quad_2D &Qd2D) {

  Mat3 mat;
  mat.define(1.0, 0.0, 0.0,
	     0.0, 1.0, 0.0,
	     0.0, 0.0, 1.0);
  
  double omega = 2 * M_PI;
  
  Onde_plane Px;
  
  Px.define_d(-0.707107,0.0,-0.707107);

  Px.compute_Ad_and_dAd(mat,1.0);
  Px.compute_K(1.0,omega);
  
  Onde_plane Py;
  
  Py.define_d(-0.707107,0.0,-0.707107);
  
  Py.compute_Ad_and_dAd(mat,1.0);
  Py.compute_K(1.0,omega);

  

  Sommet S[3];
  S[0].Allocate_X();
  S[0].set_val(0,0,0);
  
  S[1].Allocate_X();
  S[1].set_val(1.0,0.0,0.0);
  
  S[2].Allocate_X();
  S[2].set_val(0.0,1.0,0.0);
  
  Face F;
  
  F.noeud[0] = S + 0;
  F.noeud[1] = S + 1;
  F.noeud[2] = S + 2;
  F.compute_Aire_triangle();
  // S2[0].Allocate_X();
  // S2[0].set_val(2,0,0);
  
  // S2[1].Allocate_X();
  // S2[1].set_val(2.0,2.0,2);
  
  // S2[2].Allocate_X();
  // S2[2].set_val(2.0,2.0,-2);

  // double res = 0.0;
  
  // Face F2;
  
  // F2.noeud[0] = S2 + 0;
  // F2.noeud[1] = S2 + 1;
  // F2.noeud[2] = S2 + 2;

  cplx I = cplx(0.0,0.0);

  double deltaK[3];

  for (MKL_INT i = 0; i < 3; i++){
    deltaK[i] = Py.K[i] - Px.K[i];
  }
  
  I = integrale_triangle(F, Qd2D, Px, Py);

  cplx J = integrale_triangle(F, Px, Py);
  
  
  double Aire = compute_Aire_triangle(F);

  cout<<"J = "<<J<<endl;


  return I;
}

cplx integrale_triangle(Face &F, Quad_2D &Qd2D, Onde_plane &P1, std::function<cplx (double*)> f) {
  //Cette fonction calcule l'intégrale du produit d'une onde plane par une fonction sur un triangle
  /*------------------------------------------------------------ input -----------------------------------------------------
    F      : objet de type Face. il contient tout ce qui est en rapport à une face
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    P1     : objet de type Onde_plane
    f      : une fonction
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];
  
  X1 = F.noeud[0]->get_X();
  X2 = F.noeud[1]->get_X();
  X3 = F.noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * f(Xquad);
  }
  
  I = I * F.Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;

  return I;
  
}



cplx integrale_triangle(Face &F, Quad_2D &Qd2D, std::function<cplx (double*)> f, std::function<cplx (double*)> g) {
  //Cette fonction calcule l'intégrale du produit d'une onde plane par une fonction sur un triangle
  /*------------------------------------------------------------ input -----------------------------------------------------
    F      : objet de type Face. il contient tout ce qui est en rapport à une face
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    f, g   : deux fonctions
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];
  
  X1 = F.noeud[0]->get_X();
  X2 = F.noeud[1]->get_X();
  X3 = F.noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }
    
    I = I + Qd2D.Wtriangle[i] * conj(f(Xquad)) * g(Xquad);
  }
  
  I = I * F.Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;

  return I;
  
}









cplx quad_analytique_On_1_triangle(Face &F, Onde_plane &P1){
   double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *X1X2 = new double[3];
  
  X1 = F.noeud[0]->get_X();
  X2 = F.noeud[1]->get_X();
  X3 = F.noeud[2]->get_X();

  double beta_1, beta_2, beta_3, X3K;  
  
  for (MKL_INT i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];

    X1X2[i] = X1[i] - X2[i];
    
  }

  cplx I;

  beta_1 = cblas_ddot(3, X1X3, 1, P1.K, 1);

  beta_2 = cblas_ddot(3, X2X3, 1, P1.K, 1);

  beta_3 = cblas_ddot(3, X1X2, 1, P1.K, 1);

  X3K = cblas_ddot(3, X3, 1, P1.K, 1);

  double Aire = compute_Aire_triangle(F);

  if (abs(beta_1) < 1E-10) {
    if (abs(beta_2) > 1E-10) {
      I = Aire * exp(cplx(0.0,-X3K)) *  ( cplx(1.0,-beta_2) - exp(cplx(0.0,-beta_2))) / (beta_2 * beta_2);
    }
    else{
      I = Aire * exp(cplx(0.0,-X3K));
    }
  }
  if (abs(beta_1) > 1E-10) {
    if (abs(beta_2) < 1E-10) {
      I = Aire * exp(cplx(0.0,-X3K)) *  ( cplx(1.0,-beta_1) * exp(cplx(0.0,-beta_1)) - cplx(1.0,0.0)) / (beta_1 * beta_1);
    }
    else {
      I = Aire * exp(cplx(0.0,-X3K)) * ( (exp(cplx(0.0,-beta_1)) - exp(cplx(0.0,-beta_2))) / (beta_2 * (beta_1 - beta_2)) - (exp(cplx(0.0,-beta_1)) - cplx(1.0,0.0)) / (beta_1 * beta_2));
    }
  }

  I = 2.0 * I;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] X1X2; X1X2 = 0;
  
  
  return I;
}




 
/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------- Construction des intégrales pour le vecteur second membre --------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------ cas d'onde incidente associé au champs d' Hankel-----------------------------------*/



cplx integrale_triangle(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D) {
  //integrale sur une face de Pinc * conj(P') avec P' une onde plane et Pinc la fonction exp(ikr) / 4 * pi * r
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * he.P;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;
  
  return I;
  
}






cplx integrale_triangle_VincP(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D) {
 //Integral de Vinc * conj(P') avec P' une onde plane et Vinc la vitesse asscoiée à la fonction de Hankel
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;// he.V\cdot n_f avec n_f la normale à la face f et he.d la vitesse associée à la fonction Hankel
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);

    if(f == 0){     
      Vn_f = dot_u(he.V, Elem.n1);
    }
    else if(f == 1){
      Vn_f = dot_u(he.V, Elem.n2);
    }
    else if(f == 2){
      Vn_f = dot_u(he.V, Elem.n3);
    }
    else if(f == 3){
      Vn_f = dot_u(he.V, Elem.n4);
    }
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * Vn_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;
  
  return I;
  
}







cplx integrale_triangle_PincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D) {
  //Integral de Pinc * conj(V') avec V' vitesse d'une onde plane et Pinc fonction de Hankel
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;// V\cdot n_f avec n_f la normale à la face f et V la vitesse de l'onde plane
 
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de he en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);

    //calcule de l'onde plane et sa vitesse
    P1.compute_P_and_V(Xquad);

    //cout<<" P1.V = ("<<P1.V[0]<<","<<P1.V[1]<<","<<P1.V[2]<<")"<<endl;

    if(f == 0){     
      Vn_f = dot_c(P1.V, Elem.n1);
    }
    else if(f == 1){
      Vn_f = dot_c(P1.V, Elem.n2);
    }
    else if(f == 2){
      Vn_f = dot_c(P1.V, Elem.n3);
    }
    else if(f == 3){
      Vn_f = dot_c(P1.V, Elem.n4);
    }   
   
    
    I = I + Qd2D.Wtriangle[i] * he.P * Vn_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;

  return I;
  
}






cplx integrale_triangle_VincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D) {
  //Integrale de Vinc * conj(V') sur un triangle avec Vinc la vitesse de Hankel et V' la vitesse de l'onde plane
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx V1n_f;// he.V\cdot n_f avec n_f la normale à la face f et he.V la vitesse de Hankel
  
  cplx V2n_f;// P1.V\cdot n_f avec n_f la normale à la face f et P1.V la vitesse de l'onde plane
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }


  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de he en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);


    //calcule de l'onde plane et sa vitesse
    P1.compute_P_and_V(Xquad);

    if(f == 0){     
      V1n_f = dot_u(he.V, Elem.n1);
      V2n_f = dot_c(P1.V, Elem.n1);
    }
    else if(f == 1){
      V1n_f = dot_u(he.V, Elem.n2);
      V2n_f = dot_c(P1.V, Elem.n2);
    }
    else if(f == 2){
      V1n_f = dot_u(he.V, Elem.n3);
      V2n_f = dot_c(P1.V, Elem.n3);
    }
    else if(f == 3){
      V1n_f = dot_u(he.V, Elem.n4);
      V2n_f = dot_c(P1.V, Elem.n4);
    }
   
    I = I + Qd2D.Wtriangle[i] * V1n_f * V2n_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;
  return I;
  
}



cplx Sum_quad_triangle_PincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(P') sur des faces extérieures avec Pinc = exp(ikr) / 4 * pi * r et P' une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_PincP[l] * integrale_triangle(Mesh.elements[e], l, Mesh.X0, P1, he, Mesh.Qd2D);
  
    }
  }
  return I;
}



cplx Sum_quad_triangle_PincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces extérieures avec Pinc = exp(ikr) / 4 * pi * r et V' vitesse d'une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_PincV[l] * integrale_triangle_PincV(Mesh.elements[e], l, Mesh.X0, P1, he, Mesh.Qd2D);
  
    }
  }
  return I;
}



cplx Sum_quad_triangle_VincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he) {
  //Cette fonction calcule la somme des intégrales de Vinc * conj(P') sur des faces extérieures avec Vinc la vitesse associée exp(ikr) / 4 * pi * r et P' une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_VincP[l] * integrale_triangle_VincP(Mesh.elements[e], l, Mesh.X0, P1, he, Mesh.Qd2D);
    
    }
  }
  return I;
}



cplx Sum_quad_triangle_VincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces extérieures avec Vinc la vitesse associée à exp(ikr) / 4 * pi * r et V' vitesse d'une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction exp(ikr) / 4 * pi * r
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_VincV[l] * integrale_triangle_VincV(Mesh.elements[e], l, Mesh.X0, P1, he, Mesh.Qd2D);
    
    }
  }
  return I;
}


/*----------------------------------------------- CAS DE LA FONCTION DE BESSEL DE PREMIERE ESPECE -------------------------------*/

cplx integrale_triangle(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D) {
  //Cette fonction calcule l'intégrale du produit d'une onde plane par une fonction sur un triangle
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    j0     : objet pour définir la fonction de Bessel sin(z) / z
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    j0.compute_P_and_V(Elem.Ac,Xquad, X0);
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * j0.P;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] j0.V; j0.V = 0;
  delete [] j0.d; j0.d = 0;

  return I;
  
}




cplx integrale_triangle_PincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D) {
  //Cette fonction calcule l'intégrale du produit d'une vitesse d'onde plane par la fonction de Bessel j0 sur un triangle.
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    j0     : objet pour définir la fonction de Bessel sin(z) / z
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;//P1.V \cdot n_f avec n_f la normale à la face f et P1.V la vitesse de l'onde P1

  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    j0.compute_P_and_V(Elem.Ac,Xquad, X0);

    //calcule de l'onde plane et sa vitesse
    P1.compute_P_and_V(Xquad);
    
    
    if(f == 0){     
      Vn_f = dot_c(P1.V, Elem.n1);
    }
    else if(f == 1){
      Vn_f = dot_c(P1.V, Elem.n2);
    }
    else if(f == 2){
      Vn_f = dot_c(P1.V, Elem.n3);
    }
    else if(f == 3){
      Vn_f = dot_c(P1.V, Elem.n4);
    }   
   
    
    I = I + Qd2D.Wtriangle[i] * j0.P * Vn_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] j0.V; j0.V = 0;
  delete [] j0.d; j0.d = 0;

  return I;
  
}





cplx integrale_triangle_VincP(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D) {
  //Cette fonction calcule l'intégrale du produit d'une onde plane par la vitesse associée à la fonction de Bessel j0 sur un triangle.
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    j0     : objet pour définir la fonction de Bessel sin(z) / z
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;//j0.V \cdot n_f avec n_f la normale à la face f et j0.V la vitesse associée à j0
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    j0.compute_P_and_V(Elem.Ac,Xquad, X0);
    
    
    if(f == 0){     
      Vn_f = dot_u(j0.V, Elem.n1);
    }
    else if(f == 1){
      Vn_f = dot_u(j0.V, Elem.n2);
    }
    else if(f == 2){
      Vn_f = dot_u(j0.V, Elem.n3);
    }
    else if(f == 3){
      Vn_f = dot_u(j0.V, Elem.n4);
    }   
   
    
    I = I + Qd2D.Wtriangle[i] * conj(P1.pression(Xquad)) * Vn_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] j0.V; j0.V = 0;
  delete [] j0.d; j0.d = 0;

  return I;
  
}



cplx integrale_triangle_VincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D) {
  //Cette fonction calcule l'intégrale du produit d'une vitesse d'onde plane par la vitesse associée à la fonction de Bessel j0 sur un triangle.
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element.
    f      : numero de la face (ou du triangle)
    X0     : le point source de la fonction de Bessel
    P1     : objet de type Onde_plane
    j0     : objet pour définir la fonction de Bessel sin(z) / z
    Qd2D   : objet de type Quad_2D. Il contient la quadratute de Gauss-Legendre 2D
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx V1n_f;//j0.V \cdot n_f avec n_f la normale à la face f et j0.V la vitesse associée à j0
  cplx V2n_f;//P1.V \cdot n_f avec n_f la normale à la face f et P1.V la vitesse de l'onde plane P1
  
  X1 = Elem.face[f].noeud[0]->get_X();
  X2 = Elem.face[f].noeud[1]->get_X();
  X3 = Elem.face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < Qd2D.ordre * Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + Qd2D.Xtriangle[2*i] * X1X3[j] + Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de j0_e en Xquad
    j0.compute_P_and_V(Elem.Ac,Xquad, X0);


    //calcule de l'onde plane et sa vitesse
    P1.compute_P_and_V(Xquad);
    
    if(f == 0){     
      V1n_f = dot_u(j0.V, Elem.n1);
      V2n_f = dot_c(P1.V, Elem.n1);
    }
    else if(f == 1){
      V1n_f = dot_u(j0.V, Elem.n2);
      V2n_f = dot_c(P1.V, Elem.n2);
    }
    else if(f == 2){
      V1n_f = dot_u(j0.V, Elem.n3);
      V2n_f = dot_c(P1.V, Elem.n3);
    }
    else if(f == 3){
      V1n_f = dot_u(j0.V, Elem.n4);
      V2n_f = dot_c(P1.V, Elem.n4);
    }   
   
    
    I = I + Qd2D.Wtriangle[i] * V1n_f * V2n_f;
  }
  
  I = I * Elem.face[f].Aire;

  delete [] Xquad; Xquad = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] j0.V; j0.V = 0;
  delete [] j0.d; j0.d = 0;

  return I;
  
}




cplx Sum_quad_triangle_PincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(P') sur des faces extérieures avec Pinc = sin(z) / z et P' une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    j0     : objet définissant la fonction de bessel de première espèce j0
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_PincP[l] * integrale_triangle(Mesh.elements[e], l, Mesh.X0, P1, j0, Mesh.Qd2D);
  
    }
  }
  return I;
}



cplx Sum_quad_triangle_PincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces extérieures avec Pinc = sin(z) / z et V' vitesse d'une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    j0     : objet définissant la fonction de bessel de première espèce j0
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_PincV[l] * integrale_triangle_PincV(Mesh.elements[e], l, Mesh.X0, P1, j0, Mesh.Qd2D);
  
    }
  }
  return I;
}



cplx Sum_quad_triangle_VincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0) {
  //Cette fonction calcule la somme des intégrales de Vinc * conj(P') sur des faces extérieures avec Vinc la vitesse associée à la fonction de Bessel j0 et P' une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    j0     : objet définissant la fonction de bessel de première espèce j0
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_VincP[l] * integrale_triangle_VincP(Mesh.elements[e], l, Mesh.X0, P1, j0, Mesh.Qd2D);
    
    }
  }
  return I;
}



cplx Sum_quad_triangle_VincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces extérieures avec Vinc la vitesse associée à exp(ikr) / 4 * pi * r et V' vitesse d'une onde plane
    /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage 
    e      : numero d'un élément
    P1     : objet de type Onde_plane
    j0     : objet définissant la fonction de bessel de première espèce j0
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (Mesh.elements[e].voisin[l] < 0) {
    I +=  Mesh.elements[e].alpha_VincV[l] * integrale_triangle_VincV(Mesh.elements[e], l, Mesh.X0, P1, j0, Mesh.Qd2D);
    
    }
  }
  return I;
}




/*-------------------------------------------- cas d'une onde incidente plane ----------------------------------------------------*/


cplx integrale_PincP_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {
  //Somme des intégrales sur les faces extérieures du produit d'une onde incidente plane avec une onde plane associée à l'élément e
   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction-test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                               
  for (l = 0; l < 4; l++) {//boucle sur les faces
    if(Mesh.elements[e].voisin[l] < 0){//teste sur les faces extérieures (si > 0 alors la face l est intérieure
      I += Mesh.elements[e].alpha_PincP[l] * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }

  return I;
}





cplx integrale_PincV_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {
   //Somme des intégrales sur les faces extérieures du produit d'une onde incidente plane avec une vitesse d'onde plane. L'onde plane est associée à l'élément e
   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero de l'onde plane associée à la fonction-test vitesse
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker diloc \cdot n_j avec n_j la normale à la face j                
  
  for (l = 0; l < 4; l++) { //boule sur les faces                      
    if(Mesh.elements[e].voisin[l] < 0){//teste sur les faces extérieures
      if (l == 0) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n1, inc) ;//kj - kiloc
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 1) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
       
      }
      else if (l == 2) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 3) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      
      I += Mesh.elements[e].alpha_PincV[l] * dn * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}



cplx integrale_VincP_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {
   //Somme des intégrales sur les faces extérieures du produit d'une vitesse d'onde incidente plane avec une onde plane associée à l'élément e
   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction-test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker d \cdot n_j avec n_j la normale à la face j et d la direction de l'onde incidente
                                          
  for (l = 0; l < 4; l++) {//boucle sur les faces                       
    if (Mesh.elements[e].voisin[l] < 0){// teste sur les faces extérieures
      if (l == 0) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
      
      }
      else if (l == 1) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
 
      }
      else if (l == 2) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      else if (l == 3) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      
      I += Mesh.elements[e].alpha_VincP[l] * dn * integrale_triangle(Mesh.elements[e].face[l],   Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}





cplx integrale_VincV_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {
   //Somme des intégrales sur les faces extérieures du produit d'une vitesse d'onde incidente plane avec une autre vitesse d'onde plane associée à l'élément e
   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero de l'onde plane associée à la fonction-test vitesse
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn1, dn2;
  //dn1 pour stocker dinc \cdot n_j avec n_j la normale à la face j et dinc la direction de l'onde incidente
  //dn2 pour stocker diloc \cdot n_j avec n_j la normale à la face j et diloc la direction de l'onde iloc                
                                              
  for (l = 0; l < 4; l++) {//boucle sur les faces                       
    if (Mesh.elements[e].voisin[l] < 0){ // teste sur les faces extérieures
      if (l == 0) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n1, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      }
      else if (l == 1) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n2, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      }
      else if (l == 2) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n3, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      }
      else if (l == 3) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n4, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      }
      
      I += Mesh.elements[e].alpha_VincV[l] * dn1 * dn2 * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}





void Maillage::Transforme_realPartOfSolution_To_VTK_File(VecN &ALPHA) {
  //Ecriture de la partie imaginaire de la solution scalaire dans un fichier VTU
  //ALPHA : coordonnées globales de la solution dans la base d'onde plane
  
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte)); 

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte)); 

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte)); 

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_scalar_real.vtu");
	system("more paraview_points.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_tetra.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_type.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_offset.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_scalar_real.vtu");
	system("more paraview_value.txt >> paraview_finale_scalar_real.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_scalar_real.vtu");
}






void Maillage::Transforme_imaginaryPartOfSolution_To_VTK_File(VecN &ALPHA) {
  //Ecriture de la partie imaginaire de la solution scalaire dans un fichier VTU. Non valable pour une fonction de Bessel
  //ALPHA : coordonnées globales de la solution dans la base d'onde plane
  
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte)); 

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte)); 

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte)); 

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(elements[e].Pexacte));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_scalar_imaginary.vtu");
	system("more paraview_points.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_tetra.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_type.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_offset.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_scalar_imaginary.vtu");
	system("more paraview_value.txt >> paraview_finale_scalar_imaginary.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_scalar_imaginary.vtu");
}



cplx compute_exp(double *K, double *X) {
  double KX = cblas_ddot(3, K, 1, X, 1);
  cplx I =  exp(cplx(0.0,KX)) * X[0];
  return I;
}


cplx eval_f(double *x) {
  return cplx(x[1],0.0);
}


cplx eval_g(double *x) {
  return cplx(1.0, 0.0);
}


cplx fmnp(double *x) {
  return cplx(pow(x[0],1) * pow(x[1],1) * pow(x[2],1), 0.0);
}

cplx result_fmnp(MKL_INT m, MKL_INT n, MKL_INT p) {
  return cplx(1.0 / ((m+1) * (n+1) * (p+1)),0.0);
}


cplx integrale_triangle_3D_surCube(Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale anatlytique du produit P2 * conj(P1) de deux ondes planes sur le cube unité
  /*--------------------------------------------------------------- input ----------------------------------------------
    P1, P2   : deux objets de types Onde_plane
    /*------------------------------------------------------------- ouput ---------------------------------------------
    I        : le resultat de l'intégration
    /*-----------------------------------------------------------------------------------------------------------------*/
  cplx I1, I2, I3;
  cplx I = cplx(1.0,0.0);
  
  double *deltaK = new double[3];
  for (MKL_INT j = 0; j < 3; j++) {
    deltaK[j] = P2.K[j] - P1.K[j];
  }
  

  
  if (abs(deltaK[0]) < 1E-10) {
    I1 = cplx(1.0,0.0);
  }
  else {
    I1 = (exp(cplx(0.0,deltaK[0])) - cplx(1,0.0)) / cplx(0.0,deltaK[0]);
  }
  
  if (abs(deltaK[1]) < 1E-10) {
    I2 = cplx(1.0,0.0);
  }
  else {
    I2 = (exp(cplx(0.0,deltaK[1])) - cplx(1,0.0)) / cplx(0.0,deltaK[1]);
  }
  
  if (abs(deltaK[2]) < 1E-10) {
    I2 = cplx(1.0,0.0);
  }
  else {
    I3 = (exp(cplx(0.0,deltaK[2])) - cplx(1,0.0)) / cplx(0.0,deltaK[2]);
  }
  
  
  I = I1 * I2 * I3;
  cout<<"deltaK[0] = "<<deltaK[0]<<" deltaK[1] = "<<deltaK[1]<<" deltaK[2] = "<<deltaK[2]<<endl;
  
  delete [] deltaK; deltaK = 0;
  
  return I;
}



cplx integrale_triangle_3D_surCube(Onde_plane &P1, int a) {
  //Cette fonction calcule l'intégrale anatlytique du produit x[0] * conj(P1) sur le cube unité avec P1 une onde plane et x un vecteur
  /*--------------------------------------------------------------- input ----------------------------------------------
    P1   : objet de type Onde_plane
    x    : un vecteur 
    /*------------------------------------------------------------- ouput ---------------------------------------------
    I        : le resultat de l'intégration
    /*-----------------------------------------------------------------------------------------------------------------*/
  cplx I1, I2, I3;
  cplx I ;

  if (a == 1) {
  
    if (abs(P1.K[0]) < 1E-10) {
      I1 = cplx(0.5,0.0);
    }
    else {
      I1 = (cplx(1.0,P1.K[0]) * exp(cplx(0.0,- P1.K[0])) - cplx(1,0.0)) / cplx(P1.K[0]*P1.K[0],0.0);
    }
    
    if (abs(P1.K[1]) < 1E-10) {
      I2 = cplx(1.0,0.0);
    }
    else {
      I2 = (cplx(1,0.0) - exp(cplx(0.0,- P1.K[1]))) / cplx(0.0, P1.K[1]);
    }
    
    if (abs(P1.K[2]) < 1E-10) {
    I3 = cplx(1.0,0.0);
    }
    else {
      I3 = (cplx(1,0.0) - exp(cplx(0.0, - P1.K[2]))) / cplx(0.0, P1.K[2]);
    }
  }
  else if (a == 2) {

    if (abs(P1.K[0]) < 1E-10) {
      I1 = cplx(1.0,0.0);
    }
    else {
      I1 = (cplx(1.0,0.0) - exp(cplx(0.0,- P1.K[0]))) / cplx(0.0, P1.K[0]);
    }
    
    if (abs(P1.K[1]) < 1E-10) {
      I2 = cplx(0.5,0.0);
    }
    else {
      I2 = (cplx(1.0,P1.K[1]) * exp(cplx(0.0,- P1.K[1])) - cplx(1.0,0.0)) / cplx(P1.K[1]*P1.K[1],0.0);
    }
    
    if (abs(P1.K[2]) < 1E-10) {
    I3 = cplx(1.0,0.0);
    }
    else {
      I3 = (cplx(1,0.0) - exp(cplx(0.0, - P1.K[2]))) / cplx(0.0, P1.K[2]);
    }
    
  }
  else if (a == 3) {

    if (abs(P1.K[0]) < 1E-10) {
      I1 = cplx(1.0,0.0);
    }
    else {
      I1 = (cplx(1.0,0.0) - exp(cplx(0.0,- P1.K[0]))) / cplx(0.0, P1.K[0]);
    }
    
    if (abs(P1.K[1]) < 1E-10) {
      I2 = cplx(1.0,0.0);
    }
    else {
      I2 = (cplx(1.0,0.0) - exp(cplx(0.0,- P1.K[1]))) / cplx(0.0,P1.K[1]);
    }
    
    if (abs(P1.K[2]) < 1E-10) {
    I3 = cplx(0.5,0.0);
    }
    else {
      I3 = (cplx(1,P1.K[2]) * exp(cplx(0.0, - P1.K[2])) - cplx(1.0,0.0)) / cplx(P1.K[2]*P1.K[2],0.0);
    }
    
  }
  I = I1 * I2 * I3;
  cout<<"K[0] = "<<P1.K[0]<<" K[1] = "<<P1.K[1]<<" K[2] = "<<P1.K[2]<<endl;
 
  return I;
}



double Element::Check_volume(){

  double *x1, *x2, *x3, *x4;
  double *x2x1 = new double[3];//x2 - x1
  double *x3x1 = new double[3];//x3 - x1
  double *x4x1 = new double[3];//x4 - x1
  
  x1 = noeud[0]->get_X();//neoud numero 1 tetra
  x2 = noeud[1]->get_X();//............ 2 .....
  x3 = noeud[2]->get_X();//............ 3 .....
  x4 = noeud[3]->get_X();//............ 4 ....
  
   for (MKL_INT j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];
     x3x1[j] = x3[j] - x1[j];
     x4x1[j] = x4[j] - x1[j];
   }

   double H = compute_volume_tetra_II(x2x1, x3x1, x4x1);

   H = Vol - H;

    delete [] x2x1; x2x1 = 0;
    delete [] x3x1; x3x1 = 0;
    delete [] x4x1; x4x1 = 0;

    return H;
}


void Element::compute_volume_tetra() {
  //Cette fonction calcule le volume du tetra associé aux vecteurs x2x1, x3x1, x4x1
  /*----------------------------------------------------------- input -------------------------------------------
                                                        ZERO INPUT
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Vol   : le volume du tetra
    /*-----------------------------------------------------------------------------------------------------------------*/
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];//x2 - x1
  double *x3x1 = new double[3];//x3 - x1
  double *x4x1 = new double[3];//x4 - x1
  double KX;// deltaK \cdot Xquad
  
  x1 = noeud[0]->get_X();//neoud numero 1 tetra
  x2 = noeud[1]->get_X();//............ 2 .....
  x3 = noeud[2]->get_X();//............ 3 .....
  x4 = noeud[3]->get_X();//............ 4 ....
  
   for (MKL_INT j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];
     x3x1[j] = x3[j] - x1[j];
     x4x1[j] = x4[j] - x1[j];
   }

   MKL_INT inc, lda;
   inc = 1; lda = 3;

   double *prod_Vecto = produit_vectoriel(x2x1, x3x1);

   Jac = cblas_ddot(lda, prod_Vecto, inc, x4x1, inc) ;

   Jac = abs(Jac) ;
   Vol = Jac / 6.0;

    delete [] prod_Vecto; prod_Vecto = 0;
    delete [] x2x1; x2x1 = 0;
    delete [] x3x1; x3x1 = 0;
    delete [] x4x1; x4x1 = 0;
}

void Element::compute_volume_tetra(double *x2x1, double *x3x1, double *x4x1, double *prod_Vecto) {
  //Cette fonction calcule le volume du tetra associé aux vecteurs x2x1, x3x1, x4x1
  /*----------------------------------------------------------- input -------------------------------------------
                                                        ZERO INPUT
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Vol   : le volume du tetra
    /*-----------------------------------------------------------------------------------------------------------------*/
  double *x1 = nullptr, *x2 = nullptr, *x3 = nullptr, *x4 = nullptr;
  
  x1 = noeud[0]->get_X();//neoud numero 1 tetra
  x2 = noeud[1]->get_X();//............ 2 .....
  x3 = noeud[2]->get_X();//............ 3 .....
  x4 = noeud[3]->get_X();//............ 4 ....
  
   for (MKL_INT j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];
     x3x1[j] = x3[j] - x1[j];
     x4x1[j] = x4[j] - x1[j];
   }

   MKL_INT inc, lda;
   inc = 1; lda = 3;

   produit_vectoriel(x2x1, x3x1, prod_Vecto);

   Jac = cblas_ddot(lda, prod_Vecto, inc, x4x1, inc) ;

   Jac = abs(Jac) ;
   Vol = Jac / 6.0;
}



double Element::compute_Wave_length(){
  double wl = cbrt(Vol);
  return wl;
}


double Element::compute_volume_tetra_II(double *x2x1, double *x3x1, double *x4x1) {
  //Cette fonction calcule le volume du tetra associé aux vecteurs x2x1, x3x1, x4x1
  /*----------------------------------------------------------- input -------------------------------------------
    x2x1 = x2 - x1
    x3x1 = x3 - x1
    x4x1 = x4 - x1
    /*--------------------------------------------------------- ouput ---------------------------------------------
    Vol   : le volume du tetra
    /*-----------------------------------------------------------------------------------------------------------------*/

  //produit vectoriel de x2x1 et x3x1
  double *prod_Vecto = produit_vectoriel(x2x1, x3x1);

  double Jac = cblas_ddot(3, prod_Vecto, 1, x4x1, 1);
  
  Jac = abs(Jac);
  Vol = Jac / 6.0;
  delete [] prod_Vecto; prod_Vecto = 0;
  
  return Vol;
}
/********************************************************** Integrateurs 3D *********************************************/

cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * P2.pression(Xquad);
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}

/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2, MKL_INT aa) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  cplx I = cplx(0.0, 0.0);
  /*double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * P2.pression(Xquad);
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;*/
  
  return I;
}


/*------------------------------------------------------------------------------------ AVEC POINTEURS -----------------------------------------------------------------------------*/

cplx quad_tetra(Element *Elem, Quad *Qd, Onde_plane *P1, Onde_plane *P2, MKL_INT aa) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  cplx I = cplx(0.0, 0.0);
  /*double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = *Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = *Elem.noeud[1]->get_X();//................ 2
  x3 = *Elem.noeud[2]->get_X();//................ 3
  x4 = *Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * P2.pression(Xquad);
  }

  I = I * Elem.Jac;
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;*/
  
  return I;
}


/*------------------------------------------------------------------------------------- quad_tetra V V' -----------------------------------------------------------------------------------------*/

cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux vitesses d'onde planes sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calcule de P et V pour l'onde P1
    P1.compute_P_and_V(Xquad);

    //calcule de P et V pour l'onde P2
    P2.compute_P_and_V(Xquad);

    J = dot_c(P1.V, P2.V);
    
    I = I + Qd.Wtetra[i] * J;
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}


/*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, std::function<cplx (double*)> f) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. 
    f      : une fonction
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, inc, lda;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }

  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * f(Xquad);
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}




cplx quad_tetra(Element &Elem, Quad &Qd, std::function<cplx (double*)> f, std::function<cplx (double*)> g) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    f, g   : deux fonctions 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, inc, lda;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    I = I + Qd.Wtetra[i] * conj(f(Xquad)) * g(Xquad);
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}



cplx quad_tetra(Maillage &Mesh, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    I = I + quad_tetra(Mesh.elements[i], Mesh.Qd, P1, P2);
  }
  
  return I;
}



cplx quad_tetra(Maillage &Mesh, Onde_plane &P1, std::function<cplx (double*)> f) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage
    P1     : objet de type Onde_plane
    f     : une fonction
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    I = I + quad_tetra(Mesh.elements[i], Mesh.Qd, P1, f);
  }
  
  return I;
}



cplx quad_tetra(Maillage &Mesh, std::function<cplx (double*)> f, std::function<cplx (double*)> g) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Mesh   : le maillage
    f, g   : deux fonctions
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    I = I + quad_tetra(Mesh.elements[i], Mesh.Qd, f, g);
  }
  
  return I;
}



Bessel::Bessel() {
  P = 0.0;
  V = 0;
  d = 0;
  k = 2*M_PI;
  r = -1;
  kr  = 0.0;
}


MKL_INT Bessel::compute_P_and_V(Mat3 &mat, double *X, double *X0) {
  //Cette fonction calcule la pression P de la fonction de Bessel
  /*------------------------------------------------------------------- input -------------------------------------------------
    mat    : objet contenant les coefficients associés à l'équation de Helmholtz correspondante à l'élément

    X    : la variable ou est évaluée la fonction
    X0   : le point source
    /*----------------------------------------------------------------- ouput -------------------------------------------------

    P    :  la pression
    /*-----------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT j;
  double *XX0 = new double[3];//X - X0
  d = new double[3];
  for (j = 0; j < 3; j++){
    XX0[j] = X[j] - X0[j];
      }

  //r = ||X - X0||_2
  r = cblas_ddot(3, XX0, 1, XX0, 1) ;
  r = sqrt(r);
  
  if (abs(r) < 1E-10) {
    cout<<"point de discontinuité atteint "<<endl;
    return EXIT_FAILURE;
  }
  
  //kr = k * r
  kr = k * r;
 
  double rr = 1.0 / r;

  //d = (X - X0) / ||X - X0||
  for (j = 0; j < 3; j++){
    d[j] = XX0[j] * rr;
  }

  //P = sin(k * r) / (k * r)
  P = sin(kr) / kr;

  V = new cplx[3];
  
  double *Ad = new double[3];
  
  //Ad = A * d,   avec A la matrice associée à l'équation de Helmholtz sur l'élément correspondant
  cblas_dgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1.0, mat.value, 3, d, 1, 0.0, Ad, 1) ;
  
  //V = A * grad(P) /  i k r
  for (MKL_INT j = 0; j < 3; j++){
    V[j] = Ad[j] * ((kr * cos(kr) -  sin(kr)) / cplx(0.0,kr * kr));
  }
  
  delete [] Ad; Ad = 0;
  delete [] XX0; XX0 = 0;

  return(EXIT_SUCCESS);
}





void Bessel::print_info() {
  cout<<"k = "<<k<<endl;
  cout<<"r = "<<r<<endl;
  cout<<"P = "<<P<<endl;
  cout<<"Kr = "<<kr<<endl;
  if (V) cout<<"V = "<<V[0]<<"\n"<<V[1]<<"\n"<<V[2]<<endl;
  
}


Bessel::~Bessel() {
  delete [] d; d = 0;
  delete [] V; V = 0;
  P = 0.0;
  k = 0.0;
  r = -1.0;
  kr = 0.0;
}


cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Bessel &j0_e, double *X0) {
  //Cette fonction calcule l'intégrale sur un tetra du produit de la fonction de Bessel j0 avec conj(w_iloc), une fonction de base de l'élément correspondant
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element
    Qd     : objet contenant la quadrature de Guass-Legendre
    P1     : objet de type Onde_plane
    j0_e   : objet pour définir la fonction de Bessel sin(z) / z
    X0     : le point source de la fonction de Bessel
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  
  

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //Evaluation de j0_e en Xquad
    j0_e.compute_P_and_V(Elem.Ac,Xquad, X0);
    
    I = I + Qd.Wtetra[i] * j0_e.P * conj(P1.pression(Xquad));
  }
  
  I = I * Elem.Jac;
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  delete [] j0_e.d; j0_e.d = 0;
  
  return I;
}



/*--------------------------------------------------------- Integrale Second membre avec Hankel ---------------------------------------*/

cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Hankel &he, double *X0) {
  //Cette fonction calcule l'intégrale sur un tetra du produit de la fonction de Bessel j0 avec conj(w_iloc), une fonction de base de l'élément correspondant
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element
    Qd     : objet contenant la quadrature de Guass-Legendre
    P1     : objet de type Onde_plane
    he     : objet pour définir la fonction de Hankel exp(ikr) / 4 * pi * r
    X0     : le point source de la fonction de Bessel
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  
  

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //Evaluation de j0_e en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);
    
    I = I + Qd.Wtetra[i] * he.P * conj(P1.pression(Xquad));
  }
  
  I = I * Elem.Jac;
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;
  
  return I;
}





cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Bessel &j0_e, double *X0) {
  //Cette fonction calcule l'intégrale du produit de deux vitesses d'onde planes sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    j0_e   : objet pour définir la fonction de Bessel sin(z) / z
    X0     : le point source de la fonction de Bessel
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calcule de P et V pour l'onde P1
    P1.compute_P_and_V(Xquad);

    //Evaluation de j0_e en Xquad
    j0_e.compute_P_and_V(Elem.Ac,Xquad, X0);

    J = dot_c(P1.V, j0_e.V);
    
    I = I + Qd.Wtetra[i] * J;
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;

  delete [] j0_e.d; j0_e.d = 0;
  delete [] j0_e.V; j0_e.V = 0;
  
  return I;
}



cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Hankel &he, double *X0) {
  //Cette fonction calcule l'intégrale du produit de deux vitesses d'onde planes sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    he     : objet pour définir la fonction de Hankel exp(ikr) / 4 * pi * r
    X0     : le point source de la fonction de Bessel
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calcule de P et V pour l'onde P1
    P1.compute_P_and_V(Xquad);

    //Evaluation de j0_e en Xquad
    he.compute_P_and_V(Elem.Ac,Xquad, X0);

    J = dot_c(P1.V, he.V);
    
    I = I + Qd.Wtetra[i] * J;
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] he.d; he.d = 0;
  delete [] he.V; he.V = 0;
  
  return I;
}



// /******************************************************************************************************************/
void BuildMat_projection(Maillage &Mesh, MKL_INT e, cplx *MP) {
  //Cette fonction calcule la matrice de la projection sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    Mesh   : le maillage
    e      : le numero de l'element 
    MP     : tableau ou stocker la matrice
    /*---------------------------------------------------------------- ouput --------------------------------------------
    MP     : la matrice de la projection
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  for (i = 0; i < Mesh.ndof; i++) {
    for (j = 0; j < Mesh.ndof; j++) {
      MP[i * Mesh.ndof + j] = quad_tetra(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], Mesh.elements[e].OP[j]);
    }
  }
}



void BuildMat_projection_PP_VV(MKL_INT e, cplx *MP, Maillage &Mesh) {
  //Cette fonction calcule la matrice de la projection de PP + VV sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    e      : le numero de l'element 
    MP     : tableau ou stocker la matrice
    Mesh   : le maillage
    /*---------------------------------------------------------------- ouput --------------------------------------------
    MP     : la matrice de la projection
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  for (i = 0; i < Mesh.ndof; i++) {
    for (j = 0; j < Mesh.ndof; j++) {
      MP[i * Mesh.ndof + j] = quad_tetra(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], Mesh.elements[e].OP[j]) + quad_tetra_VV(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], Mesh.elements[e].OP[j]);
    }
  }
}




void BuildVector_projection(Maillage &Mesh, MKL_INT e, cplx *F, const string & arg) {
  //Cette fonction calcule le vecteur second membre de la projection sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     Mesh   : le maillage
     e      : le numero de l'element
     F      : tableau ou stocker le vecteur
     arg    : chaine de caractère pour déterminer le choix du vecteur second membre;
              si arg = "plane", on traitera le cas d'une onde plane incidente
	      sinon, on aura affaire à une fonction de bessel j0 
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
  if (arg ==  "plane") { 
    for (i = 0; i < Mesh.ndof; i++) {
      F[i] =  quad_tetra(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], Mesh.elements[e].OP[Mesh.n_i]);
    }
  }
  else if (arg ==  "bessel") {
    Bessel j0;
    for (i = 0; i < Mesh.ndof; i++) {
      F[i] =  quad_tetra(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], j0, Mesh.X0);
    }
  }
  else if (arg ==  "hankel") {
    Hankel he;
    for (i = 0; i < Mesh.ndof; i++) {
      F[i] =  quad_tetra(Mesh.elements[e], Mesh.Qd, Mesh.elements[e].OP[i], he, Mesh.X0);
    }
  }
  else {
    cout<<"probleme de mot clef"<<endl;
  }
}



void BuildVector_projection_Hetero(MKL_INT e, cplx *F, Maillage &Mesh) {
  //Cette fonction calcule le vecteur second membre de la projection sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     e      : le numero de l'element
     F      : tableau ou stocker le vecteur
     Mesh   : le maillage
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
 
  for (i = 0; i < Mesh.ndof; i++) {
    F[i] = quad_tetra_heteroPlane(e, i, Mesh);
  }
}


void BuildVector_projection_Hetero(MKL_INT e, VecN ALPHA, cplx *F, Maillage &Mesh) {
  //Cette fonction calcule le vecteur second membre de la projection sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     e      : le numero de l'element
     ALPHA  : coordonnées de la solution analytique sur tous les éléments
     F      : tableau ou stocker le vecteur
     Mesh   : le maillage
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
 
  for (i = 0; i < Mesh.ndof; i++) {
    F[i] = quad_tetra_heteroPlane(e, i, ALPHA, Mesh);
  }
}



void BuildVector_projection_Hetero_PP_VV(MKL_INT e, cplx *F, Maillage &Mesh) {
  //Cette fonction calcule le vecteur second membre de la projection de PP + VV sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     e      : le numero de l'element
     F      : tableau ou stocker le vecteur
     Mesh   : le maillage
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
 
  for (i = 0; i < Mesh.ndof; i++) {
    F[i] = quad_tetra_heteroPlane(e, i, Mesh) + quad_tetra_heteroPlane_VV(e, i, Mesh);
  }
}





void Element::transport_theMatrix_to_Matlab(MKL_INT n, cplx *MP, const char chaine[]) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING .\n", file);
    exit(EXIT_FAILURE);
  }
  cplx val;
  fprintf(fic,"%d\n",n);
  for(MKL_INT i = 0 ; i < n; i++){
    for(MKL_INT j = 0 ; j < n; j++){
      val = MP[i + j * n];
      fprintf(fic,"%d\t %d\t %10.15e\t \%10.15e\n",i+1,j+1,real(val),imag(val));
    }
  }
  fclose(fic);
  free(file); file = NULL; 
}


void Element::transport_theMatrix_to_Matlab_zero_base(MKL_INT n, cplx *MP, const char chaine[]) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING .\n", file);
    exit(EXIT_FAILURE);
  }
  cplx val;
  fprintf(fic,"%d\n",n);
  for(MKL_INT i = 0 ; i < n; i++){
    for(MKL_INT j = 0 ; j < n; j++){
      val = MP[i + j * n];
      fprintf(fic,"%d\t %d\t %10.15e\t \%10.15e\n",i,j,real(val),imag(val));
    }
  }
  fclose(fic);
  free(file); file = NULL; 
}


void transport_theMatrix_to_Matlab(MKL_INT m, MKL_INT n, cplx *MP, const char chaine[]) {
  //Cette fonction transporte la matrice MP de taille m x n vers matlab
  /*------------------------------------------------------------------------------- input ----------------------------------------------
    m      : le nombre de ligne
    n      : ............ colonne
    MP     : la matrice
    chaine : contient le nom du fichier ou écrire la matrice
    /*----------------------------------------------------------------------------- intermédiare ---------------------------------------------
    fic    : variable fichier
    file   : chaine + "txt"
    /*----------------------------------------------------------------------------------------------------------------------------------*/
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING.\n", file);
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < m; i++){
    for(MKL_INT j = 0 ; j < n; j++){
      val = MP[i + j * m];
      fprintf(fic,"%d\t %d\t %10.15e\t \%10.15e\n",i+1,j+1,real(val),imag(val));
    }
  }
  fclose(fic);
  free(file); file = NULL; 
}


void transport_theMatrix_to_Matlab_zero_base(MKL_INT m, MKL_INT n, cplx *MP, const char chaine[]) {
  //Cette fonction transporte la matrice MP de taille m x n vers matlab
  /*------------------------------------------------------------------------------- input ----------------------------------------------
    m      : le nombre de ligne
    n      : ............ colonne
    MP     : la matrice
    chaine : contient le nom du fichier ou écrire la matrice
    /*----------------------------------------------------------------------------- intermédiare ---------------------------------------------
    fic    : variable fichier
    file   : chaine + "txt"
    /*----------------------------------------------------------------------------------------------------------------------------------*/
  FILE*  fic;
  
  char *file=NULL;

  file = (char *) calloc(strlen(chaine) + 100, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING.\n", file);
    exit(EXIT_FAILURE);
  }
  

  

  cplx val;
  for(MKL_INT i = 0 ; i < m; i++){
    for(MKL_INT j = 0 ; j < n; j++){
      val = MP[i + j * m];
      fprintf(fic,"%d\t %d\t %10.15e\t \%10.15e\n",i,j,real(val),imag(val));
    }
  }
  fclose(fic);
  free(file); file = NULL; 
}



void Element::transport_Vector_to_Matlab(MKL_INT n, cplx *U, const char chaine[]) {
   FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING .\n", file);
    exit(EXIT_FAILURE);
  }
  cplx val;
  fprintf(fic,"%d\n",n);
  for(MKL_INT j = 0 ; j < n; j++){
    val = U[j];
    fprintf(fic,"%10.15e\t \%10.15e\n",real(val),imag(val));
  }
  fclose(fic);
  free(file); file = NULL; 
}
 

void Element::transport_Vector_to_Matlab(MKL_INT n, double *U, const char chaine[]) {
   FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN FILE %s FOR WRITING .\n", file);
    exit(EXIT_FAILURE);
  }
  
  for(MKL_INT j = 0 ; j < n; j++){
    fprintf(fic,"%10.15e\n",U[j]);
  }
  fclose(fic);
  free(file); file = NULL; 
}



void Element::combinaison_projectionSolution(MKL_INT n, cplx *ALPHA, double *X) {

    //Cette fonction calcul la projection de la solution locale asscoiée à l'élément en X et la stocke en Pro_P = \sum_{iloc = 1}{ndof} ALPHA[i] * exp(i K X) (pour la pression) et pro_V (pour la vitesse)
  /*-------------------------------------------------------------- input ----------------------------------------
    n     : le nombre de degré de liberté en chaque élément
    ALPHA    : les coordonnées de la solution
    X        : la variable est évalué la solution
    /*------------------------------------------------------------- ouput ---------------------------------------
    Pro_P  : la pression
    Pro_V  : la vitesse
    /*-----------------------------------------------------------------------------------------------------------*/
  
  Pro_P = cplx(0.0,0.0);
  MKL_INT i, iloc;
 
  for (i = 0; i < 3; i++)
    Pro_V[i] = cplx(0.0,0.0);

  for(iloc = 0; iloc < n; iloc++){

    //calcul de P et V en X. P est stockée dans OP[iloc].P et V dans OP[iloc].V
    OP[iloc].compute_P_and_V(X);
    
    //Pro_P += ALPHA[iloc] * P_iloc
    Pro_P += ALPHA[iloc] * OP[iloc].P;

    //V += ALPHA[iloc] * V_iloc 
    for(i = 0; i < 3; i++){
      
      Pro_V[i] += ALPHA[iloc] * OP[iloc].V[i];
    }
  }
}
/*--------------------------------------------------------------------------------------------------------------------------------------------*/

void Element::combinaison_projectionSolutionPressure(MKL_INT n, cplx *ALPHA, double *X) {

    //Cette fonction calcul la projection de la solution locale asscoiée à l'élément en X et la stocke en Pro_P = \sum_{iloc = 1}{ndof} ALPHA[i] * exp(i K X) (pour la pression) et pro_V (pour la vitesse)
  /*-------------------------------------------------------------- input ----------------------------------------
    n     : le nombre de degré de liberté en chaque élément
    ALPHA    : les coordonnées de la solution
    X        : la variable est évalué la solution
    /*------------------------------------------------------------- ouput ---------------------------------------
    Pro_P  : la pression
    Pro_V  : la vitesse
    /*-----------------------------------------------------------------------------------------------------------*/
  
  Pro_P = cplx(0.0,0.0);
  MKL_INT iloc;
 

  for(iloc = 0; iloc < n; iloc++){

    //calcul de P et V en X. P est stockée dans OP[iloc].P et V dans OP[iloc].V
    OP[iloc].compute_P_and_V(X);
    
    //Pro_P += ALPHA[iloc] * P_iloc
    Pro_P += ALPHA[iloc] * OP[iloc].P;

  }
}


/*--------------------------------------------------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales(VecN &U, const string & arg, Maillage &Mesh) {

  //Ici, on recolle toutes les solutions locales dans le vecteur U de type VecN
  /*-------------------------------------------------------------------- input ------------------------------------------------
    U    : objet de type VecN, c'est là qu'on stockera notre vecteur global

    arg  : chaine de caractère qui nous permet de déterminer le cas à traiter. 
            - Pour arg = "plane" , on a traitera le cas d'une onde plane incidente
            - Pour arg = "bessel", c'est le cas de la fonction de Bessel de première espéce j0 considèrée comme onde incidente
	    - Pour arg = "hankel", c'est le cas de la fonction de exp(ikr)/4*pi*r considèrée comme onde incidente

    Mesh : le maillage
    /*------------------------------------------------------------------ ouput -------------------------------------------------
    U    : notre vecteur global
    /*--------------------------------------------------------------------------------------------------------------------------*/
  cplx *B;//pointeur pour clôner vers une adresse de U

  MKL_INT iloc, e;
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;
   
 
  
  VecN u;// Vecteur de la projection. C'est un objet de type VecN
  
  u.n = Mesh.ndof;
  
 
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    
    MP.Allocate_MatMN();

    u.Allocate();
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(Mesh, e, MP.value);
    
    //Construction du vecteur second membre correspondant à l'élément e
    BuildVector_projection(Mesh, e, u.value, arg);
    
    //résolution du système linéaire correspondante par la méthode LU 
    MP.Solve_by_LU(u);
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(Mesh.ndof,e,&B);
    
    //On colle la solution associée à l'élément e au vecteur global U
    for(iloc = 0; iloc < Mesh.ndof; iloc++){
      B[iloc] = u.value[iloc];
    }
   
  }
  delete [] MP.value ; MP.value = 0;
  delete [] u.value ; u.value = 0;
}
/*---------------------------------------------------------------------------- Projection réduite -------------------------------------------------------------*/


void compute_sizeBlocRed(MKL_INT e, cplx* AA, double *w, cplx *A, Maillage &Mesh) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* (avec P une matrice orthonormale et D contient les valeurs propres) et calcule la taille de la nouvelle base réduite de Galerkin associée à l'élément e
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    AA     : pour stocker tous les vecteurs propres
    w      : ............ toutes les valeurs propres
    A      : la matrice de la projection sur e
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Nred : la taille de la nouvelle base réduite de Galerkin
    Nres : ndof - Nred
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour stocker les vecteurs propres
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;

  Mesh.elements[e].Nred_Pro = 0;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      AA[iloc * Mesh.ndof + jloc] = A[iloc * Mesh.ndof + jloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_ROW_MAJOR , 'V', 'L', Mesh.ndof, AA, Mesh.ndof, w);


  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    if (w[iloc] > Mesh.elements[e].eps_max){
      Mesh.elements[e].Nred_Pro++;
    }
  }

  Mesh.elements[e].Nres_Pro = Mesh.ndof - Mesh.elements[e].Nred_Pro;
  
}




void DecompositionDeBloc(MKL_INT e, cplx *A, cplx *P_red, cplx *V, Maillage &Mesh) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    A      : Matrice associée à l'élément e
    P_red  : tableau pour stocker la base réduite de vecteurs propres
    V      : .................... les N_red plus grandes valeurs propres 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour stocker tous les vecteurs propres via une fonction lapack
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC;

  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      AA[iloc * Mesh.ndof + jloc] = A[iloc * Mesh.ndof + jloc];
    }
  }
  
  
  double *w = new double[Mesh.ndof];//pour stocker les valeurs propres; w = D
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_ROW_MAJOR , 'V', 'L', Mesh.ndof, AA, Mesh.ndof, w);
  
  //stockage de la base réduite associée à l'élément e
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = Mesh.elements[e].Nres_Pro; jloc < Mesh.ndof; jloc++){
      P_red[iloc * Mesh.ndof + jloc] = AA[iloc * Mesh.ndof + jloc];
    }
  }
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc =  Mesh.elements[e].Nres_Pro; iloc < Mesh.ndof; iloc++){
    V[ILOC] = cplx(w[iloc], 0.0);
    ILOC++;
  }
  
 
  delete [] w; w = 0;
  delete [] AA; AA = 0;
}



void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, cplx *P_red,  Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    JLOC = 0;
    for (jloc = Mesh.elements[e].Nres_Pro; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
      
      a[iloc * Mesh.elements[e].Nred_Pro + JLOC] = P_red[iloc * Mesh.ndof + jloc];
      
      b[JLOC * Mesh.ndof + iloc] = conj(P_red[iloc * Mesh.ndof + jloc]);
      JLOC++;
    }      
  }
}



void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *P_red,  const string & arg, Maillage &Mesh) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra soit la restriction de 
                P_red , soit la restriction de l'adjoint de P_red sur l'élément e
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    arg       : chaîne de caractères, elle détermine l'opération à faire selon :
                arg == "selfP", stocke la restriction de P_red à l'élément e dans a
		arg == "adjtP", ....................... l'adjoint de P_red à l'élément e dans b
    Mesh      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a    :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, JLOC;
  
  if (arg == "selfP"){
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[e].Nres_Pro; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
	a[iloc * Mesh.elements[e].Nred_Pro + JLOC] = P_red[iloc * Mesh.ndof + jloc];
	
	JLOC++;
      }      
    }
  }
  
  else  if (arg == "adjtP"){
    
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
      JLOC = 0;
      for (jloc = Mesh.elements[e].Nres_Pro; jloc < Mesh.ndof; jloc++) { // numero de la colonne	  	
	a[JLOC * Mesh.ndof + iloc] = conj(P_red[iloc * Mesh.ndof + jloc]);
	
	JLOC++;
      }      
    }
  }
  
  else {
    cout<<"probleme de mot clef"<<endl;
  }
  
}



void compute_A_red(MKL_INT e, const string & choice, MatMN &A, cplx *P_red, MatMN &A_red, Maillage &Mesh){

   MKL_INT size_aloc;
  if (choice == "two"){
    size_aloc = Mesh.elements[e].Nred_Pro;
  }
  else if (choice == "one") {
    size_aloc = Mesh.elements[e].Nred;
  }
  
  
  cplx *a = new cplx[size_aloc * Mesh.ndof];
  cplx *b = new cplx[size_aloc * Mesh.ndof];
  cplx *c = new cplx[size_aloc * Mesh.ndof];

  store_P_and_transpose_P(e, b, c, P_red, Mesh);
  
  prodMatMatComplex(Mesh.ndof, Mesh.ndof, Mesh.ndof, size_aloc, A.value, b, a);
  
  prodMatMatComplexNoInit(size_aloc, Mesh.ndof, Mesh.ndof, size_aloc, c, a, A_red.value);

    delete [] a; a = 0;
    delete [] b; b = 0;
    delete [] c; c = 0;
  
}


void compute_F_red(MKL_INT e, const string & choice, cplx *b, cplx *P_red, cplx *b_red, Maillage &Mesh) {
  //Cette fonction calcule le vecteur réduit b_red à partir du vecteur b et de la matrice P_red

  /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    b       : le vecteur second membre sur l'élément
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres
    b_red   : conj(trans(P_red)) *  b
    Mesh    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           b_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    a       :=  trans(conj(P_red))
    /*-------------------------------------------------------------------------------------------------------------------------------------*/

  MKL_INT size_aloc;
  if (choice == "two"){
    size_aloc = Mesh.elements[e].Nred_Pro;
  }
  else if (choice == "one") {
    size_aloc = Mesh.elements[e].Nred;
  }
  
  cplx *a = new cplx[size_aloc * Mesh.ndof];

  store_P_and_transpose_P(e, a, P_red, "adjtP", Mesh);
  
  prodMatVecComplex(size_aloc, Mesh.ndof, Mesh.ndof, a, b, b_red);

  delete [] a; a = 0;
  
}


void reverseTo_originalVector(MKL_INT e, const string & choice, cplx *x, cplx *P_red, cplx *x_red, Maillage &Mesh) {
   //Cette fonction calcule permet de retrouver le vecteur original x à partir du vecteur réduit x_red et de la matrice P_red

  /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    x       : le vecteur second membre sur l'élément
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres
    x_red   : le vecteur réduit (ou le vecteur x dans la base réduite P_red)
    Mesh    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           x
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    a       :=  P_red
    /*-------------------------------------------------------------------------------------------------------------------------------------*/

  MKL_INT size_aloc;
  if (choice == "two"){
    size_aloc = Mesh.elements[e].Nred_Pro;
  }
  else if (choice == "one") {
    size_aloc = Mesh.elements[e].Nred;
  }
  
  cplx *a = new cplx[Mesh.ndof * size_aloc];

  store_P_and_transpose_P(e, a, P_red, "selfP", Mesh);

  //x = a * x_red
  prodMatVecComplex(Mesh.ndof, size_aloc,  size_aloc, a, x_red, x);

  delete [] a; a = 0;
}



MKL_INT MatMN::VerifyDecompositionPMP(MKL_INT e, cplx *AA, double *w){

  //Cette fonction vérifie la décomposition de la matrice M = P D P^*
  /*-------------------------------------------------------------------------------------- input --------------------------------------------------------------------------------------
    e          : indice de l'élément dont la matrice est associée
    AA         : matrice des vecteurs propres
    w          : les valeurs propres associées aux vecteurs propres AA
    /*------------------------------------------------------------------------------------ output -------------------------------------------------------------------------------------
    r          := 1 si la decomposition correspond et 0 sinon
    /*--------------------------------------------------------------------------------- intermédaire -----------------------------------------------------------------------------------
    iloc       : boucle sur les lignes
    jloc       : boucle sur les colonnes
    k          : boucle sur les colonnes de w et sur les colonnes de AA
    n          : nombre de colonnes de la matrice
    m          : nombre de lignes de la matrice
    taille     := m * n
    adjP       := AA^*
    PD         := AA * w ou D = w et P = AA
    PDP        := AA w AA^*
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, k, s, taille, r;

  taille = m * n;
  cplx *adjP = new cplx[taille];
  cplx *PD = new cplx[taille];
  cplx *PDP = new cplx[taille];
  
  for (iloc = 0; iloc < m; iloc++) { 
    for (jloc = 0; jloc < n; jloc++) {
      //adJP = AA^*
      adjP[jloc * n + iloc] = conj(AA[iloc * n + jloc]);
      
      PD[iloc * n + jloc] = cplx(0.0,0.0);
      
      for(k = 0; k < n; k++) {
	if (jloc == k) {
	  PD[iloc * n + jloc] += AA[iloc * n + k] * w[k];
	}
      }
    }
  }
  
  prodMatMatComplex(m, n, m, n, PD, adjP, PDP);
  
  s = 0;

  cplx val;
  
  for (iloc = 0; iloc < m; iloc++) { 
    for (jloc = 0; jloc < n; jloc++) {
      val = value[iloc * n + jloc] - PDP[iloc * n + jloc];
      if (abs(val) < 1E-10) {
	s++;
      }
    }
  }

  if (s == taille) { r = 1; }

  else { r = 0; }

  delete [] adjP; adjP = 0;
  delete [] PD; PD = 0;
  delete [] PDP; PDP = 0;

  return r;
}



MKL_INT VerifyDecompositionPMP(Maillage &Mesh){

  //Cette fonction vérifie la décomposition de la matrice M = P D P^* sur tous les éléments

  /*--------------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------------
    Mesh      : le maillage
    /*------------------------------------------------------------------------------------- ouput ----------------------------------------------------------------------------------------
    r         := 1 si toutes les décompositions correspondent et 0 sinon 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT s, r, e;
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;

  s = 0;
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    
    MP.Allocate_MatMN();
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(Mesh, e, MP.value);
    

    cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];//Pour stocker la matrice MP et éviter
    //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
    
    double *w = new double[Mesh.ndof];//Pour stocker les valeurs propres
    
    //Décomposition et calcul de la taille du block réduit
    compute_sizeBlocRed(e, AA, w, MP.value, Mesh);

    s += MP.VerifyDecompositionPMP(e, AA, w);

    delete [] MP.value ; MP.value = 0;
    
    delete [] AA ; AA = 0;
    delete [] w ; w = 0;
    
  }

  if (s == Mesh.get_N_elements()) { r = 1; }
  else { r = 0; }
  
  return r;
}


MKL_INT MatMN::VerifyDecompositionPMP(MKL_INT e, cplx *AA, cplx *w){

  //Cette fonction vérifie la décomposition de la matrice M = P D P^*
  /*-------------------------------------------------------------------------------------- input --------------------------------------------------------------------------------------
    e          : indice de l'élément dont la matrice est associée
    AA         : matrice des vecteurs propres
    w          : les valeurs propres associées aux vecteurs propres AA
    /*------------------------------------------------------------------------------------ output -------------------------------------------------------------------------------------
    r          := 1 si la decomposition correspond et 0 sinon
    /*--------------------------------------------------------------------------------- intermédaire -----------------------------------------------------------------------------------
    iloc       : boucle sur les lignes
    jloc       : boucle sur les colonnes
    k          : boucle sur les colonnes de w et sur les colonnes de AA
    n          : nombre de colonnes de la matrice
    m          : nombre de lignes de la matrice
    taille     := m * n
    adjP       := AA^*
    PD         := AA * w ou D = w et P = AA
    PDP        := AA w AA^*
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, k, s, taille, r;

  taille = m * n;
  cplx *adjP = new cplx[taille];
  cplx *PD = new cplx[taille];
  cplx *PDP = new cplx[taille];
  
  for (iloc = 0; iloc < m; iloc++) { 
    for (jloc = 0; jloc < n; jloc++) {
      //adJP = AA^*
      adjP[jloc * n + iloc] = conj(AA[iloc * n + jloc]);
      
      PD[iloc * n + jloc] = cplx(0.0,0.0);
      
      for(k = 0; k < n; k++) {
	if (jloc == k) {
	  PD[iloc * n + jloc] += AA[iloc * n + k] * real(w[k]);
	}
      }
    }
  }
  
  prodMatMatComplex(m, n, m, n, PD, adjP, PDP);
  
  s = 0;

  cplx val;
  
  for (iloc = 0; iloc < m; iloc++) { 
    for (jloc = 0; jloc < n; jloc++) {
      val = value[iloc * n + jloc] - PDP[iloc * n + jloc];
      if (abs(val) < 1E-10) {
	s++;
      }
    }
  }

  if (s == taille) { r = 1; }

  else { r = 0; }

  delete [] adjP; adjP = 0;
  delete [] PD; PD = 0;
  delete [] PDP; PDP = 0;

  return r;
}



MKL_INT MatMN::TEST_Valeurs_propres_et_Vecteurs(MKL_INT e, Maillage &Mesh){

  MKL_INT iloc, jloc;
  
  cplx *AA = new cplx[n * n];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[n];//Pour stocker les valeurs propres
  
  //Décomposition et calcul de la taille du block réduit
  compute_sizeBlocRed(e, AA, w, value, Mesh);

  /*Mesh.elements[e].transport_theMatrix_to_Matlab(n, AA, "data/Matrice_P");

  
    Mesh.elements[e].transport_Vector_to_Matlab(n, w, "data/Vecteur_w");*/
  
  MKL_INT r = VerifyDecompositionPMP(e, AA, w);

  
  delete [] AA ; AA = 0;
  delete [] w ; w = 0;

  return r;
}


/*******************************************************************************************************************************************/
/************************************************* CLASS TEST_PROJECTION ********************************************/


test_projection::test_projection() {
  x1 = 0;
  x2 = 0;
  x3 = 0;
  x4 = 0;
}


void test_projection::Allocate() {
  x1 = new double[3];
  x2 = new double[3];
  x3 = new double[3];
  x4 = new double[3];
}


void test_projection::get_Element(Element &Elem) {
  x1 = Elem.noeud[0]->get_X();
  x2 = Elem.noeud[1]->get_X();
  x3 = Elem.noeud[2]->get_X();
  x4 = Elem.noeud[3]->get_X();
}


double test_projection::compute_volume_tetra_II(double *x2x1, double *x3x1, double *x4x1) {
  
  double *prod_Vecto = produit_vectoriel(x2x1, x3x1);

  double Jac = cblas_ddot(3, prod_Vecto, 1, x4x1, 1);
  
  Jac = abs(Jac);
  double Vol = Jac / 6.0;

  delete [] prod_Vecto; prod_Vecto = 0;
  
  return Vol;
}


cplx test_projection::quad_tetra(Quad &Qd, std::function<cplx (double*)> f, std::function<cplx (double*)> g) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    f, g   : deux fonctions 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
  
   //calcule du volume du tetra
   
   double Vol = compute_volume_tetra_II(x2x1, x3x1, x4x1);
   cout<<"Vol = "<<Vol<<endl;
   double Jac = Vol * 6.0;
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * (x2[j] - x1[j]) + Qd.Xtetra[3*i+1] * (x3[j] - x1[j]) + Qd.Xtetra[3*i+2] * (x4[j] - x1[j]);
    }
    
    I = I + Qd.Wtetra[i] * conj(f(Xquad)) * g(Xquad);
  }

  I = I * Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}


test_projection::~test_projection() {
  delete [] x1; x1 = 0;
  delete [] x2; x2 = 0;
  delete [] x3; x3 = 0;
  delete [] x4; x4 = 0;
}



cplx test_projection::quad_tetra(Quad &Qd, Onde_plane &P1, std::function<cplx (double*)> f) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. 
    f      : une fonction
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }

   //calcule du volume du tetra
   
   double Vol = compute_volume_tetra_II(x2x1, x3x1, x4x1);
   double Jac = Vol * 6.0;
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * (x2[j] - x1[j]) + Qd.Xtetra[3*i+1] * (x3[j] - x1[j]) + Qd.Xtetra[3*i+2] * (x4[j] - x1[j]);
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * f(Xquad);
  }

  I = I * Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}



cplx test_projection::quad_tetra(Quad &Qd, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];


   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }

   //calcule du volume du tetra
   
   double Vol = compute_volume_tetra_II(x2x1, x3x1, x4x1);
   double Jac = Vol * 6.0;
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * (x2[j] - x1[j]) + Qd.Xtetra[3*i+1] * (x3[j] - x1[j]) + Qd.Xtetra[3*i+2] * (x4[j] - x1[j]);
    }
    
    I = I + Qd.Wtetra[i] * conj(P1.pression(Xquad)) * P2.pression(Xquad);
  }

  I = I * Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  
  return I;
}




/***************************************************** FIN TEST_PROJECTION ****************************************************************/









void Maillage::Transforme_Bessel_analytique_Scalar_Solution_To_VTK_File() {

  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Bessel Be;
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Bessel Be;
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		 
		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	      
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Bessel Be;
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P));
		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_points.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_tetra.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_type.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_type.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_offset.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_offset.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_analytique_scalar_Bessel.vtu");
	system("more paraview_value.txt >> paraview_analytique_scalar_Bessel.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_analytique_scalar_Bessel.vtu");
}






void Maillage::Transforme_Bessel_analytique_Velocity_Solution_To_VTK_File_Vector() {
  //Cette fonction ecrit la velocity solution analytique (la fonction de Bessel) dans un fichier VTU. 
 
  
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Bessel Be;
		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		  delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;


		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		    te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		  delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		    te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		  delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   	  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		  delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		   delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		     delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		       delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(Be.V[0]), imag(Be.V[1]), imag(Be.V[2]));

		    delete [] Be.d; Be.d = 0;
		  delete [] Be.V; Be.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Bessel_full_Velocity.vtu");
	system("more paraview_points.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_tetra.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_preambule_type.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_type.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_offset.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Bessel_full_Velocity.vtu");
	system("more paraview_value.txt >> paraview_Bessel_full_Velocity.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Bessel_full_Velocity.vtu");
}








/******************************************************* PROJECTION SUR UN ELEMENT *************************************/

void Maillage::Transforme_Solution_To_VTK_File_OnElement(VecN &ALPHA, MKL_INT e) {

  	int nd = 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 ;

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
      
	for (i = 0; i < Ndiv + 1; i++){
	  for (j = 0; j < Ndiv + 1 - i; j++){
	    for (k = 0; k < Ndiv + 1 - j - i; k++){
	      l = Ndiv - i - j - k;
	      
	      //VOXEL
	      if (l >= 3) {
		Nelt++;
		//Pour i, j, k
		elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P)); 
		
		//Pour i+1, j, k
		elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//Pour i+1, j+1, k
		elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		
		//Pour i, j+1, k
		elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//Pour i, j, k+1
		elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//Pour i+1, j, k+1
		elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//Pour i+1, j+1, k+1
		elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//Pour i, j+1, k+1
		elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		  
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);
		
		fprintf(fic3,"12 \n");
		
		shift += 8;
		fprintf(fic4,"%d\n", shift);
	      }
	      
	      //WEDGE + PYRAMID
	      if (l == 2) {
		
		//WEDGE
		Nelt++;
		
		//i, j, k
		elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i+1, j, k
		elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P)); 
		
		//i, j, k+1
		elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i, j+1, k
		elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P)); 
		
		//i+1, j+1, k
		elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		  
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i, j+1, k+1
		elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		fprintf(fic3,"13 \n");
		
		shift += 6;
		fprintf(fic4,"%d\n", shift);
		
		
		
		//PYRAMID
		Nelt++;
		
		//i+1, j, k
		elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i, j, k+1
		elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i, j+1, k+1
		elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));

		//i+1, j+1, k
		elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));

		//i+1, j, k+1
		elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		  
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		fprintf(fic3,"14 \n");
		
		shift += 5;
		fprintf(fic4,"%d\n", shift);
	      }
	      
	      //TETRA
	      else if (l == 1) {
		Nelt++;
		
		//i, j, k
		elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		//i+1, j, k
		elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		  
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));

		//i, j+1, k
		elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);
		
		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));

		//i, j, k+1
		elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		xx[0] = x; xx[1] = y; xx[2] = z;
		elements[e].combinaison_projectionSolution(ndof, ALPHA.value, xx);
		
		fprintf(fic2, " %g %g %g\n", x, y, z);
		fprintf(fic5, "%g\n",real(elements[e].Pro_P));
		
		fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		fprintf(fic3,"10 \n");
		
		shift += 4;
		fprintf(fic4,"%d\n", shift);
	      }
	      
	      
	      //cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale.vtu");
	system("more paraview_points.txt >> paraview_finale.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale.vtu");
	system("more paraview_tetra.txt >> paraview_finale.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale.vtu");
	system("more paraview_type.txt >> paraview_finale.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale.vtu");
	system("more paraview_offset.txt >> paraview_finale.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale.vtu");
	system("more paraview_value.txt >> paraview_finale.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale.vtu");
}




void print_rows_matrix(cplx *M, MKL_INT n, MKL_INT m, MKL_INT l, const string & arg) {
  MKL_INT i, j;
  if (arg == "row") {
    for( j = 0 ; j < m; j++){
      cout<<"M["<<l * m + j<<"] = "<<M[l * m + j]<<endl;
    }
  }
  else if  (arg == "col") {
    for(i = 0 ; i < n; i++){
      cout<<"M["<<i * m + l<<"] = "<<M[i * m + l]<<endl;
    }
  }
  else{
    cout<<"probleme de mot clef"<<endl;
  }
}






void Maillage::Transforme_ErrorWithBessel_To_VTK_File(VecN &ALPHA) {

  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte)); 

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	     
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
	     
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  Bessel Be;
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(Be.P - elements[e].Pexacte));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Bessel_scalar_error.vtu");
	system("more paraview_points.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_tetra.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_preambule_type.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_type.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_offset.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Bessel_scalar_error.vtu");
	system("more paraview_value.txt >> paraview_Bessel_scalar_error.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Bessel_scalar_error.vtu");
}




/*--------------------------------------------------------------------------------------------------------------------------/*
  /*-------------------------------------------- VTU pour la vitesse ------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------------*/


void Maillage::Transforme_realPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA) {
  //Ecriture des parties réelles des composantes des vecteurs vitesse dans un fichier VTU. Non valable pour la velocity de Bessel 
  //ALPHA : coordonnées globales de la solution dans l'espace d'ondes planes
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z, val;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic7;
	FILE*  fic8;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41x;
	FILE* fic41y;
	FILE* fic41z;
	FILE* fic41abs;
	FILE* fic51x;
	FILE* fic51y;
	FILE* fic51z;
	FILE* fic51abs;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_x.txt","w");
	fic6 = fopen("paraview_value_y.txt","w");
	fic7 = fopen("paraview_value_z.txt","w");
	fic8 = fopen("paraview_value_abs.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  
		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);
		  
		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",real(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",real(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",real(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic7);
	fclose(fic8);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41x = fopen("paraview_preambule_fonction_enX.txt","w");

	fprintf(fic41x, "</DataArray>\n");
	fprintf(fic41x, "</Cells>\n");
	fprintf(fic41x, "<PointData Scalars=\"V\">\n");
	fprintf(fic41x, "<DataArray type=\"Float64\" Name=\"Vx\" format=\"ascii\">\n");
	
	fclose(fic41x);

	fic41y = fopen("paraview_preambule_fonction_enY.txt","w");

	fprintf(fic41y, "</DataArray>\n");
	fprintf(fic41y, "<DataArray type=\"Float64\" Name=\"Vy\" format=\"ascii\">\n");
	
	fclose(fic41y);

	fic41z = fopen("paraview_preambule_fonction_enZ.txt","w");

	fprintf(fic41z, "</DataArray>\n");
	fprintf(fic41z, "<DataArray type=\"Float64\" Name=\"Vz\" format=\"ascii\">\n");
	
	fclose(fic41z);

	fic41abs = fopen("paraview_preambule_fonction_abs.txt","w");

	fprintf(fic41abs, "</DataArray>\n");
	fprintf(fic41abs, "<DataArray type=\"Float64\" Name=\"abs(V)\" format=\"ascii\">\n");
	
	fclose(fic41abs);

	fic51x = fopen("paraview_preambule_finale_enX.txt","w");

	fprintf(fic51x,"</DataArray>\n");
	fprintf(fic51x,"</PointData>\n");
	fprintf(fic51x,"</Piece>\n");
	fprintf(fic51x,"</UnstructuredGrid>\n");
	fprintf(fic51x,"</VTKFile>");
  
	fclose(fic51x);


	
	system("more paraview_preambule.txt > paraview_velocity_real.vtu");
	system("more paraview_points.txt >> paraview_velocity_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_velocity_real.vtu");
	system("more paraview_tetra.txt >> paraview_velocity_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_velocity_real.vtu");
	system("more paraview_type.txt >> paraview_velocity_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_velocity_real.vtu");
	system("more paraview_offset.txt >> paraview_velocity_real.vtu");
	system("more paraview_preambule_fonction_enX.txt >> paraview_velocity_real.vtu");
	system("more paraview_value_x.txt >> paraview_velocity_real.vtu   ");
	system("more paraview_preambule_fonction_enY.txt >> paraview_velocity_real.vtu");
	system("more paraview_value_y.txt >> paraview_velocity_real.vtu   ");

	system("more paraview_preambule_fonction_enZ.txt >> paraview_velocity_real.vtu");
	system("more paraview_value_z.txt >> paraview_velocity_real.vtu   ");

	system("more paraview_preambule_fonction_abs.txt >> paraview_velocity_real.vtu");
	system("more paraview_value_abs.txt >> paraview_velocity_real.vtu   ");
		
	system("more paraview_preambule_finale_enX.txt >> paraview_velocity_real.vtu");
	
	

	
	//system("more paraview_preambule_finale_enY.txt >> paraview_velocity_real.vtu");
}





void Maillage::Transforme_imaginaryPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA) {
  //Ecriture des parties imaginaires des composantes des vecteurs vitesse dans un fichier VTU
  //ALPHA : coordonnées globales de la solution dans l'espace d'ondes planes
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z, val;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic7;
	FILE*  fic8;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41x;
	FILE* fic41y;
	FILE* fic41z;
	FILE* fic41abs;
	FILE* fic51x;
	FILE* fic51y;
	FILE* fic51z;
	FILE* fic51abs;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_x.txt","w");
	fic6 = fopen("paraview_value_y.txt","w");
	fic7 = fopen("paraview_value_z.txt","w");
	fic8 = fopen("paraview_value_abs.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  
		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);
		  
		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(elements[e].Vexacte[0] * elements[e].Vexacte[0] + elements[e].Vexacte[1] * elements[e].Vexacte[1] + elements[e].Vexacte[2] * elements[e].Vexacte[2]));
		  
		  fprintf(fic5, "%g\n",imag(elements[e].Vexacte[0]));
		  fprintf(fic6, "%g\n",imag(elements[e].Vexacte[1]));
		  fprintf(fic7, "%g\n",imag(elements[e].Vexacte[2]));
		  fprintf(fic8, "%g\n",val);
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic7);
	fclose(fic8);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41x = fopen("paraview_preambule_fonction_enX.txt","w");

	fprintf(fic41x, "</DataArray>\n");
	fprintf(fic41x, "</Cells>\n");
	fprintf(fic41x, "<PointData Scalars=\"V\">\n");
	fprintf(fic41x, "<DataArray type=\"Float64\" Name=\"Vx\" format=\"ascii\">\n");
	
	fclose(fic41x);

	fic41y = fopen("paraview_preambule_fonction_enY.txt","w");

	fprintf(fic41y, "</DataArray>\n");
	fprintf(fic41y, "<DataArray type=\"Float64\" Name=\"Vy\" format=\"ascii\">\n");
	
	fclose(fic41y);

	fic41z = fopen("paraview_preambule_fonction_enZ.txt","w");

	fprintf(fic41z, "</DataArray>\n");
	fprintf(fic41z, "<DataArray type=\"Float64\" Name=\"Vz\" format=\"ascii\">\n");
	
	fclose(fic41z);

	fic41abs = fopen("paraview_preambule_fonction_abs.txt","w");

	fprintf(fic41abs, "</DataArray>\n");
	fprintf(fic41abs, "<DataArray type=\"Float64\" Name=\"abs(V)\" format=\"ascii\">\n");
	
	fclose(fic41abs);

	fic51x = fopen("paraview_preambule_finale_enX.txt","w");

	fprintf(fic51x,"</DataArray>\n");
	fprintf(fic51x,"</PointData>\n");
	fprintf(fic51x,"</Piece>\n");
	fprintf(fic51x,"</UnstructuredGrid>\n");
	fprintf(fic51x,"</VTKFile>");
  
	fclose(fic51x);


	
	system("more paraview_preambule.txt > paraview_velocity_imaginary.vtu");
	system("more paraview_points.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_tetra.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_preambule_type.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_type.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_preambule_offset.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_offset.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_preambule_fonction_enX.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_value_x.txt >> paraview_velocity_imaginary.vtu   ");
	system("more paraview_preambule_fonction_enY.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_value_y.txt >> paraview_velocity_imaginary.vtu   ");

	system("more paraview_preambule_fonction_enZ.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_value_z.txt >> paraview_velocity_imaginary.vtu   ");

	system("more paraview_preambule_fonction_abs.txt >> paraview_velocity_imaginary.vtu");
	system("more paraview_value_abs.txt >> paraview_velocity_imaginary.vtu   ");
		
	system("more paraview_preambule_finale_enX.txt >> paraview_velocity_imaginary.vtu");
	
	

	
	//system("more paraview_preambule_finale_enY.txt >> paraview_velocity_imaginary.vtu");
}






/*--------------------------------------------------------------- Full Vector -----------------------------------------------*/


void Maillage::Transforme_realPartOfSolution_Velocity_To_VTK_File_Vector(VecN &ALPHA) {
  //Cette fonction ecrit la partie réelle de la velocity solution numerique dans un fichier VTU. Non valable pour le cas d'une fonction de Bessel de première espèce j0
  //ALPHA : coordonnées globales de la solution dans l'espace de Trefftz.
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2])); 

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2])); 

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Vector_Velocity_real.vtu");
	system("more paraview_points.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_tetra.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_type.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_offset.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Vector_Velocity_real.vtu");
	system("more paraview_value.txt >> paraview_Vector_Velocity_real.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Vector_Velocity_real.vtu");
}





void Maillage::Transforme_imaginaryPartOfSolution_Velocity_To_VTK_File_Vector(VecN &ALPHA) {
  //Cette fonction ecrit la partie réelle de la velocity solution numerique dans un fichier VTU. 
  //ALPHA : coordonnées globales de la solution dans l'espace de Trefftz.
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2])); 

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2])); 

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(elements[e].Vexacte[0]), imag(elements[e].Vexacte[1]), imag(elements[e].Vexacte[2]));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_points.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_tetra.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_preambule_type.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_type.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_offset.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Vector_Velocity_imaginary.vtu");
	system("more paraview_value.txt >> paraview_Vector_Velocity_imaginary.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Vector_Velocity_imaginary.vtu");
}



// /*--------------------------------------------------------------------------------------------- Erreur ------------------------------------------------------------------------------------------------*/


void Maillage::Transforme_BesselError_Vitesse_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA) {

  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z, val;
	cplx *W = new cplx[3];
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic7;
	FILE*  fic8;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41x;
	FILE* fic41y;
	FILE* fic41z;
	FILE* fic41abs;
	FILE* fic51x;
	FILE* fic51y;
	FILE* fic51z;
	FILE* fic51abs;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_x.txt","w");
	fic6 = fopen("paraview_value_y.txt","w");
	fic7 = fopen("paraview_value_z.txt","w");
	fic8 = fopen("paraview_value_abs.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	      
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	     
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	      
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

	      
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   
		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	     
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		 
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		 
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		   
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic7);
	fclose(fic8);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41x = fopen("paraview_preambule_fonction_enX.txt","w");

	fprintf(fic41x, "</DataArray>\n");
	fprintf(fic41x, "</Cells>\n");
	fprintf(fic41x, "<PointData Scalars=\"V\">\n");
	fprintf(fic41x, "<DataArray type=\"Float64\" Name=\"Vx\" format=\"ascii\">\n");
	
	fclose(fic41x);

	fic41y = fopen("paraview_preambule_fonction_enY.txt","w");

	fprintf(fic41y, "</DataArray>\n");
	fprintf(fic41y, "<DataArray type=\"Float64\" Name=\"Vy\" format=\"ascii\">\n");
	
	fclose(fic41y);

	fic41z = fopen("paraview_preambule_fonction_enZ.txt","w");

	fprintf(fic41z, "</DataArray>\n");
	fprintf(fic41z, "<DataArray type=\"Float64\" Name=\"Vz\" format=\"ascii\">\n");
	
	fclose(fic41z);

	fic41abs = fopen("paraview_preambule_fonction_abs.txt","w");

	fprintf(fic41abs, "</DataArray>\n");
	fprintf(fic41abs, "<DataArray type=\"Float64\" Name=\"abs(V)\" format=\"ascii\">\n");
	
	fclose(fic41abs);

	fic51x = fopen("paraview_preambule_finale_enX.txt","w");

	fprintf(fic51x,"</DataArray>\n");
	fprintf(fic51x,"</PointData>\n");
	fprintf(fic51x,"</Piece>\n");
	fprintf(fic51x,"</UnstructuredGrid>\n");
	fprintf(fic51x,"</VTKFile>");
  
	fclose(fic51x);


	
	system("more paraview_preambule.txt > paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_points.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_tetra.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_type.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_offset.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_preambule_fonction_enX.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_value_x.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu   ");
	system("more paraview_preambule_fonction_enY.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_value_y.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu   ");

	system("more paraview_preambule_fonction_enZ.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_value_z.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu   ");

	system("more paraview_preambule_fonction_abs.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	system("more paraview_value_abs.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu   ");
		
	system("more paraview_preambule_finale_enX.txt >> paraview_finale_ErrorBessel_VxVyVz.vtu");
	
	free(W);
}




void Maillage::Transforme_BesselError_Vitesse_To_VTK_File_Vector(VecN &ALPHA) {

  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	cplx *W = new cplx[3];
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2])); 

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Bessel Be;
		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (Be.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (Be.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (Be.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] Be.d; Be.d = 0; 
		  delete [] Be.V; Be.V = 0;

		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_points.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_tetra.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_type.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_offset.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_ErrorBessel_V.vtu");
	system("more paraview_value.txt >> paraview_finale_ErrorBessel_V.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_ErrorBessel_V.vtu");

	free(W);
}



// /*---------------------------------------------------------------------------------------------------------------------------------*/
// /*------------------------------------------------------------- Solution avec Hankel -----------------------------------------------*/
// /*----------------------------------------------------------------------------------------------------------------------------------*/

void Maillage::Transforme_realPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File() {
 //Cette fonction ecrit la partie réelle de la solution analytique scalaire (la fonction de Hankel) sur un fichier VTU.
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		 
		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	      
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hankel_analytique_real.vtu");
	system("more paraview_points.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_tetra.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_type.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_offset.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_hankel_analytique_real.vtu");
	system("more paraview_value.txt >> paraview_hankel_analytique_real.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_hankel_analytique_real.vtu");
}





void Maillage::Transforme_imaginaryPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File() {
  //Cette fonction ecrit la partie imaginaire de la solution analytique scalaire (la fonction de Hankel) sur un fichier VTU.
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		 
		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	      
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(he.P));
		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hankel_analytique_imag.vtu");
	system("more paraview_points.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_tetra.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_preambule_type.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_type.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_offset.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_hankel_analytique_imag.vtu");
	system("more paraview_value.txt >> paraview_hankel_analytique_imag.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_hankel_analytique_imag.vtu");
}





void Maillage::Transforme_imaginaryPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector() {
  //Cette fonction ecrit la velocity solution analytique (la fonction de Bessel) dans un fichier VTU. 
 
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;


		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		    te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		    te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   	  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		     delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		       delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(he.V[0]), imag(he.V[1]), imag(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_points.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_tetra.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_preambule_type.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_type.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_offset.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Hankel_full_imag_Velocity.vtu");
	system("more paraview_value.txt >> paraview_Hankel_full_imag_Velocity.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Hankel_full_imag_Velocity.vtu");
}




void Maillage::Transforme_realPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector() {
  //Cette fonction ecrit la velocity solution analytique (la fonction de Bessel) dans un fichier VTU. 
 
  
  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Hankel he;
		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;


		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		    te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		    te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   	  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		  delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		   delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		     delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		       delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   te=he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(he.V[0]), real(he.V[1]), real(he.V[2]));

		    delete [] he.d; he.d = 0;
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_points.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_tetra.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_preambule_type.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_type.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_offset.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Hankel_full_real_Velocity.vtu");
	system("more paraview_value.txt >> paraview_Hankel_full_real_Velocity.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Hankel_full_real_Velocity.vtu");
}



/*------------------------------------------------------------ Error With Hankel ----------------------------------------------------------*/

void Maillage::Transforme_ErrorWithHankel_To_VTK_File(VecN &ALPHA) {

  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte)); 

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	     
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		   
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
	     
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  Hankel he;
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(he.P - elements[e].Pexacte));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_Error_Hankel.vtu");
	system("more paraview_points.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_tetra.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_type.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_offset.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_Error_Hankel.vtu");
	system("more paraview_value.txt >> paraview_finale_Error_Hankel.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_Error_Hankel.vtu");
}






// /*------------------------------------------------------------ Error velocity with Hankel ----------------------------------------*/


void Maillage::Transforme_HankelError_Vitesse_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA) {

  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z, val;
	cplx *W = new cplx[3];
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic7;
	FILE*  fic8;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41x;
	FILE* fic41y;
	FILE* fic41z;
	FILE* fic41abs;
	FILE* fic51x;
	FILE* fic51y;
	FILE* fic51z;
	FILE* fic51abs;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_x.txt","w");
	fic6 = fopen("paraview_value_y.txt","w");
	fic7 = fopen("paraview_value_z.txt","w");
	fic8 = fopen("paraview_value_abs.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	      
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	     
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	      
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		  W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		  W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		  W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);

		  val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		  
		  fprintf(fic5, "%g\n",imag(W[0]));
		  fprintf(fic6, "%g\n",imag(W[1]));
		  fprintf(fic7, "%g\n",imag(W[2]));
		  fprintf(fic8, "%g\n",val);

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

	      
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   
		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	     
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		 
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

	       
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		 
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		
		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		   
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		   
		   fprintf(fic2, " %g %g %g\n", x, y, z);
		   
		   val = sqrt(abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]));
		   
		   fprintf(fic5, "%g\n",imag(W[0]));
		   fprintf(fic6, "%g\n",imag(W[1]));
		   fprintf(fic7, "%g\n",imag(W[2]));
		   fprintf(fic8, "%g\n",val);

		     delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		   
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic7);
	fclose(fic8);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41x = fopen("paraview_preambule_fonction_enX.txt","w");

	fprintf(fic41x, "</DataArray>\n");
	fprintf(fic41x, "</Cells>\n");
	fprintf(fic41x, "<PointData Scalars=\"V\">\n");
	fprintf(fic41x, "<DataArray type=\"Float64\" Name=\"Vx\" format=\"ascii\">\n");
	
	fclose(fic41x);

	fic41y = fopen("paraview_preambule_fonction_enY.txt","w");

	fprintf(fic41y, "</DataArray>\n");
	fprintf(fic41y, "<DataArray type=\"Float64\" Name=\"Vy\" format=\"ascii\">\n");
	
	fclose(fic41y);

	fic41z = fopen("paraview_preambule_fonction_enZ.txt","w");

	fprintf(fic41z, "</DataArray>\n");
	fprintf(fic41z, "<DataArray type=\"Float64\" Name=\"Vz\" format=\"ascii\">\n");
	
	fclose(fic41z);

	fic41abs = fopen("paraview_preambule_fonction_abs.txt","w");

	fprintf(fic41abs, "</DataArray>\n");
	fprintf(fic41abs, "<DataArray type=\"Float64\" Name=\"abs(V)\" format=\"ascii\">\n");
	
	fclose(fic41abs);

	fic51x = fopen("paraview_preambule_finale_enX.txt","w");

	fprintf(fic51x,"</DataArray>\n");
	fprintf(fic51x,"</PointData>\n");
	fprintf(fic51x,"</Piece>\n");
	fprintf(fic51x,"</UnstructuredGrid>\n");
	fprintf(fic51x,"</VTKFile>");
  
	fclose(fic51x);


	
	system("more paraview_preambule.txt > paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_points.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_tetra.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_type.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_offset.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_preambule_fonction_enX.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_value_x.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu   ");
	system("more paraview_preambule_fonction_enY.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_value_y.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu   ");

	system("more paraview_preambule_fonction_enZ.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_value_z.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu   ");

	system("more paraview_preambule_fonction_abs.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	system("more paraview_value_abs.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu   ");
		
	system("more paraview_preambule_finale_enX.txt >> paraview_finale_ErrorHankel_VxVyVz.vtu");
	
	free(W);
}









// /*------------------------------------------------------------- Vector velocity VTU ----------------------------------*/


void Maillage::Transforme_HankelError_Vitesse_To_VTK_File_Vector(VecN &ALPHA) {

  	MKL_INT nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();

	double xx[3];
	double x, y, z;
	cplx *W = new cplx[3];
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2])); 

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Hankel he;
		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  he.compute_P_and_V(elements[e].Ac,xx,X0);
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);

		   W[0] =  (he.V[0] - elements[e].Vexacte[0]);
		   W[1] =  (he.V[1] - elements[e].Vexacte[1]);
		   W[2] =  (he.V[2] - elements[e].Vexacte[2]);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",imag(W[0]), imag(W[1]), imag(W[2]));

		    delete [] he.d; he.d = 0; 
		  delete [] he.V; he.V = 0;

		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_points.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_tetra.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_type.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_offset.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_ErrorHankel_V.vtu");
	system("more paraview_value.txt >> paraview_finale_ErrorHankel_V.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_ErrorHankel_V.vtu");

	free(W);
}


// void Maillage::Analytique_Solution_To_VTK(char arg1[], char arg2[], char arg3[]) {
//   if (strcmp(arg1, "bessel") == 0){
//     if (strcmp(arg2, "p") == 0){
//       Transforme_Bessel_analytique_Scalar_Solution_To_VTK_File();
//     }
//     else  if (strcmp(arg2, "v") == 0){
//       Transforme_Bessel_analytique_Velocity_Solution_To_VTK_File_Vector();
//     }

//     else {
//       cout<<" probleme de mot clef "<<endl;
//     }
//   }
  
  
//   else if (strcmp(arg1, "hankel") == 0){
    
//     if (strcmp(arg2, "p") == 0){
//         if (strcmp(arg3, "real") == 0){
// 	  Transforme_realPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File();
// 	}
// 	else if (strcmp(arg3, "imag") == 0){
// 	  Transforme_imaginaryPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File();
// 	}
// 	else {
// 	  cout<<" probleme de mot clef "<<endl;
// 	}
//     }
    
//     else if (strcmp(arg2, "v") == 0){
//       if (strcmp(arg3, "real") == 0){
// 	Transforme_realPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector();
//       }
//       else if (strcmp(arg3, "imag") == 0){
// 	Transforme_imaginaryPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector();
//       }
//       else {
// 	cout<<" probleme de mot clef "<<endl;
//       }
//     }
//   }
    
//   else {
//     cout<<" probleme de mot clef "<<endl;
//   }
// }




// void Maillage::numerical_Solution_To_VTK(VecN ALPHA, char arg1[], char arg2[], char arg3[]) {
  
//   if (strcmp(arg1, "p") == 0){
    
//     if (strcmp(arg3, "real") == 0){
//       Transforme_realPartOfSolution_To_VTK_File(ALPHA);
//     }
//     else if (strcmp(arg3, "imag") == 0){
//       Transforme_imaginaryPartOfSolution_To_VTK_File(ALPHA);
//     }

//     else {
//       cout<<" probleme de mot clef "<<endl;
//     }
//   }

  
  
//   else if (strcmp(arg1, "v") == 0){
    
//     if (strcmp(arg2, "piece") == 0){
      
//       if (strcmp(arg3, "real") == 0){
// 	Transforme_realPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(ALPHA);
//       }
//       else if (strcmp(arg3, "imag") == 0){
// 	Transforme_imaginaryPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(ALPHA);
//       }
//       else {
// 	cout<<" probleme de mot clef "<<endl;
//       }
      
//     }
    
//     else if (strcmp(arg2, "full") == 0){
      
//       if (strcmp(arg3, "real") == 0){
// 	Transforme_realPartOfSolution_Velocity_To_VTK_File_Vector(ALPHA);
//       }
//       else if (strcmp(arg3, "imag") == 0){
// 	Transforme_imaginaryPartOfSolution_Velocity_To_VTK_File_Vector(ALPHA);
//       }

//       else {
// 	cout<<" probleme de mot clef "<<endl;
//       }
      
//     }

//     else {
//       cout<<" probleme de mot clef "<<endl;
//     }
    
//   }

  
//   else {
//     cout<<" probleme de mot clef "<<endl;
//   }
// }

   
 
// MKL_INT determine_position(double *x, double L) {
  
//   if ((x[0] <= L * 0.5) && (x[1] <= L) && (x[2] <= L * 0.5)){
//     return 0;
//   }
//   else if ((x[0] <= L * 0.5) && (x[1] <= L * 0.5) && (x[2] <= L)){
//     return 0;
//   }
//   else {
//     return 1;
//   }
// }



// MKL_INT determine_position(Face &F, double L){

//   MKL_INT a = 0;
//   for(MKL_INT i = 0; i < 3; i++){
//      a +=  determine_position(F.noeud[i]->get_X(), L);
//   }
  
//   return a;
// }


double norme_L2(MKL_INT e, VecN &X, Maillage &Mesh, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I = cplx(0.0,0.0);
  cplx val ;
  cplx *x;
  double s;
  
  X.get_F_elem(Mesh.ndof, e, &x);
  
    
  for(iloc = 0; iloc < Mesh.ndof; iloc++){
    for(jloc = 0; jloc < Mesh.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(Mesh.elements[e].face[j], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
      }
      
      I += x[jloc] * conj(x[iloc]) * val;
    }
  }
 
  s = abs(I);

  return s;
}


double norme_L2(VecN &X, Maillage &Mesh, const char chaine[]) {
  double s;
  MKL_INT e;
  s = 0.0;
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    s += norme_L2(e, X, Mesh, chaine);
  }

  return sqrt(s);
}


double norme_L2_Vitesse(MKL_INT e, VecN &X, Maillage &Mesh, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I, val ;
  cplx *x;
  double r, s;
  
  X.get_F_elem(Mesh.ndof, e, &x);

  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < Mesh.ndof; iloc++){
    for(jloc = 0; jloc < Mesh.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(Mesh.elements[e].face[j], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
      }
      
      r = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].OP[iloc].Ad, 1) ;

      r = r * Mesh.elements[e].OP[jloc].ksurOmega * Mesh.elements[e].OP[iloc].ksurOmega;
      
      I += x[jloc] * conj(x[iloc]) * r * val;
    }
  }
  
  s = abs(I);

  return s;
}


double norme_L2_Vitesse(VecN &X, Maillage &Mesh, const char chaine[]) {
  double s;
  MKL_INT e;
  s = 0.0;
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    s += norme_L2_Vitesse(e, X, Mesh, chaine);
  }
  
  s = sqrt(s);

  return s;

}
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

cplx L2_norm_OnOneElementByGauss_Hetero(MKL_INT e, cplx *ALPHA, Maillage &Mesh) {

  //Ici, on calcule la norme L2 de en pression de u_h - u_ex avec u_h  = sum_{i} alpha[i] * P_iloc  et u_ex la solution analytique
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    e       : indice de l'élément
    ALPHA   : solution numérique locale sur e
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    P       : pression analytique
    V       : vitesse analytique
    Mesh.elements[e].P_exacte : la pression numérique associée à l'élément e
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  
  
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de la solution numerique en Xquad
    Mesh.elements[e].combinaison_projectionSolution(Mesh.ndof, ALPHA, Xquad);
    
    //Evaluation de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);
    
    I = I + Mesh.Qd.Wtetra[i] * abs((P - Mesh.elements[e].Pro_P) * (P - Mesh.elements[e].Pro_P));
  }
  
  I = I * Mesh.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] V; V = 0;
  
  return I;
}


cplx L2_norm_OnOneElementByGauss_Hetero(MKL_INT e, Maillage &Mesh) {

  //Cette fonction calcule la norme L2 sur l'élément e de la solution analytique dans le cas d'une onde incidente plane

  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    e       : indice de l'élémen
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    P       : pression analytique
    V       : vitesse analytique
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  
  
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);
    
    I = I + Mesh.Qd.Wtetra[i] * abs(P * P);
  }
  
  I = I * Mesh.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] V; V = 0;
  
  return I;
}


cplx L2_norm_OfanalytiquePlanePressureByGauss_Hetero(Maillage &Mesh) {

  //cette fonction calcule la norme L2 sur de la solution analytique dans tout le domaine dans le cas d'une onde plane 
  cplx I = cplx(0.0, 0.0);
  
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++) {

    I += L2_norm_OnOneElementByGauss_Hetero(e, Mesh);

  }

  I = sqrt(I);
  
  return I;
}



cplx L2_norm_OfErrorPlanePressureByGauss_Hetero(VecN &ALPHA, Maillage &Mesh) {

   //Ici, on calcule la norme L2 de en pression de u_h - u_ex dans tout le domaine avec u_h  = sum_{i} alpha[i] * P_iloc  et u_ex la solution analytique
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    ALPHA   : coordonnées de la solution numérique 
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    u       : tableau pour clôner les coordonnées de la solution numérique localement
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

  cplx I = cplx(0.0, 0.0);

  cplx *u;
  
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++) {

    ALPHA.get_F_elem(Mesh.ndof, e, &u);

    I += L2_norm_OnOneElementByGauss_Hetero(e, u, Mesh);

  }

  I = sqrt(I / L2_norm_OfanalytiquePlanePressureByGauss_Hetero(Mesh));
  
  return I;
}


/*------------------------------------------------------------------------------- NORME EN VITESSE --------------------------------------------------------------------------------*/

cplx L2_norm_OfVelocity_OnOneElementByGauss_Hetero(MKL_INT e, cplx *ALPHA, Maillage &Mesh) {

  //Ici, on calcule la norme L2 en vitesse de u_h - u_ex avec u_h  = sum_{i} alpha[i] * V_iloc  et u_ex la solution analytique en Vitesse
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    e       : indice de l'élément
    ALPHA   : solution numérique locale sur e
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    P       : pression analytique
    V       : vitesse analytique
    Mesh.elements[e].Vexacte : la Vitesse numérique associée à l'élément e
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  cplx W[3];// V - Mesh.elements[e].Vexacte
  
  
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de la solution numerique en Xquad
    Mesh.elements[e].combinaison_projectionSolution(Mesh.ndof, ALPHA, Xquad);
    
    //Evaluation de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);

    for (j = 0; j < 3; j++){
      W[j] = V[j] - Mesh.elements[e].Pro_V[j];
    }
    
    I = I + Mesh.Qd.Wtetra[i] * abs(W[0] * W[0] + W[1] * W[1] + W[2] * W[2]);
  }
  
  I = I * Mesh.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] V; V = 0;
  
  return I;
}


cplx L2_norm_OfVelocity_OnOneElementByGauss_Hetero(MKL_INT e, Maillage &Mesh) {

  //Cette fonction calcule la norme L2 de la Vitesse analytique asscoiée à l'élément e dans le cas d'une onde incidente plane

  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    e       : indice de l'élément
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    P       : pression analytique
    V       : vitesse analytique
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  
  
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);
    
    I = I + Mesh.Qd.Wtetra[i] * abs(V[0] * V[0] + V[1] * V[1] + V[2] * V[2]);
  }
  
  I = I * Mesh.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] V; V = 0;
  
  return I;
}


cplx L2_norm_OfanalytiquePlaneVelocityByGauss_Hetero(Maillage &Mesh) {

  //cette fonction calcule la norme L2 de la vitesse analytique dans tout le domaine dans le cas d'une onde plane 
  cplx I = cplx(0.0, 0.0);
  
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++) {

    I += L2_norm_OfVelocity_OnOneElementByGauss_Hetero(e, Mesh);

  }

  I = sqrt(I);
  
  return I;
}


cplx L2_norm_OfErrorPlaneVelocityByGauss_Hetero(VecN &ALPHA, Maillage &Mesh) {

  //Ici, on calcule la norme L2 de en vitesse de u_h - u_ex dans tout le domaine avec u_h  = sum_{i} alpha[i] * P_iloc  et u_ex la solution analytique
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    ALPHA   : coordonnées de la solution numérique 
    Mesh    : le maillage 
    /*-------------------------------------------------------------------------------------- output ----------------------------------------------------------------------------------------
    I       : la norme L2 de ||u_h - u_ex||^2
    /*----------------------------------------------------------------------------------- intermédiare -----------------------------------------------------------------------------------
    u       : tableau pour clôner les coordonnées de la solution numérique localement
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  
  cplx I = cplx(0.0, 0.0);

  cplx *u;
  
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++) {

    ALPHA.get_F_elem(Mesh.ndof, e, &u);

    I += L2_norm_OfVelocity_OnOneElementByGauss_Hetero(e, u, Mesh);

  }

  I = sqrt(I / L2_norm_OfanalytiquePlaneVelocityByGauss_Hetero(Mesh));
  
  return I;
}


/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/

void collection_Solutions_locales_selfDecompose(VecN &U, const string & choice, const string & arg, const string & chaine, A_skyline &P_red, Maillage &Mesh) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    arg     : chaîne de caractères pour définir le cas pri en charge. 
              arg = "LU" resolution par LU, sinon, resolution par Cholesky

    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection

    chaine  : chaine de caractère qui nous permet de déterminer le cas à traiter. 
               - Pour chaine = "plane" , on a traitera le cas d'une onde plane incidente
               - Pour chaine = "bessel", c'est le cas de la fonction de Bessel
                  de première espéce j0 considèrée comme onde incidente
	       - Pour arg = "hankel", c'est le cas de la fonction de exp(ikr)/4*pi*r
                  considèrée comme onde incidente
       
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    Mesh    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc, taille;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;

  Mesh.taille_red = 0;

  taille = Mesh.ndof * Mesh.ndof;

  cplx *AA = new cplx[taille];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[Mesh.ndof];//Pour stocker les valeurs propres
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    
    MP.Allocate_MatMN();

    initialize_tab(taille, AA);//initialisation de AA à 0

    initialize_tab(Mesh.ndof, w);//initialisation de w à 0

    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(Mesh.ndof, e, &B);
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(Mesh, e, MP.value);
    
    //Construction du vecteur second membre correspondant à l'élément e
    BuildVector_projection(Mesh, e, B, chaine);


    //Décomposition et calcul de la taille du block réduit
    compute_sizeBlocRed(e, AA, w, MP.value, Mesh);

    //stockage de la taille des blocs dans le tableau SIZE (version MUMPS)
    Mesh.SIZE_LOC[e] = Mesh.taille_red;
    
    Mesh.taille_red += Mesh.elements[e].Nred_Pro;

    //clônage de P_red sur e
    cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();

    //stockage des Nred_Pro plus grandes valeurs propres
    cplx *V = new cplx[Mesh.elements[e].Nred_Pro];

    //Décomposition de la matrice MP et stockage de PP_red et des valeurs propres
    DecompositionDeBloc(e, MP.value, PP_red, V, Mesh);
    
    
    MatMN MP_red;
    MP_red.m = Mesh.elements[e].Nred_Pro;
    MP_red.n = Mesh.elements[e].Nred_Pro;
    
    MP_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = Mesh.elements[e].Nred_Pro;
    u_red.Allocate();

    //Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red 
    compute_A_red(e, choice, MP, PP_red, MP_red, Mesh);

    //u_red = conj(trans(PP_red)) * B
    compute_F_red(e, choice, B, PP_red, u_red.value, Mesh);

    if (arg == "LU"){
    
      //résolution du système linéaire MP_red x_red = u_red correspondante par la méthode LU
      MP_red.Solve_by_LU(u_red);
      
      //On retrouve la solution dans l'ancienne base B = PP_red u_red
      reverseTo_originalVector(e, choice, B, PP_red, u_red.value, Mesh);
      
    }
    
    else {
      
      cplx *x_red = new cplx[u_red.n]; //pour stocker la solution réduite
      
      Solve_By_CHOLESKY(u_red.n, MP_red.value, u_red.value, x_red);
      
      //On retrouve la solution dans l'ancienne base B = PP_red x_red
      reverseTo_originalVector(e, choice, B, PP_red, x_red, Mesh);

      delete [] x_red; x_red = 0;
    }

    //libération de la mémoire 
    delete [] MP.value ; MP.value = 0;

    delete [] V; V = 0;
    delete [] MP_red.value; MP_red.value = 0;
    delete [] u_red.value; u_red.value = 0;
  }

  delete [] AA ; AA = 0;
  delete [] w ; w = 0;
}


/*-------------------------------------------------------------------------------------------------------------------------*/
cplx integrale_tetra(Element &E, MKL_INT iloc, MKL_INT jloc){
  cplx I = cplx(0.0, 0.0);
  cplx alpha_1_2, alpha_1_3, alpha_1_4;
  cplx alpha_2_1, alpha_2_3, alpha_2_4;
  cplx alpha_3_1, alpha_3_2, alpha_3_4;
  cplx alpha_4_1, alpha_4_2, alpha_4_3;
  cplx x1dk, x2dk, x3dk, x4dk;

  double *x1, *x2, *x3, *x4;
  x1 = E.noeud[0]->get_X();
  x2 = E.noeud[1]->get_X();
  x3 = E.noeud[2]->get_X();
  x4 = E.noeud[3]->get_X();

  double *x1x2 = new double[3];// x1 - x2
  double *x1x3 = new double[3];// x1 - x3
  double *x1x4 = new double[3];// x1 - x4

  double *x2x3 = new double[3];// x2 - x3
  double *x2x4 = new double[3];// x2 - x4
  double *x3x4 = new double[3];// x3 - x4

  double *k = new double[3];
  
  MKL_INT i;
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];

    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    k[i] = E.OP[jloc].K[i] - E.OP[iloc].K[i];
  }
  alpha_1_2 = cplx(0.0, 1.0) * cblas_ddot(3, x1x2, 1, k, 1);//i * (x1 - x2) \cdot k
  alpha_1_3 = cplx(0.0, 1.0) * cblas_ddot(3, x1x3, 1, k, 1);//i * (x1 - x3) \cdot k
  alpha_1_4 = cplx(0.0, 1.0) * cblas_ddot(3, x1x4, 1, k, 1);//i * (x1 - x4) \cdot k

  alpha_2_3 = cplx(0.0, 1.0) * cblas_ddot(3, x2x3, 1, k, 1);//i * (x2 - x3) \cdot k
  alpha_2_4 = cplx(0.0, 1.0) * cblas_ddot(3, x2x4, 1, k, 1);//i * (x2 - x4) \cdot k
  alpha_3_4 = cplx(0.0, 1.0) * cblas_ddot(3, x3x4, 1, k, 1);//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;
  
  double epsi = 1E-12;

  double UnSurEPSI = 1.0 / epsi;

 
  if (abs(alpha_1_4)* UnSurEPSI < 1){
    if (abs(alpha_2_4)* UnSurEPSI < 1 ){
      if(abs(alpha_3_4) / epsi < 1){
	cout<<"cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 = 0"<<endl;
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	
	I = E.Vol * exp(x4dk); 
      }

      else{
	cout<<"cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 neq 0"<<endl;
	x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	I = exp(x3dk) - (alpha_3_4 * alpha_3_4 + 2.0 * alpha_3_4 + 2.0) * 0.5  * exp(x4dk);
	I = (I * E.Jac) /  (alpha_3_4 * alpha_3_4 * alpha_3_4) ;
      }
    }

    else{
      if(abs(alpha_3_4) / epsi < 1){
	cout<<"cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0"<<endl;
	x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	I = exp(x2dk) - (alpha_2_4 * alpha_2_4 + 2.0 * alpha_2_4 + 2.0) * 0.5  * exp(x4dk);
	I = (I * E.Jac) /  (alpha_2_4 * alpha_2_4 * alpha_2_4) ;
      }
      
      else{
	cout<<"cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0"<<endl;
	if (abs(alpha_2_3)* UnSurEPSI < 1){
	  cout<<"cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0"<<endl;
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	  I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  cout<<"cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0"<<endl;
	  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k

	  I = ((alpha_2_4  + alpha_3_4 + alpha_2_4 * alpha_3_4) / (alpha_2_4 * alpha_2_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_2)
	    + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_3);
	  I = (I * E.Jac);
	}
      }
      
    }

    
  }
  
  else{
    if (abs(alpha_2_4)* UnSurEPSI < 1 ){
      if(abs(alpha_3_4) / epsi < 1){
	cout<<"cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0"<<endl;
	x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	
	I = exp(x1dk) - (alpha_1_4 * alpha_1_4 + 2.0 * alpha_1_4 + 2.0) * 0.5  * exp(x4dk);
	I = (I * E.Jac) /  (alpha_1_4 * alpha_1_4 * alpha_1_4) ;
      }
      else{
	cout<<"cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0"<<endl;
	if(abs(alpha_1_3) / epsi < 1){
	  cout<<"cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0 alpha_1_3 = 0"<<endl;
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x1 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	  I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  cout<<"cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0 alpha_1_3 != 0"<<endl;
	  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = ((alpha_1_4 + alpha_3_4 + alpha_1_4 * alpha_3_4) / (alpha_1_4 * alpha_1_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_1)
	   +  exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_3);
	  I = I * E.Jac;
	}
      } 
    }
    else{
      if(abs(alpha_3_4) / epsi < 1){
	cout<<"cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0"<<endl;
	if(abs(alpha_1_2) / epsi < 1){
	  cout<<"Si en plus alpha_1_2 = 0"<<endl;
	  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = (alpha_1_4 + 2.0) * exp(x4dk) + (alpha_1_4 - 2.0) * exp(x1dk);
	  I = (I * E.Jac) / (alpha_1_4 * alpha_1_4 * alpha_1_4);
	}
	else{
	  cout<<"Si en plus alpha_1_2 neq 0"<<endl;
	  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = ((alpha_1_4 + alpha_2_4 + alpha_1_4 * alpha_2_4) / (alpha_1_4 * alpha_1_4 * alpha_2_4 * alpha_2_4)) * exp(x4dk) + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_1)
	    + exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_2);
	  I = I * E.Jac;
	}
      } 
      else{
	cout<<"cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0"<<endl;
	if(abs(alpha_2_3) / epsi < 1){
	    if(abs(alpha_1_3) / epsi < 1){
	      cout<<"**** Si en plus cas alpha_2_3 = 0 et alpha_1_3 = 0"<<endl;
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      // I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - exp(x4dk);
	      //I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	      I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - 2.0 * exp(x4dk);
	      I = (I * E.Jac) / (2.0 * alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      cout<<"Si en plus cas alpha_2_3 = 0 et alpha_1_3 neq 0"<<endl;
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = ((alpha_3_4 * alpha_1_4 - alpha_1_3 * alpha_1_4 + alpha_1_3 * alpha_1_3) / (alpha_3_4 * alpha_3_4 * alpha_1_3 * alpha_1_3 * alpha_1_4)) * exp(x1dk) - exp(x4dk) / (alpha_3_4 * alpha_3_4 * alpha_1_4) + ((alpha_1_3 - alpha_3_4 - alpha_1_3 * alpha_3_4) / (alpha_1_3 * alpha_1_3 * alpha_3_4 * alpha_3_4)) * exp(x3dk);
	      I = I * E.Jac;
	    } 
	}
	
	else{
	  if(abs(alpha_1_2) / epsi < 1){
	    if(abs(alpha_1_3) / epsi < 1){
	      cout<<" Si en plus cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0"<<endl;
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = exp(x1dk) / (alpha_2_4 * alpha_3_4) + exp(x2dk) / (alpha_2_4 * alpha_2_3)
		- exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4);
	      I = I * E.Jac;
	    }
	    else{
	      cout<<"Si en plus cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0"<<endl;
	    
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      //I = ((alpha_3_4 - alpha_2_4 - alpha_1_4) / (alpha_1_4 * alpha_2_4 * alpha_2_3 * alpha_1_3)) * exp(x1dk) + exp(x2dk) / (alpha_2_4 * alpha_2_3) + exp(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_1_3) - exp(x4dk) / (alpha_2_3 * alpha_3_4 * alpha_1_4);
	      I = ((alpha_2_3 * alpha_2_4 * alpha_2_4 - alpha_2_4 * alpha_2_4 - alpha_2_3 * alpha_2_3 * alpha_2_4 + alpha_2_3 * alpha_2_3) / (alpha_2_4 * alpha_2_4 * alpha_2_3 * alpha_2_3 * alpha_3_4)) * exp(x2dk) + exp(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_2_4 * alpha_3_4); 
	      I = I * E.Jac;
	    }
	  }
	  
	  else{
	    if(abs(alpha_1_3) / epsi < 1){
	      cout<<" Si en plus cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0 "<<endl;
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = ((alpha_3_4 + alpha_1_4 - alpha_2_4) / (alpha_1_4 * alpha_3_4 * alpha_2_3 * alpha_1_2)) * exp(x1dk) - exp(x2dk) / (alpha_2_4 * alpha_1_2 * alpha_2_3) - exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4 * alpha_1_4);
	      I = I * E.Jac;
	    }
	    else{
	      cout<<"cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0"<<endl;
	      cout<<"cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0"<<endl;
	   
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = E.Jac * ( (exp(x1dk) / (alpha_1_2 * alpha_1_3 * alpha_1_4)) + (exp(x2dk) / (alpha_2_1 * alpha_2_3 * alpha_2_4)) + (exp(x3dk) / (alpha_3_1 * alpha_3_2 * alpha_3_4)) + (exp(x4dk) / (alpha_4_1 * alpha_4_2 * alpha_4_3)) );
	    }
	  }
	}
      }
    }
  }
  
  
  delete [] x1x2; x1x2 = 0;
  delete [] x1x3; x1x4 = 0;
  delete [] x1x4; x1x4 = 0;
  
  delete [] x2x3; x2x3 = 0;
  delete [] x2x4; x2x4 = 0;
  delete [] x3x4; x3x4 = 0;
  
  delete [] k; k = 0;
  return I;
}


void debugg_integral_tetra(const char file_name[], MailleSphere &MSph){

  cplx I1, I2;

  double diff;

   MKL_INT i, e, iloc, jloc;

  e =5; 
  iloc = 23; jloc = 14;
  
  cplx alpha_1_2, alpha_1_3, alpha_1_4;
  cplx alpha_2_1, alpha_2_3, alpha_2_4;
  cplx alpha_3_1, alpha_3_2, alpha_3_4;
  cplx alpha_4_1, alpha_4_2, alpha_4_3;
  cplx x1dk, x2dk, x3dk, x4dk;

  double *x1, *x2, *x3, *x4;
  x1 = MSph.elements[e].noeud[0]->get_X();
  x2 = MSph.elements[e].noeud[1]->get_X();
  x3 = MSph.elements[e].noeud[2]->get_X();
  x4 = MSph.elements[e].noeud[3]->get_X();

  double *x1x2 = new double[3];// x1 - x2
  double *x1x3 = new double[3];// x1 - x3
  double *x1x4 = new double[3];// x1 - x4

  double *x2x3 = new double[3];// x2 - x3
  double *x2x4 = new double[3];// x2 - x4
  double *x3x4 = new double[3];// x3 - x4

  double *k = new double[3];
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];

    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    k[i] = MSph.elements[e].OP[jloc].K[i] - MSph.elements[e].OP[iloc].K[i];
  }
  alpha_1_2 = cplx(0.0, 1.0) * cblas_ddot(3, x1x2, 1, k, 1);//i * (x1 - x2) \cdot k
  alpha_1_3 = cplx(0.0, 1.0) * cblas_ddot(3, x1x3, 1, k, 1);//i * (x1 - x3) \cdot k
  alpha_1_4 = cplx(0.0, 1.0) * cblas_ddot(3, x1x4, 1, k, 1);//i * (x1 - x4) \cdot k

  alpha_2_3 = cplx(0.0, 1.0) * cblas_ddot(3, x2x3, 1, k, 1);//i * (x2 - x3) \cdot k
  alpha_2_4 = cplx(0.0, 1.0) * cblas_ddot(3, x2x4, 1, k, 1);//i * (x2 - x4) \cdot k
  alpha_3_4 = cplx(0.0, 1.0) * cblas_ddot(3, x3x4, 1, k, 1);//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;
  
  double epsi = 1E-6;
  double val, C1, C2, divide;
  double maxx = 0.0, minn = 1000.0;
  double maxx_g = 0.0, minn_g = 1000.0;

  C1 = 0.0; C2 = 0.0; divide = 1.0 / 1000.0;
  cout<< " alpha_1_2 = "<<alpha_1_2<< " alpha_1_3 = "<<alpha_1_3<< " alpha_1_4 = "<<alpha_1_4<<endl;
  cout<< " alpha_2_3 = "<<alpha_2_3<< " alpha_2_4 = "<<alpha_2_4<< " alpha_3_4 = "<<alpha_3_4<<endl;
  //if ( (abs(alpha_1_4)/epsi >= 1) && (abs(alpha_2_4)/epsi >= 1 ) && (abs(alpha_3_4)/epsi >= 1 ) ){
  FILE* fic;
  if( !(fic = fopen(file_name,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }

  for(e = 0; e < MSph.get_N_elements(); e++) {
    maxx = 0.0, minn = 1000.0;
    for(iloc = 0; iloc < MSph.ndof; iloc++){
      for(jloc = 0; jloc < MSph.ndof; jloc++){
	auto t1 = std::chrono::high_resolution_clock::now();
	I2 = integrale_tetra(MSph.elements[e], iloc, jloc, "quad");
	auto t2 = std::chrono::high_resolution_clock::now();
	
	std::chrono::duration<double, std::milli> float_M1 = t2 - t1;

	C1 += float_M1.count();

	auto t3 = std::chrono::high_resolution_clock::now();
	I1 = quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
	auto t4 = std::chrono::high_resolution_clock::now();
	
	std::chrono::duration<double, std::milli> float_M2 = t4 - t3;

	C2 += float_M2.count();
	
	if (abs(I1-I2) < 1E-10){
	  val = 0.0;
	}
	else{
	  val = abs(I1-I2);
	}
	
	//cout<<"iloc = "<<iloc<<" jloc = "<<jloc<<" I1 = "<<I1<<" I2 = "<<I2<<"   abs(I1 - I2) = "<<val<<endl;
	maxx = max(maxx, abs(I1-I2));
	minn = min(minn, abs(I1-I2));
      }
    }
   
    maxx_g = max(maxx_g, maxx);
    minn_g = min(minn_g, minn);
    if ((maxx >1E-10) || (minn > 1E-10)){
      cout<<"e = "<<e<<" maxx_i = "<<maxx<<" minn_i = "<<minn<<" (Vol["<<e<<"])^{1/3} = "<<MSph.elements[e].rc_Vol<<endl;
      fprintf(fic," %d\t %10.15e\t %10.15e\n",e, maxx, minn);
    }
  }
  cout<<" maxx = "<<maxx_g<<" minn = "<<minn_g<<endl;
  cout<<" temps integral analytique = "<<C1 * divide<<" secs"<<endl;
  cout<<" temps integral numerique = "<<C2 * divide<<" secs"<<endl;
     /*
  }
  else{
    cout<<" Hors test "<<endl;
  }
    */
  fclose(fic);
}

/*------------------------------------------------------------------------------------------------------------------------*/
double norme_L2_volumique(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I = cplx(0.0,0.0);
  cplx val ;
  cplx *x;
  double s;
  
  X.get_F_elem(MSph.ndof, e, &x);
  
    
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){
      
      val = integrale_tetra(MSph.elements[e], iloc, jloc, chaine);
      
      I += x[jloc] * conj(x[iloc]) * val;
    }
  }
  
  s = abs(I);
  
  return s;
}


double norme_L2_volumique(VecN &X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
 
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_volumique(e, X, MSph, chaine);
  }

 return s;
}



void norme_L2_volumique(double *r, VecN &X, MailleSphere &MSph, const char chaine[]) {
  //initialiser *r = 0.0 en dehors de la fonction hors region parallele
  double s;
  MKL_INT e;
  s = 0.0;
  
#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_volumique(e, X, MSph, chaine);
  }

 #pragma omp atomic
  *r += s;
}


double norme_L2_volumique_Vitesse(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I, val ;
  cplx *x;
  double s;
  
  X.get_F_elem(MSph.ndof, e, &x);

  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){     
      
      val = integrale_tetra_VV(MSph.elements[e], iloc, jloc, chaine);      
      
      I += x[jloc] * conj(x[iloc]) * val;
    }
  }
  
  s = abs(I);
  
  return s;
}


double norme_L2_volumique_Vitesse(VecN &X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_volumique_Vitesse(e, X, MSph, chaine);
  }

  //s = sqrt(s);

  return s;

}


void norme_L2_volumique_Vitesse(double *r, VecN &X, MailleSphere &MSph, const char chaine[]) {
  //initialiser *r = 0.0 en dehors de la fonction hors region parallele
  double s;
  MKL_INT e;
  
  s = 0.0;

#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_volumique_Vitesse(e, X, MSph, chaine);
  }

  #pragma omp atomic
  *r += s;
  //s = sqrt(s);

}

#ifdef USE_QUADMATH
__float128 compute_derivative(__float128 h){
  __float128 res = 0.0;
  // s = (1 + h)^{-1/2}
  __float128 s = 1.0 / sqrtq(1.0 + h);

  //res = (s - 1) / h
  res = (s - 1.0) / h;

  return res;
}

__complex128 compute_derivative(__complex128 h){
  __complex128 res = 0.0;
  // s = (1 + h)^{-1/2}
  __complex128 s = (1.0 + h) * (1.0 + h);

  //res = (s - 1) / h
  res = (s - 1.0) / h;

  return res;
}


__complex128 compute_quad(__float128 h){
  __complex128 res = {(__float128)0.0, (__float128)0.0};
  // s = (1 + h)^{-1/2}
  res = cexpq({0.0, h + sqrtq(2)}) - cexpq({0.0, sqrtq(2)});
  __complex128 s = {(__float128)0.0, (__float128)1.0};
  s = s * cexpq({0.0,sqrtq(2)});
  //res = i - (s / h)
  res = (res / h);
  res = res - s;

  return res;
}

#endif

double compute_derivative(double h){
  double res = 0.0;
  double s = 1.0 / sqrt(1.0 + h);
  res = (s - 1.0) / h;

  return res;
}


cplx compute_double(double h){
  cplx res = cplx(0.0, 0.0);
  res = exp(cplx(0.0,h + sqrt(2))) - exp(cplx(0.0, sqrt(2)));
  res = (res / h) - cplx(0.0, 1.0) * exp(cplx(0.0, sqrt(2)));

  return res;
}



cplx compute_derivative(cplx h){
  cplx res = 0.0;
  cplx s = (1.0 + h) * (1.0 + h);
  res = (s - 1.0) / h;

  return res;
}


#ifdef USE_QUADMATH
void TEST_quadmath(){
  __float128 tab[15] = {1E-4, 1E-5, 1E-7, 1E-8, 1E-9, 1E-10, 1E-11, 1E-12, 1E-13, 1E-14, 1E-15, 1E-16, 1E-17, 1E-18, 1E-19};
  for(MKL_INT i = 0; i< 15; i++) {
    /*----------------------------------- cas quad precision ------------------------------*/
  __float128 t =  compute_derivative(tab[i]);

  //conversion du resultat en double
  double s = (double) t;

   /*----------------------------------- cas double precision ------------------------------*/
  double x = (double) tab[i];
  double r = compute_derivative(x);
  
  cout<<" tab["<<i<<"] = "<<x<<"    t = "<<s<<"       r = "<<r<<"     t - r = "<<s-r<<endl;
  }
}



void TEST_quadmath_complex(){
  __float128 tab[15] = {1E-4, 1E-5, 1E-7, 1E-8, 1E-9, 1E-10, 1E-11, 1E-12, 1E-13, 1E-14, 1E-15, 1E-16, 1E-17, 1E-18, 1E-19};
 
  for(MKL_INT i = 0; i< 15; i++) {
    /*----------------------------------- cas quad precision ------------------------------*/
    __complex128 t = compute_quad(tab[i]);
  //conversion du resultat en double
  cplx s = (cplx) t;
 
   /*----------------------------------- cas double precision ------------------------------*/
  double x = (double) tab[i];
  /*
  x.real(__real__ tab[i]);
  x.imag(__imag__ tab[i]);
  */
  cplx r = compute_double(x);
  
  cout<<" tab["<<i<<"] = "<<x<<"   result_quad  = "<<s<<"    result_double = "<<r<<"   error = "<<s-r<<endl;
  }
}


__float128 dot_u(MKL_INT n, __float128 *u, __float128 *v){
  __float128 r = (__float128)0.0;
  r = u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
  return r;
}


void TEST_dot_product_quadmath(){
  __float128 uq[3] = {(__float128)1E-4, (__float128)4E-5, (__float128)5E-7};
  __float128 vq[3] = {(__float128)1E-2, (__float128)9E-3, (__float128)2E-6};

  double u[3] = {1E-4, 4E-5, 5E-7};
  double v[3] = {1E-2, 9E-3, 2E-6};

  __float128 rq = dot_u(3, uq, vq);
  double r =  cblas_ddot(3, u, 1, v, 1);

  double s = (double) rq;

  cout<<"   result_quad = "<<s<<"   result_double = "<<r<<"   error = "<<abs(s - r)<<endl;
}


cplx integrale_tetra_quad(Element &E, MKL_INT iloc, MKL_INT jloc){
  cplx I = cplx(0.0, 0.0);
  __complex128 J = {0.0, 0.0};
  cplx alpha_1_2, alpha_1_3, alpha_1_4;
  cplx alpha_2_1, alpha_2_3, alpha_2_4;
  cplx alpha_3_1, alpha_3_2, alpha_3_4;
  cplx alpha_4_1, alpha_4_2, alpha_4_3;
  cplx x1dk, x2dk, x3dk, x4dk;

  double *x1, *x2, *x3, *x4;
  x1 = E.noeud[0]->get_X();
  x2 = E.noeud[1]->get_X();
  x3 = E.noeud[2]->get_X();
  x4 = E.noeud[3]->get_X();

  double *x1x2 = new double[3];// x1 - x2
  double *x1x3 = new double[3];// x1 - x3
  double *x1x4 = new double[3];// x1 - x4

  double *x2x3 = new double[3];// x2 - x3
  double *x2x4 = new double[3];// x2 - x4
  double *x3x4 = new double[3];// x3 - x4

  __float128 *X1X2 = new __float128[3];// x1 - x2
  __float128 *X1X3 = new __float128[3];// x1 - x3
  __float128 *X1X4 = new __float128[3];// x1 - x4

  __float128 *X1 = new __float128[3];
  __float128 *X2 = new __float128[3]; 
  __float128 *X3 = new __float128[3];
  __float128 *X4 = new __float128[3];

  __float128 *X2X3 = new __float128[3];// x2 - x3
  __float128 *X2X4 = new __float128[3];// x2 - x4
  __float128 *X3X4 = new __float128[3];// x3 - x4

  double *k = new double[3];

  __float128 *K = new __float128[3];
  
  MKL_INT i;

  for(i = 0; i < 3; i++) {
    X1[i] = (__float128) x1[i];
    X2[i] = (__float128) x2[i];
    X3[i] = (__float128) x3[i];
    X4[i] = (__float128) x4[i];

    k[i] = E.OP[jloc].K[i] - E.OP[iloc].K[i];
  }
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];

    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    X1X2[i] = X1[i] - X2[i];
    X1X3[i] = X1[i] - X3[i];
    X1X4[i] = X1[i] - X4[i];

    X2X3[i] = X2[i] - X3[i];
    X2X4[i] = X2[i] - X4[i];
    X3X4[i] = X3[i] - X4[i];
    K[i] = (__float128) k[i];
   
  }
  alpha_1_2 = cplx(0.0, 1.0) * cblas_ddot(3, x1x2, 1, k, 1);//i * (x1 - x2) \cdot k
  alpha_1_3 = cplx(0.0, 1.0) * cblas_ddot(3, x1x3, 1, k, 1);//i * (x1 - x3) \cdot k
  alpha_1_4 = cplx(0.0, 1.0) * cblas_ddot(3, x1x4, 1, k, 1);//i * (x1 - x4) \cdot k

  alpha_2_3 = cplx(0.0, 1.0) * cblas_ddot(3, x2x3, 1, k, 1);//i * (x2 - x3) \cdot k
  alpha_2_4 = cplx(0.0, 1.0) * cblas_ddot(3, x2x4, 1, k, 1);//i * (x2 - x4) \cdot k
  alpha_3_4 = cplx(0.0, 1.0) * cblas_ddot(3, x3x4, 1, k, 1);//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;

  __complex128 ALPHA_1_2, ALPHA_1_3, ALPHA_1_4;
  __complex128 ALPHA_2_1, ALPHA_2_3, ALPHA_2_4;
  __complex128 ALPHA_3_1, ALPHA_3_2, ALPHA_3_4;
  __complex128 ALPHA_4_1, ALPHA_4_2, ALPHA_4_3;

  ALPHA_1_2 = {0.0, dot_u(3, X1X2, K)};
  ALPHA_1_3 = {0.0, dot_u(3, X1X3, K)};
  ALPHA_1_4 = {0.0, dot_u(3, X1X4, K)};

  ALPHA_2_3 = {0.0, dot_u(3, X2X3, K)};
  ALPHA_2_4 = {0.0, dot_u(3, X2X4, K)};
  ALPHA_3_4 = {0.0, dot_u(3, X3X4, K)};

  ALPHA_2_1 = - ALPHA_1_2;
  ALPHA_3_1 = - ALPHA_1_3;
  ALPHA_4_1 = - ALPHA_1_4;
  
  ALPHA_3_2 = - ALPHA_2_3;
  ALPHA_4_2 = - ALPHA_2_4;
  ALPHA_4_3 = - ALPHA_3_4;

  __complex128 X1DK, X2DK, X3DK, X4DK;
  
  double epsi_II = 1E-3;
  
  if (abs(alpha_1_4) < 1E-8 * E.rc_Vol){
    if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
      if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	
	I = E.Vol * exp(x4dk); 
      }
      
      else{
	/*-------------------- cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 neq 0 ------------*/
	//double precision
	if(abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = exp(x3dk) - (alpha_3_4 * alpha_3_4 + 2.0 * alpha_3_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_3_4 * alpha_3_4 * alpha_3_4) ;
	}
	//quad precision
	else{
	  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  J = cexpq(X3DK) - (ALPHA_3_4 * ALPHA_3_4 + 2.0 * ALPHA_3_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4) ;
	  I = (cplx) J;
	}
      }
    }
    
    else{
      if(abs(alpha_2_4) > epsi_II * E.rc_Vol) {
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = exp(x2dk) - (alpha_2_4 * alpha_2_4 + 2.0 * alpha_2_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_2_4 * alpha_2_4 * alpha_2_4) ;
	}
	
	else{//--------------- abs(alpha_3_4) > 1E-8 * E.rc_Vol
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	  if(abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	    if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	      I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //double precision
	      if (abs(alpha_2_3) > epsi_II * E.rc_Vol) {
		//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
		x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		
		I = ((alpha_2_4  + alpha_3_4 + alpha_2_4 * alpha_3_4) / (alpha_2_4 * alpha_2_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_2)
		  + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_3);
		I = (I * E.Jac);
	      }
	      //quad precision
	      else{
		//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0	
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
		  + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
		J = (J * E.Jac);
		I = (cplx) J;
	      }
	    }
	  }//if abs(alpha_3_4) > epsi_II * E.rc_Vol

	  else{//***************** else de if abs(alpha_3_4) > epsi_II * E.rc_Vol ************
	    if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0	    
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0	      
	      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
		+ cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
	      J = (J * E.Jac);
	      I = (cplx) J;
	    }
	  }//************** fin else de if abs(alpha_3_4) > epsi_II * E.rc_Vol **************
	  
	}//fin if abs(alpha_3_4) > 1E-8 * E.rc_Vol)
	
      }//fin if abs(alpha_2_4) > epsi_II * E.rc_Vol
      else{
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  J = cexpq(X2DK) - (ALPHA_2_4 * ALPHA_2_4 + 2.0 * ALPHA_2_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_4) ;
	  I = (cplx) J;
	}
	
	else{
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	  if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	    J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	    I = (cplx) J;
	  }
	  else{
	    //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
	    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
	      + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
	    J = (J * E.Jac);
	    I = (cplx) J;
	  }
	}
      }//fin else du if abs(alpha_2_4) > epsi_II * E.rc_Vol
    }//fin else du if abs(alpha_2_4) < 1E-8 * E.rc_Vol
    
    
  }//fin if abs(alpha_1_4) < 1E-8
  
  else{//else de abs(alpha_1_4) > 1E-8
    if (abs(alpha_1_4) > epsi_II * E.rc_Vol) {
      if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  
	  I = exp(x1dk) - (alpha_1_4 * alpha_1_4 + 2.0 * alpha_1_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_1_4 * alpha_1_4 * alpha_1_4) ;
	}
	else{/*-------------------------- abs(alpha_3_4) > 1E-8 E.rc_Vol ---------------*/
	  if (abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	    //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_3 = 0
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x1 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	      I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //Si en plus alpha_1_3 neq 0
	      //precision double
	      if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
		x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		I = ((alpha_1_4 + alpha_3_4 + alpha_1_4 * alpha_3_4) / (alpha_1_4 * alpha_1_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_1)
		  +  exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_3);
		I = I * E.Jac;
	      }
	      //quad precision
	      else{
		//Si en plus alpha_1_3 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
		  +  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	  }//fin if abs(alpha_3_4) > epsi * E.rc_Vol

	  else{//else de if abs(alpha_3_4) > epsi * E.rc_Vol
	    //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_3 = 0
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //Si en plus alpha_1_3 neq 0	     
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
		+  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }//fin else de if abs(alpha_3_4) > epsi * E.rc_Vol
	  
	}/*---------------------------- fin abs(alpha_3_4) > 1E-8 * E.rc_Vol -----------------*/
      }//fin if abs(alpha_1_4 > 1E-8) et abs(alpha_2_4 < 1E-8)

      
      else{//if abs(alpha_2_4) > epsi_II
	if (abs(alpha_2_4) > epsi_II * E.rc_Vol) {
	  if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_2 = 0
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_1_4 + 2.0) * exp(x4dk) + (alpha_1_4 - 2.0) * exp(x1dk);
	      I = (I * E.Jac) / (alpha_1_4 * alpha_1_4 * alpha_1_4);
	    }
	    else{
	      //Si en plus alpha_1_2 neq 0
	      //double precision
	      if (abs(alpha_1_2) > epsi_II * E.rc_Vol) {
		x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		I = ((alpha_1_4 + alpha_2_4 + alpha_1_4 * alpha_2_4) / (alpha_1_4 * alpha_1_4 * alpha_2_4 * alpha_2_4)) * exp(x4dk) + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_1)
		  + exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_2);
		I = I * E.Jac;
	      }
	      //quad precision
	      else{	
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
		  + cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }//fin else de if abs(alpha_1_2) < 1E-8 * E.rc_Vol
	  }
	  else{
	    /*--------------cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0------------*/
	    if (abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	      if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 = 0 et alpha_1_3 = 0
		  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		  //I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - exp(x4dk);
		  //I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
		  I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - 2.0 * exp(x4dk);
		  I = (I * E.Jac) / (2.0 * alpha_3_4 * alpha_3_4 * alpha_3_4);
		}
		else{
		  //cas alpha_2_3 = 0 et alpha_1_3 neq 0
		  //double precision
		  if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
		    x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		    x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		    x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		    I = ((alpha_3_4 * alpha_1_4 - alpha_1_3 * alpha_1_4 + alpha_1_3 * alpha_1_3) / (alpha_3_4 * alpha_3_4 * alpha_1_3 * alpha_1_3 * alpha_1_4)) * exp(x1dk) - exp(x4dk) / (alpha_3_4 * alpha_3_4 * alpha_1_4) + ((alpha_1_3 - alpha_3_4 - alpha_1_3 * alpha_3_4) / (alpha_1_3 * alpha_1_3 * alpha_3_4 * alpha_3_4)) * exp(x3dk);
		    I = I * E.Jac;
		  }//quad precision
		  else{
		    //cas alpha_2_3 = 0 et alpha_1_3 neq 0		   
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		}
	      }
	      
	      else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
		if (abs(alpha_2_3) > epsi_II * E.rc_Vol) {
		  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		      I = exp(x1dk) / (alpha_2_4 * alpha_3_4) + exp(x2dk) / (alpha_2_4 * alpha_2_3)
			- exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4);
		      I = I * E.Jac;
		    }
		    else{
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
		      //double precision
		      if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
			//x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			I = ((alpha_2_3 * alpha_2_4 * alpha_2_4 - alpha_2_4 * alpha_2_4 - alpha_2_3 * alpha_2_3 * alpha_2_4 + alpha_2_3 * alpha_2_3) / (alpha_2_4 * alpha_2_4 * alpha_2_3 * alpha_2_3 * alpha_3_4)) * exp(x2dk) + exp(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_2_4 * alpha_3_4); 
			I = I * E.Jac;
		      }//quad precision
		      else{
			
			//X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
			J = J * E.Jac;
			I = (cplx) J;
		      }
		    }
		  }
		  
		  else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 >1E-8) et abs(alpha_1_2 > 1E-8)
		    if (abs(alpha_1_2) > epsi_II * E.rc_Vol) {
		      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
			//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
			x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			I = ((alpha_3_4 + alpha_1_4 - alpha_2_4) / (alpha_1_4 * alpha_3_4 * alpha_2_3 * alpha_1_2)) * exp(x1dk) - exp(x2dk) / (alpha_2_4 * alpha_1_2 * alpha_2_3) - exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4 * alpha_1_4);
			I = I * E.Jac;
		      }
		      else{
			//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
			//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
			//double precision
			if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
			  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			  I = E.Jac * ( (exp(x1dk) / (alpha_1_2 * alpha_1_3 * alpha_1_4)) + (exp(x2dk) / (alpha_2_1 * alpha_2_3 * alpha_2_4)) + (exp(x3dk) / (alpha_3_1 * alpha_3_2 * alpha_3_4)) + (exp(x4dk) / (alpha_4_1 * alpha_4_2 * alpha_4_3)) );
			}
			else{
			  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			  J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
			  I = (cplx) J;
			}
		      }
		      
		    }//fin if abs(alpha_1_2) > epsi_II * E.rc_Vol
		    
		    else{//***************else de if abs(alpha_1_2) > epsi_II * E.rc_Vol *******
		      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
			//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		
			X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
			J = J * E.Jac;
			I = (cplx) J;
		      }
		      else{
			//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
			//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		      
			X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
			I = (cplx) J;
		      }
		    }//************* fin else de if abs(alpha_1_2) > epsi_II * E.rc_Vol *******
		    
		  }//fin else de if abs(alpha_1_2) < 1E-8 * E.rc_Vol
		}//fin if abs(alpha_2_3) > epsi_II * E.rc_Vol


		else{/***************** 1E-8 < abs(alpha_2_3) < epsi_II * E.rc_Vol **********/
		  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
			- cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		    else{
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0		     
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		  }
		  
		  else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 >1E-8) et abs(alpha_1_2 > 1E-8)
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		    else{
		      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		      
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		      I = (cplx) J;
		    }
		  }
		}/************ **fin 1E-8 < abs(alpha_2_3) < epsi_II * E.rc_Vol *************/
		
	      }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	    }//fin if abs(alpha_3_4) > 1E-4
	    
	    //***************** quad precision avec 1E-8 < abs(alpha_3_4) < 1E-4 *************
	    else{
	      if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 = 0 et alpha_1_3 = 0
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
		  J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
		  I = (cplx) J;
		}
		else{
		  //cas alpha_2_3 = 0 et alpha_1_3 neq 0
		  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
	      }
	      
	      else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
		if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		    //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		      - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		  else{
		    //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		}
		
		else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
		  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		    //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		  else{
		    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		    //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		   
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		    I = (cplx) J;
		  }
		}
	      }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	    }//*fin else de abs(alpha_1_4) > 1E-4 abs(alpha_2_4) > 1E-4 if abs(alpha_3_4) > 1E-4 ******************** 
	    
	  }//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4 < 1E-8)
	  
	}//fin if abs(alpha_2_4) > epsi_II
	
	/*-------------------------------1E-8 < abs(alpha_2_4) < epsi_II --------------------------*/
	else{//else de if abs(alpha_2_4) > epsi_II
	  if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_2 = 0
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_1_4 + 2.0) * cexpq(X4DK) + (ALPHA_1_4 - 2.0) * cexpq(X1DK);
	      J = (J * E.Jac) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4);
	      I = (cplx) J;
	    }
	    else{
	      //Si en plus alpha_1_2 neq 0
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
		+ cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }
	  else{
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	    if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 = 0 et alpha_1_3 = 0
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
		J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
		I = (cplx) J;
	      }
	      else{
		//cas alpha_2_3 = 0 et alpha_1_3 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	    
	    else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	      if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		    - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
		else{
		  //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	  
		  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		 
		  J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		  J = J * E.Jac;
		  I = (cplx) J;
		}
	      }
	      
	      else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		 
		 X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		 X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		 X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		 X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
		else{
		  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		  //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		  //cout<<" cas tous non nuls et 1e-8 < alpha_2_4 < "<<epsi_II*E.rc_Vol<<endl;
		  
		 X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		 X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		 X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		 X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		  I = (cplx) J;
		}
	      }
	    }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	  }//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4)
	}//fin else de if abs(alpha_2_4) > epsi_II
	/*----------------------------- 1E-8 < abs(alpha_2_4) < epsi_II --------------------------*/
	
      }//fin else de abs(alpha_1_4 < 1E-8) et abs(alpha_2_4 < 1E-8)
      
      
    }//fin if abs(alpha_1_4 > epsi_II)



    
    //cas quad precision
    else{//else de if abs(alpha_1_4 > epsi_II)
      if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  
	  J = cexpq(X1DK) - (ALPHA_1_4 * ALPHA_1_4 + 2.0 * ALPHA_1_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4) ;
	  I = (cplx) J;
	}
	else{
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	    //Si en plus alpha_1_3 = 0
	
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	    J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	    I = (cplx) J;
	  }
	  else{
	    //Si en plus alpha_1_3 neq 0
	  
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
	      +  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
	    J = J * E.Jac;
	    I = (cplx) J;
	  }
	} 
      }//fin if abs(alpha_1_4 > 1E-8) et abs(alpha_2_4 < 1E-8)
      else{
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	    //Si en plus alpha_1_2 = 0
	   
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = (ALPHA_1_4 + 2.0) * cexpq(X4DK) + (ALPHA_1_4 - 2.0) * cexpq(X1DK);
	    J = (J * E.Jac) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4);
	    I = (cplx) J;
	  }
	  else{
	    //Si en plus alpha_1_2 neq 0
	   
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
	      + cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
	    J = J * E.Jac;
	    I = (cplx) J;
	  }
	}
	else{
	  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	  if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //cas alpha_2_3 = 0 et alpha_1_3 = 0	    
	     
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //cas alpha_2_3 = 0 et alpha_1_3 neq 0	     
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K	     
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }
	  
	  else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		  - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	      else{
		//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	    
	    else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	      else{
		//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		I = (cplx) J;
	      }
	    }
	  }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	}//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4)
      }//fin else de abs(alpha_1_4 < 1E-8) et abs(alpha_2_4 < 1E-8)
    }//fin else de if abs(alpha_1_4 > epsi_II)


    
    
  }//fin else de abs(alpha_1_4 < 1E-8)
  
  
  delete [] x1x2; x1x2 = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x1x4; x1x4 = 0;
  
  delete [] x2x3; x2x3 = 0;
  delete [] x2x4; x2x4 = 0;
  delete [] x3x4; x3x4 = 0;

  delete [] X1X2; X1X2 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] X1X4; X1X4 = 0;
  
  delete [] X2X3; X2X3 = 0;
  delete [] X2X4; X2X4 = 0;
  delete [] X3X4; X3X4 = 0;

  delete [] k; k = 0;

  delete [] X1; X1 = 0;
  delete [] X2; X2 = 0;
  delete [] X3; X3 = 0;
  delete [] X4; X4 = 0;
  delete [] K; K = 0;
  return I;
}
/*------------------------------------------------------------------------------------------------------------------*/
cplx integrale_tetra_quad_precision(Element &E, MKL_INT iloc, MKL_INT jloc){
  cplx I = cplx(0.0, 0.0);
  __complex128 J = {(__float128)0.0, (__float128)0.0};
  __complex128 alpha_1_2, alpha_1_3, alpha_1_4;
  __complex128 alpha_2_1, alpha_2_3, alpha_2_4;
  __complex128 alpha_3_1, alpha_3_2, alpha_3_4;
  __complex128 alpha_4_1, alpha_4_2, alpha_4_3;
  __complex128 x1dk, x2dk, x3dk, x4dk;

  __float128 *x1, *x2, *x3, *x4;

  x1 = new __float128[3];
  x2 = new __float128[3];
  x3 = new __float128[3];
  x4 = new __float128[3];
  
  __float128 *x1x2 = new __float128[3];// x1 - x2
  __float128 *x1x3 = new __float128[3];// x1 - x3
  __float128 *x1x4 = new __float128[3];// x1 - x4

  __float128 *x2x3 = new __float128[3];// x2 - x3
  __float128 *x2x4 = new __float128[3];// x2 - x4
  __float128 *x3x4 = new __float128[3];// x3 - x4

  __float128 *k = new __float128[3];

  double KIJ;
  
  MKL_INT i;
  
  for(i = 0; i < 3; i++) {
    
    x1[i] = (__float128)(E.noeud[0]->get_X())[i];
    x2[i] = (__float128)(E.noeud[1]->get_X())[i];
    x3[i] = (__float128)(E.noeud[2]->get_X())[i];
    x4[i] = (__float128)(E.noeud[3]->get_X())[i];
  }
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];
    
    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    KIJ = E.OP[jloc].K[i] - E.OP[iloc].K[i];
    
    k[i] = (__float128) KIJ;
  }
  alpha_1_2 = {0.0, dot_u(3, x1x2, k)};//i * (x1 - x2) \cdot k
  alpha_1_3 = {0.0, dot_u(3, x1x3, k)};//i * (x1 - x3) \cdot k
  alpha_1_4 = {0.0, dot_u(3, x1x4, k)};//i * (x1 - x4) \cdot k

  alpha_2_3 = {0.0, dot_u(3, x2x3, k)};//i * (x2 - x3) \cdot k
  alpha_2_4 = {0.0, dot_u(3, x2x4, k)};//i * (x2 - x4) \cdot k
  alpha_3_4 = {0.0, dot_u(3, x3x4, k)};//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;
  
  double epsi = 1E-8 * E.rc_Vol;

 
  if (cabsq(alpha_1_4) < epsi){
    if (cabsq(alpha_2_4) < epsi ){
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	
	J = E.Vol * cexpq(x4dk); 
      }

      else{
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	J = cexpq(x3dk) - (alpha_3_4 * alpha_3_4 + 2.0 * alpha_3_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_3_4 * alpha_3_4 * alpha_3_4) ;
      }
    }

    else{
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	x2dk = {0.0, dot_u(3, x2, k)};//i * x3 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	J = cexpq(x2dk) - (alpha_2_4 * alpha_2_4 + 2.0 * alpha_2_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_2_4 * alpha_2_4 * alpha_2_4) ;
      }
      
      else{
	//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	if (cabsq(alpha_2_3) < epsi){
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_3_4 - 2.0) * cexpq(x3dk) + (alpha_3_4 + 2.0) * cexpq(x4dk);
	  J = (J * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
	  x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k

	  J = ((alpha_2_4  + alpha_3_4 + alpha_2_4 * alpha_3_4) / (alpha_2_4 * alpha_2_4 * alpha_3_4 * alpha_3_4)) * cexpq(x4dk) + cexpq(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_2)
	    + cexpq(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_3);
	  J = (J * E.Jac);
	}
      }
      
    }

    
  }
  
  else{
    if (cabsq(alpha_2_4) < epsi ){
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	
	J = cexpq(x1dk) - (alpha_1_4 * alpha_1_4 + 2.0 * alpha_1_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_1_4 * alpha_1_4 * alpha_1_4) ;
      }
      else{
	//cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	if(cabsq(alpha_1_3)  < epsi){
	  //Si en plus alpha_1_3 = 0
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_3_4 - 2.0) * cexpq(x3dk) + (alpha_3_4 + 2.0) * cexpq(x4dk);
	  J = (J * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  //Si en plus alpha_1_3 neq 0	 
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  
	  J = ((alpha_1_4 + alpha_3_4 + alpha_1_4 * alpha_3_4) / (alpha_1_4 * alpha_1_4 * alpha_3_4 * alpha_3_4)) * cexpq(x4dk) + cexpq(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_1)
	   +  cexpq(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_3);
	  J = J * E.Jac;
	}
      } 
    }
    else{
      if(cabsq(alpha_3_4)  < epsi){
      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	if(cabsq(alpha_1_2)  < epsi){
	  //Si en plus alpha_1_2 = 0
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_1_4 + 2.0) * cexpq(x4dk) + (alpha_1_4 - 2.0) * cexpq(x1dk);
	  J = (J * E.Jac) / (alpha_1_4 * alpha_1_4 * alpha_1_4);
	}
	else{
	  //Si en plus alpha_1_2 neq 0	 
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  
	  J = ((alpha_1_4 + alpha_2_4 + alpha_1_4 * alpha_2_4) / (alpha_1_4 * alpha_1_4 * alpha_2_4 * alpha_2_4)) * cexpq(x4dk) + cexpq(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_1)
	    + cexpq(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_2);
	  J = J * E.Jac;
	}
      }
      else{
	//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	if(cabsq(alpha_2_3)  < epsi){
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 = 0 et alpha_1_3 = 0
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * cexpq(x3dk) - 2.0 * cexpq(x4dk);
	      J = (J * E.Jac) / (2.0 * alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //cas alpha_2_3 = 0 et alpha_1_3 neq 0
	      x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = ((alpha_3_4 * alpha_1_4 - alpha_1_3 * alpha_1_4 + alpha_1_3 * alpha_1_3) / (alpha_3_4 * alpha_3_4 * alpha_1_3 * alpha_1_3 * alpha_1_4)) * cexpq(x1dk) - cexpq(x4dk) / (alpha_3_4 * alpha_3_4 * alpha_1_4) + ((alpha_1_3 - alpha_3_4 - alpha_1_3 * alpha_3_4) / (alpha_1_3 * alpha_1_3 * alpha_3_4 * alpha_3_4)) * cexpq(x3dk);
	      J = J * E.Jac;
	    }
	}
	
	else{
	  if(cabsq(alpha_1_2)  < epsi){
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0	    
	       x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	       x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	       x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	       x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	       
	      J = cexpq(x1dk) / (alpha_2_4 * alpha_3_4) + cexpq(x2dk) / (alpha_2_4 * alpha_2_3)
		- cexpq(x3dk) / (alpha_3_4 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_3_4);
	      J = J * E.Jac;
	    }
	    else{
	      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	    
	      //x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      
	      J = ((alpha_2_3 * alpha_2_4 * alpha_2_4 - alpha_2_4 * alpha_2_4 - alpha_2_3 * alpha_2_3 * alpha_2_4 + alpha_2_3 * alpha_2_3) / (alpha_2_4 * alpha_2_4 * alpha_2_3 * alpha_2_3 * alpha_3_4)) * cexpq(x2dk) + cexpq(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_2_4 * alpha_3_4); 
	      J = J * E.Jac;
	    }
	  }
	  
	  else{
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0	    
	       x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	       x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	       x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	       x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      
	      J = ((alpha_3_4 + alpha_1_4 - alpha_2_4) / (alpha_1_4 * alpha_3_4 * alpha_2_3 * alpha_1_2)) * cexpq(x1dk) - cexpq(x2dk) / (alpha_2_4 * alpha_1_2 * alpha_2_3) - cexpq(x3dk) / (alpha_3_4 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_3_4 * alpha_1_4);
	      J = J * E.Jac;
	    }
	    else{
	      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
	   
	      x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = E.Jac * ( (cexpq(x1dk) / (alpha_1_2 * alpha_1_3 * alpha_1_4)) + (cexpq(x2dk) / (alpha_2_1 * alpha_2_3 * alpha_2_4)) + (cexpq(x3dk) / (alpha_3_1 * alpha_3_2 * alpha_3_4)) + (cexpq(x4dk) / (alpha_4_1 * alpha_4_2 * alpha_4_3)) );
	    }
	  }
	}
      }
    }
  }
  
  I = (cplx) J;
  
  delete [] x1; x1 = 0;
  delete [] x2; x2 = 0;
  delete [] x3; x3 = 0;
  delete [] x4; x4 = 0;
  
  delete [] x1x2; x1x2 = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x1x4; x1x4 = 0;
  
  delete [] x2x3; x2x3 = 0;
  delete [] x2x4; x2x4 = 0;
  delete [] x3x4; x3x4 = 0;
  
  delete [] k; k = 0;
  return I;
}


/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
cplx integrale_tetra_quad(Element &E, Onde_plane &P1, Onde_plane &P2){
  cplx I = cplx(0.0, 0.0);
  __complex128 J = {0.0, 0.0};
  cplx alpha_1_2, alpha_1_3, alpha_1_4;
  cplx alpha_2_1, alpha_2_3, alpha_2_4;
  cplx alpha_3_1, alpha_3_2, alpha_3_4;
  cplx alpha_4_1, alpha_4_2, alpha_4_3;
  cplx x1dk, x2dk, x3dk, x4dk;

  double *x1, *x2, *x3, *x4;
  x1 = E.noeud[0]->get_X();
  x2 = E.noeud[1]->get_X();
  x3 = E.noeud[2]->get_X();
  x4 = E.noeud[3]->get_X();

  double *x1x2 = new double[3];// x1 - x2
  double *x1x3 = new double[3];// x1 - x3
  double *x1x4 = new double[3];// x1 - x4

  double *x2x3 = new double[3];// x2 - x3
  double *x2x4 = new double[3];// x2 - x4
  double *x3x4 = new double[3];// x3 - x4

  __float128 *X1X2 = new __float128[3];// x1 - x2
  __float128 *X1X3 = new __float128[3];// x1 - x3
  __float128 *X1X4 = new __float128[3];// x1 - x4

  __float128 *X1 = new __float128[3];
  __float128 *X2 = new __float128[3]; 
  __float128 *X3 = new __float128[3];
  __float128 *X4 = new __float128[3];

  __float128 *X2X3 = new __float128[3];// x2 - x3
  __float128 *X2X4 = new __float128[3];// x2 - x4
  __float128 *X3X4 = new __float128[3];// x3 - x4

  double *k = new double[3];

  __float128 *K = new __float128[3];
  
  MKL_INT i;

  for(i = 0; i < 3; i++) {
    X1[i] = (__float128) x1[i];
    X2[i] = (__float128) x2[i];
    X3[i] = (__float128) x3[i];
    X4[i] = (__float128) x4[i];

    k[i] = P2.K[i] - P1.K[i];
  }
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];

    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    X1X2[i] = X1[i] - X2[i];
    X1X3[i] = X1[i] - X3[i];
    X1X4[i] = X1[i] - X4[i];

    X2X3[i] = X2[i] - X3[i];
    X2X4[i] = X2[i] - X4[i];
    X3X4[i] = X3[i] - X4[i];
    K[i] = (__float128) k[i];
   
  }
  alpha_1_2 = cplx(0.0, 1.0) * cblas_ddot(3, x1x2, 1, k, 1);//i * (x1 - x2) \cdot k
  alpha_1_3 = cplx(0.0, 1.0) * cblas_ddot(3, x1x3, 1, k, 1);//i * (x1 - x3) \cdot k
  alpha_1_4 = cplx(0.0, 1.0) * cblas_ddot(3, x1x4, 1, k, 1);//i * (x1 - x4) \cdot k

  alpha_2_3 = cplx(0.0, 1.0) * cblas_ddot(3, x2x3, 1, k, 1);//i * (x2 - x3) \cdot k
  alpha_2_4 = cplx(0.0, 1.0) * cblas_ddot(3, x2x4, 1, k, 1);//i * (x2 - x4) \cdot k
  alpha_3_4 = cplx(0.0, 1.0) * cblas_ddot(3, x3x4, 1, k, 1);//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;

  __complex128 ALPHA_1_2, ALPHA_1_3, ALPHA_1_4;
  __complex128 ALPHA_2_1, ALPHA_2_3, ALPHA_2_4;
  __complex128 ALPHA_3_1, ALPHA_3_2, ALPHA_3_4;
  __complex128 ALPHA_4_1, ALPHA_4_2, ALPHA_4_3;

  ALPHA_1_2 = {0.0, dot_u(3, X1X2, K)};
  ALPHA_1_3 = {0.0, dot_u(3, X1X3, K)};
  ALPHA_1_4 = {0.0, dot_u(3, X1X4, K)};

  ALPHA_2_3 = {0.0, dot_u(3, X2X3, K)};
  ALPHA_2_4 = {0.0, dot_u(3, X2X4, K)};
  ALPHA_3_4 = {0.0, dot_u(3, X3X4, K)};

  ALPHA_2_1 = - ALPHA_1_2;
  ALPHA_3_1 = - ALPHA_1_3;
  ALPHA_4_1 = - ALPHA_1_4;
  
  ALPHA_3_2 = - ALPHA_2_3;
  ALPHA_4_2 = - ALPHA_2_4;
  ALPHA_4_3 = - ALPHA_3_4;

  __complex128 X1DK, X2DK, X3DK, X4DK;
  
  double epsi_II = 1E-3;
  
  if (abs(alpha_1_4) < 1E-8 * E.rc_Vol){
    if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
      if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	
	I = E.Vol * exp(x4dk); 
      }
      
      else{
	/*-------------------- cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 neq 0 ------------*/
	//double precision
	if(abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = exp(x3dk) - (alpha_3_4 * alpha_3_4 + 2.0 * alpha_3_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_3_4 * alpha_3_4 * alpha_3_4) ;
	}
	//quad precision
	else{
	  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  J = cexpq(X3DK) - (ALPHA_3_4 * ALPHA_3_4 + 2.0 * ALPHA_3_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4) ;
	  I = (cplx) J;
	}
      }
    }
    
    else{
      if(abs(alpha_2_4) > epsi_II * E.rc_Vol) {
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  I = exp(x2dk) - (alpha_2_4 * alpha_2_4 + 2.0 * alpha_2_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_2_4 * alpha_2_4 * alpha_2_4) ;
	}
	
	else{//--------------- abs(alpha_3_4) > 1E-8 * E.rc_Vol
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	  if(abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	    if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	      I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //double precision
	      if (abs(alpha_2_3) > epsi_II * E.rc_Vol) {
		//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
		x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		
		I = ((alpha_2_4  + alpha_3_4 + alpha_2_4 * alpha_3_4) / (alpha_2_4 * alpha_2_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_2)
		  + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_3);
		I = (I * E.Jac);
	      }
	      //quad precision
	      else{
		//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0	
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
		  + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
		J = (J * E.Jac);
		I = (cplx) J;
	      }
	    }
	  }//if abs(alpha_3_4) > epsi_II * E.rc_Vol

	  else{//***************** else de if abs(alpha_3_4) > epsi_II * E.rc_Vol ************
	    if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0	    
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0	      
	      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
		+ cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
	      J = (J * E.Jac);
	      I = (cplx) J;
	    }
	  }//************** fin else de if abs(alpha_3_4) > epsi_II * E.rc_Vol **************
	  
	}//fin if abs(alpha_3_4) > 1E-8 * E.rc_Vol)
	
      }//fin if abs(alpha_2_4) > epsi_II * E.rc_Vol
      else{
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  J = cexpq(X2DK) - (ALPHA_2_4 * ALPHA_2_4 + 2.0 * ALPHA_2_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_4) ;
	  I = (cplx) J;
	}
	
	else{
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	  if (abs(alpha_2_3) < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	    J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	    I = (cplx) J;
	  }
	  else{
	    //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
	    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_2_4  + ALPHA_3_4 + ALPHA_2_4 * ALPHA_3_4) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_2)
	      + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3);
	    J = (J * E.Jac);
	    I = (cplx) J;
	  }
	}
      }//fin else du if abs(alpha_2_4) > epsi_II * E.rc_Vol
    }//fin else du if abs(alpha_2_4) < 1E-8 * E.rc_Vol
    
    
  }//fin if abs(alpha_1_4) < 1E-8
  
  else{//else de abs(alpha_1_4) > 1E-8
    if (abs(alpha_1_4) > epsi_II * E.rc_Vol) {
      if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	  
	  I = exp(x1dk) - (alpha_1_4 * alpha_1_4 + 2.0 * alpha_1_4 + 2.0) * 0.5  * exp(x4dk);
	  I = (I * E.Jac) /  (alpha_1_4 * alpha_1_4 * alpha_1_4) ;
	}
	else{/*-------------------------- abs(alpha_3_4) > 1E-8 E.rc_Vol ---------------*/
	  if (abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	    //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_3 = 0
	      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x1 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_3_4 - 2.0) * exp(x3dk) + (alpha_3_4 + 2.0) * exp(x4dk);
	      I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //Si en plus alpha_1_3 neq 0
	      //precision double
	      if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
		x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		I = ((alpha_1_4 + alpha_3_4 + alpha_1_4 * alpha_3_4) / (alpha_1_4 * alpha_1_4 * alpha_3_4 * alpha_3_4)) * exp(x4dk) + exp(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_1)
		  +  exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_3);
		I = I * E.Jac;
	      }
	      //quad precision
	      else{
		//Si en plus alpha_1_3 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
		  +  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	  }//fin if abs(alpha_3_4) > epsi * E.rc_Vol

	  else{//else de if abs(alpha_3_4) > epsi * E.rc_Vol
	    //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_3 = 0
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //Si en plus alpha_1_3 neq 0	     
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
		+  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }//fin else de if abs(alpha_3_4) > epsi * E.rc_Vol
	  
	}/*---------------------------- fin abs(alpha_3_4) > 1E-8 * E.rc_Vol -----------------*/
      }//fin if abs(alpha_1_4 > 1E-8) et abs(alpha_2_4 < 1E-8)

      
      else{//if abs(alpha_2_4) > epsi_II
	if (abs(alpha_2_4) > epsi_II * E.rc_Vol) {
	  if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_2 = 0
	      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
	      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
	      I = (alpha_1_4 + 2.0) * exp(x4dk) + (alpha_1_4 - 2.0) * exp(x1dk);
	      I = (I * E.Jac) / (alpha_1_4 * alpha_1_4 * alpha_1_4);
	    }
	    else{
	      //Si en plus alpha_1_2 neq 0
	      //double precision
	      if (abs(alpha_1_2) > epsi_II * E.rc_Vol) {
		x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		I = ((alpha_1_4 + alpha_2_4 + alpha_1_4 * alpha_2_4) / (alpha_1_4 * alpha_1_4 * alpha_2_4 * alpha_2_4)) * exp(x4dk) + exp(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_1)
		  + exp(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_2);
		I = I * E.Jac;
	      }
	      //quad precision
	      else{	
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
		  + cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }//fin else de if abs(alpha_1_2) < 1E-8 * E.rc_Vol
	  }
	  else{
	    /*--------------cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0------------*/
	    if (abs(alpha_3_4) > epsi_II * E.rc_Vol) {
	      if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 = 0 et alpha_1_3 = 0
		  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		  //I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - exp(x4dk);
		  //I = (I * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
		  I = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * exp(x3dk) - 2.0 * exp(x4dk);
		  I = (I * E.Jac) / (2.0 * alpha_3_4 * alpha_3_4 * alpha_3_4);
		}
		else{
		  //cas alpha_2_3 = 0 et alpha_1_3 neq 0
		  //double precision
		  if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
		    x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		    x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		    x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		    I = ((alpha_3_4 * alpha_1_4 - alpha_1_3 * alpha_1_4 + alpha_1_3 * alpha_1_3) / (alpha_3_4 * alpha_3_4 * alpha_1_3 * alpha_1_3 * alpha_1_4)) * exp(x1dk) - exp(x4dk) / (alpha_3_4 * alpha_3_4 * alpha_1_4) + ((alpha_1_3 - alpha_3_4 - alpha_1_3 * alpha_3_4) / (alpha_1_3 * alpha_1_3 * alpha_3_4 * alpha_3_4)) * exp(x3dk);
		    I = I * E.Jac;
		  }//quad precision
		  else{
		    //cas alpha_2_3 = 0 et alpha_1_3 neq 0		   
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		}
	      }
	      
	      else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
		if (abs(alpha_2_3) > epsi_II * E.rc_Vol) {
		  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		      x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
		      x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
		      x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
		      x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
		      I = exp(x1dk) / (alpha_2_4 * alpha_3_4) + exp(x2dk) / (alpha_2_4 * alpha_2_3)
			- exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4);
		      I = I * E.Jac;
		    }
		    else{
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
		      //double precision
		      if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
			//x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			I = ((alpha_2_3 * alpha_2_4 * alpha_2_4 - alpha_2_4 * alpha_2_4 - alpha_2_3 * alpha_2_3 * alpha_2_4 + alpha_2_3 * alpha_2_3) / (alpha_2_4 * alpha_2_4 * alpha_2_3 * alpha_2_3 * alpha_3_4)) * exp(x2dk) + exp(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_2_4 * alpha_3_4); 
			I = I * E.Jac;
		      }//quad precision
		      else{
			
			//X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
			J = J * E.Jac;
			I = (cplx) J;
		      }
		    }
		  }
		  
		  else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 >1E-8) et abs(alpha_1_2 > 1E-8)
		    if (abs(alpha_1_2) > epsi_II * E.rc_Vol) {
		      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
			//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
			x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			I = ((alpha_3_4 + alpha_1_4 - alpha_2_4) / (alpha_1_4 * alpha_3_4 * alpha_2_3 * alpha_1_2)) * exp(x1dk) - exp(x2dk) / (alpha_2_4 * alpha_1_2 * alpha_2_3) - exp(x3dk) / (alpha_3_4 * alpha_2_3) - exp(x4dk) / (alpha_2_4 * alpha_3_4 * alpha_1_4);
			I = I * E.Jac;
		      }
		      else{
			//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
			//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
			//double precision
			if (abs(alpha_1_3) > epsi_II * E.rc_Vol) {
			  x1dk = cplx(0.0, 1.0) * cblas_ddot(3, x1, 1, k, 1);//i * x1 \cdot k
			  x2dk = cplx(0.0, 1.0) * cblas_ddot(3, x2, 1, k, 1);//i * x2 \cdot k
			  x3dk = cplx(0.0, 1.0) * cblas_ddot(3, x3, 1, k, 1);//i * x3 \cdot k
			  x4dk = cplx(0.0, 1.0) * cblas_ddot(3, x4, 1, k, 1);//i * x4 \cdot k
			  I = E.Jac * ( (exp(x1dk) / (alpha_1_2 * alpha_1_3 * alpha_1_4)) + (exp(x2dk) / (alpha_2_1 * alpha_2_3 * alpha_2_4)) + (exp(x3dk) / (alpha_3_1 * alpha_3_2 * alpha_3_4)) + (exp(x4dk) / (alpha_4_1 * alpha_4_2 * alpha_4_3)) );
			}
			else{
			  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			  J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
			  I = (cplx) J;
			}
		      }
		      
		    }//fin if abs(alpha_1_2) > epsi_II * E.rc_Vol
		    
		    else{//***************else de if abs(alpha_1_2) > epsi_II * E.rc_Vol *******
		      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
			//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		
			X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
			J = J * E.Jac;
			I = (cplx) J;
		      }
		      else{
			//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
			//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		      
			X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
			X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
			X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
			X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
			J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
			I = (cplx) J;
		      }
		    }//************* fin else de if abs(alpha_1_2) > epsi_II * E.rc_Vol *******
		    
		  }//fin else de if abs(alpha_1_2) < 1E-8 * E.rc_Vol
		}//fin if abs(alpha_2_3) > epsi_II * E.rc_Vol


		else{/***************** 1E-8 < abs(alpha_2_3) < epsi_II * E.rc_Vol **********/
		  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
			- cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		    else{
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0		     
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		  }
		  
		  else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 >1E-8) et abs(alpha_1_2 > 1E-8)
		    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		      J = J * E.Jac;
		      I = (cplx) J;
		    }
		    else{
		      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		      
		      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		      J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		      I = (cplx) J;
		    }
		  }
		}/************ **fin 1E-8 < abs(alpha_2_3) < epsi_II * E.rc_Vol *************/
		
	      }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	    }//fin if abs(alpha_3_4) > 1E-4
	    
	    //***************** quad precision avec 1E-8 < abs(alpha_3_4) < 1E-4 *************
	    else{
	      if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 = 0 et alpha_1_3 = 0
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
		  J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
		  I = (cplx) J;
		}
		else{
		  //cas alpha_2_3 = 0 et alpha_1_3 neq 0
		  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
	      }
	      
	      else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
		if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		    //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		      - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		  else{
		    //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		}
		
		else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
		  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		    //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		    J = J * E.Jac;
		    I = (cplx) J;
		  }
		  else{
		    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		    //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		   
		    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		    J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		    I = (cplx) J;
		  }
		}
	      }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	    }//*fin else de abs(alpha_1_4) > 1E-4 abs(alpha_2_4) > 1E-4 if abs(alpha_3_4) > 1E-4 ******************** 
	    
	  }//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4 < 1E-8)
	  
	}//fin if abs(alpha_2_4) > epsi_II
	
	/*-------------------------------1E-8 < abs(alpha_2_4) < epsi_II --------------------------*/
	else{//else de if abs(alpha_2_4) > epsi_II
	  if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      //Si en plus alpha_1_2 = 0
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = (ALPHA_1_4 + 2.0) * cexpq(X4DK) + (ALPHA_1_4 - 2.0) * cexpq(X1DK);
	      J = (J * E.Jac) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4);
	      I = (cplx) J;
	    }
	    else{
	      //Si en plus alpha_1_2 neq 0
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	      X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
		+ cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }
	  else{
	    //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	    if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 = 0 et alpha_1_3 = 0
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
		J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
		I = (cplx) J;
	      }
	      else{
		//cas alpha_2_3 = 0 et alpha_1_3 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	    
	    else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	      if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		    - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
		else{
		  //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	  
		  X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		  X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		 
		  J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		  J = J * E.Jac;
		  I = (cplx) J;
		}
	      }
	      
	      else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
		if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		  //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		 
		 X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		 X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		 X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		 X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		  J = J * E.Jac;
		  I = (cplx) J;
		}
		else{
		  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		  //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		  //cout<<" cas tous non nuls et 1e-8 < alpha_2_4 < "<<epsi_II*E.rc_Vol<<endl;
		  
		 X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		 X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		 X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		 X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		  J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		  I = (cplx) J;
		}
	      }
	    }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	  }//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4)
	}//fin else de if abs(alpha_2_4) > epsi_II
	/*----------------------------- 1E-8 < abs(alpha_2_4) < epsi_II --------------------------*/
	
      }//fin else de abs(alpha_1_4 < 1E-8) et abs(alpha_2_4 < 1E-8)
      
      
    }//fin if abs(alpha_1_4 > epsi_II)



    
    //cas quad precision
    else{//else de if abs(alpha_1_4 > epsi_II)
      if (abs(alpha_2_4) < 1E-8 * E.rc_Vol ){
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	  X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	  X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	  
	  J = cexpq(X1DK) - (ALPHA_1_4 * ALPHA_1_4 + 2.0 * ALPHA_1_4 + 2.0) * 0.5  * cexpq(X4DK);
	  J = (J * E.Jac) /  (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4) ;
	  I = (cplx) J;
	}
	else{
	  //cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	  if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	    //Si en plus alpha_1_3 = 0
	
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    J = (ALPHA_3_4 - 2.0) * cexpq(X3DK) + (ALPHA_3_4 + 2.0) * cexpq(X4DK);
	    J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	    I = (cplx) J;
	  }
	  else{
	    //Si en plus alpha_1_3 neq 0
	  
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_1_4 + ALPHA_3_4 + ALPHA_1_4 * ALPHA_3_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X4DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_1)
	      +  cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_3);
	    J = J * E.Jac;
	    I = (cplx) J;
	  }
	} 
      }//fin if abs(alpha_1_4 > 1E-8) et abs(alpha_2_4 < 1E-8)
      else{
	if(abs(alpha_3_4)  < 1E-8 * E.rc_Vol){
	  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	  if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	    //Si en plus alpha_1_2 = 0
	   
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = (ALPHA_1_4 + 2.0) * cexpq(X4DK) + (ALPHA_1_4 - 2.0) * cexpq(X1DK);
	    J = (J * E.Jac) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_4);
	    I = (cplx) J;
	  }
	  else{
	    //Si en plus alpha_1_2 neq 0
	   
	    X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
	    X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
	    X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	    
	    J = ((ALPHA_1_4 + ALPHA_2_4 + ALPHA_1_4 * ALPHA_2_4) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_2_4 * ALPHA_2_4)) * cexpq(X4DK) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_1)
	      + cexpq(X1DK) / (ALPHA_1_4 * ALPHA_1_4 * ALPHA_1_2);
	    J = J * E.Jac;
	    I = (cplx) J;
	  }
	}
	else{
	  //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	  if(abs(alpha_2_3)  < 1E-8 * E.rc_Vol){
	    if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
	      //cas alpha_2_3 = 0 et alpha_1_3 = 0	    
	     
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = (2.0 - 2.0 * ALPHA_3_4 + ALPHA_3_4 * ALPHA_3_4) * cexpq(X3DK) - cexpq(X4DK);
	      J = (J * E.Jac) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_3_4);
	      I = (cplx) J;
	    }
	    else{
	      //cas alpha_2_3 = 0 et alpha_1_3 neq 0	     
	      X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K	     
	      X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
	      X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
	      
	      J = ((ALPHA_3_4 * ALPHA_1_4 - ALPHA_1_3 * ALPHA_1_4 + ALPHA_1_3 * ALPHA_1_3) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_3 * ALPHA_1_3 * ALPHA_1_4)) * cexpq(X1DK) - cexpq(X4DK) / (ALPHA_3_4 * ALPHA_3_4 * ALPHA_1_4) + ((ALPHA_1_3 - ALPHA_3_4 - ALPHA_1_3 * ALPHA_3_4) / (ALPHA_1_3 * ALPHA_1_3 * ALPHA_3_4 * ALPHA_3_4)) * cexpq(X3DK);
	      J = J * E.Jac;
	      I = (cplx) J;
	    }
	  }
	  
	  else{//if abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 >1E-8)
	    if(abs(alpha_1_2)  < 1E-8 * E.rc_Vol){
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = cexpq(X1DK) / (ALPHA_2_4 * ALPHA_3_4) + cexpq(X2DK) / (ALPHA_2_4 * ALPHA_2_3)
		  - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	      else{
		//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		
		J = ((ALPHA_2_3 * ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_4 * ALPHA_2_4 - ALPHA_2_3 * ALPHA_2_3 * ALPHA_2_4 + ALPHA_2_3 * ALPHA_2_3) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_2_3 * ALPHA_2_3 * ALPHA_3_4)) * cexpq(X2DK) + cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_2_4 * ALPHA_3_4); 
		J = J * E.Jac;
		I = (cplx) J;
	      }
	    }
	    
	    else{//if de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) abs(alpha_2_3 <1E-8) et abs(alpha_1_2 > 1E-8)
	      if(abs(alpha_1_3)  < 1E-8 * E.rc_Vol){
		//cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = ((ALPHA_3_4 + ALPHA_1_4 - ALPHA_2_4) / (ALPHA_1_4 * ALPHA_3_4 * ALPHA_2_3 * ALPHA_1_2)) * cexpq(X1DK) - cexpq(X2DK) / (ALPHA_2_4 * ALPHA_1_2 * ALPHA_2_3) - cexpq(X3DK) / (ALPHA_3_4 * ALPHA_2_3) - cexpq(X4DK) / (ALPHA_2_4 * ALPHA_3_4 * ALPHA_1_4);
		J = J * E.Jac;
		I = (cplx) J;
	      }
	      else{
		//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
		//cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
		
		X1DK = {0.0, dot_u(3, X1, K)};//i * X1\cdot K
		X2DK = {0.0, dot_u(3, X2, K)};//i * X2\cdot K
		X3DK = {0.0, dot_u(3, X3, K)};//i * X3\cdot K
		X4DK = {0.0, dot_u(3, X4, K)};//i * X4\cdot K
		J = E.Jac * ( (cexpq(X1DK) / (ALPHA_1_2 * ALPHA_1_3 * ALPHA_1_4)) + (cexpq(X2DK) / (ALPHA_2_1 * ALPHA_2_3 * ALPHA_2_4)) + (cexpq(X3DK) / (ALPHA_3_1 * ALPHA_3_2 * ALPHA_3_4)) + (cexpq(X4DK) / (ALPHA_4_1 * ALPHA_4_2 * ALPHA_4_3)) );
		I = (cplx) J;
	      }
	    }
	  }//else abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) abs(alpha_3_4) et abs(alpha_2_3 <1E-8)
	}//fin else de abs(alpha_1_4 < 1E-8) abs(alpha_2_4 < 1E-8) et abs(alpha_3_4)
      }//fin else de abs(alpha_1_4 < 1E-8) et abs(alpha_2_4 < 1E-8)
    }//fin else de if abs(alpha_1_4 > epsi_II)


    
    
  }//fin else de abs(alpha_1_4 < 1E-8)
  
  
  delete [] x1x2; x1x2 = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x1x4; x1x4 = 0;
  
  delete [] x2x3; x2x3 = 0;
  delete [] x2x4; x2x4 = 0;
  delete [] x3x4; x3x4 = 0;

  delete [] X1X2; X1X2 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] X1X4; X1X4 = 0;
  
  delete [] X2X3; X2X3 = 0;
  delete [] X2X4; X2X4 = 0;
  delete [] X3X4; X3X4 = 0;

  delete [] k; k = 0;

  delete [] X1; X1 = 0;
  delete [] X2; X2 = 0;
  delete [] X3; X3 = 0;
  delete [] X4; X4 = 0;
  delete [] K; K = 0;
  return I;
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
cplx integrale_tetra_quad_precision(Element &E, Onde_plane &P1, Onde_plane &P2){
  cplx I = cplx(0.0, 0.0);
  __complex128 J = {(__float128)0.0, (__float128)0.0};
  __complex128 alpha_1_2, alpha_1_3, alpha_1_4;
  __complex128 alpha_2_1, alpha_2_3, alpha_2_4;
  __complex128 alpha_3_1, alpha_3_2, alpha_3_4;
  __complex128 alpha_4_1, alpha_4_2, alpha_4_3;
  __complex128 x1dk, x2dk, x3dk, x4dk;

  __float128 *x1, *x2, *x3, *x4;

  x1 = new __float128[3];
  x2 = new __float128[3];
  x3 = new __float128[3];
  x4 = new __float128[3];
  
  __float128 *x1x2 = new __float128[3];// x1 - x2
  __float128 *x1x3 = new __float128[3];// x1 - x3
  __float128 *x1x4 = new __float128[3];// x1 - x4

  __float128 *x2x3 = new __float128[3];// x2 - x3
  __float128 *x2x4 = new __float128[3];// x2 - x4
  __float128 *x3x4 = new __float128[3];// x3 - x4

  __float128 *k = new __float128[3];

  double KIJ;
  
  MKL_INT i;
  
  for(i = 0; i < 3; i++) {
    
    x1[i] = (__float128)(E.noeud[0]->get_X())[i];
    x2[i] = (__float128)(E.noeud[1]->get_X())[i];
    x3[i] = (__float128)(E.noeud[2]->get_X())[i];
    x4[i] = (__float128)(E.noeud[3]->get_X())[i];
  }
  
  for(i = 0; i < 3; i++) {
    x1x2[i] = x1[i] - x2[i];
    x1x3[i] = x1[i] - x3[i];
    x1x4[i] = x1[i] - x4[i];
    
    x2x3[i] = x2[i] - x3[i];
    x2x4[i] = x2[i] - x4[i];
    x3x4[i] = x3[i] - x4[i];

    KIJ = P2.K[i] - P1.K[i];
    
    k[i] = (__float128) KIJ;
  }
  alpha_1_2 = {0.0, dot_u(3, x1x2, k)};//i * (x1 - x2) \cdot k
  alpha_1_3 = {0.0, dot_u(3, x1x3, k)};//i * (x1 - x3) \cdot k
  alpha_1_4 = {0.0, dot_u(3, x1x4, k)};//i * (x1 - x4) \cdot k

  alpha_2_3 = {0.0, dot_u(3, x2x3, k)};//i * (x2 - x3) \cdot k
  alpha_2_4 = {0.0, dot_u(3, x2x4, k)};//i * (x2 - x4) \cdot k
  alpha_3_4 = {0.0, dot_u(3, x3x4, k)};//i * (x3 - x4) \cdot k

  alpha_2_1 = - alpha_1_2;
  alpha_3_1 = - alpha_1_3;
  alpha_4_1 = - alpha_1_4;
  
  alpha_3_2 = - alpha_2_3;
  alpha_4_2 = - alpha_2_4;
  alpha_4_3 = - alpha_3_4;
  
  double epsi = 1E-8 * E.rc_Vol;

 
  if (cabsq(alpha_1_4) < epsi){
    if (cabsq(alpha_2_4) < epsi ){
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	
	J = E.Vol * cexpq(x4dk); 
      }

      else{
	//cas alpha_1_4 = 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	J = cexpq(x3dk) - (alpha_3_4 * alpha_3_4 + 2.0 * alpha_3_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_3_4 * alpha_3_4 * alpha_3_4) ;
      }
    }

    else{
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 = 0 alpha_3_4 = 0 et alpha_2_4 neq 0
	x2dk = {0.0, dot_u(3, x2, k)};//i * x3 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	J = cexpq(x2dk) - (alpha_2_4 * alpha_2_4 + 2.0 * alpha_2_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_2_4 * alpha_2_4 * alpha_2_4) ;
      }
      
      else{
	//cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0
	if (cabsq(alpha_2_3) < epsi){
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 = 0
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_3_4 - 2.0) * cexpq(x3dk) + (alpha_3_4 + 2.0) * cexpq(x4dk);
	  J = (J * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  //cas alpha_1_4 = 0 alpha_3_4 neq 0 et alpha_2_4 neq 0 et alpha_2_3 neq 0
	  x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k

	  J = ((alpha_2_4  + alpha_3_4 + alpha_2_4 * alpha_3_4) / (alpha_2_4 * alpha_2_4 * alpha_3_4 * alpha_3_4)) * cexpq(x4dk) + cexpq(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_2)
	    + cexpq(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_3);
	  J = (J * E.Jac);
	}
      }
      
    }

    
  }
  
  else{
    if (cabsq(alpha_2_4) < epsi ){
      if(cabsq(alpha_3_4)  < epsi){
	//cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 = 0
	x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	
	J = cexpq(x1dk) - (alpha_1_4 * alpha_1_4 + 2.0 * alpha_1_4 + 2.0) * 0.5  * cexpq(x4dk);
	J = (J * E.Jac) /  (alpha_1_4 * alpha_1_4 * alpha_1_4) ;
      }
      else{
	//cas alpha_1_4 neq 0 alpha_2_4 = 0 et alpha_3_4 neq 0
	if(cabsq(alpha_1_3)  < epsi){
	  //Si en plus alpha_1_3 = 0
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_3_4 - 2.0) * cexpq(x3dk) + (alpha_3_4 + 2.0) * cexpq(x4dk);
	  J = (J * E.Jac) / (alpha_3_4 * alpha_3_4 * alpha_3_4);
	}
	else{
	  //Si en plus alpha_1_3 neq 0	 
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  
	  J = ((alpha_1_4 + alpha_3_4 + alpha_1_4 * alpha_3_4) / (alpha_1_4 * alpha_1_4 * alpha_3_4 * alpha_3_4)) * cexpq(x4dk) + cexpq(x3dk) / (alpha_3_4 * alpha_3_4 * alpha_3_1)
	   +  cexpq(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_3);
	  J = J * E.Jac;
	}
      } 
    }
    else{
      if(cabsq(alpha_3_4)  < epsi){
      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 = 0
	if(cabsq(alpha_1_2)  < epsi){
	  //Si en plus alpha_1_2 = 0
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  J = (alpha_1_4 + 2.0) * cexpq(x4dk) + (alpha_1_4 - 2.0) * cexpq(x1dk);
	  J = (J * E.Jac) / (alpha_1_4 * alpha_1_4 * alpha_1_4);
	}
	else{
	  //Si en plus alpha_1_2 neq 0	 
	  x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	  x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	  x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	  
	  J = ((alpha_1_4 + alpha_2_4 + alpha_1_4 * alpha_2_4) / (alpha_1_4 * alpha_1_4 * alpha_2_4 * alpha_2_4)) * cexpq(x4dk) + cexpq(x2dk) / (alpha_2_4 * alpha_2_4 * alpha_2_1)
	    + cexpq(x1dk) / (alpha_1_4 * alpha_1_4 * alpha_1_2);
	  J = J * E.Jac;
	}
      }
      else{
	//cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	if(cabsq(alpha_2_3)  < epsi){
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 = 0 et alpha_1_3 = 0
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = (2.0 - 2.0 * alpha_3_4 + alpha_3_4 * alpha_3_4) * cexpq(x3dk) - 2.0 * cexpq(x4dk);
	      J = (J * E.Jac) / (2.0 * alpha_3_4 * alpha_3_4 * alpha_3_4);
	    }
	    else{
	      //cas alpha_2_3 = 0 et alpha_1_3 neq 0
	      x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = ((alpha_3_4 * alpha_1_4 - alpha_1_3 * alpha_1_4 + alpha_1_3 * alpha_1_3) / (alpha_3_4 * alpha_3_4 * alpha_1_3 * alpha_1_3 * alpha_1_4)) * cexpq(x1dk) - cexpq(x4dk) / (alpha_3_4 * alpha_3_4 * alpha_1_4) + ((alpha_1_3 - alpha_3_4 - alpha_1_3 * alpha_3_4) / (alpha_1_3 * alpha_1_3 * alpha_3_4 * alpha_3_4)) * cexpq(x3dk);
	      J = J * E.Jac;
	    }
	}
	
	else{
	  if(cabsq(alpha_1_2)  < epsi){
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 = 0	    
	       x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	       x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	       x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	       x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	       
	      J = cexpq(x1dk) / (alpha_2_4 * alpha_3_4) + cexpq(x2dk) / (alpha_2_4 * alpha_2_3)
		- cexpq(x3dk) / (alpha_3_4 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_3_4);
	      J = J * E.Jac;
	    }
	    else{
	      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 = 0
	    
	      //x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      
	      J = ((alpha_2_3 * alpha_2_4 * alpha_2_4 - alpha_2_4 * alpha_2_4 - alpha_2_3 * alpha_2_3 * alpha_2_4 + alpha_2_3 * alpha_2_3) / (alpha_2_4 * alpha_2_4 * alpha_2_3 * alpha_2_3 * alpha_3_4)) * cexpq(x2dk) + cexpq(x3dk) / (alpha_3_4 * alpha_2_3 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_2_4 * alpha_3_4); 
	      J = J * E.Jac;
	    }
	  }
	  
	  else{
	    if(cabsq(alpha_1_3)  < epsi){
	      //cas alpha_2_3 neq 0 et alpha_1_3 = 0 et alpha_1_2 neq 0	    
	       x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	       x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	       x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	       x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      
	      J = ((alpha_3_4 + alpha_1_4 - alpha_2_4) / (alpha_1_4 * alpha_3_4 * alpha_2_3 * alpha_1_2)) * cexpq(x1dk) - cexpq(x2dk) / (alpha_2_4 * alpha_1_2 * alpha_2_3) - cexpq(x3dk) / (alpha_3_4 * alpha_2_3) - cexpq(x4dk) / (alpha_2_4 * alpha_3_4 * alpha_1_4);
	      J = J * E.Jac;
	    }
	    else{
	      //cas alpha_1_4 neq 0 alpha_2_4 neq 0 et alpha_3_4 neq 0
	      //cas alpha_2_3 neq 0 et alpha_1_3 neq 0 et alpha_1_2 neq 0
	   
	      x1dk = {0.0, dot_u(3, x1, k)};//i * x1 \cdot k
	      x2dk = {0.0, dot_u(3, x2, k)};//i * x2 \cdot k
	      x3dk = {0.0, dot_u(3, x3, k)};//i * x3 \cdot k
	      x4dk = {0.0, dot_u(3, x4, k)};//i * x4 \cdot k
	      J = E.Jac * ( (cexpq(x1dk) / (alpha_1_2 * alpha_1_3 * alpha_1_4)) + (cexpq(x2dk) / (alpha_2_1 * alpha_2_3 * alpha_2_4)) + (cexpq(x3dk) / (alpha_3_1 * alpha_3_2 * alpha_3_4)) + (cexpq(x4dk) / (alpha_4_1 * alpha_4_2 * alpha_4_3)) );
	    }
	  }
	}
      }
    }
  }
  
  I = (cplx) J;
  
  delete [] x1; x1 = 0;
  delete [] x2; x2 = 0;
  delete [] x3; x3 = 0;
  delete [] x4; x4 = 0;
  
  delete [] x1x2; x1x2 = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x1x4; x1x4 = 0;
  
  delete [] x2x3; x2x3 = 0;
  delete [] x2x4; x2x4 = 0;
  delete [] x3x4; x3x4 = 0;
  
  delete [] k; k = 0;
  return I;
}


#endif
cplx integrale_tetra(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]){
  //Cette fonction calcul l'integrale de deux ondes planes sur un tetraedre, en double precision ou quad precision
  /*------------------------------------------------ input --------------------------------------
    E            : objet de type Element, il contient les coordonnées de l'element
    iloc, jloc   : des indices de fonctions de bases
    chaine       : chaine de caractère définissant le choix de précision des intégrateurs.
          Si chaine = "double", dans ce cas on calcule l'intégrale de P1 et P2 en double précision
	  Sinon, le calcul est effectué en quad précision
    /*--------------------------------------------- output -------------------------------------*/
  cplx I = cplx(0.0, 0.0);
  
  if(chaine == "double") {
    I =  integrale_tetra_quad(E, iloc, jloc);
  }
 #ifdef USE_QUADMATH
  else{

    I =  integrale_tetra_quad_precision(E, iloc, jloc);
  }
#endif  
  return I;
}


cplx integrale_tetra_VV(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) {
  cplx I, J;
  double dd;
  MKL_INT lda = 3, inc = 1;

  //dd = ((A * k * diloc)/omega) \cdot ((A * k * djloc) / omega) 
  dd = cblas_ddot(lda, E.OP[iloc].Ad, inc, E.OP[jloc].Ad, inc) ;
  dd = dd * E.OP[iloc].ksurOmega * E.OP[jloc].ksurOmega;
  
  /* integrale de conj(P_iloc) P_jloc */
  J = integrale_tetra(E, iloc, jloc, chaine);
  
  I = dd * J;

  return I;
  
}


cplx integrale_tetra(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]){
  //Cette fonction calcul l'integrale de deux ondes planes sur un tetraedre, en double precision ou quad precision
  /*------------------------------------------------ input --------------------------------------
    E            : objet de type Element, il contient les coordonnées de l'element
    iloc, jloc   : des indices de fonctions de bases
    chaine       : chaine de caractère définissant le choix de précision des intégrateurs.
          Si chaine = "double", dans ce cas on calcule l'intégrale de P1 et P2 en double précision
	  Sinon, le calcul est effectué en quad précision
    /*--------------------------------------------- output -------------------------------------*/
  cplx I = cplx(0.0, 0.0);
  
  if(chaine == "double") {
    I =  integrale_tetra_quad(E, P1, P2);
  }
 #ifdef USE_QUADMATH
  else{

    I =  integrale_tetra_quad_precision(E, P1, P2);
  }
#endif  
  return I;
}

cplx integrale_tetra_VV(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]) {
  cplx I, J;
  double dd;
  MKL_INT lda = 3, inc = 1;

  //dd = ((A * k * P1.d)/omega) \cdot ((A * k * P2.d) / omega) 
  dd = cblas_ddot(lda, P1.Ad, inc, P2.Ad, inc) ;
  dd = dd * P1.ksurOmega * P2.ksurOmega;
  
  /* integrale de conj(P1) P2 */
  J = integrale_tetra(E, P1, P2, chaine);
  
  I = dd * J;

  return I;
  
}

/*--------------------------------------------------------------------------------------------------------------------------*/
#ifdef USE_QUADMATH
__complex128 eval_cexpq(__float128 x){
  //Cette fonction donne l'approximation polynomiale de la fonction (exp(ix) - 1 - ix) / x 
  __complex128 res = {-x / 2.0, (__float128)0.0};
  __complex128 tn = {(__float128)1.0 ,(__float128)0.0};
  __complex128 One = {(__float128)1.0, (__float128)0.0};
  __complex128 xc = {(__float128)0.0, x};
  __complex128 OneX = {1.0, x};
  if (fabsq(x) < 0.1) {
  for(MKL_INT n = 10; n >=3; n--){
    tn = One + (xc / (__float128) n) * tn;
  }
  tn = res * tn;
  }
  else {
    tn = (cexpq(xc) - OneX) / x;
  }
  return tn;
}


cplx integrale_triangle_quad_precision(Face &F, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction représente l'intégrale sur face du produit de deux ondes planes
  /*------------------------------------------------------------------ input -----------------------------------------------------------------------------

    F      : objet de type Face. Elle contient la face sur laquelle l'intégrale est effectuée
    P1, P2 : objets de type Onde_plane

    /*---------------------------------------------------------------- ouput ----------------------------------------------------------------------------

    I   : le résultat de l'intégrale 

    /*--------------------------------------------------------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0, 0.0);
  __complex128 J = {(__float128)0.0, (__float128)0.0};
  
  __float128  beta_1, beta_2, beta_3, X1dk, X2dk, X3dk;
  
  __float128 *X1 = new __float128[3];//neoud numero 1 de la face
  __float128 *X2 = new __float128[3];//............ 2 ..........
  __float128 *X3 = new __float128[3];//............ 3 ..........
  
  __float128 *X3X1 = new __float128[3];// X3 - X1
  __float128 *X2X3 = new __float128[3];// X2 - X3
  __float128 *X1X2 = new __float128[3];// X1 - X2
  __float128 *X1X3 = new __float128[3];// X1 - X3
  __float128 *k = new __float128[3];

  double kij;

  MKL_INT i;

   for (i = 0; i < 3; i++){
     X1[i] = (__float128) (F.noeud[0]->get_X())[i];
     X2[i] = (__float128) (F.noeud[1]->get_X())[i];
     X3[i] = (__float128) (F.noeud[2]->get_X())[i];
     kij = P2.K[i] - P1.K[i];
     k[i] = (__float128) kij;
   }
  
  double epsi = 1E-8;
  for (i = 0; i < 3; i++){
    
    X3X1[i] = X3[i] - X1[i];
    
    X1X3[i] = - X3X1[i];
    
    X2X3[i] = X2[i] - X3[i];
    
    X1X2[i] = X1[i] - X2[i];
    
  }
    
  beta_1 = dot_u(3, X2X3, k);//X2X3 \cdot k
  
  beta_2 = dot_u(3, X3X1, k);//X3X1 \cdot k
  
  beta_3 = dot_u(3, X1X2, k);//X1X2 \cdot k
 
  if (fabsq(beta_1) / epsi < 1){
    if (fabsq(beta_2) / epsi < 1 ){
      //cas beta_1 = 0 et beta_2 = 0
      X3dk = dot_u(3, X3, k);
     
      J =  0.5 * F.Aire * cexpq({(__float128)0.0 ,X3dk});
    }
    else{
      //cas beta_1 = 0 et beta_2 neq 0
      X3dk = dot_u(3, X3, k);
      
      J = (F.Aire * cexpq({(__float128)0.0, X3dk}) / beta_2) * eval_cexpq(beta_3);
    }
  }
  else{
    if (fabsq(beta_2) / epsi < 1){
      //cas beta_1 neq 0 et beta_2 = 0
      X1dk = dot_u(3, X1, k);

      J =  (F.Aire * cexpq({(__float128)0.0, X1dk}) / beta_3) * eval_cexpq(beta_1); 
    }
    else{
      if (fabsq(beta_3) / epsi < 1 ){
	//cas beta_1 neq 0 et beta_2 neq 0 et beta_3 = 0
	X2dk = dot_u(3, X2, k);
	
	J = (F.Aire * cexpq({(__float128)0.0, X2dk}) / beta_1) * eval_cexpq(beta_2); 
      }
      else{
	//cas beta_1 neq 0 et beta_2 neq 0 et beta_3 neq 0
	X1dk = dot_u(3, X1, k);
	
	J = (F.Aire * cexpq({(__float128)0.0, X1dk}) / beta_1) * (eval_cexpq(beta_2) - eval_cexpq(-beta_3));
      }
      
    }
  }

  I = (cplx) J;
   
  //libération de la mémoire
  delete [] X1; X1 = 0;
  delete [] X2; X2 = 0;
  delete [] X3; X3 = 0;
  delete [] X1X2; X1X2 = 0;
  delete [] X1X3; X1X3 = 0;
  delete [] X3X1; X3X1 = 0;
  delete [] X2X3; X2X3 = 0;
  delete [] k; k = 0;

   
   return I;
}

#endif
cplx integrale_triangle(Face &F, Onde_plane &P1, Onde_plane &P2, const char chaine[]){
  //Cette fonction calcul l'integrale de deux ondes planes P1 et P2 sur une face F, en double precision ou quad precision
  /*------------------------------------------------ input --------------------------------------
    F      : objet de type Face, il contient les coordonnées de la face F
    P1     : classe de type Onde_plane, elle contient toutes les informations sur l'onde plane P1
    P2     : ................................................................................. P2
    chaine : chaine de caractère définissant le choix de précision des intégrateurs.
          Si chaine = "double", dans ce cas on calcule l'intégrale de P1 et P2 en double précision
	  Sinon, le calcul est effectué en quad précision
    /*--------------------------------------------- output -------------------------------------*/
  cplx I = cplx(0.0, 0.0);
  
  if(chaine == "double") {
    I =  integrale_triangle(F, P1, P2);
  }
 #ifdef  USE_QUADMATH
  else{
    I =  integrale_triangle_quad_precision(F, P1, P2);
  }
 #endif  
  return I;
}


void debugg_integral_triangle(MailleSphere &MSph){
  cplx I1, I2;
  
  double diff;
  
  MKL_INT i, e, iloc, jloc, l;
  
  e =5; l = 0;
  iloc = 23; jloc = 14;

  double *X1, *X2, *X3, beta_1, beta_2, beta_3;

  double *X3X1 = new double[3];// X3 - X1
  double *X2X3 = new double[3];// X2 - X3
  double *X1X2 = new double[3];// X1 - X2
  double *X1X3 = new double[3];// X1 - X3
  double *k = new double[3];
  
   X1 = MSph.elements[e].face[l].noeud[0]->get_X();
   X2 = MSph.elements[e].face[l].noeud[1]->get_X();
   X3 = MSph.elements[e].face[l].noeud[2]->get_X(); 
  
  double epsi = 1E-10;
  for (i = 0; i < 3; i++){
    
    X3X1[i] = X3[i] - X1[i];
    
    X1X3[i] = - X3X1[i];
    
    X2X3[i] = X2[i] - X3[i];
    
    X1X2[i] = X1[i] - X2[i];

    k[i] = MSph.elements[e].OP[jloc].K[i] - MSph.elements[e].OP[iloc].K[i];
    
  }
    
  beta_1 = cblas_ddot(3, X2X3, 1, k, 1);//X2X3 \cdot k
  
  beta_2 = cblas_ddot(3, X3X1, 1, k, 1);//X3X1 \cdot k
  
  beta_3 = cblas_ddot(3, X1X2, 1, k, 1);//X1X2 \cdot k
  double val, C1, C2, divide;
  double maxx = 0.0, minn = 1000.0;
  double maxx_g = 0.0, minn_g = 1000.0;

  C1 = 0.0; C2 = 0.0; divide = 1.0 / 1000.0;
  cout<< " beta_1 = "<<beta_1<< " beta_2 = "<<beta_2<< " beta_3 = "<<beta_3<<endl;
  
  FILE* fic;
  if( !(fic = fopen("/home/ibrahima/Documents/sauvarge-26-06-23/NewResults/quad_triangle.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }

  for(e = 0; e < MSph.get_N_elements(); e++) {
    for(l = 0; l < 4; l++) {
      
      maxx = 0.0, minn = 1000.0;
      for(iloc = 0; iloc < 26; iloc++){
	for(jloc = 0; jloc < 26; jloc++){
	  //cout<<" Analytique"<<endl;
	  auto t1 = std::chrono::high_resolution_clock::now();
	  I2 = integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], "quad");
	  auto t2 = std::chrono::high_resolution_clock::now();
	  
	  std::chrono::duration<double, std::milli> float_M1 = t2 - t1;
	  
	  C1 += float_M1.count();
	  //cout<<" numerique"<<endl;
	  auto t3 = std::chrono::high_resolution_clock::now();
	  I1 = integrale_triangle(MSph.elements[e].face[l], MSph.Qd2D, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
	  auto t4 = std::chrono::high_resolution_clock::now();
	  
	  std::chrono::duration<double, std::milli> float_M2 = t4 - t3;
	  
	  C2 += float_M2.count();
	  
	  if (abs(I1-I2) < 1E-10){
	    val = 0.0;
	  }
	  else{
	    val = abs(I1-I2);
	    }
	  
	  //cout<<"iloc = "<<iloc<<" jloc = "<<jloc<<" I1 = "<<I1<<" I2 = "<<I2<<"   abs(I1 - I2) = "<<val<<endl;
	  maxx = max(maxx, abs(I1-I2));
	  minn = min(minn, abs(I1-I2));
	}
      }
      
      maxx_g = max(maxx_g, maxx);
      minn_g = min(minn_g, minn);
      if ((maxx >1E-10) || (minn > 1E-10)){
	cout<<"e = "<<e<<" l = "<<l<<" maxx_e = "<<maxx<<" minn_e = "<<minn<<endl;
      fprintf(fic," %d\t %d\t %10.15e\t %10.15e\n",e, l, maxx, minn);
      }
    }
  }
  
  cout<<" maxx = "<<maxx_g<<" minn = "<<minn_g<<endl;
  cout<<" temps integral analytique = "<<C1 * divide<<" secs"<<endl;
  cout<<" temps integral numerique = "<<C2 * divide<<" secs"<<endl;
  fclose(fic);
}
/*-----------------------------------------------------------------------------------------------------------------------*/
void exporte_critere_filtrage_tetra_to_file(MailleSphere &MSph){
  MKL_INT e;
  FILE *fic1;
  FILE *fic2;

  if( !(fic1 = fopen("/home/ibrahima/Documents/sauvarge-26-06-23/NewResults/critere_volume.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  if( !(fic2 = fopen("/home/ibrahima/Documents/sauvarge-26-06-23/NewResults/critere_surface.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }

  for(e = 0; e < MSph.get_N_elements(); e++) {
    
    fprintf(fic1,"%10.15e\n", MSph.elements[e].Vol);
    fprintf(fic2,"%10.15e\n", pow(MSph.elements[e].Vol, 2.0 / 3.0));
    
  }
}


/*-------------------------------------------------------------------------------------------------------------------------------------------*/
MKL_INT Read_number_of_elements(char chaine[]){
  MKL_INT np;
  ifstream fic(chaine);
  if(fic) {
      fic>>np;
  }
  else{
    cout<<"probleme d''ouverture du fichier points"<<"\n";
  }

  return np;
}


void Read_Points(char chaine[], MKL_INT np, double *P){
  //lecteur du fichier contenant les noeuds du maillage 
  MKL_INT i, n, l;
  double x1, x2, x3;
  ifstream fic(chaine);
  if(fic) {
    fic>>n;
    cout<< " n = "<<n<<endl;
      for(i = 0; i<np; i++){
		fic >> x1 >> x2 >> x3 ;
		l = 3*i;
		P[l] = x1; 
		P[l+1] = x2; 
		P[l+2] = x3;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier points"<<"\n";
  }
}


void Read_Tetra(char chaine[], MKL_INT ne, MKL_INT *T){
  //lecture des tetras à partir d'un fichier
  MKL_INT i, n, l;
  MKL_INT x1, x2, x3, x4;
  ifstream fic(chaine);
  if(fic)
    {
      fic>>n;
     
      for(i = 0; i<n; i++){
	fic>>x1>>x2>>x3>>x4;
	l = 4*i;
	T[l] = x1-1; T[l+1] = x2-1; T[l+2] = x3-1; T[l+3] = x4-1;
      }
    }
  else{
    cout<<"probleme d''ouverture du fichier tetra"<<"\n";
  }

}
void spy_to_python(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]){
  MKL_INT i, nnz, n;

  n = MSph.get_N_elements() * MSph.ndof;
  
  nnz = compute_nnz_for_Sparse_COO_format(A_glob, MSph);

  
  MKL_INT *rows = new MKL_INT[nnz];
  MKL_INT *cols = new MKL_INT[nnz];
  cplx *values = new cplx[nnz];
  
  FILE*  fic;
  
  char *file;
  
  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");
  
  transforme_A_skyline_to_Sparse_COO_format(A_glob, rows, cols, values, MSph);
  
  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);

  }

  cout<<" < ecriture des donnees de la matrice sparse dans un fichier > "<<endl;
  fprintf(fic, "%d %d\n", n, nnz);

  for(i = 0; i < nnz; i++) {
    fprintf(fic, "%d %d %10.15e\n",rows[i]-1, cols[i]-1, abs(values[i]));
  }
  fclose(fic);

  cout<<" fin d'ecriture ... "<<endl;
  system("python3 spy.py");
  
  free(file); file = NULL;
  
  delete [] rows; rows = 0;
  delete [] cols; cols = 0;
  delete [] values; values = 0;
  
}



void spy_to_python(A_skyline &A_glob, Maillage &Mesh, const char chaine[]){
  MKL_INT i, nnz, n;

  n = Mesh.get_N_elements() * Mesh.ndof;
  
  nnz = compute_nnz(A_glob, Mesh);

  
  MKL_INT *rows = new MKL_INT[nnz];
  MKL_INT *cols = new MKL_INT[nnz];
  cplx *values = new cplx[nnz];
  
  FILE*  fic;
  
  char *file;
  
  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");
  
  transforme_A_skyline_to_Sparse_COO_format(A_glob, rows, cols, values, Mesh);
  
  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);

  }

  cout<<" < ecriture des donnees de la matrice sparse dans un fichier > "<<endl;
  fprintf(fic, "%d %d\n", n, nnz);

  for(i = 0; i < nnz; i++) {
    fprintf(fic, "%d %d %10.15e\n",rows[i]-1, cols[i]-1, abs(values[i]));
  }
  fclose(fic);

  cout<<" fin d'ecriture ... "<<endl;
  // system("python3 spy.py");
  
  free(file); file = NULL;
  
  delete [] rows; rows = 0;
  delete [] cols; cols = 0;
  delete [] values; values = 0;
  
}
