#include "Header.h"
using namespace std;


int main(int argc, char *argv[]) {
/*-----------------------------------------------------------------------------------------------------
    argv[0] : le nom de l'executable
    argv[1] : le nom du fichier des noeuds du maillage
    argv[2] : le fichier des indices des tétras
    argv[3] : le nom du tableau de connectivité des voisins des tétras
    argv[4] : chaine de caractères pour désigner le nom de la méthode Trefftz à considérer
              si argv[4] == cess-des, c'est la méthode de Cessenat-Desprès
	      sinon c'est la méthode de Décomposition spectrale
    argv[5] : chaine pour désigner l'espace des phases des bases de Trefftz
              si argv[5] == "sphere", l'espace est construit en discrétisant la sphère unité
              sinon, il est obtenu par discrétisant du cube unité
    argv[6] : contiendra le nom du fichier contenant les directions des ondes planes
                si argv[5] == "sphere". Et si argv[5] != "sphere" alors argv[6] doit 
                 contenir un entier nous permettant de définir la dimension de l'espace des phases
    argv[7] : argument pour définir la taille de l'élément virtuel. Ce dernier est utilisé pour
                construire une matrice de préconditionnement.
                argv[7] == "hmin" dans ce cas la taille de l'élément est
                  hmin =  min_{hmin, K un element}. Sinon hmax = max_{hmax, K un element} 
    argv[8] : contient le seuil de filtrage des espaces d'ondes planes par un TSVD
    argv[9] : ..................régularisation par Tikhonov lors des décompositions
               spectrales pour former la matrice du système linéaire de notre méthode spectrale
    argv[10] : stocke un entier pour désignant le type de filtre à considérer 
                 sur chaque élément |T| du maillage.
              Si argv[10] == "0", alors c'est un filtre surfacique (argv[8] * |T|^{2/3}).
              Sinon c'est un filtre volumique (argv[8] * |T|). |T| est le volume de l'élément T.
    argv[11] : le type de métrique choisi. Si argv[11] == "surfacique", alors 
                c'est une métrique surfacique. Sinon c'est volumique
    argv[12] : chaine de caractère désignant le type de solveur à utiliser pour la résolution.
               Si argv[12] == "lu", alors c'est par la méthode LU de inter pardiso.
	       Sinon, c'est par une méthode de GMRES.
    argv[13] : le nom du fichier où stocké à chaque itération le résidu relatif du GMRES,
                ainsi que les normes UWVF et DG.
    argv[14] : pour déterminer le type d'opération à effectuer, c'est-à-dire 
               - si argv[14] = "assemblage", on assemble la matrice puis on stocke la version sparse
                   dans un (des) fichier(s). Pour cela, si argv[12] == "lu", on transforme
                    la matrice en sparse CSR et elle est stockée comme suit:
		    * un fichier du nom de "lu_mat_rows.txt" pour les indices de lignes
		    * .................... "lu_mat_vals_&_cols.txt" pour les indices de colonnes
                                                      et les non zeros correspondant.
	       - si argv[14] = "compose", alors elle est transformée en sparse COO puis écrite dans
	              un fichier du nom de compose_mat.txt
                si argv[14] = solve, on résout le système associé on lisant la matrice contenu
                   dans un (des) fichier(s). 
		   ----------------------------------------------------------------------------------------------------*/

  if (argc != 16) {
  cout<<" Usage:"<<argv[0]<<" filein fileout\n"<<endl;
  return(EXIT_FAILURE);
 }
 else{
   MKL_INT nb_taches; 
#pragma omp parallel
   {
     nb_taches = omp_get_num_threads();
   }
   cout<<" execution du programme avec "<<nb_taches<<" thread(s)"<<endl;
   

   if (strcmp(argv[14], "assemblage") == 0){
     //Assembly_optimized_version_of_Spectral_Methode_regulated_with_VirtualElement_and_write_in_file(argv, "double");
     
     char **argw;
     int arcw = 0;
     MPI_Init(&arcw, &argw);
     Assembly_distributed_Spectral_linear_method_regulated_with_Virtual_Element_and_write_it_on_file(argv, "double");
     //Assembly_distributed_linear_Cessenat_method_and_write_it_on_file(argv, "double");
     MPI_Finalize();
     
     
   }
   else if (strcmp(argv[14], "solve") == 0){
     
     // Solve_optimized_version_of_Spectral_Methode_regulated_with_VirtualElement(argv, 2000000, "double");
     
     char **argw;
     int arcw = 0;
     MPI_Init(&arcw, &argw);
     Solve_distributed_Spectral_linear_method_regulated_with_Virtual_Element(argv, "double");
     //Solve_distributed_linear_Cessenat_method_loaded_from_file(argv, "double");
     MPI_Finalize();
     
   }
   
   // debugg_Reciprocite_matrix(argv, "double");
 }
  return 0;
  
}
