#include "Header.h"

using namespace std;

void Distribute_and_Solve_full_linear_system(char *argv[]) {
#if defined(USE_COMPOSE)
  double divide = 1.0 / 1000.0;
  
  
  MKL_INT np, N_elements, ndof, e, n;
  
  //On récupère la taille du maillage dans N_elements
  //et la dimension de la base Trefftz sur chaque élément dans ndof
  get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);
  
  //pointeur pour le second du système linéaire
  cplx *B = nullptr;
  
  //la taille du système linéaire est initialiser à 0
  n = 0;
  
  int rank = MMPI::rank();
  
  //initialisation de la structure sparse coo
  std::size_t compose_n = 0;
  std::size_t compose_nnz = 0;
  std::vector<int> compose_i(0);
  std::vector<int> compose_j(0);
  std::vector<cplx> compose_v(0);

  double start_time = MPI_Wtime();
  
  if (rank == 0){
    
    MKL_INT nnz ;
    
    char *path_matrix = NULL;
    path_matrix = (char *) calloc(strlen(argv[13]) + 5, sizeof(char));
    strcpy(path_matrix, argv[13]);
    strcat(path_matrix,".txt");
    
    cout<<" path_matrix = "<<path_matrix<<endl;

    //lecture du nnz et de la taille du système linéaire
    read_size_of_Sparse_COO_matrix_from_file(path_matrix, &nnz, &n);
    cout<<" nnz = "<<nnz<<" n = "<<n<<endl;       
    
    
    compose_n = n;
    compose_nnz = nnz;
    cout<<" compose_nnz = "<<compose_nnz<<" compose_n = "<<compose_n<<endl;
    using namespace composyx;
    
    
    // resize de la structure sparse coo dans le processus master
    compose_i.resize(compose_nnz);
    compose_j.resize(compose_nnz);
    compose_v.resize(compose_nnz);
    MKL_INT e, i, j;
    double x1, x2;
    
    //lecture de la matrice sparse coo
    ifstream fic(path_matrix);
    if(fic) {
      fic>>j>>i;
      
      for(e = 0; e < nnz; e++) {
	fic>> i >> j >> x1 >> x2;
	compose_i[e] = i-1;
	compose_j[e] = j-1;
	compose_v[e] = cplx(x1,x2);
	
      }
    }
    else{
      cout<<"probleme d''ouverture du fichier des indice"<<"\n";
    }
    
    //allocation du vecteur rhs
    B = new cplx[n];
    
    char *path_rhs = NULL;
    path_rhs = (char *) calloc(strlen(argv[13]) + 20, sizeof(char));
    strcpy(path_rhs, argv[13]);
    strcat(path_rhs,"_Vector.txt");
    
    //lecture et stockage du vecteur rhs dans B
    read_Vector_from_file(path_rhs, B);
    
    free(path_rhs); path_rhs = NULL;
    
    free(path_matrix); path_matrix = NULL;
  }
  
  cout<<" Create the Compose matrix a_in "<<endl;
  SparseMatrixCOO<cplx, int> a_in(compose_n, compose_n,
				  compose_nnz, std::move(compose_i),
				  std::move(compose_j), std::move(compose_v));
  
  cout<<" Create the Compose rhs vector b_in "<<endl;
  Vector<cplx> b_in(compose_n, 1, B);
  
  Paddle<cplx> paddle;
  paddle.set_verbose();
  
  auto [p_A, p_rhs] = paddle.centralized_input(a_in, b_in);
  
  //Create the Compose solver
  // Here we choose a GMRES solver
  /*
  GMRES<PartMatrix<SparseMatrixCOO<cplx, int>>, PartVector<Vector<cplx>>> dist_solver(p_A);
  dist_solver.setup(parameters::tolerance<double>{1e-8},
	      parameters::max_iter<MKL_INT>{1000},
	      parameters::restart<MKL_INT>{10},
	      parameters::orthogonalization<Ortho>{Ortho::MGS2},
	      parameters::verbose<bool>{(rank==0)});
  */

  using Solver_K = Pastix<SparseMatrixCOO<cplx>, Vector<cplx>>;
  using Solver_S = GMRES<PartMatrix<DenseMatrix<cplx>>, PartVector<Vector<cplx>>>;
  using Solver = PartSchurSolver<decltype(p_A), decltype(p_rhs), Solver_K, Solver_S>;
  
  Solver dist_solver; 
  dist_solver.setup(parameters::verbose{(rank == 0)},
	       parameters::A{p_A});
  
  Solver_S& iter_solver = dist_solver.get_solver_S();
  
  iter_solver.setup(parameters::max_iter{2000000},
		    parameters::restart{10},
		    parameters::tolerance{1e-8},
		    parameters::always_true_residual{false},
		    parameters::orthogonalization{Ortho::CGS},
		    parameters::verbose{(rank == 0)});
  
  cout<<" Solve the linear system "<<endl;
  auto p_x = dist_solver * p_rhs;

  auto ctr_x = paddle.solution_redistribution(p_x);

  double end_time = MPI_Wtime(); 
 
  if (rank == 0){

    cout<<" temps de resolution par GMRES distribuee "<<end_time - start_time<< " secs "<<endl;

    char *path_solution = NULL;
    path_solution = (char *) calloc(strlen(argv[13]) + 20, sizeof(char));
    strcpy(path_solution, argv[13]);
    strcat(path_solution,"_Solution.txt");
    
    // Ouvrir un fichier en mode écriture
    std::ofstream file(path_solution,std::ios::out);
    
    // Vérifier si le fichier a été ouvert avec succès
    if (!file.is_open()) {
      std::cerr << "Erreur : Impossible d'ouvrir le fichier." << std::endl;
      exit(0);
    }

    cout<<" ecriture de la solution dans un fichier ... "<<endl;

    file << n <<"\n";
    for (const auto& complexNum : ctr_x) {
      file<<std::real(complexNum)<<" "<<std::imag(complexNum)<<"\n";
    }

    free(path_solution); path_solution = NULL;
    
    file.close();
    delete [] B; B = nullptr;
  }

 #endif
}




/*--------------------------------------- computing error norms -------------------------------*/
void compute_error_norms(char *argv[], const char precision[]){

 double divide = 1.0 / 1000.0;
  
  MKL_INT np, N_elements, ndof, e;
  get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);

double t3 = omp_get_wtime(); 
  double epsilon = strtod(argv[8], nullptr);
  double epsilon_reg_tikhonov = strtod(argv[9], nullptr);
  MKL_INT filtre = strtod(argv[10], nullptr);
  
  cout<<" valeur du filtre constant epsilon = "<<epsilon<<" type de filtre = "<<filtre<<endl;
  
  double *points = nullptr;
  MKL_INT *tetra = nullptr, *voisin = nullptr;
  
  points = new double[3 * np];
  
  tetra = new MKL_INT[4 * N_elements];
  
  voisin = new MKL_INT[4 * N_elements];
  
  build_mesh(argv[1], argv[2], argv[3], points, tetra, voisin);
  
  double *Dir = nullptr;
  Dir = new double[3 * ndof];
  
  build_direction_of_space_phases(argv[5], argv[6], ndof, Dir);
  
  MKL_INT n_gL = 12;
  double *X1d = nullptr, *W1d = nullptr;
  X1d = new double[n_gL];
  W1d = new double[n_gL];
  quad_Gauss_1D(n_gL, X1d, W1d);
  
  double *X2d = nullptr, *W2d = nullptr;
  
  MKL_INT n_quad2D = n_gL * n_gL;
  X2d = new double[2 * n_quad2D];
  W2d = new double[n_quad2D];
  
  double *X3d = nullptr, *W3d = nullptr;
  
  MKL_INT n_quad3D = n_gL * n_gL * n_gL;
  X3d = new double[3 * n_quad3D];
  W3d = new double[n_quad3D];
  
  double *third_coord = nullptr;
  third_coord = new double[N_elements];
  
  MKL_INT *new2old = nullptr, *old2new = nullptr;
  new2old = new MKL_INT[N_elements];
  old2new = new MKL_INT[N_elements];
  
  MKL_INT *NewListeVoisin = nullptr, *newloc2glob = nullptr;
  
  NewListeVoisin = new MKL_INT[N_elements * 4];
  
  newloc2glob = new MKL_INT[4 * N_elements];
  
  Sommet *sommets = nullptr;
  
  sommets = new Sommet[np];
  
  Element *elements = nullptr;
  elements = new Element[N_elements];
  
  MKL_INT N_Face_double = N_elements * 4;
  Face *face_double = nullptr;
  face_double = new Face[N_Face_double];
  
  Face *face = nullptr;
  face = new Face[N_Face_double];
  
  MKL_INT *Tri = nullptr;
  Tri = new MKL_INT[N_Face_double];
  
  for(MKL_INT e = 0; e < N_elements; e++) {
    elements[e].OP = new Onde_plane[ndof];
  }
  
  MKL_INT *cumul_NRED = nullptr;
  cumul_NRED = new MKL_INT[N_elements];
  
  double *virtual_points = nullptr;
  virtual_points = new double[12];
  
  Sommet *virtual_sommets = nullptr;
  virtual_sommets = new Sommet[4];
  
  Onde_plane *virtual_plane_wave = nullptr;
  virtual_plane_wave = new Onde_plane[ndof];
  
  MailleSphere MSph(epsilon, epsilon_reg_tikhonov, np, points, N_elements, tetra, voisin, sommets, ndof, Dir, n_gL, X1d, W1d, X2d, W2d, X3d, W3d);
  MSph.build_elements_and_faces(argv[4], argv[7], filtre, third_coord, new2old, old2new, NewListeVoisin, newloc2glob, elements, face_double, Tri, face, cumul_NRED, virtual_points, virtual_sommets, virtual_plane_wave, argv[15]);
  double t4 = omp_get_wtime();
  
  cout<<" temps de creation du maillage : "<<t4 - t3<<" secs"<<endl;

  cout<<"************************************************ Eps_max_filtre = "<<epsilon<<" Eps_max_Tikhonov = "<<epsilon_reg_tikhonov<<endl;
  
  delete [] old2new; old2new =  nullptr;
  delete [] new2old; new2old =  nullptr;
  delete [] NewListeVoisin; NewListeVoisin =  nullptr;
  delete [] newloc2glob; newloc2glob =  nullptr;
  delete [] voisin; voisin =  nullptr;
  delete [] tetra; tetra =  nullptr;
  delete [] Tri; Tri =  nullptr;
  delete [] third_coord; third_coord =  nullptr;
  delete [] Dir; Dir =  nullptr;

  MKL_INT n = N_elements * ndof;
  MKL_INT n_M = n * ndof;

  cplx *P_full = nullptr;
  P_full = new cplx[n_M];
  A_Block *array_P_loc = nullptr;
  array_P_loc = new A_Block[N_elements];
  
  A_skyline P_glob;
  P_glob.set_n_elem(N_elements); // fixe le nombre d'elements 
  P_glob.set_n_inter(1); // fixe le nombre d'interactions 
  P_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  P_glob.set_n_A(); // calcule le nombre de coeff a stocker
  P_glob.build_mat(P_full, array_P_loc); // Construit les blocks et la structure en mémoire continue
  
   if (filtre == 0){
     
     auto t5 = std::chrono::high_resolution_clock::now();
     
     Build_UWVF_MetricMatrix_parallel_version(P_glob, MSph, precision);
     
     auto t6 = std::chrono::high_resolution_clock::now();
     
     std::chrono::duration<double, std::milli> float_BM = t6 - t5;
     
     cout<<" temps de construction de la matrice M_UWVF: "<<float_BM.count() * divide<<" secs"<<endl;
  }
   else{
     auto t5 = std::chrono::high_resolution_clock::now();
     
     BuildVolumeMetricMatrix_parallel_version(P_glob, MSph, precision);
     
     auto t6 = std::chrono::high_resolution_clock::now();
     
     std::chrono::duration<double, std::milli> float_BM = t6 - t5;
     
     cout<<" temps de construction de la matrice M associee au metrique volumique: "<<float_BM.count() * divide<<" secs"<<endl;
   }
   
  auto tN_red_1 = std::chrono::high_resolution_clock::now();
  
  MKL_INT n_red = compute_sizeBlocRed_parallel_version(P_glob, MSph);
  
  auto tN_red_2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_N_red = tN_red_2 - tN_red_1;
  
  cout<<" temps de calcul des dimensions des bases reduites : "<<float_N_red.count() * divide<<" secs"<<endl;
  
  cplx *lambda = nullptr; lambda = new cplx[n_red];
  VecN LAMBDA;
  LAMBDA.n = n_red; LAMBDA.value = lambda;
  
  auto t7xx = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs_ET_Normalisation(P_glob, LAMBDA, MSph);
  auto t8xx = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_Dim = t8xx - t7xx;
  
  cout<<" temps de construction de la dimension de la base fixe: "<<float_Dim.count() * divide<<" secs"<<endl;
  
  auto tn1 = std::chrono::high_resolution_clock::now();
  compute_max_and_min_of_all_blocks_eigenvalues(LAMBDA, MSph);
  
  cout<<"lambda_max_normalisee = "<<MSph.lambda_max_normalisee<<" lambda_min_normalisee = "<<MSph.lambda_min_normalisee<<endl;
  cout<<"Nred_mean = "<<MSph.Nred_mean<<endl;
  auto tn2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_n21 = tn2 - tn1;
  
  cout<<" temps de calcul du max et du min de toutes les valeurs propres: "<<float_n21.count() * divide<<" secs "<<endl;
  
  delete [] lambda; lambda = nullptr;
  
  cplx *U = nullptr; U = new cplx[n];
  cplx *U_red = nullptr; U_red = new cplx[n_red];
  
  for(e = 0; e < n; e++) {
    U[e] = cplx(0.0, 0.0);
  }

  char *file = NULL;
  file = (char *) calloc(strlen(argv[13]) + 20, sizeof(char));
  strcpy(file, argv[13]);
  strcat(file,"_Solution.txt");

  read_Vector_from_file(file, U_red);
  
  free(file); file = NULL;

  VecN vectU, vectU_red;
  vectU_red.n = n_red;
  vectU_red.value = U_red;

  vectU.n = n;
  vectU.value = U;

  double t_U_real_1 = omp_get_wtime();
  produit_P_skyline_Solution_red(P_glob, vectU_red, vectU, MSph);
  double t_U_real_2 = omp_get_wtime();
  cout<<" temps de d'expression de la solution U dans la base originale "<<t_U_real_2 - t_U_real_1<<" secs"<<endl;

  delete [] U_red; U_red = nullptr;
  delete [] P_full; P_full = nullptr;
  delete [] array_P_loc; array_P_loc = nullptr;

  compute_pressure_and_velocity_norms(vectU.value, argv[13], argv[4], MSph);
  
  cout<<"Liberation de la mémoire de U_sky "<<endl;
  delete [] U; U = nullptr;
  
  //omp_set_num_threads(nb_taches);
  delete [] points; points =  nullptr;
  delete [] X1d; X1d =  nullptr;
  delete [] W1d; W1d =  nullptr;
  delete [] X2d; X2d =  nullptr;
  delete [] W2d; W2d =  nullptr;
  delete [] X3d; X3d =  nullptr;
  delete [] W3d; W3d =  nullptr;
  delete [] cumul_NRED; cumul_NRED =  nullptr;
  delete [] Dir; Dir =  nullptr;
  for(MKL_INT e = 0; e < N_elements; e++) {
    delete [] elements[e].OP; elements[e].OP =  nullptr;
    for(MKL_INT j = 0; j < 4; j++){
      elements[e].noeud[j] = nullptr;
      for(MKL_INT l = 0; l < 3; l++) {
	elements[e].face[j].noeud[l] = nullptr;
      }
      
    }
  }
  
  for(MKL_INT i = 0; i < np; i++) {
    sommets[i].set_X(nullptr);
  }
  
  for(MKL_INT i = 0; i < N_Face_double; i++) {
    for(MKL_INT l = 0; l < 3; l++) {
      face_double[i].noeud[l] = nullptr;
	face[i].noeud[l] = nullptr; 
    }
  }
  delete [] sommets; sommets = nullptr;
  delete [] face_double; face_double = nullptr;
  delete [] face; face = nullptr;
  delete [] elements; elements =  nullptr;
  delete [] virtual_points; virtual_points =  nullptr;
  for(MKL_INT l = 0; l < 4; l++) {
    virtual_sommets[l].set_X(nullptr);
  }
  delete [] virtual_sommets; virtual_sommets =  nullptr;
  delete [] virtual_plane_wave; virtual_plane_wave =  nullptr;

}



/*#####################################################################################################################*/
void TEST_Solver_in_Distribute(){
#if defined(USE_COMPOSE)
  int rank;
   std::vector<cplx> rhs_v;

  std::vector<int> rhs_i;
  SparseMatrixCOO<cplx> Z;
  const size_t n_glob = 8;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if(rank == 0){
    //0 -> [10.,  0., -4., -1.,  0.,  0.,  0.,  0.]
    //1 -> [ 0., 11.,  0.,  0., -1.,  0., -3.,  0.]
    Z = SparseMatrixCOO<cplx>({ 0, 0, 0, 1, 1, 1},
                                { 0, 2, 3, 1, 4, 6},
      {cplx(10,0),cplx(-4,0),-cplx(1,0),cplx(11,0),-cplx(1,0),-cplx(3,0)}, n_glob, n_glob);
    rhs_i = std::vector<int>{0, 1};
    rhs_v = std::vector<cplx>{-cplx(6,0), -cplx(4,0)};
    //expected_X = Vector<cplx>{1, 2};
  } else if(rank == 1){
    //2 -> [ 0.,  0., 12.,  0.,  0., -4., -4.,  0.]
    //3 -> [ 0.,  0.,  0., 13.,  0.,  0., -2.,  0.]
    Z = SparseMatrixCOO<cplx>({ 2, 2, 2, 3, 3},
                                { 2, 5, 6, 3, 6},
      {cplx(12,0),-cplx(4,0),-cplx(4,0),cplx(13,0),-cplx(2,0)}, n_glob, n_glob);
    rhs_i = std::vector<int>{2, 3};
    rhs_v = std::vector<cplx>{-cplx(16,0), cplx(38,0)};
    // expected_X = Vector<cplx>{3, 4};
  } else if(rank == 2){
    //4 -> [ 0.,  0.,  0.,  0., 14.,  0.,  0.,  0.]
    //5 -> [ 0., -2.,  0.,  0.,  0., 15.,  0.,  0.]
    Z = SparseMatrixCOO<cplx>({ 4, 5, 5},
                                { 4, 1, 5},
      {cplx(14,0),-cplx(2,0),cplx(15,0)}, n_glob, n_glob);
    rhs_i = std::vector<int>{4, 5};
    rhs_v = std::vector<cplx>{cplx(70,0), cplx(86,0)};
    //expected_X = Vector<cplx>{5, 6};
  } else if(rank == 3){
    //6 -> [ 0.,  0.,  0.,  0.,  0.,  0., 16.,  0.],
    //7 -> [ 0.,  0.,  0.,  0.,  0.,  0., -2., 17.]
    Z = SparseMatrixCOO<cplx>({ 6, 7, 7},
                                { 6, 6 ,7},
      {cplx(16,0),-cplx(2,0),cplx(17,0)}, n_glob, n_glob);
    rhs_i = std::vector<int>{6, 7};
    rhs_v = std::vector<cplx>{112, 122};
    // expected_X = Vector<cplx>{7, 8};
  }

  Paddle<cplx> paddle;
  paddle.set_verbose();

  auto [p_A, p_rhs] = paddle.distributed_input(Z, rhs_i, rhs_v);
  // GCR<PartMatrix<SparseMatrixCOO<cplx, int>>, PartVector<Vector<cplx>>> solver(p_A);
  //solver.setup(parameters::max_iter{50}, parameters::tolerance{Real{1e-9}}, parameters::verbose{(rank == 0)});
  using Solver_K = Pastix<SparseMatrixCOO<cplx>, Vector<cplx>>;
  using Solver_S = GMRES<PartMatrix<DenseMatrix<cplx>>, PartVector<Vector<cplx>>>;
  using Solver = PartSchurSolver<decltype(p_A), decltype(p_rhs), Solver_K, Solver_S>;
  
  Solver dist_solver;
  dist_solver.setup(parameters::verbose{(rank == 0)},
	       parameters::A{p_A});
  
  Solver_S& iter_solver = dist_solver.get_solver_S();
  
  iter_solver.setup(parameters::max_iter{1000},
		    parameters::restart{10},
		    parameters::tolerance{1e-6},
		    parameters::always_true_residual{false},
		    parameters::orthogonalization{Ortho::CGS},
		    parameters::verbose{(rank == 0)});
  cout<<" Solve the linear system "<<endl;
  auto p_x = dist_solver * p_rhs;
  //REQUIRE(solver.get_n_iter() != -1);

  auto dist_x = paddle.solution_redistribution(p_x);
  cout<<" X_num = "<<endl;
   for (const auto& complexNum : dist_x) {
      cout<<complexNum<<endl;
    }
#endif
}




void TEST_COMPOSE_prodMV(){
// #if defined(USE_COMPOSE)
//   SparseMatrixCOO<cplx> A({0, 1, 1 , 2, 2}, {0, 0, 1, 1, 2}, { cplx(4,1),  cplx(-1,3),
// 	cplx(3,5), cplx(2,7), cplx(9,0.0)}, 3, 3);

//   Vector<cplx> u{cplx(1,5), cplx(2,3), cplx(1, 0)};
//   Vector<cplx> b = A * u;

//   A.display("A");
//   u.display("u");
//   b.display("b");
// #endif

  MKL_INT *ia = nullptr, *ja = nullptr;
  cplx *a = nullptr;
  
  cplx *y = nullptr, *Y = nullptr, *x = nullptr, *Z = nullptr;

  int rank ;
  int nb_procs ;
  
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &nb_procs );

  MKL_INT n0 = 2, nnz_loc, e, n, m;

  MKL_INT nnz_glob = 7;
  
  n = 4;

  Y = new cplx[n];
  initialize_tab(n, Y);

  Z = new cplx[n];
  initialize_tab(n, Z);


   x = new cplx[n0];
  initialize_tab(n0, x);
 

  

  if (rank == 0){
    cout<<" affectation des tableaux locaux "<<endl;
  }
  
  if (rank == 0){
    nnz_loc = 3;
    m = n0;
    y = new cplx[n0];
    initialize_tab(n0, y);
  }
  else{
    nnz_loc = 4;
    m = n;
    y = new cplx[n];
    initialize_tab(n, y);
  }

  ia = new MKL_INT[nnz_loc];  ja = new MKL_INT[nnz_loc]; a = new cplx[nnz_loc];
  
  if (rank == 0){
    ia[0] = 0; ia[1] = 1; ia[2] = 1;
    ja[0] = 0; ja[1] = 0; ja[2] = 1;
    a[0] = cplx(1,0); a[1] = cplx(2,0); a[2] = cplx(5,0);
    cout<< "rank = "<<rank<<endl;
    for(e = 0; e< nnz_loc; e++){
      cout<<" ia["<<e<<"] = "<<ia[e]<<"ja["<<e<<"] ="<<ja[e]<<" a["<<e<<"] = "<<a[e]<<endl;
    }
  }
  else{
    ia[0] = 2; ia[1] = 2; ia[2] = 3; ia[3] = 3;
    ja[0] = 2; ja[1] = 3; ja[2] = 2; ja[3] = 3;
    a[0] = cplx(3,0); a[1] = cplx(4,0); a[2] = cplx(2,0); a[3]= cplx(7,0);
    cout<< "rank = "<<rank<<endl;
    for(e = 0;e < nnz_loc; e++){
      cout<<" ia["<<e<<"] = "<<ia[e]<<"ja["<<e<<"] ="<<ja[e]<<" a["<<e<<"] = "<<a[e]<<endl;
    }
  }

 MKL_INT I[] = {0, 1, 1, 2, 2, 3, 3};
  MKL_INT J[] = {0, 0, 1, 2, 3, 2, 3};
  cplx V[] = {cplx(1,0), cplx(2,0), cplx(5,0), cplx(3,0), cplx(4,0), cplx(2,0), cplx(7,0)};

  cplx X[] = {cplx(1,0), cplx(2,0), cplx(3,0), cplx(4,0)};

  cout<< "rank = "<<rank<<endl;
  for(e = 0; e< nnz_glob; e++){
    cout<<" I["<<e<<"] = "<<I[e]<<"J["<<e<<"] ="<<J[e]<<" V["<<e<<"] = "<<V[e]<<endl;
  }
  /*
  if(rank == 0){
    //0 -> [10.,  0., -4., -1.,  0.,  0.,  0.,  0.]
    //1 -> [ 0., 11.,  0.,  0., -1.,  0., -3.,  0.]
    MKL_INT i0[] = {0, 0, 0, 1, 1, 1};
    MKL_INT j0[] = { 0, 2, 3, 1, 4, 6};
    cplx a0[] ={cplx(10,0),cplx(-4,0),-cplx(1,0),cplx(11,0),-cplx(1,0),-cplx(3,0)};
    cplx b0[] = {-cplx(6,0), -cplx(4,0)};
    nnz_loc = 6;

     ia = new MKL_INT[nnz_loc];  ja = new MKL_INT[nnz_loc]; a = new cplx[nnz_loc];

    for(e = 0; e < nnz_loc; e++){
    ia[e] = i0[e];
    ja[e] = j0[e];
    a[e] = a0[e];
  }

  for(e = 0; e < n0; e++){
    x[e] = b0[e];
  }


  } else if(rank == 1){
    //2 -> [ 0.,  0., 12.,  0.,  0., -4., -4.,  0.]
    //3 -> [ 0.,  0.,  0., 13.,  0.,  0., -2.,  0.]
    MKL_INT i0[] = { 2, 2, 2, 3, 3};
    MKL_INT j0[] = { 2, 5, 6, 3, 6};
    cplx a0[] = {cplx(12,0),-cplx(4,0),-cplx(4,0),cplx(13,0),-cplx(2,0)};
    cplx b0[] = {-cplx(16,0), cplx(38,0)};
    nnz_loc = 5;

     ia = new MKL_INT[nnz_loc];  ja = new MKL_INT[nnz_loc]; a = new cplx[nnz_loc];

    for(e = 0; e < nnz_loc; e++){
    ia[e] = i0[e];
    ja[e] = j0[e];
    a[e] = a0[e];
  }

  for(e = 0; e < n0; e++){
    x[e] = b0[e];
  }


  } else if(rank == 2){
    //4 -> [ 0.,  0.,  0.,  0., 14.,  0.,  0.,  0.]
    //5 -> [ 0., -2.,  0.,  0.,  0., 15.,  0.,  0.]
    MKL_INT i0[] = { 4, 5, 5};
    MKL_INT j0[] = { 4, 1, 5};
    cplx a0[] = {cplx(14,0),-cplx(2,0),cplx(15,0)};
    cplx b0[] = {cplx(70,0), cplx(86,0)};
    nnz_loc = 3;

    ia = new MKL_INT[nnz_loc];  ja = new MKL_INT[nnz_loc]; a = new cplx[nnz_loc];

    for(e = 0; e < nnz_loc; e++){
    ia[e] = i0[e];
    ja[e] = j0[e];
    a[e] = a0[e];
  }

  for(e = 0; e < n0; e++){
    x[e] = b0[e];
  }


  } else if(rank == 3){
    //6 -> [ 0.,  0.,  0.,  0.,  0.,  0., 16.,  0.],
    //7 -> [ 0.,  0.,  0.,  0.,  0.,  0., -2., 17.]
    MKL_INT i0[] = { 6, 7, 7};
    MKL_INT j0[] = { 6, 6 ,7};
    cplx a0[] = {cplx(16,0),-cplx(2,0),cplx(17,0)};
    cplx b0[] = {cplx(112,0), cplx(122,0)};
    nnz_loc = 3;

    ia = new MKL_INT[nnz_loc];  ja = new MKL_INT[nnz_loc]; a = new cplx[nnz_loc];

    for(e = 0; e < nnz_loc; e++){
    ia[e] = i0[e];
    ja[e] = j0[e];
    a[e] = a0[e];
  }

  for(e = 0; e < n0; e++){
    x[e] = b0[e];
  }

  }


  MKL_INT I[] = { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3,  4, 5, 5,  6, 7, 7};
  MKL_INT J[] =  { 0, 2, 3, 1, 4, 6, 2, 5, 6, 3, 6,  4, 1, 5,  6, 6 ,7};
  cplx V[] = {cplx(10,0),cplx(-4,0),-cplx(1,0),cplx(11,0),-cplx(1,0),-cplx(3,0), cplx(12,0),-cplx(4,0),-cplx(4,0),cplx(13,0),-cplx(2,0), cplx(14,0),-cplx(2,0),cplx(15,0), cplx(16,0),-cplx(2,0),cplx(17,0)};

  cplx X[] = {-cplx(6,0), -cplx(4,0), -cplx(16,0), cplx(38,0), cplx(70,0), cplx(86,0), cplx(112,0), cplx(122,0)};
*/

  if (rank == 0){
     cout<<"initialisation des structures sparse locales "<<endl;
  }

  sparse_matrix_t coo_matrix;

  struct matrix_descr descr;

  descr.type = SPARSE_MATRIX_TYPE_GENERAL;

  preprocessing_of_Sparse_COO_MV_product(&descr, &coo_matrix, nnz_loc, m, n, ia, ja, a);

if (rank == 0){
  cout<<"produit mat sparse vecteur en local "<<endl;
  }  

  prodM_local_Vector_Sparse_COO_distribue(nnz_loc, cplx(1.0,0.0), descr, coo_matrix, X, cplx(0.0,0.0), y);

  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0){
     cout<<"produit mat sparse vecteur en global "<<endl;
  }
  if (rank == 1){
  cout<<" rank = "<<rank<<endl;
  affiche_mat(y, m, 1, "y");
  }
  
  //produit général
  sparse_matrix_t COO;
  struct matrix_descr Descr;
  Descr.type = SPARSE_MATRIX_TYPE_GENERAL;
  preprocessing_of_Sparse_COO_MV_product(&Descr, &COO, nnz_glob, n, n, I, J, V);
  
  prodM_local_Vector_Sparse_COO_distribue(nnz_glob, cplx(1.0,0.0), Descr, COO, X, cplx(0.0,0.0), Y);
  if (rank == 0){
    cout<<"collecte et comparaison des résultats "<<endl;
    affiche_mat(Y, n, 1, "Y");
  }

  
  cplx *z;
   
  if (rank == 0){
    z = y;
  }else{
    z = y + n0;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  //collecte des résultats
  collecte_Vector_with_MPI_routine(rank, nb_procs, n0, z, Z);
  
  if (rank == 0){
    double maxx = 0.0, minn = 1000.0;
    for(e = 0; e < n; e++){
      maxx = max(maxx, abs(Z[e]-Y[e]));
      minn = min(minn, abs(Z[e]-Y[e]));
    }
    cout<<" maxx = "<<maxx<<" minn = "<<minn<<endl;
  } 
  
  destroy_Sparse_COO_Matrix(&coo_matrix);
  destroy_Sparse_COO_Matrix(&COO);
}


/*--------------------------------------- computing error norms -------------------------------*/
void compute_error_norms_associated_to_virtual_basis(char *argv[], const char precision[]){

 double divide = 1.0 / 1000.0;
  
  MKL_INT np, N_elements, ndof, e;
  get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);

double t3 = omp_get_wtime(); 
  double epsilon = strtod(argv[8], nullptr);
  double epsilon_reg_tikhonov = strtod(argv[9], nullptr);
  MKL_INT filtre = strtod(argv[10], nullptr);
  
  cout<<" valeur du filtre constant epsilon = "<<epsilon<<" type de filtre = "<<filtre<<endl;
  
  double *points = nullptr;
  MKL_INT *tetra = nullptr, *voisin = nullptr;
  
  points = new double[3 * np];
  
  tetra = new MKL_INT[4 * N_elements];
  
  voisin = new MKL_INT[4 * N_elements];
  
  build_mesh(argv[1], argv[2], argv[3], points, tetra, voisin);
  
  double *Dir = nullptr;
  Dir = new double[3 * ndof];
  
  build_direction_of_space_phases(argv[5], argv[6], ndof, Dir);
  
  MKL_INT n_gL = 12;
  double *X1d = nullptr, *W1d = nullptr;
  X1d = new double[n_gL];
  W1d = new double[n_gL];
  quad_Gauss_1D(n_gL, X1d, W1d);
  
  double *X2d = nullptr, *W2d = nullptr;
  
  MKL_INT n_quad2D = n_gL * n_gL;
  X2d = new double[2 * n_quad2D];
  W2d = new double[n_quad2D];
  
  double *X3d = nullptr, *W3d = nullptr;
  
  MKL_INT n_quad3D = n_gL * n_gL * n_gL;
  X3d = new double[3 * n_quad3D];
  W3d = new double[n_quad3D];
  
  double *third_coord = nullptr;
  third_coord = new double[N_elements];
  
  MKL_INT *new2old = nullptr, *old2new = nullptr;
  new2old = new MKL_INT[N_elements];
  old2new = new MKL_INT[N_elements];
  
  MKL_INT *NewListeVoisin = nullptr, *newloc2glob = nullptr;
  
  NewListeVoisin = new MKL_INT[N_elements * 4];
  
  newloc2glob = new MKL_INT[4 * N_elements];
  
  Sommet *sommets = nullptr;
  
  sommets = new Sommet[np];
  
  Element *elements = nullptr;
  elements = new Element[N_elements];
  
  MKL_INT N_Face_double = N_elements * 4;
  Face *face_double = nullptr;
  face_double = new Face[N_Face_double];
  
  Face *face = nullptr;
  face = new Face[N_Face_double];
  
  MKL_INT *Tri = nullptr;
  Tri = new MKL_INT[N_Face_double];
  
  for(MKL_INT e = 0; e < N_elements; e++) {
    elements[e].OP = new Onde_plane[ndof];
  }
  
  MKL_INT *cumul_NRED = nullptr;
  cumul_NRED = new MKL_INT[N_elements];
  
  double *virtual_points = nullptr;
  virtual_points = new double[12];
  
  Sommet *virtual_sommets = nullptr;
  virtual_sommets = new Sommet[4];
  
  Onde_plane *virtual_plane_wave = nullptr;
  virtual_plane_wave = new Onde_plane[ndof];
  
  MailleSphere MSph(epsilon, epsilon_reg_tikhonov, np, points, N_elements, tetra, voisin, sommets, ndof, Dir, n_gL, X1d, W1d, X2d, W2d, X3d, W3d);
  MSph.build_elements_and_faces(argv[4], argv[7], filtre, third_coord, new2old, old2new, NewListeVoisin, newloc2glob, elements, face_double, Tri, face, cumul_NRED, virtual_points, virtual_sommets, virtual_plane_wave, argv[15]);
  double t4 = omp_get_wtime();
  
  cout<<" temps de creation du maillage : "<<t4 - t3<<" secs"<<endl;

  cout<<"************************************************ Eps_max_filtre = "<<epsilon<<" Eps_max_Tikhonov = "<<epsilon_reg_tikhonov<<endl;
  
  delete [] old2new; old2new =  nullptr;
  delete [] new2old; new2old =  nullptr;
  delete [] NewListeVoisin; NewListeVoisin =  nullptr;
  delete [] newloc2glob; newloc2glob =  nullptr;
  delete [] voisin; voisin =  nullptr;
  delete [] tetra; tetra =  nullptr;
  delete [] Tri; Tri =  nullptr;
  delete [] third_coord; third_coord =  nullptr;
  delete [] Dir; Dir =  nullptr;

  MKL_INT n = N_elements * ndof;
   cplx *M = new cplx[ndof * ndof];
  cplx *P = new cplx[ndof * ndof];
  double *lambda = new double[ndof];
  
  cplx *AA = new cplx[ndof * ndof];
  double *w = new double[ndof];

  if (filtre == 0){
    
    auto t5 = std::chrono::high_resolution_clock::now();
    
    buildArealMetrique(MSph.ndof, M, MSph.fixe_element, precision);
    
    auto t6 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_BM = t6 - t5;
    
    cout<<" temps de construction de la matrice surfacique de l'element virtuel: "<<float_BM.count() * divide<<" secs"<<endl;
  }
  else{
    auto t5 = std::chrono::high_resolution_clock::now();
    
    buildVolumeMetrique(MSph.ndof, M, MSph, precision);
    
    auto t6 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_BM = t6 - t5;
    
    cout<<" temps de construction de la matrice M associee au metrique volumique de l'element virtuel: "<<float_BM.count() * divide<<" secs"<<endl;
  }

  
  auto t7 = std::chrono::high_resolution_clock::now();
  Definition_des_nouvelles_dimensions_des_bases_Trefftz_par_element_virtuel(M, MSph);
  auto t8 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_Dim = t8 - t7;
  
  cout<<" temps de construction de la dimension de la base fixe: "<<float_Dim.count() * divide<<" secs"<<endl;
  
  cout<<" Nred = "<<MSph.fixe_element.Nred<<" Nres = "<<MSph.fixe_element.Nres<<endl;
  
  auto t_diag_metrique_1 = std::chrono::high_resolution_clock::now();
  DecompositionDeBloc_ET_Normalisation_TSVD_version(MSph.ndof, M, P, lambda, MSph.fixe_element, AA, w) ;
  auto t_diag_metrique_2 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_diag_metrique = t_diag_metrique_2 - t_diag_metrique_1;
  
  cout<<" temps de construction des bases reduites : "<<float_diag_metrique.count() * divide<<" secs"<<endl;
  
  cout<<" max et min des valeurs propres non reduites et non normalisées "<<endl;
  cout<<" max = "<<w[MSph.ndof-1] <<" min = "<<w[0]<<endl;
  
  double Cond = lambda[MSph.fixe_element.Nred-1] / (double) lambda[0];
  
  cout<<" max et min des valeurs propres reduites et normalisées"<<endl;
  cout<<" max = "<<lambda[MSph.fixe_element.Nred-1] / (double) MSph.fixe_element.facto_volume<<" min = "<<lambda[0] / (double) MSph.fixe_element.facto_volume<<" Cond = "<<Cond<<endl;
  
  n = N_elements * ndof;


  MKL_INT n_red;

  char *file_Sol = NULL;
  file_Sol = (char *) calloc(strlen(argv[13]) + 20, sizeof(char));
  strcpy(file_Sol, argv[13]);
  strcat(file_Sol,"_Solution.txt");
  
  read_size(file_Sol, &n_red);
  
  cplx *U_red = nullptr; U_red = new cplx[n_red];
  
  for(e = 0; e < n_red; e++) {
    U_red[e] = cplx(0.0, 0.0);
  }

  
  read_Vector_from_file(file_Sol, U_red);
  
  free(file_Sol); file_Sol = NULL;
  

  cplx *U = nullptr;
  U = new cplx[n];
  
  for(e = 0; e < n; e++){
    U[e] = cplx(0.0, 0.0);
  }

  
  VecN vectU, vectU_red;
  vectU_red.n = n_red;
  vectU_red.value = U_red;
  
  vectU.n = n;
  vectU.value = U;
  
  double t_U_real_1 = omp_get_wtime();
  produit_P_virtual_Vector_red(P, vectU_red, vectU, MSph);
  double t_U_real_2 = omp_get_wtime();
  cout<<" temps de d'expression de la solution U dans la base originale "<<t_U_real_2 - t_U_real_1<<" secs"<<endl;
  
  delete [] U_red; U_red = nullptr;
  delete [] P; P = nullptr;
  delete [] w; w = nullptr;
  delete [] M; M = nullptr;
  
  if (filtre == 0) {
    auto t29 = std::chrono::high_resolution_clock::now();
    double L2P_ex = norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
    auto t30 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_ex = t30 - t29;
    
    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<float_L2P_ex.count() * divide<<" secs"<<endl;
    
    auto t31 = std::chrono::high_resolution_clock::now();
    double L2V_ex = norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
    auto t32 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_ex = t32 - t31;
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<float_L2V_ex.count() * divide<<" secs"<<endl;
    
    auto t33 = std::chrono::high_resolution_clock::now();
    double L2P_err =  norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_parallel_version(vectU.value, MSph);
    auto t34 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_err = t34 - t33;
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<float_L2P_err.count() * divide<<" secs"<<endl;
    
    auto t35 = std::chrono::high_resolution_clock::now();
    double L2V_err = norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_parallel_version(vectU.value, MSph);
    auto t36 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_err = t36 - t35;
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<float_L2V_err.count() * divide<<" secs"<<endl;
    
    auto zz7 = std::chrono::high_resolution_clock::now();
    double HS_ex = norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(MSph);
    auto zz8 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_HS_ex = zz8 - zz7;
    cout<<" temps de calcul de ||U_ex||_{H^1} surfacique: "<<float_HS_ex.count() * divide<<" secs"<<endl;
    
   auto zoo7 = std::chrono::high_resolution_clock::now();
   double HS_err = norme_H1_erreur_surfacique_avec_integ_numerique_parallel_version(vectU.value, MSph);
   auto zoo8 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_HS_err = zoo8 - zoo7;
   cout<<" temps de calcul de ||U_ex - U_h||_{H^1} surfacique: "<<float_HS_err.count() * divide<<" secs"<<endl;
   
   auto zo9 = std::chrono::high_resolution_clock::now();
 
   double H1M_err = norme_H1_du_saut_de_l_erreur_avec_integ_numerique(vectU.value, MSph);
   auto zo10 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_Hmodif_err = zo10 - zo9;
   cout<<" temps de calcul de ||U_ex - U_h||_{H^1} modifiée: "<<float_Hmodif_err.count() * divide<<" secs"<<endl;
   
   double P_ex_inf =  norme_inf_Bessel(MSph);
   
   double P_err_inf = norme_inf_erreur(vectU.value, MSph);
   
   char *file_absolue = NULL;
   file_absolue = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
   strcpy(file_absolue, argv[13]);
   strcat(file_absolue,"_norme_absolue_Surf.txt");
   
   char *file_relative = NULL;
   file_relative = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
   strcpy(file_relative, argv[13]);
   strcat(file_relative,"_norme_relative_Surf.txt");
   
   char *file_Cond = NULL;
   file_Cond = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
   strcpy(file_Cond, argv[13]);
   strcat(file_Cond,"_Cond_Surf.txt");
   
   
   FILE* fic_absolue;
   
   FILE* fic_relative;
   
   FILE* fic_Cond;
   
   if( !(fic_absolue = fopen(file_absolue,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
   if( !(fic_relative = fopen(file_relative,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
   if( !(fic_Cond = fopen(file_Cond,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
   cout<<" valeur du filtre constant sans la variation surfacique = "<<MSph.eps_0<<endl;
   cout<<" ||P_ex - P_h||_{L2} = "<<L2P_err<<" ||V_ex - V_h||_{L2} = "<<L2V_err<<" ||P_ex - P_h||_{inf} = "<<P_err_inf<<endl;
   
   
   cout<<" ||P_ex - P_h||_{L2} / ||P_ex||_{L2} = "<<L2P_err / (double) L2P_ex <<" ||V_ex - V_h||_{L2} / ||V_ex||_{L2} = "<<L2V_err / (double) L2V_ex<<" ||P_ex - P_h||_{inf} / ||P_ex||_{inf} = "<<P_err_inf / (double) P_ex_inf<<endl;
   
   double L2P_err_rela = L2P_err / (double) L2P_ex;
   double L2V_err_rela = L2V_err / (double) L2V_ex;
   double HS_err_rela = HS_err / (double) HS_ex;
   double H1M_err_rela = H1M_err / (double) HS_ex;
   
   cout<<" norme H1 surfacique ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<HS_err_rela<<endl;
   
   cout<<" norme H1 modifiée ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<H1M_err_rela<<endl;
   
   fprintf(fic_absolue,"%d\t %d\t %d\t %g\t %g\t %g\t %g\n",N_elements, ndof, MSph.fixe_element.Nred, L2P_err, L2V_err, HS_err, H1M_err);
   
   fprintf(fic_relative,"%d\t %d\t %d\t %g\t %g\t %g\t %g\n",N_elements, ndof, MSph.fixe_element.Nred, L2P_err_rela, L2V_err_rela, HS_err_rela, H1M_err_rela);
   
   fprintf(fic_Cond,"%d %d %d %g %g %g\n",N_elements, ndof, MSph.fixe_element.Nred, lambda[0], lambda[MSph.fixe_element.Nred-1], Cond);
   
   fclose(fic_Cond); free(file_Cond); file_Cond = NULL;
   
   fclose(fic_absolue); fclose(fic_relative);
   free(file_relative); file_relative = NULL;
   free(file_absolue); file_absolue = NULL;
   
  }
  
  else{
    
    auto t29 = std::chrono::high_resolution_clock::now();
    double L2P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
    auto t30 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_ex = t30 - t29;
    
    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<float_L2P_ex.count() * divide<<" secs"<<endl;
    
    auto t31 = std::chrono::high_resolution_clock::now();
    double L2V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
    auto t32 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_ex = t32 - t31;
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<float_L2V_ex.count() * divide<<" secs"<<endl;
    
    auto t33 = std::chrono::high_resolution_clock::now();
    double L2P_err =  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(vectU.value, MSph);
    auto t34 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_err = t34 - t33;
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<float_L2P_err.count() * divide<<" secs"<<endl;
    
    auto t35 = std::chrono::high_resolution_clock::now();
    double L2V_err = norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(vectU.value, MSph);
    auto t36 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_err = t36 - t35;
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<float_L2V_err.count() * divide<<" secs"<<endl;
    
    auto z7 = std::chrono::high_resolution_clock::now();
    double HV_ex = norme_H1_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MSph);
    auto z8 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_HV_ex = z8 - z7;
    cout<<" temps de calcul de ||U_ex||_{H^1} volumique: "<<float_HV_ex.count() * divide<<" secs"<<endl;
    
    auto zo7 = std::chrono::high_resolution_clock::now();
    double HV_err = norme_H1_erreur_volumique_avec_integ_numerique_parallel_version(vectU.value, MSph);
    auto zo8 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_HV_err = zo8 - zo7;
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} volumique: "<<float_HV_err.count() * divide<<" secs"<<endl;

    auto zo9 = std::chrono::high_resolution_clock::now();
    double HS_ex = norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(MSph);
    
    double H1M_err = norme_H1_du_saut_de_l_erreur_avec_integ_numerique(vectU.value, MSph);
    auto zo10 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_Hmodif_err = zo10 - zo9;
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} modifiée: "<<float_Hmodif_err.count() * divide<<" secs"<<endl;

    char *file_absolue = NULL;
    file_absolue = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
    strcpy(file_absolue, argv[13]);
    strcat(file_absolue,"_norme_absolue_Vol.txt");
    
    char *file_relative = NULL;
    file_relative = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
    strcpy(file_relative, argv[13]);
    strcat(file_relative,"_norme_relative_Vol.txt");

    char *file_Cond = NULL;
    file_Cond = (char *) calloc(strlen(argv[13]) + 50, sizeof(char));
    strcpy(file_Cond, argv[13]);
    strcat(file_Cond,"_Cond_Vol.txt");
    
    FILE* fic_absolue;
    
    FILE* fic_relative;

    FILE* fic_Cond;
  
    if( !(fic_absolue = fopen(file_absolue,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
    
    if( !(fic_relative = fopen(file_relative,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }

    if( !(fic_Cond = fopen(file_Cond,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
    
    double P_ex_inf =  norme_inf_Bessel(MSph);
    
    double P_err_inf = norme_inf_erreur(vectU.value, MSph);
    
    cout<<" valeur du filtre constant sans la variation volumique= "<<MSph.eps_0<<endl;
    cout<<" ||P_ex - P_h||_{L2} = "<<L2P_err<<" ||V_ex - V_h||_{L2} = "<<L2V_err<<" ||P_ex - P_h||_{inf} = "<<P_err_inf<<endl;
    
    
    cout<<" ||P_ex - P_h||_{L2} / ||P_ex||_{L2} = "<<L2P_err / (double) L2P_ex <<" ||V_ex - V_h||_{L2} / ||V_ex||_{L2} = "<<L2V_err / (double) L2V_ex<<" ||P_ex - P_h||_{inf} / ||P_ex||_{inf} = "<<P_err_inf / (double) P_ex_inf<<endl;

    double L2P_err_rela = L2P_err / (double) L2P_ex;
    double L2V_err_rela = L2V_err / (double) L2V_ex;
    double HV_err_rela = HV_err / (double) HV_ex;
    double H1M_err_rela = H1M_err / (double) HS_ex;
    
    cout<<" norme H1 volumique ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<HV_err_rela<<endl;

    cout<<" norme H1 modifiée ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<H1M_err_rela<<endl;
    
    fprintf(fic_absolue,"%d %d %d %g %g %g %g\n",N_elements, ndof, MSph.fixe_element.Nred, L2P_err, L2V_err, HV_err, H1M_err);
    
    fprintf(fic_relative,"%d %d %d %g %g %g %g\n",N_elements, ndof, MSph.fixe_element.Nred, L2P_err_rela, L2V_err_rela, HV_err_rela, H1M_err_rela);
    
    fprintf(fic_Cond,"%d %d %d %g %g %g\n",N_elements, ndof, MSph.fixe_element.Nred, lambda[0], lambda[MSph.fixe_element.Nred-1], Cond);
    
    fclose(fic_Cond); free(file_Cond); file_Cond = NULL;
    
    fclose(fic_absolue); fclose(fic_relative);
    free(file_relative); file_relative = NULL;
    free(file_absolue); file_absolue = NULL;
    
  }
  delete [] U; U = nullptr;
  
  //omp_set_num_threads(nb_taches);
  delete [] points; points =  nullptr;
  delete [] X1d; X1d =  nullptr;
  delete [] W1d; W1d =  nullptr;
  delete [] X2d; X2d =  nullptr;
  delete [] W2d; W2d =  nullptr;
  delete [] X3d; X3d =  nullptr;
  delete [] W3d; W3d =  nullptr;
  delete [] cumul_NRED; cumul_NRED =  nullptr;
  delete [] Dir; Dir =  nullptr;
  for(MKL_INT e = 0; e < N_elements; e++) {
    delete [] elements[e].OP; elements[e].OP =  nullptr;
    for(MKL_INT j = 0; j < 4; j++){
      elements[e].noeud[j] = nullptr;
      for(MKL_INT l = 0; l < 3; l++) {
	elements[e].face[j].noeud[l] = nullptr;
      }
      
    }
  }
  
  for(MKL_INT i = 0; i < np; i++) {
    sommets[i].set_X(nullptr);
  }
  
  for(MKL_INT i = 0; i < N_Face_double; i++) {
    for(MKL_INT l = 0; l < 3; l++) {
      face_double[i].noeud[l] = nullptr;
	face[i].noeud[l] = nullptr; 
    }
  }
  delete [] sommets; sommets = nullptr;
  delete [] face_double; face_double = nullptr;
  delete [] face; face = nullptr;
  delete [] elements; elements =  nullptr;
  delete [] virtual_points; virtual_points =  nullptr;
  for(MKL_INT l = 0; l < 4; l++) {
    virtual_sommets[l].set_X(nullptr);
  }
  delete [] virtual_sommets; virtual_sommets =  nullptr;
  delete [] virtual_plane_wave; virtual_plane_wave =  nullptr;

}
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
int solve_with_centralized_LU(char *argw[]){
  MKL_INT n;//taille global du systeme lineaire
  MKL_INT mtype = 13; /* set matrix type to "complex unsymmetric matrix" */
  
  /* RHS and solution vectors. */
  cplx *b = nullptr;
  cplx *x = nullptr;
  cplx *bs = nullptr;
  double        res, res0;
  MKL_INT nrhs = 1; /* Number of right hand sides. */
  /* Internal solver memory pointer pt, */
  /* 32-bit: int pt[64]; 64-bit: long int pt[64] */
  /* or void *pt[64] should be OK on both architectures */
  void *pt[64] = { 0 };
  /* Cluster Sparse Solver control parameters. */
  MKL_INT iparm[64] = { 0 };
  MKL_INT maxfct, mnum, phase, msglvl, error;
  
  /* Auxiliary variables. */
  double  ddum; /* Double dummy   */
  MKL_INT idum; /* Integer dummy. */
  MKL_INT i, j, e;
  int     mpi_stat = 0;
  int     argc = 0;
  int     comm = 0, rank = 0;
  char**  argv;

  MKL_INT *ia = nullptr, *ja = nullptr;
  cplx *a = nullptr;
 

  char *file_size_Vector = NULL;
  file_size_Vector = (char *) calloc(strlen(argw[13]) + 20, sizeof(char));
  strcpy(file_size_Vector, argw[13]);
  strcat(file_size_Vector,"_Vector.txt");


  //lecture de la taille du systeme lineaire
  read_size(file_size_Vector, &n);
  free(file_size_Vector); file_size_Vector = NULL;

  char *file_LU_info = NULL;
  file_LU_info = (char *) calloc(strlen(argw[13]) + 100, sizeof(char));
  strcpy(file_LU_info, argw[13]);
  char chaine[10];
  sprintf(chaine, "_%d", rank);
  strcat(file_LU_info,"_LU_information");
  strcat(file_LU_info,chaine);
  strcat(file_LU_info,"_.txt");

  FILE* fic;
  
  if( !(fic = fopen(file_LU_info,"a")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);
  }

/* -------------------------------------------------------------------- */
/* .. Init MPI.                                                         */
/* -------------------------------------------------------------------- */
    mpi_stat = MPI_Init( &argc, &argv );
    if (mpi_stat != MPI_SUCCESS) {
        error = mpi_stat;
        if ( rank == 0 ) printf ("\nERROR during MPI initialization: %lli", (long long int)error);
        goto final;
    }
    mpi_stat = MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    if (mpi_stat != MPI_SUCCESS) {
        error = mpi_stat;
        if ( rank == 0 ) printf ("\nERROR getting MPI ranks: %lli", (long long int)error);
        goto final;
    }
    comm =  MPI_Comm_c2f( MPI_COMM_WORLD );

    if (rank == 0){
      double t1 = MPI_Wtime();

      cout<<" lecture de la matrice sparse csr ... "<<endl;
      MKL_INT nnz, size_cols;
      char *file_vals_cols = NULL;
      file_vals_cols = (char *) calloc(strlen(argw[13]) + 100, sizeof(char));
      strcpy(file_vals_cols, argw[13]);
      strcat(file_vals_cols,"_vals_&_cols.txt");
      
      char *file_rows = NULL;
      file_rows = (char *) calloc(strlen(argw[13]) + 100, sizeof(char));
      strcpy(file_rows, argw[13]);
      strcat(file_rows,"_rows.txt");

      cout<<" file_vals = "<<file_vals_cols<<" file_rows = "<<file_rows<<endl;
      
      cout<<"lecture du nnz et de la taille du tableau des indices de lignes "<<endl;
      read_size_of_Sparse_CSR_matrix_from_file(file_vals_cols, file_rows, &nnz, &size_cols);
      
      ia = new MKL_INT[size_cols]; ja = new MKL_INT[nnz]; a = new cplx[nnz];
      
      //lecture de la matrice
      read_Sparse_CSR_matrix_from_file(file_vals_cols, file_rows, nnz, size_cols, ia, ja, a);
      double t2 = MPI_Wtime();
      
      free(file_rows); file_rows = NULL;
      free(file_vals_cols); file_vals_cols = NULL;
      cout<<" temps de lecture de la matrice sparse csr à partir d'un fichier : "<< t2 - t1<<" secs"<<endl;
    }
/* -------------------------------------------------------------------- */
/* .. Setup Cluster Sparse Solver control parameters.                                 */
/* -------------------------------------------------------------------- */
    iparm[ 0] =  1; /* Solver default parameters overriden with provided by iparm */
    iparm[ 1] =  2; /* Use METIS for fill-in reordering */
    iparm[ 5] =  0; /* Write solution into x */
    iparm[ 7] =  1; /* Max number of iterative refinement steps */
    iparm[ 9] = 16; /* Perturb the pivot elements with 1E-13 */
    iparm[10] =  1; /* Use nonsymmetric permutation and scaling MPS */
    iparm[12] =  0;//iparm[12] =  1; /* Switch on Maximum Weighted Matching algorithm (default for non-symmetric) */
    iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
    iparm[18] = -1; /* Output: Mflops for LU factorization */
    iparm[26] =  1; /* Check input data for correctness */
    iparm[39] =  0; /* Input: matrix/rhs/solution stored on master */
    maxfct = 1; /* Maximum number of numerical factorizations. */
    mnum   = 1; /* Which factorization to use. */
    msglvl = 1; /* Print statistical information in file */
    error  = 0; /* Initialize error flag */

/* -------------------------------------------------------------------- */
/* .. Reordering and Symbolic Factorization. This step also allocates   */
/* all memory that is necessary for the factorization.                  */
/* -------------------------------------------------------------------- */
    phase = 11;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
                &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during symbolic factorization: %lli", (long long int)error);
        goto final;
    }
    if ( rank == 0 ) printf ("\nReordering completed ... ");
    fprintf(fic,"%d %d", n, iparm[17]);
/* -------------------------------------------------------------------- */
/* .. Numerical factorization.                                          */
/* -------------------------------------------------------------------- */
    phase = 22;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
                &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during numerical factorization: %lli", (long long int)error);
        goto final;
    }
    if ( rank == 0 ) printf ("\nFactorization completed ... ");

/* -------------------------------------------------------------------- */
/* .. Back substitution and iterative refinement.                       */
/* -------------------------------------------------------------------- */
    phase = 33;
    /* Set right hand side to one. */
    if ( rank == 0 )
    {
      double t3 = MPI_Wtime();
      
      b = new cplx[n];
      x = new cplx[n];
      bs = new cplx[n];
      
      char *file = NULL;
      file = (char *) calloc(strlen(argw[13]) + 20, sizeof(char));
      strcpy(file, argw[13]);
      strcat(file,"_Vector.txt");
      
      for(e = 0; e < n; e++) {
	b[e] = cplx(0.0, 0.0);
	x[e] = cplx(0.0, 0.0);
      }
      
      
      read_Vector_from_file(file, b);
      
      free(file); file = NULL;
      
      double t4 = MPI_Wtime();
      
      cout<<" temps de lecture du second membre à partir d'un fichier : "<<t4 - t3<<" secs"<<endl;
    }
    if ( rank == 0 ) printf ("\nSolving system...");
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
                &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, b, x, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during solution: %lli", (long long int)error);
        goto final;
    }
    fprintf(fic, " %d\n",max(iparm[14], iparm[15]+iparm[16]));
    if ( rank == 0 )
    {

      char *file_Sol = NULL;
      file_Sol = (char *) calloc(strlen(argw[13]) + 20, sizeof(char));
      strcpy(file_Sol, argw[13]);
      strcat(file_Sol,"_Solution");
      write_Vector_in_file(file_Sol, n, x) ;
      
      free(file_Sol); file_Sol = NULL;
      /*
        printf ("\nThe solution of the system is: ");
        for ( j = 0; j < n; j++ )
        {
	  printf ("\n x [%lli] = % f % f", (long long int)j, real(x[j]), imag(x[j]));
        }


	initialize_tab(n, bs);
      
      //  Compute residual 
        struct matrix_descr descrA;
        sparse_matrix_t csrA;
        sparse_status_t status;
        status = mkl_sparse_z_create_csr( &csrA, SPARSE_INDEX_BASE_ONE, n, n, ia, ia+1, ja, a);
        if (status != SPARSE_STATUS_SUCCESS) {
            printf("Error in mkl_sparse_z_create_csr\n");
            mkl_sparse_destroy( csrA );
            goto final;
        }
        descrA.type = SPARSE_MATRIX_TYPE_GENERAL;
        // The following decriptor fields are not applicable for matrix type
        //SPARSE_MATRIX_TYPE_GENERAL but must be specified for other matrix types:
         
        // descrA.mode = SPARSE_FILL_MODE_FULL;
        // descrA.diag = SPARSE_DIAG_NON_UNIT;
        cplx alpha, beta;
        alpha = cplx(1.0, 0.0);
        beta = cplx(0.0,0.0);
        status = mkl_sparse_z_mv( SPARSE_OPERATION_NON_TRANSPOSE, alpha, csrA, descrA, x, beta, bs); 
        if (status != SPARSE_STATUS_SUCCESS) {
            printf("Error in mkl_sparse_z_mv\n");
            mkl_sparse_destroy( csrA );
            goto final;
        }
        mkl_sparse_destroy( csrA );

        res = 0.0;
        res0 = 0.0;
        for(i=0;i<n;i++){
	  res=res + abs((bs[i]-b[i])*(bs[i]-b[i]));
	  res0=res0 + abs(b[i]*b[i]);
        }
        res = sqrt ( res ) / sqrt ( res0 );
        printf ( "\nRelative residual = %e\n", res );
      */
    }

/* -------------------------------------------------------------------- */
/* .. Termination and release of memory. */
/* -------------------------------------------------------------------- */
    phase = -1; /* Release internal memory. */
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
                &n, &ddum, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during release memory: %lli", (long long int)error);
        goto final;
    }
    /* Check residual */
	if(rank == 0)
	{
        if ( res > 1e-10 )
        {
            printf ("\nError: residual is too high!\n");
            error = 5;
            goto final;
        }
	}
final:
    if ( rank == 0 )
    {
        if ( error != 0 )
        {
            printf("\n TEST FAILED\n");
        } else {
            printf("\n TEST PASSED\n");
        }
    }
    mpi_stat = MPI_Finalize();
    if (mpi_stat != MPI_SUCCESS) {
        if ( rank == 0 ) printf ("\nERROR finalizing MPI: %lli", (long long int)mpi_stat);
        if ( error == 0 ) error = mpi_stat;
    }
    
    fclose(fic);
    free(file_LU_info); file_LU_info = NULL;
    return error; 
    
}
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/

int solve_with_distributed_LU_method(int rank, MKL_INT debut, MKL_INT fin, MKL_INT n_glob, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *b, cplx *x, const char chaine[]){

  char *file = NULL;
  FILE* fic;
  if (rank == 0){
    file = (char *) calloc(strlen(chaine) + 20, sizeof(char));
    strcpy(file, chaine);
    strcat(file,"_LU_information.txt");
    
    if( !(fic = fopen(file,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
  }
    MKL_INT mtype = 13; /* Complex unsymmetric matrix */    

    MKL_INT nrhs = 1; /* Number of right hand sides. */
    /* Internal solver memory pointer pt, */
    /* 32-bit: int pt[64]; 64-bit: long int pt[64] */
    /* or void *pt[64] should be OK on both architectures */
    void *pt[64] = { 0 };
    /* Cluster Sparse Solver control parameters. */
    MKL_INT iparm[64] = { 0 };
    MKL_INT maxfct, mnum, phase, msglvl, error, err_mem;;

    /* Auxiliary variables. */
    cplx  ddum; /* Complex dummy   */
    MKL_INT idum; /* Integer dummy. */
    MKL_INT j;
    int     mpi_stat = 0;
    int     comm = 0;
   
    /* -------------------------------------------------------------------- */
    /* .. Init MPI.                                                         */
    /* -------------------------------------------------------------------- */
   
    comm =  MPI_Comm_c2f( MPI_COMM_WORLD );

    /* -------------------------------------------------------------------- */
    /* .. Setup Cluster Sparse Solver control parameters.                                 */
    /* -------------------------------------------------------------------- */
    iparm[ 0] =  1; /* Solver default parameters overriden with provided by iparm */
    iparm[ 1] =  2; /* Use METIS for fill-in reordering */
    iparm[ 5] =  0; /* Write solution into x */
    iparm[ 7] =  1; /* Max number of iterative refinement steps */
    iparm[ 9] = 16; /* Perturb the pivot elements with 1E-13 */
    iparm[10] =  0; /* Use nonsymmetric permutation and scaling MPS */
    iparm[12] =  0; //iparm[12] =  1; /* Switch on Maximum Weighted Matching algorithm (default for non-symmetric) */
    iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
    iparm[18] = -1; /* Output: Mflops for LU factorization */
    iparm[26] =  1; /* Check input data for correctness */
    iparm[39] =  2; /* Input: matrix/rhs/solution are distributed between MPI processes  */
    /* If iparm[39]=2, the matrix is provided in distributed assembled matrix input          
       format. In this case, each MPI process stores only a part (or domain) of the matrix A 
       data. The bounds of the domain should be set via iparm(41) and iparm(42). Solution    
       vector is distributed between process in same manner with rhs. */

    iparm[40] = debut; /* The number of row in global matrix, rhs element and solution vector
		      that begins the input domain belonging to this MPI process */
    iparm[41] = fin; /* The number of row in global matrix, rhs element and solution vector*/
    iparm[59] = 2;
    maxfct = 1; /* Maximum number of numerical factorizations. */
    mnum   = 1; /* Which factorization to use. */
    msglvl = 1; /* Print statistical information in file */
    error  = 0; /* Initialize error flag */
    err_mem = 0; /* Initialize error flag for memory allocation */

    cout<<" phase reordering and symbolic factorization ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Reordering and Symbolic Factorization. This step also allocates   */
    /* all memory that is necessary for the factorization.                  */
    /* -------------------------------------------------------------------- */
    phase = 11;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during symbolic factorization: %lli", (long long int)error);
        exit(rank);
    }

    if ( rank == 0 ) printf ("\nReordering completed ... ");
    if (rank == 0){
      fprintf(fic,"%d %d", n_glob, iparm[17]);
    }
    cout<<" phase numerical factorization ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Numerical factorization.                                          */
    /* -------------------------------------------------------------------- */
    phase = 22;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during numerical factorization: %lli", (long long int)error);
        exit(rank);
    }
    if ( rank == 0 ) printf ("\nFactorization completed ... ");
    cout<<" phase back substitution and iterative refinement ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Back substitution and iterative refinement.                       */
    /* -------------------------------------------------------------------- */
    phase = 33;

    if ( rank == 0 ) printf ("\nSolving system...");
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
       &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, b, x, &comm, &error );
    if ( error != 0 )
      {
	if ( rank == 0 ) printf ("\nERROR during solution: %lli", (long long int)error);
	exit(rank);
      }
   
    if (rank == 0){
      fprintf(fic, " %d\n",max(iparm[14], iparm[15]+iparm[62]));
      fclose(fic);
      free(file); file = NULL;
    }
  
    MPI_Barrier(MPI_COMM_WORLD);
   
    /* -------------------------------------------------------------------- */
    /* .. Termination and release of memory. */
    /* -------------------------------------------------------------------- */
    phase = -1; /* Release internal memory. */
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, &ddum, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during release memory: %lli", (long long int)error);
        exit(rank);
    }
final:
    MPI_Barrier(MPI_COMM_WORLD);
   
    if ( rank == 0 ) 
    {
        if ( error != 0 )
        {
            printf("\n TEST FAILED\n");
        } else {
            printf("\n TEST PASSED\n");
        }
    }
  
    return error;
}
  /*#####################################################################################################*/
int solve_with_distributed_LU_method(int rank, MKL_INT debut, MKL_INT fin, MKL_INT n_glob, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *b, cplx *x, MKL_INT *NNZ_LU, MKL_INT *MEM_LU, const char chaine[]){
/*
  char *file = NULL;
  FILE* fic;
  if (rank == 0){
    file = (char *) calloc(strlen(chaine) + 20, sizeof(char));
    strcpy(file, chaine);
    strcat(file,"_LU_information.txt");
    
    if( !(fic = fopen(file,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
  }
*/
    MKL_INT mtype = 13; /* Complex unsymmetric matrix */    

    MKL_INT nrhs = 1; /* Number of right hand sides. */
    /* Internal solver memory pointer pt, */
    /* 32-bit: int pt[64]; 64-bit: long int pt[64] */
    /* or void *pt[64] should be OK on both architectures */
    void *pt[64] = { 0 };
    /* Cluster Sparse Solver control parameters. */
    MKL_INT iparm[64] = { 0 };
    MKL_INT maxfct, mnum, phase, msglvl, error, err_mem;;

    /* Auxiliary variables. */
    cplx  ddum; /* Complex dummy   */
    MKL_INT idum; /* Integer dummy. */
    MKL_INT j;
    int     mpi_stat = 0;
    int     comm = 0;
   
    /* -------------------------------------------------------------------- */
    /* .. Init MPI.                                                         */
    /* -------------------------------------------------------------------- */
   
    comm =  MPI_Comm_c2f( MPI_COMM_WORLD );

    /* -------------------------------------------------------------------- */
    /* .. Setup Cluster Sparse Solver control parameters.                                 */
    /* -------------------------------------------------------------------- */
    iparm[ 0] =  1; /* Solver default parameters overriden with provided by iparm */
    iparm[ 1] =  2; /* Use METIS for fill-in reordering */
    iparm[ 5] =  0; /* Write solution into x */
    iparm[ 7] =  1; /* Max number of iterative refinement steps */
    iparm[ 9] = 16; /* Perturb the pivot elements with 1E-13 */
    iparm[10] =  0; /* Use nonsymmetric permutation and scaling MPS */
    iparm[12] =  0; //iparm[12] =  1; /* Switch on Maximum Weighted Matching algorithm (default for non-symmetric) */
    iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
    iparm[18] = -1; /* Output: Mflops for LU factorization */
    iparm[26] =  1; /* Check input data for correctness */
    iparm[39] =  2; /* Input: matrix/rhs/solution are distributed between MPI processes  */
    /* If iparm[39]=2, the matrix is provided in distributed assembled matrix input          
       format. In this case, each MPI process stores only a part (or domain) of the matrix A 
       data. The bounds of the domain should be set via iparm(41) and iparm(42). Solution    
       vector is distributed between process in same manner with rhs. */

    iparm[40] = debut; /* The number of row in global matrix, rhs element and solution vector
		      that begins the input domain belonging to this MPI process */
    iparm[41] = fin; /* The number of row in global matrix, rhs element and solution vector*/
    iparm[59] = 2;
    maxfct = 1; /* Maximum number of numerical factorizations. */
    mnum   = 1; /* Which factorization to use. */
    msglvl = 1; /* Print statistical information in file */
    error  = 0; /* Initialize error flag */
    err_mem = 0; /* Initialize error flag for memory allocation */

    cout<<" phase reordering and symbolic factorization ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Reordering and Symbolic Factorization. This step also allocates   */
    /* all memory that is necessary for the factorization.                  */
    /* -------------------------------------------------------------------- */
    phase = 11;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during symbolic factorization: %lli", (long long int)error);
        exit(rank);
    }

    if ( rank == 0 ) printf ("\nReordering completed ... ");
/*
    if (rank == 0){
      fprintf(fic,"%d %d", n_glob, iparm[17]);
    }
*/
    *NNZ_LU = iparm[17];

    cout<<" phase numerical factorization ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Numerical factorization.                                          */
    /* -------------------------------------------------------------------- */
    phase = 22;
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during numerical factorization: %lli", (long long int)error);
        exit(rank);
    }
    if ( rank == 0 ) printf ("\nFactorization completed ... ");
    cout<<" phase back substitution and iterative refinement ..."<<endl;
    /* -------------------------------------------------------------------- */
    /* .. Back substitution and iterative refinement.                       */
    /* -------------------------------------------------------------------- */
    phase = 33;

    if ( rank == 0 ) printf ("\nSolving system...");
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
       &n_glob, a, ia, ja, &idum, &nrhs, iparm, &msglvl, b, x, &comm, &error );
    if ( error != 0 )
      {
	if ( rank == 0 ) printf ("\nERROR during solution: %lli", (long long int)error);
	exit(rank);
      }
   /*
    if (rank == 0){
      fprintf(fic, " %d\n",max(iparm[14], iparm[15]+iparm[62]));
      fclose(fic);
      free(file); file = NULL;
    }
  */
    *MEM_LU = max(iparm[14], iparm[15]+iparm[62]);
    MPI_Barrier(MPI_COMM_WORLD);
   
    /* -------------------------------------------------------------------- */
    /* .. Termination and release of memory. */
    /* -------------------------------------------------------------------- */
    phase = -1; /* Release internal memory. */
    cluster_sparse_solver ( pt, &maxfct, &mnum, &mtype, &phase,
        &n_glob, &ddum, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &comm, &error );
    if ( error != 0 )
    {
        if ( rank == 0 ) printf ("\nERROR during release memory: %lli", (long long int)error);
        exit(rank);
    }
final:
    MPI_Barrier(MPI_COMM_WORLD);
   
    if ( rank == 0 ) 
    {
        if ( error != 0 )
        {
            printf("\n TEST FAILED\n");
        } else {
            printf("\n TEST PASSED\n");
        }
    }
  
    return error;
}
 
/************************************************** NORMES DISTRIBUEES ************************************************************/
double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  return I;
}


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  return I;
}


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin, l;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph);

    }
#pragma omp atomic
    I += s; 
  }
 
  return I;
}



double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin, l;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      cplx *u_loc;

      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);

    }
#pragma omp atomic
    I += s; 
  }
 
  return I;
}



double norme_H1_volumique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  return I;
}



double  norme_H1_erreur_volumique_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin, l;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];
  
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  return I;
}





double norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      s += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, MSph) + norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
    
  return I;
}


double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];

  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin ; e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph) + norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }
  
  
  return I;
}


double norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph, const char chaine[]){
  //Cette fonction construit la matrice A_skyline des termes int_{F}[P][P']+[V][V']
  //F étant une face 
  /*---------------------------------------------------------------------------------- input ---------------------------------------------------
    A_glob    : tableau skyline ou stocké la matrice
    MSph      : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------
                                              A_glob la matrice du systeme A U = F
    /*------------------------------------------------------------------------------ intermédiaire --------------------------------------------
    e         : indice global d'un élément
    f         : indice local d'une face
    B1        : pour stocker un bloc d'interaction element-element sur une face l;
                 int_{l} P_T P'_T

    B2        : .................................. voisin-element ............. l;
                 int_{l} P_K P'_T

    AA1       : pour stocker l'adresse de A_glob dans l'emplacement correspondant à l'interaction 
                element-element pour un element e quelconque

    AA2       : pour stocker A_glob dans l'emplacement correspondant à l'interaction
                 voisin-element pour un voisin quelconque de l'element e quelconque
    /*-----------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, taille = MSph.ndof * MSph.ndof;
  MKL_INT debut, fin;
  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

  double S1 = 0.0;
  double S2 = 0.0;
  double s1, s2;
  cplx *B1 = nullptr, *B2 = nullptr, *bloc_diag = nullptr, *bloc_extra = nullptr;
#pragma omp parallel private(B1, B2, e, bloc_diag, bloc_extra, s1, s2)
  {
    B1 = new cplx[taille];
    B2 = new cplx[taille];
    bloc_diag = new cplx[taille];
    bloc_extra = new cplx[4 * taille];
   
    s1 = 0.0; s2 = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      cplx I = cplx(0.0, 0.0);
      MKL_INT iloc, jloc;
      initialize_tab(taille, bloc_diag);
      
      initialize_tab(4 * taille, bloc_extra);
      
      for (MKL_INT f = 0; f < 4; f++) {
	
	
	if (MSph.elements[e].voisin[f] >= 0) {//Sur les faces intérieures
	  
	  //stockage de int_{l} P_T P'_T  dans B1 et int_{l} P_K P'_T dans B2
	  Build_interior_planeBloc(e, f, B1, B2, MSph, chaine);
	  
	  //--------------------------------------  interaction element-element ------------------------------------------------
	  //ajout du bloc P_T P'_T associé à la face f
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face
	  Ajouter_BlocIntV_TxV_T(e, f, 1.0, B1, bloc_diag, MSph);
	  	  
	  //--------------------------------------  interaction voisin-element ------------------------------------------------
	  cplx *D = bloc_extra + f * taille;
	  
	  //ajout du bloc P_K P'_T associé à la face
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K V'_Tn_T associé à la face f
	  Ajouter_BlocIntV_KxV_T(e, f, 1.0, B2, D, MSph);

	}//Fin du if
      }//Fin boucle sur les faces

      cplx *x;
      get_F_elem(MSph.ndof, e, &x, U);
      
      for(iloc = 0; iloc < MSph.ndof; iloc++){
	for(jloc = 0; jloc < MSph.ndof; jloc++){
	  I +=  conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
      
      for(MKL_INT f = 0; f < 4; f++){
	if (MSph.elements[e].voisin[f] >=0){
	  
	  cplx *D = bloc_extra + f * taille;
	  
	  cplx *y;
	  
	  get_F_elem(MSph.ndof, MSph.elements[e].voisin[f], &y, U);
	  
	  
	  for(iloc = 0; iloc < MSph.ndof; iloc++){
	    for(jloc = 0; jloc < MSph.ndof; jloc++){
	      I +=  conj(x[iloc]) * D[jloc * MSph.ndof + iloc] * y[jloc];
	    }
	  }

	}
      }

      s1 += real(I);
      s2 += imag(I);
      
    }//Fin boucle sur les éléments
    delete [] B1; B1 = nullptr;
    delete [] B2; B2 = nullptr;
    delete [] bloc_diag; bloc_diag = nullptr;
    delete [] bloc_extra; bloc_extra = nullptr;

#pragma omp atomic
    S1 += s1;

#pragma omp atomic
    S2 += s2;
  }//Fin de la région parallele
  
  cplx J = cplx(S1, S2);
  
  double r = abs(J);

  return r;
}


double norme_H1_de_l_erreur_avec_integ_mixed(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph, const char chaine[]){
  
  double I = 0.0;
  
  I = norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(rank, counts_elem, displs_elem, U, MSph,chaine);
  I += norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  
  return I;
}





void compute_pressure_and_velocity_norms(int rank, int *counts_elem, int *displs_elem, cplx *U, const char path_to_repertory[], const char method[], MailleSphere &MSph){

  MKL_INT n = MSph.get_N_elements() * MSph.ndof;

  double L2P_ex, L2V_ex, L2P_err, L2V_err, HV_err, HV_ex, HS_ex, H1M_err;
  L2P_ex = 0.0; L2V_ex = 0.0;
  L2P_err = 0.0; L2V_err = 0.0;
  HV_err = 0.0; HV_ex = 0.0;
  HS_ex = 0.0; H1M_err = 0.0;

  cplx H1S_err_int = cplx(0.0, 0.0);
  double H1S_err_ext = 0.0;
  
  double t1 = MPI_Wtime();
  double local_L2P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t2 = MPI_Wtime();
  if (rank == 0){
    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<t2-t1<<" secs"<<endl;
  }
  
  double t3 = MPI_Wtime();
  double local_L2V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t4 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<t4-t3<<" secs"<<endl;
  }
  double t5 = MPI_Wtime();
  double local_L2P_err =  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t6 = MPI_Wtime();
  
  if (rank == 0){
    
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<t6-t5<<" secs"<<endl;
  }
  
  double t7 = MPI_Wtime();
  double local_L2V_err = norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t8 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<t8-t7<<" secs"<<endl;
  }
  double t9 = MPI_Wtime();
  double local_HV_ex = norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t10 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex||_{H^1} volumique: "<<t10-t9<<" secs"<<endl;
  }
  double t11 = MPI_Wtime();
  double local_HV_err = norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t12 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} volumique: "<<t12-t11<<" secs"<<endl;
  }
  
  double t13 = MPI_Wtime();
  double local_HS_ex = norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  
  
  double local_H1M_err = norme_H1_de_l_erreur_avec_integ_mixed(rank, counts_elem, displs_elem, U, MSph, "double");
  double t14 = MPI_Wtime();


  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} modifiée: "<<t14-t13<<" secs"<<endl;
  }

  MPI_Datatype resized_Complex = create_mpi_complex_type();
  
  MPI_Allreduce(&local_L2P_ex ,&L2P_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2V_ex ,&L2V_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_HV_ex ,&HV_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_HS_ex ,&HS_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2P_err ,&L2P_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2V_err ,&L2V_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_HV_err ,&HV_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_H1M_err ,&H1M_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Barrier(MPI_COMM_WORLD);
  
  L2P_err = sqrt(L2P_err);
  L2V_err = sqrt(L2V_err);
  
  L2P_ex = sqrt(L2P_ex);
  L2V_ex = sqrt(L2V_ex);
  
  HV_err = sqrt(HV_err);
  HV_ex = sqrt(HV_ex);
  
  HS_ex = sqrt(HS_ex);
  
  H1M_err = sqrt(H1M_err);
  
  cout<<"norme Volumique "<<endl;
  
  double L2P_err_rela = L2P_err / (double) L2P_ex;
  double L2V_err_rela = L2V_err / (double) L2V_ex;
  double HV_err_rela = HV_err / (double) HV_ex;
  double H1M_err_rela = H1M_err / (double) HS_ex;
  
  if (rank == 0){
    cout<<" ||P_ex - P_h||_{L2} / ||P_ex[[_{L^2} = "<<L2P_err_rela<<" ||V_ex - V_h||_{L2} / ||V_ex||_{L^2}= "<<L2V_err_rela<<endl;
    
    cout<<" norme H1 volumique ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<HV_err_rela<<endl;
    
    cout<<" norme H1 modifiée ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<H1M_err_rela<<endl;
    
    char *file_absolue = NULL;
    file_absolue = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
    strcpy(file_absolue, path_to_repertory);
    strcat(file_absolue,"_norme_absolue.txt");
      
    char *file_relative = NULL;
    file_relative = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
    strcpy(file_relative, path_to_repertory);
    strcat(file_relative,"_norme_relative.txt");
      
    FILE* fic_absolue;
    
    FILE* fic_relative;
    
    if( !(fic_absolue = fopen(file_absolue,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
    
    if( !(fic_relative = fopen(file_relative,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
 
    cout<<"norme Volumique "<<endl;
    
    if (strcmp(method, "cess-des") == 0){
      
      fprintf(fic_absolue,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err, L2V_err, HV_err, H1M_err);
      
      fprintf(fic_relative,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err_rela, L2V_err_rela, HV_err_rela, H1M_err_rela);
    }
    else{
      
      FILE* fic_Cond;
      char *file_Cond = NULL;
      file_Cond = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
      strcpy(file_Cond, path_to_repertory);
      strcat(file_Cond,"_Cond_Vol.txt");
      
      if( !(fic_Cond = fopen(file_Cond,"a")) ) {
	fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
	    exit(EXIT_FAILURE);
      }
      
      fprintf(fic_absolue,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err, L2V_err, HV_err, H1M_err);
      
      fprintf(fic_relative,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err_rela, L2V_err_rela, HV_err_rela, H1M_err_rela);
      
      fprintf(fic_Cond,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, MSph.lambda_min, MSph.lambda_max, MSph.lambda_max_normalisee / (double) MSph.lambda_min_normalisee);
      
      
      fclose(fic_Cond); free(file_Cond); file_Cond = NULL;
      
    }
    
    
    fclose(fic_absolue); fclose(fic_relative);
    free(file_relative); file_relative = NULL;
    free(file_absolue); file_absolue = NULL;
  }
}
/************************************************** NORMES PONDEREES EN DISTRIBUEE *************************************/

double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];

#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      s +=  MSph.elements[e].xi * norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_ponderee_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  return I;
}


double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e, debut, fin, l;

  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];
  
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s +=  MSph.elements[e].xi * norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_ponderee_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  return I;
}



void BuildVolumeMetricMatrix_ponderee_numerical_integ(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique surfacique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e, l; //e : boucle sur le numeros globaux des éléments
                //l : ..................... locaux ............
  /*------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc; //boucle sur les fonctions de bases
  
  l = 0;
  
  for (e = displs_elem[rang]; e < displs_elem[rang] + counts_elem[rang]; e++) {
    
    cplx *M =  M_glob.Tab_A_loc[l].get_Aloc(); // pour stocker l'adresse du bloc associé à e
    
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      for(jloc = 0; jloc < MSph.ndof; jloc++) {
	M[jloc * MSph.ndof + iloc] =   MSph.elements[e].xi * quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]) + quad_tetra_VV_ponderee(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
      }
    }
    
    l++;
  }
}


void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
  /*------------------------------------------------------------------------------------------------------------------*/
  MKL_INT debut, fin, l;
  debut = displs_elem[rang];
  fin = displs_elem[rang] + counts_elem[rang];
  
  l = 0;
#pragma omp parallel private(e,l)
  {
    MKL_INT iloc, jloc; //boucle sur les fonctions de bases
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      l = e - debut;
      
      cplx *M =  M_glob.Tab_A_loc[l * M_glob.get_n_inter() + 0].get_Aloc(); // pour stocker l'adresse du bloc associé à e
      
      for(iloc = 0; iloc < MSph.ndof; iloc++) {
	for(jloc = 0; jloc < MSph.ndof; jloc++) {
	  M[jloc * MSph.ndof + iloc] =   MSph.elements[e].xi * quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]) + quad_tetra_VV_ponderee(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
	}
      }
    }
  }
}


void BuildVolumeMetricMatrix_ponderee_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
    /*------------------------------------------------------------------------------------------------------------------*/
  MKL_INT debut, fin, l;
  debut = displs_elem[rang];
  fin = displs_elem[rang] + counts_elem[rang];
  
  l = 0;
#pragma omp parallel private(e,l)
  {
    MKL_INT iloc, jloc; //boucle sur les fonctions de bases
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {
      
      l = e - debut;
      
      cplx *M =  M_glob.Tab_A_loc[l * M_glob.get_n_inter() + 0].get_Aloc(); // pour stocker l'adresse du bloc associé à e
      for(iloc = 0; iloc < MSph.ndof; iloc++) {
	for(jloc = 0; jloc < MSph.ndof; jloc++) {
	  M[jloc * MSph.ndof + iloc] =   MSph.elements[e].xi * integrale_tetra(MSph.elements[e], iloc, jloc, chaine) + integrale_tetra_VV_ponderee(MSph.elements[e], iloc, jloc, chaine);
	}
      }
    }
  }
}


void BuildVector_Of_Volume_Projection_ponderee(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph){
  MKL_INT e, iloc, iglob, l;
  l = 0;
  for(e = displs_elem[rang]; e < displs_elem[rang] + counts_elem[rang]; e++) {
   
    //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
    for(iloc = 0; iloc < MSph.ndof; iloc++) {

      iglob = local_to_glob(iloc, l, MSph.ndof);

      B[iglob] =  MSph.elements[e].xi * quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV_ponderee(e, iloc, MSph);
    }
    l++;
  }
}


void BuildVector_Of_Volume_Projection_ponderee_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph){
  MKL_INT e, l, debut, fin;
  
  debut = displs_elem[rang];
  fin = displs_elem[rang] + counts_elem[rang];
  
  //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
#pragma omp parallel private(e, l)
  {
#pragma omp for schedule(runtime)
    for(e = debut; e < fin; e++) {
      
      l = e - debut;
	
	for(MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	  
	  MKL_INT iglob = local_to_glob(iloc, l, MSph.ndof); 
	  
	  B[iglob] =  MSph.elements[e].xi * quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV_ponderee(e, iloc, MSph);
	}
    }
  }
}


double normeDG(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *X, MailleSphere &MSph) {
  //Cette fonction calcule la norme DG associée au vecteur X
  /*------------------------------------------------------------------------- input -----------------------------------------------------------------
    A_glob    : objet de type A_skyline, c'est la matrice du système linéaire
    X         : objet de type VecN, il contient un vecteur
    MSph      : le maillage
    /*----------------------------------------------------------------------- ouput -----------------------------------------------------------------
    s         : sqrt(real(X^* A_glob X)), la norme DG de X
    /*-------------------------------------------------------------- variables intermédiares -------------------------------------------------------
    i         : boucle sur les éléments
    j         : .......... les numéros locals des voisins des des éléments
    iloc      : .......... les fonctions-tests
    jloc      : .......... les fonctions solutions
    AA        : pour clôner la matrice locale sur chaque élément
    x         : pour clôner le vecteur local sur chaque élément
    y         : ................................ le voisin de chaque élément
    I         : X^* A_glob X
    /*----------------------------------------------------------------------------------------------------------------------------------------------*/  
  MKL_INT i, taille = MSph.ndof * MSph.ndof;
  MKL_INT debut, fin, l;
  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];
  
  double r = 0.0;
  double s;
 
#pragma omp parallel private(i, l, s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (i = debut; i < fin; i++) {
      cplx I = cplx(0.0, 0.0);
      MKL_INT iloc, jloc;
      
      l = i - debut;
      cplx *bloc_diag = A_glob.Tab_A_loc[l * A_glob.get_n_inter() + 0].get_Aloc();//bloc associé à l'élément i
      cplx *x;
      get_F_elem(MSph.ndof, i, &x, X);//clônage de X sur l'élément i
      
      for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
	for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
	  
	  I += conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
    
    
    for (int j = 0; j < 4; j++) {
      
      if (MSph.elements[i].voisin[j] >= 0) {

	cplx *y;

	cplx *bloc_extra = A_glob.Tab_A_loc[l * A_glob.get_n_inter() + (j+1)].get_Aloc();//bloc associé au voisin j de l'élément i
	
	get_F_elem(MSph.ndof, MSph.elements[i].voisin[j], &y, X);//clônage de X sur le voisin j de l'élément i
	
	for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
	  for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
	    
	    I += conj(x[iloc]) * bloc_extra[jloc * MSph.ndof + iloc] * y[jloc];
	  }
	}
      }
    }
    s += real(I);
    }//fin boucle sur les éléments
  
#pragma omp atomic
    r += s;
    
  }//Fin de la région parallele
  
  return r;
}




double normeDG_en_Desamblee(int rank, int *counts_elem, int *displs_elem, cplx *X, MailleSphere &MSph, const char precision[]) {
  //Cette fonction calcule la norme DG associée au vecteur X
  /*------------------------------------------------------------------------- input -----------------------------------------------------------------
    A_glob    : objet de type A_skyline, c'est la matrice du système linéaire
    X         : objet de type VecN, il contient un vecteur
    MSph      : le maillage
    /*----------------------------------------------------------------------- ouput -----------------------------------------------------------------
    s         : sqrt(real(X^* A_glob X)), la norme DG de X
    /*-------------------------------------------------------------- variables intermédiares -------------------------------------------------------
    i         : boucle sur les éléments
    j         : .......... les numéros locals des voisins des des éléments
    iloc      : .......... les fonctions-tests
    jloc      : .......... les fonctions solutions
    AA        : pour clôner la matrice locale sur chaque élément
    x         : pour clôner le vecteur local sur chaque élément
    y         : ................................ le voisin de chaque élément
    I         : X^* A_glob X
    /*----------------------------------------------------------------------------------------------------------------------------------------------*/  
  MKL_INT e, taille = MSph.ndof * MSph.ndof;
  MKL_INT debut, fin;
  debut = displs_elem[rank];
  fin = debut + counts_elem[rank];
  
  double r = 0.0;
  double s;
  cplx *B1 = nullptr, *B2 = nullptr, *bloc_diag = nullptr, *bloc_extra = nullptr;
#pragma omp parallel private(B1, B2, e, bloc_diag, bloc_extra, s)
  {
    B1 = new cplx[taille];
    B2 = new cplx[taille];
    bloc_diag = new cplx[taille];
    bloc_extra = new cplx[4 * taille];
    
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = debut; e < fin; e++) {

      cplx I = cplx(0.0, 0.0);

      MKL_INT iloc, jloc;

      initialize_tab(taille, bloc_diag);
      
      initialize_tab(4 * taille, bloc_extra);
      
      for (MKL_INT f = 0; f < 4; f++) {
	
	
	if (MSph.elements[e].voisin[f] >= 0) {//Sur les faces intérieures
	  
	  //stockage de int_{l} P_T P'_T  dans B1 et int_{l} P_K P'_T dans B2
	  Build_interior_planeBloc(e, f, B1, B2, MSph, precision);
	  
	  //--------------------------------------  interaction element-element ------------------------------------------------
	  //ajout du bloc P_T P'_T associé à la face f 
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face
	  Ajouter_BlocIntV_TxV_T(e, f, MSph.elements[e].T_VV[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T P'_T associé à la face
	  Ajouter_BlocIntV_TxP_T(e, f, MSph.elements[e].T_VP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc P_T V'_Tn_T associé à la face
	  Ajouter_BlocIntP_TxV_T(e, f, MSph.elements[e].T_PV[f], B1, bloc_diag, MSph);
	  
	  //--------------------------------------  interaction voisin-element ------------------------------------------------
	  cplx *D = bloc_extra + f * taille;
	  
	  //ajout du bloc P_K P'_T associé à la face
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K V'_Tn_T associé à la face f
	  Ajouter_BlocIntV_KxV_T(e, f, MSph.elements[e].T_VV[f], B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K P'_T associé à la face f
	  Ajouter_BlocIntV_KxP_T(e, f, MSph.elements[e].T_VP[f], B2, D, MSph);
	  
	  //ajout du bloc P_K V'_Tn_T associé à la face
	  Ajouter_BlocIntP_KxV_T(e, f, MSph.elements[e].T_PV[f], B2, D, MSph);

	}//Fin du if
	else {
	  
	  //stockage de int_{l} P_T P'_T  dans B1
	  Build_exterior_planeBloc(e, f, B1, MSph, precision);
	  
	  //ajout du bloc P_T P'_T associé à la face f
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face f
	  Ajouter_BlocExtV_TxV_T(e, f, MSph.elements[e].T_VV[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T P'_T associé à la face f
	  Ajouter_BlocExtV_TxP_T(e, f, MSph.elements[e].T_VP[f], B1, bloc_diag, MSph);
	  
	//ajout du bloc P_T V'_Tn_T associé à la face f
	  Ajouter_BlocExtP_TxV_T(e, f, MSph.elements[e].T_PV[f], B1, bloc_diag, MSph);
	  
	}//Fin du else
      }//Fin boucle sur les faces
      
      cplx *x;
      get_F_elem(MSph.ndof, e, &x, X);
      
      for(iloc = 0; iloc < MSph.ndof; iloc++){
	for(jloc = 0; jloc < MSph.ndof; jloc++){
	  I +=  conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
      
      for(MKL_INT f = 0; f < 4; f++){
	if (MSph.elements[e].voisin[f] >=0){
	  
	  cplx *bloc_voisin_elem = bloc_extra + f * taille;
	  
	  cplx *y;
	  
	  get_F_elem(MSph.ndof, MSph.elements[e].voisin[f], &y, X);
	  
	  
	  for(iloc = 0; iloc < MSph.ndof; iloc++){
	    for(jloc = 0; jloc < MSph.ndof; jloc++){
	      I +=  conj(x[iloc]) * bloc_voisin_elem[jloc * MSph.ndof + iloc] * y[jloc];
	    }
	  }
	  
	}
      }
      
      s += real(I);    
    }//Fin boucle sur les éléments
    delete [] B1; B1 = nullptr;
    delete [] B2; B2 = nullptr;
    delete [] bloc_diag; bloc_diag = nullptr;
    delete [] bloc_extra; bloc_extra = nullptr;

#pragma omp atomic
    r += s;

  }//Fin de la région parallele

  return r;
}


double Assembly_and_compute_DG_norm(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *U, MailleSphere &MSph, const char precision[]){
  
  //Assemblage de A_glob dans le process rank
  ASSEMBLAGE_DE_Askyline_parallel_version(rank, counts_elem, displs_elem, A_glob, MSph, precision);

  //norme DG de U dans le process rank
  double I = normeDG(rank, counts_elem, displs_elem, A_glob, U, MSph);

  return I;
}
