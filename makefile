
# $(COMPOSYX_INCLUDES)compsyx.hpp $(COMPOSYX_INCLUDES)solver/GMRES.hpp $(COMPOSYX_INCLUDES)solver/Pastix.hpp $(COMPOSYX_INCLUDES)solver/PartSchurSolver.hpp $(COMPOSYX_INCLUDES)/solver/ConjugateGradient.hpp $(COMPOSYX_INCLUDES)IO/ReadParam.hpp $(COMPOSYX_INCLUDES)graph/Paddle.hpp
# -I$(TLAPACK_INCLUDES)
include  ./Make.inc

OPTC = -std=c++11  -DUSE_MKL=ON -DUSE_QUADMATH=ON -w -c -fopenmp -I$(INC_MPI)   -I$(INC_MKL)  -I$(COMPOSYX_INCLUDES)

all : main_trefftz  

OBJS  = Source_cluster.o Source_Vec3.o Source_Mesh.o Source_A_Block.o Source_A_skyline.o Useful_Source.o Solve.o 


Source_cluster.o : Source_cluster.cpp Header.h
	$(CC) $(OPTC) Source_cluster.cpp 

Source_Vec3.o : Source_Vec3.cpp Header.h
	$(CC) $(OPTC) Source_Vec3.cpp 

Source_Mesh.o : Source_Mesh.cpp Header.h
	$(CC) $(OPTC) Source_Mesh.cpp 

Source_A_Block.o : Source_A_Block.cpp Header.h
	$(CC) $(OPTC) Source_A_Block.cpp 

Source_A_skyline.o : Source_A_skyline.cpp Header.h
	$(CC) $(OPTC) Source_A_skyline.cpp

Useful_Source.o : Useful_Source.cpp Header.h
	$(CC) $(OPTC) Useful_Source.cpp  

Solve.o : Solve.cpp Header.h
	$(CC) $(OPTC) Solve.cpp 

main_trefftz: $(OBJS)
	$(CC)   $(OBJS) $(LIBSS)   -o main_trefftz 
clean:
	rm -f *.o *.mod
