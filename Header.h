#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <ctime>
#include <fstream>
#include <cstring>
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <sys/time.h>
#include <cassert>
#include <omp.h>
#include <bits/stdc++.h>
#include <cstddef>
#include <random>
#include <vector>
#include <mpi.h>

using cplx = std::complex<double>;

#define MKL_Complex16 std::complex<double>
#ifdef USE_QUADMATH
#include <quadmath.h>
#endif

#if defined(USE_COMPOSE)
#ifndef COMPOSE_DEFINED
#define COMPOSE_DEFINED
#define COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED
#include <composyx.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/solver/PartSchurSolver.hpp>

#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/graph/Paddle.hpp>
using namespace::composyx;
#endif
#endif // USE_COMPOSE

#ifdef USE_MKL
#include <mkl.h>
#include "mkl_cluster_sparse_solver.h"
#include <mkl_cblas.h>
#include <mkl_lapacke.h>
#else
#if !defined(USE_COMPOSE) 
#define  lapack_complex_double std::complex<double>
#endif
//#include <cblas.h>
//#include <lapacke.h>
#endif

#if defined(MKL_ILP64)
#define MKL_INT long long
#define MPI_DT MPI_LONG
#else
#define MKL_INT int
#define MPI_DT MPI_INT
#endif


struct ComplexDouble {
    double real;
    double imag;
};

//Structure pour la matrice A_skyline
struct A__skyline
{
  MKL_INT n_A = -1;
  MKL_INT n_elem = -1;
  MKL_INT n_dof = -1;
  MKL_INT n_inter = -1;
};
 
struct type_2{
  int incre = 0;
  char *chaine =NULL;
  double *residual = nullptr;
};

class Sommet {
public :
	Sommet();
	void set_X(double *A); // fixe l'adresse de X
	double* get_X(); // recupere l'adresse de X
	void set_ref(MKL_INT N); // fixe le numéro de reference du sommet
	MKL_INT get_ref(); // récupère le numéro de référence
	void print_info(); // écrit les informations stockées dans Sommet
        void Allocate_X();
        void set_val(double a, double b, double c);
private :
	double* X = (double*)nullptr;  // Le vecteur avec les coordonnées des sommets 
	MKL_INT ref;  // le numero de reference du sommet
};
// Classe utilisee pour les 4 faces d'un tetra
class Face {
public:
	Face();
	Sommet* noeud[3]; // Liste des trois sommets de la face
	void print_info(); // ecrit à la console ce qui se trouve dans Face
	void compute_XG(); // Calcule les coordonnees du centre de gravité
	void set_ref(MKL_INT N); // fixe le numéro de ref de la face
	MKL_INT get_ref(); // recupere le numero de ref de la face
//	void set_voisin(MKL_INT N); // fixe le numero de ref du tetra voisin
//	MKL_INT get_voisin(); // recupere le numero de ref  du tetra voisin
	double XG[3]; // coordonnee du centre de gravité
	MKL_INT voisin1; // numéro du voisin 1
	MKL_INT voisin2; // numéro du voisin 2
        double Aire;
        void compute_Aire_triangle();
        void compute_Aire_triangle(double *X1X2, double *X1X3, double *prod_Vecto) ;
private:
	MKL_INT ref; // numero de reference de la face
//	MKL_INT voisin; // ref du tetra voisin a la face
};


void affectation(Face &G, Face &F);

class Mat3 {
public:
	Mat3();
        double value[9];//stocker une matrice de taille 3
        double eigen[3];//valeurs propre de la matrice value
        double ADP[9];//matrice dont les coeffs sont les racines carrées de la matrice value
        double ADN[9];//matrice dont les coeffs sont 1 / sqrt de la matrice value
	double invmat[9];// l'inverse de la matrice value
        void print_info();//info de la classe
        void define(double a, double b, double c, double d, double e, double f, double g, double h, double i);//remplissage de la matrice value
	double eval(MKL_INT i, MKL_INT j);
        double det();//determinant de la matrice value
        MKL_INT issym();//test si la matrice value est symmetric
	MKL_INT isantisym();//test si la matrice value est antisymmetric
	void compute_inverse();
};

void affectation(Mat3 &mat_Y, Mat3 &mat_X);

class VecN {
public :
  VecN();
  std::complex<double>* value =  nullptr;
  MKL_INT n;
  void Allocate();
  void set_n(MKL_INT an);
  MKL_INT get_n();
  void get_F_elem(MKL_INT ndof, MKL_INT e, cplx **B);
  void get_F_elem(MKL_INT ndof, cplx **B);
  void SoustractionVecN(VecN &v);
  void print_info();
  //~VecN();
};


class Onde_plane {
public :
  Onde_plane();
  cplx P;
  cplx V[3];
  double K[3];
  double d[3];
  double Ad[3];
  double AK[3];
  double dAd;
  double ksurOmega;
  void compute_Ad_and_dAd(Mat3 &mat, double m_xi);
  void compute_P_and_V(double *X);
  void compute_K(double m_xi, double omega);
  void compute_KofCesDep(Mat3 &mat, double m_xi, double omega);
  double get_normk();
  void set_normk(double r);
  void define_d(double x, double y, double z);
  void copy(Onde_plane &OP);
  cplx pression(double *X);
  cplx calcule(double *X);
  void print_info();
private :
  double normk;
};

void affectation(Onde_plane &P1, Onde_plane &P2);

class Hankel {
public :
  Hankel();
  std::complex<double> P;//la pression incidente en X 
  std::complex<double> *V;// la vitesse incidente en X
  double *d;//la direction de l'onde incidente
  double r;// r = ||X - X0||
  double k;
  double kr;//kr = normk * r
  double v;//v = 1.0 / (4 * M_PI * r)
  void set_d(double *dd);
  double * get_d();
  MKL_INT compute_P_and_V(Mat3 &mat, double *X, double *X0);//pour calculer P et V en X
  void print_info();//information sur la classe
  ~Hankel();//destructeur
};



class Quad_2D {
public :
  Quad_2D();
  MKL_INT ordre;
  double *Xtriangle =  nullptr;
  double *Wtriangle =  nullptr;
  void Allocate_quad_2D();
  void Allocate_quad_2D(double *X, double *W);
  void quad_gauss_triangle(double *X1d, double *W1d);
  void test_zero();
  void test_One();
  void test_XYZ();
  void print_info();
  // ~Quad_2D();
};


class Quad {
public :
  Quad();
  MKL_INT ordre;
  double *Xtetra =  nullptr;
  double *Wtetra =  nullptr;
  void Allocate_quad();
  void Allocate_quad(double *X3d, double *W3d);
  void quad_gauss_tetra(double *X1d, double *W1d);
  void print_info();
  void test_zero();
  void test_One();
  void test_X1X2X3X4();
  //~Quad();
};


class Element {
public:
	Element();
        void compute_An_and_nAn(MKL_INT l);
        void compute_An_and_nAn(MKL_INT l, double Z);
	Sommet* noeud[4];
	Face face[4];
	MKL_INT get_ref();
	void set_ref(MKL_INT N);
        void compute_XG();
	void print_info();
        cplx Pexacte;
        cplx Vexacte[3];
        cplx Pro_P;
        cplx Pro_V[3];
	MKL_INT voisin[4];

        double temps_A;//temps de calcule de la matrice de projection 
        double temps_F;//temps de calcule du vecteur de projection
        double temps_inv;//temps de résolution
        double temps_Total;

        double Jac;// la jacobienne de la transformation lors du calcul des intégrales volumique
        double Vol;//calcule le volume du tetra
        double eps_max;// notre mode de filtrage sur chaque element
        double eps_max_Tikhonov;// regulateur Tikhonov associé au volume de l'élément
        double rc_Vol;//la racine cubique du vol du tetra
        double facto_volume;//facto_volume = Vol pour troncature avec la matrice de mass volumique 
                            //facto_volume = pow(Vol, 2/ 3) pour troncature matrice mass surfacique
        double hmax = 0.0;
        double hmin = 1000;
        double n1[3];
        double n2[3];
        double n3[3];
        double n4[3];
        double Y[4];
        double Z[4];
        double Q_T;// en rapport avec le coefficient de reflexion
        double An1[3];
        double An2[3];
        double An3[3];
        double An4[3];
        double alphaPP[4];//alphaPP[i] coeff de PP associé à la face i
        double alphaPV[4];//alphaPV[i] ........ PV ................. i
        double alphaVP[4];//alphaVP[i] ........ VP ................. i
        double alphaVV[4];//alphaVV[i] ........ VV ................. i
        double TL_PP[4]; //TL_PP[i] coeff de p_Tp_L associé à la face i
        double TL_PV[4];//TL_PV[i]  coeff de p_TV_L ................. i
        double TL_VP[4];//TL_PV[i]  coeff de V_Tp_L ................. i
        double TL_VV[4];//TL_VV[i]  coeff de V_TV_L ................. i
        double LT_PP[4];//LT_PP[i]  coeff de p_Lp_T ................. i
        double LT_PV[4];//LT_PV[i]  coeff de p_LV_T ................. i
        double LT_VP[4];//LT_VP[i]  coeff de V_Lp_T ................. i
        double LT_VV[4];//LT_VV[i]  coeff de V_TV_L ................. i
        double betaPP[4];//betaPP[i] : coefficient de PP associé à la i ème face de bord
        double betaPV[4];//betaPV[i] : coefficient de PV associé à la i ème face de bord
        double betaVP[4];//betaVP[i] : coefficient de VP associé à la i ème face de bord
        double betaVV[4];//betaVV[i] : coefficient de VV associé à la i ème face de bord
        double alpha_PincP[4];//alpha_PincP[i] : coefficient de Pinc * P associé à la i ème face
        double alpha_PincV[4];//alpha_PincV[i] : coefficient de Pinc * V associé à la i ème face
        double alpha_VincP[4];//alpha_VincP[i] : coefficient de Pinc * V associé à la i ème face
        double alpha_VincV[4];//alpha_VincV[i] : coefficient de Pinc * V associé à la i ème face
        double XG[3];
        double rad;
  
        double T_PP[4];//TT_PP[i] coeff de P_T * P'_T associé à la face i
        double L_PP[4];//LT_PP[i] coeff de P_L * P'_T associé à la face i
        double T_PV[4];//TT_PV[i] coeff de P_T * V'_T n_T associé à la face i
        double L_PV[4];//LT_PV[i] coeff de P_L * V'_T n_T associé à la face i
        double T_VP[4];//TT_VP[i] coeff de V_T * P'_T associé à la face i
        double L_VP[4];//LT_VP[i] coeff de V_L * P'_T associé à la face i
        double T_VV[4];//TT_VV[i] coeff de V_T * V'_T associé à la face i
        double L_VV[4];//LT_VV[i] coeff de V_L * V'_T associé à la face i

        MKL_INT Nred;// nouvelle taille de chaque espace de Galerkin local
        MKL_INT Nres;// ndof - Nred

        //Pour la projection de la solution
        MKL_INT Nred_Pro;// nouvelle taille de chaque espace de Galerkin local
        MKL_INT Nres_Pro;// ndof - Nred
       
        double xi;
        Mat3 Ac;
        Onde_plane *OP =  nullptr;
  
        void xyz(MKL_INT i, MKL_INT j, MKL_INT k, MKL_INT Ndiv, double *x, double *y, double *z);
  
        void combinaison_solution(MKL_INT ndof, MKL_INT e, VecN &ALPHA, double *X);

        void compute_volume_tetra();
  
        void compute_volume_tetra(double *x2x1, double *x3x1, double *x4x1, double *prod_Vecto);

        double Check_volume();
  
        double compute_volume_tetra_II(double *x2x1, double *x3x1, double *x4x1);

        double compute_Wave_length();

        void transport_theMatrix_to_Matlab(MKL_INT n, cplx *MP, const char chaine[]);

        void transport_theMatrix_to_Matlab_zero_base(MKL_INT n, cplx *MP, const char chaine[]);

        void transport_Vector_to_Matlab(MKL_INT n, cplx *U, const char chaine[]) ;

        void transport_Vector_to_Matlab(MKL_INT n, double *U, const char chaine[]) ;

        void combinaison_projectionSolution(MKL_INT n, cplx *ALPHA, double *X);

        void combinaison_projectionSolutionPressure(MKL_INT n, cplx *ALPHA, double *X);

        void compute_rad_XG();

        void compute_hmax();

        void compute_hmax(double *x2x1, double *x3x1, double *x4x1, double *x3x2, double *x4x2, double *x4x3);

        void compute_eps_max_For_Eigenvalue_Filtering(double eps, MKL_INT s);

        void compute_eps_of_Tikhonov_regulator(double eps, MKL_INT s);
        
  //~Element();
private:
	MKL_INT ref; // numero de reference de l'element
};



class Bessel {
public :
  Bessel();//contsructeur
  double P;//la pression
  cplx Pc;
  cplx *V;//la vitesse
  double *d;//la direction
  double k;//le nombre d'onde
  double r;// ||X - X0|| avec X0 le point source et X la variable en laquelle la fonction est évaluée
  double kr;//kr = k * r
  MKL_INT compute_P_and_V(Mat3 &mat, double *X, double *X0);//calcule P et V en X - X0
  void compute_P_and_V(double *X, double *X0, cplx alpha1, cplx alpha2, cplx beta2, double r1, double a1, double a2, double k1, double k2, double xi1, double xi2, double rad);
  void print_info();//info sur l'objet
  ~Bessel();//destructeur
};



class MailleSphere {
public :
	MailleSphere();
  // MailleSphere(char chaine_points[], char chaine_tetra[], char chaine_voisins[], char chaine_methode[], char type_espace_phases[], char phase_tag[], char which_h[], double epsilon_0, double epsilon_reg_Tikhonov, MKL_INT filtre);
  MailleSphere(double epsilon_0, double epsilon_reg_Tikhonov, double np, double *points_ext, MKL_INT nt, MKL_INT *tetra_ext, MKL_INT *voisin_ext, Sommet *sommets_ext, MKL_INT n_ddl, double *Dir_ext, MKL_INT n_quad, double *X1d_ext, double *W1d_ext, double *X2d, double *W2d, double *X3d, double *W3d) ;
  void build_elements_and_faces(char chaine_method[], char which_h[], MKL_INT filtre, double *third_coord, MKL_INT *new2old_ext, MKL_INT *old2new_ext, MKL_INT *NewListeVoisin_ext, MKL_INT *newloc2glob_ext, Element *elements_ext, Face *face_double_ext, MKL_INT *Tri_ext, Face *face_ext, MKL_INT *cumul_NRED, double *virtual_points, Sommet *virtual_sommets, Onde_plane *virtual_plane_wave, char type_Sol[]);
        MKL_INT* voisin =  nullptr;
        MKL_INT* tetra =  nullptr;
        double* points =  nullptr;
        double* Dir =  nullptr;
        double *X1d =  nullptr;
        double *W1d =  nullptr;
        double X0[3];
        MKL_INT npS;
        MKL_INT ordre_onde;
        MKL_INT ndof;
        MKL_INT n_i;
        MKL_INT n_gL;
        MKL_INT *SIZE =  nullptr;
        MKL_INT *old2new =  nullptr;//tableau de permutation des anciennes numérotations vers les nouvelles
        MKL_INT *new2old =  nullptr;//.......................... nouvelles ...................... anciennes
        double *Z_XG =  nullptr;
        MKL_INT taille_red;

        double tab_PP[4];
        double tab_VV[4];
        double tab_PV[4];
        double tab_VP[4];
  
        double M_PP[4];
        double M_VV[4];
        double M_PV[4];
        double M_VP[4];
  
        double N_PP[4];
        double N_VV[4];
        double N_PV[4];
        double N_VP[4];

	char forme_Sol[100];

        double tab_inc[4];
        double Y_omega;
	double frq = 0.0;// la frequence


	Onde_plane OP_inc ;

        cplx c1;
        cplx c2;
        cplx c3;
        double r1;
        double r2;
        double xi1;
        double xi2;
        double a1;//element diag de la matrice d'anistropie de la sphere intérieure
        double a2;//.................................................... extérieure
	double b1;//element extra-diag de la matrice d'anistropie de la sphere intérieure
        double b2;//.......................................................... extérieure
        double k1;
        double k2;
        double omega;// omega = 2 * M_PI * frq;
        double Z = 0.0;

        double hmax = 0.0;
        double hmin = 1E10;

        double eps_0;
        double eps_reg_Tikhonov;//epsilon pour réguler les blocs de la métrique associée 
                       //à notre problème aux valeurs propres généralisées
        double lambda_max = 0.0;
        double lambda_min = 0.0;
        double lambda_max_normalisee = 0.0;
        double lambda_min_normalisee = 0.0;
        double Nred_mean;//somme des dimensions réduites / N_elements;
  
	void set_N_sommets(MKL_INT N);
	MKL_INT  get_N_sommets();
	void set_N_elements(MKL_INT N);
	MKL_INT  get_N_elements();
	void print_info();
	void Create_Xsommets(double *points);
	void print_Xsommets();
        void Create_Elem2sommet(MKL_INT *tetra);
	void print_loc2glob();
	void Create_ListeVoisin(MKL_INT *voisin);
	void print_ListeVoisin();
	void build_sommet();
        void build_element(MKL_INT choice);

        void build_coefficients_of_linear_global_system(const char * chaine);
  
  //void build_sorted_element();
	MKL_INT N_Face;
	MKL_INT N_Face_double;
	MKL_INT* Tri =  nullptr;
	void build_face(const char *chaine);
	void Tri_rapid(MKL_INT i1, MKL_INT i2);
	Face* face_double =  nullptr;
	Face* face =  nullptr;
	Sommet* sommets =  nullptr;
	Element* elements =  nullptr;
        Element fixe_element;
        double *noeud_fixe_element =  nullptr;
        Sommet *sommets_fixe_element =  nullptr;

        Quad_2D Qd2D;
        Quad Qd;
  
	void test_build_sommet();
	void test_build_element();
	void test_build_face();
  
        void choisir_A_et_xi(MKL_INT e, double r);
        void choisir_A_et_xi_fixe_element();
  
        void build_fixe_element(char which_h[], MKL_INT choice);
  
        void compute_coeff_hetero_Analytique_Bessel();
       
        void transform_hetero_analytique_Scalar_Solution_to_VTK();
        void transform_hetero_analytique_ProjectionOfScalarSolution_to_VTK(VecN &ALPHA);
        void transform_hetero_analytique_ProjectionOfPressureAndVelocitySolution_to_VTK(VecN &ALPHA);
        void transform_hetero_numerical_PressureAndVelocitySolution_to_VTK(VecN &ALPHA);
        void quad_Gauss_1D();
        void tri_maillage();
        void print_new_mesh(double distance);
  // void print_elem_and_neigh();
        void TEST_copy();
  
        void tools_to_define_sorted_mesh();
  
        void build_NewListeVoisin();

        void build_newloc2glob();

        void transporte_NewListeVoisin(const char chaine[]);

        void transporte_old2new(const char chaine[]);

        void TEST_readable_tools(const char ch_size[], const char ch_elts[], const char ch_neigh[], const char ch_bary_coord[], const char ch_Fsky[]);
  // ~MailleSphere();
	MKL_INT* oldloc2glob =  nullptr;
        MKL_INT* newloc2glob =  nullptr;
	MKL_INT* OldListeVoisin =  nullptr;
        MKL_INT* NewListeVoisin =  nullptr;
        void destroy_Mesh();
private :
        double* Xsommet =  nullptr;
	MKL_INT N_sommets;
	MKL_INT N_elements;
};


class Maillage {
public :
	Maillage();
	MKL_INT* voisin = NULL;
	MKL_INT* tetra = NULL;
	double* points = NULL;
        double* DirSph = NULL;
        double* Dir = NULL;
        MKL_INT n_gL;
        double *X1d;
        double *W1d;
        double sld;
        double slg;
        cplx R;
        cplx T;
        double a1;
        double a2;
        double xi1;
        double xi2;
        double Y1p;//impédence associée à l'onde incidente
        double Y1n;//impédence associée à l'onde réfléchie
        double Y_omega;//impédance de bord
        double x_I;//interface dans le cube
        double Y2p;
        double tab_PP[4];
        double tab_VV[4];
        double tab_PV[4];
        double tab_VP[4];

        double ww;
        double M_PP[4];
        double M_VV[4];
        double M_PV[4];
        double M_VP[4];
  
        double N_PP[4];
        double N_VV[4];
        double N_PV[4];
        double N_VP[4];

        double k1n[3];
        double k1p[3];
        double k2p[3];

        double d1n[3];
        double d1p[3];

        double eps_0;
        MKL_INT *SIZE =  nullptr;
        MKL_INT *SIZE_LOC =  nullptr;
        double *Z_XG =  nullptr;
        MKL_INT *old2new =  nullptr;//tableau de permutation des anciennes numérotations vers les nouvelles
        MKL_INT *new2old =  nullptr;//.......................... nouvelles ...................... anciennes
        MKL_INT taille_red;
  
        double tab_inc[4];
        double X0[3];
        double omega;//la fréquence
        MKL_INT npS;
        MKL_INT ordre_onde;
       
        MKL_INT ndof;
        MKL_INT n_i;//indice de l'onde plane incidente
        MKL_INT n_r;//indice de l'onde réfléchie   
	void set_N_sommets(MKL_INT N);
	MKL_INT  get_N_sommets();
	void set_N_elements(MKL_INT N);
	MKL_INT  get_N_elements();
	void print_info();
	void Create_Xsommets(double *points);
	void print_Xsommets();
        void Create_Elem2sommet(MKL_INT *tetra);
	void print_loc2glob();
	void Create_ListeVoisin(MKL_INT *voisin);
	void print_ListeVoisin();
	void build_sommet();
	void build_element();
	MKL_INT N_Face;
	MKL_INT N_Face_double;
	MKL_INT* Tri =  nullptr;
	void build_face();
	void Tri_rapid(MKL_INT i1, MKL_INT i2);
	Face* face_double =  nullptr;
	Face* face =  nullptr;
	Sommet* sommets =  nullptr;
	Element* elements =  nullptr;
        Quad_2D Qd2D;
        Quad Qd;
	void test_build_sommet();
	void test_build_element();
	void test_build_face();
  
        void Transforme_realPartOfSolution_To_VTK_File(VecN &ALPHA);

        void Transforme_imaginaryPartOfSolution_To_VTK_File(VecN &ALPHA);
  
        void Transforme_Solution_To_VTK_File_OnElement(VecN &ALPHA, MKL_INT e);
  
        void Transforme_Bessel_analytique_Scalar_Solution_To_VTK_File();

        void Transforme_Bessel_analytique_Velocity_Solution_To_VTK_File_Vector();
  
        void Transforme_realPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA);

        void Transforme_imaginaryPartOfSolution_Velocity_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA);
  
        void Transforme_ErrorWithBessel_To_VTK_File(VecN &ALPHA);

        void Transforme_ErrorWithHankel_To_VTK_File(VecN &ALPHA);

        void Transforme_realPartOfSolution_Velocity_To_VTK_File_Vector(VecN &ALPHA);

        void Transforme_imaginaryPartOfSolution_Velocity_To_VTK_File_Vector(VecN &ALPHA);

        void Transforme_BesselError_Vitesse_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA);

        void Transforme_HankelError_Vitesse_Vx_Vy_Vz_abs_To_VTK_File(VecN &ALPHA);

        void Transforme_BesselError_Vitesse_To_VTK_File_Vector(VecN &ALPHA);

        void Transforme_HankelError_Vitesse_To_VTK_File_Vector(VecN &ALPHA);

        void Transforme_realPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File();

        void Transforme_imaginaryPartOfAnalytique_Scalar_Hankel_Solution_To_VTK_File();

        void Transforme_realPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector();

        void Transforme_imaginaryPartOfHankel_analytique_Velocity_Solution_To_VTK_File_Vector();

        void numerical_Solution_To_VTK(VecN ALPHA, const std::string & arg1, const std::string & arg2, char arg3[]);

        void Analytique_Solution_To_VTK(const std::string & arg1, const std::string & arg2, char arg3[]);

        void compute_R_and_T();

        void CHECK_SOLUTION(double *X);

        void Define_index_Of_reflected_wave();
  
        void quad_Gauss_1D();

        void CHECK_PARAMETERS();

        void tools_to_define_sorted_mesh();

        void build_NewListeVoisin();

        void build_newloc2glob();

        void print_new_mesh(double distance);

        void transporte_NewListeVoisin(const char chaine[]);

        void transporte_NewListeVoisin();

        void transporte_old2new(const char chaine[]);

        void TEST_readable_tools(const char ch_size[], const char ch_elts[], const char ch_neigh[], const char ch_bary_coord[], const char ch_Fsky[]);
  
  //void tri_maillage();
  //cplx integrerSurElementAvecGauss(MKL_INT e, MKL_INT iloc, MKL_INT jloc);
  // void destroyMaillage();
        ~Maillage();
private :
	double* Xsommet =  nullptr;
	MKL_INT N_sommets ;
	MKL_INT N_elements ;
	MKL_INT* oldloc2glob =  nullptr;
        MKL_INT* newloc2glob =  nullptr;
	MKL_INT* OldListeVoisin =  nullptr;
        MKL_INT* NewListeVoisin =  nullptr;
};


class test_projection {//cette classe sert à tester la fonction quad_tetra (integrateur sur un tetra
public : 
  double *x1;
  double *x2;
  double *x3;
  double *x4;
  
  test_projection();
  
  void Allocate();
  
  void get_Element(Element &Elem);
  
  double compute_volume_tetra_II(double *x2x1, double *x3x1, double *x4x1);
  
  cplx quad_tetra(Quad &Qd, std::function<cplx (double*)> f, std::function<cplx (double*)> g);
  
  cplx quad_tetra(Quad &Qd, Onde_plane &P1, std::function<cplx (double*)> f);
  
  cplx quad_tetra(Quad &Qd, Onde_plane &P1, Onde_plane &P2);

  ~test_projection();
};



class A_Block
{
public:
	A_Block();
	void set_ij(MKL_INT i, MKL_INT j);
	void set_ndof(MKL_INT n);
	MKL_INT get_i();
	MKL_INT get_j();
        void set_Aloc(cplx* A); // Fixe l'adresse de Aloc
	cplx* get_Aloc(); // Fixe l'adresse de Aloc
	void print_info_A_loc();
	void print_A_loc();
	MKL_INT get_n();
	friend MPI_Datatype create_mpi_myclass_type(MKL_INT, MPI_Datatype);
private:
	MKL_INT n_dof;
	MKL_INT i_block;
	MKL_INT j_block;
        cplx* A_loc =  nullptr;
};


class A_skyline
{
public:
	A_skyline(); 
	cplx* get_Asky();
	MKL_INT get_n_elem();
	void set_n_elem(MKL_INT n);
	MKL_INT get_n_inter();
	void set_n_inter(MKL_INT n);
	MKL_INT get_n_dof();
	void set_n_dof(MKL_INT n);
	MKL_INT get_n_A();
	void set_n_A();
	void print_info();
	void build_mat();
        void build_mat(cplx *A, A_Block *array_A_loc);
  
  void modif_A_sky(Maillage &Mesh, const std::string & arg1, const std::string & arg2, const char chaine[]);
  void modif_A_sky_MN(Maillage &Mesh, const std::string & choice, const char chaine[]);
  void modif_A_sky_MN(MailleSphere &MSph, const std::string & choice, const char chaine[]);
        void Add_Block(MKL_INT i, MKL_INT j, double alpha, cplx *B);
  
        MKL_INT VerifyDecompositionPMP(MKL_INT e, MKL_INT j, cplx *AA, double *w, Maillage &Mesh);
        MKL_INT VerifyDecompositionPMP(MKL_INT e, MKL_INT j, cplx *AA, cplx *w, Maillage &Mesh);
        MKL_INT VerifyDecompositionPMP(MKL_INT e, MKL_INT j, cplx *AA, double *w, MailleSphere &MSph);
        MKL_INT VerifyDecompositionPMP(Maillage &Mesh);
        MKL_INT VerifyDecompositionPMP(A_skyline &P, VecN &LAMBDA, Maillage &Mesh);
        MKL_INT VerifyDecompositionPMP(MailleSphere &MSph);
  
        void Addition(A_skyline &B_glob, Maillage &Mesh);
  
        void Copy(A_skyline &B_glob, Maillage &Mesh);
        void Copy(A_skyline &B_glob, MailleSphere &MSph, const std::string & choice);
  
        void ProdA_skyline_Vector(VecN &U, VecN &V, MailleSphere &MSph);
        void ProdA_skyline_Vector(VecN &U, VecN &V, Maillage &Mesh);
        void ProdA_skyline_Vector_mkl(VecN &U, VecN &V, Maillage &Mesh);
        void ProdA_skyline_Vector_mkl(VecN &U, VecN &V, MailleSphere &MSph);
        void ProdA_skyline_Vector_mkl(cplx *U, cplx *V, MailleSphere &MSph);
        void ProdA_skyline_Vector_mkl(cplx *U, cplx *V, Maillage &Mesh);

  
        cplx compute_XAY(VecN &X, VecN &Y, MailleSphere &MSph);
        cplx compute_XAY(VecN &X, VecN &Y, Maillage &Mesh);

  
        cplx ProdDiagXAX(VecN &U, MailleSphere &MSph);
        void transporte_A__skyline(const char chaine[], Maillage &Mesh);
        void transporte_A_glob(const char chaine[], MailleSphere &MSph);
        void transporte_M_glob(const char chaine[], MailleSphere &MSph);
	A_Block* Tab_A_loc = (A_Block*) nullptr;
       
        void destroyer();
  //~A_skyline();
private:
	MKL_INT n_elem;
	MKL_INT n_inter;
	MKL_INT n_dof;
	MKL_INT n_A;
        cplx* A_sky =  nullptr; // Lieu de stockage de toutes les matrices blocs

  //A_Block* liste_A_loc = (A_Block*)NULL;
};




class A_skylineRed
{
public:
	A_skylineRed(); 
	cplx* get_AskyRed();
	MKL_INT get_n_elem();
	void set_n_elem(MKL_INT n);
	MKL_INT get_n_inter();
	void set_n_inter(MKL_INT n);
	MKL_INT get_n_A();
	void set_n_A(MKL_INT size_A);
	void print_info();
  
	void build_mat(Maillage &Mesh);
        void build_mat(MailleSphere &MSph);
        void build_mat(MailleSphere &MSph, cplx *A_red, A_Block *array_A_loc);
	void build_mat(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *A_red, A_Block *array_A_loc, MailleSphere &MSph);
  
        void CheckMred(MKL_INT e, MKL_INT l, MKL_INT *r1, MKL_INT *r2, Maillage &Mesh);
        void CheckMred(MKL_INT *r1, MKL_INT *r2, Maillage &Mesh);
  
        void Addition(A_skylineRed &B_red, Maillage &Mesh);

        void ProdA_skyline_Vector_mkl(cplx *U, cplx *V, MailleSphere &MSph);
        void ProdA_skyline_Vector_mkl(VecN &U, VecN &V, MailleSphere &MSph);
        void ProdA_skyline_RedVectorGivesFullVector_mkl(cplx *U, cplx *V, MailleSphere &MSph) ;
        void ProdA_skyline_FullVectorGivesFullVector_mkl(cplx *U, cplx *V, MailleSphere &MSph);
        void ProdA_skyline_FullVectorGivesRedVector_mkl(cplx *U, cplx *V, MailleSphere &MSph);

  
        void ProdA_skyline_FullVectorGivesFullVector_mkl(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph) ;

       void ProdA_skyline_RedVectorGivesFullVector_mkl(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);

       void ProdA_skyline_FullVectorGivesRedVector_mkl(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);


        void ProdA_skyline_FullVectorGivesFullVector_mkl_NewVersion(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);

        void ProdA_skyline_RedVectorGivesFullVector_mkl_NewVersion(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);


        void ProdAstar_skyline_RedVector_mkl(cplx *U, cplx *V, MailleSphere &MSph) ;
        void ProdAstar_skyline_RedVectorGivesFullVector_mkl(cplx *U, cplx *V, MailleSphere &MSph);
        void ProdAstar_skyline_FullVectorGivesFullVector_mkl(cplx *U, cplx *V, MailleSphere &MSph);
        void ProdAstar_skyline_FullVectorGivesRedVector_mkl(cplx *U, cplx *V, MailleSphere &MSph);


  
        void ProdAstar_skyline_FullVectorGivesFullVector_mkl(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);
  
        void ProdAstar_skyline_RedVectorGivesFullVector_mkl(MKL_INT s, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);
  

        void ProdAstarA_skyline_RedVector_mkl(MKL_INT n_U, MKL_INT n_V, cplx *U, cplx *V, MailleSphere &MSph);
        void ProdAstarA_skyline_RedVectorGivesFull_mkl(MKL_INT n_U, MKL_INT n_V, cplx *U, cplx *V, MailleSphere &MSph);
        void ProdAstarA_skyline_FullVectorGivesFull_mkl(MKL_INT n_U, MKL_INT n_V, cplx *U, cplx *V, MailleSphere &MSph);
        void ProdAstarA_skyline_FullVectorGivesRed_mkl(MKL_INT n_U, MKL_INT n_V, cplx *U, cplx *V, MailleSphere &MSph);



        void ProdAstarA_skyline_FullVectorGivesFull_mkl(MKL_INT i, MKL_INT n_U, MKL_INT n_V, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph, cplx *W);

         void ProdAstarA_skyline_RedVectorGivesFull_mkl(MKL_INT i, MKL_INT n_U, MKL_INT n_V, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph, cplx *W);


  
        cplx compute_XAY(VecN &X_red, VecN &Y_red, MailleSphere &MSph);
        cplx compute_XAY(VecN &X_red, VecN &Y_red, Maillage &Mesh);
        cplx ProdDiagXAX(VecN &U_red, MailleSphere &MSph);




        void ProdA_skyline_Vector_mkl(cplx *U, cplx *V, MailleSphere &MSph, const char chaine1[], const char chaine2[], double facteur, double *temps);

        void ProdA_skyline_FullVectorGivesFullVector_mkl_NewVersion(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph, const char chaine1[], const char chaine2[], double facteur, double *temps);

        void ProdA_skyline_FullFull_TEST(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);
        void ProdA_skyline_RedFull_TEST(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);
        void transporte_A_red(const char chaine[], MailleSphere &MSph);
	A_Block* Tab_A_locRed = (A_Block*) nullptr;
	cplx* A_skyRed =  nullptr; // Lieu de stockage de toutes les matrices blocs
  //~A_skylineRed();
private:
	MKL_INT n_elem;
	MKL_INT n_inter;
	MKL_INT n_A;
};


void tri_recursive(MKL_INT debut, MKL_INT fin, MKL_INT *newIndices, double *Z_XG);


void tri_elements(MKL_INT N_elements, MKL_INT *newIndices, double *Z_XG);


double compute_XG(Element &E1, Element &E2);

//compare a et b
MKL_INT compare_function(const void *a,const void *b);

//tri recursive par Etienne Poulin – ISN – Novembre 2013
void tri_recursive(MKL_INT *tab, MKL_INT debut, MKL_INT fin);

//Writing coordinates of nodes on a file
void Export_Points(MKL_INT np, double *points);

void Export_PointsSphere(MKL_INT np, double *points);

//Writing tetra on a file
void Export_Tetra(MKL_INT ne, MKL_INT *tetra);

//Writing the neighbours of tetra
void Export_Voisin_Tetra(MKL_INT ne, MKL_INT *voisin);

//Export loc2glob
void Export_Loc2glob(MKL_INT ne);

//Read nodes
double* Read_Points(MKL_INT *np);

double* Read_Points2(MKL_INT *np);

double* Read_PointsSphere(MKL_INT *npS);

//Read tetra
MKL_INT* Read_Tetra(MKL_INT *ne);

MKL_INT* Read_Tetra2(MKL_INT *ne);

//Read neighbours tetra
MKL_INT* Read_Neighbours_Tetra();

MKL_INT* Read_Neighbours_Tetra2();

//Read loc2glob
MKL_INT* Read_Loc2glob(MKL_INT *taille);

MKL_INT ordre(double* X, double* Y, MKL_INT taille);


void choisir_A_et_xi(Element &Elem, double sld, double slg, MKL_INT choice);


// Definition de la classe Vec3
class Vec3 {
public :
	Vec3();
	double value[3];
	void define(double a, double b, double c);
	void print_info();
	void compute_norm();
	void compute_dir();
private:
	double dir[3];
	double norm;
};

Vec3 operator+(Vec3 &vec1, Vec3 &vec2);
Vec3 operator-(Vec3 &vec1, Vec3 &vec2);
double operator*(Vec3 &vec1, Vec3 &vec2);
Vec3 operator^(Vec3 &vec1, Vec3 &vec2);
Vec3 operator*(double lambda, Vec3 &vec2);

//produit scalaire entre le vecteur d'onde veck et le noeud R
double operator*(Vec3 &veck, Sommet &R);


void Compute_EigenValue(double Ac[], double Bc[], double *w);


void computeMatandMatTransp(double Ac[], double AcAC[]);


void VerifyDecomposition(double Ac[], double D[], double AcDAc[]);


void prodMatMatDiag_positive(double Ac[], double D[], double AcD[]);


void prodMatMatDiag_negative(double Ac[], double D[], double AcD[]);


void verifyA_SQRT_A(double Ac[], double ASA[]);


void verifyA_OneoverSQRT_A(double Ac[], double AOSA[]);

void verifyA_OneoverSQRT_A(double T[], double P[], double w[], double AOSA[]) ;

Mat3 operator+(Mat3 &mat1, Mat3 &mat2);   // Somme de 2 matrices
Mat3 operator*(double alpha, Mat3 &mat); // Multiplication scalaire Matrice 3 x 3
Vec3 operator*(Mat3 &mat, Vec3 &U);       // Multiplication Matrice vecteur 3 x 3
double quad(Vec3 &V, Mat3 &mat, Vec3 &U); // Calcul de V'AV


void directionInCube(double *Tab, MKL_INT ordre);


void directionInCube_complete_version(double *Tab, MKL_INT ordre);


MKL_INT Defined_dimension_of_Trefftz_basis(MKL_INT ordre);

class Direction {
public :
  Direction();
  virtual void Build_Direction();
  virtual void set_ordre(MKL_INT n);
  virtual void set_Kw(double h);
  virtual MKL_INT get_ordre();
  virtual double get_Kw();
  virtual void print_info();
  MKL_INT ordre;//pour définir ndof
  MKL_INT ndof;//nombre d'ondes planes par élément
  Vec3* DD;//tableau pour stocker les informations sur les ondes planes
  double Kw;// norme d'une onde plane
  virtual ~Direction();
};



cplx eval_exp(double x);


double * produit_vectoriel(double *U, double *V);

void produit_vectoriel(double *U, double *V, double *W);

cplx integrale_triangle(Face &F, Onde_plane &P1, Onde_plane &P2);


class Sparse {
public :
  Sparse();
  //void Allocate_Sparse();
  std::complex<double>* a =  nullptr;//tableau de valeurs de la matrice sparse
  MKL_INT * ia =  nullptr;//tableau contenant la somme des coeffs non nuls de chaque ligne
  MKL_INT * ja =  nullptr;//tableau d'indices de colonnes de la matrice sparse
  MKL_INT m;//nombre de lignes
  MKL_INT n;//nombre de colonnes
  MKL_INT nnz;//nombre de coeffs non nuls
  
  void print_info();
  
  //transformation de la matrice bloc A_glob en matrice sparse (elle est stockée dans a, ia et ja)
  void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, Maillage &Mesh);

  void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MailleSphere &MSph);

  void transforme_A_skyline_to_Sparse_CSR(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);

  void transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, Maillage &Mesh);

  void transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, MailleSphere &MSph);

  
  void transforme_A_skyline_to_Sparse_CSR_version_reduite(A__skyline &Ared, cplx *Ared_mem, MailleSphere &MSph);

  void compute_solution(VecN &b, VecN &x);//inverser la matrice stockée dans value à l'aide MKL pardiso

  void compute_solution(cplx *b, cplx *x);//inverser la matrice stockée dans value à l'aide MKL pardiso
  
  //écriture de a et ja dans un fichier
  void transporte_Value_and_Columns_To_matlab(const char chaine[]);
  
  //écriture de ia dans un fichier
  void transporte_rows_To_matlab(const char chaine[]);

  //destructeur
  ~Sparse();
};

void read_Value_and_Columns_from_file(MKL_INT nnz, MKL_INT *ja, cplx *a, const char chaine[]);


void read_rows_from_file(MKL_INT n, MKL_INT *ia, const char chaine[]);

class MatMN {
public :
  MatMN();
  void Allocate_MatMN();
  std::complex<double>* value =  nullptr;
  std::complex<double>* result =  nullptr;
  cplx *gamma =  nullptr;
  MKL_INT m;//nombre de lignes
  MKL_INT n;//nombre de colonnes
  void print_info();

  void Solve_by_LU(VecN &B);//calcule la solution du systeme linéaire AX = B avec A la matrice de type MatMN

  void Solve_by_LU_C(VecN &B, VecN &U);

  void Solve_by_CHOLESKY(VecN &B);

  void Solve_by_CHOLESKY_ColMajor(VecN &B);

  void Solve_by_CHOLESKY(cplx *B);
  
  //transformation de la matrice bloc A_glob en matrice full ( elle est stockée dans value)
  void transforme_Afull(A_skyline &A_glob, Maillage &Mesh);

  void transforme_Afull(A_skylineRed &A_red, Maillage &Mesh);

  void transforme_Afull(A_skylineRed &A_red, MailleSphere &MSph);

  void transforme_Afull(A_skyline &A_glob, MailleSphere &MSph) ;

  void transforme_P_red_To_full(A_skyline &P_red, Maillage &Mesh);
  
  //transformation de la matrice bloc A_glob en matrice sparse (elle est stockée dans a, ia et ja)
  // void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, Maillage &Mesh);

  ///transformation de la matrice full en matrice sparse
  void transforme_Full_To_Sparse(Maillage &Mesh, Sparse &AS);

  void transforme_Full_To_Sparse(MailleSphere &MSph, Sparse &AS);

  //transformation de la matrice sparse en matrice full
  void transforme_sparse_To_full(Maillage &Mesh, Sparse &AS);

   void transforme_sparse_To_full(MailleSphere &MSph, Sparse &AS);

  //produit Matrice vecteur
  void produit_MatVec_Full_Version(VecN &b, VecN &res);

  void produit_MatVec_Full_Version(cplx *b, cplx *res);

  //écriture de la matrice full dans un fichier
  void transporte_To_matlab(const char chaine[]);

  void compute_eigenValueAndeigenVector();

  void CHECK_RESIDUAL(MKL_INT e, VecN &b, VecN &u);

  void CHECK_RESIDUAL(MKL_INT e, double *maxx, double *minn, VecN &b, VecN &u);

  void CHECK_RESIDUAL(MKL_INT e, double *maxx, double *minn, cplx *b, cplx *u);

  void CHECK_COLUMN_WITH_RHS_VECTOR(VecN &b, MKL_INT inc);

  void CHECK_Diagonal(MKL_INT e, Maillage &Mesh);

  MKL_INT VerifyDecompositionPMP(MKL_INT e, cplx *AA, double *w);

  MKL_INT VerifyDecompositionPMP(MKL_INT e, cplx *AA, cplx *w);

  MKL_INT TEST_Valeurs_propres_et_Vecteurs(MKL_INT e, Maillage &Mesh);


  //destructeur
  ~MatMN();
};


void AdditionMatMN(MatMN &mat1, MatMN &mat2);

void SoustractionMatMN(MatMN &mat1, MatMN &mat2);

void ScalaireMatMN(double alpha, MatMN &mat);


void get_normalToface(Face &F, double *n0);


void get_exteriorNormalToElem(Element &Elem);


void get_exteriorNormalToElem(Element &Elem, double *X2_X1, double *X4_X2, double *X1_X3, double *X3_X4);


void AdditionVecN(VecN &v1, VecN &v2, VecN &v3);


void SoustractionVecN(VecN &v1, VecN &v2, VecN &v3);


void ScalaireVecN(double alpha, VecN &v1, VecN &v2);


void ScalaireVecN(std::complex<double> alpha, VecN &v1, VecN &v2);


cplx integrale_triangle_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


void Build_Matdiag_XX(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);



cplx integrale_triangle_PP_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);



cplx integrale_triangle_PV_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);



cplx integrale_triangle_VP_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);



cplx integrale_triangle_VV_Neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);



void reciprocity_principle(Maillage &Mesh, MKL_INT e);


void compute_sizeBlocRed(MKL_INT e, MKL_INT l, cplx *AA, double *w, A_skyline &A_glob, Maillage &Mesh);


void DecompositionDeBloc(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, Maillage &Mesh);


void DecompositionGlobaleDesBlocs(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, Maillage &Mesh);


MKL_INT compute_sizeBlocRed(A_skyline &A_glob, Maillage &Mesh);


MKL_INT compute_sizeForA_red(Maillage &Mesh);


void positivite_PP(Maillage &Mesh, MKL_INT e);


void positivite_VV(Maillage &Mesh, MKL_INT e);


cplx integrale_triangle_FxUy_interieure_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PP_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PV_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VP_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VV_neighbour(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_PP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_PV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_VP(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_VV(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


void Build_Matdiag_FxUy(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_Matvoisin_FxUy(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);



MKL_INT local_to_glob(MKL_INT iloc, MKL_INT tetra, MKL_INT ndof);


MKL_INT local_to_globRed(MKL_INT iloc, MKL_INT tetra, Maillage &Mesh);


void global_to_local(MKL_INT ndof, MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc);


void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, Maillage &Mesh);


void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, MailleSphere &MSph);


void Somme_Matdiag_XX_FxUy(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Somme_Matvoisin_XX_FxUy(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);



double Aire_face(Face &F);


//double Somme_Aire_Faces(Element &Elem);


void Solution_OndePlane(VecN &U, Maillage &Mesh);


/*--------------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------- Consistance de la formulation version Cessenat et Desprès -----------------------------*/

/*---------------------------------------------------------------------------------------------------------------------------*/



void Sum_Matdiag(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Sum_Matvoisin(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*------------------------------------------------------------------------------------------------------------------------*/

void getNumberOfNeighbours(Maillage &Mesh, MKL_INT *NVS);


//produit d'une matrice de type MatMN par un vecteur de type VecN
//resultat stocké dans res, object de type VecN
void produit_MatSparse_vecteur(Sparse &AS, VecN &vec, VecN &res);

void produit_MatSparse_vecteur(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *vec, cplx *res) ;

void produit_MatSparse_vecteur_parallel_version(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *vec, cplx *res);

//produit matrice vecteur
void produit_MatVec(MatMN &mat, VecN &vec, VecN &res);


/*------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------- Second membre pour une onde incidente à direction radiale --------------------------------------*/

/*-------------------------------------------------------------------------------------------------------*/


//vecteur second membre global associé à Pinc * conj(P') + Pinc * conj(V') + Vinc * conj(P') + Vinc * conj(V') pour une onde incidente à direction radiale
void Build_vector_radiale(VecN &B, Maillage &Mesh, Hankel &he);


void Build_vector_radiale(VecN &B, Maillage &Mesh, Bessel &j0);


//integrale de Pinc * conj(P') sur un element avec comme onde incidente une onde plane à direction non radiale
cplx integrale_PincP_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//integrale de Pinc * conj(V') sur un element avec comme onde incidente une onde plane à direction non radiale
cplx integrale_PincV_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);



//integrale de Vinc * conj(P') sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincP_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//integrale de Vinc * conj(V') sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincV_Avec_directionNonRadiale(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//vecteur global associé à Pinc * conj(P') pour une onde incidente plane de direction non radiale
void Build_vector_PincP_Avec_directionNonRadiale(VecN &B, Maillage &Mesh, const char chaine[]);


//vecteur global associé à Pinc * conj(V') pour une onde incidente plane de direction non radiale
void Build_vector_PincV_Avec_directionNonRadiale(VecN &B, Maillage &Mesh, const char chaine[]);


//vecteur global associé à Vinc * conj(P') pour une onde incidente plane
void Build_vector_VincP_Avec_directionNonRadiale(VecN &B, Maillage &Mesh, const char chaine[]);


//vecteur global associé à Vinc * conj(V') pour une onde incidente plane
void Build_vector_VincV_Avec_directionNonRadiale(VecN &B, Maillage &Mesh, const char chaine[]);


//vecteur second membre global associé à Pinc * conj(P') + Pinc * conj(V') + Vinc * conj(P') + Vinc * conj(V') pour une onde plane incidente à direction non radiale
void Build_vector_Avec_directionNonRadiale(VecN &B, Maillage &Mesh, const char chaine[]);






/*----------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------- section pour systeme d'ordre 1 ---------------------------*/

/*---------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------- pour les termes interieures ---------------------*/
//integrale de Pj conj(Pi') sur les faces interieures de l'élément e
cplx integ_PP_luimeme_interieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


// integrale de P^c_j conj(P^e_i') sur les faces interieures de l'élément e ; interaction de l'élément avec son voisin
cplx integ_PP_interaction_interieure(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


// integrale de Pj * conj(Vi'. n) sur les faces interieures de l'élément e
cplx integ_PV_luimeme_interieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);



// integrale de P^c_j conj(V^e_i') sur les faces interieures de l'élément e ; interaction de l'élément avec son voisin
cplx integ_PV_interaction_interieure(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);



// integrale de Vj * conj(Pi'. n) sur les faces interieures de l'élément e
cplx integ_VP_luimeme_interieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


// integrale de V^c_j conj(P^e_i') sur les faces interieures de l'élément e ; interaction de l'élément avec son voisin
cplx integ_VP_interaction_interieure(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);


// integrale de Vj * conj(Vi'. n) sur les faces interieures de l'élément e
cplx integ_VV_luimeme_interieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


// integrale de V^c_j conj(V^e_i') sur les faces interieures de l'élément e ; interaction de l'élément avec son voisin
cplx integ_VV_interaction_interieure(Maillage &Mesh, MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, const char chaine[]);

/*----------------------------------------------------- pour les termes exterieures ------------------------*/
// integrale de Pj conj(Pi') sur les faces exterieures de l'élément e
cplx integ_PP_luimeme_exterieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


//integrale de Pj conj(Vi') sur les faces exterieures de l'élément e
cplx integ_PV_luimeme_exterieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


//integrale de Vj conj(Pi') sur les faces exterieures de l'élément e
cplx integ_VP_luimeme_exterieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);


//integrale de Vj conj(Vi') sur les faces exterieures de l'élément e
cplx integ_VV_luimeme_exterieure(Maillage &Mesh, MKL_INT e, MKL_INT i, MKL_INT j, const char chaine[]);

/*--------------------------------------------------------- second membre --------------------------------*/

//integrale Pinc * P sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_PincP_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//integrale de Pinc * V sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_PincV_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//integrale de Vinc * P sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincP_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


//integrale de Vinc * Viloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincV_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]);


/*------------------------------------------------------ Matrice PP -----------------------------------------------------*/

//Matrice P * conj(P') interaction de chaque élément avec lui meme; termes interieures
void Const_Matrice_PP_luimeme_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice P * conj(P') interaction de chaque élément avec son voisin
void Const_Matrice_PP_interaction_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice P * conj(P') interaction de chaque élément avec lui meme; termes exterieures
void Const_Matrice_PP_exterieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*------------------------------------------------------ Matrice PV -----------------------------------------------------*/

//Matrice P * conj(V') interaction de chaque élément avec lui meme; termes interieures
void Const_Matrice_PV_luimeme_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice P * conj(V') interaction de chaque élément avec son voisin
void Const_Matrice_PV_interaction_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice P * conj(V') interaction de chaque élément avec lui meme; termes exterieures
void Const_Matrice_PV_exterieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*------------------------------------------------------ Matrice VP -----------------------------------------------------*/

//Matrice V * conj(P') interaction de chaque élément avec lui meme; termes interieures
void Const_Matrice_VP_luimeme_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice V * conj(P') interaction de chaque élément avec son voisin
void Const_Matrice_VP_interaction_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice V * conj(P') interaction de chaque élément avec lui meme; termes exterieures
void Const_Matrice_VP_exterieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*------------------------------------------------------ Matrice VV -----------------------------------------------------*/

//Matrice V * conj(V') interaction de chaque élément avec lui meme; termes interieures
void Const_Matrice_VV_luimeme_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice V * conj(V') interaction de chaque élément avec son voisin
void Const_Matrice_VV_interaction_interieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


//Matrice V * conj(V') interaction de chaque élément avec lui meme; termes exterieures
void Const_Matrice_VV_exterieure(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*----------------------------------------------------- Vecteur second membre -------------------------------------------*/

//Pinc * conj(P')
void Const_vector_SourceOndePlane_PincP(VecN &B, Maillage &Mesh, const char chaine[]);


//Pinc * conj(V')
void Const_vector_SourceOndePlane_PincV(VecN &B, Maillage &Mesh, const char chaine[]);


//Vinc * conj(P')
void Const_vector_SourceOndePlane_VincP(VecN &B, Maillage &Mesh, const char chaine[]);


//Vinc * conj(V')
void Const_vector_SourceOndePlane_VincV(VecN &B, Maillage &Mesh, const char chaine[]);


//Pinc * con(P') + Pinc * conj(V') + Vinc * conj(P') + Vinc * conj(V')
void Const_vector_SourceOndePlane(VecN &B, Maillage &Mesh, const char chaine[]);



cplx compute_exp(double KX);


cplx eval_f(double *x);


cplx eval_g(double *x);


cplx fmnp(double *x);

cplx result_fmnp(MKL_INT m, MKL_INT n, MKL_INT p);


cplx integrale_triangle_3D_surCube(Onde_plane &P1, Onde_plane &P2);

cplx integrale_triangle_3D_surCube(Onde_plane &P1, MKL_INT a);

cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2);

cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, std::function<cplx (double*)> f);

cplx quad_tetra(Element &Elem, Quad &Qd, std::function<cplx (double*)> f, std::function<cplx (double*)> g);


cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Bessel &j0_e, double *X0);


cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Hankel &he, double *X0);


cplx quad_tetra(Maillage &Mesh, Onde_plane &P1, Onde_plane &P2);


cplx quad_tetra(Maillage &Mesh, Onde_plane &P1, std::function<cplx (double*)> f);


cplx quad_tetra(Maillage &Mesh, std::function<cplx (double*)> f, std::function<cplx (double*)> g);


cplx quad_tetra_Bessel(MKL_INT e, MKL_INT s, MailleSphere &MSph);


void BuildMat_projection(Maillage &Mesh, MKL_INT e, cplx *MP);


//Dans Source_A_Block
void BuildMat_projection(MailleSphere &MSph, MKL_INT e, cplx *MP);

  
void BuildVector_projection(Maillage &Mesh, MKL_INT e, cplx *F, const std::string & arg);


//Dans Source_A_Block
void BuildVector_projection(MKL_INT e, cplx *F, const std::string & arg, MailleSphere &MSph);


void collection_Solutions_locales(VecN &U, const std::string & arg, Maillage &Mesh);



void print_rows_matrix(cplx *M, MKL_INT n, MKL_INT m, MKL_INT l, const std::string & arg);


/***************************************************** Second membre avec integrale de Gauss-Legendre 2D ***********************************************/
double compute_Aire_triangle(Face &F);


double compute_Aire_triangle(double *X1X3, double *X2X3);


cplx integrale_triangle(Face &F, Quad_2D &Qd2D, Onde_plane &P1, Onde_plane &P2);


cplx quad_analytique_On_1_triangle(Face &F, Onde_plane &P1);


MKL_INT compare(Onde_plane &P1, Onde_plane &P2);


cplx integrale_triangle(Face &F, Quad_2D &Qd2D, Onde_plane &P1, std::function<cplx (double*)> f);


cplx integrale_triangle(Face &F, Quad_2D &Qd2D, std::function<cplx (double*)> f, std::function<cplx (double*)> g);


cplx integrale_triangle_test(Quad_2D &Qd2D);


cplx integrale_triangle(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D);



cplx integrale_triangle(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D);


cplx integrale_triangle_VincP(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D);


cplx integrale_triangle_VincP(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D);


cplx integrale_triangle_PincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D);


cplx integrale_triangle_PincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D);


cplx dot_c(cplx *u, double *v);


cplx dot_c(cplx *u, cplx *v);


cplx dot_u(cplx *u, double *v);


cplx dot_u(cplx *u, cplx *v);



cplx integrale_triangle_VincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Hankel &he, Quad_2D &Qd2D);


cplx integrale_triangle_VincV(Element Elem, MKL_INT f, double *X0, Onde_plane &P1, Bessel &j0, Quad_2D &Qd2D);

cplx Sum_quad_triangle_PincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he);


cplx Sum_quad_triangle_PincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0);


cplx Sum_quad_triangle_PincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he);


cplx Sum_quad_triangle_PincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0);


cplx Sum_quad_triangle_VincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he);


cplx Sum_quad_triangle_VincP(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0);


cplx Sum_quad_triangle_VincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Hankel &he);


cplx Sum_quad_triangle_VincV(Maillage &Mesh, MKL_INT e, Onde_plane &P1, Bessel &j0);


MKL_INT determine_position(double *x, double L);


MKL_INT determine_position(Face &F, double L);


/*------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------- TESTS RELATIFS A LA GRANDE MATRICE ------------------------------*/
/*----------------------------------------------------------------- [P] [P'] ------------------------------------------------------*/

cplx int_P_TxP_T_interieure_On_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, Maillage &Mesh, const char chaine[]);

cplx int_P_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_P_KxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);


cplx int_P_TxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);


cplx int_P_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);


void Const_Matrice_Saut_PP_self(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Const_Matrice_Saut_PP_interaction(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);



void BlocIntPP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntPP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxP_K(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);


cplx int_P_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]);

void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;

void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) ;


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Build_BlocExt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);

/*--------------------------------------------------------------- P P' ------------------------------------------------------*/

cplx int_P_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, Maillage &Mesh, const char chaine[]);


cplx int_P_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);



void Const_Matrice_PP_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_P_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);
 
void BlocExtPP_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

void BlocExtPP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);
/*----------------------------------------------------------------- [V] [V'] ------------------------------------------------------*/

cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]);

cplx int_V_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);


cplx int_V_TxV_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh);


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;

void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph);

cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]);

cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph);

void BlocIntVV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntVV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

/*-------------------------------------------------------------------- V V'----------------------------------------------------------*/

cplx int_V_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);



void Const_Matrice_VV_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_V_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) ;
 
void BlocExtVV_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;

void BlocExtVV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);
/*--------------------------------------------------------------------- [V] {P} -----------------------------------------------------*/

cplx int_V_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);

void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);


void BlocIntVP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntVP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

/*--------------------------------------------------------------------- V P ----------------------------------------------------------*/

cplx int_V_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Const_Matrice_VP_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;

void Build_BlocExt_V_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);
 

void BlocExtVP_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtVP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);
/*--------------------------------------------------------------------- [P] {V} -----------------------------------------------------*/

cplx int_P_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]);

void BlocIntPV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntPV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

/*-------------------------------------------------------------------- P V --------------------------------------*/

cplx int_P_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Const_Matrice_PV_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_P_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]);

void BlocExtPV_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtPV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


/*-------------------------------------------------------------------- Pinc P ----------------------------------*/

cplx int_PincP_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_PincP_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Build_VectorPincP_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]);


void Build_VectorPincP_T(cplx *B, Maillage &Mesh, const char chaine[]);


/*----------------------------------------------------------------------- Pinc V ---------------------------------------------------*/

cplx int_PincV_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) ;


cplx int_PincV_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Build_VectorPincV_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]);


void Build_VectorPincV_T(cplx *B, Maillage &Mesh, const char chaine[]);


/*---------------------------------------------------------------------- Vinc P ----------------------------------------------------*/

cplx int_VincP_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_VincP_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Build_VectorVincP_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]);


void Build_VectorVincP_T(cplx *B, Maillage &Mesh, const char chaine[]);



/*---------------------------------------------------------------- Vinc V ---------------------------------------------------------*/

cplx int_VincV_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


cplx int_VincV_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]);


void Build_VectorVincV_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]);


void Build_VectorVincV_T(cplx *B, Maillage &Mesh, const char chaine[]);


/*---------------------------------------------------------------------- [V] {P} + [P] {V} --------------------------------------------------*/

void BlocIntPV_VP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtPV_VP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_VectorSum_F_inc(cplx *B, Maillage &Mesh, const char chaine[]);


void BuildMatBlocByBloc(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BuildMatBlocByBloc_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BuildMatBlocByBloc_N(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void produit_A_skyline_P_skyline(A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, Maillage &Mesh);


void produit_matrice_M_skyline_P_skyline(A_skyline &P_red, A_skylineRed &A_red, VecN &LAMBDA, Maillage &Mesh);


void produit_M_skyline_P_skyline(A_skyline &M_glob, A_skyline &P_red, A_skylineRed &M_red, Maillage &Mesh);


void prodMatMatComplex(MKL_INT m_a, MKL_INT n_a, MKL_INT m_b, MKL_INT n_b, cplx *a, cplx *b, cplx *c);


void prodMatMatComplexNoInit(MKL_INT m_a, MKL_INT n_a, MKL_INT m_b, MKL_INT n_b, cplx *a, cplx *b, cplx *c);


void prodMatVecComplex(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c);


void produit_P_skyline_RightHandSide(A_skyline &P_red, VecN &B_red, VecN &B, Maillage &Mesh);


void produit_P_skyline_Solution_red(A_skyline &P_red, VecN &X_red, VecN &X, Maillage &Mesh);



void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A_skyline &P_red, Maillage &Mesh);


void store_P_and_transpose_P(MKL_INT e, cplx *a, A_skyline &P_red,  const std::string & arg, Maillage &Mesh);


void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, cplx *b, A_skyline &P_red, Maillage &Mesh);


void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, A_skyline &P_red, const std::string & arg, Maillage &Mesh);



double normeUWVF(A_skyline &M_glob, VecN &X, Maillage &Mesh) ;


void produit_P_skyline_SolAnaly(A_skyline &P_red, VecN &Y_red, VecN &Y, Maillage &Mesh);



void min_and_max_eigenvalues(VecN &LAMBDA, double *Wmin, double *Wmax, Maillage &Mesh);



void Allocate_VecNRed(VecN &v, Maillage &Mesh);


MKL_INT CheckEigen(MKL_INT e1, MKL_INT e2, VecN &LAMBDA, Maillage &Mesh);


void CheckEigen(VecN &LAMBDA, Maillage &Mesh);


void get_F_elemRed(MKL_INT e, cplx **B, VecN &B_red, Maillage &Mesh);


MKL_INT CheckMredAndLAMBDA(MKL_INT e, MKL_INT l, A_skylineRed &A_red, VecN &LAMBDA, Maillage &Mesh);


MKL_INT CheckMredAndLAMBDA(A_skylineRed &A_red, VecN &LAMBDA, Maillage &Mesh);



double normeDG(A_skyline &A_glob, VecN &X, Maillage &Mesh);


double norme_L2(MKL_INT e, VecN &X, Maillage &Mesh, const char chaine[]);


double norme_L2(VecN &X, Maillage &Mesh, const char chaine[]);


double norme_L2_Vitesse(MKL_INT e, VecN &X, Maillage &Mesh, const char chaine[]);


double norme_L2_Vitesse(VecN &X, Maillage &Mesh, const char chaine[]);


void min_and_max_eigenvalues_ESP(VecN &LAMBDA, Maillage &Mesh);


void DecompositionDeBloc(MKL_INT e, cplx *A, cplx *P_red, cplx *V, Maillage &Mesh);


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, cplx *P_red,  Maillage &Mesh);


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *P_red,  const std::string & arg, Maillage &Mesh);


void compute_sizeBlocRed(MKL_INT e, cplx* AA, double *w, cplx *A, Maillage &Mesh);


void compute_A_red(MKL_INT e, const std::string & choice, MatMN &A, cplx *P_red, MatMN &A_red, Maillage &Mesh);


void compute_F_red(MKL_INT e, const std::string & choice, cplx *b, cplx *P_red, cplx *b_red, Maillage &Mesh);


void reverseTo_originalVector(MKL_INT e, const std::string & choice, cplx *x, cplx *P_red, cplx *x_red, Maillage &Mesh);


double compute_rad_X(double *X, double *X0);


cplx integrale_triangle_PP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_PV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_VP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_VV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]) ;


cplx integrale_triangle_FxUy_interieure_VP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]) ;


cplx integrale_triangle_FxUy_interieure_VV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]) ;


cplx integrale_triangle_FxUy_interieure_PP_neighbour(MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_PV_neighbour(MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VP_neighbour(MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_interieure_VV_neighbour(MKL_INT e, MKL_INT c, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_PP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_PV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_VP(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


cplx integrale_triangle_FxUy_boundary_VV(MKL_INT e, MKL_INT i, MKL_INT j, MailleSphere &MSph, const char chaine[]);


void BuildDiagonaleMatriceOfCessenatDespresElement(MKL_INT e, cplx *MP, MailleSphere &MSph, const char chaine[]);


void compute_sizeBlocRed(MKL_INT e, cplx* AA, double *w, cplx *A, MailleSphere &MSph);


void DecompositionDeBloc(MKL_INT e, cplx *A, cplx *P_red, cplx *V, MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, cplx *P_red,  MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *P_red,  const std::string & arg, MailleSphere &MSph);


void compute_A_red(MKL_INT e, const std::string & choice, MatMN &A, cplx *P_red, MatMN &A_red, MailleSphere &MSph);


void compute_F_red(MKL_INT e, const std::string & choice, cplx *b, cplx *P_red, cplx *b_red, MailleSphere &MSph);


void reverseTo_originalVector(MKL_INT e, const std::string & choice, cplx *x, cplx *P_red, cplx *x_red, MailleSphere &MSph);


void collection_Solutions_locales_From_red_To_FullSize(VecN &U, const std::string & arg, const std::string & choice, A_skyline &P_red, A_skyline &M_glob, MailleSphere &MSph);


void collection_Solutions_locales_From_red_To_FullSize(VecN &U, const std::string & arg, const std::string & choice, A_skyline &P_red, MailleSphere &MSph);


cplx integrale_PincP_Avec_directionNonRadiale(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_PincV_Avec_directionNonRadiale(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_VincP_Avec_directionNonRadiale(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_VincV_Avec_directionNonRadiale(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


void Build_vector_Avec_directionNonRadiale(VecN &B, MailleSphere &MSph, const char chaine[]);


void Solution_OndePlane(VecN &U, MailleSphere &MSph);


cplx integrale_triangle(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph);

cplx integrale_triangle(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0);

cplx integrale_triangle_PincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph);

cplx integrale_triangle_PincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0);

cplx integrale_triangle_VincP(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph);

cplx integrale_triangle_VincP(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) ;

cplx integrale_triangle_VincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph);

cplx integrale_triangle_VincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP);


cplx Sum_quad_triangle_PincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph);

cplx Sum_quad_triangle_PincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0) ;

cplx Sum_quad_triangle_PincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph);

cplx Sum_quad_triangle_PincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *XX0) ;

cplx Sum_quad_triangle_PincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0);

cplx Sum_quad_triangle_VincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph);

cplx Sum_quad_triangle_VincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP);

cplx Sum_quad_triangle_VincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph);


cplx Sum_quad_triangle_VincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) ;

void Build_vector_radiale(VecN &B, MailleSphere &MSph);



void compute_sizeBlocRed(MKL_INT e, MKL_INT l, cplx *AA, double *w, A_skyline &A_glob, MailleSphere &MSph);


MKL_INT compute_sizeBlocRed(A_skyline &A_glob, MailleSphere &MSph);


MKL_INT compute_sizeBlocRed_parallel_version(A_skyline &A_glob, MailleSphere &MSph);


MKL_INT compute_sizeForA_red(MailleSphere &MSph);


void compute_Inverse(MKL_INT n, cplx *M);



void DecompositionDeBloc(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w);

//void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT taille, cplx *A_loc, cplx *P_loc, cplx *V, MailleSphere &MSph, cplx *AA, double *w) ;

void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT ndof, MKL_INT Nred, MKL_INT Nres, MKL_INT taille, cplx *A_loc, cplx *P_loc, cplx *V, cplx *AA, double *w);

void DecompositionGlobaleDesBlocs_ET_Normalisation_parallel_version(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void get_F_elemRed(MKL_INT e, cplx **B, VecN &B_red, MailleSphere &MSph);


void DecompositionGlobaleDesBlocs(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void DecompositionGlobaleDesBlocs_ET_Normalisation(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);
  

void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A_skyline &P_red,  MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT e, cplx *a, A_skyline &P_red,  const std::string & arg, MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, cplx *b, A_skyline &P_red, MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, A_skyline &P_red, const std::string & arg, MailleSphere &MSph);



double normeUWVF(MKL_INT i, A_skylineRed &M_red, cplx *X, MailleSphere &MSph);


void produit_A_skyline_P_skyline(A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, MailleSphere &Msph);


void produit_A_skyline_P_skyline_new_version(A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, MailleSphere &Msph);



void produit_P_skyline_RightHandSide(A_skyline &P_red, VecN &B_red, VecN &B, MailleSphere &MSph);


void produit_P_skyline_RightHandSide_new_version(A_skyline &P_red, VecN &B_red, VecN &B, MailleSphere &MSph);

void produit_P_skyline_Solution_red(A_skyline &P_red, VecN &X_red, VecN &X, MailleSphere &MSph);


double normeUWVF(A_skyline &M_glob, VecN &X, MailleSphere &MSph);


double normeUWVF_parallel_version(A_skyline &M_glob, VecN &X, MailleSphere &MSph) ;


double normeDG(A_skyline &A_glob, VecN &X, MailleSphere &MSph);


void produit_P_skyline_Solution_red_ForProjection(A_skyline &P_red, VecN &X_red, VecN &X, MailleSphere &MSph) ;


void collection_Projection_On_OneElement(MKL_INT e, MatMN &MP, cplx *u, cplx *Me, const std::string & arg, const std::string & choice, MailleSphere &MSph);


/*-------------------------------------------------------------------------------- PARTIE TEST DE CONSISTANCE POUR LE SYSTEME ASSOCIE AU NOUVEAU MAILLAGE -----------------------------------------*/

/*---------------------------------------------------------------------------------- P_T P_T et P_T P_K INTERACTION INTERIEUR ---------------------------------------------------------*/

cplx int_P_TxP_T_interieure_On_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, MailleSphere &MSph, const char chaine[]);

cplx int_P_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


cplx int_P_TxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_P_TxP_T(MKL_INT i, cplx *AA, MailleSphere &Msph, const char chaine[]);


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &Msph, const char chaine[]);


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void BlocIntPP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &Msph, const char chaine[]);


void BlocIntPP(A_skyline &A_glob, MailleSphere &Msph, const char chaine[]) ;

/*----------------------------------------------------------------------------------------- P_T P_T EXTERIEUR ------------------------------------------------------------------------------*/

cplx int_P_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, MailleSphere &MSph, const char chaine[]);

cplx int_P_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_BlocExt_P_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocExtPP_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocExtPP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


// /*----------------------------------------------------------------- V_T V'_T et V_T V'_K ---------------------------------------------------------*/

cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);

cplx int_V_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxV_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph);


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;

void Build_BlocInt_V_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_V_TxV_K(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph);


void BlocIntVV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntVV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


/*------------------------------------------------------------------------ V_T V'_T ---------------------------------------------------------*/

cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);

cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);

cplx int_V_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_BlocExt_V_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocExtVV_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocExtVV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


/*--------------------------------------------------------------------------- [V] {P} ---------------------------------------------*/

cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);

cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph);

cplx int_V_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]);

cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph) ;


void Build_BlocInt_V_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;

void BlocIntVP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntVP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);

cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);

cplx int_V_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_BlocExt_V_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocExtVP_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocExtVP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


/*-------------------------------------------------------------------- [P] {V} -------------------------------------------------*/

cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);

cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph) ;


cplx int_P_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]);


cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph);

void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);

void Build_BlocInt_P_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);

void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;

void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocIntPV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntPV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


/*-------------------------------------------------------------------- P_T V'_T --------------------------------------*/

cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) ;


cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);


cplx int_P_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_BlocExt_P_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocExtPV_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocExtPV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


/*---------------------------------------------------------------------- [V] {P} + [P] {V} --------------------------------------------------*/

void BlocIntPV_VP(A_skyline &A_glob, MailleSphere &MSph);


void BlocExtPV_VP(A_skyline &A_glob, MailleSphere &MSph);


/*--------------------------------------------------------------------- Pinc P --------------------------------------------------------*/

cplx int_PincP_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_VectorPincP_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]);


/*----------------------------------------------------------------------- Pinc V ---------------------------------------------------*/

cplx int_PincV_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_VectorPincV_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]);

/*---------------------------------------------------------------------- Vinc P ----------------------------------------------------*/

cplx int_VincP_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_VectorVincP_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]);

/*---------------------------------------------------------------- Vinc V ---------------------------------------------------------*/

cplx int_VincV_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]);


void Build_VectorVincV_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]);


cplx L2_norm_OnOneElementByGauss(MKL_INT e, cplx *ALPHA, MailleSphere &MSph);


cplx L2_norm_OnOneElementByGauss(MKL_INT e, MailleSphere &MSph);


cplx L2_norm_OfanalytiquePlanePressureByGauss(MailleSphere &MSph);


cplx L2_norm_OfErrorPlanePressureByGauss(VecN &ALPHA, MailleSphere &MSph);



double norme_L2(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]);


double norme_L2(VecN &X, MailleSphere &MSph, const char chaine[]);


double norme_L2_Vitesse(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]);


double norme_L2_Vitesse(VecN &X, MailleSphere &MSph, const char chaine[]);



void collection_Solutions_locales_selfDecompose(VecN &U, const std::string & type, const std::string & arg, const std::string & choice, A_skyline &P_red, MailleSphere &MSph);


void Besselj0(MKL_INT n, double z, cplx *j0, cplx *j0P);


void Bessely0(MKL_INT n, double z, cplx *y0, cplx *y0P);


void SolexacteBessel(MKL_INT e, double *X, cplx *P, cplx *V, MailleSphere &MSph);

void SolexacteBessel(MKL_INT e, double *X, cplx *P, cplx *V, MailleSphere &MSph, double *d, double *XX0, cplx *gradP);

void SolexacteBessel(MKL_INT e, double *X, cplx *P, MailleSphere &MSph, double *XX0);

void debugg_solexacteBessel(MailleSphere &MSph);

 void transform_hetero_analytique_Pressure_and_Velocity_Solution_to_VTK(MailleSphere &MSph);


void transform_hetero_ScalarAndVelocityError_to_VTK(VecN &ALPHA, MailleSphere &MSph);


void TEST_A1_to_A4(const std::string & arg, MailleSphere &MSph, const char chaine[]);


void collection_Solutions_locales_Reduites_selfDecompose(VecN &U, const std::string & arg, const std::string & choice, A_skyline &P_red, MailleSphere &MSph);


void BuildMatBlocByBloc_M(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BuildMatBlocByBloc_N(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void collection_Solutions_locales_reduites(VecN &U, const std::string & arg, const std::string & choice, A_skyline &P_red, MailleSphere &MSph);


double compute_norm(double *X);


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph);


void compute_max_and_min(double *maxx, double *minn, MKL_INT ndof, cplx *AA);


void compute_max_and_min_of_all_ordered_eigenvalues_full(VecN &LAMBDA, MailleSphere &MSph);


cplx compute_XdotY(VecN &U, VecN &V);


void TEST_UexA1_to_A4Uex(const std::string & arg, MailleSphere &MSph);


void TEST_A1_to_A4_WithReduction(const std::string & arg, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);


void choisir_A_et_xi(Element &Elem, double x_I, double a1, double a2, double b1, double b2, double xi1, double xi2);


void calcul_P_and_V_incidente(double *X, cplx *P, cplx *V, Maillage &Mesh);


void calcul_P_and_V_transmise(double *X, cplx *P, cplx *V, Maillage &Mesh);


void calcul_P_and_V_reflechie(double *X, cplx *P, cplx *V, Maillage &Mesh);


void analytique_P_and_V(double *X, MKL_INT e, cplx *P, cplx *V, Maillage &Mesh);


cplx quad_tetra_heteroPlane(MKL_INT e, MKL_INT iloc, Maillage &Mesh);


void BuildVector_projection_Hetero(MKL_INT e, cplx *F, Maillage &Mesh);


void collection_hetero_Solutions_locales(VecN &U, Maillage &Mesh);


void collection_Solutions_locales_selfDecompose(VecN &U, const std::string & choice, A_skyline &P_red, Maillage &Mesh);


MKL_INT VerifyDecompositionPMP(Maillage &Mesh);


void TEST_A1_to_A4(const std::string & arg, Maillage &Mesh, const char chaine[]);


void TEST_A1_to_A4_WithReduction(const std::string & arg, Maillage &Mesh, const char chaine[]);


MKL_INT CHOLESKY_Decompo(MKL_INT n, cplx *M, cplx *R, cplx *D, MKL_INT *I);


void Compute_Adjoint_Mat(MKL_INT n, MKL_INT m, cplx *A, cplx *Adj);


void Descente(MKL_INT n, cplx *R, cplx *D, MKL_INT *I, cplx *F, cplx *X);


void Montee(MKL_INT n, cplx *R, cplx *D, MKL_INT *I, cplx *F, cplx *X);


void Solve_By_CHOLESKY(MKL_INT n, cplx *A, cplx *F, cplx *X);


void collection_hetero_Solutions_locales_CHOLESKY(VecN &U, const std::string & arg, Maillage &Mesh);


void Transforme_Hetero_realPartOf_ErrorVelocity_To_VTK_File_Vector(VecN &ALPHA, Maillage &Mesh);


void Transforme_Hetero_realPartOfScalarError_To_VTK_File(VecN &ALPHA, Maillage &Mesh);


void collection_Solutions_locales_selfDecompose(VecN &U, const std::string & choice, const std::string & arg, A_skyline &P_red, Maillage &Mesh);


void CHECK_ALL_RESIDUAL(Maillage &Mesh);


void CHECK_ALL_RESIDUAL_PP_VV(Maillage &Mesh);


cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2);


cplx quad_tetra_heteroPlane_VV(MKL_INT e, MKL_INT iloc, Maillage &Mesh);


void BuildMat_projection_PP_VV(MKL_INT e, cplx *MP, Maillage &Mesh);


void BuildVector_projection_Hetero_PP_VV(MKL_INT e, cplx *F, Maillage &Mesh);


cplx L2_norm_OnOneElementByGauss_Hetero(MKL_INT e, cplx *ALPHA, Maillage &Mesh);


cplx L2_norm_OnOneElementByGauss_Hetero(MKL_INT e, Maillage &Mesh);


cplx L2_norm_OfErrorPlanePressureByGauss_Hetero(VecN &ALPHA, Maillage &Mesh);


cplx L2_norm_OfanalytiquePlanePressureByGauss_Hetero(Maillage &Mesh);


cplx L2_norm_OfVelocity_OnOneElementByGauss_Hetero(MKL_INT e, cplx *ALPHA, Maillage &Mesh);


cplx L2_norm_OfVelocity_OnOneElementByGauss_Hetero(MKL_INT e, Maillage &Mesh);


cplx L2_norm_OfanalytiquePlaneVelocityByGauss_Hetero(Maillage &Mesh);


cplx L2_norm_OfErrorPlaneVelocityByGauss_Hetero(VecN &ALPHA, Maillage &Mesh);


void Left_Local_PlaneWave_Solution(cplx *u, Maillage &Mesh);


void Right_Local_PlaneWave_Solution(cplx *u, Maillage &Mesh);


void Hetero_PlaneWave_Solution(VecN &U, Maillage &Mesh);


void CHECK_CHOLESKY_DECOMP(MKL_INT e, MatMN &MP, Maillage &Mesh);


cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);


void BlocIntPV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void BlocIntPV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) ;


void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph,  const char chaine[]);


void BlocIntVP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntVP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntVP_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntPV_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Transport_To_Matlab_A1_to_A4(const std::string & arg, Maillage &Mesh);


void BlocIntPV_VP_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Transport_To_Matlab_A2_add_A3(const std::string & arg, Maillage &Mesh) ;


void CHECK_ALL_PARAMETERS(MKL_INT e, Maillage &Mesh);


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh);


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntPP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh);

void BlocIntPP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);


void BlocIntVV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocIntVV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Sum_A1_to_A4(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void TEST_A1_to_A4_with_Impedance_Coeff(const std::string & chaine, Maillage &Mesh, const char arg[]);


void TEST_SUM_A1_to_A4_with_Impedance_Coeff(Maillage &Mesh, const char chaine[]);


void Build_A1(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A2(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A3(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void Build_A4(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void Build_A1(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A2(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A3(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void Build_A4(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void Build_BlocExt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh);


void BlocExtPP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtPP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void Build_A5(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A5(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) ;


void Build_BlocExt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);

void Build_BlocExt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);

void BlocExtVV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtVV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A8(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A8(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);


void BlocExtVP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtVP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A7(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A7(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]);


void Build_BlocExt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]);

void Build_BlocExt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]);

void BlocExtPV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void BlocExtPV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A6(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A6(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void compare_Vector_and_Column_Matrix(MKL_INT e, double *maxx, double *minn, cplx *AA, cplx *b, Maillage &Mesh);


void compare_Vector_and_Column_Matrix(const std::string & arg, Maillage &Mesh, const char chaine[]);


void Build_SUM_F5_to_F8(cplx *B, Maillage &Mesh, const char chaine[]);


void TEST_Error_SolA1_to_A8_and_AnaSol(VecN &U, A_skyline &A_glob, Maillage &Mesh);


void compare_Vector_and_Column_Matrix(MKL_INT e, double *maxx, double *minn, MatMN &MP, VecN &b, Maillage &Mesh);

/*--------------------------------------------------------------------------- diag ([P] [P']) --------------------------------------------------------------------------*/
void BlocIntDiagPP(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void Build_DiagA1(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

/*----------------------------------------------------------------------------- diag([V] [V'])----------------------------------------------------------------------*/
void BlocIntDiagVV(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_DiagA4(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

/*-------------------------------------------------------------------------------  Building M with A1, A4, A5 and A8 ----------------------------------------------------------*/
void Build_M_withA1_A4_A5_A8(A_skyline &M_glob, Maillage &Mesh, const char chaine[]) ;

void DecompositionDesBlocD1Matrice(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P, VecN &LAMBDA, Maillage &Mesh);

void DecompositionGlobaleDesBlocsD1Matrice(A_skyline &A_glob, A_skyline &P, VecN &LAMBDA, Maillage &Mesh);
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/


void Build_SUM_A1_to_A8(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skyline &A_glob, Maillage &Mesh);


cplx quad_tetra_heteroPlane(MKL_INT e, MKL_INT iloc, VecN &ALPHA, Maillage &Mesh);


void BuildVector_projection_Hetero(MKL_INT e, VecN ALPHA, cplx *F, Maillage &Mesh);


cplx quad_tetra_Bessel_VV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) ;


void BuildMat_projection_PP_VV(MKL_INT e, cplx *MP, MailleSphere &MSph);


void BuildVector_projection_Hetero_PP_VV(MKL_INT e, cplx *F, MailleSphere &MSph);


void collection_hetero_Solutions_locales_CHOLESKY(VecN &U, const std::string & arg, const std::string & cas, MailleSphere &MSph);


void TEST_A1_to_A4_with_Impedance_Coeff(const std::string & chaine, MailleSphere &MSph, const char arg[]);


cplx quad_tetra(Element *Elem, Quad *Qd, Onde_plane *P1, Onde_plane *P2, MKL_INT aa);


cplx quad_tetra(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2, MKL_INT aa);


cplx integrale_ForProjec_PP(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_ForProjec_VV(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_sur_4_face_PP(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) ;


cplx integrale_sur_4_face_VV(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) ;


void BuildMat_AreaProjection(MKL_INT e, cplx *MP, MailleSphere &MSph, const char chaine[]);


cplx Sum_integral_projection_PP(MKL_INT e, MKL_INT iloc, MailleSphere &Mesh);


cplx Sum_integral_projection_VV(MKL_INT e, MKL_INT iloc, MailleSphere &Mesh);


void BuildVector_AreaProjection(MKL_INT e, cplx *F, MailleSphere &MSph);


void collection_hetero_Solutions_locales_Surfaciques_CHOLESKY(VecN &U, MailleSphere &MSph, const char chaine[]);

void SoustractionVecN(MKL_INT n, cplx *v1, cplx *v2, cplx *v3);


void collection_Solutions_locales_Surfacique_selfDecompose(VecN &U, const std::string & choice, A_skyline &P_red, MailleSphere &MSph, const char chaine[]);


void Bessely0(MKL_INT n, double z, double *y0);


void Besselj0(MKL_INT n, double z, double *j0);


void SolexacteBessel(MKL_INT e, double *X, cplx *P, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS(cplx *B, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS_parallel_version(cplx *B, MailleSphere &MSph);

void ASSEMBLAGE_DU_VecteurRHS_parallel_version_II(cplx *B, MailleSphere &MSph);

void Build_SUM_A1_to_A8(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void BlocIntDiagPP(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_DiagA1(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void BlocIntDiagVV(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_DiagA4(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) ;


void Build_M_withA1_A4_A5_A8(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skyline &A_glob, MailleSphere &MSph);


void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_Error_SolA1_to_A8_and_AnaSol(VecN &U, A_skyline &A_glob, MailleSphere &MSph);


void Build_A8_M(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A5_M(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Build_A5_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);


void Build_A8_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) ;


void TEST_A5_to_A8_with_Impedance_Coeff(const std::string & chaine, MailleSphere &MSph, const char arg[]);


void ASSEMBLAGE_DU_VecteurRHS_F5(cplx *B, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS_F6(cplx *B, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS_F7(cplx *B, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS_F8(cplx *B, MailleSphere &MSph);


void collection_Solutions_locales_Surfacique_CessenatDespres_Matrices_Decomposed(VecN &U, const std::string & choice, A_skyline &P_red, MailleSphere &MSph, const char chaine[]);


void TEST_Du_Solution(VecN &U) ;


void TEST_Error_SolA1_to_A8_and_AnaSol(VecN &Uex, VecN &U, A_skyline &A_glob, MailleSphere &MSph);


void TEST_Solution(VecN &U);


void TEST_AU(VecN &U, A_skyline &A_glob, MailleSphere &MSph);


double TEST_U_AU(VecN &U, A_skyline &A_glob, MailleSphere &MSph);


void compute_infty_norm(MKL_INT e, cplx *u, MailleSphere &MSph);


void compute_infty_norm(double *P, VecN &ALPHA, MailleSphere &MSph);


void Solve_Sol_with_Sparse(VecN &U, VecN &B, Sparse &AS, MailleSphere &MSph);


void integrale_triangle(MKL_INT e, MKL_INT f, cplx *I, MailleSphere &MSph);


void integrale_triangle_PincV(MKL_INT e, MKL_INT f, cplx *I, MailleSphere &MSph);

/*-------------------------------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------- NOUVELLE METHODE D'ASSEMBLAGE -----------------------------------------------------*/ 

/*--------------------------------------------------------------------------------------------------------------------------------------*/

void Build_interior_planeBloc(MKL_INT i, MKL_INT l, cplx *B1, cplx *B2, MailleSphere &MSph, const char chaine[]) ;


void Build_exterior_planeBloc(MKL_INT i, MKL_INT l, cplx *B3, MailleSphere &MSph, const char chaine[]);


cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph);


cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph);


cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);


cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]);


void Ajouter_BlocP_XP_Y(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void TESTER_BLOC_ASSEMBLAGE(const std::string & arg, MKL_INT e, MKL_INT f, A_skyline &A_glob, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Ajouter_BlocIntV_TxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocIntV_KxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocExtV_TxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph);


cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph) ;


cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);


void Ajouter_BlocIntV_TxP_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocIntV_KxP_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocExtV_TxP_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B1, MailleSphere &MSph);


cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, cplx *B2, MailleSphere &MSph);


cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, cplx *B3, MailleSphere &MSph);


void Ajouter_BlocIntP_TxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocIntP_KxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph);


void Ajouter_BlocExtP_TxV_T(MKL_INT i, MKL_INT l, double alpha, cplx *B, cplx *AA, MailleSphere &MSph) ;


void TESTER_BLOC_ASSEMBLAGE(MKL_INT e, A_skyline &A_glob, A_skyline &M_glob, MailleSphere &MSph);


void ASSEMBLAGE_DE_Askyline(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_Askyline_parallel_version(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);

/*---------------------------------------------------------------------------- NOUVELLE METHODE D'ASSEMBLAGE POUR M ----------------------------------------------*/

void Build_interior_planeBloc(MKL_INT i, MKL_INT l, cplx *B, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DelaMatrice_M_Skyline(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DelaMatrice_M_Skyline_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]) ;

/*----------------------------------------------------------------------------------------------------------------------------------*/

MKL_INT ordonner(double *X, double *Y);

void tri_recursive(MKL_INT debut, MKL_INT fin, MailleSphere &MSph);

void tri_recursive(MKL_INT debut, MKL_INT fin, Maillage &Mesh);


void tri_maillage(Maillage &Mesh);


void tri_selection(double *tab, MKL_INT debut, MKL_INT fin);


void tri_selection(MKL_INT debut, MKL_INT fin, Maillage &Mesh);


void tri_selection(MKL_INT debut, MKL_INT fin, MailleSphere &MSph);

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2(double *r, VecN &X, MailleSphere &MSph, const char chaine[]);

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2_Vitesse(double *r, VecN &X, MailleSphere &MSph, const char chaine[]);


void get_column(MKL_INT j, MKL_INT n, cplx **col, cplx *Z);


void initialize_tab(MKL_INT n, cplx *ZZ);


void initialize_tab(MKL_INT n, double *ZZ);


void get_F_elem(MKL_INT ndof, MKL_INT e, cplx **b, cplx *B);


void initialize(MKL_INT n, cplx *W1, cplx *W2);


void Solveur_GMRES(MKL_INT nkry, VecN &F, A_skyline &A_glob, VecN &U, MailleSphere &MSph) ;


cplx compute_XdotY(MKL_INT n, cplx *U, cplx *V);


MKL_INT check_vector(MKL_INT n, cplx *v);


void check_Allocation(MKL_INT n, cplx *v);


double norme_2(MKL_INT n, cplx *U) ;


void compute_adjoint_of_matrix(MKL_INT n, MKL_INT m, cplx *A, cplx *B) ;


void copy_krylov_basis(MKL_INT nkry, MKL_INT n, cplx *ZZ, cplx *Z_n) ;


void copy_tab(MKL_INT n_x, cplx *X, MKL_INT n_y, cplx *Y);


void compute_residul(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c) ;


void get_F_elemRed(MKL_INT e, cplx **B, cplx *B_red, Maillage &Mesh);


void get_F_elemRed(MKL_INT e, cplx **B, cplx *B_red, MailleSphere &MSph);


void prodMatVecComplexWithoutInit(MKL_INT m_a, MKL_INT n_a,  MKL_INT n_b, cplx *a, cplx *b, cplx *c);


void Solveur_GMRES(double *compte, MKL_INT nkry, VecN &F, A_skylineRed &A_red, VecN &U, MailleSphere &MSph);


void transport_the_Matrix_to_Matlab(MKL_INT m, MKL_INT n, cplx *A, const char chaine[]) ;

void transport_theMatrix_to_Matlab_zero_base(MKL_INT m, MKL_INT n, cplx *MP, const char chaine[]) ;


void transport_Vector_to_Matlab(MKL_INT n, cplx *U, const char chaine[]) ;


void Correcte_Krylov_Basis(MKL_INT nkry, MKL_INT n, cplx *ZZ, cplx *Kn);


void compute_residual(double *compte, A_skylineRed &A_red, VecN &U, VecN &F, VecN &Fdelta, cplx *AU, MailleSphere &MSph);


void GMRES_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, VecN &F, A_skylineRed &A_red, VecN &U, MailleSphere &MSph, MKL_INT total_iter);


void ProdAB(MKL_INT m_A, MKL_INT n_A, MKL_INT n_B, cplx *A, cplx *B, cplx *C) ;


double norme_j_A(cplx *A, MKL_INT m_A, MKL_INT n_A, MKL_INT j) ;


cplx dot_product(cplx *A, MKL_INT m_A, MKL_INT n_A, MKL_INT j1, MKL_INT j2) ;


void prodAB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodMV(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F) ;


void prodAstarB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodMstarV(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F) ;


void QR(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *R) ;


void affiche_mat(cplx *A, MKL_INT m_A, MKL_INT n_A, const char arg[]) ;


void Solve_tri_sup(cplx *R, cplx *X, cplx *F, MKL_INT n) ;


void Solve_With_QR(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *F, MKL_INT n_F, cplx *X);


void Solve_With_QR(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *F, MKL_INT n_F, cplx *X, cplx *R, cplx *b);


void prodtransAB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodtransMV(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F);


void prodAstar_transB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodA_transB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void produit_A_skyline_P_skyline_ColMajor(A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, MailleSphere &Msph);


void transport_theMatrix_to_Matlab(MKL_INT m, MKL_INT n, cplx *MP, const char chaine[]) ;


void extract_bloc_to_matlab(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, MailleSphere &Msph);


void TESTProdA_skyline_Vector_OneOneElement_to_matlab(MKL_INT e, MKL_INT l, VecN &U, VecN &V, A_skylineRed &A_red, MailleSphere &Msph) ;


void transform_P_red_to_wide_skyline(A_skyline &P_red, A_skylineRed &S_red, MailleSphere &MSph);


void transform_adjointP_red_to_wide_skyline(A_skyline &P_red, A_skylineRed &S_red, MailleSphere &MSph);



void prodAB_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodABstar_ColMajor(cplx alpha, cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx beta, cplx *C);


void prodMV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F) ;


void prodAB_ColMajor(cplx alpha, cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx beta, cplx *C);


void prodMV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx alpha, cplx *V, MKL_INT n_V, cplx *F, cplx beta) ;


void prodAstarB_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodMstarV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F);


void prodMstarV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx alpha, cplx *V, MKL_INT n_V, cplx *F, cplx beta);


void compute_inverse_diagonal_skyline(A_skyline &M_glob, A_skyline &invM_glob, MailleSphere &MSph) ;


void compute_inverse_diagonal_skyline(A_skylineRed &M_red, A_skylineRed &invM_red, MailleSphere &MSph);


void TEST_result_Astar_skyline_RedVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_Astar_skyline_FullVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_A_skyline_RedVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_A_skyline_FullVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_AstarA_skyline_RedVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


cplx compute_XdotY_BlocByBloc(MKL_INT n_X, MKL_INT n_Y, cplx *X, cplx *Y, MailleSphere &MSph);


double norme_2_BlocByBloc(MKL_INT n_X, cplx *X, MailleSphere &MSph);


void TEST_XdotY_product(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_AstarA_skyline_FullVectorGivesFullVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void solveur_gradient_conjugue(MKL_INT N_res, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph);


void produit_P_skyline_Solution_Full(A_skyline &P_red, VecN &X, VecN &U, MailleSphere &MSph);


void copy_tab_BlocByBloc_FulltoFull(MKL_INT n_X, cplx *X, MKL_INT n_Y, cplx *Y, MailleSphere &MSph);

void copy_tab_BlocByBloc_RedtoFull(MKL_INT n_X, cplx *X, MKL_INT n_Y, cplx *Y, MailleSphere &MSph);


void TEST_result_A_skyline_FullVectorGivesRedVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_Astar_skyline_FullVectorGivesRedVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_AstarA_skyline_FullVectorGivesRedVector(MKL_INT size_Vh_red, A_skylineRed &A_red, MailleSphere &MSph);


void compute_residual_Full_to_red(A_skylineRed &A_red, VecN &X, VecN &F_red, VecN &Fdelta, cplx *AX, MailleSphere &MSph);


void gradient_conjugue_avec_restart(MKL_INT N_res, double eps, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph);


void compute_sizeBlocRed(MKL_INT i, MKL_INT e, MKL_INT l, double epsilon, MKL_INT *NRED, MKL_INT *NDIFF, cplx *AA, double *w, A_skyline &A_glob, MailleSphere &MSph);


MKL_INT compute_sizeBlocRed(MKL_INT i, double epsilon, MKL_INT *TAILLE, MKL_INT *NRED, MKL_INT *NDIFF, A_skyline &A_glob, MailleSphere &MSph) ;


void get_F_elemRed(MKL_INT i, MKL_INT e, MKL_INT *TAILLE, cplx **B, VecN &B_red, MailleSphere &MSph) ;


void get_F_elemRed(MKL_INT i, MKL_INT e, MKL_INT *TAILLE, cplx **B, cplx *B_red, MailleSphere &MSph) ;


void DecompositionDeBloc(MKL_INT i, MKL_INT e, MKL_INT l, MKL_INT *TAILLE, MKL_INT *NDIFF, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void DecompositionGlobaleDesBlocs(MKL_INT i, MKL_INT *TAILLE, MKL_INT *NDIFF, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT i, MKL_INT e, MKL_INT *NRED, MKL_INT *NDIFF, cplx *a, cplx *b, A_skyline &P_red,  MailleSphere &MSph) ;


void store_P_and_transpose_P(MKL_INT i, MKL_INT e, MKL_INT *NRED, MKL_INT *NDIFF, cplx *a, A_skyline &P_red,  const std::string & arg, MailleSphere &MSph);


void store_P_and_transpose_P(MKL_INT i, MKL_INT e, MKL_INT j, MKL_INT *NRED, MKL_INT *NDIFF, cplx *a, cplx *b, A_skyline &P_red, MailleSphere &MSph) ;


void store_P_and_transpose_P(MKL_INT i, MKL_INT e, MKL_INT j, MKL_INT *NRED, MKL_INT *NDIFF, cplx *a, A_skyline &P_red, const std::string & arg, MailleSphere &MSph);


void TEST_result_A_skyline_FullVectorGivesFullVector(MKL_INT i, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_A_skyline_RedVectorGivesFullVector(MKL_INT i, MKL_INT size_red_O, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_Astar_skyline_RedVectorGivesFullVector(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_Astar_skyline_FullVectorGivesFullVector(MKL_INT i, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_AstarA_skyline_RedVectorGivesFullVector(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_AstarA_skyline_FullVectorGivesFullVector(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


MKL_INT compute_sizeForA_red(MKL_INT i, MKL_INT *NRED, MailleSphere &MSph);


void P_operator(MKL_INT i, const std::string & chaine, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph) ;


void P_operator_II(const std::string & chaine, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph);


void SVD(MKL_INT *NRED, MKL_INT *NDIFF, MKL_INT *TAILLE, A_skyline &M_glob, MailleSphere &MSph);


void TEST_result_A_skyline_FullVectorGivesRedVector(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void solveur_gradient_conjugue(MKL_INT i, MKL_INT N_res, MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph);


void combination_zaxpy(MKL_INT i, const std::string & chaine, MKL_INT *NRED, cplx beta, cplx *X, cplx *Y, MailleSphere &MSph);


void compute_residual_Full_to_red(MKL_INT i, MKL_INT *NRED, A_skylineRed &A_red, VecN &X, VecN &F_red, VecN &Fdelta, cplx *AX, MailleSphere &MSph);


void TEST_combination_zaxpy_full(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_combination_zaxpy_red(MKL_INT i, MKL_INT size_Vh_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void gradient_conjugue_avec_restart(MKL_INT N_res, double eps, MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph);


void test_indices_iter() ;

void build_newi(MKL_INT ifin, MKL_INT size, MKL_INT *newi);

MKL_INT minimise(MKL_INT size, double *tab) ;


void transporte_voisin(MailleSphere &MSph);


void transporte_taille_Galerkin(MKL_INT *NRED, MailleSphere &MSph);


void transporte_MUMPS_red(MKL_INT *TAILLE, MailleSphere &MSph);


void transporte_MUMPS_originale(MailleSphere &MSph);


void compute_residual_Full_to_red(A_skylineRed &A_red, cplx *X, VecN &F_red, VecN &Fdelta, cplx *AX, MailleSphere &MSph);


void compute_residual_Full_to_red(A_skylineRed &A_red, VecN &X, VecN &F_red, VecN &Fdelta, cplx *AX, MailleSphere &MSph, double *temps, double facteur);


void compute_residual_Full_to_red(A_skylineRed &A_red, cplx *X, VecN &F_red, VecN &Fdelta, cplx *AX, MailleSphere &MSph, double *temps, double facteur);


void min_gradient_conjugue(MKL_INT i, MKL_INT N_res, double facteur, MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &Xdelta, double norm_Fi0, double *temps, double *taux_nc, double *taux, double *norm_Fnew, VecN &Fnew, VecN &Xnew, VecN &AXnew, MailleSphere &MSph) ;


void gradient_conjugue_avec_restart(MKL_INT i0, MKL_INT N_res, double eps, const char chaine1[], const char chaine2[], MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph, cplx *W);



void solveur_gradient_conjugue(MKL_INT i, MKL_INT N_res, MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &X, MailleSphere &MSph, cplx *r, cplx *r0, cplx *p, cplx *p0, cplx *b, cplx *Ap, cplx *W, double *temps_prod, double facteur, MKL_INT *n_multi);


void min_gradient_conjugue(MKL_INT i, MKL_INT N_res, double facteur, MKL_INT *NRED, VecN &F_red, A_skylineRed &A_red, VecN &Xdelta, double norm_Fi0, double *temps, double *taux_nc, double *taux, double *norm_Fnew, VecN &Fnew, VecN &Xnew, VecN &AXnew, MailleSphere &MSph, cplx *r, cplx *r0, cplx *p, cplx *p0, cplx *b, cplx *Ap, cplx *W, double *temps_prod, MKL_INT *n_multi);


void read_file(MKL_INT m, MKL_INT n, const char chaine[], double *TAB);


void read_and_merge_file(MKL_INT m, const char chaine1[], const char chaine2[], const char new_chaine[]);


void P_operator(MKL_INT i, MKL_INT *NRED, A_skylineRed &A_red, A_skylineRed &S_red, MailleSphere &MSph) ;


void prodMV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx alpha, cplx *V, MKL_INT n_V, cplx *F, cplx beta, MKL_INT lda);


void prodMstarV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx alpha, cplx *V, MKL_INT n_V, cplx *F, cplx beta, MKL_INT lda, MKL_INT ldb, MKL_INT ldc);

void TEST_result_A_skyline_FullVectorGivesFullVector_NewVersion(MKL_INT i, MKL_INT sizeA_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void TEST_result_A_skyline_RedVectorGivesFullVector_NewVersion(MKL_INT i, MKL_INT size_red_O, MKL_INT sizeA_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void Solveur_GMRES_RHS(double *compte, MKL_INT nkry, MKL_INT e, MKL_INT *NRED, VecN &F, A_skylineRed &A_red, VecN &U, MailleSphere &MSph, cplx *U_red, cplx *H, cplx *Az, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *v);


cplx compute_XdotY_BlocByBloc(MKL_INT i, MKL_INT *NRED, MKL_INT n_X, MKL_INT n_Y, cplx *X, cplx *Y, MailleSphere &MSph);


double norme_2_BlocByBloc(MKL_INT i, MKL_INT *NRED, MKL_INT n_X, cplx *X, MailleSphere &MSph);


void TEST_XdotY_product(MKL_INT i, MKL_INT size_red, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);

void TEST_time_A_skyline_NewandOldVersion(MKL_INT i, MKL_INT *NRED, A_skylineRed &A_red, MailleSphere &MSph);


void multiply(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y) ;


void multiply(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y, MKL_INT lda);


void multiply_address(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y) ;


void multiply_address(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y, MKL_INT lda);


void multiply_address_MV(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y) ;


void multiply_address_MV(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *X, MKL_INT n_X, cplx *Y, MKL_INT lda);


void TEST_multiply();

void TEST_multiply_address();

void TEST_multiply_address_MV();
/*-----------------------------------------------------------------------------------------------------------------------------------------*/


void build_A__skyline(MKL_INT ndof, MKL_INT n_inter, MKL_INT n_elem, A__skyline &Asky);


void get_A__skyline_Block(A__skyline &Asky, cplx *Asky_mem, MKL_INT i, MKL_INT l, MKL_INT taille, cplx **block);

void get_P__skyline_Block_red(A__skyline &Asky, cplx *Asky_mem, MKL_INT i, cplx **block, MailleSphere &MSph);

void get_P__skyline_Block_red(MKL_INT shift, MKL_INT e, MKL_INT ndof, MKL_INT *TAILLE, cplx *Psky_mem, cplx **block);

void get_V__skyline_Block(cplx *valeur, MKL_INT i, MKL_INT taille, cplx **block);


//i l'exposant de notre mode de réduction, e = numero de l'élément
void get_V__skyline_Block_red(cplx *valeur, MKL_INT i, MKL_INT e, MKL_INT *TAILLE, cplx **block, MKL_INT N_elements);


//shift = (i-1) * N_elements avec i l'exposant de notre mode de réduction
//e = numero de l'élément
void get_V__skyline_Block_red(cplx *valeur, MKL_INT shift, MKL_INT e, MKL_INT *TAILLE, cplx **block);


void get_V__skyline_Block_red(cplx *valeur, MKL_INT i, cplx **block, MailleSphere &MSph);

void ASSEMBLAGE_DE_A__skyline(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_A__skyline_parallel_version(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph, const char chaine[]);

void compare_A_glob_AND_Asky(const char chaine1[], const char chaine2[], A__skyline &Asky, cplx *Asky_mem, A_skyline &A_glob, MailleSphere &MSph);


void ProdA__skyline_Vector_mkl(A__skyline &Asky, cplx *Asky_mem, cplx *U, cplx *V, MailleSphere &MSph);

void compare_prodA__skylineVector_and_A_globVector(A__skyline &Asky, cplx *Asky_mem, A_skyline &A_glob, MailleSphere &MSph);


void ASSEMBLAGE_DelaMatrice_M__Skyline(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DelaMatrice_M__Skyline_parallel_version(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph, const char chaine[]);

void compare_M_glob_AND_Msky(const char chaine[], A__skyline &Msky, cplx *Msky_mem, A_skyline &M_glob, MailleSphere &MSph, const char arg[]);

void compute_sizeBlocRed(MKL_INT e, MKL_INT taille, cplx *AA, double *w, A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);


void copy_A__skyline(A__skyline &Asky, cplx *Asky_mem, A__skyline &Bsky, cplx *Bsky_mem, MailleSphere &MSph);


void TEST_copy_A__skyline_affichage(A__skyline &Asky, cplx *Asky_mem, A__skyline &Bsky, cplx *Bsky_mem, MailleSphere &Msph);

void TEST_copy_A__skyline_diff(A__skyline &Asky, cplx *Asky_mem, A__skyline &Bsky, cplx *Bsky_mem, MailleSphere &MSph, const char chaine1[], const char chaine2[]);


MKL_INT VerifyDecompositionPMP(A__skyline &Msky, cplx *Msky_mem, MKL_INT e, MKL_INT taille, cplx *P, double *D, MailleSphere &MSph, cplx *adjP, cplx *PD, cplx *PDP);


MKL_INT compute_sizeBlocRed(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);


MKL_INT compute_thesizeOfA_red(MailleSphere &MSph);


void DecompositionDeBloc(MKL_INT e, MKL_INT taille, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph, cplx *AA, double *D);


void DecompositionGlobaleDesBlocs(A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph);


void TEST_Decomposition(const char chaine1[], const char chaine2[], MailleSphere &MSph, const char arg[]);


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT taille, A__skyline &Asky, cplx *Asky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph, cplx *AA, double *w) ;


void DecompositionGlobaleDesBlocs_ET_Normalisation(A__skyline &Asky, cplx *Asky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph);


void produit_A_skyline_P_skyline(A__skyline &Asky, cplx *Asky_mem, A__skyline &Psky, cplx *Psky_mem, A__skyline &Ared, cplx *Ared_mem, MailleSphere &Msph) ;


void compare_Ared_and_Ared_sky(const char chaine1[], const char chaine2[], MailleSphere &MSph, const char chaine3[]);


void produit_P_skyline_RightHandSide(A__skyline &Psky, cplx *Psky_mem, cplx *Bred, cplx *B, MailleSphere &MSph);


void produit_P_skyline_RightHandSide_versionContinue(A__skyline &Psky, cplx *Psky_mem, cplx *Bred, cplx *B, MailleSphere &MSph);


void compare_Bred_and_Bred_sky(const char chaine[], MailleSphere &MSph, const char arg[]);


void ProdA__skylineRed_VectorRed_mkl(A__skyline &Ared, cplx *Ared_mem, cplx *U, cplx *V, MailleSphere &MSph);


void compare_prodAredVred_and_Ared_skyVred_sky(MailleSphere &MSph, const char chaine[]);


void compute_sizeBlocRed(MKL_INT i, MKL_INT e, MKL_INT taille, double epsilon, MKL_INT *NRED, MKL_INT *NDIFF, cplx *AA, double *D, A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);


void compute_sizeBlocRed(MKL_INT i, double epsilon, MKL_INT *TAILLE, MKL_INT *NRED, MKL_INT *NDIFF, A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);


void SVD(MKL_INT *NRED, MKL_INT *NDIFF, MKL_INT *TAILLE, A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);


void TEST_AllGalerkinSpaces(MailleSphere &MSph, const char chaine[]);


void copy_A__skyline_red(MKL_INT i, MKL_INT *NRED, A__skyline &Asky, cplx *Asky_mem, A__skyline &Bsky, cplx *Bsky_mem, MailleSphere &MSph);


void TEST_copy_Ared(MailleSphere &MSph, const char chaine[]);


void copy_A__skyline_red_NewVersion(MKL_INT i, MKL_INT e, MKL_INT taille, MKL_INT *compteur, MKL_INT *NRED, A__skyline &Ared, cplx *Ared_mem, cplx *AA, MKL_INT *Tab_voisins, MailleSphere &MSph);


void test_copy_block_red(MailleSphere &MSph, const char chaine[]);



void get_5_blocks_Of_A__skyline(cplx *Asky_mem, MKL_INT i, MKL_INT e, MKL_INT taille, cplx **blocks);


void copy_A__skyline_red_NewVersion(MKL_INT i, MKL_INT *NRED, A__skyline &Ared, cplx *Ared_mem, A__skyline &Tred, cplx *Tred_mem, MKL_INT *Tab_voisins, MailleSphere &MSph);


void copy_V__skyline_red(MKL_INT i, MKL_INT e, MKL_INT *compteur, MKL_INT *NRED, cplx *U, cplx *V_e, MKL_INT *Tab_voisins, MKL_INT N_elements, MKL_INT ndof);


void copy_V__skyline_red_blas(MKL_INT i, MKL_INT e, MKL_INT *compteur, MKL_INT *NRED, cplx *U, cplx *V_e, MKL_INT *Tab_voisins, MKL_INT N_elements, MKL_INT ndof);


void test_copy_Vect_red(MailleSphere &MSph, const char chaine[]);


//void copy_V__skyline_red(MKL_INT i, MKL_INT *SDE_V, MKL_INT *NRED, MKL_INT *TAILLE, cplx *U, cplx *V, MailleSphere &MSph);


void ProdA__skyline_Vector_mem_continueblas(MKL_INT i, MKL_INT *SDE, MKL_INT *NRED, cplx *Asky_mem, cplx *U, cplx *V, MKL_INT *Tab_voisins, MKL_INT N_elements, MKL_INT ndof, MKL_INT size_u, cplx *u);


void compare_prodMV_mem_cont_and_oldVersion(MailleSphere &MSph, const char chaine[]);


cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Bessel &j0_e, double *X0);


cplx quad_tetra_VV(Element &Elem, Quad &Qd, Onde_plane &P1, Hankel &he, double *X0);


void collection_Solutions_locales_selfDecompose(VecN &U, const std::string & choice, const std::string & arg, const std::string & chaine, A_skyline &P_red, Maillage &Mesh);


void copy_Neighbours(MKL_INT *Tab_voisins, MailleSphere &MSph);


void Build_A(cplx *A, MKL_INT m, MKL_INT n);


void Build_V(cplx *V, MKL_INT n);


void prodA__skyline_Vector_mem_discontinue_blas(MKL_INT i, MKL_INT *NRED, A__skyline &Asky, cplx *Asky_mem, cplx *U, cplx *V, MKL_INT *Tab_voisins, MKL_INT N_elements, MKL_INT ndof, MKL_INT taille);


void compare_prodMV_mem_cont_and_oldVersion_II(MailleSphere &MSph);


MKL_INT compute_nnz_ofVector(MKL_INT i, MKL_INT e, MKL_INT N_elements, MKL_INT ndof, MKL_INT *NRED, MKL_INT *Tab_voisins);


MKL_INT compute_nnz_ofVector(MKL_INT i, MKL_INT N_elements, MKL_INT ndof, MKL_INT *NRED, MKL_INT *Tab_voisins);


void  TEST_nnz_Vector_and_Matrix(MailleSphere &MSph);


MKL_INT compute_nnz_Asky(MKL_INT i,  MKL_INT e, MKL_INT *NRED, MKL_INT N_elements, MKL_INT *Tab_voisins);


MKL_INT compute_nnz_Asky(MKL_INT i,  MKL_INT *NRED, MKL_INT N_elements, MKL_INT *Tab_voisins);


MKL_INT local_to_globRed(MKL_INT iloc, MKL_INT tetra, MailleSphere &MSph);


MKL_INT local_to_globRed(MKL_INT shift, MKL_INT iloc, MKL_INT tetra, MKL_INT *TAILLE);


MKL_INT compute_nnz_for_Sparse_COO_format(MKL_INT shift, MKL_INT *NRED, MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Ared, cplx *Ared_mem, MailleSphere &MSph);


void transforme_A__skyline_to_Sparse_COO_format(MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Ared, cplx *Ared_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void TEST_Sparse_COO(MailleSphere &MSph, const char chaine[]);


void transforme_A__skyline_to_Sparse_COO_format_ColMajor(MKL_INT i, MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Ared, cplx *Ared_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void prodMV_Sparse_COO(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *U, cplx *V);


void prodMV_Sparse_COO(MKL_INT m_A, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *V);


void global_to_localRed(MKL_INT N_elements, MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc);


void transforme_A__skyline_to_Sparse_COO_format__(MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Ared, cplx *Ared_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void transforme_A__skyline_to_Sparse_COO_format__full(MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Ared, cplx *Ared_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void Adiez(double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT m_A, MKL_INT nnz, MKL_INT size, cplx *Bred, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b);


void Adiez_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, MKL_INT m_A, MKL_INT nnz, MKL_INT size, cplx *F, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b, cplx *Fdelta, cplx *Udelta, cplx *AU, MKL_INT size_Kn, MKL_INT shift, MKL_INT N_elements, MKL_INT size_max, MKL_INT *NRED, MKL_INT *TAILLE, A__skyline &Psky, cplx *Psky_mem, cplx *V, cplx *Uh, cplx *Uerr, const char chaine[], MailleSphere &MSph, MKL_INT total_iter);


void compute_residual(double *tAU, double *tV, MKL_INT m, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *F, cplx *Fdelta, cplx *AU);


void TEST_Adiez(MailleSphere &MSph, const char chaine[]);


void Solveur_GMRES_precond(double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT m_A, MKL_INT nnz_i, cplx *Bred_i, MKL_INT *rows_i, MKL_INT *cols_i, cplx *values_i, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b, cplx *A$F, cplx *H_p, cplx *K_p, MKL_INT size, MKL_INT nnz_, cplx *Bred_, MKL_INT *rows_, MKL_INT *cols_, cplx *values_, MKL_INT size_K_p);

void produit_P_skyline_Vecteur_red(A__skyline &Psky, cplx *Psky_mem, cplx *Xred, cplx *X, MKL_INT N_elements, MailleSphere &MSph);


void produit_P_skyline_VecteurContinu_red(A__skyline &Psky, cplx *Psky_mem, cplx *Xred, cplx *X, MKL_INT N_elements, MailleSphere &MSph);


void produit_P_skyline_VecteurContinu_red(MKL_INT shift, MKL_INT *NRED, MKL_INT *TAILLE, A__skyline &Psky, cplx *Psky_mem, cplx *Xred, cplx *X, MKL_INT N_elements, MailleSphere &MSph);


void complete_vector(MKL_INT m_A, MKL_INT size_max, cplx *U);


void complete_vector(MKL_INT m_A, MKL_INT size_max, cplx *U, cplx *V);



void TEST_GMRES_precond(MailleSphere &MSph);


void TESTError_GMRES_precond(MailleSphere &MSph);


MKL_INT compute_nnz_for_Sparse_COO_format(MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);


void transforme_A__skyline_to_Sparse_COO_format__full(MKL_INT ndof, MKL_INT N_elements, MKL_INT *Tab_voisins, A__skyline &Asky, cplx *Asky_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void Adiez_complete(double *tt1, double *tt2, double *tt3, double *tt4, MKL_INT nkry, MKL_INT m_A_i, MKL_INT nnz_i, cplx *Fred_i, MKL_INT *rows_i, MKL_INT *cols_i, cplx *values_i, cplx *tilde_U, cplx *U_gmres, cplx *H, cplx *v, cplx *Fkry, cplx *Kn, cplx *Kr, cplx *R, cplx *b, MKL_INT m_A, MKL_INT nnz, cplx *Fred_max, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *hat_U, MKL_INT size_Kn);


void Adiez_complete_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, MKL_INT m_A_i, MKL_INT nnz_i, cplx *Fred_i, MKL_INT *rows_i, MKL_INT *cols_i, cplx *values_i, MKL_INT m_A, MKL_INT nnz, cplx *Fred_max, MKL_INT *rows, MKL_INT *cols, cplx *values, MKL_INT size_Kn, MKL_INT shift, MKL_INT N_elements, MKL_INT size_max, MKL_INT *NRED, MKL_INT *TAILLE, A__skyline &Psky, cplx *Psky_mem, cplx *Uh, const char chaine[], MailleSphere &MSph, MKL_INT total_iter);


void TEST_Adiez_complete(MailleSphere &MSph, const char chaine[]);


void TEST_transformation_of_A__skylineToSparseCSR(MailleSphere &MSph, const char chaine[]);


void transporte_A__skyline(const char chaine[], A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);

void transporte_M__skyline(const char chaine[], A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);

void transporte_P__skyline(const char chaine[], A__skyline &Psky, cplx *Psky_mem, MailleSphere &MSph);

void read_A__skyline_from_file(const char chaine[], A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);

void read_M__skyline_from_file(const char chaine[], A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph);

void transporte_A__skyline_red(const char chaine[], A__skyline &Ared, cplx *Ared_mem, MailleSphere &MSph);

void read_A__skyline_red_from_file(const char chaine[], MKL_INT size_A_red, A__skyline &Ared, cplx *Ared_mem, MailleSphere &MSph);

void TEST_transportation_A__slyline_red(MailleSphere &MSph, const char chaine[]);

void TEST_transportation_A__slyline(MailleSphere &MSph, const char chaine[]);

void TEST_transportation_M__slyline(MailleSphere &MSph, const char chaine[]);

void transporte_size_mesh_and_ndof(const char chaine[], MKL_INT N_elements, MKL_INT ndof);

void transporte_tools(MailleSphere &MSph, const char chaine[]);

void read_A_glob_from_file(const char chaine[], A_skyline &A_glob, MailleSphere &MSph);

void TEST_transportation_A_glob(MailleSphere &MSph, const char chaine[]);

void TEST_transportation_M_glob(MailleSphere &MSph, const char chaine[]);

void transporte_All_eigenvalues_red(const char chaine[], VecN &LAMBDA, MailleSphere &MSph);

void read_All_eignevalues_red(const char chaine[], VecN &LAMBDA, MailleSphere &MSph);

void TEST_transportation_P_glob(MailleSphere &MSph, const char chaine[]);

void read_A_red_from_file(const char chaine[], MKL_INT size_A_red, A_skylineRed &A_red, MailleSphere &MSph);

void read_Vector_from_file(MKL_INT n, cplx *U, const char chaine[]);


void read_third_component_of_barycentric_coordinate(const char chaine[], MKL_INT N_elements, double *bary_coord);

void read_sorted_elements(const char chaine[], MKL_INT *SRDELTS);


void read_neighbours(const char chaine[], MKL_INT *voisins);


void read_size_mesh_and_ndof(const char chaine[], MKL_INT *N_elements, MKL_INT *ndof);

/*-----------------------------------------------------------------------------------------------------------------------*/
void read_A__skyline_from_file(const char chaine[], A__skyline &Asky, cplx *Asky_mem, Maillage &Mesh);

void TEST_transportation_A__slyline(Maillage &Mesh, const char chaine[]);


void transporte_tools(Maillage &Mesh, const char chaine[]);

/*----------------------------------------------------------------------------------------------------------------------------*/

void BuildNewTracesMatrix_1_faceElemElem(MKL_INT e, MKL_INT l, cplx *B, MailleSphere &MSph, const char chaine[]);


void BuildNewTracesMatrix_1_faceVoisinElem(MKL_INT e, MKL_INT l, cplx *B, MailleSphere &MSph, const char chaine[]);


void BuildNewTracesMatrix_1_ExtfaceElemElem(MKL_INT e, MKL_INT l, cplx *B, MailleSphere &MSph, const char chaine[]);


void TEST_NewTracesMatrix_1_Face(MailleSphere &MSph, const char chaine[]);


void computeSizeDecomposedNewTracesMatrix(cplx *B, cplx *P, double *LAMBDA, MKL_INT *nm, MKL_INT *np, MailleSphere &MSph);


MKL_INT DecomposeNewTracesMatrix(MKL_INT n, cplx *B, cplx *P, double *LAMBDA, MKL_INT nm, cplx betam, cplx *Bm, cplx *Pm, double *LAMBDAm, cplx *AdjtPm, MKL_INT np, cplx betap, cplx *Bp, cplx *Pp, double *LAMBDAp, cplx *AdjtPp, cplx *G, MailleSphere &MSph);

void BuildPositivePart_Of_NewTracesMatrix(MKL_INT n, cplx *B, cplx *P, double *LAMBDA, MKL_INT nm, MKL_INT np, cplx beta, cplx *Bp, cplx *Pp, double *LAMBDAp, cplx *AdjtPp, cplx *G, MailleSphere &MSph);

void BuildNegativePart_Of_NewTracesMatrix(MKL_INT n, cplx *B, cplx *P, double *LAMBDA, MKL_INT nm, cplx beta, cplx *Bm, cplx *Pm, double *LAMBDAm, cplx *AdjtPm, cplx *G, MailleSphere &MSph);

MKL_INT TEST_Decomposition_Of_NewTracesMatrix(MKL_INT n, cplx *B, MKL_INT ndof, cplx *Bm, cplx *Bp);

void Ajouter_Les_4_BlocInt_ElementElement(MKL_INT i, MKL_INT l, cplx alphaPP, cplx alphaPV, cplx alphaVP, cplx alphaVV, cplx *B, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Ajouter_Les_4_BlocInt_VoisinElement(MKL_INT i, MKL_INT l, cplx alphaPP, cplx alphaPV, cplx alphaVP, cplx alphaVV, cplx *B, cplx *AA, MailleSphere &MSph, const char chaine[]);


void Ajouter_Les_4_BlocExt_ElementElement(MKL_INT i, MKL_INT l, cplx alphaPP, cplx alphaPV, cplx alphaVP, cplx alphaVV, cplx *B, cplx *AA, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_LA_Reciprocite(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph, const char chaine[]);

void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph, const char chaine[]);


void TEST_NewTracesBloc(MailleSphere &MSph, const char chaine[]);


void debugg_A__skyline_to_A_skyline_to_Sparse_CSR(MailleSphere &MSph, const char chaine[]);


void transforme_A_skyline_to_Sparse_CSR(A__skyline &Asky, cplx *Asky_mem, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);


void transporte_Value_and_rows_To_matlab(const char chaine[], MKL_INT nnz, MKL_INT *ia, cplx *a);


void transporte_columns_To_matlab(const char chaine[], MKL_INT n, MKL_INT *ja) ;


MKL_INT compute_nnz_for_Sparse_COO_format_full(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);


void transforme_A__skyline_to_Sparse_COO_format__full(A__skyline &Asky, cplx *Asky_mem, MKL_INT nnz, MKL_INT n, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void transport_Vector_to_Matlab(MKL_INT n, MKL_INT *U, const char chaine[]);

void Step_to_solution(MailleSphere &MSph, const char chaine[]);


double normeUWVF(A__skyline &Msky, cplx *Msky_mem, cplx *X, MailleSphere &MSph);


double normeDG(A__skyline &Asky, cplx *Asky_mem, cplx *X, MailleSphere &MSph);


double norme_L2(MKL_INT e, cplx *X, MailleSphere &MSph, const char chaine[]);


double norme_L2(cplx *X, MailleSphere &MSph, const char chaine[]);

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2(double *r, cplx *X, MailleSphere &MSph, const char chaine[]);


double norme_L2_Vitesse(MKL_INT e, cplx *X, MailleSphere &MSph, const char chaine[]);


double norme_L2_Vitesse(cplx *X, MailleSphere &MSph, const char chaine[]);

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2_Vitesse(double *r, cplx *X, MailleSphere &MSph, const char chaine[]);

/*------------------------------------------------- AVEC REDUCTION PAR SVD ---------------------------------------------*/
void compute_sizeBlocRed(MKL_INT e, MKL_INT l, cplx *AA, MKL_INT taille, double *w, A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);

MKL_INT compute_sizeBlocRed(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);


MKL_INT VerifyDecompositionPMP(A__skyline &Asky, cplx *Asky_mem, MKL_INT e, MKL_INT j, MKL_INT taille, cplx *AA, double *w, cplx *PD, cplx *adjtP, cplx *PDP, MailleSphere &MSph);


void TEST_ASSEMBLAGE_DE_LA_Reciprocite_reduite(MailleSphere &MSph, const char chaine[]);


void debugg_A__skyline_to_A_skyline_to_Sparse_CSR_version_reduite(MailleSphere &MSph, const char chaine[]);


MKL_INT compute_nnz_for_Sparse_COO_format_reduced_to_SVD_max(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph);


void transforme_A__skyline_to_Sparse_COO_format__reduced_to_SVD_max(A__skyline &Asky, cplx *Asky_mem, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void Solveur_GMRES(double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT m_A, MKL_INT nnz, cplx *Bred, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b);


void GMRES_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, cplx *Fred, MKL_INT m_A, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, MailleSphere &MSph, MKL_INT total_iter, char chaine[]);


void GMRES_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, VecN &Fred, MKL_INT m_A, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, VecN &U, A_skyline &A_glob, A_skyline &M_glob, A_skyline &P_red, VecN &Uex, VecN &Ufull, MailleSphere &MSph, MKL_INT total_iter, char chaine[]);


void TEST_Sparse_COO_recuded_to_SVD_max(MailleSphere &MSph, const char chaine[]);


void TEST_ASSEMBLAGE_DE_LA_Reciprocite_reduite(MailleSphere &MSph);


void Step_to_Solution_With_SVD_and_oldMethod(MailleSphere &MSph, const char chaine[]) ;



/*-------------------------------------------------------------------------------------------------------------*/
void ASSEMBLAGE_DE_LA_Reciprocite(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


MKL_INT compute_nnz_for_Sparse_COO_format(A_skylineRed &A_red, MailleSphere &MSph);


void transforme_A_skyline_to_Sparse_COO_format(A_skylineRed &A_red, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


MKL_INT compute_nnz_for_Sparse_COO_format(A_skyline &A_glob, MailleSphere &MSph);


void transforme_A_skyline_to_Sparse_COO_format(A_skyline &A_glob, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void TEST_ASSEMBLAGE_DE_LA_Reciprocite_reduite_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


void BuildArealMetricMatrix(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildArealMetricMatrix_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Debugg_Projection(MailleSphere &MSph, const char chaine[]);


cplx integrale_tetra(Element &E, MKL_INT iloc, MKL_INT jloc);


void debugg_integral_tetra(const char file_name[], MailleSphere &MSph);


void BuildVolumeMetricMatrix(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);

void BuildVolumeMetricMatrix_numerical_integ_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildArealMetricMatrix(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_parallel_version(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph, const char chaine[]);


void copy_transpose(MKL_INT m_x, MKL_INT n_x, cplx *X, MKL_INT m_y, MKL_INT n_y, cplx *Y) ;


void compute_A_red_ColMajor(MKL_INT e, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP);


void compute_A_red_ColMajor(MKL_INT e, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P);


void compute_A_red_ColMajor(MKL_INT e, MKL_INT taille, A__skyline &Msky, cplx *Msky_mem, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP);


void compute_F_red_ColMajor(MKL_INT e, cplx *B, cplx *B_red, MailleSphere &MSph, cplx *adjtP) ;


void compute_F_red_ColMajor__(MKL_INT e, cplx *B, cplx *B_red, MailleSphere &MSph, cplx *P);


void reverseTo_originalVector_ColMajor(MKL_INT e, cplx *x, cplx *x_red, MailleSphere &MSph, cplx *P);


void collection_Solutions_locales_Volume_selfDecompose(VecN &U, A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);

void debugg_Volume_Projection(MailleSphere &MSph, const char chaine[]);


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A__skyline &Psky, cplx *Psky_mem,  MailleSphere &MSph) ;


void collection_Solutions_locales_Volume_selfDecompose(VecN &U, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph);


void DecompositionDeBloc_ET_Normalisation_Sans_Troncature(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w);


void DecompositionGlobaleDesBlocs_ET_Normalisation_Sans_Troncature(A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph);


void produit_A_skyline_P_skyline_Sans_Troncature(A_skyline &A_glob, A_skyline &P_glob, A_skyline &R_glob, MailleSphere &Msph);


void produit_P_skyline_RightHandSide_Sans_Troncature(A_skyline &P_glob, VecN &B_old, VecN &B_new, MailleSphere &MSph);


void produit_P_skyline_Solution_Sans_Troncature(A_skyline &P_glob, VecN &X_new, VecN &X_old, MailleSphere &MSph) ;

void Courbes_De_Convergence_Avec_La_Nouvelle_Methode_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


void Courbes_Convergences_Avec_La_Nouvelle_Methode_Combinee_Avec_Une_MetriqueSurfacique_ET_DU_Filtrage_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


double norme_L2_volumique(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique(VecN &X, MailleSphere &MSph, const char chaine[]) ;

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2_volumique(double *r, VecN &X, MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique_Vitesse(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique_Vitesse(VecN &X, MailleSphere &MSph, const char chaine[]) ;

//initialiser *r = 0.0 en dehors de la fonction hors region parallele
void norme_L2_volumique_Vitesse(double *r, VecN &X, MailleSphere &MSph, const char chaine[]) ;


__float128 compute_derivative(__float128 h);


double compute_derivative(double h);

#ifdef USE_QUADMATH
__complex128 compute_derivative(__complex128 h);
__complex128 compute_quad(__float128 h);
#endif

cplx compute_double(double h);
cplx compute_derivative(cplx h);


void TEST_quadmath();


void TEST_quadmath_complex();

__float128 dot_u(MKL_INT n, __float128 *u, __float128 *v);

void TEST_dot_product_quadmath();

cplx integrale_tetra_quad(Element &E, MKL_INT iloc, MKL_INT jloc);


cplx integrale_tetra_quad(Element &E, Onde_plane &P1, Onde_plane &P2);


void collection_Solutions_locales_Areal_selfDecompose(VecN &U, A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);

void collection_Solutions_locales_Areal_selfDecompose(VecN &U, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph) ;


void Courbes_Convergences_Avec_La_Nouvelle_Methode_Combinee_Avec_Une_MetriqueVolumique_ET_DU_Filtrage_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


cplx integrale_tetra_quad_precision(Element &E, MKL_INT iloc, MKL_INT jloc);

cplx integrale_tetra_quad_precision(Element &E, Onde_plane &P1, Onde_plane &P2);

cplx integrale_tetra(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]);


cplx integrale_tetra(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]);


cplx integrale_tetra_VV(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]);

cplx integrale_tetra_VV(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]);

#ifdef USE_QUADMATH
__complex128 eval_cexpq(__float128 x);
#endif
cplx integrale_triangle_quad_precision(Face &F, Onde_plane &P1, Onde_plane &P2);

cplx integrale_triangle(Face &F, Onde_plane &P1, Onde_plane &P2, const char chaine[]);

void debugg_integral_triangle(MailleSphere &MSph);


void Get_New_Normalised_Trefftz_Basis(A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void compute_sizeOf_reducedVector(MailleSphere &MSph);


void debugg_Decomposition_to_build_New_Normalised_Trefftz_Basis(MailleSphere &MSph, const char chaine[]);


void collection_Solutions_locales_Areal_selfDecompose_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph);


void TEST_ASSEMBLAGE_OF_THE_TWO_VERSION(MailleSphere &MSph, const char chaine[]);

void TEST_ASSEMBLAGE_OF_THE_TWO_RHS_VECTOR(MailleSphere &MSph, const char chaine[]);


void collection_Solutions_locales_Volume_selfDecompose_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) ;

void TEST_ASSEMBLAGE_OF_THE_TWO_VERSION_OF_REDUCED_MATRICES(MailleSphere &MSph, const char chaine[]);


void TEST_ASSEMBLAGE_OF_THE_TWO_REDUCED_RHS_VECTOR(MailleSphere &MSph, const char chaine[]);


void TEST_SOLUTION_PROJECTEE(MKL_INT filtre, MailleSphere &MSph);


void BuildVector_Of_Volume_Projection(cplx *B, MailleSphere &MSph);


void BuildVector_Of_Volume_Projection_parallel_version(cplx *B, MailleSphere &MSph);


void BuildVector_Of_Volume_Projection_incidentPlaneWave_parallel_version(cplx *B, MailleSphere &MSph, const char chaine[]);


void BuildVector_Of_Areal_Projection(cplx *B, MailleSphere &MSph);


void BuildVector_Of_Areal_Projection_parallel_version(cplx *B, MailleSphere &MSph);


void collection_Solutions_locales_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph);


void compute_max_and_min_of_all_blocks_eigenvalues(VecN &LAMBDA, MailleSphere &MSph);


void test_blas_matmat_product();


void debugg_max_and_min_of_all_blocks_eigenvalues(MailleSphere &MSph, const char chaine[]);



void combinaison_solution(Element &E, MKL_INT ndof, cplx *alpha_loc, double *X);


void combinaison_solution_en_pression(Element &E, MKL_INT ndof, cplx *alpha_loc, double *X);


void debugg_combinaison_solution(MailleSphere &MSph,  const char chaine[]);


double norme_L2_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(MKL_INT e, MailleSphere &MSph);


double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(MailleSphere &MSph);


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MKL_INT e, MailleSphere &MSph);


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MailleSphere &MSph);


double norme_inf_erreur(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double norme_inf_erreur(cplx *U, MailleSphere &MSph);


double norme_inf_Bessel(MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph);

double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_un_element(MKL_INT e, MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique(MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, MailleSphere &MSph) ;


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_un_element(MKL_INT e, MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique(MailleSphere &MSph);


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, MailleSphere &MSph);

double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MailleSphere &MSph);


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph);


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) ;


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) ;


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MailleSphere &MSph) ;


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MailleSphere &MSph) ;


void debugg_integrale_numerique(MailleSphere &MSph);


void Resolution_Par_Methode_Cessenat_Despres_Avec_Une_MetriqueVolumique_ET_DU_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, char solveur[], char name_dir[], const char chaine[]);


void Resolution_Par_Methode_Cessenat_Despres_Avec_Une_MetriqueSurfacique_ET_DU_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, char solveur[], char name_dir[], const char chaine[]);

void Resolution_Sans_Filtrage_DE_La_Nouvelle_Methode_Avec_Une_MetriqueVolumique_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


void Resolution_Sans_Filtrage_DE_La_Nouvelle_Methode_Avec_Une_MetriqueSurfacique_Vu_Par_Les_Classes(MailleSphere &MSph, const char chaine[]);


void exporte_critere_filtrage_tetra_to_file(MailleSphere &MSph);


MKL_INT Read_number_of_elements(char chaine[]);


void Read_Points(char chaine[], MKL_INT np, double *P);


void Read_Tetra(char chaine[], MKL_INT ne, MKL_INT *T);


void debug_function_for_reading_mesh(MailleSphere &MSph);


void computeSizeDecomposedNewTracesMatrix(cplx *R_bloc, cplx *M_bloc, cplx *z, double *w, cplx *q, MKL_INT *nm, MKL_INT *np, cplx *ab, cplx *bb, MKL_INT *ifail, MailleSphere &MSph);


void prodAtransB_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);

void debugg_Reciprocite_assemblage(MailleSphere &MSph, const char chaine[]);


void DecompositionDeBloc_ET_Normalisation_dans_ordre(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, A_skyline &Q_red, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w);

void DecompositionGlobaleDesBlocs_ET_Normalisation_dans_ordre(A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph);

void compute_BstarAB(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *AB, cplx *BstarAB);

void compute_BstarAB(cplx *C, MKL_INT m_C, MKL_INT n_C, cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *AB, cplx *CstarAB);

void ASSEMBLAGE_DE_LA_Reciprocite(A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, MailleSphere &MSph, const char chaine[]);


void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_MetriqueVolumique_ET_DU_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_MetriqueSurfacique_ET_DU_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


void Courbes_Convergences_Avec_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, char methode[], char metrique[], MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


MKL_INT compute_nnz_for_Sparse_COO_format(A_skylineRed &A_red, MailleSphere &MSph);


void transforme_A_skyline_to_Sparse_COO_format(A_skylineRed &A_red, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);

void GMRES_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, VecN &Fred, MKL_INT m_A, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, VecN &U, A_skyline &A_glob, A_skyline &M_L2, A_skyline &M_UWVF, A_skyline &P_red, VecN &Uex, VecN &Ufull, MailleSphere &MSph, MKL_INT total_iter, char chaine[]);


void Resolution_Par_Methode_Cessenat_Despres_Sans_Projection_ET_Sans_Filtrage_Vu_Par_Les_Classes(MailleSphere &MSph, char solveur[], char name_dir[], const char chaine[]);

void debugg_virtual_element(MailleSphere &MSph);


void buildVolumeMetrique(MKL_INT ndof, cplx *M, Element &E, const char chaine[]);

void buildArealMetrique(MKL_INT ndof, cplx *M, Element &E, const char chaine[]);


void compute_sizeBlocRed(MKL_INT ndof, cplx *M, cplx *AA, double *w, Element &E) ;

MKL_INT VerifyDiagonalization(MKL_INT ndof, cplx *A_loc, cplx *AA, double *w);


void Definition_des_nouvelles_dimensions_des_bases_Trefftz_par_element_virtuel(cplx *M, MailleSphere &MSph);

void DecompositionDeBloc_ET_Normalisation(MKL_INT ndof, cplx *M, cplx *PinvSigma, double *LAMBDA, cplx *QinvSigma, cplx *QSigma,  double *Sigma, Element &E, cplx *AA, double *w) ;


void produit_Adjoint_P_virtual_A_skyline_P_virtual(A_skyline &A_glob, cplx *P_red, A_skylineRed &A_red, MailleSphere &MSph) ;


void produit_Adjoint_P_virtual_Vector(cplx *P_red, VecN &B_red, VecN &B, MailleSphere &MSph);


void produit_P_virtual_Vector_red(cplx *P_red, VecN &X_red, VecN &X, MailleSphere &MSph) ;


void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A_skyline &A_glob, cplx *PSigma, cplx *QSigma, MailleSphere &MSph, const char chaine[]);

void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_Metrique_D1_ElementVirtuel_ET_DU_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


void Hankel3D(double k, double z, cplx *y, cplx *derr);

void Hankel3D(double k, double z, cplx *y);


void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_Metrique_D1_ElementVirtuel_Sans_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);

void transforme_A_skyline_to_Sparse_CSR_new_version(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);

void transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);

void Resolution_Par_Methode_Cessenat_Despres_Sans_Projection_ET_DU_Filtrage_Par_1_Element_Virtuel_Vu_Par_Les_Classes(MailleSphere &MSph, char solveur[], MKL_INT filtre, char name_dir[], const char chaine[]);

void buildVolumeMetrique(MKL_INT ndof, cplx *M, MailleSphere &MSph, const char chaine[]);

void debugg_integrale_tetraedre(MailleSphere &MSph);

void spy_to_python(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_Metrique_Regulee_Par_Tikhonov_Sans_Filtrage_Sans_Projection_Vu_Par_Les_Classes(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);

void DecompositionDeBloc_ET_Normalisation_TSVD_version(MKL_INT ndof, cplx *M, cplx *P, double *LAMBDA, Element &E, cplx *AA, double *w) ;


void computeSizeDecomposedNewTracesMatrix(MKL_INT Nred, cplx *B, cplx *P, double *LAMBDA, MKL_INT *nm, MKL_INT *np);

MKL_INT DecomposeNewTracesMatrix_TSVD_version(MKL_INT Nred, MKL_INT n, cplx *B, cplx *P, double *LAMBDA, MKL_INT nm, cplx betam, cplx *Bm, cplx *Pm, double *LAMBDAm, cplx *AdjtPm, MKL_INT np, cplx betap, cplx *Bp, cplx *Pp, double *LAMBDAp, cplx *AdjtPp, cplx *G);

void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A_skylineRed &A_red, cplx *P_red, MailleSphere &MSph, const char chaine[]);

void Resolution_Par_Decomposition_Spectrale_Avec_Filtrage_Par_Une_Metrique_D1_ElementVirtuel(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);

void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_Metrique_Regulee_Par_Tikhonov_ET_Filtree_Par_Metrique_Virtuelle(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, cplx *M, cplx *PinvSigma, cplx *LAMBDA, cplx *QinvSigma, cplx *QSigma,  cplx *Sigma, MailleSphere &MSph, cplx *AA, double *w) ;


void Resolution_Par_Decomposition_Spectrale_Avec_Preconditionnement_Par_Une_Metrique_Regulee_Par_Tikhonov_ET_Filtree_meme_Metrique(MailleSphere &MSph, MKL_INT filtre, char solveur[], char name_dir[], const char chaine[]);

void Resolution_Par_Decomposition_Spectrale_Sans_Regulation(MailleSphere &MSph, const char chaine[]);


void DecompositionGlobaleDesBlocs_ET_Normalisation(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, A_skyline &QinvSigma, A_skyline &QSigma, VecN &Sigma, MailleSphere &MSph);


void Solve_with_COMPOSE(A_skyline &A_glob, MKL_INT n, cplx *B, cplx *U, MailleSphere & sph);

void spy_to_python(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void swap(MKL_INT *arr, MKL_INT i, MKL_INT j) ;

MKL_INT partition(double *Z_XG, MKL_INT *arr, MKL_INT low, MKL_INT high) ;

void quicksort(double *Z_XG, MKL_INT *arr, MKL_INT low, MKL_INT high) ;

void teste_algo_tri_recursive(MailleSphere &MSph);


void copy_reduced_Trefftz_basis(MailleSphere &MSph, MKL_INT *tab);


void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, MKL_INT ndof, MKL_INT N_elements, MKL_INT *NRED) ;

MKL_INT local_to_globRed(MKL_INT iloc, MKL_INT tetra, MKL_INT *NRED);


MKL_INT compute_nnz_for_Sparse_format(A_skyline &A_glob, MKL_INT ndof, MKL_INT N_elements, MKL_INT *voisins);


void transforme_A_skyline_to_Sparse_COO_format(A_skyline &A_glob, MKL_INT ndof, MKL_INT N_elements, MKL_INT *voisins, MKL_INT *rows, MKL_INT *cols, cplx *values);


void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MKL_INT ndof, MKL_INT N_elements, MKL_INT *voisins);

MKL_INT compute_nnz_for_Sparse_format(A_skylineRed &A_red, MKL_INT N_elements, MKL_INT *voisins, MKL_INT *NRED);

void transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, MKL_INT nnz, MKL_INT *ia, MKL_INT *ja, cplx *a, MKL_INT ndof, MKL_INT N_elements, MKL_INT *voisins, MKL_INT *NRED);

void compute_CSR_solution(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *b, cplx *x, const char path_to_file[]) ;

void Solve_with_COMPOSE_COO(A_skyline &A_glob, MKL_INT n, cplx *B, cplx *U, MKL_INT ndof, MKL_INT N_elements, MKL_INT *voisins, cplx *A_full, A_Block *array_A_loc);


void get_mesh_size_and_Trefftz_basis_dimension( char chaine_tetra[],  char type_espace_phases[], char phase_tag[], MKL_INT *N_elements, MKL_INT *ndof);

void Assembly_and_write_in_file_A_skyline_and_right_hand_side_of_Cessenat_method(char *argv[], const char precision[]);

void Assembly_and_write_in_file_A_skyline_and_right_hand_side_of_spectral_method_regulated_with_Tikhonov(char *argv[], const char precision[]);


void write_Sparse_CSR_matrix_in_file(char *path, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a);

void read_size_of_Sparse_CSR_matrix_from_file(char path_vals_cols[], char path_rows[], MKL_INT *nnz, MKL_INT *size_cols);

void read_Sparse_CSR_matrix_from_file(char path_vals_cols[], char path_rows[], MKL_INT nz, MKL_INT size_cols, MKL_INT *ia, MKL_INT *ja, cplx *a);

void write_Sparse_COO_matrix_in_file(char *path, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values);

void write_Sparse_COO_matrix_in_file(char *path, MKL_INT nnz, MKL_INT n, MKL_INT *rows, MKL_INT *cols, cplx *values);

void read_size_of_Sparse_COO_matrix_from_file(char path[], MKL_INT *nnz);


void read_size_of_Sparse_COO_matrix_from_file(char path[], MKL_INT *nnz, MKL_INT *n);


void read_Sparse_COO_matrix_from_file(char path[], MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values);

void read_Sparse_COO_matrix_from_file(char path[], MKL_INT nnz, MKL_INT n, MKL_INT *rows, MKL_INT *cols, cplx *values);

void write_Vector_in_file(char *path, MKL_INT n, cplx *U) ;

void read_Vector_from_file(char path[], cplx *U);

void read_size(char path[], MKL_INT *n);

void Solve_with_COMPOSE_COO(char path[], MKL_INT n, cplx *B, cplx *U);

void Assembly_of_A_skyline_with_Cessenat_method_and_write_in_file(char *argv[], const char precision[]);

void Solve_full_linear_system(char *argv[]) ;

void Assembly_of_A_skyline_with_spectrale_method_without_regulation_and_write_in_file(char *argv[], const char precision[]);

void Assembly_of_Spectral_method_regulated_with_Tikhonov_variable_parameter_and_write_in_file(char *argv[], const char precision[]);

void compute_Right_Hand_Side_and_Solve_full_linear_system(char *argv[]);

void Assembly_of_Reduced_A_skyline_with_spectral_method_regulated_with_VirtualElement_and_write_in_file(char *argv[], const char precision[]);

void Solve_reduced_linear_system_regulated_with_VirtualElement(char *argv[], const char precision[]);
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
void get_mesh_size_and_Trefftz_basis_dimension(char chaine_points[], char chaine_tetra[],  char type_espace_phases[], char phase_tag[], MKL_INT *np, MKL_INT *N_elements, MKL_INT *ndof);

void build_mesh(char chaine_points[], char chaine_tetra[], char chaine_voisins[], double *points, MKL_INT *tetra, MKL_INT *voisin);

void build_direction_of_space_phases(char type_espace_phases[], char phase_tag[], MKL_INT ndof, double *Dir);

void debugg_mesh(char *argv[], MailleSphere &MSph);


void quad_Gauss_1D(MKL_INT n_gL, double *X1d, double *W1d) ;
/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
void Assembly_and_write_in_file_A_skyline_and_right_hand_side_of_Cessenat_method_LU_and_COMPOSE_SPARSE(char *argv[], const char precision[]);

void Solve_full_linear_system_LU_and_COMPOSE_SPARSE(char *argv[]) ;


void produit_MatSparse_CSR_vecteur(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *vec, cplx *res);


void correcte_the_vectors_of_Krylov( MKL_INT nkry, MKL_INT m_A, MKL_INT j, double *t, cplx **z_j, cplx *z, cplx *v, cplx **Kn, cplx *H);


void Solveur_GMRES_with_CSR_product(double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT m_A, cplx *Bred, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b) ;

void GMRES_With_Restart_CSR_product(MKL_INT *iter_ext, MKL_INT nkry, cplx *Fred, MKL_INT m_A, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *U, MKL_INT total_iter, char chaine[]);

void GMRES_With_Restart_CSR_product_writing_Solutions_on_file(MKL_INT *iter_ext, MKL_INT nkry, cplx *Fred, MKL_INT m_A, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *U, MKL_INT total_iter, char chaine[]);

void TEST_different_version_GMRES_Solvers(MailleSphere &MSph, const char precision[]);

void Solve_with_COMPOSE_COO(char path[], MKL_INT n, cplx *B, cplx *U, MKL_INT max_iter, double *temps);

void Solve_with_COMPOSE_COO(char path[], MKL_INT n, cplx *B, cplx *U, MKL_INT max_iter, double tol, double *temps);

void Solve_with_COMPOSE_COO(char path[], char chaine_residual[], char sortie_standard[], MKL_INT n, cplx *B, cplx *U, MKL_INT max_iter, double tol, double *temps);

void Solve_with_modified_COMPOSE_COO(char path[], MKL_INT n, cplx *B, cplx *U, MKL_INT max_iter);

void Solve_with_modified_COMPOSE_COO_and_write_residual(char path[], MKL_INT n, cplx *B, cplx *U, MKL_INT max_iter, double tol);

void TEST_norme(char *argv[]);

void store_P_and_transpose_P(cplx *P_red, cplx *adjtP_red, MailleSphere &MSph) ;

void collection_Solutions_locales_parallel_version(VecN &U, A_skyline &M_glob, cplx *P_red, MailleSphere &MSph);

void collection_Solutions_locales_parallel_version(VecN &U, VecN &U_red, A_skyline &M_glob, cplx *P_red, MailleSphere &MSph);

void Testing_projection(char *argv[], const char precision[]);


void Convergence_of_Cessenat_Despres_methode_with_LU_and_COMPOSE_SPARSE_With_projected_Solution(char *argv[], const char precision[]) ;
 

MKL_INT compute_nnz(A_skyline &A_glob, Maillage &Mesh);


void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, Maillage &Mesh) ;

void TEST_spblas();

void transforme_A_skyline_to_Sparse_COO_format(A_skyline &A_glob, MKL_INT *rows, MKL_INT *cols, cplx *values, Maillage &Mesh);

void compute_nnz_for_Sparse_BSR(A_skyline &A_glob, Maillage &Mesh, MKL_INT *nnzb, MKL_INT *nnz);


MKL_INT is_zero_block(cplx *block, MKL_INT n1, MKL_INT n2);


void transforme_A_skyline_to_Sparse_BSR(A_skyline &A_glob, MKL_INT total_nnz, MKL_INT *rowPtr, MKL_INT *columns, cplx *values, Maillage &Mesh);

void spy_to_python(A_skyline &A_glob, Maillage &Mesh, const char chaine[]);

void combinaison_solution_projection(Element &E, MKL_INT ndof, cplx *alpha_loc, double *X);

double norme_L2_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, cplx *v_loc, MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, cplx *V, MailleSphere &MSph) ;


double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, cplx *v_loc, MailleSphere &MSph);


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, cplx *V, MailleSphere &MSph) ;

void compute_nnz_for_Sparse_BSR(A_skyline &A_glob, MailleSphere &MSph, MKL_INT *nnzb, MKL_INT *nnz);

void transforme_A_skyline_to_Sparse_BSR(A_skyline &A_glob, MKL_INT total_nnz, MKL_INT *rowPtr, MKL_INT *columns, cplx *values, MailleSphere &MSph);

#ifdef USE_MKL
void TEST_spblas(char *argv[], const char precision[]);


void preprocessing_of_SparseBSR_MV_product(struct matrix_descr *descrA, sparse_matrix_t *bsrA, MKL_INT N_elements, MKL_INT block_size, MKL_INT NRHS, MKL_INT *rowPtr, MKL_INT *columns, cplx *values);

void SparseBSR_MV_product(cplx alpha, struct matrix_descr descrA, sparse_matrix_t bsrA, MKL_INT NRHS, cplx *B, cplx beta, cplx *X);

void destroy_SparseBSR_Matrix(sparse_matrix_t *bsrA);
#endif

void Solveur_GMRES(double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT m_A, cplx *Bred, struct matrix_descr descrA, sparse_matrix_t bsrA, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b) ;

void compute_residual(double *tAU, double *tV, MKL_INT m, struct matrix_descr descrA, sparse_matrix_t bsrA, cplx *U, cplx *F, cplx *Fdelta, cplx *AU) ;

void GMRES_With_Restart(MKL_INT *iter_ext, MKL_INT nkry, cplx *Fred, MKL_INT m_A, struct matrix_descr descrA, sparse_matrix_t bsrA, cplx *U, MailleSphere &MSph, MKL_INT total_iter, char chaine[]);


void Solve_full_linear_system_LU_and_COMPOSE_SPARSE(char *argv[], MKL_INT iter_max) ;
 
void Cessenat_Despres_methode_Compare_residual_COMPOSE_With_Projected_Solution(char *argv[], MKL_INT iter_max, const char precision[]) ;


double norme_L2_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph);

double  norme_L2_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph);

void Solve_full_linear_and_compare_all_norms_with_LU_and_COMPOSE(char *argv[], MKL_INT iter_max, const char precision[]) ;

void Compute_conditioning_of_global_MetricMatrix(char *argv[], const char chaine[]);

void Assembly_of_Reduced_A_skyline_and_Right_Hand_Side_with_Cessenat_Method_regulated_with_VirtualElement_and_write_in_file(char *argv[], const char precision[]);

void Solve_reduced_linear_system_associated_to_VirtualElement(char *argv[], MKL_INT iter_max, const char precision[]);

void Solve_full_linear_and_compare_all_norms_with_LU_and_COMPOSE_II(char *argv[], MKL_INT iter_max, const char precision[]) ;

cplx integrale_UWVF_PP(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_UWVF_VV(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);

cplx integrale_UWVF_PV(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);


cplx integrale_UWVF_VP(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]);


void Build_UWVF_MetricMatrix(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Build_UWVF_MetricMatrix_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Assembly_Precond_linear_system_Cessenat_Method_regulated_and_write_in_file(char *argv[], const char precision[]);


void Solve_Precond_linear_system_of_Cessenat(char *argv[], MKL_INT iter_max, const char precision[]) ;

void Solve_Precond_linear_system_of_Cessenat_comparing_allnorms(char *argv[], MKL_INT iter_max, const char precision[]);


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w) ;


void DecompositionGlobaleDesBlocs_ET_Normalisation(A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void Test_Assemblage_of_reduced_matrix(char *argv[], const char precision[]);


void ASSEMBLAGE_DE_Askyline_parallel_version(A_skylineRed &A_red, A_skyline &P_red, MailleSphere &MSph, const char chaine[]);

void Assembly_optimized_version_of_Precond_linear_system_Cessenat_Method_and_write_in_file(char *argv[], const char precision[]);

void Solve_optimized_version_of_Precond_linear_system_of_Cessenat(char *argv[], MKL_INT iter_max, const char precision[]) ;


void Assembly_optimized_version_of_Spectral_Methode_regulated_with_VirtualElement_and_write_in_file(char *argv[], const char precision[]);


void Solve_optimized_version_of_Spectral_Methode_regulated_with_VirtualElement(char *argv[], MKL_INT iter_max, const char precision[]) ;


void Compute_local_conditioning_of_Cessenat_Despres_method(char *argv[], const char chaine[]);


void Compute_local_conditioning_of_Spectral_method_regulated_with_Tikhonov(char *argv[], const char chaine[]);

void Compute_local_conditioning_of_Spectral_method_regulated_with_VirtualElement(char *argv[], const char precision[]);

void Distribute_and_Solve_full_linear_system(char *argv[]) ;


void compute_error_norms(char *argv[], const char precision[]);


void Distribute_mesh_to_processes(MKL_INT nb_procs, MKL_INT rang, MKL_INT N_elements, MKL_INT *n_elem_loc, MKL_INT *counts_elem, MKL_INT *displs_elem);

MPI_Datatype create_mpi_complex_type();


void TEST_distribution_of_mesh_to_processes(char *argv[]);


void ASSEMBLAGE_DE_Askyline(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);

void ASSEMBLAGE_DE_Askyline_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);

void ASSEMBLAGE_DU_VecteurRHS(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph);


void ASSEMBLAGE_DU_VecteurRHS_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph);


void compare_A_glob(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob_proc, A_skyline &Big_A, MailleSphere &MSph);


void compare_RHS(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B_proc, cplx *Big_B, MKL_INT ndof);


void compare_RHS_global(MKL_INT rang, MKL_INT n, cplx *U, cplx *V);

void Build_UWVF_MetricMatrix(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void Build_UWVF_MetricMatrix_and_neighboring_interactions(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);

void Build_UWVF_MetricMatrix_and_neighboring_interactions_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void compare_diagA_glob(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob_proc, A_skyline &Big_A, MailleSphere &MSph);


void compare_diagA_glob(MKL_INT rang, A_skyline &A_glob, A_skyline &B_glob, MailleSphere &MSph);


void compare_UWVF_MetricMatrix_and_neighboring_interactions(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob_proc, A_skyline &Big_A, MailleSphere &MSph);


void compute_sizeBlocRed(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT l, MKL_INT e, cplx *AA, double *w, A_skyline &A_glob, MailleSphere &MSph);


MKL_INT compute_sizeBlocRed(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &A_glob, MailleSphere &MSph) ;

MKL_INT compute_sizeBlocRed_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &A_glob, MailleSphere &MSph) ;

MKL_INT store_sizeBlocRed_in_cumuled_order(MKL_INT n_elem_loc, MKL_INT *local_cumul_Nred, MailleSphere &MSph);

MKL_INT store_sizeBlocRed_in_cumuled_order(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, MailleSphere &MSph);


MKL_INT compute_sizeForA_red(MKL_INT rang, int *counts_elem, int *displs_elem, MailleSphere &MSph);


void get_F_elemRed(MKL_INT l, cplx **B, cplx *B_red, MKL_INT *local_cumul_Nred) ;


void DecompositionDeBloc_ET_Normalisation(MKL_INT l, MKL_INT e, MKL_INT *local_cumul_Nred, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w) ;


void DecompositionDeBloc_ET_Normalisation_extra_bloc(MKL_INT l, MKL_INT j, MKL_INT f, A_skyline &P_red, MailleSphere &MSph, cplx *AA, double *w) ;


void DecompositionGlobaleDesBlocs_ET_Normalisation(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void DecompositionGlobaleDesBlocs_ET_Normalisation_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph);


void compare_Decomposition(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_glob_proc, VecN &LAMBDA_proc, A_skyline &Big_P, VecN &SIGMA, MailleSphere &MSph);

void produit_A_skyline_P_skyline_global(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, A_skyline &P_red, A_skylineRed &A_red, MailleSphere &Msph) ;

void ASSEMBLAGE_DE_Askyline_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red, A_skyline &P_red, MailleSphere &MSph, const char chaine[]);


void produit_P_skyline_global_RightHandSide(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_red, cplx *B_red, cplx *B, MailleSphere &MSph);


void produit_P_skyline_RightHandSide(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_red, cplx *B_red, cplx *B, MailleSphere &MSph);


void produit_P_skyline_Reduced_Vector(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &P_red, cplx *X_red, cplx *X, MailleSphere &MSph);


void compare_Vector_Red(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, cplx *B_red_proc, cplx *Big_B_red, MailleSphere &MSph);


void ASSEMBLAGE_DE_Askyline_avec_P_skyline_global_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red, A_skyline &P_red, MailleSphere &MSph, const char chaine[]);


void compare_A_red(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red_proc, A_skylineRed &Big_A_red, MailleSphere &MSph);

MKL_INT compute_nnz_for_Sparse_COO_format(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red, MailleSphere &MSph);

void transforme_A_skyline_to_Sparse_COO_format(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);

void compare_Sparse_COO_matrices(MKL_INT rang, int *counts_nnz, int *displs_nnz, MKL_INT *rows_loc, MKL_INT *cols_loc, cplx *values_loc, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void transforme_A_skyline_to_Sparse_COO_format(MKL_INT rang, int *counts_elem, int *displs_elem, A_skylineRed &A_red, std::vector<int>& rows, std::vector<int>& cols, std::vector<cplx>& values, MailleSphere &MSph);


void compare_Sparse_COO_matrices(MKL_INT rang, int *counts_nnz, int *displs_nnz, std::vector<int>& rows_loc, std::vector<int>& cols_loc, std::vector<cplx>& values_loc, MKL_INT *rows, MKL_INT *cols, cplx *values, MailleSphere &MSph);


void Define_RHS_for_COMPOSE(MKL_INT rang, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, cplx *B_red_proc, std::vector<int>& rhs_i, std::vector<cplx>& rhs_v, MailleSphere &MSph);

void Define_Index_for_RHS(MKL_INT rang, int *counts_elem, int *displs_elem, std::vector<int> rhs_i, MailleSphere &MSph);

void Assembly_and_Solve_linear_system_of_Cessenat_Despres_Distributed_version(char *argv[]);


void compare_RHS_COMPOSE_to_myRHS(MKL_INT rang, MKL_INT n_B, cplx *B_red_proc, std::vector<cplx>& rhs_v, MailleSphere &MSph);

void TEST_Solver_in_Distribute();

MKL_INT local_to_globRed_proc(MKL_INT iloc, MKL_INT tetra, MKL_INT *local_cumul_Nred);


void compute_full_error_norms(char *argv[], const char precision[]);


double norme_H1_volumique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) ;


double  norme_H1_erreur_volumique_avec_integ_numerique(cplx *U, MailleSphere &MSph) ;


double norme_H1_erreur_surfacique_avec_integ_numerique(cplx *U, MailleSphere &MSph) ;


double norme_H1_surfacique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) ;


double norme_H1_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph);


double  norme_H1_erreur_volumique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) ;

double norme_H1_erreur_surfacique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph);


double norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph);


void debugg_integrale_numerique_H1(char *argv[], const char precision[]);



void Assembly_and_Solve_Spectral_Method_Distributed_version(char *argv[]);


void debugg_ASSEMBLAGE_with_MPI_routines(int rank, int nb_procs, MKL_INT N_elements, MKL_INT sizeR_local, cplx *R_local, MKL_INT size_R_glob, cplx *tab_R_glob, MailleSphere &MSph);


void debugg_new_RHS_with_MPI_routines(int rank, int nb_procs, MKL_INT N_elements, MKL_INT size_new_B_loc, cplx *new_B_loc, MKL_INT size_new_B_glob, cplx *new_B_glob);


void debugg_Sparse_COO_matrices_with_MPI_routines(int rank, int nb_procs, MKL_INT compose_nnz, std::vector<int> &compose_i, std::vector<int> &compose_j, std::vector<cplx> &compose_v, MKL_INT nnz_glob, MKL_INT *rows_glob, MKL_INT *cols_glob, cplx *values_glob);

void collect_Sparse_COO_matrices_in_one_process(int rank, int nb_procs, MKL_INT compose_nnz, std::vector<int> &compose_i, std::vector<int> &compose_j, std::vector<cplx> &compose_v, MKL_INT nnz_glob, MKL_INT *rows_glob, MKL_INT *cols_glob, cplx *values_glob);

void TEST_COMPOSE_prodMV();

void DecompositionDeBloc_ET_Normalisation_avec_un_regulateur_du_type_Tikhonov_virtuel(MKL_INT ndof, cplx *M, cplx *P, cplx *Q, double *LAMBDA, Element &E, cplx *AA, double *w) ;

void ASSEMBLAGE_DE_LA_Reciprocite_avec_un_regulateur_du_type_Tikhonov_virtuel(A_skyline &A_glob, cplx *P, cplx *Q, MailleSphere &MSph, const char chaine[]);

void ASSEMBLAGE_DE_LA_Reciprocite_avec_un_regulateur_du_type_Tikhonov_virtuel_parallel_version(A_skyline &A_glob, cplx *P, cplx *Q, MailleSphere &MSph, const char chaine[]);


void debugg_Reciprocite_matrix(char *argv[], const char precision[]);


void Assembly_optimized_version_of_Spectral_Method_Regulated_with_Tikhonov_VirtualElement_and_Filtered_with_Variable_Metric(char *argv[], const char precision[]);


void Solve_optimized_version_of_Spectral_Methode_Regulated_with_Tikhonov_VirtualElement_and_Filtered_with_Variable_Metric(char *argv[], MKL_INT iter_max, const char precision[]) ;


double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph) ;


double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph);


double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph);


double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph);


double norme_H1_du_saut_de_l_erreur_avec_integ_numerique(cplx *U, MailleSphere &MSph) ;


void Nouvelle_Technique_d_Assemblage_de_la_reciprocite(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void check_faces_of_mesh(char *argv[]);

void debugg_Nouvelle_technique_d_assemblage_de_la_reciprocite(char *argv[], const char precision[]);


cplx integrale_PincP_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]) ;


void Const_vector_SourceOndePlane_PincP(VecN &B, MailleSphere &MSph, const char chaine[]) ;


cplx integrale_VincV_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


void Const_vector_SourceOndePlane_VincV(VecN &B, MailleSphere &MSph, const char chaine[]) ;

void Const_vector_SourceOndePlane_PincP_VincV(VecN &B, MailleSphere &MSph, const char chaine[]) ;

cplx integrale_PincV_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);

cplx integrale_VincP_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DU_VecteurRHS_SourceOndePlane(cplx *B, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DU_VecteurRHS_SourceOndePlane_parallel_version(cplx *B, MailleSphere &MSph, const char chaine[]);


void Nouvelle_Technique_d_Assemblage_du_RHS_avec_une_onde_plane_incidente(cplx *B, MailleSphere &MSph);

void construction_de_la_projection_surfacique_de_la_solution_analytique(VecN &PUex, A_skyline &M_glob, A_skyline &P_glob, MailleSphere &MSph);


void Nouvelle_Technique_d_Assemblage_du_RHS_avec_une_fonction_de_Bessel(cplx *B, cplx *PUex, MailleSphere &MSph);


void Nouvelle_Technique_d_Assemblage_de_la_reciprocite_partie_interieure(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Nouvelle_Technique_d_Assemblage_de_la_reciprocite_partie_exterieure(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


void Assemblage_du_system_Spectral_par_une_nouvelle_technique(char *argv[], const char precision[]);

void debugg_Nouvelle_technique_d_assemblage_de_la_reciprocite_avec_une_fonction_de_Bessel(char *argv[], const char precision[]);


void Solve_full_linear_Cessenat_system_and_compare_all_norms_with_LU_and_COMPOSE_solveur(char *argv[], MKL_INT iter_max, const char precision[]);


void Solve_Spectral_linear_system_and_compare_all_norms_with_LU_and_COMPOSE_solveur(char *argv[], MKL_INT iter_max, const char precision[]) ;


void Distribute_Solve_and_compare_all_gmres_error_norms_(char *argv[], const char precision[]);

void collecte_diagA_glob_with_MPI_routine(int rank, int nb_procs, MKL_INT size_A_loc, cplx *tab_A_loc, cplx *tab_A_glob);


void define_and_collecte_dimension_of_new_trefftz_basis(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skyline &A_glob, MKL_INT *n_red_loc, MKL_INT *n_red, MailleSphere &MSph);


void collecte_Vector_with_MPI_routine(int rank, int nb_procs, MKL_INT size_V_loc, cplx *tab_V_loc, cplx *tab_V_glob);


void collecte_and_compare_A_skyline(int rank, int nb_procs, MKL_INT size_A_local, cplx *tab_A_local, MKL_INT size_A_glob, cplx *tab_A_glob);


void collecte_and_compare_resulting_A_skyline_to_full_A_skyline(int rank, int nb_procs, MKL_INT size_A_local, cplx *tab_A_local, MKL_INT size_A_glob, cplx *tab_A_glob);

void collecte_and_compare_resulting_A_skyline_to_full_A_skyline(int rank, int nb_procs, MKL_INT size_A_red, cplx *tab_A_red, MKL_INT size_Z_red, cplx *tab_Z_red, A_skylineRed &Z_red, MailleSphere &MSph);


void collecte_and_compare_resulting_Vector_to_full_Vector(int rank, int nb_procs, MKL_INT size_V_loc, cplx *tab_V_loc, MKL_INT size_V_glob, cplx *tab_V_glob);

void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, MKL_INT n_elem_loc, MKL_INT *local_cumul_Nred);

void transforme_A_skyline_to_Sparse_CSR(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skylineRed &A_red, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);

void transforme_A_skyline_to_Sparse_CSR_new_version(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, A_skylineRed &A_red, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph);


int solve_with_distributed_LU_method(int rank, MKL_INT debut, MKL_INT fin, MKL_INT n_glob, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *b, cplx *x, const char chaine[]);

int solve_with_distributed_LU_method(int rank, MKL_INT debut, MKL_INT fin, MKL_INT n_glob, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *b, cplx *x, MKL_INT *NNZ_LU, MKL_INT *MEM_LU, const char chaine[]);

int test_example_of_distriuted_intel_LU_solver();

void compute_pressure_and_velocity_norms(cplx *U, const char path_to_repertory[], const char method[], MailleSphere &MSph);

void define_the_size_of_local_linear_system(int rank, int nb_procs, int size_b_loc, int *counts_I, int *displs_I);

void TEST_the_distributed_LU_solver(char *argv[]);

void debugg_Sparse_CSR_matrices_with_MPI_routines(int rank, int nb_procs, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT *ia, MKL_INT *ja, cplx *a, MKL_INT nnz_glob, MKL_INT n_glob, MKL_INT *IA, MKL_INT *JA, cplx *A);


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT l, A_skyline H_glob, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, VecN &SIGMA, MailleSphere &MSph, cplx *AA, double *w);


cplx norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph, const char chaine[]);

cplx norme_L2_du_saut_en_pression_avec_integ_analytique_sur_les_faces_interieures_d_un_element(MKL_INT e, cplx *U, MailleSphere &MSph, const char chaine[]) ;

double norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


cplx norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph, const char chaine[]);


double norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]);

cplx norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_les_faces_interieures_d_un_element(MKL_INT e, cplx *U, MailleSphere &MSph, const char chaine[]) ;

double norme_H1_du_saut_avec_integ_analytique(cplx *U, MailleSphere &MSph, const char chaine[]) ;


double norme_H1_du_saut_avec_integ_analytique_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) ;


double norme_L2_surfacique_en_pression_avec_integ_analytique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


double norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


double norme_L2_surfacique_en_pression_avec_integ_analytique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


double norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


double norme_H1_surfacique_avec_integ_analytique(cplx *U, MailleSphere &MSph, const char chaine[]);

double norme_H1_surfacique_avec_integ_analytique_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);

void Solve_Spectral_linear_system_with_LU_method_and_compute_projection_of_analytical_solution(char *argv[], const char precision[]) ;

void compute_error_norms_associated_to_virtual_basis(char *argv[], const char precision[]);


void Solve_with_modified_COMPOSE_COO(char path[], MKL_INT n, cplx *B, cplx *U, MKL_INT iter_max, double tol);

void compute_norm_of_GMRES_Solution_for_Spectral_Methode_regulated_with_VirtualElement(char *argv[], MKL_INT imin, MKL_INT iter_max, const char precision[]);

void compute_norm_of_GMRES_Solution_for_Spectral_Methode_regulated_with_VirtualElement_distributed_version(char *argv[], MKL_INT iter_max, const char precision[]);


double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures(cplx *U, MailleSphere &MSph);

double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(cplx *U, MailleSphere &MSph);


void Assembly_and_Solve_Cessenat_linear_system_with_distributed_LU_solver(char *argv[], const char precision[]);

int solve_with_centralized_LU(char *argv[]);

void Assembly_and_solve_linear_Cessenat_method_with_distributred_LU(char *argv[], const char precision[]);


void write_local_Sparse_CSR_matrix_in_file(char *path, int rank, MKL_INT imin, MKL_INT imax, MKL_INT nnz, MKL_INT size_cols, MKL_INT *ia, MKL_INT *ja, cplx *a, MKL_INT n);

void write_local_vector_in_file(char *path, int rank, MKL_INT n, cplx *U);


void write_local_solution_in_file(char *path, int rank, MKL_INT n, cplx *U) ;


void Assembly_distributed_linear_Cessenat_method_and_write_it_on_file(char *argv[], const char precision[]);


MKL_INT compute_nnz_for_Sparse_COO_format(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, MailleSphere &MSph);

void transforme_A_skyline_to_Sparse_CSR_new_version(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph) ;

void read_size_of_local_Sparse_CSR_matrix_from_file(char path_vals_cols[], char path_rows[], MKL_INT *imin, MKL_INT *imax, MKL_INT *nnz, MKL_INT *size_cols, MKL_INT *n_glob);

void read_local_Sparse_CSR_matrix_from_file(char path_vals_cols[], char path_rows[], MKL_INT nnz, MKL_INT size_cols, MKL_INT *ia, MKL_INT *ja, cplx *a);

void Solve_distributed_linear_Cessenat_method_loaded_from_file(char *argv[], const char precision[]);


void define_and_collecte_dimension_of_new_trefftz_basis(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, cplx *M, MKL_INT *n_red_loc, MKL_INT *n_red, MailleSphere &MSph);


void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(int rank, int *counts_elem, int *displs_elem, A_skylineRed &A_red, cplx *P_red, MailleSphere &MSph, const char chaine[]);


void produit_Adjoint_P_virtual_Vector(int rank, int *counts_elem, int *displs_elem, MKL_INT *local_cumul_Nred, cplx *P_red, cplx *B_red, cplx *B, MailleSphere &MSph);


void Assembly_distributed_Spectral_linear_method_regulated_with_Virtual_Element_and_write_it_on_file(char *argv[], const char precision[]);


void Solve_distributed_Spectral_linear_method_regulated_with_Virtual_Element(char *argv[], const char precision[]);

void ASSEMBLAGE_DE_A1_plus_A4_plus_A5_plus_A8_parallel_version(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]);


double norme_H1_du_saut_d_un_vecteur(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *U);


double norme_H1_de_l_erreur_avec_integ_mixed(MKL_INT n, cplx *U, MailleSphere &MSph, const char chaine[]);

double norme_H1_du_saut_d_un_vecteur(cplx *U, MailleSphere &MSph, const char chaine[]) ;


double norme_H1_du_saut_d_un_vecteur_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);


double norme_H1_de_l_erreur_avec_integ_mixed(cplx *U, MailleSphere &MSph, const char chaine[]);


double norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);
/************************************************************ NORMES DISTRIBUEES **********************************************/
double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) ;


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) ;


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) ;


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) ;


double norme_H1_volumique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph) ;


double  norme_H1_erreur_volumique_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph) ;


double norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph);


double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph);


double norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph, const char chaine[]);


double norme_H1_de_l_erreur_avec_integ_mixed(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph, const char chaine[]);


void compute_pressure_and_velocity_norms(int rank, int *counts_elem, int *displs_elem, cplx *U, const char path_to_repertory[], const char method[], MailleSphere &MSph);


void produit_A_skyline_P_skyline_new_version(A_skyline &A_glob, cplx *P_red, A_skylineRed &A_red, MailleSphere &Msph);

void transforme_A_skyline_to_Sparse_COO_format(A_skylineRed &A_red, std::vector<int>& rows, std::vector<int>& cols, std::vector<cplx>& values, MailleSphere &MSph);


void ASSEMBLAGE_DE_LA_Reciprocite_avec_un_regulateur_du_type_Tikhonov_virtuel_parallel_version(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *P, cplx *Q, MailleSphere &MSph, const char chaine[]);


void produit_A_skyline_P_skyline_new_version(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *P_red, A_skylineRed &A_red, MailleSphere &Msph);


void distributed_prodMV_Sparse_COO(MKL_INT m_A, MKL_INT nnz, MKL_INT *rows, MKL_INT *cols, cplx *values, cplx *U, cplx *V_local, cplx *V_global);


void Assembly_and_Solve_optimized_version_of_Spectral_Method_Regulated_with_Tikhonov_VirtualElement_and_filtered_with_VirtualElement(char *argv[], const char precision[]);

/*===========================================================================================================================*/
double norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]);


double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MKL_INT e, MailleSphere &MSph, const char chaine[]) ;


double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(MKL_INT e, MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]);


double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]);


double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]);

double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]);


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]);

double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]);


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);


double norme_H1_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]);


double norme_H1_volumique_pour_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]);


double  norme_H1_erreur_volumique_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]);

double  norme_H1_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]);


void prodMatreal_Vectcomplex_ColMajor(double *A, MKL_INT m, MKL_INT n, cplx *V, MKL_INT p, cplx *b);


cplx integrale_tetra_VV_ponderee(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) ;


cplx integrale_tetra_VV_ponderee(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]) ;


cplx quad_tetra_VV_ponderee(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2) ;


void buildVolumeMetrique_ponderee(MKL_INT ndof, cplx *M, Element &E, const char chaine[]);


void buildVolumeMetrique_ponderee(MKL_INT ndof, cplx *M, MailleSphere &MSph, const char chaine[]);


void buildVolumeMetrique_ponderee_parallel_version(MKL_INT ndof, cplx *M, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_ponderee(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_ponderee_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


double norme_L2_ponderee_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph);

double norme_L2_ponderee_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph);

double norme_L2_ponderee_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MKL_INT e, MailleSphere &MSph) ;

double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique(cplx *U, MailleSphere &MSph) ;


double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) ;


double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) ;


double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph);

cplx quad_tetra_Bessel_VV_ponderee(MKL_INT e, MKL_INT iloc, MailleSphere &MSph);

void BuildVector_Of_Volume_Projection_ponderee(cplx *B, MailleSphere &MSph);


void BuildVector_Of_Volume_Projection_ponderee_parallel_version(cplx *B, MailleSphere &MSph);

void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_and_optimized_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, MailleSphere &MSph);


double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(int rank, int *counts_elem, int *displs_elem, cplx *U, MailleSphere &MSph);


void BuildVolumeMetricMatrix_ponderee_numerical_integ(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);


void BuildVector_Of_Volume_Projection_ponderee(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph);


void BuildVector_Of_Volume_Projection_ponderee_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, cplx *B, MailleSphere &MSph);

double normeDG(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *X, MailleSphere &MSph) ;


double Assembly_and_compute_DG_norm(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *U, MailleSphere &MSph, const char precision[]);


double normeDG_en_Desamblee(int rank, int *counts_elem, int *displs_elem, cplx *X, MailleSphere &MSph, const char precision[]) ;


void store_P_and_transpose_P_local_to_process(MKL_INT indice_glob, MKL_INT indice_loc, cplx *a, cplx *b, A_skyline &P_red,  MailleSphere &MSph) ;

void compute_A_red_ColMajor(MKL_INT indice_glob, MKL_INT indice_loc, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP);


void collection_Solutions_locales_parallel_version(int rang, int *counts_elem, int *displs_elem, cplx *U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) ;

void collection_Solutions_locales_parallel_version(int rang, int *counts_elem, int *displs_elem, cplx *U, A_skyline &M_glob, cplx *P_red, MailleSphere &MSph) ;


void TEST_the_distributed_projection(char *argv[]);

void TEST_the_distributed_projection_regulated_with_Virtual_Element(char *argv[]);

void compute_pressure_and_velocity_norms(int rank, int nb_procs, int *counts_elem, int *displs_elem, cplx *U, cplx *P, const char path_to_repertory[], const char method[], MKL_INT filtre, MailleSphere &MSph);


void DecompositionDeBloc_ET_Normalisation_dans_ordre(MKL_INT l, MKL_INT e, A_skyline &P_glob, A_skyline &Q_glob, cplx *LAMBDA, MailleSphere &MSph, cplx *AA, double *w);

void DecompositionGlobaleDesBlocs_ET_Normalisation_dans_ordre(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &P_glob, A_skyline &Q_glob, cplx *LAMBDA, MailleSphere &MSph);

void ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, MailleSphere &MSph, const char chaine[]);

void Assembly_distributed_linear_spectral_method_regulated_with_Tikhonov_variable_parameter_and_write_it_on_file(char *argv[], const char precision[]);

void BuildVolumeMetricMatrix_ponderee_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &M_glob, MailleSphere &MSph, const char chaine[]);

double normeDG_parallel_version(A_skyline &A_glob, cplx *X, MailleSphere &MSph) ;


double normeDG_en_Desamblee_parallel_version(cplx *X, MailleSphere &MSph, const char precision[]) ;

void compute_pressure_and_velocity_norms(cplx *U, cplx *P, MKL_INT filtre, const char path_to_repertory[], const char method[], MailleSphere &MSph);


void Assembly_of_Spectral_method_regulated_with_Tikhonov_variable_parameter_and_Filtered_with_Virtual_Metric(char *argv[], const char precision[]);

void DecompositionDeBloc_ET_Normalisation_dans_ordre(MKL_INT e, MKL_INT l, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w);


void DecompositionGlobaleDesBlocs_ET_Normalisation_dans_ordre(A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph);


void check_matrix(cplx *bloc, MKL_INT n, MKL_INT m) ;


void check_vector(cplx *vec, MKL_INT n) ;


void Assembly_and_Solve_with_Compose_distributed_Spectral_linear_method_regulated_with_Virtual_Element(char *argv[], const char precision[]);


void write_local_Sparse_COO_matrix_in_file(char *path, MKL_INT rank, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT n_glob, std::vector<int>& rows, std::vector<int>& cols, std::vector<cplx>& values);


void read_size_of_local_Sparse_COO_matrix_from_file(char path[], MKL_INT *nnz_loc, MKL_INT *n_loc, MKL_INT *n_glob);


void read_local_Sparse_COO_matrix_from_file(char path[], MKL_INT nnz, std::vector<int>& rows, std::vector<int>& cols, std::vector<cplx>& values);


void Read_and_Solve_with_Compose_distributed_Spectral_linear_method_regulated_with_Virtual_Element(char *argv[], const char precision[]);


void preprocessing_of_Sparse_COO_MV_product(struct matrix_descr *descr, sparse_matrix_t *coo_matrix, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT n_glob, MKL_INT *local_rows, MKL_INT *local_cols, cplx *local_values);


void prodM_local_Vector_Sparse_COO_distribue(MKL_INT nnz_loc, cplx alpha, struct matrix_descr descr, sparse_matrix_t coo_matrix, cplx *X_glob, cplx beta, cplx *y_loc);


void destroy_Sparse_COO_Matrix(sparse_matrix_t *coo_matrix);


void read_size(char path[], MKL_INT rank, MKL_INT *n);

void TEST_distributed_Sparse_COO_product(char *argv[], const char precision[]);


void Assembly_Read_and_Solve_with_Compose_distributed_Spectral_linear_method_regulated_with_Virtual_Element(char *argv[], MKL_INT iter_max, double eps_gmres, const char precision[]);
void TEST_newCOO_product(char *argv[], const char precision[]);


void Distributed_gmres_solver(int rank, int nb_procs, int *counts_elem, int *displs_elem, double *t1, double *t2, double *t3, double *t4, MKL_INT nkry, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT n_glob, struct matrix_descr descr, sparse_matrix_t coo_matrix, cplx *allrhs, cplx *U, cplx *U_gmres, cplx *H, cplx *v, cplx *F_tilde, cplx *Kn, cplx *K_r, cplx *R, cplx *b, MKL_INT space, cplx *AU_loc);


void compute_distributed_residual_with_COO_product(int rank, int nb_procs, int *counts_elem, int *displs_elem, double *tAU, double *tV, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT n_glob, struct matrix_descr descr, sparse_matrix_t coo_matrix, cplx *all_U, cplx *all_F, cplx *Fdelta, MKL_INT space, cplx *AU_loc, cplx *AU) ;

void GMRES_With_Restart_CSR_product(int rank, int nb_procs, int *counts_elem, int *displs_elem, MKL_INT *iter_ext, MKL_INT nkry, cplx *Fred, MKL_INT nnz_loc, MKL_INT n_loc, MKL_INT n_glob, struct matrix_descr descr, sparse_matrix_t coo_matrix, cplx *U, MKL_INT iter_max, char chaine[], MailleSphere &MSph);

void computeSizeDecomposedNewTracesMatrix(double eps_reg, cplx *B, cplx *P, double *LAMBDA, MKL_INT *nmoins, MKL_INT *nplus, MailleSphere &MSph);


void BuildPositivePart_Of_NewTracesMatrix_adapted_format(cplx *hat_P, double *hat_VP, MKL_INT n_neg, MKL_INT n_pos, cplx *hat_P_pos, double *hat_VP_pos, cplx *Adjt_hat_P_pos, MailleSphere &MSph);

MKL_INT DecomposeNewTracesMatrix_adpted_format(MKL_INT n, cplx *B, cplx *P, double *VP, MKL_INT n_neg, cplx *P_neg, double *VP_neg, cplx *Adjt_P_neg, MKL_INT n_pos, cplx *P_pos, double *VP_pos, cplx *Adjt_P_pos, MailleSphere &MSph);

MKL_INT DecomposeNewTracesMatrix_adpted_format(double eps_reg, MKL_INT n, cplx *B, cplx *P, double *VP, MKL_INT n_neg, cplx *P_neg, double *VP_neg, cplx *Adjt_P_neg, MKL_INT n_pos, cplx *P_pos, double *VP_pos, cplx *Adjt_P_pos, MailleSphere &MSph);

MKL_INT DecomposeNewTracesMatrix_adpted_format(MKL_INT e, MKL_INT f, double eps_reg, MKL_INT n, cplx *B, cplx *P, double *VP, MKL_INT n_neg, cplx *P_neg, double *VP_neg, cplx *Adjt_P_neg, MKL_INT n_pos, cplx *P_pos, double *VP_pos, cplx *Adjt_P_pos, MailleSphere &MSph);

void compute_reflexion_and_transmission_coefficients(MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT N, cplx *hat_P_T_neg, double *hat_VP_T_neg, cplx *hat_P_T_pos, double *hat_VP_T_pos, cplx *Adjt_hat_P_T_pos, double *hat_VP_K_pos, cplx *Adjt_P_K_pos, cplx *Rf, cplx *Tv, cplx *G, cplx *H, cplx *D);


void fill_blocks(cplx *block_diag, cplx *block_extra, cplx *P_TsqrtSigma_T, cplx *P_KsqrtSigma_K, MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT N, double *hat_VP_T_neg, cplx *hat_P_T_pos, double *hat_VP_T_pos, cplx *Adjt_hat_P_T_pos, cplx *hat_P_K_pos, cplx *Adjt_hat_P_K_pos, cplx *Rf, cplx *Tv, cplx *ATF, cplx *AVF, cplx *VP_T_R, cplx *VP_T_T, cplx *G, cplx *H);


MKL_INT fill_blocks_minus_Reciprocite_over_2(MKL_INT elem_diag, MKL_INT face, cplx *RTF, cplx gamma, cplx *block_diag, cplx *block_extra, cplx *P_TsqrtSigma_T, cplx *P_KsqrtSigma_K, MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT N, double *hat_VP_T_neg, cplx *hat_P_T_pos, double *hat_VP_T_pos, cplx *Adjt_hat_P_T_pos, cplx *hat_P_K_pos, double *hat_VP_K_pos, cplx *Adjt_hat_P_K_pos, cplx *Rf, cplx *Tv, cplx *ATF, cplx *AVF, cplx *VP_T_R, cplx *VP_T_T, cplx *G, cplx *H, cplx *W, MailleSphere &MSph);

void compute_reflexion_and_transmission_coefficients(cplx *P_TsqrtSigma_T, MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT N, cplx *hat_P_T_neg, double *hat_VP_T_neg, cplx *hat_P_T_pos, double *hat_VP_T_pos, cplx *Adjt_hat_P_T_pos, double *hat_VP_K_pos, cplx *Adjt_hat_P_K_pos, cplx *Rf, cplx *Tv, cplx *G, cplx *H, cplx *D);


MKL_INT fill_blocks(cplx *RTF, cplx gamma, cplx *block_diag, cplx *block_extra, cplx *P_TsqrtSigma_T, cplx *P_KsqrtSigma_K, MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT N, double *hat_VP_T_neg, cplx *hat_P_T_pos, double *hat_VP_T_pos, cplx *Adjt_hat_P_T_pos, cplx *hat_P_K_pos, double *hat_VP_K_pos, cplx *Adjt_hat_P_K_pos, cplx *Rf, cplx *Tv, cplx *ATF, cplx *AVF, cplx *VP_T_R, cplx *VP_T_T, cplx *G, cplx *H, cplx *W);

void check_dimension_block_associated_to_faces(MKL_INT e, MKL_INT g, MKL_INT f, MKL_INT n_T_neg, MKL_INT n_T_pos, MKL_INT n_K_neg, MKL_INT n_K_pos);

void check_dimension_block_associated_to_faces(MKL_INT *n_T_neg, MKL_INT *n_T_pos, MKL_INT *n_K_neg, MKL_INT *n_K_pos);

void ASSEMBLAGE_DE_LA_Reciprocite_format_adapte(A_skyline &A_glob, A_skyline &M_glob, A_skyline &P_glob, A_skyline &Q_glob, MailleSphere &MSph, const char chaine[]);


void Decomposition_ET_Construction_de_Bloc_regularise(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w, cplx *Q) ;


void Decomposition_ET_Construction_de_Bloc_regularise(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w) ;



void Decomposition_ET_Construction_des_Blocs_regularises(A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph);


void Decomposition_ET_Construction_des_Blocs_regularises(A_skyline &A_glob, A_skyline &P_glob, A_skyline &Q_glob, VecN &LAMBDA, MailleSphere &MSph);


void Decomposition_ET_Construction_de_Bloc_regularise(MKL_INT ndof, cplx *M, cplx *P, cplx *Q, double *V, Element E, cplx *AA, double *w);


void determine_neighboring_face(MKL_INT e, MKL_INT fe, MKL_INT *fg, MailleSphere &MSph);





MKL_INT check_orthogonality(MKL_INT e, MKL_INT f, MKL_INT N, cplx *M,cplx *P, cplx *PM, cplx *PMP);


void ASSEMBLAGE_DE_LA_Reciprocite_format_adapte_parallel_version(A_skyline &A_glob, A_skyline &M_glob, A_skyline &P_glob, MailleSphere &MSph, const char chaine[]);


void Assembly_optimized_version_of_Adapted_Spectral_method_regulated_with_Tikhonov_variable_parameter_filtred_with_VirtualElement(char *argv[], const char precision[]);


void Assembly_optimized_version_of_Adapted_Spectral_method_regulated_with_Tikhonov_variable_parameter_filtred_with_Variable_Metric(char *argv[], const char precision[]);


void Compute_local_conditioning_of_Adapted_Spectral_method_regulated_with_Tikhonov_variable_parameter(char *argv[], const char precision[]);


void ASSEMBLAGE_DE_LA_Reciprocite_format_adapte(A_skyline &A_glob, cplx *M, cplx *P_sqrtSigma, MailleSphere &MSph, const char chaine[]);


void ASSEMBLAGE_DE_LA_Reciprocite_format_adapte_parallel_version(A_skyline &A_glob, cplx *M, cplx *P_sqrtSigma, MailleSphere &MSph, const char chaine[]);


void Assembly_optimized_version_of_Adapted_Spectral_method_regulated_with_Tikhonov_VirtualElement(char *argv[], const char precision[]);


void Assembly_optimized_version_of_Adapted_Spectral_method_regulated_with_Tikhonov_VirtualElement_Filtred_with_VirtualElement(char *argv[], const char precision[]);


void Decomposition_ET_Construction_de_Bloc_regularise(MKL_INT e, MKL_INT E, MKL_INT s, A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w, cplx *Q) ;

void Decomposition_ET_Construction_des_Blocs_regularises(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph);

void Decomposition_ET_Construction_des_Blocs_regularises_parallel_version(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph);

void ASSEMBLAGE_DE_LA_Reciprocite_format_adapte_parallel_version(int rank, int *counts_elem, int *displs_elem, A_skyline &A_glob, A_skyline &M_glob, A_skyline &P_glob, MailleSphere &MSph, const char chaine[]);

void produit_Adjoint_P_virtual_A_skyline_P_virtual(MKL_INT rang, int *counts_elem, int *displs_elem, A_skyline &A_glob, cplx *P_red, A_skylineRed &A_red, MailleSphere &MSph) ;


void Assembly_distributed_Adapted_spectral_linear_method_regulated_with_Tikhonov_variable_parameter_and_write_it_on_file(char *argv[], const char precision[]);
