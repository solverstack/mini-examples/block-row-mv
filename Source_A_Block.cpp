#include "Header.h"
 
using namespace std;
A_Block::A_Block() {
	i_block = -1;
	j_block = -1;
	n_dof = -1;
	A_loc =  nullptr;
}
void A_Block::set_Aloc(cplx* A) // Fixe l'adresse de Aloc
{
	A_loc = A;
}
cplx* A_Block::get_Aloc() // Fixe l'adresse de Aloc
{
	return A_loc;
} 
void A_Block::set_ij(MKL_INT i, MKL_INT j)
{
	i_block = i;
	j_block = j;
}
void A_Block::set_ndof(MKL_INT n)
{
	n_dof = n;
}
MKL_INT A_Block::get_i()
{
	return i_block;
}
MKL_INT A_Block::get_j()
{
	return j_block;
}

MKL_INT A_Block::get_n(){
  return n_dof;
}

void A_Block::print_info_A_loc()
{
	cout << "i_block = " << i_block << ", jblock = " << j_block << ", ndof = " << n_dof << ", adresse = " << A_loc << "." << endl;
}
void A_Block::print_A_loc()
{
	MKL_INT l = 0;
	for (MKL_INT i = 0; i < n_dof; i++) {
		for (MKL_INT j = 0; j< n_dof; j++) {
			cout << A_loc[l]<<" ";
			l = l + 1;
		}
		cout << endl;
	}
	cout << endl;
}


/*********************************************************************************************************************************************************************************************************/
// /*---------------------------------------------------------second membre -------------------------------*/

//integrale Pinc * Piloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_PincP_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {
   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                                
  for (l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      I += Mesh.elements[e].T_PP[l] * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
 
  return I;
}



//integrale de Pinc * Viloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_PincV_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker diloc \cdot n_j avec n_j la normale à la face j                
  
  for (l = 0; l < 4; l++) {                       
    if (Mesh.elements[e].voisin[l] < 0) {
      if (l == 0) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n1, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
     
      }
      else if (l == 1) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 2) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
     
      }
      else if (l == 3) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn = dn * Mesh.elements[e].OP[iloc].ksurOmega;
	
      }
      
      I += Mesh.elements[e].T_PV[l] * dn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}



//integrale de Vinc * Piloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincP_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker d \cdot n_j avec n_j la normale à la face j et d la direction de l'onde incidente               
                                              
  for (l = 0; l < 4; l++) {                       
    if (Mesh.elements[e].voisin[l] < 0) {
      if (l == 0) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
       
      }
      else if (l == 1) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
     
      }
      else if (l == 2) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
       
      }
      else if (l == 3) {
	dn = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn = dn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      I += Mesh.elements[e].T_VP[l] * dn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}



//integrale de Vinc * Viloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincV_SourceOndePlane(Maillage &Mesh, MKL_INT e, MKL_INT iloc, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    Mesh : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn1, dn2;
  //dn1 pour stocker dinc \cdot n_j avec n_j la normale à la face j et dinc la direction de l'onde incidente
  //dn2 pour stocker diloc \cdot n_j avec n_j la normale à la face j et diloc la direction de l'onde iloc                
                                              
  for (l = 0; l < 4; l++) {                       
    if (Mesh.elements[e].voisin[l] < 0){
      if (l == 0) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n1, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n1, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 1) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n2, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n2, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 2) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n3, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n3, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 3) {
	dn1 = cblas_ddot(lda, Mesh.elements[e].OP[Mesh.n_i].Ad, inc, Mesh.elements[e].n4, inc) ;
	dn2 = cblas_ddot(lda, Mesh.elements[e].OP[iloc].Ad, inc, Mesh.elements[e].n4, inc) ;

	dn1 = dn1 * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	dn2 = dn2 * Mesh.elements[e].OP[iloc].ksurOmega;
       
      }
      
      I += Mesh.elements[e].T_VV[l] * dn1 * dn2 * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}


/*------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------- TESTS RELATIFS A LA GRANDE MATRICE ------------------------------*/
/*----------------------------------------------------------------- [P] [P'] ------------------------------------------------------*/

cplx int_P_TxP_T_interieure_On_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   f        : numero de la face
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  if (Mesh.elements[e].voisin[f] >= 0){
    I = integrale_triangle(Mesh.elements[e].face[f], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
  }
 
  return I;
}



cplx int_P_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
  
  for (l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] >= 0){
      I += integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
 
  return I;
}


cplx int_P_KxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
   //Calcul de l'intégrale de P_K * conj(P'_K) sur les faces intérieures de l'élément n° e avec K le voisin de l'élément 
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : numero local d'un voisin de l'element e
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT h;                                              
  if (Mesh.elements[e].voisin[c] >= 0){
      
    h = Mesh.elements[e].voisin[c];

    I = integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[h].OP[iloc], Mesh.elements[h].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_P_TxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
   //Calcul de l'intégrale de P_T * conj(P'_K) sur les faces intérieures de l'élément n° e avec K le triangle voisin à l'élément e et T le triangle associé à l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : numero local d'un voisin de l'element e
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT h;                                              
  if (Mesh.elements[e].voisin[c] >= 0){
      
    h = Mesh.elements[e].voisin[c];

    I =  -integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[h].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_P_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
   //Calcul de l'intégrale de P_K * conj(P'_T) sur les faces intérieures de l'élément n° e avec P_K l'onde associée au voisin de l'élément e et P'_T celle associée à l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : numero local d'un voisin de l'element e
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT h, s;                                              
  if (Mesh.elements[e].voisin[c] >= 0){
    h = Mesh.elements[e].voisin[c];
    I = - integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[e].OP[iloc], Mesh.elements[h].OP[jloc], chaine);
    
  }
  
  return I;
}







/*----------------------------------------------------------------- BLOC P_T P_T ------------------------------------------*/

void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxP_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxP_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] = int_P_TxP_T_interieure_On_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}


/*----------------------------------------------------------------- BLOC P_K P_K ------------------------------------------*/

void Build_BlocInt_P_KxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_KxP_K_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_KxP_K(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_KxP_K_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


/*----------------------------------------------------------------- BLOC P_T P_K ------------------------------------------*/

void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxP_K_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxP_K_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


/*----------------------------------------------------------------- BLOC P_K P_T ------------------------------------------*/

void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_KxP_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_KxP_T_interieure(iloc, jloc, i, c, Mesh, chaine);
     
    }
  }
}




void BlocIntPP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [P][P'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
  Build_BlocInt_P_TxP_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  

    /*----------------------------------------- Interaction element - voisin P_K x P_T --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (Mesh.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_P_KxP_T(i, j, AA, Mesh, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA = 0;
}


void BlocIntPP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [P][P'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
    Build_BlocInt_P_TxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);
    
    
    /*----------------------------------------- Interaction element - voisin P_K x P_T --------------------------------*/
    
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_P_KxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA = 0;
}



void BlocIntPP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    BlocIntPP(i, Mesh.tab_PP[0], Mesh.tab_PP[0], A_glob, Mesh, chaine);
  }
}


// /*----------------------------------------------------------------- P P' --------------------------------------------------------*/




cplx int_P_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur une face exterieure de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   f        : numero d'une face f
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
                                               
  if (Mesh.elements[e].voisin[f] < 0){
    I = integrale_triangle(Mesh.elements[e].face[f], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
  }
  
  return I;
}



cplx int_P_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur une face exterieure de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    alpha    : coefficient associée à cette intégrale
    iloc     : numero de l'onde pour la fonction-test
    jloc     : ......................... solution
    e     : numero de l'élément
    Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  MKL_INT l;
  for(l = 0; l < 4; l++) {                                            
    if (Mesh.elements[e].voisin[l] < 0){
      I += int_P_TxP_T_exterieure_1_face(iloc, jloc, e, l, Mesh, chaine);
    }
  }
  return I;
}




void Const_Matrice_PP_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT i, iloc, jloc;
  for(i = 0; i < Mesh.get_N_elements(); i++){
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
	for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
	  for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
	    AA[iloc * A_glob.get_n_dof() + jloc] =  int_P_TxP_T_exterieure(iloc, jloc, i, Mesh, chaine);
	  }
	}
  }
}


void Build_BlocExt_P_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxP_T_exterieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocExt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la bloc P_T * P_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    Mesh : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] = int_P_TxP_T_exterieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}


void BlocExtPP_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice P P' associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_P_TxP_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA = 0;
}


void BlocExtPP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute au tableau A_skyline A_glob, la matrice P P' associée à l'élément i et à la face de bord l.
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face de i
    alpha     : coeff de la matrice. Elle est associée à i
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i, 0) dans A_skyline
  if (Mesh.elements[i].voisin[l] < 0 ){
    
    Build_BlocExt_P_TxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);

  }
  
  delete [] AA; AA = nullptr;
}



void BlocExtPP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    BlocExtPP_Add(i, Mesh.tab_PP[3], A_glob, Mesh, chaine);
  }
}


void Build_A5(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {//boucle sur les éléments
    for(l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtPP_Add(i, l, Mesh.elements[i].T_PP[l], A_glob, Mesh, chaine);
    }
  }
}



void Build_A5_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {//boucle sur les éléments
    for(l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtPP_Add(i, l, Mesh.elements[i].alphaPP[l], A_glob, Mesh, chaine);
    }
  }
}




// /*----------------------------------------------------------------- [V] [V'] ---------------------------------------------------------*/


cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   l     : numero local 'une face
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
  if (Mesh.elements[e].voisin[l] >= 0){ 
    
    if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    
    I += din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
  }
  
  
  return I;
}



cplx int_V_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] >= 0){
      I += din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}


cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Kn_K * conj(V'_K)n_K sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------

   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : ...... local du voisin de l'élément e
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);

  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[c] >= 0) {

    MKL_INT h = Mesh.elements[e].voisin[c];

     if (c == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (c == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }

    
     I = - din * djn * integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[e].OP[iloc], Mesh.elements[h].OP[jloc], chaine);
  }
  
  return I;
}


/*------------------------------------------------------------------------ V V' ---------------------------------------------------------*/

cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur une face exterieure de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    I = din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_V_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] < 0){
      I += din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_TxV_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocInt_V_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxV_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}



void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxV_T_interieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}



void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_KxV_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_KxV_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}



void Const_Matrice_VV_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT i, iloc, jloc;
  for(i = 0; i < Mesh.get_N_elements(); i++){
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
	for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
	  for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
	    AA[iloc * A_glob.get_n_dof() + jloc] =  int_V_TxV_T_exterieure(iloc, jloc, i, Mesh, chaine);
	  }
	}
  }
}



void BlocIntVV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [V][V'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
  Build_BlocInt_V_TxV_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  

    /*----------------------------------------- Interaction element - voisin V_T x V_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (Mesh.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_V_KxV_T(i, j, AA, Mesh, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA = 0;
}



void BlocIntVV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [V][V'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
    Build_BlocInt_V_TxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);
    
    
    /*----------------------------------------- Interaction element - voisin V_K x V_T --------------------------------*/
    
    
    
    //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
    Build_BlocInt_V_KxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA = 0;
}



void Build_BlocExt_V_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxV_T_exterieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocExt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
   //Cette fonction calcule la bloc V_Tn_T * V'_Tn_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    Mesh : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxV_T_exterieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}



void BlocExtVV_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute dans le tableau A_skyline A_glob, la matrice V n V' n associée à l'élément i et à la face de bord l. 
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_V_TxV_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}


void BlocExtVV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice V_T n_T V'_T n_T associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i,0) dans A_skyline
  if (Mesh.elements[i].voisin[l] < 0) {
    
    Build_BlocExt_V_TxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
  }
  
  delete [] AA; AA  =  nullptr;
}


void BlocExtVV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
   //Calcul la matrice globale V V' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    BlocExtVV_Add(i, Mesh.tab_VV[3], A_glob, Mesh, chaine);
  }
}


void Build_A8(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
   //Calcul la matrice globale V n V' n sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtVV_Add(i, l, Mesh.elements[i].T_VV[l], A_glob, Mesh, chaine);
    }
  }
}


void Build_A8_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
   //Calcul la matrice globale V n V' n sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtVV_Add(i, l, Mesh.elements[i].alphaVV[l], A_glob, Mesh, chaine);
    }
  }
}



void BlocIntVV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    BlocIntVV(i, Mesh.tab_VV[0], Mesh.tab_VV[0], A_glob, Mesh, chaine);
  }
}


/*--------------------------------------------------------------------------- [V] {P} ---------------------------------------------*/


cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[l] >= 0){

     if (l == 0) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    
     I += djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  
  return I;
}



cplx int_V_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] >= 0){
      I += djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_K) sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   c        : numero local du voisin
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT h;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[c] >= 0) {

    h = Mesh.elements[e].voisin[c];

     if (c == 0) {
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;
      
    }
    else if (c == 1) {
     
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;
      
    }
    else if (c == 2) {
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;
     
    }
    else if (c == 3) {
      
      djn = cblas_ddot(3, Mesh.elements[h].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[h].OP[jloc].ksurOmega;      
   
    }
     
     
     I =  - djn * integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[e].OP[iloc], Mesh.elements[h].OP[jloc], chaine);
      
  }
  
  return I;
}


void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_TxP_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocInt_V_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxP_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}



void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxP_T_interieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}




void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_KxP_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_KxP_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}




void BlocIntVP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [V]{P'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale sur les faces intérieures. Elle correspond à j = 0 dans A_skyline

    //interaction element-element
    Build_BlocInt_V_TxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);

    
    /*----------------------------------------- Interaction element - voisin V_T x P_K --------------------------------*/
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_V_KxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);

  }
  
  delete [] AA; AA  =  nullptr;
}



void BlocIntVP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [V]{P'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/
  
  //construction de la matrice bloc diagonale sur les faces intérieures. Elle correspond à j = 0 dans A_skyline
  Build_BlocInt_V_TxP_T(i, AA, Mesh, chaine);
  
  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  
  
  /*----------------------------------------- Interaction element - voisin V_T x P_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (Mesh.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_V_KxP_T(i, j, AA, Mesh, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
    }
  }
  delete [] AA; AA = 0;
}



void BlocIntVP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    
    BlocIntVP(i, Mesh.tab_VP[0], Mesh.tab_VP[0], A_glob, Mesh, chaine);
  }
}


void BlocIntVP_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntVP(i, j, Mesh.tab_VP[0], Mesh.tab_VP[0], A_glob, Mesh, chaine);
    }
  }
}



/*----------------------------------------------------------------------- V P -----------------------------------------------------*/


cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
 
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;      
      
    }
    
    
    
    I = djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_V_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, Mesh.elements[e].OP[jloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      djn = djn * Mesh.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] < 0){
      I += djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}




void Build_BlocExt_V_TxP_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxP_T_exterieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}



void Build_BlocExt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la bloc V_T * P'_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    Mesh : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_V_TxP_T_exterieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}



void BlocExtVP_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [V]{P'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures. Elle correspond à j = 0 dans A_skyline
  Build_BlocExt_V_TxP_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}


void BlocExtVP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute dans le tableau A_skyline A_glob, la matrice V_Tn_T P'_T associée à l'élément i et à la face de bord l.
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures. Elle correspond à l'emplacement (i,0) dans A_skyline

  if (Mesh.elements[i].voisin[l] < 0 ){
    
    Build_BlocExt_V_TxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);

  }
  delete [] AA; AA  =  nullptr;
}



void BlocExtVP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    
    BlocExtVP_Add(i, Mesh.tab_VP[3], A_glob, Mesh, chaine);
  }
}


void Build_A7(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
   //Calcul la matrice globale V n P' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V n P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (l = 0; l < 4; l++) {
      
      BlocExtVP_Add(i, l, Mesh.elements[i].T_VP[l], A_glob, Mesh, chaine);
    }
  }
}


void Const_Matrice_VP_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT i, iloc, jloc;
  for(i = 0; i < Mesh.get_N_elements(); i++){
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
	for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
	  for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
	    AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_TxP_T_exterieure(iloc, jloc, i, Mesh, chaine);
	  }
	}
  }
}



/*-------------------------------------------------------------------- [P] {V} -------------------------------------------------*/


cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face 
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  if (Mesh.elements[e].voisin[l] >= 0){
    if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    I += din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
  }
  
  return I;
}



cplx int_P_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
       
    if (Mesh.elements[e].voisin[l] >= 0){
      I += din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_K * conj(V'_T)n_T sur les faces intérieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------

   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : ...... local du voisin de l'élément e
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);

  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  if (Mesh.elements[e].voisin[c] >= 0) {

    MKL_INT h = Mesh.elements[e].voisin[c];

     if (c == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (c == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
     I = - din * integrale_triangle(Mesh.elements[e].face[c], Mesh.elements[e].OP[iloc], Mesh.elements[h].OP[jloc], chaine);
  }
  
  return I;
}


void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxV_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxV_T_interieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}



void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxV_T_interieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}



void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_KxV_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}


void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_KxV_T_interieure(iloc, jloc, i, c, Mesh, chaine);
    }
  }
}



void BlocIntPV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [P]{V'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces intérieures correspondante à j = 0 dans A_skyline
  Build_BlocInt_P_TxV_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
 

    /*----------------------------------------- Interaction element - voisin P_T x V_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (Mesh.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_P_KxV_T(i, j, AA, Mesh, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA  =  nullptr;
}



void BlocIntPV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice [P]{V'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale pour les faces intérieures correspondante à j = 0 dans A_skyline

    //interaction élément-élément
    Build_BlocInt_P_TxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);

    
    /*----------------------------------------- Interaction element - voisin P_T x V_K --------------------------------*/
    
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_P_KxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}




void BlocIntPV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    
    BlocIntPV(i, Mesh.tab_PV[0], Mesh.tab_PV[0], A_glob, Mesh, chaine);

  }
}


void BlocIntPV_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j; 
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntPV(i, j, Mesh.tab_PV[0], Mesh.tab_PV[0], A_glob, Mesh, chaine);
      
    }
  }
}



/*-------------------------------------------------------------------- P V --------------------------------------*/

cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    
    iloc     : numero de l'onde pour la fonction-test
    jloc     : ......................... solution
    e        : numero de l'élément
    l        : numero local d'une face
    Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  

  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (Mesh.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
      
      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    
    I = din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}


cplx int_P_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] < 0){
      I += din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



void Build_BlocExt_P_TxV_T(MKL_INT i, cplx *AA, Maillage &Mesh, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxV_T_exterieure(iloc, jloc, i, Mesh, chaine);
    }
  }
}



void Build_BlocExt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, Maillage &Mesh, const char chaine[]) {
   //Cette fonction calcule la bloc P_T * V'_T n_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    Mesh : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < Mesh.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < Mesh.ndof; jloc++) { // numero de la colonne
      AA[iloc * Mesh.ndof + jloc] =  int_P_TxV_T_exterieure_1_face(iloc, jloc, i, l, Mesh, chaine);
    }
  }
}



void BlocExtPV_Add(MKL_INT i, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [P]{V'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_P_TxV_T(i, AA, Mesh, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}



void BlocExtPV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [P]{V'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face 
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i,0) dans A_skyline

  if (Mesh.elements[i].voisin[l] < 0) {
    
    Build_BlocExt_P_TxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_A6(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Calcul la matrice globale P V'n sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                       A_glob, la matrice P V'n
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < Mesh.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      
      BlocExtPV_Add(i, l, Mesh.elements[i].T_PV[l], A_glob, Mesh, chaine);
    }
  }
}


void BlocExtPV(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Calcul la matrice globale P V'n sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                       A_glob, la matrice P V'n
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {

    BlocExtPV_Add(i, Mesh.tab_PV[3], A_glob, Mesh, chaine);
  }
}



void Const_Matrice_PV_ext(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  cplx *AA;
  MKL_INT i, iloc, jloc;
  for(i = 0; i < Mesh.get_N_elements(); i++){
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
	for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
	  for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
	    AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxV_T_exterieure(iloc, jloc, i, Mesh, chaine);
	  }
	}
  }
}


/*--------------------------------------------------------------------- Pinc P --------------------------------------------------------*/

cplx int_PincP_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Pinc * conj(P'_T) sur une face exterieure de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    iloc     : numero de l'onde pour la fonction-test
    e        : numero de l'élément
    Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
                                                I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  for(l = 0; l < 4; l++) {
    if (Mesh.elements[e].voisin[l] < 0){
      I += integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  return I;
}



cplx int_PincP_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Pinc * conj(P'_T) sur une face exterieure de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    iloc     : numero de l'onde pour la fonction-test
    e        : numero de l'élément
    Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
                                                I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;

  //Partie domaine 1 :  (Pinc + R * P_ref) * P'_T
  if (Mesh.elements[e].get_ref() == 1) {

    cplx I1 = cplx(0.0, 0.0);
    cplx I2 = cplx(0.0, 0.0);
    
    for(l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	I1 = integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
	I2 = integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_r], chaine);

	I += Mesh.elements[e].alpha_PincP[l] * (I1 + Mesh.R * I2);
      }
    }
    
  }


  //Partie domaine 2 : T * P_t * P'_T
  else {
    
    for(l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	I += Mesh.elements[e].alpha_PincP[l] * Mesh.T * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
      }
     
    }
    
  }
  return I;
}



void Build_VectorPincP_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
    
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { 
      iglob = local_to_glob(iloc, i, Mesh.ndof);
      B[iglob] =  alpha * int_PincP_T(iloc, i, Mesh, chaine);
    }
  }
  
}


void Build_VectorPincP_T(cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { 
      iglob = local_to_glob(iloc, i, Mesh.ndof);
      B[iglob] = int_PincP_T_Hetero(iloc, i, Mesh, chaine);
    }
  }
  
}

 

/*----------------------------------------------------------------------- Pinc V ---------------------------------------------------*/

cplx int_PincV_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Pinc * conj(V'_T)n_T sur les faces exterieures de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    
   iloc     : numero de l'onde pour la fonction-test
   e        : numero de l'élément
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;

      din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (Mesh.elements[e].voisin[l] < 0){
      I += din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}



cplx int_PincV_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Pinc * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
  
   iloc     : numero de l'onde pour la fonction-test
   e        : numero de l'élément
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc


  //Partie du domaine 1 : (Pinc + R * P_ref) * V'_T n_T
  if (Mesh.elements[e].get_ref() == 1) {

    cplx I1 = cplx(0.0, 0.0);//Pour stocker l'integrale de Pinc * V'_T n_T sur une face 
    cplx I2 = cplx(0.0, 0.0);//........................... P_ref * V'_T n_T ..........
    
    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){

	if (l == 0) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 1) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 2) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 3) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
	  
	}
	
	I1 = din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
	
	I2 = din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_r], chaine);
	
	I += Mesh.elements[e].alpha_PincV[l] * (I1 + Mesh.R * I2);
      }
    }
    
  }
  
  //Partie du domaine 2 : T * P_t * V'_T n_T
  else {
    
    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	
	if (l == 0) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 1) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 2) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 3) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
	  
	}
      
	
	I += Mesh.elements[e].alpha_PincV[l] * Mesh.T * din * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
      }
    }

    
  }
  
  return I;
}



void Build_VectorPincV_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha    : scalaire
    B        : tableau ou stocké le vecteur
    Mesh     : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;

    for (i = 0; i < Mesh.get_N_elements(); i++) {
      for (iloc = 0; iloc < Mesh.ndof; iloc++) { 
	iglob = local_to_glob(iloc, i, Mesh.ndof);
	B[iglob] =  alpha * int_PincV_T(iloc, i, Mesh, chaine);
      }
    }
    
}
  

void Build_VectorPincV_T(cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B        : tableau ou stocké le vecteur
    Mesh     : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) { 
      iglob = local_to_glob(iloc, i, Mesh.ndof);
      B[iglob] = int_PincV_T_Hetero(iloc, i, Mesh, chaine);
    }
  }
  
}

/*---------------------------------------------------------------------- Vinc P ----------------------------------------------------*/

cplx int_VincP_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Vinc n * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e        : numero de l'élément
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*dinc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde incidente
  
  for (l = 0; l < 4; l++) {
    
    if (Mesh.elements[e].voisin[l] < 0){
      
      if (l == 0) {
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      else if (l == 1) {
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      else if (l == 2) {
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
      }
      else if (l == 3) {
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;      
	
      }
      
      
      I +=  djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}



cplx int_VincP_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de Vinc n_T * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e        : numero de l'élément
   Mesh     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn, drn;
  //djn = A*djloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc

  if (Mesh.elements[e].get_ref() == 1) {

    cplx I1 = cplx(0.0, 0.0);
    cplx I2 = cplx(0.0, 0.0);
    
    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	if (l == 0) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	else if (l == 1) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	else if (l == 2) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	else if (l == 3) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;      

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	}
      
      
	I1 =  djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);

      I2 =  drn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_r]);

      I += Mesh.elements[e].alpha_VincP[l] * (I1 + Mesh.R * I2);
      }
    }
  }

  else {
    
    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	if (l == 0) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	}
	else if (l == 1) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	}
	else if (l == 2) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	}
	else if (l == 3) {
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;      
	  
	}
      
	
	I += Mesh.elements[e].alpha_VincP[l] * Mesh.T * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
      }
    }
    
  }
  return I;
}



void Build_VectorVincP_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input --------------------------------------------------
    alpha    : scalaire
    B        : tableau ou stocké le vecteur
    Mesh     : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;

  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  alpha * int_VincP_T(iloc, i, Mesh, chaine);
    }
  }
  
}


void Build_VectorVincP_T(cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input -------------------------------------------------
    B        : tableau ou stocké le vecteur
    Mesh     : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  int_VincP_T_Hetero(iloc, i, Mesh, chaine);
    }
  }
  
}



/*---------------------------------------------------------------- Vinc V ---------------------------------------------------------*/

cplx int_VincV_T(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {
    
    if (Mesh.elements[e].voisin[l] < 0){

      if (l == 0) {
	din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
	din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 1) {
	din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
	din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 2) {
	din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
	din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 3) {
	din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
	
	djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	
	djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	
	din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
	
      }
      
      I +=  din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
    }
  }
  
  return I;
}


cplx int_VincV_T_Hetero(MKL_INT iloc, MKL_INT e, Maillage &Mesh, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e     : numero de l'élément
   Mesh  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn, drn;
  //din = A*diloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*d_Mesh.n_i\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et à la direction de l'onde incidente
  //drn = A*d_Mesh.n_r\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et à la direction de l'onde réfléchie

  //Dans le domaine 1 : (Vinc n_T + R * V_ref n_T) V'_T n_T
  if (Mesh.elements[e].get_ref() == 1) {
    
    cplx I1 = cplx(0.0, 0.0);
    cplx I2 = cplx(0.0, 0.0);
    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	if (l == 0) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n1, 1) ;

	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	}
	else if (l == 1) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n2, 1) ;

	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	else if (l == 2) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n3, 1) ;

	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	else if (l == 3) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;

	  drn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_r].Ad, 1, Mesh.elements[e].n4, 1) ;

	  drn = drn * Mesh.elements[e].OP[Mesh.n_r].ksurOmega;
	  
	}
	
	I1 =  din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
	
	I2 =  din * drn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_r], chaine);
	
	I += Mesh.elements[e].alpha_VincV[l] * (I1 + Mesh.R * I2);
      }
    }
    
  }

  //Dans le domaine 2 : T * V_t n_T V'_T n_T
  else {

    for (l = 0; l < 4; l++) {
      
      if (Mesh.elements[e].voisin[l] < 0){
	
	if (l == 0) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n1, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 1) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n2, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 2) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n3, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;
	  
	}
	else if (l == 3) {
	  din = cblas_ddot(3, Mesh.elements[e].OP[iloc].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = cblas_ddot(3, Mesh.elements[e].OP[Mesh.n_i].Ad, 1, Mesh.elements[e].n4, 1) ;
	  
	  djn = djn * Mesh.elements[e].OP[Mesh.n_i].ksurOmega;
	  
	  din = din * Mesh.elements[e].OP[iloc].ksurOmega;      
	  
	}
	
	I +=  Mesh.elements[e].alpha_VincV[l] * Mesh.T * din * djn * integrale_triangle(Mesh.elements[e].face[l], Mesh.elements[e].OP[iloc], Mesh.elements[e].OP[Mesh.n_i], chaine);
      }
    }
  }
  
  return I;
}



void Build_VectorVincV_T(double alpha, cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  alpha * int_VincV_T(iloc, i, Mesh, chaine);
    }
  }
}



void Build_VectorVincV_T(cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  int_VincV_T_Hetero(iloc, i, Mesh, chaine);
    }
  }
}


void Build_SUM_F5_to_F8(cplx *B, Maillage &Mesh, const char chaine[]){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  int_PincP_T_Hetero(iloc, i, Mesh, chaine) + int_PincV_T_Hetero(iloc, i, Mesh, chaine)
	+ int_VincP_T_Hetero(iloc, i, Mesh, chaine) + int_VincV_T_Hetero(iloc, i, Mesh, chaine);
    }
  }
}


/*---------------------------------------------------------------------- [V] {P} + [P] {V} --------------------------------------------------*/

void BlocIntPV_VP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {
    
    BlocIntVP(i, Mesh.tab_VP[0], Mesh.tab_VP[1], A_glob, Mesh, chaine);

    BlocIntPV(i, Mesh.tab_PV[0], Mesh.tab_PV[1], A_glob, Mesh, chaine);
  }
}


void BlocIntPV_VP_II(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      BlocIntVP(i, j, Mesh.tab_VP[0], Mesh.tab_VP[1], A_glob, Mesh, chaine);
      
      BlocIntPV(i, j, Mesh.tab_PV[0], Mesh.tab_PV[1], A_glob, Mesh, chaine);
    }
  }
}



void BlocExtPV_VP(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {

    BlocExtPV_Add(i, Mesh.tab_PV[3], A_glob, Mesh, chaine);

    BlocExtVP_Add(i, Mesh.tab_VP[3], A_glob, Mesh, chaine);
  }
}


void Build_VectorSum_F_inc(cplx *B, Maillage &Mesh, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc P + Pinc V + Vinc * P + Vinc V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    Mesh    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, Mesh.ndof); 
      B[iglob] =  Mesh.tab_inc[0] * int_PincP_T(iloc, i, Mesh, chaine) + Mesh.tab_inc[1] * int_PincV_T(iloc, i, Mesh, chaine) + Mesh.tab_inc[2] * int_VincP_T(iloc, i, Mesh, chaine) + Mesh.tab_inc[3] * int_VincV_T(iloc, i, Mesh, chaine);
    }
  }
}



void BuildMatBlocByBloc(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {

    //Blocs intérieurs
    BlocIntPP(i, Mesh.tab_PP[0], Mesh.tab_PP[1], A_glob, Mesh, chaine);

    BlocIntPV(i, Mesh.tab_PV[0], Mesh.tab_PV[1], A_glob, Mesh, chaine);

    BlocIntVP(i, Mesh.tab_VP[0], Mesh.tab_VP[1], A_glob, Mesh, chaine);

    BlocIntVV(i, Mesh.tab_VV[0], Mesh.tab_VV[1], A_glob, Mesh, chaine);

    //Blocs extérieurs
    BlocExtPP_Add(i, Mesh.tab_PP[3], A_glob, Mesh, chaine);

    BlocExtPV_Add(i, Mesh.tab_PV[3], A_glob, Mesh, chaine);

    BlocExtVP_Add(i, Mesh.tab_VP[3], A_glob, Mesh, chaine);

    BlocExtVV_Add(i, Mesh.tab_VV[3], A_glob, Mesh, chaine);
  }
}


void BuildMatBlocByBloc_M(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {

    //Blocs intérieurs
    BlocIntPP(i, Mesh.M_PP[0], Mesh.M_PP[1], A_glob, Mesh, chaine);

    BlocIntPV(i, Mesh.M_PV[0], Mesh.M_PV[1], A_glob, Mesh, chaine);

    BlocIntVP(i, Mesh.M_VP[0], Mesh.M_VP[1], A_glob, Mesh, chaine);

    BlocIntVV(i, Mesh.M_VV[0], Mesh.M_VV[1], A_glob, Mesh, chaine);

    //Blocs extérieurs
    BlocExtPP_Add(i, Mesh.M_PP[3], A_glob, Mesh, chaine);

    BlocExtPV_Add(i, Mesh.M_PV[3], A_glob, Mesh, chaine);

    BlocExtVP_Add(i, Mesh.M_VP[3], A_glob, Mesh, chaine);

    BlocExtVV_Add(i, Mesh.M_VV[3], A_glob, Mesh, chaine);
  }
}


void BuildMatBlocByBloc_N(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  
  for (MKL_INT i = 0; i < Mesh.get_N_elements(); i++) {

    //Blocs intérieurs
    BlocIntPP(i, Mesh.N_PP[0], Mesh.N_PP[1], A_glob, Mesh, chaine);

    BlocIntPV(i, Mesh.N_PV[0], Mesh.N_PV[1], A_glob, Mesh, chaine);

    BlocIntVP(i, Mesh.N_VP[0], Mesh.N_VP[1], A_glob, Mesh, chaine);

    BlocIntVV(i, Mesh.N_VV[0], Mesh.N_VV[1], A_glob, Mesh, chaine);

    //Blocs extérieurs
    BlocExtPP_Add(i, Mesh.N_PP[3], A_glob, Mesh, chaine);

    BlocExtPV_Add(i, Mesh.N_PV[3], A_glob, Mesh, chaine);

    BlocExtVP_Add(i, Mesh.N_VP[3], A_glob, Mesh, chaine);

    BlocExtVV_Add(i, Mesh.N_VV[3], A_glob, Mesh, chaine);
  }
}


/*--------------------------------------------------------------------- PARTIE SOLUTION ANALYTIQUE HETEROGENE ---------------------------------------------------*/

MailleSphere::MailleSphere() {
	MKL_INT print = 0;
	MKL_INT taille_loc;

	omega = 2 * M_PI;
	r1 = 1.0;
	r2 = 1.0;
	a1 = 2.0;
	a2 = 2.0;//2.0
	xi1 = 2.0;
	xi2 = 2.0;//2.0
	k1 = sqrt(xi1 / a1) * omega;
	k2 = sqrt(xi2 / a2) * omega;
	Y_omega = 1;
	X0[0] = 1.5; X0[1] = 1.0; X0[2] = 0.0;

	/********************************************* quadrature de Gauss-Legendre *******************************************/
	//1D
	n_gL = 12;
	
	X1d = new double[n_gL];
	W1d = new double[n_gL];
	quad_Gauss_1D();
	
	//2D
	
	Qd2D.ordre = n_gL;
	
	Qd2D.Allocate_quad_2D();
	
	Qd2D.quad_gauss_triangle(X1d, W1d);
	
	//3D
	Qd.ordre = n_gL;
	
	Qd.Allocate_quad();
	
	Qd.quad_gauss_tetra(X1d, W1d);
	
	/**********************************************************************************************************************/
	
	n_i = 5;
	
	cout<<"lecture du fichier des points "<<endl;
	// lecture du fichier maillage sommets.txt
	points = Read_Points2(&N_sommets);
	cout<<"lecture réussi pour les points "<<endl;

	
	// lecture du fichier maillage tetra.txt
	cout<<"lecture du tableau des tetra "<<endl;
	tetra = Read_Tetra2(&N_elements);
	cout<<"lecture réussi pour les tetra "<<endl;
	  
	// lecture du fichier maillage voisin.txt
	cout<<" lecture du tableau des voisins "<<endl;
	voisin = Read_Neighbours_Tetra2();
	cout<<"lecture réussi pour les voisins des tetra "<<endl;
	
	//variable permettant de définir le nombre d'ondes planes par élément
	ordre_onde = 1;
	//ndof = 6;
	
	//nombre d'ondes planes par élément
	//ndof =  (2 * ordre_onde + 1) * (2 * ordre_onde + 1) * (2 * ordre_onde + 1) -  (2 * ordre_onde - 1) * (2 * ordre_onde - 1) * (2 * ordre_onde - 1);
	ndof = Defined_dimension_of_Trefftz_basis(ordre_onde);
      
	//initialisation du tableau DirCub
	Dir = new double[ndof * 3];

	//remplissage du tableau des directions sur un cube
	directionInCube_complete_version(Dir, ordre_onde);

	lambda_max = 0.0;
	lambda_min = 1000.0;
	lambda_max_normalisee = 0.0;
	lambda_min_normalisee = 1000.0;
	eps_0 = 1E-6;
	eps_reg_Tikhonov = 1E-10;
	SIZE = new MKL_INT[N_elements];

	//calcul des coefficients de la solution analytique de Bessel
	//compute_coeff_hetero_Analytique_Bessel();
	cout<<"k1 = "<<k1<<" k2 = "<<k2<<endl;
	cout << "creation et affichage du tableau contenant la liste des sommets" << endl;
	Create_Xsommets(points);
	cout << "creation du tableau avec la liste des voisins" << endl;
	Create_ListeVoisin(voisin);
	cout << "Creation du tableau loc to glob" << endl;
	Create_Elem2sommet(tetra); 
	cout << "creation de l'objet qui permet de lire facilement la liste des sommets" << endl;
	build_sommet();
	cout << "creation de l'objet qui permet de lire facilement la liste des elements" << endl;

	//trie des éléments du maillage
	tools_to_define_sorted_mesh();

	//création du nouveau tableau des éléments triés
	build_NewListeVoisin();

	//définition des indices globaux des éléments
        build_newloc2glob();

	//mise en place de la nouvelle structure de l'objet elements
	build_element(1);
	
	if (print == 1) {
		print_info();
		print_loc2glob();
		print_ListeVoisin();
		test_build_sommet();
		test_build_element();
	}
	build_face("supra");
	
}
/*-------------------------------------------------------------------------------------------------
-------------*/

void get_mesh_size_and_Trefftz_basis_dimension(char chaine_points[], char chaine_tetra[],  char type_espace_phases[], char phase_tag[], MKL_INT *np, MKL_INT *N_elements, MKL_INT *ndof){

   *np = Read_number_of_elements(chaine_points);
  
  *N_elements = Read_number_of_elements(chaine_tetra);
  
  if (strcmp(type_espace_phases, "sphere") == 0){
    
    *ndof = Read_number_of_elements(phase_tag);
   
  }
  else {
    MKL_INT ordre_onde = strtod(phase_tag, nullptr);
    //nombre d'ondes planes par élément
    *ndof = Defined_dimension_of_Trefftz_basis(ordre_onde);
  }
  
}


void build_mesh(char chaine_points[], char chaine_tetra[], char chaine_voisins[], double *points, MKL_INT *tetra, MKL_INT *voisin){

  MKL_INT np, nt;
  cout<<"lecture du fichier des points "<<endl;
  // lecture du fichier maillage sommets.txt
  np = Read_number_of_elements(chaine_points);
  
  Read_Points(chaine_points, np, points);
  cout<<"lecture réussi pour les points "<<endl;
  
  
  // lecture du fichier maillage tetra.txt
  cout<<"lecture du tableau des tetra "<<endl;
  nt = Read_number_of_elements(chaine_tetra);
  
  Read_Tetra(chaine_tetra, nt, tetra);
  cout<<"lecture réussi pour les tetra "<<endl;
  
  // lecture du fichier maillage voisin.txt
  cout<<" lecture du tableau des voisins "<<endl;
  
  Read_Tetra(chaine_voisins, nt, voisin);
  cout<<"lecture réussi pour les voisins des tetra "<<endl;
}


void build_direction_of_space_phases(char type_espace_phases[], char phase_tag[], MKL_INT ndof, double *Dir){
  
  if (strcmp(type_espace_phases, "sphere") == 0){
    Read_Points(phase_tag, ndof, Dir);
  }
  else {
    MKL_INT ordre_onde = strtod(phase_tag, nullptr);
    
    //remplissage du tableau des directions sur un cube
    directionInCube_complete_version(Dir, ordre_onde);
  }
}


void quad_Gauss_1D(MKL_INT n_gL, double *X1d, double *W1d) {
  //Cette fonction calcule les points de Gauss-Legendre 1D
  //Dans un premier temps, la quadrature est sur [-1 1]
  //C'est à la fin qu'elle est estimée sur [0 1] par un changement de variable
  /*---------------------------------------------------------------------------------------------------------
    A et D des matrices qui contiennent les polynômes de Gauss-Legendre
    X1d : les points de Gauss-Legendre
    W1d : les poids asscoiés
    /*---------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *D = nullptr, *A = nullptr;
  D = new double[n_gL * n_gL];
  A = new double[n_gL * n_gL];
  
  for (i = 0; i < n_gL; i++){
    if (i % 2 == 0) {
      W1d[i] = 2 / (double) (i+1);
    }
    else{
      W1d[i] = 0.0;
    }
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = 0.0;
      D[i * n_gL + j] = 0.0;
    }
  }

  for (i = 0; i < n_gL-1; i++){
    A[i*n_gL + (i+1)] = i+1;
    A[(i+1)*n_gL + i] = i+1;
    D[i * n_gL + i] = 2 * (i+1) - 1;
  }
  D[(n_gL-1) * n_gL + (n_gL-1)] = 2 * n_gL - 1;

  double *alphar = nullptr, *alphai = nullptr, *beta = nullptr;
  alphar = new double[ n_gL];
  alphai = new double[ n_gL];
  beta = new double[ n_gL];

  double *work = nullptr;
  work = new double[ n_gL];
  MKL_INT info, ldr, ldl;
  ldl = n_gL;
  ldr = n_gL;

  double *vsl = nullptr, *vsr = nullptr;
  
  vsl = new double[ldl * n_gL];
  vsr = new double[ldr * n_gL];

  //Calcul des valeurs propres de (A,D)
  info = LAPACKE_dggev(LAPACK_COL_MAJOR, 'V', 'V', n_gL, A, n_gL, D, n_gL, alphar, alphai, beta, vsl, n_gL, vsr, n_gL);

  cout<<"info = "<<info<<endl;
 
  //calcul des points de Gauss
  cout<<"eigen(A,D) : "<<endl;
  for (i = 0; i < n_gL; i++){
    X1d[i] = alphar[i] / beta[i];
  }


  for (i = 0; i < n_gL; i++){
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = pow(X1d[j],i);
    }
  }


  //calcul des poids de Gauss
  MKL_INT *ipiv = nullptr;
  ipiv = new MKL_INT[n_gL];
   info = LAPACKE_dgetrf( LAPACK_COL_MAJOR, n_gL, n_gL, A, n_gL, ipiv);

   MKL_INT nrhs = 1;
   if (info != 0) {
     cout << "une erreur est renvoyée : "<< info << endl << endl;
   }
   else{
     cout << "résolution du système..."<< endl << endl;
     
     info = LAPACKE_dgetrs( LAPACK_COL_MAJOR, 'T', n_gL, nrhs, A, n_gL, ipiv, W1d, n_gL);
     cout<<"info = "<<info<<endl;
   }

   cout<<"affichage des poids : "<<endl;

  
   //adaption à l'intervalle [0 1]
   for (i = 0; i < n_gL; i++){
     X1d[i] = (X1d[i] + 1) * 0.5;
     W1d[i] *= 0.5;
     
   }

   cout<<" liberation de la mémoire des points :"<<endl;
   
   delete [] work; work  =  nullptr;
   delete [] beta; beta  =  nullptr;
   delete [] alphar; alphar  =  nullptr;
   delete [] alphai; alphai  =  nullptr;
   delete [] ipiv; ipiv  =  nullptr;
   delete [] vsr; vsr  =  nullptr;
   delete [] vsl; vsl  =  nullptr;
   
   delete [] A; A  =  nullptr;
   delete [] D; D  =  nullptr;
   
}

 

MailleSphere::MailleSphere(double epsilon_0, double epsilon_reg_Tikhonov, double np, double *points_ext, MKL_INT nt, MKL_INT *tetra_ext, MKL_INT *voisin_ext, Sommet *sommets_ext, MKL_INT n_ddl, double *Dir_ext, MKL_INT n_quad, double *X1d_ext, double *W1d_ext, double *X2d, double *W2d, double *X3d, double *W3d) {
	MKL_INT taille_loc;
	
	frq = 1.0 /(2 * M_PI) ;
	omega = 2 * M_PI * frq;
	r1 = 10.0;
	r2 = 10.0;
	a1 = 3.0;
	a2 = 3.0;//2.0
	b1 = 1.0;
	b2 = 1.0;//2.0
	xi1 = 2.0;
	xi2 = 2.0;//2.0
	k1 = sqrt(xi1 / a1) * omega;
	k2 = sqrt(xi2 / a2) * omega;
	Y_omega = 1;
	X0[0] = 30; X0[1] = 15; X0[2] = 0.0;
	
	/*
	frq = 1.0 /(2 * M_PI) ;
	omega = 2 * M_PI * frq;
	r1 = 5.0;
	r2 = 5.0;
	a1 = 3.0;
	a2 = 3.0;//2.0
	b1 = 1.0;
	b2 = 1.0;//2.0
	xi1 = 2.0;
	xi2 = 2.0;//2.0
	k1 = sqrt(xi1 / a1) * omega;
	k2 = sqrt(xi2 / a2) * omega;
	Y_omega = 1;
	X0[0] = 30.0; X0[1] = 15; X0[2] = 0.0;
	*/
	/*
	frq = 1.0 /(2 * M_PI) ;
	omega = 2 * M_PI * frq;
	r1 = 2.0;
	r2 = 2.0;
	a1 = 3.0;
	a2 = 3.0;//2.0
	b1 = 1.0;
	b2 = 1.0;//2.0
	xi1 = 2.0;
	xi2 = 2.0;//2.0
	k1 = sqrt(xi1 / a1) * omega;
	k2 = sqrt(xi2 / a2) * omega;
	Y_omega = 1;
	X0[0] = 30.0 ; X0[1] = 15; X0[2] = 0.0;
	*/
	/*
	frq = 1.0 /(2 * M_PI);
	omega = 2 * M_PI * frq;
	r1 = 1.0;
	r2 = 1.0;
	a1 = 3.0;
	a2 = 3.0;//3.0
	b1 = 1.0;
	b2 = 1.0;//1.0
	xi1 = 2.0;
	xi2 = 2.0;//2.0
	k1 = sqrt(xi1 / a1) * omega;
	k2 = sqrt(xi2 / a2) * omega;
	Y_omega = 1;
	X0[0] = 30.0 ; X0[1] = 15; X0[2] = 0.0;
	*/
	/********************************************* quadrature de Gauss-Legendre *******************************************/
	//1D
	n_gL = n_quad;
	
	X1d = X1d_ext;
	W1d = W1d_ext;
	//quad_Gauss_1D();
	
	//2D
	
	Qd2D.ordre = n_gL;
	
	Qd2D.Allocate_quad_2D(X2d, W2d);
	
	Qd2D.quad_gauss_triangle(X1d, W1d);
	
	//3D
	Qd.ordre = n_gL;
	
	Qd.Allocate_quad(X3d, W3d);
	
	Qd.quad_gauss_tetra(X1d, W1d);
	
	/**********************************************************************************************************************/
	
	n_i = 5;
	
	N_sommets = np; //Read_number_of_elements(chaine_points);
	
	N_elements = nt;//Read_number_of_elements(chaine_tetra);
       
	ndof = n_ddl;
	
	cout<<" ndof = "<<ndof<<endl;
	Dir = Dir_ext;
	
	lambda_max = 0.0;
	lambda_min = 1000.0;
	lambda_max_normalisee = 0.0;
	lambda_min_normalisee = 1000.0;
	eps_0 = epsilon_0;
	eps_reg_Tikhonov = epsilon_reg_Tikhonov;	

	cout<<"k1 = "<<k1<<" k2 = "<<k2<<endl;

	cout << "creation et affichage du tableau contenant la liste des sommets" << endl;
	Create_Xsommets(points_ext);
	cout << "creation du tableau avec la liste des voisins" << endl;
	Create_ListeVoisin(voisin_ext);
	cout << "Creation du tableau loc to glob" << endl;
	Create_Elem2sommet(tetra_ext);

	sommets = sommets_ext;
	
	cout << "creation de l'objet qui permet de lire facilement la liste des sommets" << endl;
	build_sommet();
	cout << "creation de l'objet qui permet de lire facilement la liste des elements" << endl;
	
}


void MailleSphere::build_elements_and_faces(char chaine_method[], char which_h[], MKL_INT filtre, double *third_coord, MKL_INT *new2old_ext, MKL_INT *old2new_ext, MKL_INT *NewListeVoisin_ext, MKL_INT *newloc2glob_ext, Element *elements_ext, Face *face_double_ext, MKL_INT *Tri_ext, Face *face_ext, MKL_INT *cumul_NRED, double *virtual_points, Sommet *virtual_sommets, Onde_plane *virtual_plane_wave, char type_Sol[]){

       

        MKL_INT print = 0;
	Z_XG = third_coord;
	new2old = new2old_ext;
	old2new = old2new_ext;

	NewListeVoisin = NewListeVoisin_ext;

	newloc2glob = newloc2glob_ext;
	
	elements = elements_ext;

	face_double = face_double_ext;

	Tri = Tri_ext;

	face = face_ext;
	
	//trie des éléments du maillage
	tools_to_define_sorted_mesh();

	cout<<"création du nouveau tableau des elements triés"<<endl;
	build_NewListeVoisin();

	cout<<"définition des indices globaux des elements"<<endl;
        build_newloc2glob();

	cout<<"mise en place de la nouvelle structure de l'objet elements virtuel :"<<endl;
	build_element(filtre);

	SIZE = cumul_NRED;

	strcpy(forme_Sol,type_Sol);

	noeud_fixe_element = virtual_points;
	sommets_fixe_element = virtual_sommets;
	fixe_element.OP = virtual_plane_wave;
	
	//construction de l'element virtuel
	build_fixe_element(which_h, filtre);

	
	if (print == 1) {
		print_info();
		print_loc2glob();
		print_ListeVoisin();
		test_build_sommet();
		test_build_element();
	}
	build_face(chaine_method);
}


void MailleSphere::print_info() {
	cout << "N_sommets = " << N_sommets << ", N_elements = " << N_elements << endl;
}
void MailleSphere::set_N_sommets(MKL_INT N){
	N_sommets = N;
}
MKL_INT MailleSphere::get_N_sommets() {
	return N_sommets;
}
void MailleSphere::set_N_elements(MKL_INT N) {
	N_elements = N;
}
MKL_INT MailleSphere::get_N_elements() {
	return N_elements;
}
void MailleSphere::Create_Xsommets(double *points)
{
	Xsommet = points;
}
void MailleSphere::print_Xsommets(){
	for (MKL_INT i = 0; i < N_sommets; i++) {
		cout << "Numero du sommet = " << i << " : X1 = " << Xsommet[i] << " : X2 = " << Xsommet[i + 1] << " : X3 = " << Xsommet[i + 2] << endl;
	}
	cout << endl;
}
void MailleSphere::Create_Elem2sommet(MKL_INT *tetra)
{

  oldloc2glob = tetra;
}
void MailleSphere::print_loc2glob() {
	for (MKL_INT i = 0; i < N_elements; i++) {
		cout << "Numero de l element = " << i << " : i0 = " << oldloc2glob[4 * i] << " : i1 = " << oldloc2glob[4 * i + 1] << " : i2 = " << oldloc2glob[4 * i + 2] << " : i3 = " << oldloc2glob[4 * i + 3] << endl;
	}
	cout << endl;
}
void MailleSphere::Create_ListeVoisin(MKL_INT *voisin)
{
	OldListeVoisin = voisin;
}
void MailleSphere::print_ListeVoisin() {
	for (MKL_INT i = 0; i < N_elements; i++) {
		cout << "Numero de l element = " << i << " : Voisin 0 = " << OldListeVoisin[4 * i] << " : Voisin 1 = " << OldListeVoisin[4 * i +1] << " : Voisin 2 = " << OldListeVoisin[4 * i+2] << " : Voisin 3 = " << OldListeVoisin[4 * i+3] << endl;
	}
	cout << endl;
}

void MailleSphere::build_sommet()
{
  //sommets = new Sommet[N_sommets];

	for (MKL_INT i = 0; i < N_sommets; i++) {
		sommets[i].set_X(Xsommet+3*i);
		sommets[i].set_ref(i);
	}
}
void MailleSphere::test_build_sommet()
{
	for (MKL_INT i = 0; i < N_sommets; i++) {
		sommets[i].print_info();
	}
}
void MailleSphere::build_face(const char * chaine)
{
        double *XX = nullptr, *YY = nullptr, *ZZ = nullptr;//pour faciliter le calcul
	                                                   //d'aire des triangle
	XX = new double[3];  YY = new double[3];  ZZ = new double[3];
        cout << "Construction de la liste des faces" << endl;
	N_Face_double = N_elements * 4;
	//face_double = new Face[N_Face_double];
	MKL_INT l = 0;
	for (MKL_INT i = 0; i < N_elements; i++) {
		for (MKL_INT j = 0; j < 4; j++) {
			face_double[l] = elements[i].face[j];
			l++;
		}
	}
	cout<<" nbre Elem: "<<N_elements<<endl;
	//Tri = new MKL_INT[N_Face_double];
	
	for (l = 0; l < N_Face_double; l++) {
		Tri[l] = l;
	}
	// Determination de l'ordre des faces
	Tri_rapid(0, N_Face_double-1);

	N_Face = 1	; // Compteur du nombre de faces distinctes
	MKL_INT test;
	for (MKL_INT i = 0; i < N_Face_double-1; i++) {
		 test = ordre(face_double[Tri[i]].XG, face_double[Tri[i + 1]].XG, 3);
		 if (test == 2) N_Face++;
	}
	//face = new Face[N_Face];
	face[0] = face_double[Tri[0]];
       
	face_double[Tri[0]].set_ref(0);
	MKL_INT compteur = 0;
	MKL_INT n_elt;
	MKL_INT n_face;
	for (MKL_INT i = 0; i < N_Face_double - 1; i++) {
		test = ordre(face_double[Tri[i]].XG, face_double[Tri[i + 1]].XG, 3);
		if (test == 2) {
			compteur++;
		}
		if (test == 1) {
			cout << "BUG dans Source_Mesh.cpp : problème dans le tri des faces" << endl;
		}
		face_double[Tri[i + 1]].set_ref(compteur);
		if (test == 2) {
			face[compteur] = face_double[Tri[i + 1]];
		}

//		n_elt = Tri[i]/4;
//		n_face = Tri[i]%4;
//		elements[n_elt].face[n_face].set_ref(compteur);
	}
	l = 0;
   
	for (MKL_INT i = 0; i < N_elements; i++) {
	  for (MKL_INT j = 0; j < 4; j++) {
	    
	    face_double[l].compute_Aire_triangle(XX, YY, ZZ);
	    elements[i].face[j]= face_double[l] ;
	    l++;
	  } 
	}

	//Définitions des coefficients du système linéaire
	build_coefficients_of_linear_global_system(chaine);

	
	//	test_build_element();
	//	test_build_face();
	cout<<" construction de la liste face reussie "<<endl;
	delete [] XX; XX = 0;
	delete [] YY; YY = 0;
	delete [] ZZ; ZZ = 0;
}
void MailleSphere::test_build_face() {
  for (MKL_INT i = 0; i < N_Face ; i++) {
    face[i].print_info();
  }
}
void MailleSphere::Tri_rapid(MKL_INT i1, MKL_INT i2) {
	MKL_INT tampon;
	MKL_INT pivot = Tri[i1];
//	Tri[(i1 + i2) / 2] = Tri[i1];
//	Tri[i1]=pivot;
	MKL_INT idebut = i1+1;
	MKL_INT ifin = i2;
//	cout << i1 << i2 << endl;
	if (i1<i2){
		for (MKL_INT i = i1; i < i2; i++)
		{ 

				if (ordre(face_double[Tri[idebut]].XG, face_double[pivot].XG, 3) == 2)
				{
					Tri[idebut-1] = Tri[idebut];
					idebut++;
				}
				else {
					tampon = Tri[ifin];
					Tri[ifin] = Tri[idebut];
					Tri[idebut] = tampon;
					ifin--;
				}
		}
		Tri[idebut-1] = pivot;
		Tri_rapid(i1, idebut -2 );
		Tri_rapid(idebut, i2);
	}

}
 
void MailleSphere::tools_to_define_sorted_mesh() {
  MKL_INT i, j;
  
  if (Z_XG != 0) {
  for(i = 0; i < N_elements; i++) {
    Z_XG[i] = 0.0;
  }

  for(i = 0; i < N_elements; i++) {
    for(j = 0; j < 4; j++) {
      Z_XG[i] += (sommets[oldloc2glob[4 * i + j]].get_X())[2];
    }
    Z_XG[i] = Z_XG[i] / 4.0;
  }

  if ((new2old == 0) || (old2new == 0)){
    cout<<" old2new ou new2old n'est pas correctement alloue"<<endl;
    exit(0);
  }
  
  tri_elements(N_elements, new2old, Z_XG);
   
  for(i = 0; i < N_elements; i++){
    j = new2old[i];
    old2new[j] = i;
  }
  }
  else{
    cout<<" Z_XG n'est pas correctement alloue"<<endl;
    exit(0);
  }
  /*
  FILE* fic;
  
  if( !(fic = fopen("data/tools/coord.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  for(i = 0; i < N_elements; i++) {
    j = old2new[i];
    fprintf(fic,"%d\t %d\t %10.15e\n",i, j, Z_XG[j]);
  }
  fclose(fic);
  */
}


void MailleSphere::build_NewListeVoisin(){
  MKL_INT e_old, e_new, j, e_old_v, e_new_v;
  
  for(e_old = 0; e_old < N_elements; e_old++) {
    
    e_new = old2new[e_old];
    
    for(j = 0; j < 4; j++) {
      e_old_v = OldListeVoisin[4 * e_old + j];
      
      if (e_old_v == -1){
	e_new_v = -1;
      }
      else{
	e_new_v = old2new[e_old_v];
      }
      
      NewListeVoisin[4 * e_new + j] = e_new_v;
    }
   
  }
 
}


void MailleSphere::build_newloc2glob(){
  MKL_INT e_old, e_new, j, x;

 
  
  for(e_old = 0; e_old < N_elements; e_old++) {
    
    e_new = old2new[e_old];

    for(j = 0; j < 4 ; j++) {
      
      x = oldloc2glob[e_old * 4 + j];
      
      newloc2glob[e_new * 4 + j] = x;
    }
  } 
  
}


void MailleSphere::build_element(MKL_INT choice)
// choice = pour choisir le mode de selection des valeurs propres où le mode de régularisation
// si choice = 0, alors le mode considéré vérifie sur chaque élément T, eps_max = eps * |T|^{2/3}
// avec |T| le volume de l'élément T. C'est un critère de selection surfacique. Pour toute autre
// valeurs de choice, le mode satisfait eps_max = eps * |T| qui est un critère de régulation volumique
{
  MKL_INT ll, s, e_new;
  double *H = nullptr, *w = nullptr, *Am = nullptr;
  double *XX = nullptr, *YY = nullptr, *ZZ = nullptr, *XYZ = nullptr, *ZX = nullptr;
  
        H = new double[9];  w = new double [3];//pour diagonaliser la matrice associée
	                                        // l'élément
	XX = new double[3]; YY = new double [3];// utiliser pour calculer
	ZZ = new double [3]; XYZ = new double [3];//le volume de chaque élément
	ZX = new double [3];
	Am = new double[9];
	//elements = new Element[N_elements];
	for (e_new = 0; e_new < N_elements; e_new++) {
		elements[e_new].set_ref(e_new);
	}
	for (e_new = 0; e_new < N_elements; e_new++) {

		for (MKL_INT j = 0; j < 4; j++) {
		       elements[e_new].noeud[j] = sommets + newloc2glob[4 * e_new + j];
		       elements[e_new].voisin[j] = NewListeVoisin[4 * e_new + j];
		       elements[e_new].face[j].set_ref(-1);
		       
		       	if (e_new > NewListeVoisin[4 * e_new + j])
			{
				elements[e_new].face[j].voisin1 = e_new;
				elements[e_new].face[j].voisin2 = NewListeVoisin[4 * e_new + j];
			}
			if (e_new < NewListeVoisin[4 * e_new + j])
			{
				elements[e_new].face[j].voisin1 = NewListeVoisin[4 * e_new + j];
				elements[e_new].face[j].voisin2 = e_new; 
			}
			if (e_new == NewListeVoisin[4 * e_new + j])
			{
			  //print_ListeVoisin();
				cout << "voisin 1 =" << endl;
				cout << "BUG Dans Source_Mesh.cpp affectation voisin1 et voisin2"<<endl;
				exit(1);
			}
		       
		       ll = 0;
		       for (MKL_INT l = 0; l < 4; l++) {
				if (j != l) {
				  elements[e_new].face[j].noeud[ll] = sommets + newloc2glob[4 * e_new + l];
					ll++;
				}
			}
			elements[e_new].face[j].compute_XG();
		}

		//calcule du centre de gravité du tetra i
		elements[e_new].compute_XG();
		
		//calcule du volume du tetra i
		elements[e_new].compute_volume_tetra(XX, YY, ZZ, XYZ);
		
		//rad = sqrt(|XG|)
		elements[e_new].compute_rad_XG();

		//définition du mode de filtrage des valeurs propres sur chaque élément
		elements[e_new].compute_eps_max_For_Eigenvalue_Filtering(eps_0, choice);

		elements[e_new].compute_eps_of_Tikhonov_regulator(eps_reg_Tikhonov, choice);

		//racine cubique du vol du tetra
		elements[e_new].rc_Vol = pow(elements[e_new].Vol, 1.0 / 3.0);
		
		for(s = 0; s < 9; s++)
		  H[s] = 0.0;
		
		for(s = 0; s < 3; s++)
		  w[s] = 0.0;
		//J'ai défini les coeffs de façon homogène (ref == 2 pour tous les éléments)
		choisir_A_et_xi(e_new, elements[e_new].rad);
		
		Compute_EigenValue(elements[e_new].Ac.value,H, w);
		elements[e_new].Ac.eigen[0] = w[0];
		elements[e_new].Ac.eigen[1] = w[1];
		elements[e_new].Ac.eigen[2] = w[2];
		prodMatMatDiag_positive(H,w,elements[e_new].Ac.ADP);
		prodMatMatDiag_negative(H,w,elements[e_new].Ac.ADN);
		elements[e_new].Ac.compute_inverse();
		verifyA_OneoverSQRT_A(elements[e_new].Ac.ADN, H, w, Am) ;
		
		/*-------------------- definition des ondes planes -------------------------*/
		
		//directions des ondes planes dans un cube
		//elements[e_new].OP = new Onde_plane[ndof];
		MKL_INT s;
		for (s = 0; s < ndof; s++){
		  elements[e_new].OP[s].define_d(Dir[3*s], Dir[3*s+1], Dir[3*s+2]);
		  elements[e_new].OP[s].compute_Ad_and_dAd(elements[e_new].Ac, elements[e_new].xi);
		  
		  //calculer le vecteur K de Hélène, Sebastien, Julien et Ben
		  elements[e_new].OP[s].compute_K(elements[e_new].xi, omega);
		}

		//calcul des normales aux faces de l'élément e_new
		get_exteriorNormalToElem(elements[e_new], XX, YY, ZZ, XYZ);
		
		//calculer les impédances directionnelles
		for(s = 0; s < 4; s++){
		  elements[e_new].compute_An_and_nAn(s);
		}

		elements[e_new].compute_hmax(XX, YY, ZZ, XYZ, w, ZX);

		hmax = max(hmax, elements[e_new].hmax);
		hmin = min(hmin, elements[e_new].hmin);
		
		elements[e_new].Nred = 0;
		elements[e_new].Nres = 0;
		
		
	}
 
	delete [] H; H =  nullptr;
	delete [] w; w =  nullptr;
	delete [] XX; XX =  nullptr;
	delete [] YY; YY =  nullptr;
	delete [] ZZ; ZZ =  nullptr;
	delete [] XYZ; XYZ =  nullptr;
	delete [] ZX; ZX =  nullptr;
	delete [] Am; Am = nullptr;
}




void MailleSphere::build_coefficients_of_linear_global_system(const char * chaine){
  MKL_INT i, j;
  	for (i = 0; i < N_elements; i++) {
	  MKL_INT i1, i2;
	  for(j = 0; j < 4; j++){
	    if (elements[i].voisin[j] >=0){
	      
	      i1 = i;
	      i2 = elements[i].voisin[j];
	      
	      // cout<<"i1 = "<<i1<<" i2 = "<<i2<<endl;
	      
	      double TmL = (elements[i1].Y[j] - elements[i2].Y[j]);
	      double TpL = (elements[i1].Y[j] + elements[i2].Y[j]);
	    //double pp = 
	      
	      elements[i].TL_VV[j] = TmL / (4 * elements[i1].Y[j] * TpL);
	      
	      elements[i].LT_VV[j] = 1.0 / (2 * TpL);
	      
	      elements[i].TL_VP[j] = - TmL / (4 * TpL);
	      
	      elements[i].LT_VP[j] = - elements[i1].Y[j] / (2 * TpL);
	      
	      elements[i].TL_PV[j] = TmL / (4 * TpL);
	      
	      elements[i].LT_PV[j] = elements[i2].Y[j] / (2 * TpL);
	      
	      elements[i].TL_PP[j] = - (elements[i1].Y[j] * TmL) / (4 * TpL);
	      
	      elements[i].LT_PP[j] = - (elements[i1].Y[j] * elements[i2].Y[j]) / (2 * TpL);

	      //Coeffs de la formulation issue du systeme d'ordre 1
	      
	      elements[i].T_PP[j] = (elements[i1].Y[j] * elements[i2].Y[j]) / (2*TpL);
	      elements[i].T_PV[j] = - elements[i2].Y[j] / (2*TpL);
	      elements[i].T_VP[j] = - elements[i1].Y[j] / (2*TpL);
	      elements[i].T_VV[j] = 1.0 / (2*TpL);
	      

	      elements[i].L_PP[j] = (elements[i1].Y[j] * elements[i2].Y[j]) / (2*TpL);
	      elements[i].L_PV[j] = - elements[i2].Y[j] / (2*TpL);
	      elements[i].L_VP[j] = - elements[i1].Y[j] / (2*TpL);
	      elements[i].L_VV[j] = 1.0 / (2*TpL);

	      
	    }
	    else {
	      i1 = i;
	      i2 = i;
	      
	      double TpL = (elements[i1].Y[j] + Y_omega);
	      double TmL = (elements[i1].Y[j] - Y_omega);
	      
	      //coeff de réflexion sur une interface exterieure
	      elements[i].Q_T = (elements[i1].Y[j] - Y_omega) / TpL;
	      
	      elements[i].betaPP[j] = - (elements[i1].Y[j] * TmL) / (4 * TpL) ;
	      elements[i].betaPV[j] = TmL / (4 * TpL);
	      elements[i].betaVP[j] = - TmL / (4 * TpL);
	      elements[i].betaVV[j] = TmL / (4 * elements[i1].Y[j] * TpL);
	      
	      elements[i].T_PP[j] = (elements[i1].Y[j] * Y_omega) / (2*TpL);
	      elements[i].T_PV[j] = - Y_omega / (2*TpL);
	      elements[i].T_VP[j] = - elements[i1].Y[j] / (2*TpL);
	      elements[i].T_VV[j] = 1.0 / (2*TpL);


	      if (strcmp(chaine, "cess-des") == 0) {
	      //alpha_PincP[j] = (Y[j] * Y[j]) / 2
		elements[i].alpha_PincP[j] = (elements[i1].Y[j] * Y_omega) / (2*TpL);
	      
	      //alpha_PincV[j] = - Y[j] / 2
		elements[i].alpha_PincV[j] = - Y_omega / (2*TpL);
	      
	      //alpha_VincP[j] = Y[j] / 2
		elements[i].alpha_VincP[j] = - elements[i1].Y[j] / (2*TpL);
	      
	      //alpha_VincV[j] = Y[j] / 2
		elements[i].alpha_VincV[j] =  1.0 / (2*TpL);
	      
	      }
	      else{
	      //alpha_PincP[j] = Y_omega
	      elements[i].alpha_PincP[j] = Y_omega;
	      
	      //alpha_PincV[j] = - 1
	      elements[i].alpha_PincV[j] =  - 1.0 ;
	      
	      //alpha_VincP[j] = -1
	      elements[i].alpha_VincP[j] =  - 1.0 ;
	      
	      //alpha_VincV[j] = Y[j] / 2
	      elements[i].alpha_VincV[j] = (1.0 / Y_omega);
	      }
	    }
	  }
	  
	}
}


void MailleSphere::test_build_element()
{
  //information sur les éléments du maillage
	cout << "-----------------------------" << endl;
	for (MKL_INT i = 0; i < N_elements; i++) {
		elements[i].print_info();
		for(MKL_INT s = 0; s < ndof; s++) {
		  elements[i].OP[s].print_info();
		}
	}
	cout << "-----------------------------" << endl;
}


double compute_XG(Element &E1, Element &E2) {
  double r = 0;
  MKL_INT i;
  
  for(i = 0; i < 3; i++) {
    r += (E1.XG[i] - E2.XG[i]) * (E1.XG[i] - E2.XG[i]);
  }
  r = sqrt(r);
  return r;
}


void MailleSphere::print_new_mesh(double distance){
  MKL_INT i, j, l, f;
  double r , t = -1; 
  FILE* fic;
  
  if( !(fic = fopen("data/TEST_new_mesh.txt","w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  for(i = 0; i < N_elements; i++) {
    
    fprintf(fic,"(%lg, %lg, %lg)\t",elements[i].XG[0],elements[i].XG[1],elements[i].XG[2]);
    
    for(j = 0; j < 4; j++) {
      if (elements[i].voisin[j] >= 0){
	
	f = elements[i].voisin[j];
	
	r = compute_XG(elements[i], elements[f]);
       
	if (r > distance) {
	  cout<<" Bug au tetra "<<i<<" voisin "<<j<<endl;
	  cout<< newloc2glob[4 * f]<<" "<<newloc2glob[4 * f + 1]<<" "<<newloc2glob[4 * f + 2]<<" "<<newloc2glob[4 * f + 3]<<" r = "<<r<<endl;
	  exit(1);
	}
	
	fprintf(fic, "(%lg, %lg, %lg)\t %lg\t",elements[f].XG[0],elements[f].XG[1],elements[f].XG[2], r);
      }
      else {
	fprintf(fic,"%lg\t %lg",t, t);
      }
    }
    fprintf(fic,"\n");
  }
  fclose(fic);
}


void MailleSphere::destroy_Mesh(){
  MKL_INT i, j, l;
  delete [] points; points = nullptr;
  // delete [] voisin; voisin = nullptr;
  //delete [] tetra; tetra = nullptr;
  //delete [] Tri; Tri = nullptr;
  /*
  delete [] NewListeVoisin; NewListeVoisin  =  nullptr;
  delete [] newloc2glob; newloc2glob  =  nullptr;
  delete [] OldListeVoisin; OldListeVoisin  =  nullptr;
  delete [] oldloc2glob; oldloc2glob  =  nullptr;
  */
  for (i  =  0; i < N_elements; i++) {
    for(j = 0; j < 4; j++){
      /*delete [] elements[i].noeud[j];*/ elements[i].noeud[j] = nullptr;
      for(l = 0; l < 3; l++) {
	/*delete [] elements[i].face[j].noeud[l];*/ elements[i].face[j].noeud[l] = nullptr;
      }
      //delete [] elements[i].face; elements[i].face = nullptr;
    }
     delete [] elements[i].OP; elements[i].OP =  nullptr;
  }
  
  for(i = 0; i < N_Face_double; i++) {
    for(l = 0; l < 3; l++) {
      /*delete []  face_double[i].noeud[l];*/face_double[i].noeud[l] = nullptr; 
    }
  }
 
 
  for(i = 0; i < N_sommets; i++) {
    /*delete [] sommets[i];*/ sommets[i].set_X(nullptr);
  }
 
  
  for(i = 0; i < N_Face; i++) {
    for(l = 0; l < 3; l++) {
      /*delete []  face[i].noeud[l];*/face[i].noeud[l] = nullptr; 
    }
  }
  
  /*delete [] sommets;*/ sommets = nullptr;
  Xsommet = nullptr;
  /* delete [] face_double;*/ face_double = nullptr;
  /* delete [] face*/; face = nullptr;
  //delete [] DirSph; DirSph = nullptr;
  /* delete [] Dir*/; Dir = nullptr;
  i = 0;
  cout<<" elements["<<i<< "].xi : "<<elements[i].xi<<endl;
  
  /*delete [] elements;*/ elements =  nullptr;
  /*delete [] X1d;*/ X1d =  nullptr;
  /*delete [] W1d; */W1d =  nullptr;
  /*delete [] SIZE;*/ SIZE =  nullptr;
  /*
  delete [] old2new; old2new  =  nullptr;
  delete [] new2old; new2old  =  nullptr;*/
  //delete [] Z_XG; Z_XG =  nullptr;
  /*delete [] noeud_fixe_element;*/ noeud_fixe_element =  nullptr;
  for(l = 0; l < 4; l++) {
    sommets_fixe_element[l].set_X(nullptr);
    fixe_element.noeud[l] = nullptr;
  }
  /*delete [] sommets_fixe_element; */sommets_fixe_element =  nullptr;
  /*delete [] fixe_element.OP;  */fixe_element.OP =  nullptr;
} 


//destructeur de la classe MailleSphere
// MailleSphere::~MailleSphere(){
//   MKL_INT i, j, l;
//   delete [] points; points = nullptr;
//   delete [] voisin; voisin = nullptr;
//   delete [] tetra; tetra = nullptr;
//   delete [] Tri; Tri = nullptr;
//   /*
//   delete [] NewListeVoisin; NewListeVoisin  =  nullptr;
//   delete [] newloc2glob; newloc2glob  =  nullptr;
//   delete [] OldListeVoisin; OldListeVoisin  =  nullptr;
//   delete [] oldloc2glob; oldloc2glob  =  nullptr;
//   */
//   for (i  =  0; i < N_elements; i++) {
//     for(j = 0; j < 4; j++){
//       /*delete [] elements[i].noeud[j];*/ elements[i].noeud[j] = nullptr;
//       for(l = 0; l < 3; l++) {
// 	/*delete [] elements[i].face[j].noeud[l];*/ elements[i].face[j].noeud[l] = nullptr;
//       }
//       //delete [] elements[i].face; elements[i].face = nullptr;
//     }
//      delete [] elements[i].OP; elements[i].OP =  nullptr;
//   }
  
//   for(i = 0; i < N_Face_double; i++) {
//     for(l = 0; l < 3; l++) {
//       /*delete []  face_double[i].noeud[l];*/face_double[i].noeud[l] = nullptr; 
//     }
//   }
 
 
//   for(i = 0; i < N_sommets; i++) {
//     /*delete [] sommets[i];*/ sommets[i].set_X(nullptr);
//   }
 
  
//   for(i = 0; i < N_Face; i++) {
//     for(l = 0; l < 3; l++) {
//       /*delete []  face[i].noeud[l];*/face[i].noeud[l] = nullptr; 
//     }
//   }
  
//   delete [] sommets; sommets = nullptr;
//   Xsommet = nullptr;
//   delete [] face_double; face_double = nullptr;
//   delete [] face; face = nullptr;
//   //delete [] DirSph; DirSph = nullptr;
//   delete [] Dir; Dir = nullptr;
//   i = 0;
//   cout<<" elements["<<i<< "].xi : "<<elements[i].xi<<endl;
  
//   delete [] elements; elements =  nullptr;
//   delete [] X1d; X1d =  nullptr;
//   delete [] W1d; W1d =  nullptr;
//   delete [] SIZE; SIZE =  nullptr;
//   /*
//   delete [] old2new; old2new  =  nullptr;
//   delete [] new2old; new2old  =  nullptr;*/
//   delete [] Z_XG; Z_XG =  nullptr;
//   delete [] noeud_fixe_element; noeud_fixe_element =  nullptr;
//   for(l = 0; l < 4; l++) {
//     sommets_fixe_element[l].set_X(nullptr);
//     fixe_element.noeud[l] = nullptr;
//   }
//   delete [] sommets_fixe_element; sommets_fixe_element =  nullptr;
//   delete [] fixe_element.OP;  fixe_element.OP =  nullptr;
// } 



void tri_selection(MKL_INT debut, MKL_INT fin, MailleSphere &MSph) {
  
  MKL_INT i,j;
  MKL_INT c;
  
  for(i = debut ; i < fin-1 ; i++){
    for(j = i+1 ; j < fin ; j++){
      if ( MSph.elements[MSph.old2new[i]].XG[2] > MSph.elements[MSph.old2new[j]].XG[2] ) {
 	c = MSph.old2new[i];
 	MSph.old2new[i] = MSph.old2new[j];
 	MSph.old2new[j] = c;
      }   
    }    
  }

}


void tri_recursive(MKL_INT debut, MKL_INT fin, MailleSphere &MSph) {
  //tri recursive par Etienne Poulin – ISN – Novembre 2013  
  const MKL_INT pivot = MSph.old2new[debut];
  MKL_INT pos = debut;
  MKL_INT i;
  if (debut < fin)
    {
      
      /* cette boucle permet de placer le pivot (début du tableau à trier)
	 au bon endroit dans le tableau
	 avec toutes les valeurs plus petites avant
	 et les valeurs plus grandes après
	 à la fin, la valeur pivot se trouve dans le tableau à tab[pos]
      */
      for (i = debut; i < fin; i++)
	{
	  if (MSph.elements[MSph.old2new[i]].XG[2] < MSph.elements[pivot].XG[2]) {
	    
	    MSph.old2new[pos] = MSph.old2new[i];
	    pos++;
	    MSph.old2new[i] = MSph.old2new[pos];
	    MSph.old2new[pos] = pivot;
	  }
	}
      
      /* Il ne reste plus qu'à rappeler la procédure de tri
	 sur le début du tableau jusquà pos (exclu) : tab[pos-1]
      */
      tri_recursive(debut, pos, MSph);
  
      /* et de rappeler la procédure de tri
	 sur la fin du tableau à partir de la première valeur après le pivot
	 tab[pos+1]
      */
      tri_recursive(pos+1, fin, MSph);
    }
}


void tri_recursive(MKL_INT debut, MKL_INT fin, MKL_INT *old2new, double *Z_XG) {
  //tri recursive par Etienne Poulin – ISN – Novembre 2013  
  const MKL_INT pivot = old2new[debut];
  MKL_INT pos = debut;
  MKL_INT i;
  if (debut < fin)
    {
      
      /* cette boucle permet de placer le pivot (début du tableau à trier)
	 au bon endroit dans le tableau
	 avec toutes les valeurs plus petites avant
	 et les valeurs plus grandes après
	 à la fin, la valeur pivot se trouve dans le tableau à tab[pos]
      */
      for (i = debut; i < fin; i++)
	{
	  if (Z_XG[old2new[i]] < Z_XG[pivot]) {
	    
	    old2new[pos] = old2new[i];
	    pos++;
	    old2new[i] = old2new[pos];
	    old2new[pos] = pivot;
	  }
	}
      
      /* Il ne reste plus qu'à rappeler la procédure de tri
	 sur le début du tableau jusquà pos (exclu) : tab[pos-1]
      */
      tri_recursive(debut, pos, old2new, Z_XG);
  
      /* et de rappeler la procédure de tri
	 sur la fin du tableau à partir de la première valeur après le pivot
	 tab[pos+1]
      */
      tri_recursive(pos+1, fin, old2new, Z_XG);
    }
}



void MailleSphere::tri_maillage() {
  //Cette fonction initialise et tri le tableau old2new des indices des éléments en fonction des barycentres des éléments suivant l'axe oz
  MKL_INT i;
  for (i = 0; i < get_N_elements(); i++) {
    old2new[i] = i;
  }

  //tri du tableau old2new
  tri_recursive(0, get_N_elements(), *this);
  //tri_selection(0, get_N_elements(), *this);
}


void tri_elements(MKL_INT N_elements, MKL_INT *old2new, double *Z_XG) {
  //Cette fonction initialise et tri le tableau old2new des indices des éléments en fonction des barycentres des éléments suivant l'axe oz
  MKL_INT i;
  for (i = 0; i < N_elements; i++) {
    old2new[i] = i;
  }

  //tri du tableau old2new
  tri_recursive(0, N_elements, old2new, Z_XG);
 
}


void MailleSphere::choisir_A_et_xi_fixe_element() {
  //Cette fonction permet de définir les coeffs A et xi de l'equation Helmholtz sur un élément
 
  if (fixe_element.rad < r1) {
    fixe_element.xi = xi1;
    fixe_element.set_ref(2);
    fixe_element.Ac.define(a1,b1,b1,
		   b1,a1,b1,
		   b1,b1,a1);
    
  }
  else {
    fixe_element.xi = xi2;
    fixe_element.set_ref(2);
    fixe_element.Ac.define(a2,b2,b2,
		   b2,a2,b2,
		   b2,b2,a2);
  }
}
  


void MailleSphere::choisir_A_et_xi(MKL_INT e, double r) {
  //Cette fonction permet de définir les coeffs A et xi de l'equation Helmholtz sur un élément
  /*----------------------------------------------------------------------------------------
    e          : numero d'un element
    
    r          : |XG| avec XG le centre de gravité de l'élément    
    ---------------------------------------------------------------------------------------*/
  if (r < r1) {
    elements[e].xi = xi1;
    elements[e].set_ref(2);
    elements[e].Ac.define(a1,b1,b1,
		   b1,a1,b1,
		   b1,b1,a1);
    
  }
  else {
    elements[e].xi = xi2;
    elements[e].set_ref(2);
    elements[e].Ac.define(a2,b2,b2,
		   b2,a2,b2,
		   b2,b2,a2);
  }
}
  

void Element::compute_rad_XG() {
  rad = sqrt(XG[0] * XG[0] + XG[1] * XG[1] + XG[2] * XG[2]);
}


double compute_rad_X(double *X, double *X0) {
  double *XX0 = new double[3];
  for(MKL_INT i = 0; i < 3; i++){
    XX0[i] = X[i] - X0[i];
  }
  double r = sqrt(XX0[0] * XX0[0] + XX0[1] * XX0[1] + XX0[2] * XX0[2]);

  delete [] XX0; XX0  =  nullptr;
  
  return r;
}



void MailleSphere::compute_coeff_hetero_Analytique_Bessel(){
  //Cette fonction calcule les coefficients de la solution analytique de Bessel
  cplx M[9];
  cplx F[3];
  
  M[0] = cplx(k2 * sin(k1 * r1), 0.0);
  M[3] = cplx(sqrt(a1 * xi1) * k2 * k2 * (k1 * r1 * cos(k1 * r1) - sin(k1 * r1)), 0.0);
  M[6] = cplx(0.0, 0.0);
  M[1] = - cplx(k1 * sin(k2 * r1), 0.0);
  M[4] = - cplx(sqrt(a2 * xi2) * k1 * k1 * (k2 * r1 * cos(k2 * r1) - sin(k2 * r1)), 0.0);
  M[7] = cplx(sqrt(a2 * xi2) * (k2 * r2 * cos(k2 * r2) - sin(k2 * r2)), k2 * r2 * Y_omega * sin(k2 * r2));
  M[2] = cplx(k1 * cos(k2 * r1), 0.0);
  M[5] = - cplx(sqrt(a2 * xi2) * k1 * k1 * (k2 * r1 * sin(k2 * r1) + cos(k2 * r1)), 0.0);
  M[8] = cplx(sqrt(a2 * xi2) * (k2 * r2 * sin(k2 * r2) + cos(k2 * r2)), - k2 * r2 * Y_omega * cos(k2 * r2));

  F[0] = cplx(0.0, 0.0);
  F[1] = cplx(0.0, 0.0);
  F[2] = cplx(0.0, k2 * k2 * r2 * r2);

  MKL_INT info, nrhs;
  MKL_INT *ipiv;
  nrhs = 1;
  ipiv = new MKL_INT[3];

  MKL_INT taille = 3;
  
  LAPACKE_zgetrf(LAPACK_COL_MAJOR,  taille, taille, M, taille, ipiv);
  
  info = LAPACKE_zgetrs(LAPACK_COL_MAJOR, 'T', taille, nrhs, M, taille, ipiv, F, taille) ;
  
  cout<<"info = "<<info<<endl;

  c1 = F[0];

  c2 = F[1];

  c3 = F[2];

  cout<<" c1 = "<<c1<<" c2 = "<<c2<<" c3 = "<<c3<<endl;
  
  delete [] ipiv; ipiv = 0;
}


void Besselj0(MKL_INT n, double z, cplx *j0, cplx *j0P) {
  if (n == 0) {
    
    double t = 1.0 / z;

    //j0 = sin(z) / z
    *j0 = sin(z) * t;

    //j0P = j0' = (z * cos(z) - sin(z)) / z*z
    *j0P = (z * cos(z) - sin(z)) * (t * t);
  }

  else {
    cout<<"pas encore défini"<<endl;
    
  }
}


void Besselj0(MKL_INT n, double z, double *j0){
  if (n == 0) {
    *j0 = sin(z) / z;
  }
  else {
    cout<<"pas encore défini"<<endl;
 
  }
}


void Bessely0(MKL_INT n, double z, cplx *y0, cplx *y0P) {
  if (n == 0) {
    
    double t = 1.0 / z;

    //y0 = - cos(z) / z
    *y0 = - cos(z) * t;

    //y0P = j0' = (z * sin(z) + cos(z)) / z*z
    *y0P = (z * sin(z) + cos(z)) * (t * t) ;
  }

  else {
    cout<<"pas encore défini"<<endl;
    
  }
}


void Hankel3D(double k, double z, cplx *y, cplx *derr){
  *y = k * (exp(cplx(0.0, z)) / (4 * M_PI * z));
  *derr = k * k * ((cplx(0.0, z) - cplx(1.0, 0.0)) / (4 * M_PI * z * z)) * exp(cplx(0.0, z));
}



void Hankel3D(double k, double z, cplx *y){
  *y = k * (exp(cplx(0.0, z)) / (4 * M_PI * z));
}

void Bessely0(MKL_INT n, double z, double *y0) {
  if (n == 0) {
    *y0 = - cos(z) / z;
  }

  else {
    cout<<"pas encore défini"<<endl;
    
  }
}

// void SolexacteBessel(MKL_INT e, double *X, cplx *P, cplx *V, MailleSphere &MSph) {

//   //cette fonction calcule la solution exacte (P, V) associée à l'élément e en X
//   /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
//     e       : l'indice de l'élément
//     X       : variable ou veut évaluer la solution
//     P       : la pression
//     V       : la vitesse
//     MSph    : le maillage
//     /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
//     La pression P  et la vitesse   V
//     /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
//     j0      : fonction de Bessel de première espèce j0
//     j0P     := (j0')
//     y0      : fonction de Bessel de première espèce y0
//     y0P     := (y0)'
//     d       : la direction; d = (X - X0) / ||X - X0|| avec X0 la source
//     XX0     : X - X0
//     gradP   : le gradient de P
//     r       : ||X - X0||
//     /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
//   cplx j0, j0P, y0, y0P;
  
//   double *d = new double[3];

//   double *XX0 = new double[3];

//   cplx *gradP = new cplx[3];

//    MKL_INT i;
  
//   for (i = 0; i < 3; i++){
//     XX0[i] = X[i] - MSph.X0[i];
//   }
  
//   //r = ||X - X0||_2
//   double r = cblas_ddot(3, XX0, 1, XX0, 1) ;
//   r = sqrt(r);

//   double rr = 1.0 / r;

//   //d = (X - X0) / ||X - X0||
//   for (i = 0; i < 3; i++){
//     d[i] = XX0[i] * rr;
//   }

//   if (MSph.elements[e].get_ref() == 1) {

//     rr = MSph.k1 * r;

//     Besselj0(0, rr, &j0, &j0P);
    
//     *P =  MSph.c1 * j0;

//     for (i = 0; i < 3; i++) {
      
//       gradP[i] = (MSph.c1 * MSph.k1 * j0P ) * d[i];
      
//       V[i] =  (MSph.a1 * gradP[i]) / cplx(0.0,MSph.omega);
//     }

//   }

//   else if (MSph.elements[e].get_ref() == 2) {

//     rr = MSph.k2 * r;
    
//     Besselj0(0, rr, &j0, &j0P);

//     Bessely0(0, rr, &y0, &y0P);
    
//     *P =  MSph.c2 * j0 + MSph.c3 * y0;

//     for (i = 0; i < 3; i++) {
      
//       gradP[i] = (MSph.c2 * MSph.k2 * j0P + MSph.c3 * MSph.k2 * y0P) * d[i];
      
//       V[i] =  (MSph.a2 * gradP[i]) / cplx(0.0,MSph.omega);
//     }
//   }

//   else {
//     cout<<"problème de situation des éléments dans les spheres"<<endl;
//   }

//   delete [] d; d  =  nullptr;
//   delete [] XX0; XX0  =  nullptr;
//   delete [] gradP; gradP  =  nullptr;
// }
void SolexacteBessel(MKL_INT e, double *X, cplx *P, cplx *V, MailleSphere &MSph) {

  //cette fonction calcule la solution exacte (P, V) associée à l'élément e en X
  /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
    e       : l'indice de l'élément
    X       : variable ou veut évaluer la solution
    P       : la pression
    V       : la vitesse
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
    La pression P  et la vitesse   V
    /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
    j0      : fonction de Bessel de première espèce j0
    j0P     := (j0')
    y0      : fonction de Bessel de première espèce y0
    y0P     := (y0)'
    d       : la direction; d = (X - X0) / ||X - X0|| avec X0 la source
    XX0     : X - X0
    gradP   : le gradient de P
    r       : ||X - X0||
    /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  if (strcmp(MSph.forme_Sol, "onde_plane") == 0){

    double *XX0 = new double[3];
    
    for (int i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
    }

    
    //MSph.OP_inc.compute_P_and_V(XX0);
    
    *P = MSph.OP_inc.pression(XX0);
    
    for(int i = 0; i < 3; i++){
      V[i] = MSph.OP_inc.Ad[i] * MSph.OP_inc.ksurOmega * (*P);
    }

    delete [] XX0; XX0 = nullptr;

  }
  else{
    cplx h0, h0P;
    
    double *d = new double[3];
    
    double *XX0 = new double[3];
    
    cplx *gradP = new cplx[3];

    double alpha = 1.0;
    double beta = 0.0;
   
    
    int i;
    
    for (i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
      gradP[i] = cplx(0.0, 0.0);
       d[i] = 0.0;
    }

    //d = A^{1/2} * (X - X0) avec A^{1/2} = Ac.ADP
    cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, alpha, MSph.elements[0].Ac.ADP, 3, XX0, 1, beta, d, 1);
    /*
    //d = d * sqrt(xi) = sqrt(xi) * A^{1/2} * (X-X0)
    for (i = 0; i < 3; i++){
      d[i] = d[i] * sqrt(MSph.elements[0].xi);
    }
    */
    //r = ||sqrt(xi) * A^{1/2} (X - X0)||_2
    double r = cblas_ddot(3, d, 1, d, 1) ;
    r = sqrt(r);
    
    double rr ;
    
    
    rr = sqrt(MSph.elements[0].xi) * MSph.omega * r;
    
    Hankel3D(MSph.omega * sqrt(MSph.elements[0].xi), rr, &h0, &h0P);
    
    *P =  h0;

    rr = 1.0 / r;


    //d = d / r 
    for (i = 0; i < 3; i++){
      d[i] = d[i] * rr;
    }

    
    //gradP = (d * h0P) / (i * omega) 
    for (i = 0; i < 3; i++) {
      
      gradP[i] =  (d[i] * h0P) / cplx(0.0,MSph.omega);
      V[i] = cplx(0.0,0.0);
    }

    //V = A^{1/2} gradP 
    prodMatreal_Vectcomplex_ColMajor(MSph.elements[0].Ac.ADN, 3, 3, gradP, 3, V);
    
    
    delete [] d; d  =  nullptr;
    delete [] XX0; XX0  =  nullptr;
    delete [] gradP; gradP = nullptr;
  }
}

void SolexacteBessel(MKL_INT e, double *X, cplx *P, cplx *V, MailleSphere &MSph, double *d, double *XX0, cplx *gradP) {

  //cette fonction calcule la solution exacte (P, V) associée à l'élément e en X
  /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
    e       : l'indice de l'élément
    X       : variable ou veut évaluer la solution
    P       : la pression
    V       : la vitesse
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
    La pression P  et la vitesse   V
    /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
    j0      : fonction de Bessel de première espèce j0
    j0P     := (j0')
    y0      : fonction de Bessel de première espèce y0
    y0P     := (y0)'
    d       : la direction; d = (X - X0) / ||X - X0|| avec X0 la source
    XX0     : X - X0
    gradP   : le gradient de P
    r       : ||X - X0||
    /*----------------------------------------------------------------------------------------------------------------------------------------------------*/

  if (strcmp(MSph.forme_Sol, "onde_plane") == 0){
    double *XX0 = new double[3];
    
    for (int i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
    }

    // MSph.OP_inc.compute_P_and_V(XX0);
    
    *P = MSph.OP_inc.pression(XX0);
    
    for(int i = 0; i < 3; i++){
      V[i] = MSph.OP_inc.Ad[i] * MSph.OP_inc.ksurOmega * (*P);
    }


    delete [] XX0; XX0 = nullptr;
  }
  else{
    cplx h0, h0P;
    
    double alpha = 1.0;
    double beta = 0.0;
    int i;
    
    for (i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
      d[i] = 0.0;
    }

    //d = A^{1/2} * (X-X0) avec A^{1/2} = mat.ADP
    cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, alpha, MSph.elements[0].Ac.ADP, 3, XX0, 1, beta, d, 1);
    
    /*
    //d = d * sqrt(xi) = sqrt(xi) * A^{1/2} * (X-X0)
    for (i = 0; i < 3; i++){
      d[i] = d[i] * sqrt(MSph.elements[0].xi);
    }
    */
    //r = ||sqrt(xi) * A^{1/2} (X - X0)||_2
    double r = cblas_ddot(3, d, 1, d, 1) ;
    r = sqrt(r);
    
    double rr ;
        
    rr = sqrt(MSph.elements[0].xi) * MSph.omega * r;
    
    Hankel3D(MSph.omega * sqrt(MSph.elements[0].xi), rr, &h0, &h0P);
    
    *P =  h0 ;

    rr = 1.0 / r;
    
    //d = d / r 
    for (i = 0; i < 3; i++){
      d[i] = d[i] * rr;
    }
    
    //gradP = (d * h0P) / (i * omega) 
    for (i = 0; i < 3; i++) {
      
      gradP[i] =  (d[i] * h0P) / cplx(0.0,MSph.omega);
      V[i] = cplx(0.0,0.0);
    }
    
    //V = A^{1/2} gradP 
    prodMatreal_Vectcomplex_ColMajor(MSph.elements[0].Ac.ADN, 3, 3, gradP, 3, V);
    
  }
}
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

// void SolexacteBessel(MKL_INT e, double *X, cplx *P, MailleSphere &MSph) {

//   //cette fonction calcule la solution exacte P associée à l'élément e en X
//   /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
//     e       : l'indice de l'élément
//     X       : variable ou veut évaluer la solution
//     P       : la pression
//     MSph    : le maillage
//     /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
//                                                   La pression P 
//     /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
//     j0      : fonction de Bessel de première espèce j0
//     y0      : fonction de Bessel de première espèce y0
//     XX0     : X - X0
//     r       : ||X - X0||
//     /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
//   double j0, y0;
  
//   double *XX0 = new double[3];

//    MKL_INT i;
  
//   for (i = 0; i < 3; i++){
//     XX0[i] = X[i] - MSph.X0[i];
//   }
  
//   //r = ||X - X0||_2
//   double r = cblas_ddot(3, XX0, 1, XX0, 1) ;
//   r = sqrt(r);

//   double rr ;

//   if (MSph.elements[e].get_ref() == 1) {

//     rr = MSph.k1 * r;

//     Besselj0(0, rr, &j0);
    
//     *P =  MSph.c1 * j0;
//   }

//   else if (MSph.elements[e].get_ref() == 2) {

//     rr = MSph.k2 * r;
    
//     Besselj0(0, rr, &j0);

//     Bessely0(0, rr, &y0);
    
//     *P =  MSph.c2 * j0 + MSph.c3 * y0;
//   }

//   else {
//     cout<<"problème de situation des éléments dans les spheres"<<endl;
//   }

//   delete [] XX0; XX0  =  nullptr;
// }

void SolexacteBessel(MKL_INT e, double *X, cplx *P, MailleSphere &MSph) {

  //cette fonction calcule la solution exacte P associée à l'élément e en X
  /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
    e       : l'indice de l'élément
    X       : variable ou veut évaluer la solution
    P       : la pression
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
                                                  La pression P 
    /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
    j0      : fonction de Bessel de première espèce j0
    y0      : fonction de Bessel de première espèce y0
    XX0     : X - X0
    r       : ||X - X0||
    /*----------------------------------------------------------------------------------------------------------------------------------------------------*/

  if (strcmp(MSph.forme_Sol, "onde_plane") == 0){
   
     double *XX0 = new double[3];
    
    for (int i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
    }

    *P = MSph.OP_inc.pression(XX0);
    
    delete [] XX0; XX0 = nullptr;
  }
  else{
    
    cplx h0;
    
    double *XX0 = new double[3];

    double *d = new double[3];
    
    double alpha = 1.0;
    double beta = 0.0;
    
    
    int i;
    
    for (i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
      d[i] = 0.0;
    }
    
    //d = A^{1/2} * (X - X0) avec A^{1/2} = mat.ADP
    cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, alpha, MSph.elements[0].Ac.ADP, 3, XX0, 1, beta, d, 1);
    /*
    //d = d * sqrt(xi) = sqrt(xi) * A^{1/2} * (X-X0)
    for (i = 0; i < 3; i++){
      d[i] = d[i] * sqrt(MSph.elements[0].xi);
    }
    */
    //r = ||xi * A^{1/2} (X - X0)||_2
    double r = cblas_ddot(3, d, 1, d, 1) ;
    r = sqrt(r);
    
    double rr ;
    
    rr =  sqrt(MSph.elements[0].xi) * MSph.omega * r;
    
    Hankel3D(sqrt(MSph.elements[0].xi) * MSph.omega, rr, &h0);
    
    *P =  h0;
    
    delete [] XX0; XX0  =  nullptr;
    delete [] d; d  =  nullptr;
  }
}

void SolexacteBessel(MKL_INT e, double *X, cplx *P, MailleSphere &MSph, double *XX0) {

  //cette fonction calcule la solution exacte P associée à l'élément e en X
  /*---------------------------------------------------------------------------------------- input -------------------------------------------------------
    e       : l'indice de l'élément
    X       : variable ou veut évaluer la solution
    P       : la pression
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- ouput -------------------------------------------------------
                                                  La pression P 
    /*-------------------------------------------------------------------------------------- intermédiare -------------------------------------------------
    j0      : fonction de Bessel de première espèce j0
    y0      : fonction de Bessel de première espèce y0
    XX0     : X - X0
    r       : ||X - X0||
    /*----------------------------------------------------------------------------------------------------------------------------------------------------*/

  if (strcmp(MSph.forme_Sol, "onde_plane") == 0){
    double *XX0 = new double[3];
    
    for (int i = 0; i < 3; i++){
      XX0[i] = X[i] - MSph.X0[i];
    }

    *P = MSph.OP_inc.pression(XX0);
    
    delete [] XX0; XX0 = nullptr;
    
  }
  else{
    
    cplx h0;
    
    int i;

    double *d = new double[3];
    
    double alpha = 1.0;
    double beta = 0.0;
   
    
    for (i = 0; i < 3; i++){
      d[i] = 0.0;
      XX0[i] = X[i] - MSph.X0[i];
    }

    //d = A^{1/2} * (X - X0) avec A^{1/2} = mat.ADP
    cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, alpha, MSph.elements[0].Ac.ADP, 3, XX0, 1, beta, d, 1);
    /*
    //d = d * sqrt(xi) = sqrt(xi) * A^{1/2} * (X-X0)
    for (i = 0; i < 3; i++){
      d[i] = d[i] * sqrt(MSph.elements[0].xi);
    }
    */
    //r = ||xi * A^{1/2} (X - X0)||_2

    double r = cblas_ddot(3, d, 1, d, 1) ;
    r = sqrt(r);
    
    double rr ;
    
    rr =  sqrt(MSph.elements[0].xi) * MSph.omega * r;
    
    Hankel3D(sqrt(MSph.elements[0].xi) * MSph.omega, rr, &h0);
    
    *P = h0 ;

    delete [] d; d  =  nullptr;
  }
}


void Bessel::compute_P_and_V(double *X, double *X0, cplx alpha1, cplx alpha2, cplx beta2, double r1, double a1, double a2, double k1, double k2, double xi1, double xi2, double rad) {

  //Cette fonction calcule en X, la Pression et la vitesse de la fonction de Bessel de source X0
  /*--------------------------------------------------------------------------- input ---------------------------------------------------------
    X                    : la variable ou est évaluée la fonction
    X0                   : le point source
    alph1, alpha2, beta2 : les coefficients de la fonction de Bessel
    r1                   : le rayon de la petite sphère
    a1                   : le coefficient diagonale de la matrice associée à l'équation de Helmholtz sur la petite sphère
    a2                   : le coefficient diagonale de la matrice associée à l'équation de Helmholtz sur la grande sphère
    k1                  := omega * sqrt(xi1 / e_r\cdot A e_r)
    xi1                  : le xi de l'équation de Helmholtz sur la petite sphère
    k2                  := omega * sqrt(xi2 / e_r\cdot A e_r)
    xi2                  : le xi de l'équation de Helmholtz sur la grande sphère
    omega                : la fréquence
    e_r = (X - X0) / ||X - X0||
    rad = ||XG||, avec XG le barycentre de l'element ou est défini la fonction de Bessel
   /*------------------------------------------------------------------------- ouput ----------------------------------------------------------
                         La pression Pc et la vitesse V
    /*------------------------------------------------------------------------- intermédiare --------------------------------------------------
    d   = e_r
    r   = ||X - X0||
    rr = 1 / r
    /*-----------------------------------------------------------------------------------------------------------------------------------------*/
  
  d = new double[3];

  V = new cplx[3];

  double *XX0 = new double[3];

   MKL_INT i;
  
  for (i = 0; i < 3; i++){
    XX0[i] = X[i] - X0[i];
  }
  
  //r = ||X - X0||_2
  r = cblas_ddot(3, XX0, 1, XX0, 1) ;
  r = sqrt(r);

  double rr = 1.0 / r;

  //d = (X - X0) / ||X - X0||
  for (i = 0; i < 3; i++){
    d[i] = XX0[i] * rr;
  }

  /*double rrr =  cblas_ddot(3, d, 1, d, 1) ;
  
  cout<<" rrr = "<<rrr<<endl;
  
  for (i = 0; i < 3; i++){
    cout<<"d["<<i<<"] = "<<d[i]<<endl;
    }*/
  
  
  if (r <=  r1) {
     Pc =  alpha1 * (sin( k1 * r) / ( k1 * r));
    
    for (i = 0; i < 3; i++){
      V[i] =  alpha1 * sqrt( a1 *  xi1) * (cplx( k1 * r * cos( k1 * r) - sin( k1 * r), 0.0) / cplx(0.0,  k1 *  k1 * r * r)) * d[i]; 
    }
  }
  
  else {
    Pc = ( alpha2 * sin( k2 * r) -  beta2 * cos( k2 * r)) / ( k2 * r);

    for (i = 0; i < 3; i++){
      V[i] = sqrt( a2 *  xi2) * ( ( alpha2 * ( k2 * r * cos( k2 * r) - sin( k2 * r)) +  beta2 * ( k2 * r * sin( k2 * r) + cos( k2 * r)) ) / cplx(0.0,  k2 *  k2 * r * r) ) * d[i];
    }
  }


  delete [] XX0; XX0  =  nullptr;
  
}


void MailleSphere::quad_Gauss_1D() {
  //Cette fonction calcule les points de Gauss-Legendre 1D
  //Dans un premier temps, la quadrature est sur [-1 1]
  //C'est à la fin qu'elle est estimée sur [0 1] par un changement de variable
  /*---------------------------------------------------------------------------------------------------------
    A et D des matrices qui contiennent les polynômes de Gauss-Legendre
    X1d : les points de Gauss-Legendre
    W1d : les poids asscoiés
    /*---------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *D = nullptr, *A = nullptr;
  D = new double[n_gL * n_gL];
  A = new double[n_gL * n_gL];
  
  for (i = 0; i < n_gL; i++){
    if (i % 2 == 0) {
      W1d[i] = 2 / (double) (i+1);
    }
    else{
      W1d[i] = 0.0;
    }
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = 0.0;
      D[i * n_gL + j] = 0.0;
    }
  }

  for (i = 0; i < n_gL-1; i++){
    A[i*n_gL + (i+1)] = i+1;
    A[(i+1)*n_gL + i] = i+1;
    D[i * n_gL + i] = 2 * (i+1) - 1;
  }
  D[(n_gL-1) * n_gL + (n_gL-1)] = 2 * n_gL - 1;

  double *alphar = nullptr, *alphai = nullptr, *beta = nullptr;
  alphar = new double[ n_gL];
  alphai = new double[ n_gL];
  beta = new double[ n_gL];

  double *work = nullptr;
  work = new double[ n_gL];
  MKL_INT info, ldr, ldl;
  ldl = n_gL;
  ldr = n_gL;

  double *vsl = nullptr, *vsr = nullptr;
  
  vsl = new double[ldl * n_gL];
  vsr = new double[ldr * n_gL];

  //Calcul des valeurs propres de (A,D)
  info = LAPACKE_dggev(LAPACK_COL_MAJOR, 'V', 'V', n_gL, A, n_gL, D, n_gL, alphar, alphai, beta, vsl, n_gL, vsr, n_gL);

  cout<<"info = "<<info<<endl;
 
  //calcul des points de Gauss
  cout<<"eigen(A,D) : "<<endl;
  for (i = 0; i < n_gL; i++){
    X1d[i] = alphar[i] / beta[i];
  }


  for (i = 0; i < n_gL; i++){
    for (j = 0; j < n_gL; j++){
      A[i * n_gL + j] = pow(X1d[j],i);
    }
  }


  //calcul des poids de Gauss
  MKL_INT *ipiv = nullptr;
  ipiv = new MKL_INT[n_gL];
   info = LAPACKE_dgetrf( LAPACK_COL_MAJOR, n_gL, n_gL, A, n_gL, ipiv);

   MKL_INT nrhs = 1;
   if (info != 0) {
     cout << "une erreur est renvoyée : "<< info << endl << endl;
   }
   else{
     cout << "résolution du système..."<< endl << endl;
     
     info = LAPACKE_dgetrs( LAPACK_COL_MAJOR, 'T', n_gL, nrhs, A, n_gL, ipiv, W1d, n_gL);
     cout<<"info = "<<info<<endl;
   }

   cout<<"affichage des poids : "<<endl;

  
   //adaption à l'intervalle [0 1]
   for (i = 0; i < n_gL; i++){
     X1d[i] = (X1d[i] + 1) * 0.5;
     W1d[i] *= 0.5;
     
   }

   cout<<" liberation de la mémoire des points :"<<endl;
   
   delete [] work; work  =  nullptr;
   delete [] beta; beta  =  nullptr;
   delete [] alphar; alphar  =  nullptr;
   delete [] alphai; alphai  =  nullptr;
   delete [] ipiv; ipiv  =  nullptr;
   delete [] vsr; vsr  =  nullptr;
   delete [] vsl; vsl  =  nullptr;
   
   delete [] A; A  =  nullptr;
   delete [] D; D  =  nullptr;
   
}



cplx quad_tetra_Bessel(MKL_INT e, MKL_INT s, MailleSphere &MSph) {
  //Cette fonction calcule l'intégrale sur un tetra du produit de la fonction de Bessel P avec conj(w_iloc), une fonction de base de l'élément correspondant
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : le numero de l'element
    s      : le numero local de d'une onde plane associée à l'element
    MSph     : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  
  cplx P;

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //Evaluation de j0_e en Xquad
   SolexacteBessel(e, Xquad, &P, MSph);

   I += MSph.Qd.Wtetra[i] * P * conj(MSph.elements[e].OP[s].pression(Xquad));
  }
  
  I = I * MSph.elements[e].Jac;
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  
  return I;
}
/*----------------------------------------------------------------------------------------*/

cplx quad_tetra_Bessel_VV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : indice du tetra
    iloc   : numero d'une onde plane
    MSph   : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  cplx P;
  cplx *V = new cplx[3];
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calul de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);

    //calcul de P et V pour l'onde OP[iloc]
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);

    J = dot_c(MSph.elements[e].OP[iloc].V, V);
    
    I = I + MSph.Qd.Wtetra[i] * J;
  }

  I = I * MSph.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  
  return I;
}




void BuildMat_projection(MailleSphere &MSph, MKL_INT e, cplx *MP) {
  //Cette fonction calcule la matrice de la projection sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    MSph   : le maillage
    e      : le numero de l'element 
    MP     : tableau ou stocker la matrice
    /*---------------------------------------------------------------- ouput --------------------------------------------
    MP     : la matrice de la projection
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  for (i = 0; i < MSph.ndof; i++) {
    for (j = 0; j < MSph.ndof; j++) {
      MP[i * MSph.ndof + j] = quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[i], MSph.elements[e].OP[j]);
    }
  }
}



void BuildMat_projection_PP_VV(MKL_INT e, cplx *MP, MailleSphere &MSph) {
  //Cette fonction calcule la matrice de la projection de PP + VV sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    e      : le numero de l'element 
    MP     : tableau ou stocker la matrice
    MSph   : le maillage
    /*---------------------------------------------------------------- ouput --------------------------------------------
    MP     : la matrice de la projection
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  for (i = 0; i < MSph.ndof; i++) {
    for (j = 0; j < MSph.ndof; j++) {
      MP[i * MSph.ndof + j] = quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[i], MSph.elements[e].OP[j]) + quad_tetra_VV(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[i], MSph.elements[e].OP[j]);
    }
  }
}



void BuildVector_projection(MKL_INT e, cplx *F, const string & arg, MailleSphere &MSph) {
  //Cette fonction calcule le vecteur second membre de la projection sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     MSph   : le maillage
     e      : le numero de l'element
     F      : tableau ou stocker le vecteur
     arg    : chaine de caractère pour déterminer le choix du vecteur second membre;
              si arg = "plane", on traitera le cas d'une onde plane incidente
	      sinon, on aura affaire à une fonction de bessel j0 
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
  if (arg == "plane") { 
    for (i = 0; i < MSph.ndof; i++) {
      F[i] =  quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[i], MSph.elements[e].OP[MSph.n_i]);
    }
  }
  else if (arg == "bessel") {
   
    for (i = 0; i < MSph.ndof; i++) {
      F[i] =  quad_tetra_Bessel(e, i, MSph);
    }

  }
  
}



void BuildVector_projection_Hetero_PP_VV(MKL_INT e, cplx *F, MailleSphere &MSph) {
  //Cette fonction calcule le vecteur second membre de la projection de PP + VV sur un élément
   /*----------------------------------------------------------------- input --------------------------------------------
     e      : le numero de l'element
     F      : tableau ou stocker le vecteur
     MSph   : le maillage
     /*---------------------------------------------------------------- ouput --------------------------------------------
     F     : le vecteur second membre 
     /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
 
  for (i = 0; i < MSph.ndof; i++) {
    F[i] = quad_tetra_Bessel(e, i, MSph) + quad_tetra_Bessel_VV(e, i, MSph);
  }
}
/*-------------------------------------------------------------- MATRICE DE LA PROJECTION SUR LE DOMAINE A DEUX SPHERES ------------------------------------------------*/
cplx integrale_ForProjec_PP(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la somme des intégrales sur les faces du produit de deux ondes planes associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    e      : le numero de l'élément
    i      : numero de la fonction-test
    j      : le numero de la solution
    Mesh   : le maillage 
   
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                              
  for (l = 0; l < 4; l++) {//boucle sur les faces                      
    I += integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
  }
  
  return I;
}



cplx integrale_ForProjec_VV(MKL_INT e, MKL_INT iloc, MKL_INT jloc, MailleSphere &MSph, const char chaine[]){
  /*----------------------------------------------------------------------------------- input ------------------------------------------------------------
    e       : numero local d'un élément
    iloc    : numero local fonction-test
    jloc    : ............ de la solution
    MSph    : le maillage
    /*--------------------------------------------------------------------------------- output ----------------------------------------------------------
                                                  I le résultat de l'intégrale
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {
    
    if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    I += din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}
/*---------------------------------------------------------------------------------------------------------------*/
cplx integrale_sur_4_face_PP(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) {
  //Cette fonction calcule la somme des intégrales sur les faces du produit de deux ondes planes associées à un meme élément
  /*------------------------------------------------------- input ---------------------------------------------------
    E      : structure définissant un element
    i      : numero de la fonction-test
    j      : le numero de la solution
    chaine : chaine de caratère pour définir la précision des intégrateurs.
             Si chaine = "double", on aura une double précision. 
             Sinon c'est la précision en quad  
    /*----------------------------------------------------- ouput ---------------------------------------------------
    I      : le resultat
    /*--------------------------------------------------------------------------------------------------------------*/ 
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                              
  for (l = 0; l < 4; l++) {//boucle sur les faces                      
    I += integrale_triangle(E.face[l], E.OP[iloc], E.OP[jloc], chaine);
  }
  
  return I;
}


cplx integrale_sur_4_face_VV(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]){
  /*----------------------------------------------------------------------------------- input ------------------------------------------------------------
    E      : structure définissant un element
    i      : numero de la fonction-test
    j      : le numero de la solution
    chaine : chaine de caratère pour définir la précision des intégrateurs.
             Si chaine = "double", on aura une double précision. 
             Sinon c'est la précision en quad  
    /*--------------------------------------------------------------------------------- output ----------------------------------------------------------
                                                  I le résultat de l'intégrale
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/

  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face l, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {
    
    if (l == 0) {
      din = cblas_ddot(3, E.OP[iloc].Ad, 1, E.n1, 1) ;
      
      djn = cblas_ddot(3, E.OP[jloc].Ad, 1, E.n1, 1) ;
      
      djn = djn * E.OP[jloc].ksurOmega;
      
      din = din * E.OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, E.OP[iloc].Ad, 1, E.n2, 1) ;
      
      djn = cblas_ddot(3, E.OP[jloc].Ad, 1, E.n2, 1) ;
      
      djn = djn * E.OP[jloc].ksurOmega;
      
      din = din * E.OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, E.OP[iloc].Ad, 1, E.n3, 1) ;
      
      djn = cblas_ddot(3, E.OP[jloc].Ad, 1, E.n3, 1) ;
      
      djn = djn * E.OP[jloc].ksurOmega;
      
      din = din * E.OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, E.OP[iloc].Ad, 1, E.n4, 1) ;
      
      djn = cblas_ddot(3, E.OP[jloc].Ad, 1, E.n4, 1) ;
      
      djn = djn * E.OP[jloc].ksurOmega;
      
      din = din * E.OP[iloc].ksurOmega;      
      
    }
    
    
    I += din * djn * integrale_triangle(E.face[l], E.OP[iloc], E.OP[jloc], chaine);
    
  }
  
  return I;
}

/*-----------------------------------------------------------------------------------------------------------------*/
void BuildMat_AreaProjection(MKL_INT e, cplx *MP, MailleSphere &MSph, const char chaine[]) {
 //Cette fonction calcule la matrice de projection Surfacique de PP + VV sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    e      : le numero de l'element 
    MP     : tableau ou stocker la matrice
    MSph   : le maillage
    /*---------------------------------------------------------------- ouput --------------------------------------------
    MP     : la matrice de la projection
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  for (i = 0; i < MSph.ndof; i++) {
    for (j = 0; j < MSph.ndof; j++) {
      MP[i * MSph.ndof + j] = integrale_ForProjec_PP(e, i, j, MSph, chaine) + integrale_ForProjec_VV(e, i, j, MSph, chaine);
    }
  }
}
  



/*-------------------------------------------------------------------- SECOND MEMBRE DE LA PROJECTION SUR LE DOMAINE A DEUX SPHERES -----------------------------------------*/

cplx Sum_integral_projection_PP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph){
  //Cette fonction calcule la somme des intégrales sur les faces de l'élément e, le produit d'une onde plane par la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0, 0.0);
  
  for ( l = 0; l < 4; l++) {
    I += integrale_triangle(e, l, iloc, MSph);
  }

  return I;
}



cplx Sum_integral_projection_VV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph){
  //Cette fonction calcule la somme des intégrales sur les faces de l'élément e, le produit d'une vitesse d'onde plane par la vitesse analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0, 0.0);
  
  for ( l = 0; l < 4; l++) {
    I += integrale_triangle_VincV(e, l, iloc, MSph);
  }
  
  return I;
}


void BuildVector_AreaProjection(MKL_INT e, cplx *F, MailleSphere &MSph){
  //Cette fonction calcule le vecteur second membre de la projection de PP + VV sur un élément
  /*----------------------------------------------------------------- input --------------------------------------------
    e      : le numero de l'element
    F      : tableau ou stocker le vecteur
    MSph   : le maillage
    /*---------------------------------------------------------------- ouput --------------------------------------------
    F     : le vecteur second membre 
    /*-------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc;
  for(iloc = 0; iloc < MSph.ndof; iloc++) {
    F[iloc] = Sum_integral_projection_PP(e, iloc, MSph) + Sum_integral_projection_VV(e, iloc, MSph);
  }
}



/*----------------------------------------------- CAS DE LA FONCTION DE BESSEL DE PREMIERE ESPECE -------------------------------*/

cplx integrale_triangle(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une onde plane par la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx P;
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    I = I + MSph.Qd2D.Wtriangle[i] * conj(MSph.elements[e].OP[iloc].pression(Xquad)) * P;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;

  return I;
  
}

cplx integrale_triangle(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une onde plane par la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;
  
  double *X1 = nullptr;//pour stocker le noeud numero 1 de la face f
  double *X2 = nullptr;//............................ 2 ............
  double *X3 = nullptr;//............................ 3 ............

  cplx P;
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph, XX0);
    
    I = I + MSph.Qd2D.Wtriangle[i] * conj(MSph.elements[e].OP[iloc].pression(Xquad)) * P;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  return I;
  
}



void integrale_triangle(MKL_INT e, MKL_INT f, cplx *I, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une onde plane par la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    I      : tableau des résultats d'intégrations
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    iloc   : numéro d'une fonction-test
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

  MKL_INT i, j, iloc;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx P;
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);

    for (iloc = 0; iloc < MSph.ndof; iloc++) {
    
    I[iloc] = I[iloc] + MSph.Qd2D.Wtriangle[i] * conj(MSph.elements[e].OP[iloc].pression(Xquad)) * P *  MSph.elements[e].face[f].Aire;
    }
  }

  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;
  
}




cplx integrale_triangle_PincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une vitesse d'onde plane par la solution analytique associée aux fontions de Bessel j0 et y0.
   /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;

   cplx P;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;//OP[iloc].V \cdot n_f avec n_f la normale à la face f et OP[iloc].V la vitesse de l'onde OP[iloc].V
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);

    //calcule de l'onde plane et sa vitesse
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);
    //la vitesse est stockée dans OP[iloc].V
    
    if(f == 0){     
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n1);
    }
    else if(f == 1){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n2);
    }
    else if(f == 2){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n3);
    }
    else if(f == 3){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * P * Vn_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;

  return I;
  
}


cplx integrale_triangle_PincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une vitesse d'onde plane par la solution analytique associée aux fontions de Bessel j0 et y0.
   /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;

   cplx P;
  
  double *X1 = nullptr;//pour stocker le noeud numero 1 de la face f
  double *X2= nullptr;//............................ 2 ............
  double *X3= nullptr;//............................ 3 ............

  cplx Vn_f;//OP[iloc].V \cdot n_f avec n_f la normale à la face f et OP[iloc].V la vitesse de l'onde OP[iloc].V
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph, XX0);

    //calcule de l'onde plane et sa vitesse
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);
    //la vitesse est stockée dans OP[iloc].V
    
    if(f == 0){     
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n1);
    }
    else if(f == 1){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n2);
    }
    else if(f == 2){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n3);
    }
    else if(f == 3){
      Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * P * Vn_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  return I;
  
}


void integrale_triangle_PincV(MKL_INT e, MKL_INT f, cplx *I, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une vitesse d'onde plane par la solution analytique associée aux fontions de Bessel j0 et y0.
   /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces de e
    I      : tableau pour stocker les résultats d'intégrations
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    iloc   : numero d'une fonction-test ou onde plane
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

  MKL_INT i, j, iloc;

   cplx P;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;//OP[iloc].V \cdot n_f avec n_f la normale à la face f et OP[iloc].V la vitesse de l'onde OP[iloc].V
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();


  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);

    for(iloc = 0; iloc < MSph.ndof; iloc++) {
    
      //calcule de l'onde plane et sa vitesse
      MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);
      //la vitesse est stockée dans OP[iloc].V
      
      if(f == 0){     
	Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n1);
      }
      else if(f == 1){
	Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n2);
      }
      else if(f == 2){
	Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n3);
      }
      else if(f == 3){
	Vn_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n4);
      }   
      
      
      I[iloc] = I[iloc] + MSph.Qd2D.Wtriangle[i] * P * Vn_f * MSph.elements[e].face[f].Aire;
      
    }
  }
  
  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;
  
}




cplx integrale_triangle_VincP(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une onde plane par la vitesse de la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces (triangles) de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx Vn_f;//V \cdot n_f avec n_f la normale à la face f et V la vitesse de la solution analytique
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();

   cplx P;
   cplx *V = new cplx[3];

  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    
    if(f == 0){     
      Vn_f = dot_u(V, MSph.elements[e].n1);
    }
    else if(f == 1){
      Vn_f = dot_u(V, MSph.elements[e].n2);
    }
    else if(f == 2){
      Vn_f = dot_u(V, MSph.elements[e].n3);
    }
    else if(f == 3){
      Vn_f = dot_u(V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * conj(MSph.elements[e].OP[iloc].pression(Xquad)) * Vn_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;
  delete [] V; V  =  nullptr;

  return I;
  
}


cplx integrale_triangle_VincP(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une onde plane par la vitesse de la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces (triangles) de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 

   MKL_INT i, j;
  
  double *X1 = nullptr;//pour stocker le noeud numero 1 de la face f
  double *X2= nullptr;//............................ 2 ............
  double *X3= nullptr;//............................ 3 ............
  

  cplx Vn_f;//V \cdot n_f avec n_f la normale à la face f et V la vitesse de la solution analytique
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();

   cplx P;

  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph, d, XX0, gradP);
    
    
    if(f == 0){     
      Vn_f = dot_u(V, MSph.elements[e].n1);
    }
    else if(f == 1){
      Vn_f = dot_u(V, MSph.elements[e].n2);
    }
    else if(f == 2){
      Vn_f = dot_u(V, MSph.elements[e].n3);
    }
    else if(f == 3){
      Vn_f = dot_u(V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * conj(MSph.elements[e].OP[iloc].pression(Xquad)) * Vn_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  return I;
  
}



cplx integrale_triangle_VincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une vitesse d'onde plane par la vitesse de la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces (triangles) de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1;//pour stocker le noeud numero 1 de la face f
  double *X2;//............................ 2 ............
  double *X3;//............................ 3 ............
  double *X2X3 = new double[3];//X2 - X3
  double *X1X3 = new double[3];//X1 - X3
  double *Xquad = new double[3];

  cplx V1n_f;//j0.V \cdot n_f avec n_f la normale à la face f et j0.V la vitesse associée à j0
  cplx V2n_f;//P1.V \cdot n_f avec n_f la normale à la face f et P1.V la vitesse de l'onde plane P1
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();

   cplx P;
   cplx *V = new cplx[3];

  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);


    //calcule de l'onde plane et sa vitesse
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);
    
    if(f == 0){     
      V1n_f = dot_u(V, MSph.elements[e].n1);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n1);
    }
    else if(f == 1){
      V1n_f = dot_u(V, MSph.elements[e].n2);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n2);
    }
    else if(f == 2){
      V1n_f = dot_u(V, MSph.elements[e].n3);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n3);
    }
    else if(f == 3){
      V1n_f = dot_u(V, MSph.elements[e].n4);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * V1n_f * V2n_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  delete [] Xquad; Xquad  =  nullptr;
  delete [] X2X3; X2X3  =  nullptr;
  delete [] X1X3; X1X3  =  nullptr;
  delete [] V; V  =  nullptr;

  return I;
  
}


cplx integrale_triangle_VincV(MKL_INT e, MKL_INT f, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) {
  //Cette fonction calcule sur un triangle, l'intégrale du produit d'une vitesse d'onde plane par la vitesse de la solution analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------------------------ input ------------------------------------------------------------------------------
    e      : numero de l'élément.
    f      : numero d'une des faces (triangles) de e
    iloc   : numero d'une fonction-test ou onde plane
    MSph   : le maillage 
    /*----------------------------------------------------------------------------- ouput -------------------------------------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------- Intermédiare ------------------------------------------------------------------------
    X1     : noeud numero 1 de la face f
    X2     : noeud numero 2 ............
    X3     : noeud numero 3 ............
    P      : la pression analytique
    V      : la vitesse analytique
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

   MKL_INT i, j;
  
  double *X1 = nullptr;//pour stocker le noeud numero 1 de la face f
  double *X2 = nullptr;//............................ 2 ............
  double *X3 = nullptr;//............................ 3 ............

  cplx V1n_f;//j0.V \cdot n_f avec n_f la normale à la face f et j0.V la vitesse associée à j0
  cplx V2n_f;//P1.V \cdot n_f avec n_f la normale à la face f et P1.V la vitesse de l'onde plane P1
  
  X1 = MSph.elements[e].face[f].noeud[0]->get_X();
  X2 = MSph.elements[e].face[f].noeud[1]->get_X();
  X3 = MSph.elements[e].face[f].noeud[2]->get_X();

   cplx P;

  for (i = 0; i < 3; i++){
    
    X1X3[i] = X1[i] - X3[i];
    
    X2X3[i] = X2[i] - X3[i];
    
  }

  cplx I = cplx(0.0,0.0);
 
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre ; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = X3[j] + MSph.Qd2D.Xtriangle[2*i] * X1X3[j] + MSph.Qd2D.Xtriangle[2*i+1] * X2X3[j];
    }

    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph, d, XX0, gradP);


    //calcule de l'onde plane et sa vitesse
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);
    
    if(f == 0){     
      V1n_f = dot_u(V, MSph.elements[e].n1);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n1);
    }
    else if(f == 1){
      V1n_f = dot_u(V, MSph.elements[e].n2);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n2);
    }
    else if(f == 2){
      V1n_f = dot_u(V, MSph.elements[e].n3);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n3);
    }
    else if(f == 3){
      V1n_f = dot_u(V, MSph.elements[e].n4);
      V2n_f = dot_c(MSph.elements[e].OP[iloc].V, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * V1n_f * V2n_f;
  }
  
  I = I * MSph.elements[e].face[f].Aire;

  return I;
  
}



cplx Sum_quad_triangle_PincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(P') sur des faces exterieures avec Pinc la pression analytique associée aux fonctions de Bessel j0 et y0.
    /*------------------------------------------------------------ input -----------------------------------------------------
      e      : numero d'un élément
      iloc   : numero d'une fonction-test
      MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_PincP[l] * integrale_triangle(e, l, iloc, MSph);
  
    }
  }
  return I;
}


cplx Sum_quad_triangle_PincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(P') sur des faces exterieures avec Pinc la pression analytique associée aux fonctions de Bessel j0 et y0.
    /*------------------------------------------------------------ input -----------------------------------------------------
      e      : numero d'un élément
      iloc   : numero d'une fonction-test
      MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_PincP[l] * integrale_triangle(e, l, iloc, MSph, X2X3, X1X3, Xquad, XX0);
  
    }
  }
  return I;
}


cplx Sum_quad_triangle_PincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces exterieures avec Pinc la pression analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_PincV[l] * integrale_triangle_PincV(e, l, iloc, MSph);
  
    }
  }
  return I;
}


cplx Sum_quad_triangle_PincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, double *XX0) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces exterieures avec Pinc la pression analytique associée aux fonctions de Bessel j0 et y0.
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_PincV[l] * integrale_triangle_PincV(e, l, iloc, MSph, X2X3, X1X3, Xquad, XX0);
  
    }
  }
  return I;
}



cplx Sum_quad_triangle_VincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule la somme des intégrales de Vinc * conj(P') sur des faces exterieures avec Vinc la vitesse analytique associée aux fonctions de Bessel j0 et y0, P' une onde plane.
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_VincP[l] * integrale_triangle_VincP(e, l, iloc, MSph);
    
    }
  }
  return I;
}

cplx Sum_quad_triangle_VincP(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) {
  //Cette fonction calcule la somme des intégrales de Vinc * conj(P') sur des faces exterieures avec Vinc la vitesse analytique associée aux fonctions de Bessel j0 et y0, P' une onde plane.
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_VincP[l] * integrale_triangle_VincP(e, l, iloc, MSph, X2X3, X1X3, Xquad, V, d, XX0, gradP);
    
    }
  }
  return I;
}


cplx Sum_quad_triangle_VincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces exterieures avec Vinc la vitesse analytique associée aux fonctions de Bessel j0 et y0 et V' vitesse d'une onde plane
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_VincV[l] * integrale_triangle_VincV(e, l, iloc, MSph);
    
    }
  }
  return I;
}


cplx Sum_quad_triangle_VincV(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, double *X2X3, double *X1X3, double *Xquad, cplx *V, double *d, double *XX0, cplx *gradP) {
  //Cette fonction calcule la somme des intégrales de Pinc * conj(V') sur des faces exterieures avec Vinc la vitesse analytique associée aux fonctions de Bessel j0 et y0 et V' vitesse d'une onde plane
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : numero d'un élément
    iloc   : numero d'une fonction-test
    MSph   : le maillage 
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  cplx I = cplx(0.0,0.0);
  for(l = 0; l < 4; l++){
    if (MSph.elements[e].voisin[l] < 0) {
      I +=  MSph.elements[e].alpha_VincV[l] * integrale_triangle_VincV(e, l, iloc, MSph, X2X3, X1X3, Xquad, V, d, XX0, gradP);
    
    }
  }
  return I;
}


void ASSEMBLAGE_DU_VecteurRHS(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = Sum_quad_triangle_PincP(i, iloc, MSph) + Sum_quad_triangle_PincV(i, iloc, MSph) + Sum_quad_triangle_VincP(i, iloc, MSph) + Sum_quad_triangle_VincV(i, iloc, MSph);  
    }
  }
}


void ASSEMBLAGE_DU_VecteurRHS_parallel_version(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for (i = 0; i < MSph.get_N_elements(); i++) {
      for (MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	MKL_INT iglob = local_to_glob(iloc, i, MSph.ndof); 
	B[iglob] = Sum_quad_triangle_PincP(i, iloc, MSph) + Sum_quad_triangle_PincV(i, iloc, MSph) + Sum_quad_triangle_VincP(i, iloc, MSph) + Sum_quad_triangle_VincV(i, iloc, MSph);  
      }
    }
  }
}


void ASSEMBLAGE_DU_VecteurRHS_parallel_version_II(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
  double *X2X3_PP = nullptr, *X2X3_PV = nullptr, *X2X3_VP = nullptr, *X2X3_VV = nullptr;
  double *X1X3_PP = nullptr, *X1X3_PV = nullptr, *X1X3_VP = nullptr, *X1X3_VV = nullptr;
  double *Xquad_PP = nullptr, *Xquad_PV = nullptr, *Xquad_VP = nullptr, *Xquad_VV = nullptr;
  double *d_VP = nullptr, *d_VV = nullptr;
  double *XX0_PP = nullptr, *XX0_PV = nullptr, *XX0_VP = nullptr, *XX0_VV = nullptr;
  cplx *gradP_VP = nullptr, *gradP_VV = nullptr;
  cplx   *V_VP = nullptr, *V_VV = nullptr;

 
  
#pragma omp parallel private(X2X3_PP, X1X3_PP, Xquad_PP, XX0_PP, X2X3_PV, X1X3_PV, Xquad_PV, XX0_PV, X2X3_VP, X1X3_VP, Xquad_VP, XX0_VP, X2X3_VV, X1X3_VV, Xquad_VV, XX0_VV, d_VP, d_VV, gradP_VP, gradP_VV, V_VP, V_VV) 
  {
    X2X3_PP = new double[3]; X1X3_PP = new double[3]; Xquad_PP = new double[3]; XX0_PP = new double[3];
    X2X3_VP = new double[3]; X1X3_VP = new double[3]; Xquad_VP = new double[3]; XX0_VP = new double[3];
    X2X3_PV = new double[3]; X1X3_PV = new double[3]; Xquad_PV = new double[3]; XX0_PV = new double[3];
    X2X3_VV = new double[3]; X1X3_VV = new double[3]; Xquad_VV = new double[3]; XX0_VV = new double[3];
    gradP_VP = new cplx[3];  gradP_VV = new cplx[3];
    d_VP = new double[3];  d_VV = new double[3];
    V_VP = new cplx[3];  V_VV = new cplx[3];
#pragma omp for schedule(runtime)
    for (i = 0; i < MSph.get_N_elements(); i++) {
      for (MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	MKL_INT iglob = local_to_glob(iloc, i, MSph.ndof); 
	B[iglob] = Sum_quad_triangle_PincP(i, iloc, MSph, X2X3_PP, X1X3_PP, Xquad_PP, XX0_PP) + Sum_quad_triangle_PincV(i, iloc, MSph, X2X3_PV, X1X3_PV, Xquad_PV, XX0_PV) + Sum_quad_triangle_VincP(i, iloc, MSph, X2X3_VP, X1X3_VP, Xquad_VP, V_VP, d_VP, XX0_VP, gradP_VP) + Sum_quad_triangle_VincV(i, iloc, MSph, X2X3_VV, X1X3_VV, Xquad_VV, V_VV, d_VV, XX0_VV, gradP_VV);  
      }
    }
    delete [] X2X3_PP; X2X3_PP = nullptr; delete [] X1X3_PP; X1X3_PP = nullptr;
    delete [] Xquad_PP; Xquad_PP = nullptr; delete [] XX0_PP; XX0_PP = nullptr;

    delete [] X2X3_PV; X2X3_PV = nullptr; delete [] X1X3_PV; X1X3_PV = nullptr;
    delete [] Xquad_PV; Xquad_PV = nullptr; delete [] XX0_PV; XX0_PV = nullptr;

    delete [] X2X3_VP; X2X3_VP = nullptr; delete [] X1X3_VP; X1X3_VP = nullptr;
    delete [] Xquad_VP; Xquad_VP = nullptr; delete [] XX0_VP; XX0_VP = nullptr;

    delete [] X2X3_VV; X2X3_VV = nullptr; delete [] X1X3_VV; X1X3_VV = nullptr;
    delete [] Xquad_VV; Xquad_VV = nullptr; delete [] XX0_VV; XX0_VV = nullptr;

    delete [] d_VP; d_VP = nullptr; delete [] d_VV; d_VV = nullptr;
    delete [] gradP_VP; gradP_VP = nullptr; delete [] gradP_VV; gradP_VV = nullptr;

    delete [] V_VP; V_VP = nullptr; delete [] V_VV; V_VV = nullptr;
 
  }
}


/*-------------------------------------------------------------------------------------- Build F5 --------------------------------------------------------*/

void ASSEMBLAGE_DU_VecteurRHS_F5(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global Pinc P' et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = Sum_quad_triangle_PincP(i, iloc, MSph);  
    }
  }
}


/*-------------------------------------------------------------------------------------- Build F6 --------------------------------------------------------*/

void ASSEMBLAGE_DU_VecteurRHS_F6(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global Pinc V'n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = Sum_quad_triangle_PincV(i, iloc, MSph);  
    }
  }
}


/*-------------------------------------------------------------------------------------- Build F7 ---------------------------------------------------------*/

void ASSEMBLAGE_DU_VecteurRHS_F7(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = Sum_quad_triangle_VincP(i, iloc, MSph) ;  
    }
  }
}


/*-------------------------------------------------------------------------------------- Build F8 ----------------------------------------------------------*/

void ASSEMBLAGE_DU_VecteurRHS_F8(cplx *B, MailleSphere &MSph){
   //cette fonction calcule le vecteur global V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = Sum_quad_triangle_VincV(i, iloc, MSph);  
    }
  }
}


/*-----------------------------------------------------------------------------------------------------------------------------------------------------------*/

void Build_SUM_A1_to_A8(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice de la formulation consistante de Cessenat-Desprès à partir des 8 formes de A1 à A8
  /*--------------------------------------------------------------------------------- input ---------------------------------------------------------------------------------
    A_glob     : tableau skyline ou stocké la matrice 
    MSph       : le maillage
    /*------------------------------------------------------------------------------- output -------------------------------------------------------------------------------
                        A_glob la matrice consistante de Cessenat-Desprès
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  Build_A1(A_glob, MSph, chaine);

  Build_A2(A_glob, MSph, chaine);

  Build_A3(A_glob, MSph, chaine);

  Build_A4(A_glob, MSph, chaine);

  Build_A5(A_glob, MSph, chaine);

  Build_A6(A_glob, MSph, chaine);

  Build_A7(A_glob, MSph, chaine);

  Build_A8(A_glob, MSph, chaine); 
}


/*--------------------------------------------------------------------------- diag ([P] [P']) --------------------------------------------------------------------------*/
void BlocIntDiagPP(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [P][P'] sur l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    alpha     : coeff associé au bloc
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale correspondante à l'emplacement (i,0) dans A_skyline
    Build_BlocInt_P_TxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_DiagA1(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [P][P']
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntDiagPP(i, j, MSph.elements[i].alphaPP[j], A_glob, MSph, chaine);
    }
  }
}

/*----------------------------------------------------------------------------- diag([V] [V'])----------------------------------------------------------------------*/
void BlocIntDiagVV(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [V][V'] sur l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    alpha     : coeff associé au bloc
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale correspondante à l'emplacement (i,0) dans A_skyline
    Build_BlocInt_V_TxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_DiagA4(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [V][V']
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntDiagVV(i, j, MSph.elements[i].alphaVV[j], A_glob, MSph, chaine);
    }
  }
}


void Build_A5_M(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {//boucle sur les éléments
    for(l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtPP_Add(i, l, MSph.elements[i].alphaPP[l], A_glob, MSph, chaine);
    }
  }
}


void Build_A8_M(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
   //Calcul la matrice globale V n V' n sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      BlocExtVV_Add(i, l, MSph.elements[i].alphaVV[l], A_glob, MSph, chaine);
    }
  }
}



/*------------------------------------------------------------------------------------ Building M with A1, A4, A5 and A8 ----------------------------------------------------------*/

void Build_M_withA1_A4_A5_A8(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice de M de Cessenat-Desprès
  Build_DiagA1(M_glob, MSph, chaine);

  Build_DiagA4(M_glob, MSph, chaine);

  Build_A5_M(M_glob, MSph, chaine);

  Build_A8_M(M_glob, MSph, chaine);
  
}



void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skyline &A_glob, MailleSphere &MSph) {
  //Cette fonction calcule la solution du système linéaire AU = B avec A = A1 + ... + A8
  // et B = F5 + ... + F8
  /*--------------------------------------------------------------------------------- input ----------------------------------------------------------------------------------
    U      : tableau ou stocké la solution 
    B      : le vecteur second membre
    A_glob : la matrice A en format skyline
    MSph   : le maillage
    /*------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                             U la solution
    /*------------------------------------------------------------------------------ intermédiare ----------------------------------------------------------------------------
    AS     : structure Sparse pour stocker la matrice
    res    : objet de type VecN ; res = A U
    diff   : .................. ; diff = res - B; c'est le résidu
    maxx   : le max du résidu
    minn   : le min du résidu
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
   MKL_INT iloc;
   cout<<" Resolution de (A1 + A2 + ...... + A8) U = F5 + ... + F8 :"<<endl;
  Sparse AS;
  
  AS.m = U.n;
  AS.n = U.n;

  AS.transforme_A_skyline_to_Sparse_CSR(A_glob, MSph);

  AS.compute_solution(B, U);
  
  VecN res, diff;
  
  res.n = AS.n;
  
  res.Allocate();

  diff.n = AS.n;
  
  diff.Allocate();
  
  produit_MatSparse_vecteur(AS, U, res);

  SoustractionVecN(res, B, diff);
  
  double maxx = 0;
  double minn = 1000;

  cplx val;
  
  for (MKL_INT i = 0; i < diff.n; i++) {
    maxx = max(maxx,abs(diff.value[i]));
    minn = min(minn,abs(diff.value[i])); 

  }
  
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
  
}


void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skylineRed &A_red, MailleSphere &MSph) {
  //Cette fonction calcule la solution du système linéaire AU = B avec A = A1 + ... + A8
  // et B = F5 + ... + F8
  /*--------------------------------------------------------------------------------- input ----------------------------------------------------------------------------------
    U      : tableau ou stocké la solution 
    B      : le vecteur second membre
    A_red : la matrice A en format skyline
    MSph   : le maillage
    /*------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                             U la solution
    /*------------------------------------------------------------------------------ intermédiare ----------------------------------------------------------------------------
    AS     : structure Sparse pour stocker la matrice
    res    : objet de type VecN ; res = A U
    diff   : .................. ; diff = res - B; c'est le résidu
    maxx   : le max du résidu
    minn   : le min du résidu
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
   MKL_INT iloc;
   cout<<" Resolution de (A1 + A2 + ...... + A8) U = F5 + ... + F8 :"<<endl;
  Sparse AS;
  
  AS.m = U.n;
  AS.n = U.n;

  AS.transforme_A_skyline_to_Sparse_CSR(A_red, MSph);

  AS.compute_solution(B, U);
  
  VecN res, diff;
  
  res.n = AS.n;
  
  res.Allocate();

  diff.n = AS.n;
  
  diff.Allocate();
  
  produit_MatSparse_vecteur(AS, U, res);

  SoustractionVecN(res, B, diff);
  
  double maxx = 0;
  double minn = 1000;

  cplx val;
  
  for (MKL_INT i = 0; i < diff.n; i++) {
    maxx = max(maxx,abs(diff.value[i]));
    minn = min(minn,abs(diff.value[i])); 

  }
  
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
  
}
/*-----------------------------------------------------------------------------------------------------------------------------*/

void Solve_Sol_with_Sparse(VecN &U, VecN &B, Sparse &AS, MailleSphere &MSph) {
  //Cette fonction calcule la solution du système linéaire AU = B avec A = A1 + ... + A8
  // et B = F5 + ... + F8
  /*--------------------------------------------------------------------------------- input ----------------------------------------------------------------------------------
    U      : tableau ou stocké la solution 
    B      : le vecteur second membre
    AS     : structure Sparse pour stocker la matrice
    MSph   : le maillage
    /*------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                             U la solution
    /*------------------------------------------------------------------------------ intermédiare ----------------------------------------------------------------------------
    res    : objet de type VecN ; res = A U
    diff   : .................. ; diff = res - B; c'est le résidu
    maxx   : le max du résidu
    minn   : le min du résidu
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
   MKL_INT iloc;
   cout<<" Resolution de (A1 + A2 + ...... + A8) U = F5 + ... + F8 :"<<endl;

  AS.compute_solution(B, U);
  
  VecN res, diff;
  
  res.n = AS.n;
  
  res.Allocate();

  diff.n = AS.n;
  
  diff.Allocate();
  
  produit_MatSparse_vecteur(AS, U, res);

  SoustractionVecN(res, B, diff);
  
  double maxx = 0;
  double minn = 1000;

  cplx val;
  
  for (MKL_INT i = 0; i < diff.n; i++) {
    maxx = max(maxx,abs(diff.value[i]));
    minn = min(minn,abs(diff.value[i])); 

  }
  
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
  
}

/*************************************************************************************** PARTIE SYSTEME GLOBAL *************************************************************************/


void compute_sizeBlocRed(MKL_INT e, MKL_INT l, cplx *AA, double *w, A_skyline &A_glob, MailleSphere &MSph) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* (avec P une matrice orthonormale et D contient les valeurs propres) et calcule la taille de la nouvelle base réduite de Galerkin associée à l'élément e
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    AA        : pour clôner A_glob sur chaque élément
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    A_glob : tableau contenant tous les blocs
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Nred : la taille de la nouvelle base réduite de Galerkin
    Nres : ndof - Nred
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;

  MSph.elements[e].Nred = 0;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[jloc * MSph.ndof + iloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);

  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    if (w[iloc] > MSph.elements[e].eps_max){
      MSph.elements[e].Nred++;
    }
  }

  MSph.elements[e].Nres = MSph.ndof - MSph.elements[e].Nred;
}



MKL_INT compute_sizeBlocRed(A_skyline &A_glob, MailleSphere &MSph) {
  //Cette fonction calcule la taille réduite de la grande matrice de passage P_red correspondante à la nouvelle base de Galerkin discrète
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, taille = 0;
  cplx *AA;
  double *w;
  AA = new cplx[MSph.ndof * MSph.ndof];
  w = new double[MSph.ndof];//pour stocker les valeurs propres; w = D
  for (e = 0; e < MSph.get_N_elements(); e++){
    
    compute_sizeBlocRed(e, 0, AA, w, A_glob, MSph);
    
    MKL_INT r = A_glob.VerifyDecompositionPMP(e, 0, AA, w, MSph);
    
    if (r == 1) {
      MSph.SIZE[e] = taille;
      taille += MSph.elements[e].Nred;
    }
    else{
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
    
  }
  
  delete [] AA; AA  =  nullptr;
  delete [] w; w  =  nullptr;
  
  return taille;
}


MKL_INT compute_sizeBlocRed_parallel_version(A_skyline &A_glob, MailleSphere &MSph) {
  //Cette fonction calcule la taille réduite de la grande matrice de passage P_red correspondante à la nouvelle base de Galerkin discrète
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, taille = 0;
  cplx *AA;
  double *w;
  #pragma omp parallel private(AA, w, e)
  {
    AA = new cplx[MSph.ndof * MSph.ndof];
    w = new double[MSph.ndof];//pour stocker les valeurs propres; w = D
#pragma omp for schedule(runtime) 
    for (e = 0; e < MSph.get_N_elements(); e++){
      
      compute_sizeBlocRed(e, 0, AA, w, A_glob, MSph);
      MKL_INT r = A_glob.VerifyDecompositionPMP(e, 0, AA, w, MSph);
      
      if (r != 1) {
	cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
	exit(EXIT_FAILURE);
      }
      
    }
    delete [] AA; AA  =  nullptr;
    delete [] w; w  =  nullptr;
  }
  
  for (e = 0; e < MSph.get_N_elements(); e++){
    MSph.SIZE[e] = taille;
    taille += MSph.elements[e].Nred;
  }
  
  return taille;
}


MKL_INT compute_sizeForA_red(MailleSphere &MSph){
  //Cette fonction calcule la taille réduite de la grande matrice A_red  = P_red^* A P_red
  //Faire d'abord appel à la fonction compute_sizeBlocRed(A_skyline &A_glob, Maillage &MSph) pour définir la taille de chaque bloc P_red
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de A_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille = 0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
   
    taille += MSph.elements[e].Nred * MSph.elements[e].Nred;
  }

  MKL_INT f;
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    for (MKL_INT j = 0; j < 4; j++){
      if (MSph.elements[e].voisin[j] >= 0){
	
      f = MSph.elements[e].voisin[j];
      taille += MSph.elements[e].Nred * MSph.elements[f].Nred;
      }
      else{
	
	taille += MSph.elements[e].Nred * MSph.elements[e].Nred;
      }
    }
  }
  
  return taille;
}



void DecompositionDeBloc(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    P_red  : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    P         : ........... P_red ...................
    V         : ........... LAMBDA ..................
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, JLOC;

  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  cplx *P = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *V;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[jloc * MSph.ndof + iloc];
    }
  }
  
  
  double *w = new double[MSph.ndof];//pour stocker les valeurs propres; w = D
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);
  
  //stockage de la base réduite associée à l'élément e
  ILOC = 0;
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    JLOC = 0;
    for (jloc = MSph.ndof-1; jloc >= MSph.elements[e].Nres; jloc--){
      P[JLOC * MSph.ndof + ILOC] = AA[jloc * MSph.ndof + iloc];
      JLOC++;
    }
    ILOC++;
  }
  
  //clônage du tableau LAMBDA sur l'élément e
  get_F_elemRed(e, &V, LAMBDA, MSph);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc = MSph.ndof-1; iloc >=  MSph.elements[e].Nres; iloc--){
      V[ILOC] = w[iloc];
      ILOC++;
  }
  
 
  delete [] w; w  =  nullptr;
  delete [] AA; AA  =  nullptr;
}



void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    P_red  : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    P         : ........... P_red ...................
    V         : ........... LAMBDA ..................
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, kloc, JLOC;
  
  cplx *P = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *V;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[jloc * MSph.ndof + iloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);

  //clônage du tableau LAMBDA sur l'élément e
  get_F_elemRed(e, &V, LAMBDA, MSph);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc = MSph.ndof-1; iloc >= MSph.elements[e].Nres; iloc--){
    V[ILOC] = w[iloc];
    ILOC++;
  }
  
  
  //stockage de la base réduite associée à l'élément e
  ILOC = 0;
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    JLOC = 0;
    for (jloc = MSph.ndof-1; jloc >= MSph.elements[e].Nres; jloc--){
      for (kloc = MSph.ndof-1; kloc >= MSph.elements[e].Nres; kloc--){
	if (kloc == jloc) {
	  P[JLOC * MSph.ndof + ILOC] = AA[jloc * MSph.ndof + iloc] * (1.0 / sqrt(w[kloc]));
	}
      }
      JLOC++;
    }
    ILOC++;
  }
  
}
/**********************************************************************************************************/
void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT ndof, MKL_INT Nred, MKL_INT Nres, MKL_INT taille, cplx *A_loc, cplx *P_loc, cplx *V, cplx *AA, double *w) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    taille : la taille du bloc A_loc de e
    A_loc  : tableau contenant le bloc de e à diagonaliser
    P_loc  : tableau pour stocker la base réduite de vecteurs propres sur e
    V      : .................... les N_red plus grandes valeurs propres de la diagonalisation 
    MSph   : le maillage
    AA        : pour clôner A_glob sur chaque élément
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
 /*---------------------------------------------------------------- variables intermédiaires ----------------------------------------
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, JLOC, kloc;

  copy_tab(taille, A_loc, taille, AA) ;
   
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', ndof, AA, ndof, w);

 //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  //#pragma omp parallel private(ILOC, JLOC, iloc, jloc)
  // {
    ILOC = 0;
    //#pragma omp for schedule(runtime)
    for (iloc = ndof-1; iloc >= Nres; iloc--){
      ILOC = ndof-1 - iloc;
      V[ILOC] = w[iloc];
    }
    
    //stockage de la base réduite associée à l'élément e
    //#pragma omp for schedule(runtime)
    for (iloc = 0; iloc < ndof; iloc++){
      for (jloc = ndof-1; jloc >= Nres; jloc--){
	JLOC = ndof-1 - jloc;
	P_loc[JLOC * ndof + iloc] = AA[jloc * ndof + iloc] * (1.0 / sqrt(w[jloc]));
      }
    }
    //}
}

/*******************************************************************************************************/

void DecompositionGlobaleDesBlocs(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P_red : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    
    DecompositionDeBloc(e, 0, A_glob, P_red, LAMBDA, MSph);
  }
}


void DecompositionGlobaleDesBlocs_ET_Normalisation(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P_red : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e;
  cplx *AA;
  double *D;
  AA = new cplx[MSph.ndof * MSph.ndof];
  D = new double[MSph.ndof];
  for (e = 0; e < MSph.get_N_elements(); e++){
    
    DecompositionDeBloc_ET_Normalisation(e, 0, A_glob, P_red, LAMBDA, MSph, AA, D);
  }
  delete [] AA; AA  =  nullptr;
  delete [] D; D  =  nullptr; 
}


void DecompositionGlobaleDesBlocs_ET_Normalisation_parallel_version(A_skyline &A_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P_red : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, taille = MSph.ndof * MSph.ndof, N = MSph.ndof, Nred, Nres;
  //cplx *AA;
  //double *D;
  cplx *P_loc, *A_loc, *V;
#pragma omp parallel private(e, A_loc, P_loc, V, Nred, Nres) shared(A_glob, P_red, LAMBDA, N)
  {
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++){
      cplx *AA = new cplx[taille];
      double *D = new double[N];
      Nred = MSph.elements[e].Nred;
      Nres = MSph.elements[e].Nres;
      P_loc = P_red.Tab_A_loc[e].get_Aloc();
      A_loc = A_glob.Tab_A_loc[e].get_Aloc();
      get_F_elemRed(e, &V, LAMBDA, MSph);
      DecompositionDeBloc_ET_Normalisation(e, N, Nred, Nres, taille, A_loc, P_loc, V, AA, D) ;
      //DecompositionDeBloc_ET_Normalisation(e, 0, A_glob, P_red, LAMBDA, MSph, AA, D);
      delete [] AA; AA  =  nullptr;
      delete [] D; D  =  nullptr;
    }
    
  }
}



void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A_skyline &P_red,  MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc;
  
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
   
    for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	  	
      
      a[jloc * MSph.ndof + iloc] = PP_red[jloc * MSph.ndof + iloc];
      
      b[iloc * MSph.elements[e].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
   
    }      
  }
}


void store_P_and_transpose_P(MKL_INT e, cplx *a, A_skyline &P_red,  const string & arg, MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra soit la restriction de 
                P_red , soit la restriction de l'adjoint de P_red sur l'élément e
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    arg       : chaîne de caractères, elle détermine l'opération à faire selon :
                arg == "selfP", stocke la restriction de P_red à l'élément e dans a
		arg == "adjtP", ....................... l'adjoint de P_red à l'élément e dans b
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a    :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc;
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  if (arg == "selfP"){
    for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
  
      for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	  	
	a[jloc * MSph.ndof + iloc] = PP_red[jloc * MSph.ndof + iloc];
	
      }      
    }
  }
  
  else  if (arg == "adjtP"){
    
    for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    
      for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	  	
	a[iloc * MSph.elements[e].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
	
      }      
    }
  }
  
  else {
    cout<<"probleme de mot clef"<<endl;
  }
  
}

/*----------------------------------------------------------------------- P_red and (P_red)^* -----------------------------------------------------*/

void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, cplx *b, A_skyline &P_red, MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    j         : le numero local du voisin de e
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur le voisin de l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PPP_red    : contient la restriction de P_red au voisin de e mais pas en taille réelle
    PP_red     : ....................... de l'adjoint de P_red sur e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, f;

  f = MSph.elements[e].voisin[j];
  
  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *PPP_red = P_red.Tab_A_loc[f].get_Aloc();
  
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
  
    for (jloc = 0; jloc < MSph.elements[f].Nred; jloc++) { // numero de la colonne	  	
      a[jloc * MSph.ndof + iloc] = PPP_red[jloc * MSph.ndof + iloc];
  
    }      
  }

  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    
    for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	  	      
      b[iloc * MSph.elements[e].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
    
    }      
  }
  
}				  



void store_P_and_transpose_P(MKL_INT e, MKL_INT j, cplx *a, A_skyline &P_red, const string & arg, MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    j         : le numero local de voisin de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra soit la restriction de 
                 P_red au voisin de e, soit la restriction de l'adjoint de P_red sur e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    arg       : chaîne de caractères, elle détermine l'opération à faire selon :
                arg == "Pj", stocke la restriction de P_red au voisin de e sur a
		arg == "Pi", ....................... l'adjoint de P_red à l'élément e sur a
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a    :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PPP_red    : contient la restriction de P_red au voisin de e mais pas en taille réelle
    PP_red     : ....................... de l'adjoint de P_red sur e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, f;

  f = MSph.elements[e].voisin[j];

  cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();
  
  cplx *PPP_red = P_red.Tab_A_loc[f].get_Aloc();
  
  if (arg == "pj"){ 
    
    for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
      
      for (jloc = 0; jloc < MSph.elements[f].Nred; jloc++) { // numero de la colonne	  	
	a[jloc * MSph.ndof + iloc] = PPP_red[jloc * MSph.ndof + iloc];
       
      }      
    }
  }
  
  else if (arg == "pi"){

    for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
     
      for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	      
	a[iloc * MSph.elements[e].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
   
      }      
    }
  }

  else {
    cout<<"probleme de mot clef"<<endl;
  }
}






void transform_hetero_analytique_Pressure_and_Velocity_Solution_to_VTK(MailleSphere &MSph){
  	int nd = MSph.get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * MSph.get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41P;
	FILE* fic41V;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	cplx P;
	cplx *V = new cplx[3];
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_V.txt","w");
	fic6 = fopen("paraview_value_P.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < MSph.get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //Pour i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //Pour i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));
		  
		  //Pour i, j+1, k
		  MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));
		  
		  //Pour i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //Pour i+1, j, k+1
		  MSph.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  
		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //Pour i+1, j+1, k+1
		  MSph.elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  
		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //Pour i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;
		  
		  //i, j, k
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j+1, k
		   MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));


		  //i+1, j, k+1
		  MSph.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		    SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  
		  //i, j, k
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j+1, k
		  MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   SolexacteBessel(e, xx, &P, V, MSph);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(V[0]), real(V[1]), real(V[2]));
		  fprintf(fic6, "%g\n",real(P));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}
	delete [] V; V  =  nullptr;
	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41V = fopen("paraview_preambule_fonction_V.txt","w");

	fprintf(fic41V, "</DataArray>\n");
	fprintf(fic41V, "</Cells>\n");
	fprintf(fic41V, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41V, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41V);

	fic41P = fopen("paraview_preambule_fonction_P.txt","w");

	fprintf(fic41P, "</DataArray>\n");
	fprintf(fic41P, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41P);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);

	
	system("more paraview_preambule.txt > paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_points.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_type.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_offset.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_preambule_fonction_V.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_value_V.txt >> paraview_hetero_Bessel_P_and_V.vtu   ");
	system("more paraview_preambule_fonction_P.txt >> paraview_hetero_Bessel_P_and_V.vtu");
	system("more paraview_value_P.txt >> paraview_hetero_Bessel_P_and_V.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_hetero_Bessel_P_and_V.vtu");
}



void MailleSphere::transform_hetero_analytique_Scalar_Solution_to_VTK(){

	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;

		  Bessel Be;
		  
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		   Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  Bessel Be;
		  
		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	     
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		   Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {

		  Bessel Be;
		  
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		 Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		   Be.compute_P_and_V(xx, X0,  c1,  c2,  c3,  r1,  a1,  a2,  k1,  k2,  xi1,  xi2, elements[e].rad);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",imag(Be.Pc));

		    
		  delete [] Be.V; Be.V  =  nullptr;
		  delete [] Be.d; Be.d  =  nullptr;
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_points.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_type.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_offset.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
	system("more paraview_value.txt >> paraview_hetero_analytique_scalar_Bessel.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_hetero_analytique_scalar_Bessel.vtu");
}
/************************************************************************************* PARTIE REPRESENTATION DE LA PROJECTION DE LA SOLUTION ANALYTIQUE ********************************************/

void MailleSphere::transform_hetero_analytique_ProjectionOfScalarSolution_to_VTK(VecN &ALPHA){

	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  
		  //Pour i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));
		  
		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  
		  //WEDGE
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	       
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;
		 
		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
	     
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {

		  
		  Nelt++;

		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		 
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

	       
		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));


		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(elements[e].Pexacte));

		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_points.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_type.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_offset.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
	system("more paraview_value.txt >> paraview_hetero_Projec_scalar_Bessel.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_hetero_Projec_scalar_Bessel.vtu");
}




/*-------------------------------------------------------------------------------------- VELOCITE ----------------------------------------------------------------------------------*/



void MailleSphere::transform_hetero_analytique_ProjectionOfPressureAndVelocitySolution_to_VTK(VecN &ALPHA){
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic41V;
	FILE*  fic41P;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_V.txt","w");
	fic6 = fopen("paraview_value_P.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;
		  
		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  
		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;
		  
		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41V = fopen("paraview_preambule_fonction_V.txt","w");

	fprintf(fic41V, "</DataArray>\n");
	fprintf(fic41V, "</Cells>\n");
	fprintf(fic41V, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41V, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41V);

	fic41P = fopen("paraview_preambule_fonction_P.txt","w");
	
	fprintf(fic41P, "</DataArray>\n");
	fprintf(fic41P, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41P);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_points.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_type.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_offset.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_preambule_fonction_V.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_value_V.txt >> paraview_hetero_ProBessel_P_and_V.vtu   ");

	system("more paraview_preambule_fonction_P.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
	system("more paraview_value_P.txt >> paraview_hetero_ProBessel_P_and_V.vtu   ");
	
	system("more paraview_preambule_finale.txt >> paraview_hetero_ProBessel_P_and_V.vtu");
}

/*------------------------------------------------------------------------------------- ERREUR PRESSION --------------------------------------------------------*/


void transform_hetero_ScalarAndVelocityError_to_VTK(VecN &ALPHA, MailleSphere &MSph){
  
  	int nd = MSph.get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * MSph.get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic41V;
	FILE*  fic41P;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	cplx P;
	cplx *V = new cplx[3];
	cplx *W = new cplx[3];
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_V.txt","w");
	fic6 = fopen("paraview_value_P.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < MSph.get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;
		  
		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //Pour i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));
		  
		  //Pour i, j+1, k
		  MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //Pour i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));
		  
		  //Pour i+1, j, k+1
		  MSph.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  MSph.elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //Pour i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;
		  
		  //i, j, k
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j+1, k
		   MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  
		  //i, j+1, k+1
		  MSph.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));


		  //i+1, j+1, k
		  MSph.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));


		  //i+1, j, k+1
		  MSph.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;
		  
		  //i, j, k
		  MSph.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i+1, j, k
		  MSph.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j+1, k
		  MSph.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));

		  //i, j, k+1
		  MSph.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //calcul de la solution analytique
		  SolexacteBessel(e, xx, &P, V, MSph);
		  
		  //calcul de la solution par projection
		  MSph.elements[e].combinaison_solution(MSph.ndof, e, ALPHA, xx);

		  W[0] = V[0] - MSph.elements[e].Vexacte[0];
		  W[1] = V[1] - MSph.elements[e].Vexacte[1];
		  W[2] = V[2] - MSph.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));
		  fprintf(fic6, "%g\n",real(P - MSph.elements[e].Pexacte));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	delete [] V; V  =  nullptr;
	delete [] W; W  =  nullptr;
	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41V = fopen("paraview_preambule_fonction_V.txt","w");

	fprintf(fic41V, "</DataArray>\n");
	fprintf(fic41V, "</Cells>\n");
	fprintf(fic41V, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41V, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41V);

	fic41P = fopen("paraview_preambule_fonction_P.txt","w");
	
	fprintf(fic41P, "</DataArray>\n");
	fprintf(fic41P, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41P);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_points.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_type.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_offset.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_preambule_fonction_V.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_value_V.txt >> paraview_hetero_BesselError_P_and_V.vtu   ");

	system("more paraview_preambule_fonction_P.txt >> paraview_hetero_BesselError_P_and_V.vtu");
	system("more paraview_value_P.txt >> paraview_hetero_BesselError_P_and_V.vtu   ");
	
	system("more paraview_preambule_finale.txt >> paraview_hetero_BesselError_P_and_V.vtu");
}
/*---------------------------------------------------------------------------------------- POUR LE GRAND SYSTEME LINEAIRE --------------------------------------------------------------*/

void MailleSphere::transform_hetero_numerical_PressureAndVelocitySolution_to_VTK(VecN &ALPHA){
  	int nd = get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * get_N_elements();
	MKL_INT te;
	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE*  fic6;
	FILE*  fic41V;
	FILE*  fic41P;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value_V.txt","w");
	fic6 = fopen("paraview_value_P.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  //Pour i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  //Pour i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i+1, j+1, k+1
		  elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //Pour i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;
		  
		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		   elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		   elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  
		  //i, j+1, k+1
		  elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  //i+1, j+1, k
		  elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  //i+1, j, k+1
		  elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));


		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;
		  
		  //i, j, k
		  elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i+1, j, k
		  elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j+1, k
		  elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));

		  //i, j, k+1
		  elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		  elements[e].combinaison_solution(ndof, e, ALPHA, xx);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(elements[e].Vexacte[0]), real(elements[e].Vexacte[1]), real(elements[e].Vexacte[2]));
		  fprintf(fic6, "%g\n",real(elements[e].Pexacte));
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic6);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41V = fopen("paraview_preambule_fonction_V.txt","w");

	fprintf(fic41V, "</DataArray>\n");
	fprintf(fic41V, "</Cells>\n");
	fprintf(fic41V, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41V, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41V);

	fic41P = fopen("paraview_preambule_fonction_P.txt","w");
	
	fprintf(fic41P, "</DataArray>\n");
	fprintf(fic41P, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41P);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_points.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_tetra.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_preambule_type.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_type.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_preambule_offset.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_offset.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_preambule_fonction_V.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_value_V.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu   ");

	system("more paraview_preambule_fonction_P.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
	system("more paraview_value_P.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu   ");
	
	system("more paraview_preambule_finale.txt >> paraview_hetero_numericalBessel_P_and_V_P_and_V.vtu");
}


void collection_Solutions_locales_Reduites_selfDecompose(VecN &U, const string & arg, const string & choice, A_skyline &P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales réduites dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant la matrice de la projection sur chaque élément correspondant.

   /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    arg     : chaîne de caractères pour définir le cas pri en charge. 
              arg = "bessel" pour du Bessel et arg = "plane" pour une onde plane
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales réduites
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = MSph.ndof;
  MP.n = MSph.ndof;

  VecN u;
  u.n = MSph.ndof;
  
  MSph.taille_red = 0;
  
  for(e = 0; e < MSph.get_N_elements(); e++){
    
    MP.Allocate_MatMN();

    u.Allocate();
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(MSph, e, MP.value);
    
    //Construction du vecteur second membre correspondant à l'élément e
    BuildVector_projection(e, u.value, arg, MSph);    

    cplx *AA = new cplx[MSph.ndof * MSph.ndof];
    
    double *w = new double[MSph.ndof];

    //calcule de la taille du block réduit
    compute_sizeBlocRed(e, AA, w, MP.value, MSph);
    
    MSph.SIZE[e] = MSph.taille_red;
    
    MSph.taille_red += MSph.elements[e].Nred_Pro;
    
    cplx *V = new cplx[MSph.elements[e].Nred_Pro];
    
    cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();

    //Décomposition de la matrice MP et stockage de PP_red et des valeurs propres
    DecompositionDeBloc(e, MP.value, PP_red, V, MSph);
    
    
    MatMN MP_red;
    MP_red.m = MSph.elements[e].Nred_Pro;
    MP_red.n = MSph.elements[e].Nred_Pro;
    
    MP_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = MSph.elements[e].Nred_Pro;
    u_red.Allocate();

    //Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red 
    compute_A_red(e, choice, MP, PP_red, MP_red, MSph);

    //u_red = conj(trans(PP_red)) * B
    compute_F_red(e, choice, u.value, PP_red, u_red.value, MSph);

    //résolution du système linéaire MP_red x_red = u_red correspondante par la méthode LU
    MP_red.Solve_by_LU(u_red);

    //on clône l'emplacement U correspondant à l'élément e
    get_F_elemRed(e, &B, U, MSph) ;

    for( iloc = 0; iloc < MSph.elements[e].Nred_Pro; iloc++) {
      B[iloc] = u_red.value[iloc];
    }
    
    delete [] MP.value ; MP.value  =  nullptr;
    delete [] u.value; u.value  =  nullptr;
    delete [] AA ; AA  =  nullptr;
    delete [] w ; w  =  nullptr;
    delete [] V; V  =  nullptr;
    delete [] MP_red.value; MP_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }
  
}





/*---------------------------------------------------------------------------------------- PARTIE CONSISTANCE POUR LE MAILLAGE A DOUBLE SPHERES ---------------------------------------------------*/

/*---------------------------------------------------------------------------------- P_T P'_T et P_T P'_K INTERACTION INTERIEUR ---------------------------------------------------------*/

cplx int_P_TxP_T_interieure_On_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   f        : numero de la face
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  if (MSph.elements[e].voisin[f] >= 0){
    I = integrale_triangle(MSph.elements[e].face[f], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
  }
 
  return I;
}



cplx int_P_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
  
  for (l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] >= 0){
      I += integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
 
  return I;
}



cplx int_P_TxP_K_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]) {
   //Calcul de l'intégrale de P_T * conj(P'_K) sur les faces interieures de l'élément n° e avec K le triangle voisin à l'élément e et T le triangle associé à l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : numero local d'un voisin de l'element e
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT h;                                              
  if (MSph.elements[e].voisin[c] >= 0){
      
    h = MSph.elements[e].voisin[c];

    I =  -integrale_triangle(MSph.elements[e].face[c], MSph.elements[h].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_P_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]) {
   //Calcul de l'intégrale de P_K * conj(P'_T) sur les faces interieures de l'élément n° e avec P_K l'onde associée au voisin de l'élément e et P'_T celle associée à l'élément e
 /*------------------------------------------- input ----------------------------------------------
   alpha    : coefficient associée à cette intégrale
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : numero local d'un voisin de l'element e
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
 
  MKL_INT h, s;                                              
  if (MSph.elements[e].voisin[c] >= 0){
    h = MSph.elements[e].voisin[c];
    I = - integrale_triangle(MSph.elements[e].face[c], MSph.elements[e].OP[iloc], MSph.elements[h].OP[jloc], chaine);
    
  }
  
  return I;
}



/*----------------------------------------------------------------- BLOC P_T P_T ------------------------------------------*/

void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxP_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxP_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] = int_P_TxP_T_interieure_On_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



/*----------------------------------------------------------------- BLOC P_T P_K ------------------------------------------*/

void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxP_K_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_TxP_K(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxP_K_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


/*----------------------------------------------------------------- BLOC P_K P_T ------------------------------------------*/

void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_KxP_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_KxP_T_interieure(iloc, jloc, i, c, MSph, chaine);
     
    }
  }
}



void BlocIntPP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [P][P'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
  Build_BlocInt_P_TxP_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  

    /*----------------------------------------- Interaction element - voisin P_K x P_T --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (MSph.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_P_KxP_T(i, j, AA, MSph, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA = 0;
}


void BlocIntPP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [P][P'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
    Build_BlocInt_P_TxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);
    
    
    /*----------------------------------------- Interaction element - voisin P_K x P_T --------------------------------*/
    
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_P_KxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}



void BlocIntPP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    BlocIntPP(i, MSph.tab_PP[0], MSph.tab_PP[0], A_glob, MSph, chaine);
  }
}


// /*----------------------------------------------------------------- P P' --------------------------------------------------------*/



cplx int_P_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT f, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur une face exterieure de l'élément e
 /*------------------------------------------- input ----------------------------------------------
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   f        : numero d'une face f
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
                                               
  if (MSph.elements[e].voisin[f] < 0){
    I = integrale_triangle(MSph.elements[e].face[f], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
  }
  
  return I;
}



cplx int_P_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur une face exterieure de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    alpha    : coefficient associée à cette intégrale
    iloc     : numero de l'onde pour la fonction-test
    jloc     : ......................... solution
    e     : numero de l'élément
    MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  MKL_INT l;
  for(l = 0; l < 4; l++) {                                            
    if (MSph.elements[e].voisin[l] < 0){
      I += int_P_TxP_T_exterieure_1_face(iloc, jloc, e, l, MSph, chaine);
    }
  }
  return I;
}



void Build_BlocExt_P_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxP_T_exterieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocExt_P_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la bloc P_T * P_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    MSph : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] = int_P_TxP_T_exterieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}


void BlocExtPP_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice P P' associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_P_TxP_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}


void BlocExtPP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute au tableau A_skyline A_glob, la matrice P P' associée à l'élément i et à la face de bord l.
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face de i
    alpha     : coeff de la matrice. Elle est associée à i
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i, 0) dans A_skyline
  if (MSph.elements[i].voisin[l] < 0 ){
    
    Build_BlocExt_P_TxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);

  }
  
  delete [] AA; AA  =  nullptr;
}



void BlocExtPP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    BlocExtPP_Add(i, MSph.tab_PP[3], A_glob, MSph, chaine);
  }
}


void Build_A5(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Calcul la matrice globale P P' sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice P P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {//boucle sur les éléments
    for(l = 0; l < 4; l++) {//boucle sur les faces
      if (MSph.elements[i].voisin[l] < 0) {
	BlocExtPP_Add(i, l, MSph.elements[i].T_PP[l], A_glob, MSph, chaine);
      }
    }
  }
}


// /*----------------------------------------------------------------- [V] [V'] ---------------------------------------------------------*/


cplx int_V_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   l     : numero local 'une face
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
  if (MSph.elements[e].voisin[l] >= 0){ 
    
    if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    
    I += din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
  }
  
  
  return I;
}



cplx int_V_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] >= 0){
      I += din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}


cplx int_V_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Kn_K * conj(V'_K)n_K sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------

   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : ...... local du voisin de l'élément e
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);

  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (MSph.elements[e].voisin[c] >= 0) {

    MKL_INT h = MSph.elements[e].voisin[c];

     if (c == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (c == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }

    
     I = - din * djn * integrale_triangle(MSph.elements[e].face[c], MSph.elements[e].OP[iloc], MSph.elements[h].OP[jloc], chaine);
  }
  
  return I;
}


/*------------------------------------------------------------------------ V V' ---------------------------------------------------------*/

cplx int_V_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur une face exterieure de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (MSph.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    I = din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_V_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] < 0){
      I += din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}


void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_TxV_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocInt_V_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxV_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}



void Build_BlocInt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxV_T_interieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_KxV_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


void Build_BlocInt_V_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_KxV_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}



void BlocIntVV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [V][V'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
  Build_BlocInt_V_TxV_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  

    /*----------------------------------------- Interaction element - voisin V_T x V_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (MSph.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_V_KxV_T(i, j, AA, MSph, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA  =  nullptr;
}



void BlocIntVV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [V][V'] associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale correspondante à j = 0 dans A_skyline
    Build_BlocInt_V_TxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);
    
    
    /*----------------------------------------- Interaction element - voisin V_K x V_T --------------------------------*/
    
    
    
    //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
    Build_BlocInt_V_KxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}



void Build_BlocExt_V_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxV_T_exterieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocExt_V_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
   //Cette fonction calcule la bloc V_Tn_T * V'_Tn_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    MSph : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxV_T_exterieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



void BlocExtVV_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute dans le tableau A_skyline A_glob, la matrice V n V' n associée à l'élément i et à la face de bord l. 
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_V_TxV_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}


void BlocExtVV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice V_T n_T V'_T n_T associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i,0) dans A_skyline
  if (MSph.elements[i].voisin[l] < 0) {
    
    Build_BlocExt_V_TxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
  }
  
  delete [] AA; AA  =  nullptr;
}


void BlocExtVV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
   //Calcul la matrice globale V V' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    BlocExtVV_Add(i, MSph.tab_VV[3], A_glob, MSph, chaine);
  }
}


void Build_A8(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
   //Calcul la matrice globale V n V' n sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V V'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      if (MSph.elements[i].voisin[l]) {
	BlocExtVV_Add(i, l, MSph.elements[i].T_VV[l], A_glob, MSph, chaine);
      }
    }
  }
}


void BlocIntVV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    BlocIntVV(i, MSph.tab_VV[0], MSph.tab_VV[0], A_glob, MSph, chaine);
  }
}


/*--------------------------------------------------------------------------- [V] {P} ---------------------------------------------*/


cplx int_V_TxP_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (MSph.elements[e].voisin[l] >= 0){

     if (l == 0) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    
     I += djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  
  return I;
}



cplx int_V_TxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] >= 0){
      I += djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



cplx int_V_KxP_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_K) sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   c        : numero local du voisin
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT h;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (MSph.elements[e].voisin[c] >= 0) {

    h = MSph.elements[e].voisin[c];

     if (c == 0) {
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;
      
    }
    else if (c == 1) {
     
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;
      
    }
    else if (c == 2) {
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;
     
    }
    else if (c == 3) {
      
      djn = cblas_ddot(3, MSph.elements[h].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[h].OP[jloc].ksurOmega;      
   
    }
     
     
     I =  - djn * integrale_triangle(MSph.elements[e].face[c], MSph.elements[e].OP[iloc], MSph.elements[h].OP[jloc], chaine);
      
  }
  
  return I;
}


void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_TxP_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocInt_V_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxP_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}



void Build_BlocInt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph,  const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxP_T_interieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}




void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_V_KxP_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


void Build_BlocInt_V_KxP_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_KxP_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}




void BlocIntVP(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [V]{P'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale sur les faces interieures. Elle correspond à j = 0 dans A_skyline

    //interaction element-element
    Build_BlocInt_V_TxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);

    
    /*----------------------------------------- Interaction element - voisin V_T x P_K --------------------------------*/
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_V_KxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);

  }
  
  delete [] AA; AA  =  nullptr;
}



void BlocIntVP(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [V]{P'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/
  
  //construction de la matrice bloc diagonale sur les faces interieures. Elle correspond à j = 0 dans A_skyline
  Build_BlocInt_V_TxP_T(i, AA, MSph, chaine);
  
  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
  
  
  /*----------------------------------------- Interaction element - voisin V_T x P_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (MSph.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_V_KxP_T(i, j, AA, MSph, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
    }
  }
  delete [] AA; AA  =  nullptr;
}



void BlocIntVP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    
    BlocIntVP(i, MSph.tab_VP[0], MSph.tab_VP[0], A_glob, MSph, chaine);
  }
}



/*----------------------------------------------------------------------- V P -----------------------------------------------------*/


cplx int_V_TxP_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
 
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  if (MSph.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;      
      
    }
    
    
    
    I = djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}



cplx int_V_TxP_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(P'_T) sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[jloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] < 0){
      I += djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}




void Build_BlocExt_V_TxP_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxP_T_exterieure(iloc, jloc, i, MSph, chaine);
    }
  }
}



void Build_BlocExt_V_TxP_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la bloc V_T * P'_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    MSph : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_V_TxP_T_exterieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



void BlocExtVP_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [V]{P'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures. Elle correspond à j = 0 dans A_skyline
  Build_BlocExt_V_TxP_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}


void BlocExtVP_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute dans le tableau A_skyline A_glob, la matrice V_Tn_T P'_T associée à l'élément i et à la face de bord l.
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x P_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures. Elle correspond à l'emplacement (i,0) dans A_skyline

  if (MSph.elements[i].voisin[l] < 0 ){
    
    Build_BlocExt_V_TxP_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);

  }
  delete [] AA; AA  =  nullptr;
}



void BlocExtVP(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    
    BlocExtVP_Add(i, MSph.tab_VP[3], A_glob, MSph, chaine);
  }
}


void Build_A7(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
   //Calcul la matrice globale V n P' sur toutes les faces exterieures
   /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                                   A_glob, la matrice V n P'
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (l = 0; l < 4; l++) {
      if (MSph.elements[i].voisin[l] < 0) {
	BlocExtVP_Add(i, l, MSph.elements[i].T_VP[l], A_glob, MSph, chaine);
      }
    }
  }
}


/*-------------------------------------------------------------------- [P] {V} -------------------------------------------------*/


cplx int_P_TxV_T_interieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e        : numero de l'élément
   l        : numero local d'une face 
   MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  if (MSph.elements[e].voisin[l] >= 0){
    if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    I += din * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
  }
  
  return I;
}



cplx int_P_TxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
       
    if (MSph.elements[e].voisin[l] >= 0){
      I += din * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



cplx int_P_KxV_T_interieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT c, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_K * conj(V'_T)n_T sur les faces interieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------

   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   c     : ...... local du voisin de l'élément e
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);

  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  if (MSph.elements[e].voisin[c] >= 0) {

    MKL_INT h = MSph.elements[e].voisin[c];

     if (c == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (c == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (c == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
     I = - din * integrale_triangle(MSph.elements[e].face[c], MSph.elements[e].OP[iloc], MSph.elements[h].OP[jloc], chaine);
  }
  
  return I;
}


void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT j, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_TxV_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxV_T_interieure(iloc, jloc, i, MSph, chaine);
    }
  }
}



void Build_BlocInt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxV_T_interieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT j, MKL_INT c, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  cplx *AA;
  MKL_INT iloc, jloc;
  AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
  for (iloc = 0; iloc < A_glob.get_n_dof(); iloc++) { // numero de la ligne
    for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++) { // numero de la colonne
      AA[iloc * A_glob.get_n_dof() + jloc] +=  int_P_KxV_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}


void Build_BlocInt_P_KxV_T(MKL_INT i, MKL_INT c, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_KxV_T_interieure(iloc, jloc, i, c, MSph, chaine);
    }
  }
}



void BlocIntPV(MKL_INT i, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [P]{V'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces interieures correspondante à j = 0 dans A_skyline
  Build_BlocInt_P_TxV_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha1, AA);
 

    /*----------------------------------------- Interaction element - voisin P_T x V_K --------------------------------*/
  for(MKL_INT j = 0; j < 4; j++) {
    if (MSph.elements[i].voisin[j] >= 0){
      
      //construction de la matrice d'interaction element-voisin j > 0 pour A_skyline
      Build_BlocInt_P_KxV_T(i, j, AA, MSph, chaine);
      
      //Ajout de la matrice dans A_glob
      A_glob.Add_Block(i, j+1, alpha2, AA);
      
    }
  }
  delete [] AA; AA  =  nullptr;
}



void BlocIntPV(MKL_INT i, MKL_INT l, double alpha1, double alpha2, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule la matrice [P]{V'} associée à l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  if (MSph.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale pour les faces interieures correspondante à j = 0 dans A_skyline

    //interaction élément-élément
    Build_BlocInt_P_TxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha1, AA);

    
    /*----------------------------------------- Interaction element - voisin P_T x V_K --------------------------------*/
    
    
    //construction de la matrice d'interaction element-voisin l > 0 pour A_skyline
    Build_BlocInt_P_KxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob
    A_glob.Add_Block(i, l+1, alpha2, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}




void BlocIntPV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {
    
    BlocIntPV(i, MSph.tab_PV[0], MSph.tab_PV[0], A_glob, MSph, chaine);

  }
}


/*-------------------------------------------------------------------- P V --------------------------------------*/

cplx int_P_TxV_T_exterieure_1_face(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MKL_INT l, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    
    iloc     : numero de l'onde pour la fonction-test
    jloc     : ......................... solution
    e        : numero de l'élément
    l        : numero local d'une face
    MSph     : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  

  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  if (MSph.elements[e].voisin[l] < 0){
    
    if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
      
    }
    
    
    
    I = din * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    
  }
  
  return I;
}


cplx int_P_TxV_T_exterieure(MKL_INT iloc, MKL_INT jloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   jloc     : ......................... solution
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] < 0){
      I += din * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
    }
  }
  
  return I;
}



void Build_BlocExt_P_TxV_T(MKL_INT i, cplx *AA, MailleSphere &MSph, const char chaine[]) {
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxV_T_exterieure(iloc, jloc, i, MSph, chaine);
    }
  }
}



void Build_BlocExt_P_TxV_T(MKL_INT i, MKL_INT l, cplx *AA, MailleSphere &MSph, const char chaine[]) {
   //Cette fonction calcule la bloc P_T * V'_T n_T associé à i, le numero d'un élément et et à l, numero local d'une face
  /*---------------------------------------------------------------------------------- input --------------------------------------------------------------------------------
    i    : numero global d'un élément
    l    : numero local d'une face
    AA   : tableau ou est stocké la matrice
    MSph : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------------------------------------------
                                        AA qui contiendra la matrice
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/ 
  MKL_INT iloc, jloc;
 
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
      AA[iloc * MSph.ndof + jloc] =  int_P_TxV_T_exterieure_1_face(iloc, jloc, i, l, MSph, chaine);
    }
  }
}



void BlocExtPV_Add(MKL_INT i, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [P]{V'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à j = 0 dans A_skyline
  Build_BlocExt_P_TxV_T(i, AA, MSph, chaine);

  //Ajout de la matrice dans A_glob pour j = 0
  A_glob.Add_Block(i, 0, alpha, AA);
 
  delete [] AA; AA  =  nullptr;
}



void BlocExtPV_Add(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Cette fonction calcule et ajoute la matrice [P]{V'} associée à l'élément i au bord au tableau A_skyline A_glob
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face 
    A_glob    : tableau skyline des valeurs de la matrive
    MSph      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x V_T -------------------------------*/

  //construction de la matrice bloc diagonale pour les faces exterieures correspondante à l'emplacement (i,0) dans A_skyline

  if (MSph.elements[i].voisin[l] < 0) {
    
    Build_BlocExt_P_TxV_T(i, l, AA, MSph, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_A6(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Calcul la matrice globale P V'n sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                       A_glob, la matrice P V'n
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, l;
  for (i = 0; i < MSph.get_N_elements(); i++) {//boucle sur les éléments
    for (l = 0; l < 4; l++) {//boucle sur les faces
      
      BlocExtPV_Add(i, l, MSph.elements[i].T_PV[l], A_glob, MSph, chaine);
    }
  }
}


void BlocExtPV(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  //Calcul la matrice globale P V'n sur toutes les faces exterieures
  /*---------------------------------------------------------------------------------- input -----------------------------------------------------------------------------------
    A_glob      : tableau skyline
    MSph        : le maillage
    /*-------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                                       A_glob, la matrice P V'n
    /*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT i = 0; i < MSph.get_N_elements(); i++) {

    BlocExtPV_Add(i, MSph.tab_PV[3], A_glob, MSph, chaine);
  }
}



void Build_A1(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntPP(i, j, MSph.elements[i].T_PP[j], MSph.elements[i].T_PP[j], A_glob, MSph, chaine);
    }
  }
}


void Build_A4(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntVV(i, j, MSph.elements[i].T_VV[j], MSph.elements[i].T_VV[j], A_glob, MSph, chaine);
    }
  }
}


void Build_A2(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntPV(i, j, MSph.elements[i].T_PV[j], MSph.elements[i].T_PV[j], A_glob, MSph, chaine);
    }
  }
}


void Build_A3(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntVP(i, j, MSph.elements[i].T_VP[j], MSph.elements[i].T_VP[j], A_glob, MSph, chaine);
    }
  }
}




/*--------------------------------------------------------------------- Pinc P --------------------------------------------------------*/

cplx int_PincP_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de P_T * conj(P'_T) sur une face exterieure de l'élément e
  /*------------------------------------------- input ----------------------------------------------
    iloc     : numero de l'onde pour la fonction-test
    e     : numero de l'élément
    MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  for(l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] < 0){
      I += integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[MSph.n_i], chaine);
    }
  }
  return I;
}



void Build_VectorPincP_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) { 
      iglob = local_to_glob(iloc, i, MSph.ndof);
      B[iglob] =  alpha * int_PincP_T(iloc, i, MSph, chaine);
    }
  }
}


/*----------------------------------------------------------------------- Pinc V ---------------------------------------------------*/

cplx int_PincV_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de Pinc * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] < 0){
      I += din * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[MSph.n_i], chaine);
    }
  }
  
  return I;
}



void Build_VectorPincV_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]) {
  //cette fonction calcule le vecteur global Pinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) { 
      iglob = local_to_glob(iloc, i, MSph.ndof);
      B[iglob] =  alpha * int_PincV_T(iloc, i, MSph, chaine);
    }
  }
}


/*---------------------------------------------------------------------- Vinc P ----------------------------------------------------*/

cplx int_VincP_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de Vinc * conj(P') sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double  djn;
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;
      
    }
    else if (l == 1) {
     
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;
      
    }
    else if (l == 2) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;
     
    }
    else if (l == 3) {
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;      
   
    }
    
    
    if (MSph.elements[e].voisin[l] < 0){
      I +=  djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[MSph.n_i], chaine);
    }
  }
  
  return I;
}



void Build_VectorVincP_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * P et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] =  alpha * int_VincP_T(iloc, i, MSph, chaine);
    }
  }
}



/*---------------------------------------------------------------- Vinc V ---------------------------------------------------------*/

cplx int_VincV_T(MKL_INT iloc, MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  //Calcul de l'intégrale de V_Tn_T * conj(V'_T)n_T sur les faces exterieures de l'élément e
 /*------------------------------------------- input ----------------------------------------------
 
   iloc     : numero de l'onde pour la fonction-test
   e     : numero de l'élément
   MSph  : structure maillage
    /*-------------------------------------------- ouput ---------------------------------------------
    I = le resultat
    /*-------------------------------------------------------------------------------------------------*/
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  double din, djn;
  //din = A*diloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et diloc la direction de l'onde iloc
  //djn = A*djloc\cdot n avec n la normale à la face f, A la matrice associée à l'équation de Helmholtz sur l'élément e et djloc la direction de l'onde jloc
   
  for (l = 0; l < 4; l++) {

     if (l == 0) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n1, 1) ;

      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 1) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n2, 1) ;

      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
      
    }
    else if (l == 2) {
       din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n3, 1) ;

      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;
     
    }
    else if (l == 3) {
      din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
      
      djn = cblas_ddot(3, MSph.elements[e].OP[MSph.n_i].Ad, 1, MSph.elements[e].n4, 1) ;

      djn = djn * MSph.elements[e].OP[MSph.n_i].ksurOmega;

      din = din * MSph.elements[e].OP[iloc].ksurOmega;      
   
    }
      
    if (MSph.elements[e].voisin[l] < 0){
      I +=  din * djn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[MSph.n_i], chaine);
    }
  }
  
  return I;
}



void Build_VectorVincV_T(double alpha, cplx *B, MailleSphere &MSph, const char chaine[]) {
  //cette fonction calcule le vecteur global Vinc * V et la stocke dans le tableau B
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    alpha   : scalaire
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] =  alpha * int_VincV_T(iloc, i, MSph, chaine);
    }
  }
}



/***********************************************************************************************************************************************************************/
/********************************************************************* MAILLAGE A DEUX PAVES ***********************************************************************/
/*************************************************************************************************************************************************************************/


void choisir_A_et_xi(Element &Elem, double x_I, double a1, double a2, double b1, double b2, double xi1, double xi2) {
  //Cette fonction permet de définir les coeffs A et xi de l'equation Helmholtz sur un élément
  /*----------------------------------------------------------------------------------------
    Elem       : classe d'un élément
    a1         : coefficient de la diagonale de A dans le domaine x < 0
    a2         : .............................. A ............... x > 0
    xi1        : coeff de Helmholtz dans le domaine x < 0,
    xi2        : .................................. x > 0
    ---------------------------------------------------------------------------------------*/
  if (Elem.XG[0] < x_I) {
    Elem.set_ref(1);
    Elem.xi = xi1;
    Elem.Ac.define(a1,b1,b1,
		   b1,a1,b1,
		   b1,b1,a1);
    
  }
  else {
    Elem.xi = xi2;
    Elem.set_ref(2);
    Elem.Ac.define(a2,b2,b2,
		   b2,a2,b2,
		   b2,b2,a2);
   
    }
 
}


void Maillage::compute_R_and_T(){
  //Cette fonction calcule les coeffs R et T
  double *n0 = new double[3];//la normale à l'incidente, n = ex
  
  n0[0] = 1.0; n0[1] = 0.0; n0[2] = 0.0;


  double w1 = omega * sqrt(xi1 / a1);
  double w2 = omega * sqrt(xi2 / a2);
  cout<<"w1 = "<<w1<<" w2 = "<<w2<<endl;

  MKL_INT j;
  
  for( j = 0; j < 3; j++) {
    d1p[j] = Dir[3*n_i + j];
    d1n[j] = Dir[3*n_r + j];
    k1p[j] = d1p[j] * w1;
    k1n[j] = d1n[j] * w1;
    k2p[j] = d1p[j] * w2;
  }
  

  //Y1^+ = (a_1 / omega) * n\codt K1^+
  Y1p = cblas_ddot(3, n0, 1, k1p, 1) ;//Y1p = Y1^+

  Y1p = a1 * Y1p * ww;//ici ww = 1.0 / omega

  //Y1^- = (a_1 / omega) * n\codt K1^-
  Y1n = cblas_ddot(3, n0, 1, k1n, 1) ;//Y1n = Y1^-

  Y1n = a1 * Y1n * ww;

  //Y2^+ = (a_2 / omega) * n\codt K2^+
  Y2p = cblas_ddot(3, n0, 1, k1p, 1) ;//Y2p = Y2^+

  Y2p = a2 * Y2p * ww;

  //R = - (Y2^+ - Y1^+) / (Y2^+ + Y1^+)
  R = - ((Y2p - Y1p) / (Y2p + Y1p)) * exp(cplx(0.0,2 * k1p[0] * x_I));

  T = 1.0 + R * exp(cplx(0.0,2 * k1n[0] * x_I));


  delete [] n0; n0  =  nullptr;
  
  
}


void Maillage::CHECK_SOLUTION(double *X){
  cplx TT, RR, Pref, Pt, Pinc;
  cplx Vref[3], Vt[3], Vs[3], Vinc[3];
  TT = T * exp(cplx(0.0,k1p[0]*x_I));
  RR = exp(cplx(0.0,k1p[0]*x_I)) + R * exp(cplx(0.0,-k1p[0]*x_I));

  calcul_P_and_V_incidente(X, &Pinc, Vinc, *this);

  calcul_P_and_V_transmise(X, &Pt, Vt, *this);

  calcul_P_and_V_reflechie(X, &Pref, Vref, *this);

  for(MKL_INT i = 0; i < 3; i++) {
    Vs[i] = Vinc[i] + R * Vref[i];
  }
  
  cout<<"T*exp() = "<<TT<<" exp() + R * exp() = "<<RR<<endl;
  cout<<"abs(T*exp()) = "<<abs(TT)<<" abs(exp() + R * exp()) = "<<abs(RR)<<endl;

  cout<<" Pinc + R*P_ref = "<<Pinc + R * Pref<<" T * Pt = "<<T * Pt<<endl;
  cout<<" Vinc + R * Vref = ["<<Vs[0]<<","<<Vs[1]<<","<<Vs[2]<<"]"<<endl;
  cout<<" T * Vt = ["<<T*Vt[0]<<","<<T*Vt[1]<<","<<T*Vt[2]<<"]"<<endl;
  
}



void Maillage::CHECK_PARAMETERS() {

  cout<<"x_I = "<< x_I<<" n_i = "<< n_i<<" n_r = "<< n_r<<endl;
  cout<<" R = "<< R<<" T = "<< T<<endl;
  
  cout<<"kinc = ["<< k1p[0]<<" "<< k1p[1]<<" "<< k1p[2]<<"]"<<endl;

  cout<<"kt = ["<< k2p[0]<<" "<< k2p[1]<<" "<< k2p[2]<<"]"<<endl;
  
  cout<<"kr = ["<< k1n[0]<<" "<< k1n[1]<<" "<<k1n[2]<<"]"<<endl;

   cout<<"d1p = ["<< d1p[0]<<" "<< d1p[1]<<" "<< d1p[2]<<"]"<<endl;
  
  cout<<"d1n = ["<< d1n[0]<<" "<< d1n[1]<<" "<<d1n[2]<<"]"<<endl;

  cout<<" Y1p = "<<Y1p<<" Y1n = "<<Y1n<<" Y2p = "<<Y2p<<endl;
  
}


void calcul_P_and_V_incidente(double *X, cplx *P, cplx *V, Maillage &Mesh){

  MKL_INT i;
  
  double KX = cblas_ddot(3, Mesh.k1p, 1, X, 1) ;

  *P = exp(cplx(0.0, KX));
  
  for (i = 0; i < 3; i++) {
    V[i] = Mesh.a1 * *P * Mesh.k1p[i] * Mesh.ww;
  }
  
}


void calcul_P_and_V_transmise(double *X, cplx *P, cplx *V, Maillage &Mesh){

  MKL_INT i;
  
  double KX = cblas_ddot(3, Mesh.k1p, 1, X, 1) ;

  *P = exp(cplx(0.0, KX));
  
  for (i = 0; i < 3; i++) {
    V[i] = Mesh.a2 * *P * Mesh.k1p[i] * Mesh.ww;
  }
  
}



void calcul_P_and_V_reflechie(double *X, cplx *P, cplx *V, Maillage &Mesh){

  MKL_INT i;
  
  double KX = cblas_ddot(3, Mesh.k1n, 1, X, 1) ;

  *P = exp(cplx(0.0, KX));
  
  for (i = 0; i < 3; i++) {
    V[i] = Mesh.a1 * *P * Mesh.k1n[i] * Mesh.ww;
  }
  
}

void analytique_P_and_V(double *X, MKL_INT e, cplx *P, cplx *V, Maillage &Mesh) {

  MKL_INT i;
  
  if (Mesh.elements[e].get_ref() == 1) {

    //calcule de la pression incidente Pinc et de la vitesse incidente Vinc
    Mesh.elements[e].OP[Mesh.n_i].compute_P_and_V(X);
    
    //calcule de la pression réfléchie Pr et de la vitesse réfléchie Vr
    Mesh.elements[e].OP[Mesh.n_r].compute_P_and_V(X);
    
    //P = Pinc + R * Pr
    *P = Mesh.elements[e].OP[Mesh.n_i].P + Mesh.R * Mesh.elements[e].OP[Mesh.n_r].P;
    
    //V = Vinc + R * Vr
    for (i = 0; i < 3; i++) {
      V[i] = Mesh.elements[e].OP[Mesh.n_i].V[i] + Mesh.R * Mesh.elements[e].OP[Mesh.n_r].V[i];
    }
    
  }
  
  else {
    
    //calcule de la pression P et de la vitesse transmises V
    Mesh.elements[e].OP[Mesh.n_i].compute_P_and_V(X);
    
    *P = Mesh.T * Mesh.elements[e].OP[Mesh.n_i].P;
    
    for (i = 0; i < 3; i++) {
      V[i] = Mesh.T * Mesh.elements[e].OP[Mesh.n_i].V[i];
    }
    
  }
  
}


/*void analytique_P_and_V(double *X, MKL_INT e, cplx *P, cplx *V, Maillage &Mesh) {

  MKL_INT i;
  
  if (Mesh.elements[e].get_ref() == 1) {

    cplx Pr, Pinc, *Vr, *Vinc;
    
    Vr = new cplx[3];
    Vinc = new cplx[3];

    //calcule de la pression incidente Pinc et de la vitesse incidente Vinc
    calcul_P_and_V_incidente(X, &Pinc, Vinc, Mesh);

    //calcule de la pression réfléchie Pr et de la vitesse réfléchie Vr
    calcul_P_and_V_reflechie(X, &Pr, Vr, Mesh);

    //P = Pinc + R * Pr
    *P = Pinc + Mesh.R * Pr;

    //V = Vinc + R * Vr
    for (i = 0; i < 3; i++) {
      V[i] = Vinc[i] + Mesh.R * Vr[i];
    }

    delete [] Vinc; Vinc  =  nullptr;
    delete [] Vr; Vr  =  nullptr;
  }
  else {

    cplx Pt;
    //calcule de la pression P et de la vitesse transmises V
    calcul_P_and_V_transmise(X, &Pt, V, Mesh);

    *P = Mesh.T * Pt;

    for (i = 0; i < 3; i++) {
      V[i] = Mesh.T * V[i];
    }
    
  }
  
}
*/

cplx quad_tetra_heteroPlane(MKL_INT e, MKL_INT iloc, Maillage &Mesh) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : indice du tetra
    iloc   : numero d'une onde plane
    Mesh   : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  cplx P;
  cplx *V = new cplx[3];
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calul de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);
    
    I = I + Mesh.Qd.Wtetra[i] * conj(Mesh.elements[e].OP[iloc].pression(Xquad)) * P;
  }

  I = I * Mesh.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  
  return I;
}



cplx quad_tetra_heteroPlane_VV(MKL_INT e, MKL_INT iloc, Maillage &Mesh) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : indice du tetra
    iloc   : numero d'une onde plane
    Mesh   : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  cplx P;
  cplx *V = new cplx[3];
  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calul de la solution analytique en Xquad
    analytique_P_and_V(Xquad, e, &P, V, Mesh);

    //calcul de P et V pour l'onde OP[iloc]
    Mesh.elements[e].OP[iloc].compute_P_and_V(Xquad);

    J = dot_c(Mesh.elements[e].OP[iloc].V, V);
    
    I = I + Mesh.Qd.Wtetra[i] * J;
  }

  I = I * Mesh.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  
  return I;
}




void collection_hetero_Solutions_locales(VecN &U, Maillage &Mesh) {

  //Ici, on recolle toutes les solutions locales dans le vecteur U de type VecN
  /*-------------------------------------------------------------------- input ------------------------------------------------
    U    : objet de type VecN, c'est là qu'on stockera notre vecteur global

    arg  : chaine de caractère qui nous permet de déterminer le cas à traiter. 
            - Pour arg = "plane" , on a traitera le cas d'une onde plane incidente
            - Pour arg = "bessel", c'est le cas de la fonction de Bessel de première espéce j0 considèrée comme onde incidente
	    - Pour arg = "hankel", c'est le cas de la fonction de exp(ikr)/4*pi*r considèrée comme onde incidente

    Mesh : le maillage
    /*------------------------------------------------------------------ ouput -------------------------------------------------
    U    : notre vecteur global
    /*--------------------------------------------------------------------------------------------------------------------------*/
  cplx *B;//pointeur pour clôner vers une adresse de U

  MKL_INT iloc, e;
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;
   
 
  
  VecN u;// Vecteur de la projection. C'est un objet de type VecN
  
  u.n = Mesh.ndof;
  
 
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    
    MP.Allocate_MatMN();

    u.Allocate();
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(Mesh, e, MP.value);
    
    //Construction du vecteur second membre correspondant à l'élément e
    BuildVector_projection_Hetero(e, u.value, Mesh);
    
    //résolution du système linéaire correspondante par la méthode LU 
    MP.Solve_by_LU(u);
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(Mesh.ndof,e,&B);
    
    //On colle la solution associée à l'élément e au vecteur global U
    for(iloc = 0; iloc < Mesh.ndof; iloc++){
      B[iloc] = u.value[iloc];
    }
    delete [] MP.value ; MP.value  =  nullptr;
    delete [] u.value ; u.value  =  nullptr;
  }
 
}



void collection_Solutions_locales_selfDecompose(VecN &U, const string & choice, A_skyline &P_red, Maillage &Mesh) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    arg     : chaîne de caractères pour définir le cas pri en charge. 
              arg = "bessel" pour du Bessel et arg = "plane" pour une onde plane
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    Mesh    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;

  Mesh.taille_red = 0;
  
  for(e = 0; e < Mesh.get_N_elements(); e++){
    
    MP.Allocate_MatMN();

    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(Mesh.ndof, e, &B);
    
    //Définition de la matrice de projection associée à l'élément e
    BuildMat_projection(Mesh, e, MP.value);
    
    //Construction du vecteur second membre correspondant à l'élément e
    BuildVector_projection_Hetero(e, B, Mesh);   

    cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];//Pour stocker la matrice MP et éviter
    //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
    
    double *w = new double[Mesh.ndof];//Pour stocker les valeurs propres

    //Décomposition et calcul de la taille du block réduit
    compute_sizeBlocRed(e, AA, w, MP.value, Mesh);

    //stockage de la taille des blocs dans le tableau SIZE (version MUMPS)
    Mesh.SIZE_LOC[e] = Mesh.taille_red;
    
    Mesh.taille_red += Mesh.elements[e].Nred_Pro;

    //stockage des Nred_Pro plus grandes valeurs propres
    cplx *V = new cplx[Mesh.elements[e].Nred_Pro];

    //clônage de P_red sur e
    cplx *PP_red = P_red.Tab_A_loc[e].get_Aloc();

    //Décomposition de la matrice MP et stockage de PP_red et des valeurs propres
    DecompositionDeBloc(e, MP.value, PP_red, V, Mesh);
    
    
    MatMN MP_red;
    MP_red.m = Mesh.elements[e].Nred_Pro;
    MP_red.n = Mesh.elements[e].Nred_Pro;
    
    MP_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = Mesh.elements[e].Nred_Pro;
    u_red.Allocate();

    //Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red 
    compute_A_red(e, choice, MP, PP_red, MP_red, Mesh);

    //u_red = conj(trans(PP_red)) * B
    compute_F_red(e, choice, B, PP_red, u_red.value, Mesh);

    //résolution du système linéaire MP_red x_red = u_red correspondante par la méthode LU
    MP_red.Solve_by_LU(u_red);

    //On retrouve la solution dans l'ancienne base B = PP_red u_red
    reverseTo_originalVector(e, choice, B, PP_red, u_red.value, Mesh);

    //libération de la mémoire 
    delete [] MP.value ; MP.value  =  nullptr;

    delete [] AA ; AA  =  nullptr;
    delete [] w ; w  =  nullptr;
    delete [] V; V  =  nullptr;
    delete [] MP_red.value; MP_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }
  
}


void MatMN::CHECK_RESIDUAL(MKL_INT e, VecN &b, VecN &u){
  
  MKL_INT iloc;
  
  VecN res, diff;
  
  res.n = n;
  
  res.Allocate();
  
  diff.n = n;
  
  diff.Allocate();
  
  produit_MatVec_Full_Version(u, res);
  
  SoustractionVecN(res, b, diff);
  
  double maxx = 0;
  double minn = 1000;

  // cout<<"RESIDUAL : "<<endl;
  // diff.print_info();
  
  for (iloc = 0; iloc < diff.n; iloc++) {
    maxx = max(maxx,abs(diff.value[iloc]));
    minn = min(minn,abs(diff.value[iloc]));
  }

  // cout<<"b                                  u                            res                            diff"<<endl;
  // for (iloc = 0; iloc < b.n; iloc++){
  //   cout<<b.value[iloc]<<"   "<<u.value[iloc]<<"   "<<res.value[iloc]<<"   "<<diff.value[iloc]<<endl;
  // 	}
  
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
}


void MatMN::CHECK_RESIDUAL(MKL_INT e, double *maxx, double *minn, VecN &b, VecN &u){
  
  MKL_INT iloc;
  
  VecN res, diff;
  
  res.n = n;
  
  res.Allocate();
  
  diff.n = n;
  
  diff.Allocate();
  
  produit_MatVec_Full_Version(u, res);
  
  SoustractionVecN(res, b, diff);
  
  *maxx = 0;
  *minn = 1000;

  // cout<<"RESIDUAL : "<<endl;
  // diff.print_info();
  
  for (iloc = 0; iloc < diff.n; iloc++) {
    *maxx = max(*maxx,abs(diff.value[iloc]));
    *minn = min(*minn,abs(diff.value[iloc]));
  }
  
}


void MatMN::CHECK_RESIDUAL(MKL_INT e, double *maxx, double *minn, cplx *b, cplx *u){
  
  MKL_INT iloc;
  
  VecN res, diff;
  
  res.n = n;
  
  res.Allocate();
  
  diff.n = n;
  
  diff.Allocate();
  
  produit_MatVec_Full_Version(u, res.value);
  
  SoustractionVecN(n, res.value, b, diff.value);
  
  *maxx = 0;
  *minn = 1000;

  // cout<<"RESIDUAL : "<<endl;
  // diff.print_info();
  
  for (iloc = 0; iloc < diff.n; iloc++) {
    *maxx = max(*maxx,abs(diff.value[iloc]));
    *minn = min(*minn,abs(diff.value[iloc]));
  }
  
}



void CHECK_ALL_RESIDUAL(Maillage &Mesh) {
  
  MKL_INT e;

  double maxx_e, minn_e, maxx, minn;
  
  MatMN MP;// Matrice de la projection sur chaque élément. C'est un objet de type MatMN
  MP.m = Mesh.ndof;
  MP.n = Mesh.ndof;
  
  
  
  VecN b;// Vecteur de la projection. C'est un objet de type VecN
  
  b.n = Mesh.ndof;
  
  
  VecN u;// La solution. C'est un objet de type VecN
  
  u.n = Mesh.ndof;
  
  
  MKL_INT aa;

  maxx = 0.0;
  minn = 1000;

  double MAXX[Mesh.get_N_elements()];
  double MINN[Mesh.get_N_elements()];

  for (e = 0; e < Mesh.get_N_elements(); e++) {

    cout<<"Allocation "<<endl;
    b.Allocate();
    
    u.Allocate();
    
    MP.Allocate_MatMN();
  
    cout<<" Définition de la matrice de projection associée à l'élément "<<e<<endl;
    BuildMat_projection(Mesh, e, MP.value);
    
    cout<<" Définition du vecteur de projection associé à l'élément "<<e<<endl;
    BuildVector_projection_Hetero(e, b.value, Mesh);
    
    cout<<" Résolution du système linéaire locale par Cholesky :"<<endl;
    Solve_By_CHOLESKY(MP.n, MP.value, b.value, u.value);
       
    cout<<"Calcul du résidu :"<<endl;
    MP.CHECK_RESIDUAL(e, &maxx_e, &minn_e, b, u);

    maxx = max(maxx,maxx_e);
    minn = min(minn,minn_e);

    MAXX[e] = maxx_e;
    MINN[e] = minn_e;

    cout<<" Libération de la mémoire "<<endl;
    delete [] b.value; b.value  =  nullptr;
    delete [] u.value; u.value  =  nullptr;
    delete [] MP.value; MP.value  =  nullptr;

  }

  ofstream tile("data/MAXX_MINN.txt");
  if (tile)
    {
      
      for (e = 0; e < Mesh.get_N_elements(); e++){
	tile.precision(12);
	tile<<e<<" "<<MAXX[e]<<" "<<MINN[e]<<endl;
	tile.flush();
      }
    }
  else
    {
      cout<<"Erreur: Impossible d'ouvrir le fichier:"<<"\n";
    }

  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
  
}


void compare_Vector_and_Column_Matrix(MKL_INT e, double *maxx, double *minn, MatMN &MP, VecN &b, Maillage &Mesh){
  MKL_INT iloc;
  cplx val, I;
  
  *maxx = 0.0;
  *minn = 1000;

  if (Mesh.elements[e].get_ref() == 1) {
    
    for(iloc = 0; iloc < Mesh.ndof; iloc++) {
      I = MP.value[iloc * Mesh.ndof + Mesh.n_i]  + Mesh.R * MP.value[iloc * Mesh.ndof + Mesh.n_r];
      val = I - b.value[iloc];
      cout<<I<<"               "<<b.value[iloc]<<"               "<<val<<endl;
      *maxx = max(*maxx, abs(val));
      *minn = min(*minn, abs(val));
    }

  }

  else {

    for(iloc = 0; iloc < Mesh.ndof; iloc++) {
      I = Mesh.T * MP.value[iloc * Mesh.ndof + Mesh.n_i];
      val = I - b.value[iloc];
      cout<<I<<"               "<<b.value[iloc]<<"               "<<val<<endl;
      *maxx = max(*maxx, abs(val));
      *minn = min(*minn, abs(val));
    }
    
  }
}


  


void MatMN::CHECK_COLUMN_WITH_RHS_VECTOR(VecN &b, MKL_INT inc) {

  MKL_INT i;
  VecN C;
  C.n = n;
  C.Allocate();

  for (i = 0; i < n; i++) {
    C.value[i] = value[i * n + inc];
  }

  cout<<"M(:, inc)                               b                             M(:,inc) - b"<<endl;
  for (i = 0; i < n; i++) {
    cout<<C.value[i]<<"      "<<b.value[i]<<"      "<<C.value[i] - b.value[i]<<endl;
  }
}


void Transforme_Hetero_realPartOf_ErrorVelocity_To_VTK_File_Vector(VecN &ALPHA, Maillage &Mesh) {
  //Cette fonction ecrit la partie réelle de la velocity solution numerique dans un fichier VTU. Non valable pour le cas d'une fonction de Bessel de première espèce j0
  //ALPHA : coordonnées globales de la solution dans l'espace de Trefftz.
  
  	MKL_INT nd = Mesh.get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * Mesh.get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	cplx V[3], P, W[3];
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < Mesh.get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 


		  //Pour i, j+1, k
		  Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i+1, j, k+1
		  Mesh.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i+1, j+1, k+1
		  Mesh.elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //Pour i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j+1, k
		   Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		    //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2]));  

		  //i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i+1, j, k+1
		  Mesh.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j+1, k
		  Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);

		  W[0] = V[0] - Mesh.elements[e].Vexacte[0];
		  W[1] = V[1] - Mesh.elements[e].Vexacte[1];
		  W[2] = V[2] - Mesh.elements[e].Vexacte[2];
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g %g %g\n",real(W[0]), real(W[1]), real(W[2])); 
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Vectors=\"Vitesse\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"V\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_Vector_plane_Error_real.vtu");
	system("more paraview_points.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_tetra.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_type.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_offset.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_Vector_plane_Error_real.vtu");
	system("more paraview_value.txt >> paraview_Vector_plane_Error_real.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_Vector_plane_Error_real.vtu");
}



void Transforme_Hetero_realPartOfScalarError_To_VTK_File(VecN &ALPHA, Maillage &Mesh) {
  //Ecriture de la partie imaginaire de la solution scalaire dans un fichier VTU
  //ALPHA : coordonnées globales de la solution dans la base d'onde plane
  
  	int nd = Mesh.get_N_elements() * 35;
	MKL_INT Ndiv = 4, Nelt = 0;
	MKL_INT ntet = 20 * Mesh.get_N_elements();

	double xx[3];
	double x, y, z;
	FILE*  fic1;
	FILE*  fic2;
	FILE*  fic3;
	FILE*  fic4;
	FILE*  fic0;
	FILE*  fic5;
	FILE* fic11;
	FILE* fic21;
	FILE* fic31;
	FILE* fic41;
	FILE* fic51;
	FILE* fic61;
	MKL_INT iloc, l, i, j, k, iiloc, iloc1, iloc2, iloc3, iiloc1, iiloc2, iiloc3;
	MKL_INT i1, j1, k1, l1, s, e, shift;

	cplx V[3], P;
	
	fic1 = fopen("paraview_tetra.txt","w");
	fic2 = fopen("paraview_points.txt","w");
	fic3 = fopen("paraview_type.txt","w");
	fic4 = fopen("paraview_offset.txt","w");
	fic5 = fopen("paraview_value.txt","w");
	fic0  = fopen("paraview_preambule.txt","w");
	shift = 0;
	for (e = 0; e < Mesh.get_N_elements(); e++){
	  for (i = 0; i < Ndiv + 1; i++){
	    for (j = 0; j < Ndiv + 1 - i; j++){
	      for (k = 0; k < Ndiv + 1 - j - i; k++){
		l = Ndiv - i - j - k;

		//VOXEL
		if (l >= 3) {
		  Nelt++;
		  //Pour i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);
		  
		  xx[0] = x; xx[1] = y; xx[2] = z;

		  //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 


		  //Pour i, j+1, k
		  Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i+1, j, k+1
		  Mesh.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i+1, j+1, k+1
		  Mesh.elements[e].xyz(i+1, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //Pour i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  fprintf(fic1, " %d %d %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5, shift + 6, shift + 7);

		  fprintf(fic3,"12 \n");
		  
		  shift += 8;
		  fprintf(fic4,"%d\n", shift);
		}

		//WEDGE + PYRAMID
		if (l == 2) {

		  //WEDGE
		  Nelt++;

		  //i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j+1, k
		   Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		   xx[0] = x; xx[1] = y; xx[2] = z;

		    //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  fprintf(fic1, " %d %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4, shift + 5);
		  
		  fprintf(fic3,"13 \n");
		  
		  shift += 6;
		  fprintf(fic4,"%d\n", shift);



		  //PYRAMID
		  Nelt++;

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j+1, k+1
		  Mesh.elements[e].xyz(i, j+1, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i+1, j+1, k
		  Mesh.elements[e].xyz(i+1, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i+1, j, k+1
		  Mesh.elements[e].xyz(i+1, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  fprintf(fic1, " %d %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3, shift + 4);
		  
		  fprintf(fic3,"14 \n");
		  
		  shift += 5;
		  fprintf(fic4,"%d\n", shift);
		}
		
		//TETRA
		else if (l == 1) {
		  Nelt++;

		  //i, j, k
		  Mesh.elements[e].xyz(i, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i+1, j, k
		  Mesh.elements[e].xyz(i+1, j, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j+1, k
		  Mesh.elements[e].xyz(i, j+1, k, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 

		  //i, j, k+1
		  Mesh.elements[e].xyz(i, j, k+1, Ndiv, &x, &y, &z);

		  xx[0] = x; xx[1] = y; xx[2] = z;

		   //Solution numerique
		  Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, xx);

		  //Solution analytique
		  analytique_P_and_V(xx, e, &P, V, Mesh);
		  
		  fprintf(fic2, " %g %g %g\n", x, y, z);
		  fprintf(fic5, "%g\n",real(P - Mesh.elements[e].Pexacte)); 
		  
		  fprintf(fic1, " %d %d %d %d\n", shift + 0, shift + 1, shift + 2, shift + 3);
		  
		  fprintf(fic3,"10 \n");
		  
		  shift += 4;
		  fprintf(fic4,"%d\n", shift);
		}

		
		//cout<<" x = "<<x<<" y = "<<y<<" z = "<<z<<endl;
	      }
	    }
	  }
	}

	fprintf(fic0, "<?xml version=\"1.0\"?>\n");
	fprintf(fic0, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
	fprintf(fic0, "<UnstructuredGrid>\n");
	fprintf(fic0, "<Piece NumberOfPoints=\" %d  \" NumberOfCells=\" %d \">\n", shift, Nelt);
	fprintf(fic0, "<Points>\n");
	fprintf(fic0, "<DataArray type=\"Float64\" NumberOfComponents=\"3\" Name=\"Point\" format=\"ascii\">\n");
	     
	fclose(fic1);
	fclose(fic2);
	fclose(fic3);
	fclose(fic4);
	fclose(fic5);
	fclose(fic0);

	fic11 = fopen("paraview_preambule_tetra.txt","w");

	fprintf(fic11, "</DataArray>\n");
	fprintf(fic11,"</Points>\n");
	fprintf(fic11,"<Cells>\n");
	fprintf(fic11,"<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");

	fclose(fic11);

	fic21 = fopen("paraview_preambule_type.txt","w");

	fprintf(fic21, "</DataArray>\n");
	fprintf(fic21, "<DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n");
	
	fclose(fic21);

	fic31 = fopen("paraview_preambule_offset.txt","w");

	fprintf(fic31,"</DataArray>\n");
	fprintf(fic31,"<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	
	fclose(fic31);

	fic41 = fopen("paraview_preambule_fonction.txt","w");

	fprintf(fic41, "</DataArray>\n");
	fprintf(fic41, "</Cells>\n");
	fprintf(fic41, "<PointData Scalars=\"P\">\n");
	fprintf(fic41, "<DataArray type=\"Float64\" Name=\"p\" format=\"ascii\">\n");
	
	fclose(fic41);

	fic51 = fopen("paraview_preambule_finale.txt","w");

	fprintf(fic51,"</DataArray>\n");
	fprintf(fic51,"</PointData>\n");
	fprintf(fic51,"</Piece>\n");
	fprintf(fic51,"</UnstructuredGrid>\n");
	fprintf(fic51,"</VTKFile>");
	fclose(fic51);
	
	system("more paraview_preambule.txt > paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_points.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_preambule_tetra.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_tetra.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_preambule_type.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_type.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_preambule_offset.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_offset.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_preambule_fonction.txt >> paraview_finale_scalar_plane_error_real.vtu");
	system("more paraview_value.txt >> paraview_finale_scalar_plane_error_real.vtu   ");
	system("more paraview_preambule_finale.txt >> paraview_finale_scalar_plane_error_real.vtu");
}



void Build_A1(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntPP(i, j, Mesh.elements[i].T_PP[j], Mesh.elements[i].T_PP[j], A_glob, Mesh, chaine);
    }
  }
}


void Build_A4(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntVV(i, j, Mesh.elements[i].T_VV[j], Mesh.elements[i].T_VV[j], A_glob, Mesh, chaine);
    }
  }
}


void Build_A2(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntPV(i, j, Mesh.elements[i].T_PV[j], Mesh.elements[i].T_PV[j], A_glob, Mesh, chaine);
    }
  }
}


void Build_A3(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntVP(i, j, Mesh.elements[i].T_VP[j], Mesh.elements[i].T_VP[j], A_glob, Mesh, chaine);
    }
  }
}


void compare_Vector_and_Column_Matrix(MKL_INT e, double *maxx, double *minn, cplx *AA, cplx *b, Maillage &Mesh){
  MKL_INT iloc;
  cplx val, I;
  
  *maxx = 0.0;
  *minn = 1000;

  if (Mesh.elements[e].get_ref() == 1) {
    
    for(iloc = 0; iloc < Mesh.ndof; iloc++) {
      I = AA[iloc * Mesh.ndof + Mesh.n_i]  + Mesh.R * AA[iloc * Mesh.ndof + Mesh.n_r];
      val = I - b[iloc];
      cout<<I<<"               "<<b[iloc]<<"               "<<val<<endl;
      *maxx = max(*maxx, abs(val));
      *minn = min(*minn, abs(val));
    }

  }

  else {

    for(iloc = 0; iloc < Mesh.ndof; iloc++) {
      I = Mesh.T * AA[iloc * Mesh.ndof + Mesh.n_i];
      val = I - b[iloc];
      cout<<I<<"               "<<b[iloc]<<"               "<<val<<endl;
      *maxx = max(*maxx, abs(val));
      *minn = min(*minn, abs(val));
    }
    
  }
}


void compare_Vector_and_Column_Matrix(const string & arg, Maillage &Mesh, const char chaine[]){

  A_skyline A_glob;
  A_glob.set_n_elem(Mesh.get_N_elements()); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(Mesh.ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(); // Construit les blocks et la structure en mémoire continue

  VecN U;
  
  U.n = Mesh.get_N_elements() * Mesh.ndof;
  U.Allocate();

  double maxx_e;
  double minn_e;
  
  double maxx = 0.0;
  double minn = 1000;

  MKL_INT e;

  cplx *AA, *u;
  
  if (arg == "PP") {
    Build_A5(A_glob, Mesh, chaine);
    Build_VectorPincP_T(U.value, Mesh, chaine);
    
    for (e = 0; e < Mesh.get_N_elements(); e++) {
      
      AA = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + 0].get_Aloc();//clônage du bloc e
      
      U.get_F_elem(Mesh.ndof, e, &u);//clônage du vecteur associée à l'élément e
      
      compare_Vector_and_Column_Matrix(e, &maxx_e, &minn_e, AA, u, Mesh);
      
      maxx = max(maxx, maxx_e);
      minn = min(minn, minn_e);
      
    }

    cout<<" maxx = "<<maxx<<" minn = "<<minn<<endl;
  }


  else if (arg == "PV") {
    
    Build_A6(A_glob, Mesh, chaine);
    
    Build_VectorPincV_T(U.value, Mesh, chaine);
    
    for (e = 0; e < Mesh.get_N_elements(); e++) {
      
      AA = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + 0].get_Aloc();//clônage du bloc e
      
      U.get_F_elem(Mesh.ndof, e, &u);//clônage du vecteur associée à l'élément e
      
      compare_Vector_and_Column_Matrix(e, &maxx_e, &minn_e, AA, u, Mesh);
      
      maxx = max(maxx, maxx_e);
      minn = min(minn, minn_e);
      
    }
    
    cout<<" maxx = "<<maxx<<" minn = "<<minn<<endl;
  }

  else if (arg == "VP") {
    
    Build_A7(A_glob, Mesh, chaine);
    
    Build_VectorVincP_T(U.value, Mesh, chaine);
    
    for (e = 0; e < Mesh.get_N_elements(); e++) {
      
      AA = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + 0].get_Aloc();//clônage du bloc e
      
      U.get_F_elem(Mesh.ndof, e, &u);//clônage du vecteur associée à l'élément e
      
      compare_Vector_and_Column_Matrix(e, &maxx_e, &minn_e, AA, u, Mesh);
      
      maxx = max(maxx, maxx_e);
      minn = min(minn, minn_e);
      
    }
    
    cout<<" maxx = "<<maxx<<" minn = "<<minn<<endl;
  }

   else if (arg == "VV") {
    
     Build_A8(A_glob, Mesh, chaine);
     
     Build_VectorVincV_T(U.value, Mesh, chaine);
     
     for (e = 0; e < Mesh.get_N_elements(); e++) {
       
       AA = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + 0].get_Aloc();//clônage du bloc e
       
       U.get_F_elem(Mesh.ndof, e, &u);//clônage du vecteur associée à l'élément e
       
       compare_Vector_and_Column_Matrix(e, &maxx_e, &minn_e, AA, u, Mesh);
       
       maxx = max(maxx, maxx_e);
       minn = min(minn, minn_e);
     }
     
     cout<<" maxx = "<<maxx<<" minn = "<<minn<<endl;
   }
  
  
  
  else {
    cout<<"Pas encore codé "<<endl;
  }
}
/*--------------------------------------------------------------------------- diag ([P] [P']) --------------------------------------------------------------------------*/
void BlocIntDiagPP(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice diagoanle de [P][P'] sur l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    alpha     : coeff associé au bloc
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element P_T x P_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
    
    //construction de la matrice bloc diagonale correspondante à l'emplacement (i,0) dans A_skyline
    Build_BlocInt_P_TxP_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_DiagA1(A_skyline &A_glob, Maillage &Mesh,const char chaine[])  {
  //Cette fonction calcule la matrice diagoanle de [P][P']
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntDiagPP(i, j, Mesh.elements[i].T_PP[j], A_glob, Mesh, chaine);
    }
  }
}

/*----------------------------------------------------------------------------- diag([V] [V'])----------------------------------------------------------------------*/
void BlocIntDiagVV(MKL_INT i, MKL_INT l, double alpha, A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [V][V'] sur l'élément i
  /*--------------------------------------------------------------------- input -------------------------------------------
    i         : numero global de l'élément
    l         : numero local d'une face
    alpha     : coeff associé au bloc
    A_glob    : tableau skyline des valeurs de la matrive
    Mesh      : le maillage
    /*------------------------------------------------------------------- ouput ------------------------------------------
    A_glob    : 
    /*--------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  /*--------------------------------------------- Interaction element - element V_T x V_T -------------------------------*/

  if (Mesh.elements[i].voisin[l] >= 0){
  
    //construction de la matrice bloc diagonale correspondante à l'emplacement (i,0) dans A_skyline
    Build_BlocInt_V_TxV_T(i, l, AA, Mesh, chaine);
    
    //Ajout de la matrice dans A_glob pour j = 0
    A_glob.Add_Block(i, 0, alpha, AA);
    
  }
  
  delete [] AA; AA  =  nullptr;
}


void Build_DiagA4(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice diagonale de [V][V']
  MKL_INT i, j;
  for (i = 0; i < Mesh.get_N_elements(); i++) {
    for(j = 0; j < 4; j++) {
      
      BlocIntDiagVV(i, j, Mesh.elements[i].T_VV[j], A_glob, Mesh, chaine);
    }
  }
}

/*------------------------------------------------------------------------------------ Building M with A1, A4, A5 and A8 ----------------------------------------------------------*/

void Build_M_withA1_A4_A5_A8(A_skyline &M_glob, Maillage &Mesh, const char chaine[]) {
  //Cette fonction calcule la matrice de M de Cessenat-Desprès
  Build_DiagA1(M_glob, Mesh, chaine);

  Build_DiagA4(M_glob, Mesh, chaine);

  Build_A5(M_glob, Mesh, chaine);

  Build_A8(M_glob, Mesh, chaine);
  
}

void DecompositionDesBlocD1Matrice(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P, VecN &LAMBDA, Maillage &Mesh) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    P      : tableau pour stocker la base de vecteurs propres
    LAMBDA : .................... les valeurs propres 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P      :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    P         : ........... P_red ...................
    V         : ........... LAMBDA ..................
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC;

  cplx *AA = new cplx[Mesh.ndof * Mesh.ndof];
  
  cplx *PP = P.Tab_A_loc[e].get_Aloc();
  
  cplx *V;
  
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      AA[iloc * Mesh.ndof + jloc] = (A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc())[iloc * Mesh.ndof + jloc];
    }
  }
  
  
  double *w = new double[Mesh.ndof];//pour stocker les valeurs propres; w = D
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_ROW_MAJOR , 'V', 'L', Mesh.ndof, AA, Mesh.ndof, w);
  
  //stockage de la base réduite associée à l'élément e
  for (iloc = 0; iloc < Mesh.ndof; iloc++){
    for (jloc = 0; jloc < Mesh.ndof; jloc++){
      PP[iloc * Mesh.ndof + jloc] = AA[iloc * Mesh.ndof + jloc];
    }
  }
  
  //clônage du tableau LAMBDA sur l'élément e
  LAMBDA.get_F_elem(Mesh.ndof, e, &V);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc =  0; iloc < Mesh.ndof; iloc++){
    V[ILOC] = cplx(w[iloc],0.0);
    ILOC++;
  }
  
 
  delete [] w; w  =  nullptr;
  delete [] AA; AA  =  nullptr;
}


void DecompositionGlobaleDesBlocsD1Matrice(A_skyline &A_glob, A_skyline &P, VecN &LAMBDA, Maillage &Mesh){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P      : tableau pour stocker la base de vecteurs propres
    LAMBDA : .................... les valeurs propres 
    Mesh   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P      :
    /*---------------------------------------------------------------------------------------------------------------------*/
  for (MKL_INT e = 0; e < Mesh.get_N_elements(); e++){
    DecompositionDesBlocD1Matrice(e, 0, A_glob, P, LAMBDA, Mesh);
  }
}


/*---------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

void Build_SUM_A1_to_A8(A_skyline &A_glob, Maillage &Mesh, const char chaine[]) {
   //Cette fonction calcule la matrice de la formulation consistante de Cessenat-Desprès à partir des 8 formes de A1 à A8
  /*--------------------------------------------------------------------------------- input ---------------------------------------------------------------------------------
    A_glob     : tableau skyline ou stocké la matrice 
    Mesh       : le maillage
    /*------------------------------------------------------------------------------- output -------------------------------------------------------------------------------
                        A_glob la matrice consistante de Cessenat-Desprès
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  Build_A1(A_glob, Mesh, chaine);

  Build_A2(A_glob, Mesh, chaine);

  Build_A3(A_glob, Mesh, chaine);

  Build_A4(A_glob, Mesh, chaine);

  Build_A5(A_glob, Mesh, chaine);

  Build_A6(A_glob, Mesh, chaine);

  Build_A7(A_glob, Mesh, chaine);

  Build_A8(A_glob, Mesh, chaine); 
}


void Solve_Sol_with_Sparse(VecN &U, VecN &B, A_skyline &A_glob, Maillage &Mesh) {
  //Cette fonction calcule la solution du système linéaire AU = B avec A = A1 + ... + A8
  // et B = F5 + ... + F8
  /*--------------------------------------------------------------------------------- input ----------------------------------------------------------------------------------
    U      : tableau ou stocké la solution 
    B      : le vecteur second membre
    A_glob : la matrice A en format skyline
    Mesh   : le maillage
    /*------------------------------------------------------------------------------- output ---------------------------------------------------------------------------------
                             U la solution
    /*------------------------------------------------------------------------------ intermédiare ----------------------------------------------------------------------------
    AS     : structure Sparse pour stocker la matrice
    res    : objet de type VecN ; res = A U
    diff   : .................. ; diff = res - B; c'est le résidu
    maxx   : le max du résidu
    minn   : le min du résidu
    /*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
   MKL_INT iloc;
   cout<<" Resolution de (A1 + A2 + ...... + A8) U = F5 + ... + F8 :"<<endl;
  Sparse AS;
  
  AS.m = Mesh.get_N_elements() * Mesh.ndof;
  AS.n = Mesh.get_N_elements() * Mesh.ndof;

  AS.transforme_A_skyline_to_Sparse_CSR(A_glob, Mesh);

  AS.compute_solution(B, U);
  
  VecN res, diff;
  
  res.n = AS.n;
  
  res.Allocate();

  diff.n = AS.n;
  
  diff.Allocate();
  
  produit_MatSparse_vecteur(AS, U, res);

  SoustractionVecN(res, B, diff);
  
  double maxx = 0;
  double minn = 1000;

  cplx val;
  
  for (MKL_INT i = 0; i < diff.n; i++) {
    maxx = max(maxx,abs(diff.value[i]));
    minn = min(minn,abs(diff.value[i])); 

  }
  
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<endl;
  
}

void TEST_Error_SolA1_to_A8_and_AnaSol(VecN &U, A_skyline &A_glob, Maillage &Mesh) {

  cout<<" Uex - U_num : "<<endl;
  VecN Uex;
  
  Uex.n = Mesh.get_N_elements() * Mesh.ndof;
  Uex.Allocate();

  Hetero_PlaneWave_Solution(Uex, Mesh);

  MKL_INT iloc;
  
  double maxx_u = 0;
  double minn_u = 1000;

  cplx val;
  
  for (MKL_INT i = 0; i < U.n; i++) {
    
    val = Uex.value[i] - U.value[i];

    maxx_u = max(maxx_u,abs(val));
    minn_u = min(minn_u,abs(val));
  }
  
  cout<<"maxx_u = "<<maxx_u<<" minn_u = "<<minn_u<<endl;
}




cplx quad_tetra_heteroPlane(MKL_INT e, MKL_INT iloc, VecN &ALPHA, Maillage &Mesh) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : indice du tetra
    iloc   : numero d'une onde plane
    ALPHA  : coordonnées de la solution analytique sur tous les éléments
    Mesh   : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = Mesh.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = Mesh.elements[e].noeud[1]->get_X();//................ 2
  x3 = Mesh.elements[e].noeud[2]->get_X();//................ 3
  x4 = Mesh.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  
  for (i = 0; i < Mesh.Qd.ordre * Mesh.Qd.ordre * Mesh.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Mesh.Qd.Xtetra[3*i] * x2x1[j] + Mesh.Qd.Xtetra[3*i+1] * x3x1[j] + Mesh.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calul du combinaison linéaire de la solution analytique en Xquad
    Mesh.elements[e].combinaison_solution(Mesh.ndof, e, ALPHA, Xquad);
    
    I = I + Mesh.Qd.Wtetra[i] * conj(Mesh.elements[e].OP[iloc].pression(Xquad)) * Mesh.elements[e].Pexacte;
  }

  I = I * Mesh.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  
  return I;
}



void TEST_A5_to_A8_with_Impedance_Coeff(const string & chaine, MailleSphere &MSph, const char arg[]){

  A_skyline A_glob;
  A_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(); // Construit les blocks et la structure en mémoire continue


  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(5); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue

   VecN U;
  
  U.n = MSph.get_N_elements() * MSph.ndof;
  U.Allocate();

  VecN B;
  
  B.n = U.n;
  B.Allocate();

  
  if (chaine == "PP_EXT") {
    Build_A5(A_glob, MSph, arg);

    ASSEMBLAGE_DU_VecteurRHS_F5(B.value, MSph);
  }

  else if (chaine == "PV_EXT") {
    Build_A6(A_glob, MSph, arg);

    ASSEMBLAGE_DU_VecteurRHS_F6(B.value, MSph);
  }

  else if (chaine == "VP_EXT") {
    Build_A7(A_glob, MSph, arg);

    ASSEMBLAGE_DU_VecteurRHS_F7(B.value, MSph);
  }

  else if (chaine == "VV_EXT") {
    Build_A8(A_glob, MSph, arg);

    ASSEMBLAGE_DU_VecteurRHS_F8(B.value, MSph);
  }

  else {
    cout<<" probleme de mot clef : "<<endl;
  }

  Build_M_withA1_A4_A5_A8(M_glob, MSph, arg);

  
  MKL_INT iloc;
  
  double u1, u2;
  ifstream hic("data/1E10Sol_numerique_reduite_Bessel_0125.txt");
  if(hic) { 
    for(iloc = 0; iloc < U.n; iloc++){
      hic.precision(12);
      hic >> u1 >> u2 ;
      U.value[iloc] = cplx(u1,u2);
    }
  }
  else{
    cout<<"probleme d''ouverture du fichier points"<<"\n";
  }
  
  
  Sparse AS;
  
  AS.m = U.n;
  AS.n = U.n;

  AS.transforme_A_skyline_to_Sparse_CSR(A_glob, MSph);

  VecN res, diff;
	
  res.n = AS.n;
  
  res.Allocate();

  diff.n = AS.n;
  
  diff.Allocate();

  produit_MatSparse_vecteur(AS, U, res);

  SoustractionVecN(res, B, diff);

  cplx I;
  cblas_zdotc_sub(U.n, U.value, 1, diff.value, 1, &I);

  double J = normeUWVF(M_glob, U, MSph);
  
  double maxx = 0;
  double minn = 1000;
  
  for (MKL_INT i = 0; i < diff.n; i++) {
    maxx = max(maxx,abs(diff.value[i]));
    minn = min(minn,abs(diff.value[i]));
  }
  
  cout<<"maxx (A U_h - F) = "<<maxx<<" minn (A U_h - F)= "<<minn<<endl;
  cout<<"PU^* (A U_h - F) = "<<I<<" U_h* M U_h = "<<J<<" U_h^* (A U_h - F) / U_h^* M_h U_h = "<<I / J<<endl;
  
}


void TEST_Du_Solution(VecN &U) {
  MKL_INT i;
  cplx som = cplx(0.0, 0.0);

  for( i = 0; i < U.n; i++){
    som += U.value[i];
  }

  cout<<"som(U) = "<<som<<endl;

  // for( i = 0; i < U.n; i++){
  //   if (abs(U.value[i]) == nan){
  //     cout<<"i = "<<i<<" U[i] = "<<U.value[i]<<endl;
  //   }
  // }
}


void TEST_Error_SolA1_to_A8_and_AnaSol(VecN &Uex, VecN &U, A_skyline &A_glob, MailleSphere &MSph) {

  cout<<" PUex - U_h : "<<endl;
  
  MKL_INT iloc;
  
  double maxx_u = 0;
  double minn_u = 1000;

  cplx val = cplx(0.0, 0.0);

  VecN W;
  
  W.n = U.n;
  W.Allocate();

  SoustractionVecN(Uex, U, W);
  
  for (MKL_INT i = 0; i < U.n; i++) {
    val += W.value[i];
    maxx_u = max(maxx_u,abs(W.value[i]));
    minn_u = min(minn_u,abs(W.value[i]));
  }
  
  cout<<"maxx_u = "<<maxx_u<<" minn_u = "<<minn_u<<" som(PUex - U_h) = "<<val<<endl;

  cout<<" calcul de la norme DG "<<endl;
  double DG = normeDG(A_glob, W, MSph);

  cout<<"DG = "<<DG<<endl;
}


void TEST_Solution(VecN &U) {
  MKL_INT iloc;
  
  double maxx_u = 0;
  double minn_u = 1000;
  
  cplx val = cplx(0.0, 0.0);

  double norme_2 = 0.0;
  
 
  for (MKL_INT i = 0; i < U.n; i++) {
    val += U.value[i];
    norme_2 += abs(U.value[i] * U.value[i]);
    maxx_u = max(maxx_u,abs(U.value[i]));
    minn_u = min(minn_u,abs(U.value[i]));
  }

  norme_2 = sqrt(norme_2);
  cout<<"maxx_u = "<<maxx_u<<" minn_u = "<<minn_u<<" som(U) = "<<val<<endl;

  cout<<" norme_2 = "<<norme_2<<endl;
}


void TEST_AU(VecN &U, A_skyline &A_glob, MailleSphere &MSph) {

  Sparse AS;
  
  AS.m = U.n;
  AS.n = U.n;
  
  AS.transforme_A_skyline_to_Sparse_CSR(A_glob, MSph);
  
  VecN res;
  
  res.n = AS.n;
  
  res.Allocate();

  produit_MatSparse_vecteur(AS, U, res);

  double maxx_u = 0;
  double minn_u = 1000;
  
  cplx val = cplx(0.0, 0.0);
  
  double norme_2 = 0.0;
  
  
  for (MKL_INT i = 0; i < U.n; i++) {
    val += res.value[i];
    norme_2 += abs(res.value[i] * res.value[i]);
    maxx_u = max(maxx_u,abs(res.value[i]));
    minn_u = min(minn_u,abs(res.value[i]));
  }
  
  norme_2 = sqrt(norme_2);
  cout<<"maxx_u = "<<maxx_u<<" minn_u = "<<minn_u<<" som(AU) = "<<val<<endl;
  
  cout<<" norme_2 = "<<norme_2<<endl;

  cplx I = cplx(0.0, 0.0);
  cblas_zdotc_sub(U.n, U.value, 1, res.value, 1, &I);

  cout<<"abs(U^* A U) = "<<abs(I)<<endl;
}


double TEST_U_AU(VecN &U, A_skyline &A_glob, MailleSphere &MSph) {
  
  Sparse AS;
  
  AS.m = U.n;
  AS.n = U.n;
  
  AS.transforme_A_skyline_to_Sparse_CSR(A_glob, MSph);
  
  VecN res;
  
  res.n = AS.n;
  
  res.Allocate();
  
  produit_MatSparse_vecteur(AS, U, res);
  
  cplx I = cplx(0.0, 0.0);
  cblas_zdotc_sub(U.n, U.value, 1, res.value, 1, &I);

  double J = real(I);
  
  return J;
}



void compute_infty_norm(MKL_INT e, cplx *u, MailleSphere &MSph) {
  
  MSph.elements[e].combinaison_projectionSolutionPressure(MSph.ndof, u, MSph.elements[e].XG);
  
}


void compute_infty_norm(double *P, VecN &ALPHA, MailleSphere &MSph) {

  MKL_INT i;

  cplx *u;
  
  *P = 0.0;
  
  for(i = 0; i < MSph.get_N_elements(); i++) {
    
    ALPHA.get_F_elem(MSph.ndof, i, &u);
    
    compute_infty_norm(i, u, MSph);
    
    *P = max(*P, abs(MSph.elements[i].Pro_P));
  }
}
void compute_sizeBlocRed(MKL_INT e, MKL_INT taille, cplx *AA, double *w, A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph) {
  //cette fonction decompose le bloc numero e sous la forme de Msky = P D P* (avec P une matrice orthonormale et D contient les valeurs propres) et calcule la taille de la nouvelle base réduite de Galerkin associée à l'élément e
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e          : numero de l'élément
    taille     : la taille du bloc associée à l'élément e
    AA         : pour clôner A_glob sur chaque élément
    w          : pour stocker toutes les valeurs propres du bloc de l'élément
    Msky       : structure contenant la taille de la matrice Msky
    Msky_mem   : tableau contenant tous les blocs de la matrice Msky
    MSph       : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Nred : la taille de la nouvelle base réduite de Galerkin
    Nres : ndof - Nred
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc;

  MSph.elements[e].Nred = 0;

  cplx *block;

  get_A__skyline_Block(Msky, Msky_mem, e, 0, taille, &block);

  copy_tab(taille, block, taille, AA);
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);


  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    if (w[iloc] > MSph.elements[e].eps_max){
      MSph.elements[e].Nred++;
    }
  }

  MSph.elements[e].Nres = MSph.ndof - MSph.elements[e].Nred;
  
}


MKL_INT compute_sizeBlocRed(A__skyline &Msky, cplx *Msky_mem, MailleSphere &MSph) {
  //Cette fonction calcule la taille réduite de la grande matrice de passage P_red correspondante à la nouvelle base de Galerkin discrète
  /*--------------------------------------------------------------- input -------------------------------------------------/
    Msky     : structure définissant la taille de notre matrice Msky 
    Msky_mem : tableau des blocs de Msky
    MSph     : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille = 0;
  MKL_INT size = MSph.ndof * MSph.ndof;
  cplx *AA = new cplx[size];
  cplx *adjP = new cplx[size];
  cplx *PD = new cplx[size];
  cplx *PDP = new cplx[size];
  double *w = new double[MSph.ndof];//pour stocker les valeurs propres; w = D
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    compute_sizeBlocRed(e, size, AA, w, Msky, Msky_mem, MSph);
    MKL_INT r = VerifyDecompositionPMP(Msky, Msky_mem, e, size, AA, w, MSph, adjP, PD, PDP);
    
    if (r == 1) {
      MSph.SIZE[e] = taille;
      taille += MSph.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
  }
  
  delete [] AA; AA  =  nullptr;
  delete [] w; w  =  nullptr;
  delete [] adjP; adjP  =  nullptr;
  delete [] PD; PD  =  nullptr;
  delete [] PDP; PDP  =  nullptr;
  
  return taille;
}


MKL_INT compute_thesizeOfA_red(MailleSphere &MSph){
  //Cette fonction calcule la taille réduite de la grande matrice A_red  = P_red^* A P_red
  //Faire d'abord appel à la fonction compute_sizeBlocRed(A_skyline &A_glob, Maillage &MSph) pour définir la taille de chaque bloc P_red
  /*--------------------------------------------------------------- input -------------------------------------------------/ 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    taille : la taille de A_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille = 0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
   
    taille += MSph.elements[e].Nred * MSph.elements[e].Nred;
  }

  MKL_INT f;
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    for (MKL_INT j = 0; j < 4; j++){
      if (MSph.elements[e].voisin[j] >= 0){
	
      f = MSph.elements[e].voisin[j];
      taille += MSph.elements[e].Nred * MSph.elements[f].Nred;
      }
      else{
	
	taille += MSph.elements[e].Nred * MSph.elements[e].Nred;
      }
    }
  }
  
  return taille;
}



void DecompositionDeBloc(MKL_INT e, MKL_INT taille, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph, cplx *AA, double *D) {
  //cette fonction decompose le bloc numero e de la matrice Msky sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e          : numero de l'élément
    Msky       : structure définissant la taille de la matrice Msky
    Msky_mem   : tableau contenant tous les blocs
    Psky       : structure définissant la taille de la matrice Psky
    Psky_mem   : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA     : .................... les N_red plus grandes valeurs propres 
    MSph       : le maillage
    AA         : tableau pour copier le bloc e de Msky
    D          : pour stocker toutes les valeurs propres issues de la décomposition du bloc e
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Psky et Psky_mem :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    P         : tableau pour stocker l'adresse du bloc de Psky sur e
    V         : ...........------------------ des valeurs propres LAMBDA sur e
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, JLOC;
  
  cplx *P, *block;

  //P = Psky_mem[e]
  //get_A__skyline_Block(Psky, Psky_mem, e, 0, taille, &P);
  get_P__skyline_Block_red(Psky, Psky_mem, e, &P, MSph);
  
  //block = Msky_mem[e]
  get_A__skyline_Block(Msky, Msky_mem, e, 0, taille, &block);
  
  cplx *V;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    
    D[iloc] = 0.0;
    
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = block[jloc * MSph.ndof + iloc];
    }
  }
  
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, D);
  
  //stockage de la base réduite associée à l'élément e
  ILOC = 0;
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    JLOC = 0;
    for (jloc = MSph.ndof-1; jloc >= MSph.elements[e].Nres; jloc--){
      P[JLOC * MSph.ndof + ILOC] = AA[jloc * MSph.ndof + iloc];
      JLOC++;
    }
    ILOC++;
  }
  
  //V = LAMBDA[e]
  get_V__skyline_Block_red(LAMBDA, e, &V, MSph);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc = MSph.ndof-1; iloc >=  MSph.elements[e].Nres; iloc--){
      V[ILOC] = D[iloc];
      ILOC++;
  }
  
}



void DecompositionGlobaleDesBlocs(A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue ainsi que les valeurs propres et les vecteurs propres issues de la décomposition des blocs
  /*--------------------------------------------------------------- input -------------------------------------------------/
    Msky       : structure définissant la taille de la matrice Msky
    Msky_mem   : tableau contenant tous les blocs
    Psky       : structure définissant la taille de la matrice Psky
    Psky_mem   : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA     : .................... les N_red plus grandes valeurs propres 
    MSph       : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
                                                 Psky, Psky_mem, LAMBDA
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille  = MSph.ndof * MSph.ndof;
  cplx *AA = new cplx[taille];
  double *D = new double[MSph.ndof];
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    DecompositionDeBloc(e, taille, Msky, Msky_mem, Psky, Psky_mem, LAMBDA, MSph, AA, D);
  }

  delete [] AA; AA  =  nullptr;
  delete [] D; D  =  nullptr;
}


void TEST_Decomposition(const char chaine1[], const char chaine2[], MailleSphere &MSph, const char arg[]) {

 
  
  cplx *LAMBDA = new cplx[MSph.ndof * MSph.get_N_elements()];
  for(MKL_INT i = 0; i < MSph.ndof * MSph.get_N_elements(); i++)
    LAMBDA[i] = cplx(0.0, 0.0);
  
  A__skyline Msky;
  
  cplx *Msky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Msky);
  
  Msky_mem = new cplx[Msky.n_A];
  
  for (MKL_INT i = 0; i < Msky.n_A; i++) {
    Msky_mem[i] = cplx(0.0,0.0);
  }

  A__skyline Psky;
  
  cplx *Psky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Psky);
  
  Psky_mem = new cplx[Psky.n_A];
  
  for (MKL_INT i = 0; i < Psky.n_A; i++) {
    Psky_mem[i] = cplx(0.0,0.0);
  }
  
  cout<<" Pour Msky : "<<endl;
  cout<<"n_dof = "<<Msky.n_dof<<" n_elem = "<<Msky.n_elem<<" n_inter = "<<Msky.n_inter<<" n_A = "<<Msky.n_A<<endl;

   cout<<" Pour Psky : "<<endl;
  cout<<"n_dof = "<<Psky.n_dof<<" n_elem = "<<Psky.n_elem<<" n_inter = "<<Psky.n_inter<<" n_A = "<<Psky.n_A<<endl;
  
#pragma omp parallel
  {
    ASSEMBLAGE_DelaMatrice_M__Skyline(Msky, Msky_mem, MSph, arg);
  }  
  
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(5); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue

#pragma omp parallel
  {
    ASSEMBLAGE_DelaMatrice_M_Skyline(M_glob, MSph, arg);
  }
  A_skyline P_red;
  P_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  P_red.set_n_inter(1); // fixe le nombre d'interactions 
  P_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  P_red.set_n_A(); // calcule le nombre de coeff a stocker
  P_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()

  VecN SIGMA;
  SIGMA.n = compute_sizeBlocRed(M_glob, MSph);

  SIGMA.Allocate();
  
  MKL_INT size_Vec = compute_sizeBlocRed(Msky, Msky_mem, MSph);
  
  cout<<"size_Vec = "<<size_Vec<<endl;

  double facteur = 1.0 / 1000.0;
  
  cout<<"Decomposition de M_glob : "<<endl;
  auto t1 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs(M_glob, P_red, SIGMA, MSph);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_T1 = t2 - t1;

  double TT1 = float_T1.count() * facteur;
  
  cout<<"Decomposition de Msky : "<<endl;
  auto t3 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs(Msky, Msky_mem, Psky, Psky_mem, LAMBDA, MSph);
  auto t4 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_T2 = t4 - t3;

  double TT2 = float_T2.count() * facteur;
  
  FILE* fic;
  FILE* gic;

  char *file1, *file2;
  
  file1 = (char *) calloc(strlen(chaine1) + 5, sizeof(char));
  strcpy(file1, chaine1);

  file2 = (char *) calloc(strlen(chaine2) + 5, sizeof(char));
  strcpy(file2, chaine2);
  
  strcat(file1,".txt");

  strcat(file2,".txt");

  if( !(fic = fopen(file1,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }

  if( !(gic = fopen(file2,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }

  cplx *V, *W, *P, *Q, val, wal;

  MKL_INT taille = MSph.ndof * MSph.ndof;

  double maxx_x, maxx, minn_x, minn;

  MKL_INT e, iloc, jloc;

  for(e = 0; e < MSph.get_N_elements(); e++) {
    
    get_V__skyline_Block_red(LAMBDA, e, &V, MSph);
    
    get_F_elemRed(e, &W, SIGMA, MSph);
    
    
    //get_A__skyline_Block(Psky, Psky_mem, e, 0, taille, &P);
    get_P__skyline_Block_red(Psky, Psky_mem, e, &P, MSph);
    
    Q = P_red.Tab_A_loc[e].get_Aloc();

    maxx = 0.0; minn = 1000.0;

    maxx_x = 0.0; minn_x = 1000.0;

    for (iloc = 0; iloc < MSph.ndof; iloc++){
       for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++){
	 val = P[iloc + jloc * MSph.ndof] - Q[iloc + jloc * MSph.ndof];
	 maxx = max(maxx, abs(val));
	 minn = min(minn, abs(val));
       }
    }

    fprintf(fic, "%d\t %10.15e\t %10.15e\t %10.15e\n", e, abs(P[0]), maxx, minn); 


    for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++){
      wal = V[jloc] - W[jloc];
      maxx_x = max(maxx_x, abs(wal));
      minn_x = min(minn_x, abs(wal));
    }

    fprintf(gic, "%d\t %10.15e\t %10.15e\t %10.15e\n", e, abs(V[0]), maxx_x, minn_x);

  }

  cout<<" time_decom(M_glob) = "<<TT1<<" time_decom(Msky) = "<<TT2<<endl;
}


void DecompositionDeBloc_ET_Normalisation(MKL_INT e, MKL_INT taille, A__skyline &Asky, cplx *Asky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph, cplx *AA, double *w) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e          : numero de l'élément
    Asky       : structure définissant la taille de la matrice Msky
    Asky_mem   : tableau contenant tous les blocs
    Psky       : structure définissant la taille de la matrice Psky
    Psky_mem   : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA     : .................... les N_red plus grandes valeurs propres 
    MSph       : le maillage
    AA         : tableau pour copier le bloc e de Msky et éviter d'écraser les blocs de Msky
    D          : pour stocker toutes les valeurs propres issues de la décomposition du bloc e
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    Psky et Psky_mem :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour stocker l'adresse du bloc e de Asky
    P         : ..........------------------------- Psky
    V         : ..........------------- de la restriction sur e de LAMBDA
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, kloc, JLOC;

  cplx *P;
  //P = Psky_mem[e]
  get_P__skyline_Block_red(Psky, Psky_mem, e, &P, MSph);

  cplx *block;
  //block = Asky_mem[e]
  get_A__skyline_Block(Asky, Asky_mem, e, 0, taille, &block);
  
  cplx *V;
  
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = block[jloc * MSph.ndof + iloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);

  //clônage du tableau LAMBDA sur l'élément e
  get_V__skyline_Block_red(LAMBDA, e, &V, MSph);
  
  //stockage des N_red plus grandes valeurs propres du bloc associé à l'élément e
  ILOC = 0;
  for (iloc = MSph.ndof-1; iloc >= MSph.elements[e].Nres; iloc--){
      V[ILOC] = w[iloc];
      ILOC++;
  }

 
  //stockage de la base réduite associée à l'élément e
  ILOC = 0;
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    JLOC = 0;
    for (jloc = MSph.ndof-1; jloc >= MSph.elements[e].Nres; jloc--){
      for (kloc = MSph.ndof-1; kloc >= MSph.elements[e].Nres; kloc--){
	if (kloc == jloc) {
	  P[JLOC * MSph.ndof + ILOC] = AA[jloc * MSph.ndof + iloc] * (1.0 / sqrt(w[kloc]));
	}
      }
      JLOC++;
    }
    ILOC++;
  }
  
}


void DecompositionGlobaleDesBlocs_ET_Normalisation(A__skyline &Asky, cplx *Asky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    Asky       : structure définissant la taille de la matrice Msky
    Asky_mem   : tableau contenant tous les blocs
    Psky       : structure définissant la taille de la matrice Psky
    Psky_mem   : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA     : .................... les N_red plus grandes valeurs propres 
    MSph       : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_red :
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT taille  = MSph.ndof * MSph.ndof;
  cplx *AA = new cplx[taille];
  double *D = new double[MSph.ndof];
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    DecompositionDeBloc_ET_Normalisation(e, taille, Asky, Asky_mem, Psky, Psky_mem, LAMBDA, MSph, AA, D);
  }
  
  delete [] AA; AA  =  nullptr;
  delete [] D; D  =  nullptr;
}



/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/

void compute_A_red_ColMajor(MKL_INT e, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP){

   /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    M_glob  : structure définissant la matrice de la projection
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres de la matrice M
    M_red   : conj(trans(P_red)) *  M * P_red
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           A_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    P       :=  P_red
    AdjtP   := conj(trans(P_red))
    MP       := M * b
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  cplx *M = M_glob.Tab_A_loc[e].get_Aloc();

  //MP = M  P_red[e]
  prodAB_ColMajor(M, MSph.ndof, MSph.ndof, P, MSph.ndof, MSph.elements[e].Nred, MP) ;
  
  //M_red = P_red[e]^* MP = P_red[e]^* M P_red 
  prodAB_ColMajor(adjtP, MSph.elements[e].Nred, MSph.ndof, MP, MSph.ndof, MSph.elements[e].Nred, M_red) ;
  
}


void compute_A_red_ColMajor(MKL_INT e, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P){

   /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    M_glob  : structure définissant la matrice de la projection
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres de la matrice M
    M_red   : conj(trans(P_red)) *  M * P_red
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           A_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    P       :=  P_red
    MP       := M * b
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  cplx *M = M_glob.Tab_A_loc[e].get_Aloc();

  //MP = M  P_red[e]
  prodAB_ColMajor(M, MSph.ndof, MSph.ndof, P, MSph.ndof, MSph.elements[e].Nred, MP) ;
  
  //M_red = P_red[e]^* MP = P_red[e]^* M P_red 
  prodAstarB_ColMajor(P, MSph.ndof, MSph.elements[e].Nred, MP, MSph.ndof, MSph.elements[e].Nred, M_red) ;
  
}



void compute_A_red_ColMajor(MKL_INT e, MKL_INT taille, A__skyline &Msky, cplx *Msky_mem, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP){

   /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    taille  : la taille au carrée de chaque bloc
    Msky    : structure définissant la taille de la matrice de la projection
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres de la matrice M
    M_red   : conj(trans(P_red)) *  M * P_red
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           A_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    P       :=  P_red
    AdjtP   := conj(trans(P_red))
    MP       := M * b
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  cplx *M;
  
  get_A__skyline_Block(Msky, Msky_mem, e, 0, taille, &M);

  //MP = M  P_red[e]
  prodAB_ColMajor(M, MSph.ndof, MSph.ndof, P, MSph.ndof, MSph.elements[e].Nred, MP) ;
  
  //M_red = P_red[e]^* MP = P_red[e]^* M P_red 
  prodAB_ColMajor(adjtP, MSph.elements[e].Nred, MSph.ndof, MP, MSph.ndof, MSph.elements[e].Nred, M_red) ;
  
}



void compute_F_red_ColMajor(MKL_INT e, cplx *B, cplx *B_red, MailleSphere &MSph, cplx *adjtP) {
  //Cette fonction calcule le vecteur réduit b_red à partir du vecteur b et de la matrice P_red

  /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    B       : le vecteur second membre sur l'élément
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres
    B_red   : conj(trans(P_red)) *  b
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           b_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    adjtP   :=  trans(conj(P_red))
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  //B_red = adjtP * B 
  prodMV_ColMajor(adjtP, MSph.elements[e].Nred, MSph.ndof, B, MSph.ndof, B_red) ;
  
}


void compute_F_red_ColMajor__(MKL_INT e, cplx *B, cplx *B_red, MailleSphere &MSph, cplx *P) {
  //Cette fonction calcule le vecteur réduit b_red à partir du vecteur b et de la matrice P_red

  /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    B       : le vecteur second membre sur l'élément
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres
    B_red   : conj(trans(P_red)) *  b
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           b_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    P   :=  la restriction de la matrice de passage sur e
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  //B_red = adjtP * B 
  prodMstarV_ColMajor(P, MSph.ndof, MSph.elements[e].Nred, B, MSph.ndof, B_red) ;
  
}



void reverseTo_originalVector_ColMajor(MKL_INT e, cplx *x, cplx *x_red, MailleSphere &MSph, cplx *P) {
   //Cette fonction calcule permet de retrouver le vecteur original x à partir du vecteur réduit x_red et de la matrice P_red

  /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    x       : le vecteur second membre sur l'élément
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres
    x_red   : le vecteur réduit (ou le vecteur x dans la base réduite P_red)
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           x
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    a       :=  P_red
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  //x = P * x_red
  prodMV_ColMajor(P, MSph.ndof, MSph.elements[e].Nred, x_red, MSph.elements[e].Nred, x);
}


/*---------------------------------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales_Volume_selfDecompose(VecN &U, A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc, taille =  MSph.ndof *  MSph.ndof;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
    
  MSph.taille_red = 0;
  
  cplx *AA = new cplx[taille];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[MSph.ndof];//Pour stocker les valeurs propres

   cplx *MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 

   cplx *P = new cplx[taille];//Pour stocker la matrice P_red[e]

   cplx *adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])


  for(e = 0; e < MSph.get_N_elements(); e++){
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(MSph.ndof, e, &B);

    cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
    auto t3 = std::chrono::high_resolution_clock::now();

    BuildVector_projection_Hetero_PP_VV(e, B, MSph);
    
    auto t4 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_F = t4 - t3;
    
    MSph.elements[e].temps_F = float_t_F.count();

    cout<<" temps de construction du vecteur : "<<MSph.elements[e].temps_F<<endl;

    
    cout<<"Décomposition et calcul de la taille du block réduit : "<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();

    compute_sizeBlocRed(e, 0, AA, w, M_glob, MSph);

    auto t6 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_cd = t6 - t5;

    auto tx = std::chrono::high_resolution_clock::now();
    MKL_INT r = M_glob.VerifyDecompositionPMP(e, 0, AA, w, MSph);
    
    if (r == 1) {
      MSph.SIZE[e] = MSph.taille_red;
      MSph.taille_red += MSph.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
    auto ty = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Txy = ty - tx;

    MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;

    MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
    
    cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<" SIZE["<<e<<"] = "<<MSph.SIZE[e]<<endl;

    cout<<" Décomposition de la matrice MP et stockage de PP_red et des valeurs propres :"<<endl;
    auto t7 = std::chrono::high_resolution_clock::now();
    
    DecompositionDeBloc_ET_Normalisation(e, 0, M_glob, P_red, LAMBDA, MSph, AA, w);
    
    auto t8 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_dp = t8 - t7;

    auto tu = std::chrono::high_resolution_clock::now();
    store_P_and_transpose_P(e, P, adjtP, P_red, MSph);
    auto tv = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Tuv = tv - tu;
  
    MatMN M_red;
    M_red.m = MSph.elements[e].Nred_Pro;
    M_red.n = MSph.elements[e].Nred_Pro;
    
    M_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = MSph.elements[e].Nred_Pro;
    u_red.Allocate();

    cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;
    auto t9 = std::chrono::high_resolution_clock::now();
    
    compute_A_red_ColMajor(e, M_glob, M_red.value, MSph, MP, P, adjtP);

    auto t10 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Ared = t10 - t9;
  
    cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;
    auto t11 = std::chrono::high_resolution_clock::now();
    
    compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);

    auto t12 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Fred = t12 - t11;

    cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;

    auto t13 = std::chrono::high_resolution_clock::now();

    //La Matrice et le vecteur seront écrasés
    M_red.Solve_by_CHOLESKY_ColMajor(u_red);

    auto t14 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_inv = t14 - t13;
    
    MSph.elements[e].temps_inv = float_t_inv.count();
    
    cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;

    auto t15 = std::chrono::high_resolution_clock::now();
    
    reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);

    auto t16 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_r = t16 - t15;
    
    //MSph.elements[e].temps_Total = float_Txy + float_Tuv + MSph.elements[e].temps_F + MSph.elements[e].temps_inv + float_t_cd.count() + float_t_dp.count() + float_t_Ared.count() + float_t_Fred.count() + float_t_r.count();
    
    cout<<"Libération de la mémoire :"<<endl;  
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }

  cout<<"Libération finale de la mémoire :"<<endl; 
  delete [] AA ; AA  =  nullptr;
  delete [] w ; w  =  nullptr;
  delete [] MP ; MP  =  nullptr;
  delete [] P ; P  =  nullptr;
  delete [] adjtP ; adjtP  =  nullptr;
}


void debugg_Volume_Projection(MailleSphere &MSph, const char chaine[]){

  VecN U;
  
  U.n = MSph.get_N_elements() * MSph.ndof;
  U.Allocate();

   VecN V;
  
  V.n = MSph.get_N_elements() * MSph.ndof;
  V.Allocate();

  VecN LAMBDA;
  
  LAMBDA.n = U.n;
  LAMBDA.Allocate();
  
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(1); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()
  
  A_skyline Q_red;
  Q_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  Q_red.set_n_inter(1); // fixe le nombre d'interactions 
  Q_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  Q_red.set_n_A(); // calcule le nombre de coeff a stocker
  Q_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()

  A__skyline Msky;
  
  cplx *Msky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Msky);
  
  Msky_mem = new cplx[Msky.n_A];
  
  for (MKL_INT i = 0; i < Msky.n_A; i++) {
    Msky_mem[i] = cplx(0.0,0.0);
  }
  
  A__skyline Psky;
  
  cplx *Psky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Psky);
  
  Psky_mem = new cplx[Psky.n_A];
  
  for (MKL_INT i = 0; i < Psky.n_A; i++) {
    Psky_mem[i] = cplx(0.0,0.0);
  }

  MKL_INT n_RHS = MSph.get_N_elements() * MSph.ndof;
  
  cplx *DELTA = new cplx[n_RHS];
  for(MKL_INT i = 0; i < n_RHS; i++)
    DELTA[i] = cplx(0.0, 0.0);
  
  BuildVolumeMetricMatrix_parallel_version(M_glob, MSph, chaine);
  


  BuildVolumeMetricMatrix_parallel_version(Msky, Msky_mem, MSph, chaine);
  

  
  #pragma omp parallel
  {
  collection_Solutions_locales_Volume_selfDecompose(U, M_glob, Q_red, LAMBDA, MSph);
  }

  
  collection_Solutions_locales_Volume_selfDecompose(V, Msky, Msky_mem, Psky, Psky_mem, DELTA, MSph);
  

  cplx val;
  double maxx = 0.0, minn = 1000.0;
  for(MKL_INT i = 0; i < n_RHS; i++){
    val = U.value[i] - V.value[i];
    maxx = max(maxx, abs(val));
    minn = min(minn, abs(val));
  }
  cout<<"maxx = "<<maxx<<" minn = "<<minn<<" U[0] = "<<U.value[0]<<" U[5] = "<<U.value[5]<<endl;
  //transform_hetero_ScalarAndVelocityError_to_VTK(U, MSph);
 
  
}


void store_P_and_transpose_P(MKL_INT e, cplx *a, cplx *b, A__skyline &Psky, cplx *Psky_mem,  MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc;
  
  cplx *PP_red;
  
  get_P__skyline_Block_red(Psky, Psky_mem, e, &PP_red, MSph);
  
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
   
    for (jloc = 0; jloc < MSph.elements[e].Nred; jloc++) { // numero de la colonne	  	
      
      a[jloc * MSph.ndof + iloc] = PP_red[jloc * MSph.ndof + iloc];
      
      b[iloc * MSph.elements[e].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
   
    }      
  }
}



void collection_Solutions_locales_Volume_selfDecompose(VecN &U, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc, taille =  MSph.ndof *  MSph.ndof;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
    
  MSph.taille_red = 0;
  
  cplx *AA = new cplx[taille];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[MSph.ndof];//Pour stocker les valeurs propres

   cplx *MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 

   cplx *P = new cplx[taille];//Pour stocker la matrice P_red[e]

   cplx *adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])

   cplx *adjointP = new cplx[taille];
   cplx *PD = new cplx[taille];
   cplx *PDP = new cplx[taille];
   

  for(e = 0; e < MSph.get_N_elements(); e++){
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(MSph.ndof, e, &B);

    cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
    auto t3 = std::chrono::high_resolution_clock::now();

    BuildVector_projection_Hetero_PP_VV(e, B, MSph);
    
    auto t4 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_F = t4 - t3;
    
    MSph.elements[e].temps_F = float_t_F.count();

    cout<<" temps de construction du vecteur : "<<MSph.elements[e].temps_F<<endl;

    
    cout<<"Décomposition et calcul de la taille du block réduit : "<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();

    compute_sizeBlocRed(e, taille, AA, w, Msky, Msky_mem, MSph);

    auto t6 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_cd = t6 - t5;

    auto tx = std::chrono::high_resolution_clock::now();
    MKL_INT r = VerifyDecompositionPMP(Msky, Msky_mem, e, taille, AA, w, MSph, adjointP, PD, PDP);
    
    if (r == 1) {
      MSph.SIZE[e] = MSph.taille_red;
      MSph.taille_red += MSph.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
    auto ty = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Txy = ty - tx;

    MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;

    MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
    
    cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<" SIZE["<<e<<"] = "<<MSph.SIZE[e]<<endl;

    cout<<" Décomposition de la matrice MP et stockage de PP_red et des valeurs propres :"<<endl;
    auto t7 = std::chrono::high_resolution_clock::now();
    
    DecompositionDeBloc_ET_Normalisation(e, taille, Msky, Msky_mem, Psky, Psky_mem, LAMBDA, MSph, AA, w);
    
    auto t8 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_dp = t8 - t7;

    auto tu = std::chrono::high_resolution_clock::now();
    store_P_and_transpose_P(e, P, adjtP, Psky, Psky_mem, MSph);
    auto tv = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Tuv = tv - tu;
  
    MatMN M_red;
    M_red.m = MSph.elements[e].Nred_Pro;
    M_red.n = MSph.elements[e].Nred_Pro;
    
    M_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = MSph.elements[e].Nred_Pro;
    u_red.Allocate();

    cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;
    auto t9 = std::chrono::high_resolution_clock::now();
    
    compute_A_red_ColMajor(e, taille, Msky, Msky_mem, M_red.value, MSph, MP, P, adjtP);

    auto t10 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Ared = t10 - t9;
  
    cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;
    auto t11 = std::chrono::high_resolution_clock::now();
    
    compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);

    auto t12 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Fred = t12 - t11;

    cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;

    auto t13 = std::chrono::high_resolution_clock::now();

    //La Matrice et le vecteur seront écrasés
    M_red.Solve_by_CHOLESKY_ColMajor(u_red);

    auto t14 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_inv = t14 - t13;
    
    MSph.elements[e].temps_inv = float_t_inv.count();
    
    cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;

    auto t15 = std::chrono::high_resolution_clock::now();
    
    reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);

    auto t16 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_r = t16 - t15;
    
    //MSph.elements[e].temps_Total = float_Txy + float_Tuv + MSph.elements[e].temps_F + MSph.elements[e].temps_inv + float_t_cd.count() + float_t_dp.count() + float_t_Ared.count() + float_t_Fred.count() + float_t_r.count();
    
    cout<<"Libération de la mémoire :"<<endl;  
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }

  cout<<"Libération finale de la mémoire :"<<endl; 
  delete [] AA ; AA  =  nullptr;
  delete [] w ; w  =  nullptr;
  delete [] MP ; MP  =  nullptr;
  delete [] P ; P  =  nullptr;
  delete [] adjtP ; adjtP  =  nullptr;
  delete [] PD ; PD  =  nullptr;
  delete [] PDP ; PDP  =  nullptr;
  delete [] adjointP ; adjointP  =  nullptr;
}


/*-------------------------------------------------------------------------------------------------------*/
void DecompositionDeBloc_ET_Normalisation_Sans_Troncature(MKL_INT e, MKL_INT l, A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph, cplx *AA, double *w) {
  //cette fonction decompose le bloc numero e sous la forme de M = P D P* avec P une matrice orthonormale et D contient les valeurs propres
  /*--------------------------------------------------------------- input -------------------------------------------------/
    e      : numero de l'élément
    l      : numero du bloc associé à l'élément
    A_glob : tableau contenant tous les blocs
    P_glob  : tableau pour stocker la base normalisée de vecteurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_glob :
 /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour clôner A_glob sur chaque élément
    P         : ........... P_glob ...................
    V         : ........... LAMBDA ..................
    w         : pour stocker toutes les valeurs propres du bloc de l'élément
    iloc, jloc: boucle sur les fonctions de bases
    /*---------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, ILOC, kloc, JLOC;
  
  cplx *P = P_glob.Tab_A_loc[e].get_Aloc();
  cplx *M = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + l].get_Aloc();
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    for (jloc = 0; jloc < MSph.ndof; jloc++){
      AA[jloc * MSph.ndof + iloc] = M[jloc * MSph.ndof + iloc];
    }
  }
  
  MKL_INT tt = LAPACKE_zheev( LAPACK_COL_MAJOR , 'V', 'L', MSph.ndof, AA, MSph.ndof, w);

  cplx *V;
  LAMBDA.get_F_elem(MSph.ndof, e, &V);

  ILOC = 0;
  for (iloc = MSph.ndof-1; iloc >= 0; iloc--){
    V[ILOC] = w[iloc];
    ILOC++;
  }
 
  //stockage de la base réduite associée à l'élément e
  ILOC = 0;
  for (iloc = 0; iloc < MSph.ndof; iloc++){
    JLOC = 0;
    for (jloc = MSph.ndof-1; jloc >= 0; jloc--){
      for (kloc = MSph.ndof-1; kloc >= 0; kloc--){
	if (kloc == jloc) {
	  P[JLOC * MSph.ndof + ILOC] = AA[jloc * MSph.ndof + iloc] /* (1.0 / sqrt(w[kloc]))*/;
	}
      }
      JLOC++;
    }
    ILOC++;
  }
  
}


void DecompositionGlobaleDesBlocs_ET_Normalisation_Sans_Troncature(A_skyline &A_glob, A_skyline &P_glob, VecN &LAMBDA, MailleSphere &MSph){
  //Cette fonction calcule et stocke la nouvelle base réduite de Galerkin discontinue vecteurs propre
  /*--------------------------------------------------------------- input -------------------------------------------------/
    A_glob : tableau contenant tous les blocs
    P_glob : tableau pour stocker la base réduite de vecteurs propres
    LAMBDA : .................... les N_red plus grandes valeurs propres 
    MSph   : le maillage
    /*------------------------------------------------------------- ouput -------------------------------------------------/
    P_glob :
    /*---------------------------------------------------------------------------------------------------------------------*/
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];
  double *D = new double[MSph.ndof];
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++){
    DecompositionDeBloc_ET_Normalisation_Sans_Troncature(e, 0, A_glob, P_glob, LAMBDA, MSph, AA, D);
  }
  
  delete [] AA; AA  =  nullptr;
  delete [] D; D  =  nullptr;
}

void produit_A_skyline_P_skyline_Sans_Troncature(A_skyline &A_glob, A_skyline &P_glob, A_skyline &R_glob, MailleSphere &Msph) {
  //Cette fonction calcule la matrice réduite R_glob = P_glob^* A * P_glob
  /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    A_glob    : tableau skyline contenant la matrice originale du grand système linéaire
    P_glob     : ....................... les vecteurs propres associées aux valeurs de A_glob 
               
    R_glob     : Trans(Conj(P_glob)) * A * P_glob
    Msph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    R_glob     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    AA        : pour stocker l'adresse des blocs de A_glob sur chaque élément
    R         : .................................. R_glob ...................
    P         : .................................. P_glob ...................
    i         : boucle sur les éléments
    j         : .............. voisins de chaque élément
    iloc, jloc: boucle sur les fonctions de bases

    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, i, j, f, taille;

 
  cout<<"------------------------------------------- Transformation de la matrice A en R_glob---------------------------------------------"<<endl;

  cout<<"-------------------------------------------------------- Structure diagonale ----------------------------------------------------"<<endl;

  taille = Msph.ndof * Msph.ndof;
  
  cplx *a = new cplx[taille];
  
  for(i = 0; i < Msph.get_N_elements(); i++){

    cplx *R, *AA, *Pi;
    
    initialize_tab(taille, a);
    
    //On stocke l'adresse du bloc i de A_glob dans AA
    AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
    
    //On stocke l'adresse du bloc i de R_glob dans R
    R = R_glob.Tab_A_loc[i * R_glob.get_n_inter() + 0].get_Aloc();
    
    //On stocke l'adresse du bloc i de P_glob dans P
    Pi = P_glob.Tab_A_loc[i].get_Aloc();
    
    //a = A(i, i)  P_glob[i]
    prodAB_ColMajor(AA, Msph.ndof, Msph.ndof, Pi, Msph.ndof, Msph.ndof, a) ;
    
    //R = P_glob[i]^* a = P_glob[i]^* A(i, i) P_glob 
    prodAstarB_ColMajor(Pi, Msph.ndof, Msph.ndof, a, Msph.ndof, Msph.ndof, R) ;
    
  }
  
  
  cout<<"-------------------------------------------------------- Autres Structures ------------------------------------------------------"<<endl;
  for(i = 0; i < Msph.get_N_elements(); i++){

    //On stocke l'adresse du bloc i de P_glob dans P
    cplx *Pi = P_glob.Tab_A_loc[i].get_Aloc();

    for(j = 0; j < 4; j++){
      
      if (Msph.elements[i].voisin[j] >= 0) {

	f = Msph.elements[i].voisin[j];

	cplx *R, *AA, *Pf;
	
	initialize_tab(taille, a);
	
	
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + (j+1)].get_Aloc();
	
	R = R_glob.Tab_A_loc[i * R_glob.get_n_inter() + (j+1)].get_Aloc();

	//On stocke l'adresse du bloc f de P_glob dans P
	Pf = P_glob.Tab_A_loc[f].get_Aloc();
	
	//a = A(i, f) P_glob[f]
	prodAB_ColMajor(AA, Msph.ndof, Msph.ndof, Pf, Msph.ndof, Msph.ndof, a) ;
	
	//R = P_glob[i]^* a = P_glob[i]^* A(i, f) P_glob[f]
	prodAstarB_ColMajor(Pi, Msph.ndof, Msph.ndof, a, Msph.ndof, Msph.ndof, R) ;

      }
    }
    
  }


   cout<<"-------------------------------------------------------- Fin de la construction ----------------------------------------------------------"<<endl;
   delete [] a; a  =  nullptr;
}


void produit_P_skyline_RightHandSide_Sans_Troncature(A_skyline &P_glob, VecN &B_old, VecN &B_new, MailleSphere &MSph){
    /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    P_glob     : tableau des vecteurs propres associées aux valeurs propres de 
                 la matrice A_glob du systeme linéaire à résoudre 
  
    B_old      : l'original second membre
    B_new      : trans(conj(P_glob)) \cdot B
    MSph       : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    B_new      :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    b_old      : pour stocker l'adresse du bloc i de B_old sur chaque élément
    b_new      : ................................... B_new ..............
    i         : boucle sur les éléments
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  cplx alpha, beta;

  alpha = cplx(1.0, 0.0);
  beta = cplx(1.0, 0.0);
  MKL_INT i;

  for(i = 0; i < MSph.get_N_elements(); i++){

    cplx *b_new, *b_old, *Pi;
    
    //clônage sur l'emplacement i du tableau B_old; b_old = B_old[i]
    B_old.get_F_elem(MSph.ndof, i, &b_old);
    
    //clônage sur l'emplacement i du tableau B_new; b_new = B_new[i]
    B_new.get_F_elem(MSph.ndof, i, &b_new);
    
    //On stocke l'adresse du bloc f de P_glob dans P
    Pi = P_glob.Tab_A_loc[i].get_Aloc();
    
    
    //b_new = Pi^*  b_old 
    prodMstarV_ColMajor(Pi, MSph.ndof, MSph.ndof, alpha, b_old, MSph.ndof, b_new, beta);
  
  }
  
}


void produit_P_skyline_Solution_Sans_Troncature(A_skyline &P_glob, VecN &X_new, VecN &X_old, MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    P_glob     : tableau des vecteurs propres associées aux valeurs propres de
                 A_glob la matrice du système linéaire à résoudre 
    X_new     : P_glob \cdot B
    X_old     : la solution originale
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    X_old     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    x_new     : pour stocker l'adresse de X_new sur chaque élément
    x_old     : ......................... X_old ..............
    i         : boucle sur les éléments
    iloc, jloc: boucle sur les fonctions de bases
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, iloc, jloc;
  cplx alpha;

  alpha = cplx(1.0, 0.0);

  for(i = 0; i < MSph.get_N_elements(); i++){

    cplx *x_new, *x_old, *Pi;
    
    //clônage sur l'emplacement i du tableau X_old
    X_old.get_F_elem(MSph.ndof, i, &x_old);
    
    //clônage sur l'emplacement i du tableau X_new
    X_new.get_F_elem(MSph.ndof, i, &x_new);

    //On stocke l'adresse du bloc i de P_glob dans Pi
    Pi = P_glob.Tab_A_loc[i].get_Aloc();

    //x_old = Pi * x_new
    prodMV_ColMajor(Pi, MSph.ndof, MSph.ndof, alpha, x_new, MSph.ndof, x_old, alpha) ;
  }
  
}

void Element::compute_eps_max_For_Eigenvalue_Filtering(double eps, MKL_INT s){
  /*----------------------------------------------------- input --------------------------------------
    eps    : le seuil de filtrage des valeurs propres
    s      : pour déterminer le choix de filtrage à considérer
             si s = 0, c'est alors une décomposition des blocs de la matrice associée
	         au métrique surfacique ainsi que le filtrage des valeurs propres de chaque bloc
	     si s != 0, on traite le cas ou la matrice est associée au métrique volumique
    /----------------------------------------------------- output -------------------------------------*/
  if (s == 0) {
    facto_volume = pow(Vol, 2.0 / 3.0);
    eps_max = eps * facto_volume;
  }
  else {
    facto_volume = Vol;
    eps_max = eps * Vol;
  }
}

void Element::compute_eps_of_Tikhonov_regulator(double eps, MKL_INT s){
  /*----------------------------------------------------- input --------------------------------------
    eps    : le régulateur de Tikhonov associé au volume de l'élément
    s      : pour déterminer le choix de ce régulateur
             si s = 0, c'est alors de la forme eps * |T|^{2/3}
	     sinon, c'est de la forme eps * |T|
    /----------------------------------------------------- output -------------------------------------*/
  if (s == 0) {
    eps_max_Tikhonov = eps * pow(Vol, 2.0 / 3.0);
  }
  else {
    eps_max_Tikhonov = eps * Vol;
  }
}
/*-------------------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales_Areal_selfDecompose(VecN &U, A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc, taille =  MSph.ndof *  MSph.ndof;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
    
  MSph.taille_red = 0;
  
  cplx *AA = new cplx[MSph.ndof * MSph.ndof];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[MSph.ndof];//Pour stocker les valeurs propres

   cplx *MP = new cplx[MSph.ndof * MSph.ndof];//Pour stocker la matrice M_glob[e] * P_red[e] 

   cplx *P = new cplx[MSph.ndof * MSph.ndof];//Pour stocker la matrice P_red[e]

   cplx *adjtP = new cplx[MSph.ndof * MSph.ndof];//Pour stocker la matrice conj(P_red[e])

   
  for(e = 0; e < MSph.get_N_elements(); e++){
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(MSph.ndof, e, &B);

    cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
    auto t3 = std::chrono::high_resolution_clock::now();

    BuildVector_AreaProjection(e, B, MSph);
    
    auto t4 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_F = t4 - t3;
    
    MSph.elements[e].temps_F = float_t_F.count();

    cout<<" temps de construction du vecteur : "<<MSph.elements[e].temps_F<<endl;

    
    cout<<"Décomposition et calcul de la taille du block réduit : "<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();

    compute_sizeBlocRed(e, 0, AA, w, M_glob, MSph);

    auto t6 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_cd = t6 - t5;

    auto tx = std::chrono::high_resolution_clock::now();
    MKL_INT r = M_glob.VerifyDecompositionPMP(e, 0, AA, w, MSph);
    
    if (r == 1) {
      MSph.SIZE[e] = MSph.taille_red;
      MSph.taille_red += MSph.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
    auto ty = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Txy = ty - tx;

    MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;

    MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
    
    cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<" SIZE["<<e<<"] = "<<MSph.SIZE[e]<<endl;

    cout<<" Décomposition de la matrice MP et stockage de PP_red et des valeurs propres :"<<endl;
    auto t7 = std::chrono::high_resolution_clock::now();
    
    DecompositionDeBloc_ET_Normalisation(e, 0, M_glob, P_red, LAMBDA, MSph, AA, w);
    
    auto t8 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_dp = t8 - t7;

    auto tu = std::chrono::high_resolution_clock::now();
    store_P_and_transpose_P(e, P, adjtP, P_red, MSph);
    auto tv = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Tuv = tv - tu;
  
    MatMN M_red;
    M_red.m = MSph.elements[e].Nred_Pro;
    M_red.n = MSph.elements[e].Nred_Pro;
    
    M_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = MSph.elements[e].Nred_Pro;
    u_red.Allocate();

    cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;
    auto t9 = std::chrono::high_resolution_clock::now();
    
    compute_A_red_ColMajor(e, M_glob, M_red.value, MSph, MP, P, adjtP);

    auto t10 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Ared = t10 - t9;
  
    cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;
    auto t11 = std::chrono::high_resolution_clock::now();
    
    compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);

    auto t12 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Fred = t12 - t11;

    cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;

    auto t13 = std::chrono::high_resolution_clock::now();

    //La Matrice et le vecteur seront écrasés
    M_red.Solve_by_CHOLESKY_ColMajor(u_red);

    auto t14 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_inv = t14 - t13;
    
    MSph.elements[e].temps_inv = float_t_inv.count();
    
    cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;

    auto t15 = std::chrono::high_resolution_clock::now();
    
    reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);

    auto t16 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_r = t16 - t15;
    
    //MSph.elements[e].temps_Total = float_Txy + float_Tuv + MSph.elements[e].temps_F + MSph.elements[e].temps_inv + float_t_cd.count() + float_t_dp.count() + float_t_Ared.count() + float_t_Fred.count() + float_t_r.count();
    
    cout<<"Libération de la mémoire :"<<endl;  
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }

  cout<<"Libération finale de la mémoire :"<<endl; 
  delete [] AA ; AA  =  nullptr;
  delete [] w ; w  =  nullptr;
  delete [] MP ; MP  =  nullptr;
  delete [] P ; P  =  nullptr;
  delete [] adjtP ; adjtP  =  nullptr;
}
/*----------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales_Areal_selfDecompose(VecN &U, A__skyline &Msky, cplx *Msky_mem, A__skyline &Psky, cplx *Psky_mem, cplx *LAMBDA, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, iloc, taille =  MSph.ndof *  MSph.ndof;
  
  cplx *B;//pointeur pour clôner vers une adresse de U
    
  MSph.taille_red = 0;
  
  cplx *AA = new cplx[taille];//Pour stocker la matrice MP et éviter
  //d'écraser cette dernière. AA contiendra les vecteurs propes après décomposition
  
  double *w = new double[MSph.ndof];//Pour stocker les valeurs propres

   cplx *MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 

   cplx *P = new cplx[taille];//Pour stocker la matrice P_red[e]

   cplx *adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])

   cplx *adjointP = new cplx[taille];
   cplx *PD = new cplx[taille];
   cplx *PDP = new cplx[taille];

#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    
    //on clône l'emplacement U correspondant à l'élément e
    U.get_F_elem(MSph.ndof, e, &B);

    cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
    auto t3 = std::chrono::high_resolution_clock::now();

    BuildVector_AreaProjection(e, B, MSph);
    
    auto t4 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_F = t4 - t3;
    
    MSph.elements[e].temps_F = float_t_F.count();

    cout<<" temps de construction du vecteur : "<<MSph.elements[e].temps_F<<endl;

    
    cout<<"Décomposition et calcul de la taille du block réduit : "<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();

    compute_sizeBlocRed(e, taille, AA, w, Msky, Msky_mem, MSph);

    auto t6 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_cd = t6 - t5;

    auto tx = std::chrono::high_resolution_clock::now();
    MKL_INT r = VerifyDecompositionPMP(Msky, Msky_mem, e, taille, AA, w, MSph, adjointP, PD, PDP);
    
    if (r == 1) {
      MSph.SIZE[e] = MSph.taille_red;
      MSph.taille_red += MSph.elements[e].Nred;
    }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }
    auto ty = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Txy = ty - tx;

    MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;

    MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
    
    cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<" SIZE["<<e<<"] = "<<MSph.SIZE[e]<<endl;

    cout<<" Décomposition de la matrice MP et stockage de PP_red et des valeurs propres :"<<endl;
    auto t7 = std::chrono::high_resolution_clock::now();
    
    DecompositionDeBloc_ET_Normalisation(e, taille, Msky, Msky_mem, Psky, Psky_mem, LAMBDA, MSph, AA, w);
    
    auto t8 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_dp = t8 - t7;

    auto tu = std::chrono::high_resolution_clock::now();
    store_P_and_transpose_P(e, P, adjtP, Psky, Psky_mem, MSph);
    auto tv = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_Tuv = tv - tu;
  
    MatMN M_red;
    M_red.m = MSph.elements[e].Nred_Pro;
    M_red.n = MSph.elements[e].Nred_Pro;
    
    M_red.Allocate_MatMN();
    
    VecN u_red;
    u_red.n = MSph.elements[e].Nred_Pro;
    u_red.Allocate();

    cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;
    auto t9 = std::chrono::high_resolution_clock::now();
    
    compute_A_red_ColMajor(e, taille, Msky, Msky_mem, M_red.value, MSph, MP, P, adjtP);

    auto t10 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Ared = t10 - t9;
  
    cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;
    auto t11 = std::chrono::high_resolution_clock::now();
    
    compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);

    auto t12 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_Fred = t12 - t11;

    cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;

    auto t13 = std::chrono::high_resolution_clock::now();

    //La Matrice et le vecteur seront écrasés
    M_red.Solve_by_CHOLESKY_ColMajor(u_red);

    auto t14 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_inv = t14 - t13;
    
    MSph.elements[e].temps_inv = float_t_inv.count();
    
    cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;

    auto t15 = std::chrono::high_resolution_clock::now();
    
    reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);

    auto t16 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_t_r = t16 - t15;
    
    //MSph.elements[e].temps_Total = float_Txy + float_Tuv + MSph.elements[e].temps_F + MSph.elements[e].temps_inv + float_t_cd.count() + float_t_dp.count() + float_t_Ared.count() + float_t_Fred.count() + float_t_r.count();
    
    cout<<"Libération de la mémoire :"<<endl;  
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
  }

  cout<<"Libération finale de la mémoire :"<<endl; 
  delete [] AA ; AA  =  nullptr;
  delete [] w ; w  =  nullptr;
  delete [] MP ; MP  =  nullptr;
  delete [] P ; P  =  nullptr;
  delete [] adjtP ; adjtP  =  nullptr;
  delete [] PD ; PD  =  nullptr;
  delete [] PDP ; PDP  =  nullptr;
  delete [] adjointP ; adjointP  =  nullptr;
}
/*----------------------------------------------------------------------------------------------------------------------------*/

void Get_New_Normalised_Trefftz_Basis(A_skyline &M_glob, A_skyline &P_red, VecN &LAMBDA, MailleSphere &MSph){
  
  MKL_INT e, r, taille =  MSph.ndof *  MSph.ndof;
  
  cplx *AA;//Pour stocker chaque bloc  et éviter
  //d'écraser ce dernier. AA contiendra les vecteurs propes après décomposition
  
  double *w;//Pour stocker les valeurs propres
    
#pragma omp for schedule(runtime) private(AA, w, e, r)
  for(e = 0; e < MSph.get_N_elements(); e++) {
    AA = new cplx[taille];
    w = new double[MSph.ndof];
    compute_sizeBlocRed(e, 0, AA, w, M_glob, MSph);

    r = M_glob.VerifyDecompositionPMP(e, 0, AA, w, MSph);

     if (r == 1) {      
       MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;
       MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
     }
    
    else {
      cout<<" Probleme au niveau de la decomposition du bloc numero "<<e<<" "<<endl;
      exit(EXIT_FAILURE);
    }

    DecompositionDeBloc_ET_Normalisation(e, 0, M_glob, P_red, LAMBDA, MSph, AA, w);

    delete [] AA; AA =  nullptr;
    delete [] w; w =  nullptr;
  }
  

}


void compute_sizeOf_reducedVector(MailleSphere &MSph){
  MKL_INT e;
  MSph.taille_red = 0;
  for(e = 0; e < MSph.get_N_elements(); e++){
    MSph.SIZE[e] = MSph.taille_red;
    MSph.taille_red += MSph.elements[e].Nred;
  }
}


void debugg_Decomposition_to_build_New_Normalised_Trefftz_Basis(MailleSphere &MSph, const char chaine[]){
  double divide = 1.0 /1000.0;
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(1); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  
 
  // //Tableau skyline des vecteurs propres réduites
  A_skyline P_red;
  P_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  P_red.set_n_inter(1); // fixe le nombre d'interactions 
  P_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  P_red.set_n_A(); // calcule le nombre de coeff a stocker
  P_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()

   // //Tableau skyline des vecteurs propres réduites
  A_skyline Q_red;
  Q_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  Q_red.set_n_inter(1); // fixe le nombre d'interactions 
  Q_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  Q_red.set_n_A(); // calcule le nombre de coeff a stocker
  Q_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	Q_glob.print_info()
  cout<<"ASSEMBALGE de M_glob "<<endl;
  MKL_INT e = 1, taille = MSph.ndof * MSph.ndof;
  BuildVolumeMetricMatrix_parallel_version(M_glob, MSph, chaine);
  
  VecN LAMBDA;
  cout<<" Calcul des espaces reduites de Galerkin en sequentiel "<<endl;
  LAMBDA.n = compute_sizeBlocRed_parallel_version(M_glob, MSph);
  
  LAMBDA.Allocate();

  //cplx *AA = new cplx[taille];
  //double *D = new double[MSph.ndof];
  //DecompositionDeBloc_ET_Normalisation(e, 0, M_glob, P_red, LAMBDA, MSph, AA, D);
  auto t1 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs_ET_Normalisation(M_glob, P_red, LAMBDA, MSph);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_seq = t2 - t1;
  
  cout<<" temps de calcul des espaces réduites en sequentiel : "<<float_seq.count() * divide<<" secs"<<endl;
  
 VecN DELTA;
  cout<<" Calcul des espaces reduites de Galerkin en parallel "<<endl;
 
  DELTA.n = LAMBDA.n;
  
  DELTA.Allocate();
  /*
  cplx *AA2 = new cplx[taille];
  double *D2 = new double[MSph.ndof];
  cplx *P_loc = Q_red.Tab_A_loc[e].get_Aloc();
  cplx *A_loc = M_glob.Tab_A_loc[e].get_Aloc();
  cplx *V;
  get_F_elemRed(e, &V, DELTA, MSph);
  DecompositionDeBloc_ET_Normalisation(e, taille, A_loc, P_loc, V, MSph, AA2, D2);*/
  auto t3 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs_ET_Normalisation_parallel_version(M_glob, Q_red, DELTA, MSph);
  auto t4 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_para = t4 - t3;
    
  cout<<" temps de calcul des espaces reduites en parallel : "<<float_para.count() * divide<<" secs"<<endl;
  double maxx_lambda = 0.0, minn_lambda  = 1000.0;
  double maxx = 0.0, minn  = 1000.0;
  double maxx_e = 0.0, minn_e  = 1000.0;
  MKL_INT l, iloc, jloc;

  cplx val;

  for(e = 0; e < MSph.get_N_elements(); e++) {
    cplx *P, *Q;
    P = P_red.Tab_A_loc[e].get_Aloc();
    Q = Q_red.Tab_A_loc[e].get_Aloc();
    maxx_e = 0.0, minn_e  = 1000.0;
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      for(jloc = 0; jloc < MSph.elements[e].Nred; jloc++) {
	val = P[jloc * MSph.ndof + iloc] - Q[jloc * MSph.ndof + iloc];
	maxx_e = max(maxx_e, abs(val));
	minn_e = min(minn_e, abs(val));
      }
    }
    //cout<<" maxx_e = "<<maxx_e<<" minn_e = "<<minn_e<<endl;
    maxx = max(maxx, maxx_e);
    minn = min(minn, minn_e);
  }
  
  for(e = 0; e < MSph.get_N_elements(); e++) {
    cplx  *D, *W;
    get_F_elemRed(e, &D, LAMBDA, MSph);
    get_F_elemRed(e, &W, DELTA, MSph);
    for(iloc = 0; iloc < MSph.elements[e].Nred; iloc++) {
      maxx_lambda = max(maxx_lambda, abs(D[iloc] - W[iloc]));
      minn_lambda = min(minn_lambda, abs(D[iloc] - W[iloc]));
    }
  }
  cout<<" LAMBDA.n = "<<LAMBDA.n<<" DELTA.n = "<<DELTA.n<<endl;
  cout<<" maxx = "<<maxx<<" minn = "<<minn<<" maxx_lambda = "<<maxx_lambda<<" minn_lambda = "<<minn_lambda<<endl;

}
/*--------------------------------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales_Areal_selfDecompose_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, taille =  MSph.ndof *  MSph.ndof;

  cplx *MP, *P, *adjtP;

   MatMN M_red;
   VecN u_red;
#pragma omp parallel private(MP, P, adjtP, e, M_red, u_red) 
  {
    MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 
    
    P = new cplx[taille];//Pour stocker la matrice P_red[e]
    
    adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])

   
    M_red.m = MSph.ndof;
    M_red.n = MSph.ndof;
    
    M_red.Allocate_MatMN();
    
    
    u_red.n = MSph.ndof;
    u_red.Allocate();
    
#pragma omp for schedule(runtime) 
    for(e = 0; e < MSph.get_N_elements(); e++){

      initialize_tab(taille, M_red.value);
      initialize_tab(MSph.ndof, u_red.value);

      M_red.m = MSph.elements[e].Nred;
      M_red.n = MSph.elements[e].Nred;
      
      u_red.n = MSph.elements[e].Nred;
      
      cplx *B;//pointeur pour clôner vers une adresse de U
      
      //on clône l'emplacement U correspondant à l'élément e
      U.get_F_elem(MSph.ndof, e, &B);
      
      cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
      auto t1 = std::chrono::high_resolution_clock::now();
      
      BuildVector_AreaProjection(e, B, MSph);
      
      auto t2 = std::chrono::high_resolution_clock::now();
      
      std::chrono::duration<double, std::milli> float_t_F = t2 - t1;
      
      MSph.elements[e].temps_F = float_t_F.count();
      
      cout<<" temps de construction du vecteur : "<<MSph.elements[e].temps_F<<endl;
      
      MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;
      
      MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
      
      cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<endl;
      
      auto tu = std::chrono::high_resolution_clock::now();
      store_P_and_transpose_P(e, P, adjtP, P_red, MSph);
      auto tv = std::chrono::high_resolution_clock::now();
      
      std::chrono::duration<double, std::milli> float_Tuv = tv - tu;
      
      cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;
      auto t3 = std::chrono::high_resolution_clock::now();
      
      compute_A_red_ColMajor(e, M_glob, M_red.value, MSph, MP, P, adjtP);
      
      auto t4 = std::chrono::high_resolution_clock::now();
      
      std::chrono::duration<double, std::milli> float_t_Ared = t4 - t3;
      
      cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;
      auto t5 = std::chrono::high_resolution_clock::now();
      
      compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);
      
      auto t6 = std::chrono::high_resolution_clock::now();
      
      std::chrono::duration<double, std::milli> float_t_Fred = t6 - t5;
      
      cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;
      
      auto t7 = std::chrono::high_resolution_clock::now();
      
      //La Matrice et le vecteur seront écrasés
      M_red.Solve_by_CHOLESKY_ColMajor(u_red);
      
      auto t8 = std::chrono::high_resolution_clock::now();
      
      std::chrono::duration<double, std::milli> float_t_inv = t8 - t7;
      
      MSph.elements[e].temps_inv = float_t_inv.count();
      
      cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;
      
      auto t9 = std::chrono::high_resolution_clock::now();
      
      reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);
      
      auto t10 = std::chrono::high_resolution_clock::now();
   
      std::chrono::duration<double, std::milli> float_t_r = t10 - t9;
      
      //MSph.elements[e].temps_Total = float_Txy + float_Tuv + MSph.elements[e].temps_F + MSph.elements[e].temps_inv + float_t_cd.count() + float_t_dp.count() + float_t_Ared.count() + float_t_Fred.count() + float_t_r.count();
     
    }
    cout<<"Libération finale de la mémoire :"<<endl; 
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
    delete [] MP ; MP  =  nullptr;
    delete [] P ; P  =  nullptr;
    delete [] adjtP ; adjtP  =  nullptr;
  }
   
}
/*--------------------------------------------------------------------------------------------------*/
void collection_Solutions_locales_Volume_selfDecompose_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, taille =  MSph.ndof *  MSph.ndof;

  cplx *MP, *P, *adjtP, *B;
  MatMN M_red;
  VecN u_red;
  
#pragma omp parallel private(MP, P, adjtP, e, M_red, u_red, B)
  {
    
    MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 
    
    P = new cplx[taille];//Pour stocker la matrice P_red[e]
    
    adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])
    
    M_red.m = MSph.ndof;
    M_red.n = MSph.ndof;
    
    M_red.Allocate_MatMN();
    
    
    u_red.n = MSph.ndof;
    u_red.Allocate();
    
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++){

      initialize_tab(taille, M_red.value);
      initialize_tab(MSph.ndof, u_red.value);


      M_red.m = MSph.elements[e].Nred;
      M_red.n = MSph.elements[e].Nred;

      u_red.n = MSph.elements[e].Nred;
    
      //on clône l'emplacement U correspondant à l'élément e
      U.get_F_elem(MSph.ndof, e, &B);
      
      cout<<" construction du vecteur second membre de la projection de PP' + VV' sur l'élément "<<e<<" : "<<endl;
      
      BuildVector_projection_Hetero_PP_VV(e, B, MSph);
      
      MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;
      
      MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
      
      cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<endl;
      
      store_P_and_transpose_P(e, P, adjtP, P_red, MSph);
      
      cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;    
      
      compute_A_red_ColMajor(e, M_glob, M_red.value, MSph, MP, P, adjtP);         
      
      cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;    
      
      compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);
                 
      cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;
          
      //La Matrice et le vecteur seront écrasés
      M_red.Solve_by_CHOLESKY_ColMajor(u_red);          
      
      cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;
                  
      reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);
     
    }
    cout<<"Libération finale de la mémoire :"<<endl; 
    delete [] M_red.value; M_red.value  =  nullptr;
    delete [] u_red.value; u_red.value  =  nullptr;
    delete [] MP ; MP  =  nullptr;
    delete [] P ; P  =  nullptr;
    delete [] adjtP ; adjtP  =  nullptr;
    
  }
}
/*-----------------------------------------------------------------------------------------------------*/
void TEST_SOLUTION_PROJECTEE(MKL_INT filtre, MailleSphere &MSph){
  MKL_INT e, iloc;

  double divide = 1.0/ 1000.0;
  auto t1 = std::chrono::high_resolution_clock::now();
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(1); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  
  auto t2 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_Msky = t2 - t1;
  
  cout<<" temps Allocation de la matrice globale de la projection : "<<float_A_Msky.count() * divide<<" secs"<<endl;
  
  auto t3 = std::chrono::high_resolution_clock::now();
  // //Tableau skyline des vecteurs propres réduites
  A_skyline P_red;
  P_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  P_red.set_n_inter(1); // fixe le nombre d'interactions 
  P_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  P_red.set_n_A(); // calcule le nombre de coeff a stocker
  P_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()
  
  auto t4 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_Pred = t4 - t3;

  cout<<" temps Allocation du tableau des matrices de passage des blocs de la matrice globale de la projection: "<<float_A_Pred.count() * divide<<" secs"<<endl;
  
  e = 10;
  
  VecN U;
  U.n = MSph.get_N_elements() * MSph.ndof;
  U.Allocate();

  if (filtre == 0) {
    cout<<"Assemblage de la matrice globale de la projection surfacique:"<<endl;
    auto t_UWVF_1 = std::chrono::high_resolution_clock::now();
    BuildArealMetricMatrix_parallel_version(M_glob, MSph, "double");
    auto t_UWVF_2 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_UWVF = t_UWVF_2 - t_UWVF_1;
    
    cout<<" temps d'assemblage de la matrice globale de la projection surfacique: "<<float_UWVF.count() * divide<<" secs"<<endl;

    cout<<"calcul de vecteur global de la projection surfacique:"<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();
    BuildVector_Of_Areal_Projection_parallel_version(U.value, MSph);
    auto t6 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_U = t6 - t5;
    
    cout<<" temps de calcul du vecteur global de la projection surfacique: "<<float_U.count() * divide<<" secs"<<endl;
    
  }
  else{
    cout<<"Assemblage de la matrice globale de la projection volumique:"<<endl;
    auto t_UWVF_1 = std::chrono::high_resolution_clock::now();
    BuildVolumeMetricMatrix_parallel_version(M_glob, MSph, "double");
    auto t_UWVF_2 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_UWVF = t_UWVF_2 - t_UWVF_1;
    
    cout<<" temps d'assemblage de la matrice globale de la projection volumique: "<<float_UWVF.count() * divide<<" secs"<<endl;

    cout<<"calcul de vecteur global de la projection volumique:"<<endl;
    auto t5 = std::chrono::high_resolution_clock::now();
    BuildVector_Of_Volume_Projection_parallel_version(U.value, MSph);
    auto t6 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_U = t6 - t5;
    
    cout<<" temps de calcul du vecteur global de la projection volumique: "<<float_U.count() * divide<<" secs"<<endl;
  }
  auto tN_red_1 = std::chrono::high_resolution_clock::now();
  VecN LAMBDA;
  LAMBDA.n = compute_sizeBlocRed_parallel_version(M_glob, MSph);
  LAMBDA.Allocate();
  auto tN_red_2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_N_red = tN_red_2 - tN_red_1;
  
  cout<<" temps de calcul des dimensions des bases reduites : "<<float_N_red.count() * divide<<" secs"<<endl;
  
  auto tbasis_1 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs_ET_Normalisation(M_glob, P_red, LAMBDA, MSph);
  auto tbasis_2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_basis = tbasis_2 - tbasis_1;
  
  cout<<" temps de construction des bases reduites: "<<float_basis.count() * divide<<" secs"<<endl;
  
  
  auto t7 = std::chrono::high_resolution_clock::now();
  collection_Solutions_locales_parallel_version(U, M_glob, P_red, MSph); 
  auto t8 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_Sl = t8 - t7;
  
  cout<<" temps de la projection: "<<float_Sl.count() * divide<<" secs"<<endl;

  cout<<"Calcul des normes L2 en pression et en vitesse : "<<endl;
  if (filtre == 0) {
    auto t29 = std::chrono::high_resolution_clock::now();
    double L2P_ex = norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
    auto t30 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_ex = t30 - t29;

    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<float_L2P_ex.count() * divide<<" secs"<<endl;
    
    auto t31 = std::chrono::high_resolution_clock::now();
    double L2V_ex = norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
    auto t32 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_ex = t32 - t31;
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<float_L2V_ex.count() * divide<<" secs"<<endl;

    auto t33 = std::chrono::high_resolution_clock::now();
    double L2P_err =  norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_parallel_version(U.value, MSph);
    auto t34 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_err = t34 - t33;
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<float_L2P_err.count() * divide<<" secs"<<endl;
    
    auto t35 = std::chrono::high_resolution_clock::now();
    double L2V_err = norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_parallel_version(U.value, MSph);
    auto t36 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_err = t36 - t35;
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<float_L2V_err.count() * divide<<" secs"<<endl;

    double P_ex_inf =  norme_inf_Bessel(MSph);
    
    double P_err_inf = norme_inf_erreur(U.value, MSph);
    
  cout<<" ||P_ex - P_h||_{L2} = "<<L2P_err<<" ||V_ex - V_h||_{L2} = "<<L2V_err<<" ||P_ex - P_h||_{inf} = "<<P_err_inf<<endl;

  
  cout<<" ||P_ex - P_h||_{L2} / ||P_ex||_{L2} = "<<L2P_err / (double) L2P_ex <<" ||V_ex - V_h||_{L2} / ||V_ex||_{L2} = "<<L2V_err / (double) L2V_ex<<" ||P_ex - P_h||_{inf} / ||P_ex||_{inf} = "<<P_err_inf / (double) P_ex_inf<<endl;
    }
    else{
       auto t29 = std::chrono::high_resolution_clock::now();
    double L2P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
    auto t30 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_ex = t30 - t29;

    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<float_L2P_ex.count() * divide<<" secs"<<endl;
    
    auto t31 = std::chrono::high_resolution_clock::now();
    double L2V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
    auto t32 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_ex = t32 - t31;
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<float_L2V_ex.count() * divide<<" secs"<<endl;

    auto t33 = std::chrono::high_resolution_clock::now();
    double L2P_err =  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(U.value, MSph);
    auto t34 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2P_err = t34 - t33;
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<float_L2P_err.count() * divide<<" secs"<<endl;
    
    auto t35 = std::chrono::high_resolution_clock::now();
    double L2V_err = norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(U.value, MSph);
    auto t36 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_L2V_err = t36 - t35;
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<float_L2V_err.count() * divide<<" secs"<<endl;

    double P_ex_inf =  norme_inf_Bessel(MSph);
    
    double P_err_inf = norme_inf_erreur(U.value, MSph);

    cout<<" valeur du filtre constant = "<<MSph.eps_0<<endl;
    cout<<" ||P_ex - P_h||_{L2} = "<<L2P_err<<" ||V_ex - V_h||_{L2} = "<<L2V_err<<" ||P_ex - P_h||_{inf} = "<<P_err_inf<<endl;
    
    
    cout<<" ||P_ex - P_h||_{L2} / ||P_ex||_{L2} = "<<L2P_err / (double) L2P_ex <<" ||V_ex - V_h||_{L2} / ||V_ex||_{L2} = "<<L2V_err / (double) L2V_ex<<" ||P_ex - P_h||_{inf} / ||P_ex||_{inf} = "<<P_err_inf / (double) P_ex_inf<<endl;
    }

  transform_hetero_ScalarAndVelocityError_to_VTK(U, MSph);
}


void BuildVector_Of_Volume_Projection(cplx *B, MailleSphere &MSph){
  MKL_INT e, iloc;
  for(e = 0; e < MSph.get_N_elements(); e++) {
    cplx *b = B + e * MSph.ndof;
    
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      b[iloc] = quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV(e, iloc, MSph);
    }
  }
}


void BuildVector_Of_Volume_Projection_parallel_version(cplx *B, MailleSphere &MSph){
  MKL_INT e;
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++) {
      cplx *b = B + e * MSph.ndof;
      
      for(MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	b[iloc] = quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV(e, iloc, MSph);
      }
    }
  }
}


void BuildVector_Of_Volume_Projection_incidentPlaneWave_parallel_version(cplx *B, MailleSphere &MSph, const char chaine[]){
  MKL_INT e;
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++) {
      cplx *b = B + e * MSph.ndof;
      
      for(MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	b[iloc] =  integrale_tetra(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine) + integrale_tetra_VV(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
      }
    }
  }
}



void BuildVector_Of_Areal_Projection(cplx *B, MailleSphere &MSph){
  MKL_INT e, iloc;
  for(e = 0; e < MSph.get_N_elements(); e++) {
    cplx *b = B + e * MSph.ndof;
    
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      b[iloc] = Sum_integral_projection_PP(e, iloc, MSph) + Sum_integral_projection_VV(e, iloc, MSph);
    }
  }
}


void BuildVector_Of_Areal_Projection_parallel_version(cplx *B, MailleSphere &MSph){
  MKL_INT e;
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++) {
      cplx *b = B + e * MSph.ndof;
      
      for(MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	b[iloc] = Sum_integral_projection_PP(e, iloc, MSph) + Sum_integral_projection_VV(e, iloc, MSph);
      }
    }
  }
}


void collection_Solutions_locales_parallel_version(VecN &U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, taille =  MSph.ndof *  MSph.ndof;

  cplx *MP, *P, *adjtP, *B;
  MatMN M_red;
  VecN u_red;
  
#pragma omp parallel private(MP, P, adjtP, e, M_red, u_red, B)
  {
    
    MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 
    
    P = new cplx[taille];//Pour stocker la matrice P_red[e]
    
    adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])
    
    M_red.m = MSph.ndof;
    M_red.n = MSph.ndof;
    
    M_red.Allocate_MatMN();
    
    
    u_red.n = MSph.ndof;
    u_red.Allocate();

    // struct timeval t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12;
    //double a1, a2, a3, a4, a5, a6;
    
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++){

      initialize_tab(taille, M_red.value);
      initialize_tab(MSph.ndof, u_red.value);


      M_red.m = MSph.elements[e].Nred;
      M_red.n = MSph.elements[e].Nred;

      u_red.n = MSph.elements[e].Nred;
    
      //on clône l'emplacement U correspondant à l'élément e
      //gettimeofday(&t1, NULL);
      U.get_F_elem(MSph.ndof, e, &B);
      //gettimeofday(&t2, NULL);
      //a1 = ((t2.tv_sec  - t1.tv_sec) * 1000000u + 
      //	  t2.tv_usec - t1.tv_usec) / 1.e6;
      //cout<<" temps de stockage de l'adresse du vecteur RHS sur "<<e<<" : "<<a1<<" secs"<<endl;  
      
      MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;
      
      MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
      
      //cout<<" Nred_Pro["<<e<<"] = "<<MSph.elements[e].Nred_Pro<<endl;

      //gettimeofday(&t3, NULL);
      store_P_and_transpose_P(e, P, adjtP, P_red, MSph);
      //gettimeofday(&t4, NULL);
      //a2 = ((t4.tv_sec  - t3.tv_sec) * 1000000u + 
      //      t4.tv_usec - t3.tv_usec) / 1.e6;
      //cout<<" temps de stockage du bloc de P_red et son adjoint sur"<<e<<" : "<<a2<<" secs"<<endl;  
      
      //cout<<" Calcule de MP_red = conj(trans(PP_red)) * MP * PP_red : "<<endl;    

      //gettimeofday(&t5, NULL);
      compute_A_red_ColMajor(e, M_glob, M_red.value, MSph, MP, P, adjtP);
      //gettimeofday(&t6, NULL);
      //a3 = ((t6.tv_sec  - t5.tv_sec) * 1000000u + 
      //      t6.tv_usec - t5.tv_usec) / 1.e6;
      //cout<<" temps de calcul de A_red sur"<<e<<" : "<<a3<<" secs"<<endl; 
      
      //cout<<" calcul de u_red = conj(trans(PP_red)) * B "<<endl;    

      //gettimeofday(&t7, NULL);
      compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);
      //gettimeofday(&t8, NULL);
      //a4 = ((t8.tv_sec  - t7.tv_sec) * 1000000u + 
      //      t8.tv_usec - t7.tv_usec) / 1.e6;
      //cout<<" temps de calcul de F_red sur"<<e<<" : "<<a4<<" secs"<<endl; 
      
      //cout<<"résolution du système linéaire MP_red x_red = u_red correspondante par la méthode de CHOLESKY : "<<endl;
          
      //La Matrice et le vecteur seront écrasés
      //gettimeofday(&t9, NULL);
      M_red.Solve_by_CHOLESKY_ColMajor(u_red);          
      //gettimeofday(&t10, NULL);
      //a5 = ((t10.tv_sec  - t9.tv_sec) * 1000000u + 
      //      t10.tv_usec - t9.tv_usec) / 1.e6;
       //cout<<" temps de resolution du systeme lineaire sur"<<e<<" : "<<a5<<" secs"<<endl; 
      
       //cout<<" On retrouve la solution dans l'ancienne base B = PP_red u_red : "<<endl;

      //gettimeofday(&t11, NULL);
      reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);
      //gettimeofday(&t12, NULL);
      //a6 = ((t12.tv_sec  - t11.tv_sec) * 1000000u + 
      //      t12.tv_usec - t11.tv_usec) / 1.e6;
      //cout<<" temps de representation de la solution sur l''ancienne base de "<<e<<" : "<<a6<<" secs"<<endl; 
    }
    cout<<"Libération finale de la mémoire :"<<endl; 
    delete [] M_red.value; M_red.value =  nullptr;
    delete [] u_red.value; u_red.value =  nullptr;
    delete [] MP ; MP =  nullptr;
    delete [] P ; P =  nullptr;
    delete [] adjtP ; adjtP =  nullptr;
    
  }
}


void compute_max_and_min_of_all_blocks_eigenvalues(VecN &LAMBDA, MailleSphere &MSph){
  MKL_INT e;

  double rmax = 0.0, rmin = 1000.0;

  for(e = 0; e < MSph.get_N_elements(); e++) {
    
    cplx *lambda;
    get_F_elemRed(e, &lambda, LAMBDA, MSph);

    rmax = real(lambda[0]) / (double) MSph.elements[e].facto_volume;

    rmin = real(lambda[MSph.elements[e].Nred-1]) / (double) MSph.elements[e].facto_volume;

    //lambda_min_normalisee = min des valeurs propres normalisees sur tous les elements
    MSph.lambda_min_normalisee = min(MSph.lambda_min_normalisee, rmin);
    //lambda_max_normalisee = max des valeurs propres normalisees sur tous les elements
    MSph.lambda_max_normalisee = max(MSph.lambda_max_normalisee, rmax);
  }
  
  MSph.Nred_mean = LAMBDA.n / (double) MSph.get_N_elements();
}


void compute_max_and_min_of_all_ordered_eigenvalues_full(VecN &LAMBDA, MailleSphere &MSph){
  MKL_INT e;

  double rmax = 0.0, rmin = 1000.0, rmax_normalisee = 0.0, rmin_normalisee = 0.0;

  for(e = 0; e < MSph.get_N_elements(); e++) {
    
    cplx *lambda;
    LAMBDA.get_F_elem(MSph.ndof, e, &lambda);

    rmax = real(lambda[MSph.ndof-1]);

    rmin = real(lambda[0]);

    rmax_normalisee = real(lambda[MSph.ndof-1]) / (double) MSph.elements[e].facto_volume;
    
    rmin_normalisee = real(lambda[0]) / (double) MSph.elements[e].facto_volume;

    //lambda_min_normalisee = min des valeurs propres sur tous les elements
    MSph.lambda_min = min(MSph.lambda_min, rmin);
    //lambda_max_normalisee = max des valeurs propres sur tous les elements
    MSph.lambda_max = max(MSph.lambda_max, rmax);

    //lambda_min_normalisee = min des valeurs propres normalisees sur tous les elements
    MSph.lambda_min_normalisee = min(MSph.lambda_min_normalisee, rmin_normalisee);
    //lambda_max_normalisee = max des valeurs propres normalisees sur tous les elements
    MSph.lambda_max_normalisee = max(MSph.lambda_max_normalisee, rmax_normalisee);
  }
  
}



void combinaison_solution(Element &E, MKL_INT ndof, cplx *alpha_loc, double *X){
  //Cette fonction calcul la solution locale asscoiée à l'élément e en X et la stocke en u = \sum_{iloc = 1}{ndof} ALPHA[i] * exp(i K X) (pour la pression) et en Vexacte pour la vitesse
  /*-------------------------------------------------------------- input ----------------------------------------
    E            : objet de type Element
    ndof         : le nombre de degré de liberté en chaque élément
    alpha_loc    : les coordonnées de la solution sur la base d'ondes planes associée à E
    X            : la variable est évalué la solution
    /*------------------------------------------------------------- ouput ---------------------------------------
    Pexacte  : la pression
    Vexacte  : la vitesse
    /*-----------------------------------------------------------------------------------------------------------*/
  E.Pexacte = cplx(0.0,0.0);
  
  for (MKL_INT i = 0; i < 3; i++)
    E.Vexacte[i] = cplx(0.0,0.0);
  
  for(MKL_INT iloc = 0; iloc < ndof; iloc++){

    //calcul de P et V en X. P est stockée dans OP[iloc].P et V dans OP[iloc].V
    E.OP[iloc].compute_P_and_V(X);

    //P = P + alpha_loc * P_iloc avec P_iloc = OP[iloc].P
    E.Pexacte += alpha_loc[iloc] * E.OP[iloc].P;  
    
    //V = V + alpha_loc * V_iloc avec V_iloc = OP[iloc].V
    for(MKL_INT i = 0; i < 3; i++){
   
      E.Vexacte[i] += alpha_loc[iloc] * E.OP[iloc].V[i];
    }
    
  }
}


void combinaison_solution_en_pression(Element &E, MKL_INT ndof, cplx *alpha_loc, double *X){
  //Cette fonction calcul la solution locale asscoiée à l'élément e en X et la stocke en u = \sum_{iloc = 1}{ndof} ALPHA[i] * exp(i K X) (pour la pression) et en Vexacte pour la vitesse
  /*-------------------------------------------------------------- input ----------------------------------------
    E            : objet de type Element
    ndof         : le nombre de degré de liberté en chaque élément
    alpha_loc    : les coordonnées de la solution sur la base d'ondes planes associée à E
    X            : la variable est évalué la solution
    /*------------------------------------------------------------- ouput ---------------------------------------
    Pexacte  : la pression
    Vexacte  : la vitesse
    /*-----------------------------------------------------------------------------------------------------------*/
  E.Pexacte = cplx(0.0,0.0);
  
  for (MKL_INT i = 0; i < 3; i++)
    E.Vexacte[i] = cplx(0.0,0.0);
  
  for(MKL_INT iloc = 0; iloc < ndof; iloc++){
    
    //calcul de P et V en X. P est stockée dans OP[iloc].P et V dans OP[iloc].V
    E.OP[iloc].compute_P_and_V(X);

    //P = P + alpha_loc * P_iloc avec P_iloc = OP[iloc].P
    E.Pexacte += alpha_loc[iloc] * E.OP[iloc].P;      
  }
}



void debugg_combinaison_solution(MailleSphere &MSph, const char chaine[]){
   double divide = 1.0 / 1000.0;
  auto t3 = std::chrono::high_resolution_clock::now();
  A_skyline A_glob;
  A_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  
  auto t4 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_Asky = t4 - t3;
  
  cout<<" temps Allocation de A_sky : "<<float_A_Asky.count() * divide<<" secs"<<endl;
  
  //fprintf(fic_e, " temps Allocation de A_sky : %g secs \n", float_A_Asky.count() * divide);
  
  auto t5 = std::chrono::high_resolution_clock::now();
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(1); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  
  auto t6 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_Msky = t6 - t5;
  
  cout<<" temps Allocation de M_sky : "<<float_A_Msky.count() * divide<<" secs"<<endl;
  
  //fprintf(fic_e, " temps Allocation de M_sky : %g secs \n", float_A_Msky.count() * divide);
  
  auto t7 = std::chrono::high_resolution_clock::now();
  // //Tableau skyline des vecteurs propres réduites
  A_skyline P_red;
  P_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  P_red.set_n_inter(1); // fixe le nombre d'interactions 
  P_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  P_red.set_n_A(); // calcule le nombre de coeff a stocker
  P_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()
  
  auto t8 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_Pred = t8 - t7;

  cout<<" temps Allocation de P_red : "<<float_A_Pred.count() * divide<<" secs"<<endl;
  
  //fprintf(fic_e, " temps Allocation de P_red : %g secs \n", float_A_Pred.count() * divide);
  
  VecN U;
  
  U.n = MSph.get_N_elements() * MSph.ndof;
  U.Allocate();
  
  VecN B;
  
  B.n = U.n;
  B.Allocate();
  
  auto t9 = std::chrono::high_resolution_clock::now();

  ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(A_glob, MSph, chaine);
  
  auto t10 = std::chrono::high_resolution_clock::now();

   std::chrono::duration<double, std::milli> float_A_Aglob = t10 - t9;
  
  cout<<" temps d'assemblage de A_sky : "<<float_A_Aglob.count() * divide<<" secs"<<endl;
  
  auto t11 = std::chrono::high_resolution_clock::now();
  
  BuildArealMetricMatrix_parallel_version(M_glob, MSph, chaine);
  
  auto t12 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_B_M = t12 - t11;
  
  cout<<" temps de construction de la matrice M : "<<float_B_M.count() * divide<<" secs"<<endl;
   
  
  //fprintf(fic_e, " temps de construction de la matrice non réduite M : %g secs \n", float_B_M.count() * divide);
  
  auto t3x = std::chrono::high_resolution_clock::now();

  ASSEMBLAGE_DU_VecteurRHS_parallel_version(B.value, MSph);
  
  auto t4x = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_Bsky = t4x - t3x;
  
  cout<<" temps d'assemblage de Bsky : "<<float_Bsky.count() * divide<<" secs"<<endl;
 
 
  auto tN_red_1 = std::chrono::high_resolution_clock::now();
  VecN LAMBDA;
  LAMBDA.n = compute_sizeBlocRed_parallel_version(M_glob, MSph);
  LAMBDA.Allocate();
  auto tN_red_2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_N_red = tN_red_2 - tN_red_1;
  
  cout<<" temps de calcul des dimensions des bases reduites : "<<float_N_red.count() * divide<<" secs"<<endl;

  auto tbasis_1 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs_ET_Normalisation(M_glob, P_red, LAMBDA, MSph);
  auto tbasis_2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_basis = tbasis_2 - tbasis_1;
  
  cout<<" temps de construction des bases reduites: "<<float_basis.count() * divide<<" secs"<<endl;
  
 
  MKL_INT e;
  for(e = 0; e < 9; e++) {
    cout<<"Nred["<<e<<"] = "<<MSph.elements[e].Nred<<endl;
  }
  for(e = MSph.get_N_elements()-9; e < MSph.get_N_elements(); e++) {
    cout<<"Nred["<<e<<"] = "<<MSph.elements[e].Nred<<endl;
  }

  auto tn1 = std::chrono::high_resolution_clock::now();
  compute_max_and_min_of_all_blocks_eigenvalues(LAMBDA, MSph);

  cout<<"lambda_max = "<<MSph.lambda_max<<" lambda_min = "<<MSph.lambda_min<<endl;
  cout<<"lambda_max_normalisee = "<<MSph.lambda_max_normalisee<<" lambda_min_normalisee = "<<MSph.lambda_min_normalisee<<endl;
  cout<<"Nred_mean = "<<MSph.Nred_mean<<endl;
  auto tn2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_n21 = tn2 - tn1;
  
  cout<<" temps de calcul du max et du min de toutes les valeurs propres: "<<float_n21.count() * divide<<" secs "<<endl;
  
  VecN B_red, U_red;
  
  B_red.n = LAMBDA.n;
  U_red.n = B_red.n;
  
  B_red.Allocate();
  U_red.Allocate();
  
  auto t15 = std::chrono::high_resolution_clock::now();
  
  MKL_INT sizeA_red = compute_sizeForA_red(MSph);
  
  auto t16 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_sizeA_red = t16 - t15;
  
  cout<<" calcul de la taille de A_red : "<<float_sizeA_red.count() * divide<<" secs"<<endl;
  
  auto t17 = std::chrono::high_resolution_clock::now();
  //Tableau skylineRed A_red = P_red * A_glob * conj(P_red) 
  A_skylineRed A_red;
  A_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_red.set_n_inter(5); // fixe le nombre d'interactions 
  A_red.set_n_A(sizeA_red); // calcule la taille totale de la matrice
  A_red.build_mat(MSph); // Construit les blocks et la structure en mémoire continue
  A_red.print_info();
  
  auto t18 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_A_A_red = t18 - t17;
  
  cout<<" temps d'allocation de A_red : "<<float_A_A_red.count() * divide<<" secs"<<endl;
  
  auto t21 = std::chrono::high_resolution_clock::now();
  produit_A_skyline_P_skyline_new_version(A_glob, P_red, A_red, MSph);
  
  auto t22 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_S_Ared = t22 - t21;
  
  cout<<" temps de construction de la matrice A_red : "<<float_S_Ared.count() * divide<<" secs"<<endl;
  
  auto t23 = std::chrono::high_resolution_clock::now();
  produit_P_skyline_RightHandSide_new_version(P_red, B_red, B, MSph);
  
  auto t24 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_S_Bred = t24 - t23;
  
  cout<<" temps de construction du second membre B_red: "<<float_S_Bred.count() * divide<<" secs"<<endl;
 
    auto tAS1 = std::chrono::high_resolution_clock::now();
    
    Sparse AS;
    
    AS.m = U_red.n;
    AS.n = U_red.n;
    
    AS.transforme_A_skyline_to_Sparse_CSR(A_red, MSph);
    
    auto tAS2 = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double, std::milli> float_S_AS = tAS2 - tAS1;
    
    cout<<" Transformation de la matrice A_sky en Sparse: "<<float_S_AS.count() * divide<<" secs"<<endl;
    
    cout<<"LAMBA.n = "<<U_red.n<<endl;
    
    
    auto t25 = std::chrono::high_resolution_clock::now();
   
    cout<<"Resolution par LU : "<<endl;
    AS.compute_solution(B_red, U_red);

    auto t26 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_S_Ured = t26 - t25;
    
    
    cout<<" temps de calcul de la solution réduite U_red : "<<float_S_Ured.count() * divide<<" secs"<<endl;
    
    
    auto t27 = std::chrono::high_resolution_clock::now();
    produit_P_skyline_Solution_red(P_red, U_red, U, MSph);
    auto t28 = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> float_S_U = t28 - t27;
	
    cout<<"temps global de calcul de la solution U: "<<float_S_U.count() * divide<<" secs"<<endl;
    /*
    cplx P, *V;

    V = new cplx[3];

    double maxx_P, minn_P, maxx_V1, minn_V1, maxx_V2, minn_V2, maxx_V3, minn_V3;

    maxx_P = 0.0; minn_P = 1000.0; maxx_V1 = 0.0; minn_V1 = 1000.0;
    maxx_V2 = 0.0; minn_V2 = 1000.0; maxx_V3 = 0.0; minn_V3 = 1000.0;
    for(MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

      double P_err = norme_L2_erreur_volumique_en_pression_avec_integ_numerique(U.value, MSph);
      
      double V_err =  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(U.value, MSph);
      

      maxx_P = max(maxx_P, abs(P_err - MSph.elements[e].Vol));
      minn_P = min(minn_P, abs(P_err - MSph.elements[e].Vol));
      
      maxx_V1 = max(maxx_V1, abs(V_err - MSph.elements[e].Vol));
      minn_V1 = min(minn_V1, abs(V_err - MSph.elements[e].Vol));
      
      maxx_V2 = max(maxx_V2, abs(V[1] - MSph.elements[e].Vexacte[1]));
      minn_V2 = min(minn_V2, abs(V[1] - MSph.elements[e].Vexacte[1]));

      maxx_V3 = max(maxx_V3, abs(V[2] - MSph.elements[e].Vexacte[2]));
      minn_V3 = min(minn_V3, abs(V[2] - MSph.elements[e].Vexacte[2]));
     
    

    cout<<" maxx_P = "<<maxx_P<<" minn_P = "<<minn_P<<" maxx_V1 = "<<maxx_V1<<" minn_V1 = "<<minn_V1<<endl;
    // cout<<" maxx_V2 = "<<maxx_V2<<" minn_V2 = "<<minn_V2<<" maxx_V3 = "<<maxx_V3<<" minn_V3 = "<<minn_V3<<endl;
    
   
    double P_err = norme_L2_erreur_volumique_en_pression_avec_integ_numerique(U.value, MSph);

    double V_err =  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(U.value, MSph);

    double P_ex_inf =  norme_inf_Bessel(MSph);

    double P_err_inf = norme_inf_erreur(U.value, MSph);

    cout<<" ||P_ex - P_h||_{L^2} = "<<P_err<<" ||V_ex - V_h||_{L^2} = "<<V_err<<" ||P_ex - P_h||_inf = "<<P_err_inf<<endl;

    double P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(MSph);

    double V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MSph);

    cout<<" ||P_ex - P_h||_{L^2} / ||P_ex||_{L^2} = "<<P_err / (double) P_ex<<" ||V_ex - V_h||_{L^2} / ||V_ex||_{L^2} = "<<V_err / (double) V_ex<<" ||P_ex - P_h||_inf / ||P_ex||_inf = "<<P_err_inf / P_ex_inf<<endl;*/
    double maxx_Pe, minn_Pe, maxx_Ve, minn_Ve;
   double maxx_P, minn_P, maxx_V, minn_V;

   maxx_P = 0.0; minn_P = 1000.0; maxx_V = 0.0; minn_V = 1000.0;

   auto to1 = std::chrono::high_resolution_clock::now();
   double IP =  norme_L2_erreur_surfacique_en_pression_avec_integ_numerique(U.value, MSph);
   auto to2 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_S1 = to2 - to1;
   
   auto to3 = std::chrono::high_resolution_clock::now();
   double JP =  norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_parallel_version(U.value, MSph);
   auto to4 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_S2 = to4 - to3;
   
   auto to5 = std::chrono::high_resolution_clock::now();
   double IV = norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique(U.value, MSph);
   auto to6 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_S3 = to6 - to5;
   
   auto to7 = std::chrono::high_resolution_clock::now();
   double JV = norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_parallel_version(U.value, MSph);
   auto to8 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_S4 = to8 - to7;
   
   maxx_P = max(maxx_P, abs(IP - JP));
   minn_P = min(minn_P, abs(IP - JP));
   
   maxx_V = max(maxx_V, abs(IV - JV));
   minn_V = min(minn_V, abs(IV - JV));
   //cout<<" e = "<<e<<" maxx_P = "<<maxx_P<<" minn_P = "<<minn_P<<" maxx_V = "<<maxx_V<<" minn_V = "<<minn_V<<endl;
      //}
   
   cout<<" maxx_P = "<<maxx_P<<" minn_P = "<<minn_P<<" maxx_V = "<<maxx_V<<" minn_V = "<<minn_V<<endl;
   cout<<" temps calcul norme pression en sequentiel : "<<float_S1.count()<<endl;
   cout<<" temps calcul norme velocite en sequentiel : "<<float_S3.count()<<endl;
   cout<<" temps calcul norme pression en parallel : "<<float_S2.count()<<endl;
   cout<<" temps calcul norme velocite en parallel : "<<float_S4.count()<<endl;
   
}




void MailleSphere::build_fixe_element(char which_h[], MKL_INT choice){

  MKL_INT j, l, s;
  double h;

  cout<<" hmax = "<<hmax<<" hmin = "<<hmin<<endl;
  if (strcmp(which_h, "hmin") == 0){
    h = hmin;
  }
  else{
    h = hmax;
  }

  //noeud_fixe_element = new double[12];
  
  fixe_element.set_ref(N_elements+1);
  
  cout<<" noeud 0 de l'element virtuel "<<endl;
  noeud_fixe_element[0] = 0.0;
  noeud_fixe_element[1] = 0.0;
  noeud_fixe_element[2] = 0.0;

  cout<<" noeud 1 de l'element virtuel "<<endl;
  noeud_fixe_element[3 * 1] = h;
  noeud_fixe_element[3 * 1 + 1] = 0.0;
  noeud_fixe_element[3 * 1 + 2] = 0.0;

  cout<<" noeud 2 de l'element virtuel "<<endl;
  noeud_fixe_element[3 * 2] = 0.0;
  noeud_fixe_element[3 * 2 + 1] = h;
  noeud_fixe_element[3 * 2 + 2] = 0.0;

  cout<<" noeud 3 de l'element virtuel "<<endl;
  noeud_fixe_element[3 * 3] = 0.0;
  noeud_fixe_element[3 * 3 + 1] = h;
  noeud_fixe_element[3 * 3 + 2] = h;

  //sommets_fixe_element = new Sommet[4];

  cout<<" Affectation des noeuds à l'element virtuel "<<endl;
  for(j = 0; j < 4; j++) {

    sommets_fixe_element[j].set_X(noeud_fixe_element + 3 * j);
    
    fixe_element.noeud[j] = sommets_fixe_element + j;
  }

  cout<<" Affectation des noeuds aux faces de l'element virtuel "<<endl;
  for(j = 0; j < 4; j++) {
    s = 0;
    for(l = 0; l < 4; l++) {
      if(j != l) {
	fixe_element.face[j].noeud[s] =  sommets_fixe_element + l;
	s++;
      }
    }
  }

  double *XX = nullptr, *YY = nullptr, *ZZ = nullptr, *XYZ = nullptr;
  double *Am = nullptr, *w = nullptr, *H = nullptr;
  XX = new double[3]; YY = new double [3];// utiliser pour calculer
  ZZ = new double [3]; XYZ = new double [3];//le volume de chaque élément
  Am = new double [9];  w = new double [3];  H = new double[9];
  //calcule du volume du tetra
  fixe_element.compute_volume_tetra(XX, YY, ZZ, XYZ);

  //calcule du centre de gravité
  fixe_element.compute_XG();

  //rad = sqrt(|XG|)
  fixe_element.compute_rad_XG();

  //définition du mode de filtrage des valeurs propres sur chaque élément
  fixe_element.compute_eps_max_For_Eigenvalue_Filtering(eps_0, choice);
  
  fixe_element.compute_eps_of_Tikhonov_regulator(eps_reg_Tikhonov, choice);

  cout<<" filtre de l'element virtuel : "<<fixe_element.eps_max<<endl;
  cout<<" regulateur Tikhonov pour l'element virtuel : "<<fixe_element.eps_max_Tikhonov<<endl;
  
  //racine cubique du vol du tetra
  fixe_element.rc_Vol = pow(fixe_element.Vol, 1.0 / 3.0);

 
  //J'ai défini les coeffs de façon homogène (ref == 2 pour tous les éléments)
  choisir_A_et_xi_fixe_element();


  Compute_EigenValue(fixe_element.Ac.value,H, w);
  fixe_element.Ac.eigen[0] = w[0];
  fixe_element.Ac.eigen[1] = w[1];
  fixe_element.Ac.eigen[2] = w[2];
  prodMatMatDiag_positive(H,w,fixe_element.Ac.ADP);
  prodMatMatDiag_negative(H,w,fixe_element.Ac.ADN);
  fixe_element.Ac.compute_inverse();
  verifyA_OneoverSQRT_A(fixe_element.Ac.ADN, H, w, Am) ;

  cout<<" A^{-1} = "<<endl;
  for(int i1 = 0; i1 < 3; i1++){
    for(int i2 = 0; i2 < 3; i2++){
      cout<<fixe_element.Ac.invmat[i2 * 3 + i1]<<", ";
    }
    cout<<endl;
  }
  
  /*-------------------- definition des ondes planes -------------------------*/
  
  //directions des ondes planes dans un cube
 
  for (s = 0; s < ndof; s++){
    fixe_element.OP[s].define_d(Dir[3*s], Dir[3*s+1], Dir[3*s+2]);
    fixe_element.OP[s].compute_Ad_and_dAd(fixe_element.Ac, fixe_element.xi);
    
    //calculer le vecteur K de Hélène, Sebastien, Julien et Ben
    fixe_element.OP[s].compute_K(fixe_element.xi, omega);
  }

  //calcul des normales aux faces de l'élément e_new
  get_exteriorNormalToElem(fixe_element, XX, YY, ZZ, XYZ);
  
  //calculer les impédances directionnelles
  for(s = 0; s < 4; s++){
    fixe_element.compute_An_and_nAn(s);
  }
  
  /*--------------------------definition d'une onde plane incidente ---------------------------*/

  
  //direction d de l'onde incidente
  OP_inc.define_d(1.0 / sqrt(7), 2.0 / sqrt(7), sqrt(3) / sqrt(7));
 
  //calcul de A * d et d * A * d
  OP_inc.compute_Ad_and_dAd(fixe_element.Ac, fixe_element.xi);
  //K de l'onde incidente
  OP_inc.compute_K(fixe_element.xi, omega);

  cout<<" xi = "<<fixe_element.xi<<" A = ";
  for(int l1 = 0; l1 < 3; l1++){
    for(int l2 = 0; l2 < 3; l2++){
      cout<<fixe_element.Ac.value[l1 * 3 + l2]<<", ";
    }
    cout<<endl;
  }

  OP_inc.print_info();

  fixe_element.Nred = 0;
  fixe_element.Nres = 0;

  for(j = 0; j < 4; j++) {
    fixe_element.face[j].compute_Aire_triangle(XX, YY, ZZ);
  }
  cout<<"Volume element fictif = "<<fixe_element.Vol<<endl;
  delete [] XX; XX =  nullptr;
  delete [] YY; YY =  nullptr;
  delete [] ZZ; ZZ =  nullptr;
  delete [] XYZ; XYZ =  nullptr;
  delete [] H; H =  nullptr;
  delete [] w; w =  nullptr;
  delete [] Am; Am =  nullptr;
  //delete [] ZY; ZY =  nullptr;

  //calcul de A * d et d * A * d
  //OP_inc.compute_Ad_and_dAd(fixe_element.Ac, fixe_element.xi);
  //OP_inc.define_d(fixe_element.OP[n_i].d[0], fixe_element.OP[n_i].d[1], fixe_element.OP[n_i].d[2]);
}


 



cplx integrale_PincP_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]) {
   /*---------------------------------------------------------- input -------------------------------
    MSph : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
 
  MKL_INT l;
                                                
  for (l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] < 0){
    
      I +=  MSph.elements[e].alpha_PincP[l] * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
    }
  }
 
  return I;
}


void Const_vector_SourceOndePlane_PincP(VecN &B, MailleSphere &MSph, const char chaine[]) {
  /*---------------------------------------------------------------- input -------------------------------
    B    : objet de type VecN pour stocker le vecteur Pinc * conj(P')
    MSph : object contenant le maillage
    /*--------------------------------------------------------------- ouput -------------------------------- 
    B
    /*--------------------------------------------------------------- intermediate -----------------------
    i     : numero global d'un élément
    iloc  : numero local d'une fonction de base
    iglob : numero global d'une fonction de base
    /*----------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, iloc, iglob;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof);
      B.value[iglob] = integrale_PincP_SourceOndePlane(i, iloc, MSph, chaine);
    }
  }
}



cplx integrale_VincV_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    MSph : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn1, dn2;
  //dn1 pour stocker dinc \cdot n_j avec n_j la normale à la face j et dinc la direction de l'onde incidente
  //dn2 pour stocker diloc \cdot n_j avec n_j la normale à la face j et diloc la direction de l'onde iloc                
                                              
  for (l = 0; l < 4; l++) {                       
    if (MSph.elements[e].voisin[l] < 0){
      if (l == 0) {
	dn1 = cblas_ddot(lda, MSph.OP_inc.Ad, inc, MSph.elements[e].n1, inc) ;
	dn2 = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n1, inc) ;

	dn1 = dn1 * MSph.OP_inc.ksurOmega;
	dn2 = dn2 * MSph.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 1) {
	dn1 = cblas_ddot(lda, MSph.OP_inc.Ad, inc, MSph.elements[e].n2, inc) ;
	dn2 = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n2, inc) ;

	dn1 = dn1 * MSph.OP_inc.ksurOmega;
	dn2 = dn2 * MSph.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 2) {
	dn1 = cblas_ddot(lda, MSph.OP_inc.Ad, inc, MSph.elements[e].n3, inc) ;
	dn2 = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n3, inc) ;

	dn1 = dn1 * MSph.OP_inc.ksurOmega;
	dn2 = dn2 * MSph.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 3) {
	dn1 = cblas_ddot(lda, MSph.OP_inc.Ad, inc, MSph.elements[e].n4, inc) ;
	dn2 = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n4, inc) ;

	dn1 = dn1 * MSph.OP_inc.ksurOmega;
	dn2 = dn2 * MSph.elements[e].OP[iloc].ksurOmega;
       
      }
     
      I +=  MSph.elements[e].alpha_VincV[l] * dn1 * dn2 * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
    }
  }
  
  return I;
}



void Const_vector_SourceOndePlane_VincV(VecN &B, MailleSphere &MSph, const char chaine[]) {
  /*---------------------------------------------------------------- input -------------------------------
    B    : objet de type VecN pour stocker le vecteur Vinc * conj(V')
    MSph : object contenant le maillage
    /*--------------------------------------------------------------- ouput -------------------------------- 
    B
    /*--------------------------------------------------------------- intermediate -----------------------
    i     : numero global d'un élément
    iloc  : numero local d'une fonction de base
    iglob : numero global d'une fonction de base
    /*----------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, iloc, iglob;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof);
      B.value[iglob] = integrale_VincV_SourceOndePlane(i, iloc, MSph, chaine);
    }
  }
}




void Const_vector_SourceOndePlane_PincP_VincV(VecN &B, MailleSphere &MSph, const char chaine[]) {
  /*---------------------------------------------------------------- input -------------------------------
    B    : objet de type VecN pour stocker le vecteur Vinc * conj(V')
    MSph : object contenant le maillage
    /*--------------------------------------------------------------- ouput -------------------------------- 
    B
    /*--------------------------------------------------------------- intermediate -----------------------
    i     : numero global d'un élément
    iloc  : numero local d'une fonction de base
    iglob : numero global d'une fonction de base
    /*----------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, iloc, iglob;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof);
      B.value[iglob] = integrale_PincP_SourceOndePlane(i, iloc, MSph, chaine) + integrale_VincV_SourceOndePlane(i, iloc, MSph, chaine);
    }
  }
}




//integrale de Pinc * Viloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_PincV_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    MSph : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker diloc \cdot n_j avec n_j la normale à la face j                
  
  for (l = 0; l < 4; l++) {                       
    if (MSph.elements[e].voisin[l] < 0) {
      if (l == 0) {
	dn = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n1, inc) ;
	dn = dn * MSph.elements[e].OP[iloc].ksurOmega;
     
      }
      else if (l == 1) {
	dn = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n2, inc) ;
	dn = dn * MSph.elements[e].OP[iloc].ksurOmega;
      
      }
      else if (l == 2) {
	dn = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n3, inc) ;
	dn = dn * MSph.elements[e].OP[iloc].ksurOmega;
     
      }
      else if (l == 3) {
	dn = cblas_ddot(lda, MSph.elements[e].OP[iloc].Ad, inc, MSph.elements[e].n4, inc) ;
	dn = dn * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      
      I += MSph.elements[e].alpha_PincV[l] * dn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
    }
  }
  
  return I;
}



//integrale de Vinc * Piloc sur un element avec comme onde incidente une onde plane quelconque
cplx integrale_VincP_SourceOndePlane(MKL_INT e, MKL_INT iloc, MailleSphere &MSph, const char chaine[]) {

   /*---------------------------------------------------------- input -------------------------------
    MSph : le maillage
    e    : le numero de l'élément
    iloc : le numero d'une fonction de base (fonction test)
    /*-------------------------------------------------------- ouput ---------------------------------
    I = la valeur de l'intégrale
    /*------------------------------------------------------------------------------------------------*/
  
  cplx I = cplx(0.0,0.0);
  
  MKL_INT l;
  MKL_INT lda = 3, inc = 1;
  
  double dn;//pour stocker d \cdot n_j avec n_j la normale à la face j et d la direction de l'onde incidente               
                                              
  for (l = 0; l < 4; l++) {                       
    if (MSph.elements[e].voisin[l] < 0) {
      if (l == 0) {
	dn = cblas_ddot(lda,MSph.OP_inc.Ad, inc, MSph.elements[e].n1, inc) ;
	dn = dn *MSph.OP_inc.ksurOmega;
       
      }
      else if (l == 1) {
	dn = cblas_ddot(lda,MSph.OP_inc.Ad, inc, MSph.elements[e].n2, inc) ;
	dn = dn *MSph.OP_inc.ksurOmega;
     
      }
      else if (l == 2) {
	dn = cblas_ddot(lda,MSph.OP_inc.Ad, inc, MSph.elements[e].n3, inc) ;
	dn = dn *MSph.OP_inc.ksurOmega;
       
      }
      else if (l == 3) {
	dn = cblas_ddot(lda,MSph.OP_inc.Ad, inc, MSph.elements[e].n4, inc) ;
	dn = dn *MSph.OP_inc.ksurOmega;
	
      }
      I += MSph.elements[e].alpha_VincP[l] * dn * integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc],MSph.OP_inc, chaine);
    }
  }
  
  return I;
}


void ASSEMBLAGE_DU_VecteurRHS_SourceOndePlane(cplx *B, MailleSphere &MSph, const char chaine[]){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, i, iglob;
  for (i = 0; i < MSph.get_N_elements(); i++) {
    for (iloc = 0; iloc < MSph.ndof; iloc++) {
      iglob = local_to_glob(iloc, i, MSph.ndof); 
      B[iglob] = integrale_PincP_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_PincV_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_VincP_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_VincV_SourceOndePlane(i, iloc, MSph, chaine);
    }
  }
}



void ASSEMBLAGE_DU_VecteurRHS_SourceOndePlane_parallel_version(cplx *B, MailleSphere &MSph, const char chaine[]){
   //cette fonction calcule le vecteur global P P' + P V'n + V n * P' + V n V' n et la stocke dans le tableau B
  //C'est le second associé aux fonctions de Bessel j0 et y0
  /*------------------------------------------------------------------------- input ---------------------------------------------------
    B       : tableau ou stocké le vecteur
    MSph    : le maillage 
    /*----------------------------------------------------------------------- ouput --------------------------------------------------
    B
    /*-------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i;
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for (i = 0; i < MSph.get_N_elements(); i++) {
      
      for (MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	
	MKL_INT iglob = local_to_glob(iloc, i, MSph.ndof); 
	B[iglob] = integrale_PincP_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_PincV_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_VincP_SourceOndePlane(i, iloc, MSph, chaine) +  integrale_VincV_SourceOndePlane(i, iloc, MSph, chaine);
      }
    }
  }
}


void determine_neighboring_face(MKL_INT e, MKL_INT fe, MKL_INT *fg, MailleSphere &MSph){
  
  MKL_INT l, g;
  
  g = MSph.elements[e].voisin[fe];
  for(l = 0; l < 4; l++){
    if (MSph.elements[g].voisin[l] == e){
      *fg = l;
    }
  }
}
