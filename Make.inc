
#COMPOSYX_INCLUDES=/gnu/store/xdcscbwkbfdmjc0v5sf459b9fqj3a5aw-profile/include/composyx/


COMPOSYX_INCLUDES=/gpfs/home/idjiba/composyx/include/composyx/
TLAPACK_INCLUDES=/gpfs/home/idjiba/composyx/tlapack/include/

#MKLROOT=/home/idjiba/intel/oneapi/


CC  =/gpfs/softs/oneAPI/apps/mpi/2021.8.0/bin/mpicxx


INC_MPI=/gpfs/softs/oneAPI/apps/mpi/2021.8.0/include
INC_MKL=${MKLROOT}/include/

LIBSS = -Wl,--start-group \
        -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 \
        -liomp5 -lmpi \
        -lquadmath -lpthread -lm \
        -Wl,--end-group