# THESE IBRAHIMA

  Sources en C++ du code de ma thèse

# Sans compose
```bash
guix shell --container --network --pure  vim coreutils which less git openblas cmake make gfortran-toolchain gcc-toolchain grep
```
```bash
rm -fr build && cmake -B build && make -C build -j9 && ctest --test-dir build
```

## Compose Sans MKL 
```bash
guix shell --container --network --pure maphys++ openssh-sans-x vim coreutils which less git cmake make blaspp lapackpp chameleon mumps mumps-openmpi pastix fabulous arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 grep openblas
```

```bash
rm -fr build && cmake -B build -DUSE_COMPOSE=ON -DTHREADS_PREFER_PTHREAD_FLAG=ON
```

## Compose with MKL 
```bash
guix shell --container --network --pure maphys++-mkl mkl openssh-sans-x vim coreutils which less git cmake blaspp-mkl lapackpp-mkl make chameleon-mkl mumps-mkl-openmpi pastix-mkl fabulous-mkl arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 grep the-silver-searcher
```

```bash
rm -fr build && cmake -B build -DUSE_COMPOSE=ON -DUSE_MKL=ON -DTHREADS_PREFER_PTHREAD_FLAG=ON -DUSE_QUADMATH=ON && make -j5 -C build && ctest --test-dir build -VV 
``
`
## Environment with new variety without mkl 
```bash
guix shell maphys++ --with-git-url=maphys++=$PWD/compose --with-commit=maphys++=6f43fb13200d0997a03419e41239cbcfbe31100d
``

## Environment with new variety with mkl 
```bash
guix shell maphys++-mkl --with-git-url=maphys++-mkl=$PWD/compose --with-commit=maphys++-mkl=6f43fb13200d0997a03419e41239cbcfbe31100d
```

```bash
guix shell --pure maphys++-mkl --with-git-url=maphys++-mkl=/tmp/compose --with-commit=maphys++-mkl=6f43fb13200d0997a03419e41239cbcfbe31100d mkl openssh-sans-x vim coreutils which less git cmake blaspp-mkl lapackpp-mkl make chameleon-mkl mumps-mkl-openmpi pastix-mkl fabulous-mkl arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 grep the-silver-searcher -- /bin/bash -norc
```


```bash
guix shell  --pure maphys++-mkl --with-git-url=maphys++-mkl=/tmp/compose --with-commit=maphys++-mkl=6f43fb13200d0997a03419e41239cbcfbe31100d --with-input=mkl=mkl@2020.4.304 mkl@2020.4.304 openssh-sans-x vim coreutils which less git cmake blaspp-mkl lapackpp-mkl make chameleon-mkl mumps-mkl-openmpi fabulous-mkl arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 pastix-mkl grep the-silver-searcher -- /bin/bash -norc
```


guix shell  --pure maphys++-mkl --with-git-url=maphys++-mkl=$PWD/compose --with-commit=maphys++-mkl=6f43fb13200d0997a03419e41239cbcfbe31100d --with-input=mkl=intel-oneapi-mkl-meta intel-oneapi-mkl-meta openssh-sans-x vim coreutils which less git cmake blaspp-mkl lapackpp-mkl make chameleon-mkl mumps-mkl-openmpi fabulous-mkl arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 pastix-mkl grep the-silver-searcher -- /bin/bash -norc


guix shell  --pure composyx-mkl --with-input=mkl=intel-oneapi-mkl intel-oneapi-mkl openssh-sans-x vim coreutils which less git cmake blaspp-mkl lapackpp-mkl make paddle chameleon-mkl mumps-mkl-openmpi fabulous-mkl arpack-ng gcc-toolchain@11.3.0 gfortran-toolchain@11.3.0 pastix-mkl grep the-silver-searcher -- /bin/bash -norc