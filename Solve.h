#pragma once

#include <functional>
#include <iostream>
#include <string>
#include <stdio.h>
#include <complex>
#include <iostream>
#include <stdlib.h>
#include <signal.h>
#include <ctype.h>
#include <ctime>
#include <fstream>
#include <cstring>
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>

// TODO: !!! Should be handled with USE_MKL flag !!
using cplx = std::complex<double>;
#define MKL_Complex16 std::complex<double>
#if defined(MKL_ILP64)
#define MKL_INT long long
#else
#define MKL_INT int
#endif
#ifndef USE_COMPOSE
#include <mkl.h>
#endif
#if defined(USE_COMPOSE)
#define  LAPACK_COMPLEX_STRUCTURE
#include <mkl.h>
#include <maphys.hpp>
#include <maphys/loc_data/SparseMatrixCOO.hpp>
#include <maphys/solver/GMRES.hpp>
using namespace::maphys;
#endif // USE_COMPOSE

#include <cassert>
#include <omp.h>
#include <bits/stdc++.h>
struct A__skyline
{
  MKL_INT n_A ; //l'espace mémoire à réserver pour la matrice; n_A = n_dof * n_dof * n_inter * n_elem;
  MKL_INT n_elem ;//le nombre d'élément du maillage
  MKL_INT n_dof ;//le nombre de degré de liberté utilisé pour construire la matrice
  MKL_INT n_inter ;//le nombre d'interactions entre un élément-élément et élément-voisin
};

void build_A__skyline(MKL_INT ndof, MKL_INT n_inter, MKL_INT n_elem, A__skyline &Asky);


void get_A__skyline_Block(A__skyline &Asky, cplx *Asky_mem, MKL_INT i, MKL_INT l, MKL_INT taille, cplx **block);


void get_V__skyline_Block(cplx *valeur, MKL_INT i, MKL_INT taille, cplx **block);


void initialize_tab(MKL_INT n, cplx *ZZ);

void prodAB_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *B, MKL_INT m_B, MKL_INT n_B, cplx *C);


void prodMV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx alpha, cplx *V, MKL_INT n_V, cplx *F, cplx beta);


void prodMstarV_ColMajor(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *V, MKL_INT n_V, cplx *F) ;


void ProdA__skyline_Vector_mkl(MKL_INT *voisins, A__skyline &Asky, cplx *Asky_mem, cplx *U, cplx *V);

void copy_tab(MKL_INT n_x, cplx *X, MKL_INT n_y, cplx *Y) ;


void affiche_mat(cplx *A, MKL_INT m_A, MKL_INT n_A, const char arg[]);


cplx compute_XdotY(MKL_INT n, cplx *U, cplx *V) ;


double norme_2(MKL_INT n, cplx *U);


double norme_j_A(cplx *A, MKL_INT m_A, MKL_INT n_A, MKL_INT j) ;


cplx dot_product(cplx *A, MKL_INT m_A, MKL_INT n_A, MKL_INT j1, MKL_INT j2) ;


void QR(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *R) ;


void Solve_tri_sup(cplx *R, cplx *X, cplx *F, MKL_INT n);


void Solve_With_QR(cplx *A, MKL_INT m_A, MKL_INT n_A, cplx *F, MKL_INT n_F, cplx *X, cplx *R, cplx *b) ;


void get_column(MKL_INT j, MKL_INT n, cplx **col, cplx *Z);


void copy_krylov_basis(MKL_INT nkry, MKL_INT n, cplx *ZZ, cplx *Z_n);


void Solveur_GMRES(MKL_INT n, MKL_INT *voisins, double *compte, MKL_INT nkry, A__skyline &Asky, cplx *Asky_mem, cplx *F, cplx *U, cplx *H, cplx *Az, cplx *v, cplx *Kn, cplx *K_r, cplx *F_tilde, cplx *U_red, cplx *R, cplx *b);


void compute_residual(MKL_INT n, MKL_INT *voisins, double *compte, A__skyline &Asky, cplx *Asky_mem, cplx *U, cplx *F, cplx *Fdelta, cplx *AU);


void GMRES_With_Restart(MKL_INT n, MKL_INT *voisins, MKL_INT *iter_ext, MKL_INT nkry, A__skyline &Asky, cplx *Asky_mem, cplx *F, cplx *U);


//#if defined(USE_COMPOSE)
//void A__skyline_to_SparseCOO(MKL_INT *voisins, A__skyline &Asky, cplx *Asky_mem, cplx *Fsky, SparseMatrixCOO<cplx> compose_A, Vector<cplx> compose_b);
//#endif

void Solve_system(MKL_INT nkry, MKL_INT *voisins, A__skyline &Asky, cplx *Asky_mem, cplx *Fsky, cplx *Xsky, MKL_INT method);
