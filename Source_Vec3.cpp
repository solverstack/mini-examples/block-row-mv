#include "Header.h"
using namespace std;
using cplx = complex<double>;
 
//constructeur de l'objet Vec3; vecteur de taille 3
Vec3::Vec3() {
	value[0] = 0;
	value[1] = 0;
	value[2] = 0;
	norm = 0;
	dir[0] = 0;
	dir[1] = 0;
	dir[2] = 0;
}

//remplissage du tableau value de l'object Vec3 avec les variables a, b, c 
void Vec3::define(double a, double b, double c)
{
	value[0] = a;
	value[1] = b;
	value[2] = c;
}
 
void Vec3::print_info() {
  //info sur la classe Vec3
	cout << "Vec[0]=" << value[0] << ", Vec[1]=" << value[1] << ", Vec[2]=" << value[2] << endl;
	compute_norm();
	cout << "norm = " << norm << endl;
	compute_dir();
	cout << "dir[0]=" << dir[0] << ", dir[1]=" << dir[1] << ", dir[2]=" << dir[2] << endl;
}
void Vec3::compute_norm() {
  //Calcule la norme du vecteur stocké dans le tableau value
	norm = sqrt(value[0] * value[0] + value[1] * value[1] + value[2] * value[2]);
}
void Vec3::compute_dir() {
  //Calcule la direction du vecteur stocké dans value
        compute_norm();//appel de la fonction compute_norm pour calculer la norme de l'objet Vec3
	double r = 1.0 / norm;
	dir[0] = value[0] * r;
	dir[1] = value[1] * r;
	dir[2] = value[2] * r;
}

 //Addition de deux vecteurs de type Vec3
Vec3 operator+(Vec3 &vec1, Vec3 &vec2) {
  /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type Vec3
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vec3       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
        Vec3 output;//objet de type Vec3. Elle sert à stocker le résultat
	output.value[0] = vec1.value[0] + vec2.value[0];
	output.value[1] = vec1.value[1] + vec2.value[1];
	output.value[2] = vec1.value[2] + vec2.value[2];
	return output;
}

//Soustraction de deux vecteurs de type Vec3
Vec3 operator-(Vec3 &vec1, Vec3 &vec2) {
  /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type Vec3
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vec3       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
	Vec3 output;
	output.value[0] = vec1.value[0] - vec2.value[0];
	output.value[1] = vec1.value[1] - vec2.value[1];
	output.value[2] = vec1.value[2] - vec2.value[2];
	return output;
}


//produit scalaire de deux vecteurs de type Vec3
double operator*(Vec3 &vec1, Vec3 &vec2) {
  /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type Vec3
    /*-------------------------------------------------------- ouput ---------------------------------------------
                 variable de type double, directement retournée par la fonction
    /*--------------------------------------------------------------------------------------------------------------*/
	return vec1.value[0] * vec2.value[0] + vec1.value[1] * vec2.value[1] + vec1.value[2] * vec2.value[2];
}


//produit vectoriel de deux vecteurs de type Vec3
Vec3 operator^(Vec3 &vec1, Vec3 &vec2) {
 /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type Vec3
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vec3       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
	Vec3 vec3;
	vec3.value[0] = vec1.value[1] * vec2.value[2] - vec1.value[2] * vec2.value[1];
	vec3.value[1] = vec1.value[2] * vec2.value[0] - vec1.value[0] * vec2.value[2];
	vec3.value[2] = vec1.value[0] * vec2.value[1] - vec1.value[1] * vec2.value[0];
	return vec3;
}

Vec3 operator*(double lambda, Vec3 &vec2)
//produit d'un vecteur de type Vec3 par un scalaire réel
{
	Vec3 vec3;
	vec3.value[0] = lambda * vec2.value[0];
	vec3.value[1] = lambda * vec2.value[1];
	vec3.value[2] = lambda * vec2.value[2];
	return vec3;
}


double operator*(Vec3 &veck, Sommet &R) {
  //produit scalaire d'un vecteur de type Vec3 par un vecteur de type Sommet
  /*---------------------------------------------------------- input -----------------------------------------------
    veck : objet de type Vec3
    R    : objet de type Sommet
    /*-------------------------------------------------------- ouput ---------------------------------------------
    out       : variable de type double
    /*--------------------------------------------------------------------------------------------------------------*/
  double *X;//pour pointer vers l'objet R
  double out;
  X = R.get_X();
  double *k = new double[3];
  k[0] = veck.value[0];
  k[1] = veck.value[1];
  k[2] = veck.value[2];
  out = cblas_ddot(3, k, 1, X, 1);
  delete [] k; k = 0;
  return out;
}

//Constructeur de la classe Mat3; matrice carrée de taille 3
Mat3::Mat3() {
	for (MKL_INT l = 0; l < 9; l++) {
	  value[l] = 0;
	  invmat[l] = 0.0;
	}
}

//Information de la classe Mat3
void Mat3::print_info() {
	cout << "Impression de la matrice" << endl;
	for (MKL_INT l = 0; l < 3; l++) {
		cout << value[3 * l] << " , " << value[3 * l + 1] << " , " << value[3 * l + 2] << endl;
		
	}
	
	cout<<" Impression de la matrice puissance 1/2 :"<<endl;
	for (MKL_INT l = 0; l < 3; l++) {
	  cout << ADP[3 * l] << " , " << ADP[3 * l + 1] << " , " << ADP[3 * l + 2] << endl;
	}

	cout<<" Impression de la matrice puissance -1/2 :"<<endl;
	for (MKL_INT l = 0; l < 3; l++) {
	  cout << ADN[3 * l] << " , " << ADN[3 * l + 1] << " , " << ADN[3 * l + 2] << endl;
	  }
}

//Fonction pour stocker la matrice à partir des variables données en entrée
void Mat3::define(double a, double b, double c, double d, double e, double f, double g, double h, double i)
{

	value[0] = a;
	value[1] = b;
	value[2] = c;
	value[3] = d;
	value[4] = e;
	value[5] = f;
	value[6] = g;
	value[7] = h;
	value[8] = i;
}


void affectation(Mat3 &mat_Y, Mat3 &mat_X) {

  MKL_INT i;
  
  for (i = 0; i < 9; i++) {
    mat_Y.value[i] = mat_X.value[i];
    mat_Y.ADP[i] = mat_X.ADP[i];
    mat_Y.ADN[i] = mat_X.ADN[i];
  }
  
  for (i = 0; i < 3; i++) {
    mat_Y.eigen[i] = mat_X.eigen[i];
  }

}


//Fonction pour effectuer la somme de deux matrices de memes types
Mat3 operator+(Mat3 &mat1, Mat3 &mat2)
{
  /*---------------------------------------------------------- input --------------------------------------------------------------
    mat1, mat2    : objets de types mat3
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    mat3          : objet de meme type
    /*---------------------------------------------------------------------------------------------------------------------------*/
	Mat3 mat3;
	for (MKL_INT l = 0; l < 9; l++) {
		mat3.value[l] = mat1.value[l] + mat2.value[l];
	}
	return mat3;
}
Mat3 operator*(double alpha, Mat3 &mat)
{
  /*---------------------------------------------------------- input --------------------------------------------------------------
    mat   : objet de type mat3
    alpha : scalaire de type double
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    mat2          : objet de meme type
    /*---------------------------------------------------------------------------------------------------------------------------*/
	Mat3 mat2;
	for (MKL_INT l = 0; l < 9; l++) {
		mat2.value[l] = alpha * mat.value[l];
	}
	return mat2;
}

//Retour la matrice de l'objet aux indices (i,j)
double Mat3::eval(MKL_INT i, MKL_INT j) {
	MKL_INT l = 3 * i + j;
	return value[l];
}

//calcule le déterminant de la matrice
double Mat3::det()
{
	double output;
	MKL_INT j;
	MKL_INT k;
	output = 0;
	for (MKL_INT i = 0; i < 3; i++){
		j = (i + 1) % 3;
		k = (i + 2) % 3;
		output += eval(i,0) * (eval(j,1)*eval(k,2)- eval(k,1) * eval(j, 2));
	}
	return output;
}


void Mat3::compute_inverse(){
  int l;
  int n = 3; // Taille de la matrice
  int lda = n;
  int ipiv[3]; // Pivot pour dgetrf
  int info, test = 0;
  
  for(l = 0; l < 9; l++){
    invmat[l] = value[l];
  } 
  
    // Étape 1: Décomposition LU de A
  info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, n, n, invmat, lda, ipiv);
  if (info != 0) {
    printf("Erreur dans dgetrf: info = %d\n", info);
    exit(0);
  }
  
  // Étape 2: Calcul de l'inverse en utilisant la décomposition LU
  info = LAPACKE_dgetri(LAPACK_ROW_MAJOR, n, invmat, lda, ipiv);
  if (info != 0) {
    printf("Erreur dans dgetri: info = %d\n", info);
    exit(0);
  }
  
  if (test == 1){
    // Affichage de la matrice inverse
    printf("Matrice inverse:\n");
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
	printf("%f ", invmat[i * n + j]);
      }
      printf("\n");
    }
  }
  
}


MKL_INT Mat3::issym()
{
  /*****************************************************************************************************************************************/
  /**************************************    teste si la matrice est symétrique ou non *****************************************************    renvoie 1 si la matrice est symétrique et 0 sinon *********************************************/
  /*****************************************************************************************************************************************/
  double som = 0;
  double threshold=1e-8;
  MKL_INT l1;
  MKL_INT l2;
  for (MKL_INT i = 0; i < 3; i++) {
    for (MKL_INT j = 0; i < 3; i++) {
      l1 = 3 * i + j; l2 = i + 3 * j;
      som += fabs(value[l1] - value[l2]) * 0.5;
    }
  }
  if (som <=threshold)
    return 1;
  else{
    cout << "défaut de symetrie " << som << endl;
    return 0;
  }
}

MKL_INT Mat3::isantisym()
{
  /*****************************************************************************************************************************************/
  /*****************************   teste si la matrice est antisymétrique ou non *****************************************************    renvoie 1 si la matrice est antisymétrique et 0 sinon *********************************************/
  /*****************************************************************************************************************************************/
  double som = 0;
  double threshold=1e-8;
  MKL_INT l1;
  MKL_INT l2;
  for (MKL_INT i = 0; i < 3; i++) {
    for (MKL_INT j = 0; i < 3; i++) {
      l1 = 3 * i + j; l2 = i + 3 * j;
      som += fabs(value[l1] + value[l2]) * 0.5;
    }
  }
  if (som <threshold)
    return 1;
  else {
    cout << "défaut de symetrie " << som << endl;
    return 0;
  }
}


//Pour effectuer le produit d'une matrice par un vecteur
Vec3 operator*(Mat3 &mat, Vec3 &U)
{
   /*---------------------------------------------------------- input --------------------------------------------------------------
    mat   : objet de type mat3
    U     : ............. Vec3
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    V     : objet de type Vec3
    /*---------------------------------------------------------------------------------------------------------------------------*/
	Vec3 V;
	for (MKL_INT i = 0; i < 3; i++) {
		V.value[i] = 0 ;
		for (MKL_INT j = 0; j < 3; j++) {
			V.value[i] +=  mat.value[3*i+j]*U.value[j];
		}
	}
	return V;
}


double quad(Vec3 &U, Mat3 &mat, Vec3 &V)
//calcule le produit U * mat * V
   /*---------------------------------------------------------- input --------------------------------------------------------------
    mat   : objet de type mat3
    U, V  : objets de type VecN
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    som   : variable de type double
    /*---------------------------------------------------------------------------------------------------------------------------*/
{
	double som=0;
	for (MKL_INT i = 0; i < 3; i++) {
		for (MKL_INT j = 0; j < 3; j++) {
			som += U.value[i] * mat.value[3 * i + j] * V.value[j];
		}
	}
	return som;
}


void Compute_EigenValue(double Ac[], double Bc[], double *w) {
  //Calculer les valeurs propres de la matrice Ac
  /*---------------------------------------------------------- input ------------------------------------
    Ac : une matrice
    Bc = Ac 
    /*------------------------------------------------------ ouput ------------------------------------------
    w = tableau contenant les valeurs propres de Ac
    /*----------------------------------------------------------------------------------------------------*/
  //Bc = Ac pour éviter la destruction de la matrice Ac
  for(MKL_INT i = 0; i < 9; i++){
    Bc[i] = Ac[i];
  }
 
  MKL_INT lwork = -1, info;
  MKL_INT lda = 3;
  
  info = LAPACKE_dsyev(LAPACK_ROW_MAJOR, 'V', 'L', lda,
		       Bc, lda, w );
  
  //cout<<"info = "<<info<<"\n";
  
}


void computeMatandMatTransp(double Ac[], double AcAc[]){
  //Calculer Ac * Ac^T
  /*---------------------------------------------------------- input ------------------------------------
    Ac, AcAc : deux matrices 
    /*------------------------------------------------------ ouput ------------------------------------------
    AcAc = Ac * Ac^T
    /*----------------------------------------------------------------------------------------------------*/
  //Bc = Ac pour éviter la destruction de la matrice Ac
  MKL_INT i, j, l;
  double Bc[9];
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AcAc[i * 3 + j] = 0.0;
      Bc[i * 3 + j] = Ac[i * 3 + j];
    }
  }
  MKL_INT ldc = 3, lda = 3, ldb = 3;
  double alpha = 1.0, beta = 0.0;
  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, lda, lda, ldb, alpha, Ac, lda, Bc, ldb, beta, AcAc, ldc) ;
}

//Pour vérifier la décomposition de A = AC D Ac^-1
void VerifyDecomposition(double Ac[], double D[], double AcDAc[]) {
  /*------------------------------------------------------------------- input ----------------------------------------------
    Ac  : matrice des vecteurs propres associées à la matrice A de l'equation de Helhmoltz
    D   : tableau contenant les coeff de la matrice diagonale , A = Ac D Ac^-1
    AcDAc = A
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  double Ec[9];
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AcDAc[i * 3 + j] = 0.0;
      Ec[i*3+j] = 0.0;
    }
  }

  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      for(l = 0; l < 3; l++){
	if (j == l){
	  Ec[i*3 + j] += Ac[i*3+l] * D[j];
	}
      }
    }
  }
  
  MKL_INT ldc = 3, lda = 3, ldb = 3;
  double alpha = 1.0, beta = 0.0;
  //AcDAc = Ac * D * Ac
  cblas_dgemm(CblasColMajor, CblasTrans, CblasNoTrans, lda, lda, ldb, alpha, Ec, lda, Ac, ldb, beta, AcDAc, ldc) ;
}

void prodMatMatDiag_positive(double Ac[], double D[], double AcDAc[]) {
  //Calculer  Ac * D^{1/2} * Ac
   /*------------------------------------------------------------------- input ----------------------------------------------
    Ac  : matrice des vecteurs propres associées à la matrice A de l'equation de Helhmoltz
    D   : tableau contenant les coeff de la matrice diagonale , A = Ac D Ac^-1
    AcDAc = Ac * D^{1/2} * Ac
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  double Ec[9];
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AcDAc[i * 3 + j] = 0.0;
      Ec[i*3+j] = 0.0;
    }
  }
  //Ec = Ac * sqrt(D)
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      for(l = 0; l < 3; l++){
	if (j == l){
	  Ec[i*3 + j] += Ac[i*3+l] * sqrt(D[j]);
	}
      }
    }
  }
 
  //AcDAc = Ac * D^{1/2} * Ac
  cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasTrans, 3, 3,
                 3, 1.0, Ec,
                 3, Ac, 3,
                 0.0, AcDAc, 3) ;
}

//Pour vérifier qu'on a bien A = Ac * Ac 
void verifyA_SQRT_A(double Ac[], double ASA[]) {
  //Calculer A^{1/2}
   /*------------------------------------------------------------------- input ----------------------------------------------
    Ac  : matrice dont les coeff sont les racines carrés des coeffs des vecteurs propres associées à la matrice A de l'equation de Helhmoltz

    ASA = Ac * Ac
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      ASA[i*3+j] = 0.0;
    }
  }
  //calcul de ASA = A^{1/2} * A^{1/2}
 
   cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasNoTrans, 3, 3,
                 3, 1.0, Ac,
                 3, Ac, 3,
                 0.0, ASA, 3) ;
}


void prodMatMatDiag_negative(double Ac[], double D[], double AcDAc[]) {
  //calculer  Ac * D^{ -1/2 } * Ac
   /*------------------------------------------------------------------- input ----------------------------------------------
    Ac  : matrice des vecteurs propres associées à la matrice A de l'equation de Helhmoltz
    D   : tableau contenant les coeff de la matrice diagonale , A = Ac D Ac^-1
    AcDAc = Ac * D^{ -1/2 } * Ac
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  double Ec[9];
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AcDAc[i * 3 + j] = 0.0;
      Ec[i*3+j] = 0.0;
    }
  }
  //Ec = Ac * D^{ -1/2}
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      for(l = 0; l < 3; l++){
	if (j == l){
	  Ec[i*3 + j] += Ac[i*3+l] / (double) sqrt(D[j]);
	}
      }
    }
  }
  //calcul de AcDAc
  cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasTrans, 3, 3,
                 3, 1.0, Ec,
                 3, Ac, 3,
                 0.0, AcDAc, 3) ;
}


void verifyA_OneoverSQRT_A(double Ac[], double AOSA[]) {
  //Calculer A^{ -1/2}
  /*------------------------------------------------------------------- input ----------------------------------------------
    Ac  : matrice dont les coeff sont les 1 / sqrt  des coeffs des vecteurs propres associées à la matrice A de l'equation de Helhmoltz
    
    AOSA = Ac * Ac
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AOSA[i*3+j] = 0.0;
    }
  }
  //calcul de AOSA
  MKL_INT ldc = 3, lda = 3, ldb = 3;
  double alpha = 1.0, beta = 0.0;
  cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasNoTrans, lda, lda,
                 ldb, alpha, Ac,
                 lda, Ac, ldb,
                 beta, AOSA, ldc) ;
}


void verifyA_OneoverSQRT_A(double T[], double P[], double w[], double AOSA[]) {
  //Calculer A^{ -1/2}
  /*------------------------------------------------------------------- input ----------------------------------------------
    T  = Ac^{-1/2} avec Ac une matrice symétrique définie/semi-définie positive  
    
    P :  les vecteurs propres de Ac
    w : les valeurs propres
    AOSA : pour stocker P^-1 T P^-1
    /*--------------------------------------------------------------------------------------------------
    AcDAc
    /*-----------------------------------------------------------------  output ---------------------------------*/
  MKL_INT i, j, l;
  double PT[9];
  for(i = 0; i < 3; i++){
    for(j = 0; j < 3; j++){
      AOSA[i*3+j] = 0.0;
      PT[i * 3 +j] = 0.0;
    }
  }

 

  //calcul de AOSA
  MKL_INT ldc = 3, lda = 3, ldb = 3;
  double alpha = 1.0, beta = 0.0;
  cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasNoTrans, lda, lda,
                 ldb, alpha, P,
                 lda, T, ldb,
                 beta, PT, ldc) ;

   cblas_dgemm(CblasRowMajor, CblasNoTrans,
                 CblasNoTrans, lda, lda,
                 ldb, alpha, PT,
                 lda, P, ldb,
                 beta, AOSA, ldc) ;

   double maxx, minn, maxx_, minn_;
   maxx = 0.0; minn = 1000; maxx_ = 0.0; minn_ = 1000;
   for(i = 0; i < 3; i++){
     maxx = max(maxx, abs(AOSA[i*3+i] - w[i]));
     minn = min(minn, abs(AOSA[i*3+i] - w[i]));
   }

   for(i = 0; i < 3; i++){
     for(j = 0; j < 3; j++){
       if (i!=j){
	 maxx = max(maxx, abs(AOSA[i*3+i]));
	 minn = min(minn, abs(AOSA[i*3+i]));
       }
     }
   }
   if ((maxx > 1E-10) && (minn > 1E-10) && (maxx_ > 1E-10) && (minn_ > 1E-10)){
     cout<<" probleme dans la decomposition de la matrice locale des equations de Helmhlotz "<<endl;
   }
}



//constructeur de la classe Direction
Direction::Direction() {
  ordre = -1;
  ndof = -1;
  Kw = 0;
  DD = 0;
}

void Direction::set_ordre(MKL_INT n) {
  ordre = n;
}

void Direction::set_Kw(double h) {
  Kw = h;
}

MKL_INT Direction::get_ordre() {
  return ordre;
}

double Direction::get_Kw() {
  return Kw;
}


void directionInCube(double *Tab, MKL_INT ordre){
  //Pour calculer les directions des ondes planes dans le cas cubique
  /*---------------------------------------------------------- input -------------------------------------
    Tab   : tableau des vecteurs directions des ondes planes
    ordre : entier permettant de définir le nombre d'ondes planes par élément
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Tab
    /*--------------------------------------------------------------------------------------------------------*/
  MKL_INT l = 0;
  double div, mult;
  for (MKL_INT ix = -ordre; ix <= ordre; ix++){
    for (MKL_INT iy = -ordre; iy <= ordre; iy++){
      for (MKL_INT iz = -ordre; iz <= ordre; iz++){
	if ( (ix == -ordre) || (ix == ordre) || (iy == -ordre) || (iy == ordre) || (iz == -ordre) || (iz == ordre) ){
	  div = sqrt(ix * ix + iy * iy + iz * iz);
	  div = 1.0 / div;
	  Tab[3*l] = ix * div;
	  Tab[3*l+1] = iy * div ;
	  Tab[3*l+2] = iz * div ;
	  // cout<<"indice = "<<3*l<<" ix = "<<ix<<" iy = "<<iy<<" iz = "<<iz<<"\n";
	  l++;
	 
	}
      }
    }
  }
}



MKL_INT Defined_dimension_of_Trefftz_basis(MKL_INT ordre){
  //Pour calculer les directions des ondes planes dans le cas cubique
  /*---------------------------------------------------------- input -------------------------------------
    Tab   : tableau des vecteurs directions des ondes planes
    ordre : entier permettant de définir le nombre d'ondes planes par élément
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Tab
    /*--------------------------------------------------------------------------------------------------------*/
  MKL_INT ndof;
  double div, ixx, iyy, izz;
  
  ndof = 0;
  for (MKL_INT ix = 0    ; ix <= ordre; ix++){
    ixx = ix -ordre/2.0;
    for (MKL_INT iy = 0 ; iy <= ordre; iy++){
      iyy = iy -ordre/2.0; 
      for (MKL_INT iz = 0 ; iz <= ordre; iz++){                  
	izz = iz -ordre/2.0;
	//div = 1.0 / sqrt(ixx * ixx + iyy * iyy + izz * izz);
	if ( (ix == 0) || (ix == ordre) || (iy == 0) || (iy == ordre) || (iz == 0) || (iz == ordre) ){
	  
	  //cout<<"indice = "<<ndof<<" ixx = "<<ixx * div<<" iyy = "<<iyy * div<<" izz = "<<izz * div<<"\n";
	  ndof++;
	}
      }
    }
  }
  return ndof;
}



void directionInCube_complete_version(double *Tab, MKL_INT ordre){
  //Pour calculer les directions des ondes planes dans le cas cubique
  /*---------------------------------------------------------- input -------------------------------------
    Tab   : tableau des vecteurs directions des ondes planes
    ordre : entier permettant de définir le nombre d'ondes planes par élément
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Tab
    /*--------------------------------------------------------------------------------------------------------*/
  MKL_INT l;
  double div, ixx, iyy, izz;
  l = 0;
  for (MKL_INT ix = 0    ; ix <= ordre; ix++){
    ixx = ix -ordre/2.0;
    for (MKL_INT iy = 0 ; iy <= ordre; iy++){
      iyy = iy -ordre/2.0; 
      for (MKL_INT iz = 0 ; iz <= ordre; iz++){                  
	izz = iz -ordre/2.0;
	if ( (ix == 0) || (ix == ordre) || (iy == 0) || (iy == ordre) || (iz == 0) || (iz == ordre) ){
	  div = sqrt(ixx * ixx + iyy * iyy + izz * izz);
	  div = 1.0 / div;
	  Tab[3*l] = ixx * div;
	  Tab[3*l+1] = iyy * div ;
	  Tab[3*l+2] = izz * div ;
	  //double r = sqrt(Tab[3*l] * Tab[3*l] + Tab[3*l+1] * Tab[3*l+1] + Tab[3*l+2] * Tab[3*l+2]);
	  //cout<<"indice = "<<l<<" ixx = "<<Tab[3*l]<<" iyy = "<<Tab[3*l+1]<<" izz = "<<Tab[3*l+2]<<" r = "<<r<<"\n";
	  l++;
	}
      }
    }
  }
 
}


void Direction::Build_Direction() {
  ndof =  (2 * ordre + 1) * (2 * ordre + 1) * (2 * ordre + 1) -  (2 * ordre - 1) * (2 * ordre - 1) * (2 * ordre - 1);
  
  DD = (Vec3 *)malloc(ndof * sizeof(Vec3));
  
  double* Tab =  new double[ndof * 3]; 
  MKL_INT l;
  
  directionInCube(Tab, ordre);
  
  for(l = 0; l < ndof; l++) {
    DD[l].define(Tab[3*l],Tab[3*l+1],Tab[3*l+2]);
  }

  delete [] Tab; Tab = 0;
}

void Direction::print_info() {
  for(MKL_INT l = 0; l < ndof; l++) {
    cout<<"ondes "<<l<<": \n";
    DD[l].print_info();
  }
}

Direction::~Direction() {
  free(DD); DD = 0;
}

//Constructeur de la classe Onde_plane. C'est la classe de la direction des Onde_planes de chaque élément
Onde_plane::Onde_plane() {
  normk = 0.0;
  for(MKL_INT i = 0; i < 3; i++){
    K[i] = 0.0;
    d[i] = 0.0;
    Ad[i] = 0.0;
  }
}

void Onde_plane::define_d(double x, double y, double z){
  // pour remplir d, la direction dans la classe Onde_plane
  /*------------------------------------------------------------ input ---------------------------------
    x, y, z : coordonnées d'un vecteur directionnel asscoié à une onde plane
    /*--------------------------------------------------------------------------------------------------*/
  d[0] = x;
  d[1] = y;
  d[2] = z;
}


void Onde_plane::compute_Ad_and_dAd(Mat3 &mat, double m_xi){
  //pour calculer A * d et d * A * d associés à la classe Onde_plane
  /*--------------------------------------------------------- input ----------------------------------
    mat  :  objet de type Mat3
    m_xi : coeff associé à l'equation de Helmholtz sur un élément
    /*-----------------------------------------------------------------------------------------------------*/

  //Ad = A^{-1} * d avec A^{-1} = mat.invmat
  cblas_dgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1.0, mat.invmat, 3, d, 1, 0.0, Ad, 1);
  
  //dAd = d\cdot Ad
  dAd = cblas_ddot(3, d, 1, Ad, 1) ;

  //ksurOmega = xi / dAd
  ksurOmega = sqrt(m_xi / dAd);
}

void Onde_plane::compute_K(double m_xi, double omega) {
    //Pour calculer le vecteur d'onde, version Sébastien, Ben, Julien et Hélène associé à la classe Onde_plane
  /*--------------------------------------------------------- input ----------------------------------
    m_xi : coeff associé à l'equation de Helmholtz sur un élément
    /*-----------------------------------------------------------------------------------------------------*/

  //||k|| = normk = sqrt(xi / r); 
  normk = omega * sqrt(m_xi / dAd);
  //K = d * ||k||             ajouter petit omega plus tard
  for(MKL_INT i = 0; i < 3; i++){
    K[i] = normk * d[i];
  }

}


void Onde_plane::compute_KofCesDep(Mat3 &mat, double m_xi, double omega) {
  //Pour calculer le vecteur d'onde version Cessenat associé à la classe Onde_plane
  /*--------------------------------------------------------- input ----------------------------------
    mat  :  de type Mat3
    m_xi : coeff associé à l'equation de Helmholtz sur un élément
    /*-----------------------------------------------------------------------------------------------------*/
  MKL_INT lda = 3;
  MKL_INT inc = 1;
  double alpha = 1.0;
  double beta = 0.0;
  double *A2d = new double[3];;
  for(MKL_INT i = 0; i < 3; i++){
    A2d[i] = 0.0;
  }
   
  //A2d = A^{-1/2} * d avec A^{-1/2} = mat.ADN
  cblas_dgemv(CblasColMajor, CblasNoTrans, lda, lda, alpha, mat.ADP, lda, d, inc, beta, A2d, inc);

  //K = sqrt(m_xi) * Ad         ajouter petit omega plus tard
  for(MKL_INT i = 0; i < 3; i++){
    K[i] = sqrt(m_xi) * A2d[i] * omega;
  }

  //norm(K)
  normk = sqrt(K[0] * K[0] + K[1] * K[1] + K[2] * K[2]);

  
  delete [] A2d; A2d = 0;
}


void Onde_plane::set_normk(double r){
  normk = r;
}

double Onde_plane::get_normk() {
  //return la norme du vecteur d'onde
  return normk;
}


void Onde_plane::compute_P_and_V(double *X) {
  //calcul la pression P et la vitesse d'onde plane en un point X
  
  MKL_INT inc = 1;
  MKL_INT lda = 3;
  double r = cblas_ddot(lda, K, inc, X, inc) ;
  //calcul de la pression P
  P = exp(cplx(0.0,r));

  //calcul de la vitesse 
  for(MKL_INT i = 0; i < 3; i++){
    //V[i] = (Ad[i] * sqrt(m_xi) * P) / sqrt(dAd);
    V[i] = Ad[i] * ksurOmega * P;
  }
}


cplx Onde_plane::pression(double *X) {
  double KX = cblas_ddot(3, K, 1, X, 1);
  return exp(cplx(0.0,KX));
}


cplx Onde_plane::calcule(double *X){
  double KX = cblas_ddot(3, K, 1, X, 1);
  cplx I = exp(cplx(0.0,-KX)) * X[1];
  return I;
}



void Onde_plane::copy(Onde_plane &OP) {
  P = OP.P;
  dAd = OP.dAd;
  ksurOmega = OP.ksurOmega;
  for (MKL_INT i = 0; i < 3; i++) {
    K[i] = OP.K[i];
    d[i] = OP.d[i];
    Ad[i] = OP.Ad[i];
    AK[i] = OP.AK[i];
  }
}


void Onde_plane::print_info() {
  //info relatives à la classe Onde_plane
  cout<<"K = [ "<<K[0]<<" "<<K[1]<<" "<<K[2]<<"] "<<endl;
  cout<<" normk = "<<normk<<endl;
  cout<<"dx = ["<<d[0]<<" "<<d[1]<<" "<<d[2]<<"]"<<endl;
  cout<<"Ad = ["<<Ad[0]<<" "<<Ad[1]<<" "<<Ad[2]<<"]"<<endl;
  cout<<"dAd = "<<dAd<<" sqrt(xi / dAd) = "<<ksurOmega<<endl;
  //cout<<"P = "<<P<<", Vx = "<<V[0]<<", Vy = "<<V[1]<<", Vz = "<<V[2]<<"\n";
  
}


/*************************************************************** Matrices pleines MatMN *****************************************/

/******************************************************************************************************************************/


//Constructeur de la classe MatMN, objet pour une matrice de taille M * N
MatMN::MatMN() {
  m = -1;
  n = -1;
  value = 0;
  result = 0;
}


void MatMN::Allocate_MatMN() {
  //allocation des tableaux value et result
  cout<<" allocation du tableau value de MatMN "<<endl;
  value =  new std::complex<double>[m * n];
  //result =  new std::complex<double>[m * n];

  cout<<" allocation du tableau value de MatMN reussie"<<endl;
}



//Fonction pour transformation la matrice pleine associée à cette classe en matrice sparse
void MatMN::transforme_Full_To_Sparse(Maillage &Mesh, Sparse &AS) {
   /*---------------------------------------------------- input ---------------------------------------
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iglob, jglob, l;
  
  AS.nnz = 0;
  for (iglob = 0; iglob < n; iglob++){
    for (jglob = 0; jglob < m; jglob++){
      if (abs(value[iglob * m + jglob]) > 1E-10)
	AS.nnz++;
    }
  }

  //allocation des tableaux a, ia et ja
  AS.a =  new cplx[AS.nnz];
  AS.ia =  new MKL_INT[n+1];
  AS.ja =  new MKL_INT[AS.nnz];
  
  //initialisation des tableaux a, ja et ia
  for (iglob = 0; iglob < AS.nnz; iglob++){
    AS.a[iglob] = cplx(0.0,0.0);
    AS.ja[iglob] = 0;
  }
  for (iglob = 0; iglob < n+1; iglob++)
    AS.ia[iglob] = 0;

  //remplissage des tableaux a, ia et ja
  l = 0;
  AS.ia[0] = 1;
  for (iglob = 0; iglob < n; iglob++){
    for (jglob = 0; jglob < m; jglob++){
      if (abs(value[iglob * m + jglob]) > 1E-10){
	 AS.a[l] = value[iglob * m + jglob];
	 AS.ja[l] = jglob+1;
	 l++;
      }
    }
    AS.ia[iglob+1] = l;
  }
  cout<<"ia[n] = "<<AS.ia[n]<<" nnz = "<<AS.nnz<<endl;
  //for (iglob = 0; iglob < nnz; iglob++){
  //cout<<"iglob = "<<iglob<<" a["<<iglob<<"] = "<<a[iglob]<<" ja["<<iglob<<"] = "<<ja[iglob]<<endl;
  //}
}

void MatMN::transforme_sparse_To_full(Maillage &Mesh, Sparse &AS){
  int i, j, l;
  for (i = 0; i < n; i++){
    for (l = 0; l < AS.ia[i+1] - AS.ia[i]; l++){
      j = AS.ja[AS.ia[i]+l-1]-1;
      value[i * n + j] = AS.a[AS.ia[i]+l-1];
    }
  }
}


//Fonction pour écrire la matrice pleine dans un fichier
void MatMN::transporte_To_matlab(const char chaine[]) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < m; i++){
    for(MKL_INT j = 0 ; j < n; j++){
      val = value[i + j * n];
      fprintf(fic,"%d\t %d\t %10.15e\t \%10.15e\n",i+1,j+1,real(val),imag(val));
    }
  }
  fclose(fic);
  free(file); file = NULL;
}


void MatMN::print_info() {
  //affichage des informations de la classe MatMN
  cout<<"nbre de ligne = "<<m<<" nbre de col = "<<n<<endl;
  cout<<"La matrice : "<<endl;
  for(MKL_INT i = 0; i < m; i++){
    for(MKL_INT j = 0; j < n; j++){
      cout<<value[i * n + j]<<", ";
    }
    cout<<endl;
  }
  if (result) {
    cout<<"resultat de l'operation : "<<endl;
    for(MKL_INT i = 0; i < m; i++){
      for(MKL_INT j = 0; j < n; j++){
	cout<<result[i * n + j]<<", ";
      }
      cout<<endl;
    }
    
  }
}


void MatMN::Solve_by_LU(VecN &B) {
  MKL_INT info, nrhs;
  MKL_INT *ipiv;
  nrhs = 1;
  ipiv = new MKL_INT[n];

  //Decomposition de la matrice A = L U P
  info = LAPACKE_zgetrf(LAPACK_COL_MAJOR, n, n, value, n, ipiv);

  cout<<"info1 = "<<info<<endl;
  
  //Resolution
  info = LAPACKE_zgetrs(LAPACK_COL_MAJOR, 'T' , n, nrhs, value, n, ipiv, B.value, n);
  
  cout<<"info2 = "<<info<<endl;
  
  delete [] ipiv; ipiv = 0;
}


void MatMN::Solve_by_CHOLESKY(VecN &B) {

  MKL_INT info, nrhs, iloc, jloc;
  nrhs = 1;
  
  cplx *A = new cplx[n * m];
  
  cout<<" Copie de la matrice : "<<endl;
  for (iloc = 0; iloc < n; iloc++) {
    for (jloc = 0; jloc < n; jloc++) {
      A[jloc * n + iloc] = value[iloc * n + jloc];
    }
  }
  
  info = LAPACKE_zpotrf(LAPACK_COL_MAJOR, 'U', n, A, n);
  
  cout<<"info1 = "<<info<<endl;

  info = LAPACKE_zpotrs(LAPACK_COL_MAJOR, 'U', n, nrhs, A, n, B.value, B.n);
  cout<<"info2 = "<<info<<endl;

  delete [] A; A = 0;
}



//La Matrice et le vecteur seront écrasés
void MatMN::Solve_by_CHOLESKY_ColMajor(VecN &B) {

  MKL_INT info, nrhs, iloc, jloc;
  nrhs = 1;
  
  info = LAPACKE_zpotrf(LAPACK_COL_MAJOR, 'U', n, value, n);
  
  cout<<"info1 = "<<info<<endl;

  info = LAPACKE_zpotrs(LAPACK_COL_MAJOR, 'U', n, nrhs, value, n, B.value, B.n);
  cout<<"info2 = "<<info<<endl;
}



void MatMN::Solve_by_CHOLESKY(cplx *B) {

  MKL_INT info, nrhs, iloc, jloc;
  nrhs = 1;
  
  cplx *A = new cplx[n * m];
  
  cout<<" Copie de la matrice : "<<endl;
  for (iloc = 0; iloc < n; iloc++) {
    for (jloc = 0; jloc < n; jloc++) {
      A[jloc * n + iloc] = value[iloc * n + jloc];
    }
  }
  
  info = LAPACKE_zpotrf(LAPACK_COL_MAJOR, 'U', n, A, n);
  
  cout<<"info1 = "<<info<<endl;

  info = LAPACKE_zpotrs(LAPACK_COL_MAJOR, 'U', n, nrhs, A, n, B, n);
  cout<<"info2 = "<<info<<endl;

  delete [] A; A = 0;
}



void MatMN::Solve_by_LU_C(VecN &B, VecN &U) {
  MKL_INT info, nrhs, iter, iloc, jloc;
 
  nrhs = 1;
  cplx *A = new cplx[n * m];
  /*cplx *work = new cplx[n];
  double *rwork = new double[n];
  MKL_Complex8 *swork = new MKL_Complex8[n * (n+1)];
  */
  cout<<" Copie de la matrice : "<<endl;
  for (iloc = 0; iloc < n; iloc++) {
    for (jloc = 0; jloc < n; jloc++) {
      A[jloc * n + iloc] = value[iloc * n + jloc];
    }
  }

  info = LAPACKE_zposv(LAPACK_COL_MAJOR, 'U', n, nrhs, A, n, B.value, B.n);
  cout<<" info = "<<info<<endl;
  /*cout<<" Resolution : "<<endl;
 ZCPOSV( "U", &n, &nrhs, A, &n, B.value, &B.n, U.value, &U.n, work, swork, rwork,
             &iter, &info );

 cout<<"iter = "<<iter<<" info = "<<info<<endl;


 delete [] work; work = 0;
 delete [] rwork; rwork = 0; 
 delete [] swork; swork = 0;*/
  delete [] A; A = 0;
}


void compute_Inverse(MKL_INT n, cplx *M){
  MKL_INT info, lwork, iloc, jloc;
  MKL_INT *ipiv = nullptr;
  lwork = n * n;
  ipiv = new MKL_INT[n];
  cplx *work = nullptr; new cplx[lwork];
  
  info = LAPACKE_zgetrf(LAPACK_COL_MAJOR, n, n, M, n, ipiv);

  if (info != 0) {
    delete [] ipiv; ipiv = nullptr;
    delete [] work; work = nullptr;
    printf("Erreur dans zgetrf: info = %d\n", info);
    exit(0);
  }
  

  info = LAPACKE_zgetri(LAPACK_COL_MAJOR, n, M, n, ipiv);

  if (info != 0) {
    delete [] ipiv; ipiv = nullptr;
    delete [] work; work = nullptr;
    printf("Erreur dans zgetri: info = %d\n", info);
    exit(0);
  }
   
 
  delete [] ipiv; ipiv = 0;
  delete [] work; work = 0;
}



//Cette fonction sert à transformer une matrice A_skyline en matrice pleine
void MatMN::transforme_Afull(A_skyline &A_glob, Maillage &Mesh) {
  /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, iglob, jglob, i, j, jbloc, ibloc;
  cplx *AA;
  /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*-------------------------------------------------------------------------------------------------*/
  cout<<" transformation de la matrice en full "<<endl;
  cout<<value<<endl;
  // remplissage de A_full = value
  for(iglob = 0; iglob < m; iglob++){
    for(jglob = 0; jglob < n; jglob++){
      value[jglob * m + iglob] = cplx(0.0,0.0);
    }
  }
  for (i = 0; i < Mesh.get_N_elements(); i++){
    for(j = 0; j < 5; j++){
      if (j == 0) {
	ibloc = i;
	jbloc = i;
      }
      else if (j > 0){
	ibloc = i;
	jbloc = Mesh.elements[i].voisin[j-1];
      }
      if (jbloc >= 0){
	AA = A_glob.Tab_A_loc[jbloc * A_glob.get_n_inter() + j].get_Aloc();
	for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
	  iglob = local_to_glob(iloc, ibloc, A_glob.get_n_dof());
	  
	  for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	    jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	    //cout<<"i = "<<i<<" j = "<<j<<" iglob = "<<iglob<<" jglob = "<<jglob<<" A_glob.get_n_A() = "<<A_glob.get_n_A()<<endl;
    
	    value[jglob * m + iglob] = AA[jloc * A_glob.get_n_dof() + iloc];
	  }
	}
      }
    }
  }
  cout<<" transformation de la matrice en full reussie"<<endl;
}



/*---------------------------------------------------------------------- Pour A_skylineRed ----------------------------------------------------------*/

//Cette fonction sert à transformer une matrice A_skyline en matrice pleine
void MatMN::transforme_Afull(A_skylineRed &A_red, Maillage &Mesh) {
  /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, iglob, jglob, i, j, jbloc, ibloc;
  cplx *AA;
  /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*-------------------------------------------------------------------------------------------------*/
  cout<<" transformation de la matrice en full "<<endl;
  cout<<value<<endl;
  // remplissage de A_full = value
  for(iglob = 0; iglob < m; iglob++){
    for(jglob = 0; jglob < n; jglob++){
      value[jglob * m + iglob] = cplx(0.0,0.0);
    }
  }
  
  for (i = 0; i < Mesh.get_N_elements(); i++){
    
    ibloc = i;
    jbloc = i;
    AA = A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + 0].get_Aloc();
    
    for(iloc = 0; iloc < Mesh.elements[ibloc].Nred; iloc++){
      iglob = local_to_globRed(iloc, ibloc, Mesh);
      
      for(jloc = 0; jloc < Mesh.elements[jbloc].Nred; jloc++){
	jglob = local_to_globRed(jloc, jbloc, Mesh);
    
	value[jglob * m + iglob] += AA[jloc * Mesh.elements[ibloc].Nred + iloc];
      }
    }

    for(j = 0; j < 4; j++){
      
      ibloc = i;
      jbloc = Mesh.elements[i].voisin[j];
      
      if (jbloc >= 0){
	AA = A_red.Tab_A_locRed[jbloc * A_red.get_n_inter() + (j+1)].get_Aloc();
	
	for(iloc = 0; iloc < Mesh.elements[ibloc].Nred; iloc++){
	  iglob = local_to_globRed(iloc, ibloc, Mesh);
	  
	  for(jloc = 0; jloc < Mesh.elements[jbloc].Nred; jloc++){
	    jglob = local_to_globRed(jloc, jbloc, Mesh);
	    //cout<<"i = "<<i<<" j = "<<j<<" iglob = "<<iglob<<" jglob = "<<jglob<<" A_glob.get_n_A() = "<<A_glob.get_n_A()<<endl;
	    
	    value[jglob * m + iglob] += AA[jloc * Mesh.elements[ibloc].Nred + iloc];
	  }
	}
      }
    }
  }
    cout<<" transformation de la matrice en full reussie"<<endl;
}



// void MatMN::transforme_P_red_To_full(A_skyline &P_red, Maillage &Mesh) {
//   /*---------------------------------------------------- input ---------------------------------------
//     P_red : matrice en blocs diagonale
//     Mesh   : le maillage 
//     /*-------------------------------------------------------------------------------------------------*/
//   MKL_INT iloc, jloc, iglob, jglob, i, j, jbloc, ibloc, JLOC;
//   cplx *AA;
//   /*--------------------------------------------------- intermédaire ----------------------------------
//     ibloc : numero de l'élément suivant la ligne
//     jbloc : .............................. colonne
//     AA    : pour clôner ou pour pointer sur un bloc
//     /*-------------------------------------------------------------------------------------------------*/
//   cout<<" transformation de la matrice en full "<<endl;
//   cout<<value<<endl;
//   // remplissage de A_full = value
//   for(iglob = 0; iglob < m; iglob++){
//     for(jglob = 0; jglob < n; jglob++){
//       value[iglob * n + jglob] = cplx(0.0,0.0);
//     }
//   }

  
//   MKL_INT s1 = 0, s2 = 0;
//   for (i = 0; i < Mesh.get_N_elements(); i++){
//     ibloc = i;
//     jbloc = i;
    
//     AA = P_red.Tab_A_loc[i].get_Aloc();
//     for(iloc = 0; iloc < Mesh.ndof; iloc++){
//       iglob = local_to_glob(iloc, ibloc, Mesh.ndof);
      
//       JLOC = 0;
//       for(jloc = Mesh.Nres; jloc < Mesh.ndof; jloc++){
// 	jglob = local_to_glob(JLOC, jbloc, P_red.get_n_dof());
	
// 	value[iglob * n + jglob] = AA[iloc * Mesh.ndof + jloc];
// 	cout<<"iglob = "<<iglob<<" jglob = "<<jglob<<" JLOC = "<<JLOC<<endl;
	
// 	JLOC++;
// 	//s2++;
//       }
//       //s1++;
//     }
    
//   }
//   cout<<" transformation de la matrice en full reussie"<<endl;
// }

void MatMN::produit_MatVec_Full_Version(cplx *b, cplx *res) {
  //Cette fonction effectue le produit matrice vecteur
  /*----------------------------------------------------------------------------- input ------------------------------------------------------
    value : tableau associée à la classe MatMN. Elle contient la matrice.
    b     : object de type VecN. C'est le vecteur dont on veut faire le produit.
    res   : .................... C'est le vecteur dans lequel le résultat sera stocké
    /*--------------------------------------------------------------------------- ouput ------------------------------------------------------
                            res contiendra au sortie le résultat
    /*----------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT inc;
  cplx alpha, beta;
  inc = 1;
  alpha = cplx(1.0,0.0);
  beta = cplx(0.0,0.0);
  //res = alpha * value * b + beta * res
  //zgemv("T", &m, &n, &alpha, value, &m, b.value, &inc, &beta, res.value, &inc);
  cblas_zgemv(CblasRowMajor, CblasNoTrans, m, n, &alpha, value, m, b, inc, &beta, res, inc);
}



void MatMN::produit_MatVec_Full_Version(VecN &b, VecN &res) {
  //Cette fonction effectue le produit matrice vecteur
  /*----------------------------------------------------------------------------- input ------------------------------------------------------
    value : tableau associée à la classe MatMN. Elle contient la matrice.
    b     : object de type VecN. C'est le vecteur dont on veut faire le produit.
    res   : .................... C'est le vecteur dans lequel le résultat sera stocké
    /*--------------------------------------------------------------------------- ouput ------------------------------------------------------
                            res contiendra au sortie le résultat
    /*----------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT inc;
  cplx alpha, beta;
  inc = 1;
  alpha = cplx(1.0,0.0);
  beta = cplx(0.0,0.0);
  //res = alpha * value * b + beta * res
  //zgemv("T", &m, &n, &alpha, value, &m, b.value, &inc, &beta, res.value, &inc);
  cblas_zgemv(CblasRowMajor, CblasNoTrans, m, n, &alpha, value, m, b.value, inc, &beta, res.value, inc);
}


void MatMN::compute_eigenValueAndeigenVector(){
  MKL_INT i, j;
  cplx *vr = new cplx[n*n];
  gamma = new cplx[n];
  cplx *work = new cplx[2*n];
  double *rwork = new double[2*n];
  MKL_INT info, lwork;
  lwork = 2*n;
  if (!result) result = new cplx[n * n];
  for (i = 0; i < m; i++){
    for (j = 0; j < n; j++){
      vr[i*n + j] = value[i*n + j];
    }
  }

  cout<<"result : "<<result[0]<<endl;

  lapack_int aa = LAPACKE_zgeev_work( LAPACK_ROW_MAJOR, 'N', 'V', n, vr, n, gamma, NULL, n,
                                      result, n, work, lwork, rwork );
  
  // cout<<"info = "<<info<<endl;
  delete [] work; work = 0;
  delete [] rwork; rwork = 0;
  delete [] vr; vr = 0;
 
}


//Destructeur de la classe MatMN
MatMN::~MatMN() {
   delete [] value; value = nullptr;
   //if (result != 0) { delete [] result; result = 0;}
   //if (gamma != 0) { delete [] gamma; gamma = 0;}
  m = -1;
  n = -1;
}



/***********************************************************************************************************************/

/******************************************************************* Partie Matrices Sparses ***************************/

/**********************************************************************************************************************/

Sparse::Sparse() {
  ia = 0; ja = 0; a = 0;
  m = -1; n = -1; nnz = -1;
}


//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, Maillage &Mesh) {
   /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  
  nnz = 0;//nombre de non zeros de la matrice
  
  for (i = 0; i < Mesh.get_N_elements(); i++){
	
    AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j].get_Aloc();
    for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
      for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	if (abs(AA[iloc * A_glob.get_n_dof() + jloc]) > 1E-10){
	  nnz++;
	}
      }
    }
    
    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      
      if (Mesh.elements[i].voisin[j] >= 0) {
	jbloc = Mesh.elements[i].voisin[j];
	
	AA = A_glob.Tab_A_loc[jbloc * A_glob.get_n_inter() + j+1].get_Aloc();
	for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
	  for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	    if (abs(AA[iloc * A_glob.get_n_dof() + jloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
	  
      }
    }
    
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin = nullptr, *numero_local =nullptr;
  tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(Mesh.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * Mesh.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_local(A_glob.get_n_dof(), iglob, &ibloc, &iloc);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = Mesh.elements[ibloc].voisin[0];
    tetra_voisin[2] = Mesh.elements[ibloc].voisin[1];
    tetra_voisin[3] = Mesh.elements[ibloc].voisin[2];
    tetra_voisin[4] = Mesh.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	  
	  jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	  
	  val = (A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + numero_local[j]].get_Aloc())[iloc * A_glob.get_n_dof() + jloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = nullptr;
  delete [] numero_local; numero_local = nullptr;
}


/*---------------------------------------------------------------------- SPARSE FOR A_sylineRed -------------------------------------------------------*/

//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, Maillage &Mesh) {
   /*---------------------------------------------------- input ---------------------------------------
    A_red : matrice en blocs contenant le systeme linéaire
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  
  nnz = 0;//nombre de non zeros de la matrice
  
  for (i = 0; i < Mesh.get_N_elements(); i++){

    //Pour les blocs diagonaux
    AA = A_red.Tab_A_locRed[i * A_red.get_n_inter() + 0].get_Aloc();
    
    for(iloc = 0; iloc < Mesh.elements[i].Nred; iloc++){
      for(jloc = 0; jloc < Mesh.elements[i].Nred; jloc++){
	if (abs(AA[jloc * Mesh.elements[i].Nred + iloc]) > 1E-10){
	  nnz++;
	}
      }
    }

    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      ibloc = i;
      jbloc = Mesh.elements[i].voisin[j];
      AA = A_red.Tab_A_locRed[jbloc * A_red.get_n_inter() + (j+1)].get_Aloc();
      
      if (jbloc >= 0) {	  
	for(iloc = 0; iloc < Mesh.elements[ibloc].Nred; iloc++){
	  for(jloc = 0; jloc < Mesh.elements[jbloc].Nred; jloc++){
	    if (abs(AA[jloc * Mesh.elements[ibloc].Nred + iloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
      }
      
    }
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin = nullptr, *numero_local = nullptr;
  tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(Mesh.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * Mesh.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_localRed(iglob, &ibloc, &iloc, Mesh);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = Mesh.elements[ibloc].voisin[0];
    tetra_voisin[2] = Mesh.elements[ibloc].voisin[1];
    tetra_voisin[3] = Mesh.elements[ibloc].voisin[2];
    tetra_voisin[4] = Mesh.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < Mesh.elements[jbloc].Nred; jloc++){
	  
	  jglob = local_to_globRed(jloc, jbloc, Mesh);
	  
	  val = (A_red.Tab_A_locRed[jbloc * A_red.get_n_inter() + numero_local[j]].get_Aloc())[jloc * Mesh.elements[ibloc].Nred + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = nullptr;
  delete [] numero_local; numero_local = nullptr;
}



//Fonction pour la résolution du systéme linéaire Ax = b par la méthode LU de pardiso 

void Sparse::compute_solution(VecN &b, VecN &x) {
#ifdef USE_MKL
  /*--------------------------------------------------------- input ----------------------------------------------------
             b : objet de type VecN. C'est le vecteur second membre
             x : objet de type VecN et qui sert à stocker la solution
    /*-------------------------------------------------------- ouput --------------------------------------------------
             En sortie, on aura la variable x
    /*-----------------------------------------------------------------------------------------------------------------*/
  
   MKL_INT  l;
   MKL_INT ii, jj;
   
   
   char *file = NULL;
   file = (char *) calloc(150, sizeof(char));
   strcat(file,"LU_information");

   FILE* fic;
   
   if( !(fic = fopen(file,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
   
  //phase d'utilisation de pardiso pour calculer la solution de AX = b
  
      
   	MKL_INT mtype = 13; /* Complex unsymmetric matrix */
	/* RHS and solution vectors. */
	
	MKL_INT nrhs = 1; /* Number of right hand sides. */
	/* Internal solver memory pointer pt, */
	/* 32-bit: int pt[64]; 64-bit: long int pt[64] */
	/* or void *pt[64] should be OK on both architectures */
	void *pt[64];
	/* Pardiso control parameters. */
	MKL_INT iparm[64];
	MKL_INT maxfct, mnum, phase, error, msglvl;
	/* Auxiliary variables. */
	MKL_INT i;
	cplx ddum; /* Double dummy */
	MKL_INT idum; /* Integer dummy. */
/* -------------------------------------------------------------------- */
/* .. Setup Pardiso control parameters. */
/* -------------------------------------------------------------------- */
	for (i = 0; i < 64; i++) {
		iparm[i] = 0;
	}

	iparm[0] = 1; /* No solver default */
	iparm[1] = 2; /* Fill-in reordering from METIS */
	/* Numbers of processors, value of OMP_NUM_THREADS */
	iparm[2] = 1;
	iparm[3] = 0; /* No iterative-direct algorithm */
	iparm[4] = 0; /* No user fill-in reducing permutation */
	iparm[5] = 0; /* Write solution into x */
	iparm[6] = 0; /* Not in use */
	iparm[7] = 1; /* Max numbers of iterative refinement steps */
	iparm[8] = 0; /* Not in use */
	iparm[9] = 16; /* Perturb the pivot elements with 1E-13 */
	iparm[10] = 1; /* Use nonsymmetric permutation and scaling MPS */
	iparm[11] = 0; /* Not in use */
	iparm[12] = 0; /* Not in use */
	iparm[13] = 0; /* Output: Number of perturbed pivots */
	iparm[14] = 0; /* Not in use */
	iparm[15] = 0; /* Not in use */
	iparm[16] = 0; /* Not in use */
	iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
	iparm[18] = -1; /* Output: Mflops for LU factorization */
	iparm[19] = 0; /* Output: Numbers of CG Iterations */
	maxfct = 1; /* Maximum number of numerical factorizations. */
	mnum = 1; /* Which factorization to use. */
	msglvl = 1; /* Print statistical information in file */
	error = 0; /* Initialize error flag */
	iparm[59] = 0;

	/* -------------------------------------------------------------------- */
/* .. Initialize the internal solver memory pointer. This is only */
/* necessary for the FIRST call of the PARDISO solver. */
/* -------------------------------------------------------------------- */
	for (i = 0; i < 64; i++) {
		pt[i] = 0;
	}
/* -------------------------------------------------------------------- */
/* .. Reordering and Symbolic Factorization. This step also allocates */
/* all memory that is necessary for the factorization. */
/* -------------------------------------------------------------------- */
	phase = 11;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	if (error != 0) {
		printf("\nERROR during symbolic factorization: %d", error);
		exit(1);
	}
	printf("\nReordering completed ... ");
	printf("\nNumber of nonzeros in factors = %d", iparm[17]);
	printf("\nNumber of factorization MFLOPS = %d", iparm[18]);
	

	fprintf(fic,"%d %d", n, iparm[17]);
/* -------------------------------------------------------------------- */
/* .. Numerical factorization. */
/* -------------------------------------------------------------------- */
	phase = 22;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	if (error != 0) {
		printf("\nERROR during numerical factorization: %d", error);
		exit(2);
	}
	printf("\nFactorization completed ... ");
/* -------------------------------------------------------------------- */
/* .. Back substitution and iterative refinement. */
/* -------------------------------------------------------------------- */
	phase = 33;
	iparm[7] = 1; /* Max numbers of iterative refinement steps. */

	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, b.value, x.value, &error);
	if (error != 0) {
		printf("\nERROR during solution: %d", error);
		exit(3);
	}
	printf("\nSolve completed ... ");
	cout<<" iparm[7] = "<<iparm[7]<<endl;
	cout<<" The number of executed iterative refinement iterations : "<<iparm[6]<<endl;
	cout<<" Total peak memory consumed is "<<max(iparm[14], iparm[15]+iparm[16])<<" ko "<<endl;
	fprintf(fic, " %d\n",max(iparm[14], iparm[15]+iparm[16]));
	
	
/* -------------------------------------------------------------------- */
/* .. Termination and release of memory. */
/* -------------------------------------------------------------------- */
	phase = -1; /* Release internal memory. */
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, &ddum, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	cout<<"ia[n] = "<<ia[n]<<endl; 

	fclose(fic);
	free(file); file=NULL;
  
#endif  
}


void Sparse::compute_solution(cplx *b, cplx *x) {
#ifdef USE_MKL
  /*--------------------------------------------------------- input ----------------------------------------------------
             b : le vecteur second membre
             x : tableau où stocker la solution
    /*-------------------------------------------------------- ouput --------------------------------------------------
             En sortie, on aura la variable x
    /*-----------------------------------------------------------------------------------------------------------------*/
  
   MKL_INT  l;
   MKL_INT ii, jj;
   

   char *file = NULL;
   file = (char *) calloc(150, sizeof(char));
   strcat(file,"LU_information");
   
   FILE* fic;
   
   if( !(fic = fopen(file,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
  //phase d'utilisation de pardiso pour calculer la solution de AX = b
  
      
   	MKL_INT mtype = 13; /* Complex unsymmetric matrix */
	/* RHS and solution vectors. */
	
	MKL_INT nrhs = 1; /* Number of right hand sides. */
	/* Internal solver memory pointer pt, */
	/* 32-bit: int pt[64]; 64-bit: long int pt[64] */
	/* or void *pt[64] should be OK on both architectures */
	void *pt[64];
	/* Pardiso control parameters. */
	MKL_INT iparm[64];
	MKL_INT maxfct, mnum, phase, error, msglvl;
	/* Auxiliary variables. */
	MKL_INT i;
	cplx ddum; /* Double dummy */
	MKL_INT idum; /* Integer dummy. */
/* -------------------------------------------------------------------- */
/* .. Setup Pardiso control parameters. */
/* -------------------------------------------------------------------- */
	for (i = 0; i < 64; i++) {
		iparm[i] = 0;
	}

	iparm[0] = 1; /* No solver default */
	iparm[1] = 2; /* Fill-in reordering from METIS */
	/* Numbers of processors, value of OMP_NUM_THREADS */
	iparm[2] = 1;
	iparm[3] = 0; /* No iterative-direct algorithm */
	iparm[4] = 0; /* No user fill-in reducing permutation */
	iparm[5] = 0; /* Write solution into x */
	iparm[6] = 0; /* Not in use */
	iparm[7] = 1;//2; /* Max numbers of iterative refinement steps */
	iparm[8] = 0; /* Not in use */
	iparm[9] = 16; /* Perturb the pivot elements with 1E-13 */
	iparm[10] = 1; /* Use nonsymmetric permutation and scaling MPS */
	iparm[11] = 0; /* Not in use */
	iparm[12] = 0; /* Not in use */
	iparm[13] = 0; /* Output: Number of perturbed pivots */
	iparm[14] = 0; /* Not in use */
	iparm[15] = 0; /* Not in use */
	iparm[16] = 0; /* Not in use */
	iparm[17] = -1; /* Output: Number of nonzeros in the factor LU */
	iparm[18] = -1; /* Output: Mflops for LU factorization */
	iparm[19] = 0; /* Output: Numbers of CG Iterations */
	iparm[59] = 0;
	maxfct = 1; /* Maximum number of numerical factorizations. */
	mnum = 1; /* Which factorization to use. */
	msglvl = 1; /* Print statistical information in file */
	error = 0; /* Initialize error flag */

	/* -------------------------------------------------------------------- */
/* .. Initialize the internal solver memory pointer. This is only */
/* necessary for the FIRST call of the PARDISO solver. */
/* -------------------------------------------------------------------- */
	for (i = 0; i < 64; i++) {
		pt[i] = 0;
	}
/* -------------------------------------------------------------------- */
/* .. Reordering and Symbolic Factorization. This step also allocates */
/* all memory that is necessary for the factorization. */
/* -------------------------------------------------------------------- */
	phase = 11;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	if (error != 0) {
		printf("\nERROR during symbolic factorization: %d", error);
		exit(1);
	}
	printf("\nReordering completed ... ");
	printf("\nNumber of nonzeros in factors = %d", iparm[17]);
	printf("\nNumber of factorization MFLOPS = %d", iparm[18]);

	fprintf(fic,"%d %d", n, iparm[17]);
/* -------------------------------------------------------------------- */
/* .. Numerical factorization. */
/* -------------------------------------------------------------------- */
	phase = 22;
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	if (error != 0) {
		printf("\nERROR during numerical factorization: %d", error);
		exit(2);
	}
	printf("\nFactorization completed ... ");
/* -------------------------------------------------------------------- */
/* .. Back substitution and iterative refinement. */
/* -------------------------------------------------------------------- */
	phase = 33;
	iparm[7] = 1; /* Max numbers of iterative refinement steps. */

	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, a, ia, ja, &idum, &nrhs,
		iparm, &msglvl, b, x, &error);
	if (error != 0) {
		printf("\nERROR during solution: %d", error);
		exit(3);
	}
	printf("\nSolve completed ... ");
	cout<<" iparm[7] = "<<iparm[7]<<endl;
	cout<<" The number of executed iterative refinement iterations : "<<iparm[6]<<endl;
	cout<<" Total peak memory consumed is "<<max(iparm[14], iparm[15]+iparm[16])<<" ko "<<endl;

	fprintf(fic, " %d\n",max(iparm[14], iparm[15]+iparm[16]));
	
	
/* -------------------------------------------------------------------- */
/* .. Termination and release of memory. */
/* -------------------------------------------------------------------- */
	phase = -1; /* Release internal memory. */
	PARDISO (pt, &maxfct, &mnum, &mtype, &phase,
		&n, &ddum, ia, ja, &idum, &nrhs,
		iparm, &msglvl, &ddum, &ddum, &error);
	cout<<"ia[n] = "<<ia[n]<<endl; 

	fclose(fic);
	free(file); file=NULL;
  
#endif
}



//produit d'une matrice sparse de type MatMN par un vecteur de type VecN
void produit_MatSparse_vecteur(Sparse &AS, VecN &vec, VecN &res) {
  /*--------------------------------------------------------------- input -----------------------------------
    AS : la matrice sparse
    vec : le vecteur avec lequel on fait le produit
    /*------------------------------------------------------------- ouput -----------------------------------
    res : le vecteur ou sera stocké le résultat. Tous les vecteurs de type VecN sont initialisés à leurs allocations
    /*---------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, j, nz;
  for(i = 0; i < AS.m; i++){
    //res.value[i] = cplx(0.0,0.0);
    for(nz = 0; nz < AS.ia[i+1] - AS.ia[i]; nz++){
      j = AS.ja[AS.ia[i]+ nz - 1] - 1;
      res.value[i] += AS.a[AS.ia[i] + nz - 1] * vec.value[j];
      
    }

  }
}


void produit_MatSparse_vecteur(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *vec, cplx *res) {
  /*--------------------------------------------------------------- input -----------------------------------
    AS : la matrice sparse
    vec : le vecteur avec lequel on fait le produit
    /*------------------------------------------------------------- ouput -----------------------------------
    res : le vecteur ou sera stocké le résultat. Tous les vecteurs de type VecN sont initialisés à leurs allocations
    /*---------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, j, nz;
  for(i = 0; i < n; i++){
    //res.value[i] = cplx(0.0,0.0);
    for(nz = 0; nz < ia[i+1] - ia[i]; nz++){
      j = ja[ia[i]+ nz - 1] - 1;
      res[i] += a[ia[i] + nz - 1] * vec[j];
      
    }

  }
}


void produit_MatSparse_vecteur_parallel_version(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *vec, cplx *res) {
  /*--------------------------------------------------------------- input -----------------------------------
    AS : la matrice sparse
    vec : le vecteur avec lequel on fait le produit
    /*------------------------------------------------------------- ouput -----------------------------------
    res : le vecteur ou sera stocké le résultat. Tous les vecteurs de type VecN sont initialisés à leurs allocations
    /*---------------------------------------------------------------------------------------------------*/
  
  MKL_INT i, j, nz;
#ifdef USE_MKL   
  MKL_ZCSRGEMV("N", &n, a, ia, ja, vec, res);
#else 
#pragma omp parallel private(i, j, nz) shared(ia, ja, a, vec, res)
  {
#pragma omp for schedule(runtime)
    for(i = 0; i < n; i++){
      for(nz = 0; nz < ia[i+1] - ia[i]; nz++){
	   j = ja[ia[i]+ nz - 1] - 1;
	   res[i] += a[ia[i] + nz - 1] * vec[j];
      }
    }
    
  }
 #endif  
}


/********************************************** Fonctions pour écrire la matrice sparse dans des fichiers ************************************************/
void Sparse::transporte_Value_and_Columns_To_matlab(const char chaine[]) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < nnz; i++){
    val = a[i];
    fprintf(fic,"%d\t %10.15e\t \%10.15e\n",ja[i],real(val),imag(val));
  }
  fclose(fic);
  free(file); file = NULL;
}



void Sparse::transporte_rows_To_matlab(const char chaine[]) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < n+1; i++){
    fprintf(fic,"%d\n",ia[i]);
  }
  fclose(fic);
  free(file); file = NULL;
}


void read_Value_and_Columns_from_file(MKL_INT nnz, MKL_INT *ja, cplx *a, const char chaine[]) {
  FILE*  fic;
  
  if( !(fic = fopen(chaine,"r")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT   FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  double x1, x2;
  MKL_INT i, j;
  for(i = 0 ; i < nnz; i++){
    
    fscanf(fic,"%d %lg %lg",&j, &x1, &x2);
    a[i] = cplx(x1,x2);
    ja[i] = j;
  }
  fclose(fic);
}


void read_rows_from_file(MKL_INT n, MKL_INT *ia, const char chaine[]) {
  FILE*  fic;

  if( !(fic = fopen(chaine,"r")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT   FILE.\n");
    exit(EXIT_FAILURE);
  }
  
  MKL_INT i;
  
  for(i = 0 ; i < n; i++){
    fscanf(fic,"%d ",&ia[i]);
  }
  
  fclose(fic);
}



Sparse::~Sparse() {
  delete [] a; a = nullptr;
  delete [] ja; ja = nullptr;
  delete [] ia; ia = nullptr;
}

/******************************************************************************************************************************/



/*-******************************************************************************************************************************/


MKL_INT local_to_glob(MKL_INT iloc, MKL_INT tetra, MKL_INT ndof){
  //cette fonction sert à générer le numero global d'une fonction de base à partir de son numero local et du numero de l'élément dont il est associé
  /*-------------------------------------------------- input ------------------------------------------
    iloc  : numero local d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    /*------------------------------------------------  ouput    ------------------------------------------
    iglob : numero global d'une fonction de base
  /*---------------------------------------------------------------------------------------------------*/
  
  MKL_INT iglob = iloc + tetra * ndof;
  return iglob;
}


MKL_INT local_to_globRed(MKL_INT iloc, MKL_INT tetra, Maillage &Mesh){
  //cette fonction sert à générer le numero global d'une fonction de base à partir de son numero local et du numero de l'élément dont il est associé
  /*-------------------------------------------------- input ------------------------------------------
    iloc  : numero local d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    /*------------------------------------------------  ouput    ------------------------------------------
    iglob : numero global d'une fonction de base
  /*---------------------------------------------------------------------------------------------------*/
  MKL_INT iglob, s;
  if (tetra == 0){
    iglob = iloc;
  }

  else {
    iglob = iloc + Mesh.SIZE[tetra];
  }

  
  return iglob;
}



void global_to_local(MKL_INT ndof, MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc) {
  //Cette fonction pour récupérer le numero local d'une fonction de base et le numero de l'élément correspondant à partir du numero global de cette fonction de base  et du nombre de degré de liberté en chaque élément
 /*-------------------------------------------------- input ------------------------------------------
    iglob : numero global d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    iloc  : numero local d'une fonction de base
    /*------------------------------------------------  ouput    ------------------------------------------
    iloc
    tetra
  /*---------------------------------------------------------------------------------------------------*/
  if( iglob < ndof){
    *tetra = 0;
    *iloc = iglob;
  }
  else {
    *tetra = iglob / ndof;
    *iloc = iglob - (*tetra) * ndof;
  }
}



void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, Maillage &Mesh) {
  //Cette fonction pour récupérer le numero local d'une fonction de base et le numero de l'élément correspondant à partir du numero global de cette fonction de base  et du nombre de degré de liberté en chaque élément
 /*-------------------------------------------------- input ------------------------------------------
    iglob : numero global d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    iloc  : numero local d'une fonction de base
    /*------------------------------------------------  ouput    ------------------------------------------
    iloc
    tetra
  /*---------------------------------------------------------------------------------------------------*/
  MKL_INT e, r;

  if( iglob < Mesh.SIZE[1]){
    *tetra = 0;
    *iloc = iglob;
  }
  else if ((iglob >= Mesh.SIZE[1]) &&(iglob < Mesh.SIZE[Mesh.get_N_elements()-1])){
    e = 2;
    r = -1;
    while((e < Mesh.get_N_elements()) && (r < 0)) {
      if (iglob < Mesh.SIZE[e]){
	*tetra = e-1;
	*iloc = iglob - Mesh.SIZE[*tetra];
	r = 0;
      }
      e++;
      //cout<<" tetra = "<<*tetra<<" e_i = "<<e<<endl;
    }
  }
  else {
    *tetra = Mesh.get_N_elements()-1;
    *iloc = iglob - Mesh.SIZE[*tetra];
  }
}



void getNumberOfNeighbours(Maillage &Mesh, MKL_INT *NVS){
  //Cette fonction retourne un tableau contenant le nombre de voisin des tetra
  /*-------------------------------------------------------------- input -------------------------------------
    Mesh : le maillage
    NVS  : NVS[i] le nombre de voisin de l'élément i
    /*-------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, s;
  for (i = 0; i < Mesh.get_N_elements(); i++){
    s = 0;
    for (j = 0; j < 4; j++){
      if (Mesh.elements[i].voisin[j] >= 0)
	s++;
    }
    NVS[i] = s;
  }
}



void AdditionMatMN(MatMN &mat1, MatMN &mat2) {
  //Cette effectue la somme deux objets de types MatMN
  /*---------------------------------------------------------- input --------------------------------------------------------------
    mat1, mat2    : objets de types MatMN
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    ATTENTION !!!!!!!!!!!! Le résultat est stocké dans mat2.result. Donc il faut allouer mat2.result
    /*---------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT mm = mat1.n;
  MKL_INT nn = mat1.m;
  assert(mat2.m && mat1.m);
  assert(mat2.n && mat1.n);
  for(MKL_INT i = 0; i < mm; i++){
    for(MKL_INT j = 0; j < nn; j++){
      mat2.result[i * nn + j] = mat1.value[i * nn + j] + mat2.value[i * nn + j]; 
    }
  }
}


void SoustractionMatMN(MatMN &mat1, MatMN &mat2) {
  //Cette procéde à la soustraction de deux classes de types MatMN
  /*---------------------------------------------------------- input --------------------------------------------------------------
    mat1, mat2    : objets de types MatMN
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    ATTENTION !!!!!!!! Le résultat est stoké dans un sous tableau de mat2; mat2.result    
    /*---------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT mm = mat1.n;
  MKL_INT nn = mat1.m;
  assert(mat2.m && mat1.m);
  assert(mat2.n && mat1.n);
  for(MKL_INT i = 0; i < mm; i++){
    for(MKL_INT j = 0; j < nn; j++){
      mat2.result[i * nn + j] = mat1.value[i * nn + j] - mat2.value[i * nn + j]; 
    }
  }
}


void ScalaireMatMN(double alpha, MatMN &mat) {
  //pour calculer le produit d'une classe de type MatMN par un scalaire alpha
  /*---------------------------------------------------------- input --------------------------------------------------------------
    mat   : objet de type MatMN
    /*-------------------------------------------------------- ouput -------------------------------------------------------------
    ATTENTION !!!!!!!!!!!! Le résulta est stocké dnas un sous tableau de mat; mat.result         
    /*---------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT mm = mat.m;
  MKL_INT nn = mat.n;
  for(MKL_INT i = 0; i < mm; i++){
    for(MKL_INT j = 0; j < nn; j++){
      mat.result[i * nn + j] = alpha * mat.value[i * nn + j]; 
    }
  }
}

//Constructeur de la classe VecN
VecN::VecN() {
  value = nullptr;
  n = -1;
}

//Fonction d'allocation de la classe VecN
void VecN::Allocate() {
  value = new std::complex<double>[n];
  for (MKL_INT i = 0; i < n; i++)
    value[i] = cplx(0.0,0.0);
}


void Allocate_VecNRed(VecN &v, Maillage &Mesh){
  v.n = 0;
  MKL_INT e;
  for (e = 0; e < Mesh.get_N_elements(); e++){
    v.n += Mesh.elements[e].Nred;
  }
  v.value = new std::complex<double>[v.n];
  for (e = 0; e < v.n; e++){
    v.value[e] = cplx(0.0,0.0);
  }
}

//Fonction de définition de la taille de l'objet VecN
void VecN::set_n(MKL_INT an) {
  n = an;
}

//Fonction servant à retourner la taille de l'objet
MKL_INT VecN::get_n() {
  return n;
}


void VecN::get_F_elem(MKL_INT ndof, MKL_INT e, cplx **B) {
  //Cette fonction permet de pointer l'objet associée à la classe VecN, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    B    : tableau qui sert à clôner
    e    : numero d'un élément
    ndof : le nombre de degré de liberté dans chaque élément
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = ndof * e;
  *B = value + d;
}


void get_F_elem(MKL_INT ndof, MKL_INT e, cplx **b, cplx *B) {
  //Cette fonction permet de pointer l'objet associée à la classe VecN, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    ndof : la taille d'un vecteur local
    e    : numero d'un élément
    b    : le vecteur local sur e
    B    : le vecteur global
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau b contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = ndof * e;
  *b = B + d;
}



void VecN::get_F_elem(MKL_INT ndof, cplx **B) {
  //Cette fonction permet de pointer l'objet associée à la classe VecN, à l'indice e d'un élément, via le tableau B
  //Uniquement pour stocker un vecteur réduit dans la fonction collection_locales_red
  /*------------------------------------------------------------- input -------------------------------------------------------
    B    : tableau qui sert à clôner
    ndof : le nombre de degré de liberté dans chaque élément
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse d'une partie du vecteur actuel 
    /*-----------------------------------------------------------------------------------------------------------------------*/
  *B = value + ndof;
}



void get_F_elemRed(MKL_INT e, cplx **B, VecN &B_red, Maillage &Mesh) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    B    : tableau qui sert à clôner
    e    : numero d'un élément
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = Mesh.SIZE[e];
  *B = B_red.value + d;
}


void get_F_elemRed(MKL_INT e, cplx **B, cplx *B_red, Maillage &Mesh) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    e     : numero d'un élément
    B     : tableau qui sert à clôner
    B_red : tableau à clôner
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = Mesh.SIZE[e];
  *B = B_red + d;
}



MKL_INT CheckEigen(MKL_INT e1, MKL_INT e2, VecN &LAMBDA, Maillage &Mesh) {
  MKL_INT iloc, r, s, taille;
  cplx *V1, *V2;
  
  LAMBDA.get_F_elem(Mesh.elements[e1].Nred, e1, &V1);
  LAMBDA.get_F_elem(Mesh.elements[e2].Nred, e2, &V2);
  
  cplx I;
  
  s = 0; r = 1;
  if (Mesh.elements[e1].Nred < Mesh.elements[e2].Nred) {
    taille = Mesh.elements[e1].Nred;
  }
  
  else {
    taille = Mesh.elements[e2].Nred;
  }
  
  for(iloc = 0; iloc < taille; iloc++){
    I = V1[iloc] - V2[iloc];
    if (abs(I) < 1E-8) {
      s++;
    }
  }
  
  
  if (s == taille) { r = 0;}
  
  return r;
}



void CheckEigen(VecN &LAMBDA, Maillage &Mesh) {
  MKL_INT e, r;

  for(e = 0; e < Mesh.get_N_elements()-1; e++){
    r = CheckEigen(e, e+1, LAMBDA, Mesh);
    cout<<" e = "<<e<< " e+1 = "<<e+1<<" r = "<<r<<endl;
  }
  
}



//Information de la classe VecN
void VecN::print_info() {
  cout<<"taille : "<<n<<endl;
  cout<<"Vecteur : "<<endl;
  for(MKL_INT i = 0; i < n; i++)
    cout<<value[i]<<endl;
}

//Destructeur de la classe VecN
/*
VecN::~VecN() {
  delete [] value; value = nullptr;
}
*/


void produit_MatVec(MatMN &mat, VecN &vec, VecN &res) {
  MKL_INT lda = vec.n;
  MKL_INT inc = 1;
  cplx zeta = cplx(1.0,0.0), beta = cplx(0.0,0.0);
  cblas_zgemv(CblasColMajor, CblasTrans, lda, lda, &zeta, mat.value, lda, vec.value, inc, &beta, res.value, inc);
}


void AdditionVecN(VecN &v1, VecN &v2, VecN &v3) {
  //Cette fonction permet de faire l'addiation v3 = v1 + v2
   /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type VecN
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vecN       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT nn = v1.get_n();
  for(MKL_INT i = 0; i < nn; i++) {
    v3.value[i] = v1.value[i] + v2.value[i];
  }
}


void SoustractionVecN(VecN &v1, VecN &v2, VecN &v3) {
  //Cette fonction effectue la soustraction v3 = v1 - v2
   /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type VecN
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vec3       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT nn = v1.get_n();
  for(MKL_INT i = 0; i < nn; i++) {
    v3.value[i] = v1.value[i] - v2.value[i];
  }
}


void SoustractionVecN(MKL_INT n, cplx *v1, cplx *v2, cplx *v3) {
  //Cette fonction effectue la soustraction v3 = v1 - v2
   /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type VecN
    /*-------------------------------------------------------- ouput ---------------------------------------------
    vec3       : objet de meme type
    /*--------------------------------------------------------------------------------------------------------------*/
  for(MKL_INT i = 0; i < n; i++) {
    v3[i] = v1[i] - v2[i];
  }
}



void VecN::SoustractionVecN(VecN &v) {
  if (n == v.get_n()) {
    for(MKL_INT i = 0; i < n; i++) {
      value[i] = value[i] - v.value[i];
    }
  }
}


void ScalaireVecN(double alpha, VecN &v1, VecN &v2) {
  //Cette fait le produit d'un vecteur de type VecN par un scalaire de type double
  //v2 = alpha * v1
   /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type VecN
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans vec2
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT nn = v1.get_n();
  for(MKL_INT i = 0; i < nn; i++) {
    v2.value[i] = alpha * v1.value[i];
  }
}


void ScalaireVecN(std::complex<double> alpha, VecN &v1, VecN &v2) {
    //Cette fait le produit d'un vecteur de type VecN par un scalaire de type complex
  //v2 = alpha * v1
   /*---------------------------------------------------------- input -----------------------------------------------
    vec1, vec2 : objets de type VecN
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans vec2
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT nn = v1.get_n();
  for(MKL_INT i = 0; i < nn; i++) {
    v2.value[i] = alpha * v1.value[i];
  }
}


void Solution_OndePlane(VecN &U, Maillage &Mesh) {
  //Cette fonction calcule la solution analytique dans le cas d'une onde plane incidente
  
   /*---------------------------------------------------------- input -----------------------------------------------
    U       : objets de type VecN
    Mesh    : le maillage
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans U
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, ij;
  cplx *u = new cplx[Mesh.ndof];
  for(j = 0; j < Mesh.ndof; j++)
    u[j] = cplx(0.0,0.0);
  u[Mesh.n_i] = cplx(1.0,0.0);
  for(i = 0; i < Mesh.get_N_elements(); i++){
    for(j = 0; j < Mesh.ndof; j++){
      ij = j + i * Mesh.ndof;
      U.value[ij] = u[j];   
    }
  }
  free(u); u = 0;
}


void Left_Local_PlaneWave_Solution(cplx *u, Maillage &Mesh) {
  //Cette fonction calcule la solution analytique locale pour une onde plane incidente réfléchie
  
   /*---------------------------------------------------------- input -----------------------------------------------
    u       : tableau pour stocker la solution locale
    Mesh    : le maillage
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans u
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT j;
  for(j = 0; j < Mesh.ndof; j++)
    u[j] = cplx(0.0,0.0);

  u[Mesh.n_i] = cplx(1.0,0.0);
  u[Mesh.n_r] = Mesh.R;
 
}


void Right_Local_PlaneWave_Solution(cplx *u, Maillage &Mesh) {
  //Cette fonction calcule la solution analytique locale pour une onde plane transmise
  
   /*---------------------------------------------------------- input -----------------------------------------------
    u       : tableau pour stocker la solution locale
    Mesh    : le maillage
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans u
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT j;
  for(j = 0; j < Mesh.ndof; j++)
    u[j] = cplx(0.0,0.0);

  u[Mesh.n_i] = Mesh.T;
 
}



void Hetero_PlaneWave_Solution(VecN &U, Maillage &Mesh) {
  //Cette fonction calcule la solution analytique dans le cas hétérogène et pour une onde plane à incidence réfléchie et transmise
  
   /*---------------------------------------------------------- input -----------------------------------------------
    U       : objets de type VecN
    Mesh    : le maillage
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans U
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, ij;
  cplx *u = new cplx[Mesh.ndof];
 
  for(i = 0; i < Mesh.get_N_elements(); i++){

    if (Mesh.elements[i].get_ref() == 1) {
      //calcule de la solution analytique locale avec incidence réfléchie
      Left_Local_PlaneWave_Solution(u, Mesh);
    }
    
    else {
      //calcule de la solution analytique locale avec indidence transmise 
      Right_Local_PlaneWave_Solution(u, Mesh);
    }
    
    for(j = 0; j < Mesh.ndof; j++){
      ij = j + i * Mesh.ndof;
      U.value[ij] = u[j];   
    }
  }
  
  free(u); u = 0;
}




MKL_INT compare_function(const void *a,const void *b) {
  //Cette fonction effectue la comparaison des variables a et b
  MKL_INT *x = (MKL_INT *) a;
  MKL_INT *y = (MKL_INT *) b;
  if (*x > *y) return  1;
  if (*x < *y) return -1;
  return 0;
}


void tri_recursive(MKL_INT *tab, MKL_INT debut, MKL_INT fin) {
//tri recursive par Etienne Poulin – ISN – Novembre 2013  
  const MKL_INT pivot = tab[debut];
  MKL_INT pos = debut;
  MKL_INT i;
  if (debut < fin) {

    /* cette boucle permet de placer le pivot (début du tableau à trier)
       au bon endroit dans le tableau
       avec toutes les valeurs plus petites avant
       et les valeurs plus grandes après
       à la fin, la valeur pivot se trouve dans le tableau à tab[pos]
    */
    for (i = debut; i < fin; i++)
      {
	if (tab[i] < pivot) {
	  
	  tab[pos] = tab[i];
	  pos++;
	  tab[i] = tab[pos];
	  tab[pos] = pivot;
	}
      }
    
    /* Il ne reste plus qu'à rappeler la procédure de tri
       sur le début du tableau jusquà pos (exclu) : tab[pos-1]
    */
    tri_recursive(tab, debut, pos);
    
    /* et de rappeler la procédure de tri
       sur la fin du tableau à partir de la première valeur après le pivot
       tab[pos+1]
    */
    tri_recursive(tab, pos+1, fin);
  }
  
}

//constructeur de la classe Hankel
Hankel::Hankel() {
  d = 0; V = 0; P = cplx(0.0,0.0);
  k = 2*M_PI; r = -1.0;
}


MKL_INT Hankel::compute_P_and_V(Mat3 &mat, double *X, double *X0) {
  //Cette fonction définie la direction d de l'onde incidente
  /*------------------------------------------------------------- input --------------------------------
    X   : point d'évaluation
    X0  : le point source
  
    /*--------------------------------------------------------------------------------------------------*/
  MKL_INT  j;
  
  V = new cplx[3];

  d = new double[3];

  double *XX0 = new double[3];//X - X0
 
  for (j = 0; j < 3; j++){
    XX0[j] = X[j] - X0[j];
  }
  
  r = cblas_ddot(3, XX0, 1, XX0, 1);
  r = sqrt(r);
  
  if (abs(r) < 1E-10) {
    cout<<"point de discontinuité atteint "<<endl;
    return EXIT_FAILURE;
  }
  
  kr = k * r;
  
  double rr = 1.0 / r;
  
  //P = exp(i k r) / 4 * pi * r
  P = exp(cplx(0.0,kr)) / (4 * M_PI * r);
	  
	  
 //calcul de d
 for( j = 0; j < 3; j++){
    d[j] = XX0[j] * rr;
 }
 
 double *Ad = new double[3];


 //Ad = A * d,   avec A la matrice associée à l'équation de Helmholtz sur l'élément correspondant
 cblas_dgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1.0, mat.value, 3, d, 1, 0.0, Ad, 1) ;
	  
 for (j = 0; j < 3; j++){
   V[j] = Ad[j] * (cplx(-1.0, kr) / cplx(0.0, kr)) * P;
 }

  delete [] Ad; Ad = 0;
  delete [] XX0; XX0 = 0;

  return(EXIT_SUCCESS);
}

void Hankel::set_d(double *dd) {
  d = dd;
}

double * Hankel::get_d() {
  return d;
}


void Hankel::print_info() {
  //information sur la classe

  cout<<"d : "<<endl;
  cout<<" d[0] = "<<d[0]<<" d[1] = "<<d[1]<<" d[2] = "<<d[2]<<endl;
  cout<<"k = "<<k<<endl;
  cout<<"r = "<<r<<endl;
  cout<<"P = "<<P<<endl;
  cout<<"Kr = "<<kr<<endl;
  if (V) cout<<"V = "<<V[0]<<"\n"<<V[1]<<"\n"<<V[2]<<endl;
  
}

//destructeur de la classe Hankel
Hankel::~Hankel() {
  delete [] d; d = 0;
  delete [] V; V = 0;
  r = -1;
  k = -1;
}



MKL_INT compare(Onde_plane &P1, Onde_plane &P2) {
  if ((P1.K[0] == P2.K[0]) && (P1.K[1] == P2.K[1]) && (P1.K[2] == P2.K[2])){
    return 1;
  }
  return 0;
}

/********************************************************************************** PARTIE MAILLAGE SPHERIQUE ****************************************************************/

//Fonction pour transformation la matrice pleine associée à cette classe en matrice sparse
void MatMN::transforme_Full_To_Sparse(MailleSphere &MSph, Sparse &AS) {
   /*---------------------------------------------------- input ---------------------------------------
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iglob, jglob, l;
  
  AS.nnz = 0;
  for (iglob = 0; iglob < n; iglob++){
    for (jglob = 0; jglob < m; jglob++){
      if (abs(value[iglob * m + jglob]) > 1E-10)
	AS.nnz++;
    }
  }

  //allocation des tableaux a, ia et ja
  AS.a =  new cplx[AS.nnz];
  AS.ia =  new MKL_INT[n+1];
  AS.ja =  new MKL_INT[AS.nnz];
  
  //initialisation des tableaux a, ja et ia
  for (iglob = 0; iglob < AS.nnz; iglob++){
    AS.a[iglob] = cplx(0.0,0.0);
    AS.ja[iglob] = 0;
  }
  for (iglob = 0; iglob < n+1; iglob++)
    AS.ia[iglob] = 0;

  //remplissage des tableaux a, ia et ja
  l = 0;
  AS.ia[0] = 1;
  for (iglob = 0; iglob < n; iglob++){
    for (jglob = 0; jglob < m; jglob++){
      if (abs(value[iglob * m + jglob]) > 1E-10){
	 AS.a[l] = value[iglob * m + jglob];
	 AS.ja[l] = jglob+1;
	 l++;
      }
    }
    AS.ia[iglob+1] = l;
  }
  cout<<"ia[n] = "<<AS.ia[n]<<" nnz = "<<AS.nnz<<endl;
  //for (iglob = 0; iglob < nnz; iglob++){
  //cout<<"iglob = "<<iglob<<" a["<<iglob<<"] = "<<a[iglob]<<" ja["<<iglob<<"] = "<<ja[iglob]<<endl;
  //}
}



//Cette fonction sert à transformer une matrice A_skyline en matrice pleine
void MatMN::transforme_Afull(A_skyline &A_glob, MailleSphere &MSph) {
  /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, iglob, jglob, i, j, jbloc, ibloc;
  cplx *AA;
  /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*-------------------------------------------------------------------------------------------------*/
  cout<<" transformation de la matrice en full "<<endl;
  cout<<value<<endl;
  // remplissage de A_full = value
  for(iglob = 0; iglob < m; iglob++){
    for(jglob = 0; jglob < n; jglob++){
      value[jglob * m + iglob] = cplx(0.0,0.0);
    }
  }
  for (i = 0; i < MSph.get_N_elements(); i++){
    for(j = 0; j < 5; j++){
      if (j == 0) {
	ibloc = i;
	jbloc = i;
      }
      else if (j > 0){
	ibloc = i;
	jbloc = MSph.elements[i].voisin[j-1];
      }
      if (jbloc >= 0){
	AA = A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + j].get_Aloc();
	for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
	  iglob = local_to_glob(iloc, ibloc, A_glob.get_n_dof());
	  
	  for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	    jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	    //cout<<"i = "<<i<<" j = "<<j<<" iglob = "<<iglob<<" jglob = "<<jglob<<" A_glob.get_n_A() = "<<A_glob.get_n_A()<<endl;
    
	    value[jglob * m + iglob] = AA[jloc * A_glob.get_n_dof() + iloc];
	  }
	}
      }
    }
  }
  cout<<" transformation de la matrice en full reussie"<<endl;
}



void getNumberOfNeighbours(MailleSphere &MSph, MKL_INT *NVS){
  //Cette fonction retourne un tableau contenant le nombre de voisin des tetra
  /*-------------------------------------------------------------- input -------------------------------------
    MSph : le maillage
    NVS  : NVS[i] le nombre de voisin de l'élément i
    /*-------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, s;
  for (i = 0; i < MSph.get_N_elements(); i++){
    s = 0;
    for (j = 0; j < 4; j++){
      if (MSph.elements[i].voisin[j] >= 0)
	s++;
    }
    NVS[i] = s;
  }
}


void global_to_localRed(MKL_INT iglob, MKL_INT *tetra, MKL_INT *iloc, MailleSphere &MSph) {
  //Cette fonction pour récupérer le numero local d'une fonction de base et le numero de l'élément correspondant à partir du numero global de cette fonction de base  et du nombre de degré de liberté en chaque élément
 /*-------------------------------------------------- input ------------------------------------------
    iglob : numero global d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    iloc  : numero local d'une fonction de base
    /*------------------------------------------------  ouput    ------------------------------------------
    iloc
    tetra
  /*---------------------------------------------------------------------------------------------------*/
  MKL_INT e, r;

  if( iglob < MSph.SIZE[1]){
    *tetra = 0;
    *iloc = iglob;
  }
  else if ((iglob >= MSph.SIZE[1]) &&(iglob < MSph.SIZE[MSph.get_N_elements()-1])){
    e = 2;
    r = -1;
    while((e < MSph.get_N_elements()) && (r < 0)) {
      if (iglob < MSph.SIZE[e]){
	*tetra = e-1;
	*iloc = iglob - MSph.SIZE[*tetra];
	r = 0;
      }
      e++;
      //cout<<" tetra = "<<*tetra<<" e_i = "<<e<<endl;
    }
  }
  else {
    *tetra = MSph.get_N_elements()-1;
    *iloc = iglob - MSph.SIZE[*tetra];
  }
}



MKL_INT local_to_globRed(MKL_INT iloc, MKL_INT tetra, MailleSphere &MSph){
  //cette fonction sert à générer le numero global d'une fonction de base à partir de son numero local et du numero de l'élément dont il est associé
  /*-------------------------------------------------- input ------------------------------------------
    iloc  : numero local d'une fonction de base
    tetra : numero global d'un tetra
    ndof  : nombre de degrés de libertés
    /*------------------------------------------------  ouput    ------------------------------------------
    iglob : numero global d'une fonction de base
  /*---------------------------------------------------------------------------------------------------*/
  MKL_INT iglob, s;
  if (tetra == 0){
    iglob = iloc;
  }

  else {
    iglob = iloc + MSph.SIZE[tetra];
  }

  
  return iglob;
}




//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  
  nnz = 0;//nombre de non zeros de la matrice
  
  for (i = 0; i < MSph.get_N_elements(); i++){
    
    AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();
    for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
      for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	if (abs(AA[jloc * A_glob.get_n_dof() + iloc]) > 1E-10){
	  nnz++;
	}
      }
    }
    
    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      
      if (MSph.elements[i].voisin[j] >= 0) {
       
	AA = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + j+1].get_Aloc();
	for(iloc = 0; iloc < A_glob.get_n_dof(); iloc++){
	  for(jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	    if (abs(AA[jloc * A_glob.get_n_dof() + iloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
	  
      }
    }
    
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin=nullptr, *numero_local=nullptr;
  tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(MSph.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * MSph.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_local(A_glob.get_n_dof(), iglob, &ibloc, &iloc);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	cplx *bloc = nullptr;
	
	bloc = A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + numero_local[j]].get_Aloc();
	
	for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	  
	  jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	  
	  val = bloc[jloc * A_glob.get_n_dof() + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = nullptr;
  delete [] numero_local; numero_local = nullptr;
}


/*---------------------------------------------------------------------- SPARSE FOR A_sylineRed -------------------------------------------------------*/

//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    A_red : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  
  nnz = 0;//nombre de non zeros de la matrice
  
  for (i = 0; i < MSph.get_N_elements(); i++){

    //Pour les blocs diagonaux
    AA = A_red.Tab_A_locRed[i * A_red.get_n_inter() + 0].get_Aloc();
    
    for(iloc = 0; iloc < MSph.elements[i].Nred; iloc++){
      for(jloc = 0; jloc < MSph.elements[i].Nred; jloc++){
	if (abs(AA[jloc * MSph.elements[i].Nred + iloc]) > 1E-10){
	  nnz++;
	}
      }
    }

    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      ibloc = i;
      jbloc = MSph.elements[i].voisin[j];
      AA = A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + (j+1)].get_Aloc();
      
      if (jbloc >= 0) {	  
	for(iloc = 0; iloc < MSph.elements[ibloc].Nred; iloc++){
	  for(jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	    if (abs(AA[jloc * MSph.elements[ibloc].Nred + iloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
      }
      
    }
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin=nullptr, *numero_local=nullptr;
  tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(MSph.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * MSph.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_localRed(iglob, &ibloc, &iloc, MSph);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	  
	  jglob = local_to_globRed(jloc, jbloc, MSph);
	  
	  val = (A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + numero_local[j]].get_Aloc())[jloc * MSph.elements[ibloc].Nred + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = nullptr;
  delete [] numero_local; numero_local = nullptr;
}


void MatMN::transforme_sparse_To_full(MailleSphere &MSph, Sparse &AS){
  int i, j, l;
  for (i = 0; i < n; i++){
    for (l = 0; l < AS.ia[i+1] - AS.ia[i]; l++){
      j = AS.ja[AS.ia[i]+l-1]-1;
      value[i * n + j] = AS.a[AS.ia[i]+l-1];
    }
  }
}


void Solution_OndePlane(VecN &U, MailleSphere &MSph) {
  //Cette fonction calcule la solution analytique dans le cas d'une onde plane incidente
  
   /*---------------------------------------------------------- input -----------------------------------------------
    U       : objets de type VecN
    MSph    : le maillage
    /*-------------------------------------------------------- ouput ---------------------------------------------
    Le résultat est stocké dans U
    /*--------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j, ij;
  cplx *u = new cplx[MSph.ndof];
  for(j = 0; j < MSph.ndof; j++)
    u[j] = cplx(0.0,0.0);
  u[MSph.n_i] = cplx(1.0,0.0);
  for(i = 0; i < MSph.get_N_elements(); i++){
    for(j = 0; j < MSph.ndof; j++){
      ij = j + i * MSph.ndof;
      U.value[ij] = u[j];   
    }
  }
  free(u); u = 0;
}



void get_F_elemRed(MKL_INT e, cplx **B, VecN &B_red, MailleSphere &MSph) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    B    : tableau qui sert à clôner
    e    : numero d'un élément
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = MSph.SIZE[e];
  *B = B_red.value + d;
}


void get_F_elemRed(MKL_INT e, cplx **B, cplx *B_red, MailleSphere &MSph) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B 
  /*------------------------------------------------------------- input -------------------------------------------------------
    e     : numero d'un élément
    B     : tableau qui sert à clôner
    B_red : tableau à clôner
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = MSph.SIZE[e];
  *B = B_red + d;
}




//Cette fonction sert à transformer une matrice A_skyline en matrice pleine
void MatMN::transforme_Afull(A_skylineRed &A_red, MailleSphere &MSph) {
  /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  MKL_INT iloc, jloc, iglob, jglob, i, j, jbloc, ibloc;
  cplx *AA;
  /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*-------------------------------------------------------------------------------------------------*/
  cout<<" transformation de la matrice en full "<<endl;
  cout<<value<<endl;
  // remplissage de A_full = value
  for(iglob = 0; iglob < m; iglob++){
    for(jglob = 0; jglob < n; jglob++){
      value[jglob * m + iglob] = cplx(0.0,0.0);
    }
  }
  
  for (i = 0; i < MSph.get_N_elements(); i++){
    
    ibloc = i;
    jbloc = i;
    AA = A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + 0].get_Aloc();
    
    for(iloc = 0; iloc < MSph.elements[ibloc].Nred; iloc++){
      iglob = local_to_globRed(iloc, ibloc, MSph);
      
      for(jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	jglob = local_to_globRed(jloc, jbloc, MSph);
    
	value[jglob * m + iglob] += AA[jloc * MSph.elements[ibloc].Nred + iloc];
      }
    }

    for(j = 0; j < 4; j++){
      
      ibloc = i;
      jbloc = MSph.elements[i].voisin[j];
      
      if (jbloc >= 0){
	AA = A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + (j+1)].get_Aloc();
	
	for(iloc = 0; iloc < MSph.elements[ibloc].Nred; iloc++){
	  iglob = local_to_globRed(iloc, ibloc, MSph);
	  
	  for(jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	    jglob = local_to_globRed(jloc, jbloc, MSph);
	    //cout<<"i = "<<i<<" j = "<<j<<" iglob = "<<iglob<<" jglob = "<<jglob<<" A_glob.get_n_A() = "<<A_glob.get_n_A()<<endl;
	    
	    value[jglob * m + iglob] += AA[jloc * MSph.elements[ibloc].Nred + iloc];
	  }
	}
      }
    }
  }
    cout<<" transformation de la matrice en full reussie"<<endl;
}


double norme_2(MKL_INT n, cplx *U) {

  double r = sqrt(abs(compute_XdotY(n, U, U)));

  return r;
}



void transform_P_red_to_wide_skyline(A_skyline &P_red, A_skylineRed &S_red, MailleSphere &MSph){

  cplx *SS;
  MKL_INT taille, i;
  for(i = 0; i < MSph.get_N_elements(); i++){
    
    taille = MSph.ndof * MSph.elements[i].Nred;

    cplx *a = new cplx[taille];
    
    SS = S_red.Tab_A_locRed[i * S_red.get_n_inter() + 0].get_Aloc();
    
    store_P_and_transpose_P(i, a, P_red, "selfP", MSph);
    
    cblas_zcopy(taille, a, 1, SS, 1) ;
    
    delete [] a; a = 0;
  }
}


void transform_adjointP_red_to_wide_skyline(A_skyline &P_red, A_skylineRed &S_red, MailleSphere &MSph){

  cplx *SS;
  MKL_INT taille, i;
  for(i = 0; i < MSph.get_N_elements(); i++){
    
    taille = MSph.ndof * MSph.elements[i].Nred;

    cplx *a = new cplx[taille];
    
    SS = S_red.Tab_A_locRed[i * S_red.get_n_inter() + 0].get_Aloc();

    store_P_and_transpose_P(i, a, P_red, "adjtP", MSph);

    cblas_zcopy(taille, a, 1, SS, 1) ;

    delete [] a; a = 0;
  }
}



void get_F_elemRed(MKL_INT i, MKL_INT e, MKL_INT *TAILLE, cplx **B, VecN &B_red, MailleSphere &MSph) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B
  //Elle définit pour le gradient conjugué
  /*------------------------------------------------------------- input -------------------------------------------------------
    i        : l'exposant de notre mode de réduction
    e        : numero d'un élément
    TAILLE   : tableau des dimensions réduites des espaces de Galerkin
    B        : tableau qui sert à clôner
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = TAILLE[e * 10 + (i-1)];
  *B = B_red.value + d;
}


void get_F_elemRed(MKL_INT i, MKL_INT e, MKL_INT *TAILLE, cplx **B, cplx *B_red, MailleSphere &MSph) {
  //Cette fonction permet de pointer l'objet B_red, à l'indice e d'un élément, via le tableau B
  //Elle définit pour le gradient conjugué
  /*------------------------------------------------------------- input -------------------------------------------------------
    i        : l'exposant de notre mode de réduction
    e        : numero d'un élément
    TAILLE   : tableau des dimensions réduites des espaces de Galerkin
    B        : tableau qui sert à clôner
    B_red    : tableau à clôner
    /*------------------------------------------------------------ ouput -----------------------------------------------------
    En sortie, on aura le tableau B contenant l'adresse de l'objet à l'indice e de l'élément
    /*-----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT d = TAILLE[e * 10 + (i-1)];
  *B = B_red + d;
}
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/

void A_skylineRed::ProdA_skyline_FullFull_TEST(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph) {

  //Cette fonction calcule le produit Matrice A_syline-Vecteur V = A_sky U
  //U et V sont des vecteurs pleins
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    U       : Vecteur dont on veut faire le produit avec la matrice
    V       : Vecteur qui contiendra le résultat
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- output -----------------------------------------------------------------------------------------
                                                                     V = A_sky U
    /*------------------------------------------------------------------------------------- intermédaire ------------------------------------------------------------------------------------
    AA      : pour clôner la matrice skyline sur chaque élément
    u       : ........... le vecteur U sur chaque élément
    v       : ...................... V ..................
    iloc    : boucle sur les lignes
    jloc    : boucle sur les colonnes
    j       : numero local du voisin
    W1      : pour stocker le produit local AA * u
    W2      : pour copier W2 dans W1 par la fonction blas zcopy, donc initialiser à 0 W1
              après chaque calcule de AA * u
    /*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, iloc, jloc, f, j;
  
  cplx *AA, *u, *v, alpha, beta;

  alpha = cplx(1.0, 0.0);
  beta = cplx(1.0, 0.0);
  for(e = 0; e < MSph.get_N_elements(); e++){
    
    AA = Tab_A_locRed[e * n_inter + 0].get_Aloc();//clônage de A_sky sur e
    
    get_F_elem(MSph.ndof, e, &u, U);//clônage de V sur e
    
    get_F_elem(MSph.ndof, e, &v, V);//clônage de V sur e

    //produit local V = A_ky U
    prodMV_ColMajor(AA, NRED[e * 10 + (i-1)], NRED[e * 10 + (i-1)], alpha, u, NRED[e * 10 + (i-1)], v, beta);
    
  }


  for(e = 0; e < MSph.get_N_elements(); e++){
    
    for (j = 0; j < 4; j++){
      
      if (MSph.elements[e].voisin[j] >= 0) {//test d'existence du voisin numero j 
	
	f = MSph.elements[e].voisin[j];//numero global du voisin
	
	AA = Tab_A_locRed[e * n_inter + j+1].get_Aloc();//matrice locale d'interaction element-voisin
	
	get_F_elem(MSph.ndof, f, &u, U);//clônage de V sur e
	
	get_F_elem(MSph.ndof, e, &v, V);//clônage de V sur e
	
	//produit local V = A_sky U
	prodMV_ColMajor(AA, NRED[e * 10 + (i-1)], NRED[f * 10 + (i-1)], alpha, u, NRED[f * 10 + (i-1)], v, beta);
	
      }
    }
  }
  
 
}




void A_skylineRed::ProdA_skyline_RedFull_TEST(MKL_INT i, MKL_INT *NRED, cplx *U, cplx *V, MailleSphere &MSph) {

  //Cette fonction calcule le produit Matrice A_syline-Vecteur V = A_sky U
  //U est un vecteur réduit et V un vecteur plein
  /*---------------------------------------------------------------------------------------- input ------------------------------------------------------------------------------------------
    i       : l'exposant de notre mode de réduction
    NRED    : tableau contenant les dimensions des espaces réduits de Galerkin
    U       : Vecteur dont on veut faire le produit avec la matrice
    V       : Vecteur qui contiendra le résultat
    MSph    : le maillage
    /*-------------------------------------------------------------------------------------- output -----------------------------------------------------------------------------------------
                                                                     V = A_sky U
    /*------------------------------------------------------------------------------------- intermédaire ------------------------------------------------------------------------------------
    AA      : pour clôner la matrice skyline sur chaque élément
    u       : ........... le vecteur U sur chaque élément
    v       : ...................... V ..................
    iloc    : boucle sur les lignes
    jloc    : boucle sur les colonnes
    j       : numero local du voisin
    /*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, iloc, jloc, f, j;
  
  cplx *AA, *u, *v, alpha, beta;

  alpha = cplx(1.0, 0.0);
  beta = cplx(1.0, 0.0);
  
  for(e = 0; e < MSph.get_N_elements(); e++){
    
    AA = Tab_A_locRed[e * n_inter + 0].get_Aloc();//Adresse du bloc d'interaction element-element
    
    get_F_elemRed(e, &u, U, MSph);//Adresse de U associée à l'élément e
    
    get_F_elem(MSph.ndof, e, &v, V);//Adresse de V associée à l'élément e

    //produit local v = beta * v + alpha * AA * u
    prodMV_ColMajor(AA, NRED[e * 10 + (i-1)], NRED[e * 10 + (i-1)], alpha, u, NRED[e * 10 + (i-1)], v, beta);
    
  }


  for(e = 0; e < MSph.get_N_elements(); e++){
    
    for (j = 0; j < 4; j++){
      
      if (MSph.elements[e].voisin[j] >= 0) {//test d'existence du voisin numero j 
	
	f = MSph.elements[e].voisin[j];//numero global du voisin
	
	AA = Tab_A_locRed[e * n_inter + j+1].get_Aloc();//Adresse du bloc d'interaction element-voisin
	
	get_F_elemRed(f, &u, U, MSph);//Adresse de U associée à l'élément f
	
	get_F_elem(MSph.ndof, e, &v, V);//Adresse de V associée à l'élément e
	
	//produit local v = beta * v + alpha * AA * u
	prodMV_ColMajor(AA, NRED[e * 10 + (i-1)], NRED[f * 10 + (i-1)], alpha, u, NRED[f * 10 + (i-1)], v, beta);
	
      }
    }
  }
  
 
}




MKL_INT local_to_globRed(MKL_INT shift, MKL_INT iloc, MKL_INT tetra, MKL_INT *TAILLE){
  //cette fonction sert à générer le numero global d'une fonction de base à partir de son numero local et du numero de l'élément dont il est associé
  /*-------------------------------------------------- input ------------------------------------------
    shift     : le shift du tableau TAILLE
    iloc      : numero local d'une fonction de base
    tetra     : numero global d'un tetra
    TAILLE    : tableau des dimensions des espaces réduits de Galerkin en version MUMPS
    /*------------------------------------------------  ouput    ------------------------------------------
    iglob : numero global d'une fonction de base
  /*---------------------------------------------------------------------------------------------------*/
  MKL_INT iglob;

  
  if (tetra == 0){
    iglob = iloc;
  }

  else {
    iglob = iloc + TAILLE[tetra + shift];
  }

  
  return iglob;
}
/*-------------------------------------------------------------------------------------------------------------------------------------*/

//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR(A__skyline &Asky, cplx *Asky_mem, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    Asky       : structure définissant la taille de Asky_mem
    Asky_mem   : matrice en blocs contenant le systeme linéaire
    MSph       : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s, taille;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    taille: taille des blocs au carré
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  
  nnz = 0;//nombre de non zeros de la matrice

  taille = MSph.ndof * MSph.ndof;
  
  for (i = 0; i < MSph.get_N_elements(); i++){
    
    get_A__skyline_Block(Asky, Asky_mem, i, 0, taille, &AA);
    for(iloc = 0; iloc < MSph.ndof; iloc++){
      for(jloc = 0; jloc < MSph.ndof; jloc++){
	if (abs(AA[jloc * MSph.ndof + iloc]) > 1E-10){
	  nnz++;
	}
      }
    }
    
    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      
      if (MSph.elements[i].voisin[j] >= 0) {
       
	get_A__skyline_Block(Asky, Asky_mem, i, j+1, taille, &AA);
	for(iloc = 0; iloc < MSph.ndof; iloc++){
	  for(jloc = 0; jloc < MSph.ndof; jloc++){
	    if (abs(AA[jloc * MSph.ndof + iloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
	  
      }
    }
    
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage des tableaux a et ja et ia -----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_local(MSph.ndof, iglob, &ibloc, &iloc);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < MSph.ndof; jloc++){
	  
	  jglob = local_to_glob(jloc, jbloc, MSph.ndof);
	  
	  get_A__skyline_Block(Asky, Asky_mem, ibloc, numero_local[j], taille, &AA);

	  val = AA[jloc * MSph.ndof + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}


//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse CSR
void Sparse::transforme_A_skyline_to_Sparse_CSR_version_reduite(A__skyline &Ared, cplx *Ared_mem, MailleSphere &MSph) {
  /*---------------------------------------------------- input ---------------------------------------
    A_red : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;

  MKL_INT taille = MSph.ndof * MSph.ndof;
  
  nnz = 0;//nombre de non zeros de la matrice
  
  for (i = 0; i < MSph.get_N_elements(); i++){

    //Pour les blocs diagonaux
    cplx *AA;
    get_A__skyline_Block(Ared, Ared_mem, i, 0, taille, &AA);
    
    for(iloc = 0; iloc < MSph.elements[i].Nred; iloc++){
      for(jloc = 0; jloc < MSph.elements[i].Nred; jloc++){
	if (abs(AA[jloc * MSph.elements[i].Nred + iloc]) > 1E-10){
	  nnz++;
	}
      }
    }

    //Pour les autres blocs
    for(j = 0; j < 4; j++){
      ibloc = i;
      jbloc = MSph.elements[i].voisin[j];
      cplx *BB;
      get_A__skyline_Block(Ared, Ared_mem, ibloc, j+1, taille, &BB);
      
      if (jbloc >= 0) {	  
	for(iloc = 0; iloc < MSph.elements[ibloc].Nred; iloc++){
	  for(jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	    if (abs(BB[jloc * MSph.elements[ibloc].Nred + iloc]) > 1E-10){
	      nnz++;
	    }
	  }
	}
      }
      
    }
  }
  
  cout<<"nnz = "<<nnz<<endl;
  
  //allocation des tableaux a, ia, ja
  a =  new cplx[nnz];
  ia =  new MKL_INT[n+1];
  ja =  new MKL_INT[nnz];

  MKL_INT *tetra_voisin=nullptr, *numero_local=nullptr;
  tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_localRed(iglob, &ibloc, &iloc, MSph);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	  
	  jglob = local_to_globRed(jloc, jbloc, MSph);

	  cplx *AA;
	  
	  get_A__skyline_Block(Ared, Ared_mem, ibloc, numero_local[j], taille, &AA);
	  
	  val = AA[jloc * MSph.elements[ibloc].Nred + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = nullptr;
  delete [] numero_local; numero_local = nullptr;
}

void debugg_A__skyline_to_A_skyline_to_Sparse_CSR(MailleSphere &MSph, const char chaine[]){

  MKL_INT i, n_RHS;
  
  A__skyline Asky;
  
  cplx *Asky_mem;
  
  build_A__skyline(MSph.ndof, 5, MSph.get_N_elements(), Asky);
  
  Asky_mem = new cplx[Asky.n_A];
  
  for (i = 0; i < Asky.n_A; i++) {
    Asky_mem[i] = cplx(0.0,0.0);
  }

  A_skyline A_glob;
  A_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(); // Construit les blocks et la structure en mémoire continue
  
  
#pragma omp parallel
  {
    ASSEMBLAGE_DE_Askyline(A_glob, MSph, chaine);
  }
  
#pragma omp parallel
  {
    ASSEMBLAGE_DE_A__skyline(Asky, Asky_mem, MSph, chaine);
  }

  n_RHS = MSph.get_N_elements() * MSph.ndof;
  
  Sparse ASx;
  
  ASx.m = n_RHS;
  ASx.n = n_RHS;

  Sparse ASy;
  
  ASy.m = n_RHS;
  ASy.n = n_RHS;

  cout<<"transformation de A_glob en sparse : "<<endl;
  ASx.transforme_A_skyline_to_Sparse_CSR(A_glob, MSph);

  cout<<"nnz(A_glob) = "<<ASx.nnz<<endl;

  cout<<"transformation de Asky en sparse : "<<endl;
  ASy.transforme_A_skyline_to_Sparse_CSR(Asky, Asky_mem, MSph);

  cout<<"nnz(Asky) = "<<ASy.nnz<<endl;

  double maxx_a, minn_a;
  MKL_INT maxx_ja, minn_ja, maxx_ia, minn_ia;
  maxx_a = 0.0; minn_a = 1000.0;
  maxx_ja = 0; minn_ja = 1000;
  maxx_ia = 0; minn_ia = 1000;
  if (ASx.nnz == ASy.nnz) {
    for(i = 0; i < ASx.nnz; i++) {
      maxx_a = max(maxx_a, abs(ASx.a[i] - ASy.a[i]));
      minn_a = min(minn_a, abs(ASx.a[i] - ASy.a[i]));

      maxx_ja = max(maxx_ja, abs(ASx.ja[i] - ASy.ja[i]));
      minn_ja = min(minn_ja, abs(ASx.ja[i] - ASy.ja[i]));
    }

    for(i = 0; i < n_RHS; i++) {
      maxx_ia = max(maxx_ia, abs(ASx.ia[i] - ASy.ia[i]));
      minn_ia = min(minn_ia, abs(ASx.ia[i] - ASy.ia[i]));
    }

    cout<<" maxx_a = "<<maxx_a<<" minn_a = "<<minn_a<<" maxx_ja = "<<maxx_ja<<" minn_ja = "<<minn_ja<<" maxx_ia = "<<maxx_ia<<" minn_ia = "<<minn_ia<<endl;
  }

  delete [] Asky_mem; Asky_mem = 0;
}

void debugg_A__skyline_to_A_skyline_to_Sparse_CSR_version_reduite(MailleSphere &MSph, const char chaine[]){

 cplx *LAMBDA = new cplx[MSph.ndof * MSph.get_N_elements()];
  for(MKL_INT i = 0; i < MSph.ndof * MSph.get_N_elements(); i++)
    LAMBDA[i] = cplx(0.0, 0.0);

  A__skyline Asky;

  cplx *Asky_mem;
  
  build_A__skyline(MSph.ndof, 5, MSph.get_N_elements(), Asky);
  
  Asky_mem = new cplx[Asky.n_A];
  
  for (MKL_INT i = 0; i < Asky.n_A; i++) {
    Asky_mem[i] = cplx(0.0,0.0);
  }


  A__skyline Ared;

  cplx *Ared_mem;
  
  build_A__skyline(MSph.ndof, 5, MSph.get_N_elements(), Ared);
  
  Ared_mem = new cplx[Ared.n_A];
  
  for (MKL_INT i = 0; i < Ared.n_A; i++) {
    Ared_mem[i] = cplx(0.0,0.0);
  }

	
  
  A__skyline Msky;
  
  cplx *Msky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Msky);
  
  Msky_mem = new cplx[Msky.n_A];
  
  for (MKL_INT i = 0; i < Msky.n_A; i++) {
    Msky_mem[i] = cplx(0.0,0.0);
  }

  A__skyline Psky;
  
  cplx *Psky_mem;
  
  build_A__skyline(MSph.ndof, 1, MSph.get_N_elements(), Psky);
  
  Psky_mem = new cplx[Psky.n_A];
  
  for (MKL_INT i = 0; i < Psky.n_A; i++) {
    Psky_mem[i] = cplx(0.0,0.0);
  }
  
  cout<<" Pour Msky : "<<endl;
  cout<<"n_dof = "<<Msky.n_dof<<" n_elem = "<<Msky.n_elem<<" n_inter = "<<Msky.n_inter<<" n_A = "<<Msky.n_A<<endl;

   cout<<" Pour Psky : "<<endl;
  cout<<"n_dof = "<<Psky.n_dof<<" n_elem = "<<Psky.n_elem<<" n_inter = "<<Psky.n_inter<<" n_A = "<<Psky.n_A<<endl;
  
#pragma omp parallel
  {
    ASSEMBLAGE_DelaMatrice_M__Skyline(Msky, Msky_mem, MSph, chaine);
  }

#pragma omp parallel
  {
    ASSEMBLAGE_DE_A__skyline(Asky, Asky_mem, MSph, chaine);
  }
  
  A_skyline M_glob;
  M_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  M_glob.set_n_inter(5); // fixe le nombre d'interactions 
  M_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  M_glob.set_n_A(); // calcule le nombre de coeff a stocker
  M_glob.build_mat(); // Construit les blocks et la structure en mémoire continue

#pragma omp parallel
  {
    ASSEMBLAGE_DelaMatrice_M_Skyline(M_glob, MSph, chaine);
  }
  A_skyline P_red;
  P_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  P_red.set_n_inter(1); // fixe le nombre d'interactions 
  P_red.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  P_red.set_n_A(); // calcule le nombre de coeff a stocker
  P_red.build_mat(); // Construit les blocks et la structure en mémoire continue
  //	P_glob.print_info()


  A_skyline A_glob;
  A_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(); // Construit les blocks et la structure en mémoire continue

#pragma omp parallel
  {
    ASSEMBLAGE_DE_Askyline(A_glob, MSph, chaine);
  }
  
  // VecN SIGMA;
  // SIGMA.n = compute_sizeBlocRed(M_glob, MSph);

  // SIGMA.Allocate();
  
  MKL_INT size_Vec = compute_sizeBlocRed(Msky, Msky_mem, MSph);
  
  cout<<"size_Vec = "<<size_Vec<<endl;

   VecN SIGMA;
   SIGMA.n = size_Vec;
   
   SIGMA.Allocate();

  MKL_INT sizeA_red = compute_sizeForA_red(MSph);

  cout<<"sizeA_red = "<<sizeA_red<<endl;

  A_skylineRed A_red;
  A_red.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  A_red.set_n_inter(5); // fixe le nombre d'interactions 
  A_red.set_n_A(sizeA_red); // calcule la taille totale de la matrice
  A_red.build_mat(MSph); // Construit les blocks et la structure en mémoire continue
  A_red.print_info();

  double facteur = 1.0 / 1000.0;

   cout<<"Decomposition de Msky : "<<endl;
  auto t3 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs(Msky, Msky_mem, Psky, Psky_mem, LAMBDA, MSph);
  auto t4 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_T2 = t4 - t3;

  double TT2 = float_T2.count() * facteur;
   
  cout<<"Decomposition de M_glob : "<<endl;
  auto t1 = std::chrono::high_resolution_clock::now();
  DecompositionGlobaleDesBlocs(M_glob, P_red, SIGMA, MSph);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli> float_T1 = t2 - t1;

  double TT1 = float_T1.count() * facteur;
  
   auto t21 = std::chrono::high_resolution_clock::now();
  produit_A_skyline_P_skyline(Asky, Asky_mem, Psky, Psky_mem, Ared, Ared_mem, MSph);
  auto t22 = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double, std::milli> float_Z1 = t22 - t21;

  double Z1 = float_Z1.count() * facteur;


  auto t23 = std::chrono::high_resolution_clock::now();
  produit_A_skyline_P_skyline(A_glob, P_red, A_red, MSph);
  
  auto t24 = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double, std::milli> float_Z2 = t24 - t23;

  

  MKL_INT i;
  
  Sparse ASx;
  
  ASx.m = SIGMA.n;
  ASx.n = SIGMA.n;

  Sparse ASy;
  
  ASy.m = ASx.m;
  ASy.n = ASx.m;

  cout<<"transformation de A_glob en sparse : "<<endl;
  ASx.transforme_A_skyline_to_Sparse_CSR(A_red, MSph);

  cout<<"nnz(A_glob) = "<<ASx.nnz<<endl;

  cout<<"transformation de Asky en sparse : "<<endl;
  ASy.transforme_A_skyline_to_Sparse_CSR(Ared, Ared_mem, MSph);

  cout<<"nnz(Asky) = "<<ASy.nnz<<endl;

   
  ASx.transporte_Value_and_Columns_To_matlab("/home/ibrahima/Documents/sauvarge-26-06-23/values_&_cols_Ared_glob");
  

  ASx.transporte_rows_To_matlab("/home/ibrahima/Documents/sauvarge-26-06-23/rows_Ared_glob");

  ASy.transporte_Value_and_Columns_To_matlab("/home/ibrahima/Documents/sauvarge-26-06-23/values_&_cols_Ared_sky");
  
  
  ASy.transporte_rows_To_matlab("/home/ibrahima/Documents/sauvarge-26-06-23/rows_Ared_sky");

  double maxx_a, minn_a;
  MKL_INT maxx_ja, minn_ja, maxx_ia, minn_ia;
  maxx_a = 0.0; minn_a = 1000.0;
  maxx_ja = 0; minn_ja = 1000;
  maxx_ia = 0; minn_ia = 1000;
  if (ASx.nnz == ASy.nnz) {
    for(i = 0; i < ASx.nnz; i++) {
      maxx_a = max(maxx_a, abs(ASx.a[i] - ASy.a[i]));
      minn_a = min(minn_a, abs(ASx.a[i] - ASy.a[i]));

      maxx_ja = max(maxx_ja, abs(ASx.ja[i] - ASy.ja[i]));
      minn_ja = min(minn_ja, abs(ASx.ja[i] - ASy.ja[i]));
    }

    for(i = 0; i < ASx.n; i++) {
      maxx_ia = max(maxx_ia, abs(ASx.ia[i] - ASy.ia[i]));
      minn_ia = min(minn_ia, abs(ASx.ia[i] - ASy.ia[i]));
    }

    cout<<" maxx_a = "<<maxx_a<<" minn_a = "<<minn_a<<" maxx_ja = "<<maxx_ja<<" minn_ja = "<<minn_ja<<" maxx_ia = "<<maxx_ia<<" minn_ia = "<<minn_ia<<endl;
  }

  delete [] Asky_mem; Asky_mem = 0;
  delete [] Msky_mem; Msky_mem = 0;
  delete [] Psky_mem; Psky_mem = 0;
  delete [] LAMBDA; LAMBDA = 0;
}


/*---------------------------------------------------------------------------------------------------------------------------------*/

//Cette fonction permet de transformer une matrice A_skyline en matrice Sparse RCS
void transforme_A_skyline_to_Sparse_CSR(A__skyline &Asky, cplx *Asky_mem, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    Asky       : structure définissant la taille de Asky_mem
    Asky_mem   : matrice en blocs contenant le systeme linéaire
    MSph       : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s, taille;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    taille: taille des blocs au carré
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;
  

  taille = MSph.ndof * MSph.ndof;

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;
  
  l = 0;
  ja[0] = 1;

  /*---------------------------------- remplissage des tableaux a et ja et ia -----------------------*/
  for (jglob = 0; jglob < n; jglob++){
    global_to_local(MSph.ndof, jglob, &jbloc, &jloc);
    
    tetra_voisin[0] = jbloc;
    tetra_voisin[1] = MSph.elements[jbloc].voisin[0];
    tetra_voisin[2] = MSph.elements[jbloc].voisin[1];
    tetra_voisin[3] = MSph.elements[jbloc].voisin[2];
    tetra_voisin[4] = MSph.elements[jbloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == jbloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[jbloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[jbloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[jbloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[jbloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      ibloc = tetra_voisin[j];
      
      if (ibloc >= 0){
	
	for (iloc = 0; iloc < MSph.ndof; iloc++){
	  
	  iglob = local_to_glob(iloc, ibloc, MSph.ndof);
	  
	  get_A__skyline_Block(Asky, Asky_mem, jbloc, numero_local[j], taille, &AA);

	  val = AA[jloc * MSph.ndof + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ia[l] = iglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[jglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}


void transporte_Value_and_rows_To_matlab(const char chaine[], MKL_INT nnz, MKL_INT *ia, cplx *a) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < nnz; i++){
    val = a[i];
    fprintf(fic,"%d\t %10.15e\t \%10.15e\n",ia[i],real(val),imag(val));
  }
  fclose(fic);
  free(file); file = NULL;
}



void transporte_columns_To_matlab(const char chaine[], MKL_INT n, MKL_INT *ja) {
  FILE*  fic;
  
  char *file;

  file = (char *) calloc(strlen(chaine) + 5, sizeof(char));
  strcpy(file, chaine);
  
  strcat(file,".txt");

  if( !(fic = fopen(file,"w")) ) {
    fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT FILE.\n");
    exit(EXIT_FAILURE);
  }
  cplx val;
  for(MKL_INT i = 0 ; i < n+1; i++){
    fprintf(fic,"%d\n",ja[i]);
  }
  fclose(fic);
  free(file); file = NULL;
}

/*--------------------------------------------------------------------------------------------------------------------*/
void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(MSph.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * MSph.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_local(A_glob.get_n_dof(), iglob, &ibloc, &iloc);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }
 
   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	  
	  jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	  
	  val = (A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + numero_local[j]].get_Aloc())[jloc * A_glob.get_n_dof() + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    } 
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}


void transforme_A_skyline_to_Sparse_CSR_new_version(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iloc = 0; ibloc < MSph.get_N_elements(); ibloc++){
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
    
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }
    
    for(iloc = 0; iloc < MSph.ndof; iloc++){
      
      iglob = local_to_glob(iloc, ibloc, MSph.ndof);
      
      for (j = 0; j < 5; j++){
	
	jbloc = tetra_voisin[j];
	
	if (jbloc >= 0){

	  cplx *bloc = A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + numero_local[j]].get_Aloc();
	  
	  for (jloc = 0; jloc < MSph.ndof; jloc++){
	    
	    jglob = local_to_glob(jloc, jbloc, MSph.ndof);
	    
	    val = bloc[jloc * MSph.ndof + iloc];
	    
	    if (abs(val) > 1E-10){
	      
	      a[l] = val;
	      ja[l] = jglob+1;
	      
	      l++;
	    }
	  }
	}
      }
      ia[iglob+1] = l+1;
    }
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
  
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}
  

/*##########################################################################################################*/
void transforme_A_skyline_to_Sparse_CSR(A_skylineRed &A_red, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, MailleSphere &MSph) {
   /*---------------------------------------------------- input ---------------------------------------
    A_red  : matrice en blocs contenant le systeme linéaire
    MSph   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;


  /*---------------------------------- remplissage des tableaux ia, ja et a ----------------------*/
 for (iglob = 0; iglob < n; iglob++){
    global_to_localRed(iglob, &ibloc, &iloc, MSph);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = MSph.elements[ibloc].voisin[0];
    tetra_voisin[2] = MSph.elements[ibloc].voisin[1];
    tetra_voisin[3] = MSph.elements[ibloc].voisin[2];
    tetra_voisin[4] = MSph.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == MSph.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < MSph.elements[jbloc].Nred; jloc++){
	  
	  jglob = local_to_globRed(jloc, jbloc, MSph);
	  
	  val = (A_red.Tab_A_locRed[ibloc * A_red.get_n_inter() + numero_local[j]].get_Aloc())[jloc * MSph.elements[ibloc].Nred + iloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}

/********************************************************************************************************************/

MKL_INT compute_nnz(A_skyline &A_glob, Maillage &Mesh){
//Cette fonction calcule le nombre de non-zeros en format Sparse COO (COOrdinate format), de la matrice skyline Ared 
  /*---------------------------------------------------------------------------------- input ---------------------------------------------------------------------------
    A_glob      : structure définissant la taille de la matrice A_glob
    Mesh        : le maillage
    /*-------------------------------------------------------------------------------- output --------------------------------------------------------------------------
                         nnz (le nombre de non-zeros de A_glob)
    /*------------------------------------------------------------------------------ intermediaire -----------------------------------------------------------------
    e           : pour boucler sur les éléments
    j           : .................... numeros locals des voisins de chaque élément
    iloc, jloc  : boucle sur les fonctions de bases
    block       : pour stocker l'adresse de chaque block de Ared
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, j, iloc, jloc, nnz;
  
  nnz = 0;
  
  for(e = 0; e < Mesh.get_N_elements(); e++) {

    cplx *bloc_diag;
    
    //Partie diagonale
    bloc_diag = A_glob.Tab_A_loc[e * A_glob.get_n_inter()].get_Aloc();
    
    for (iloc = 0; iloc < Mesh.ndof; iloc++) {
      
      for (jloc = 0; jloc < Mesh.ndof; jloc++) {
	
	if (abs(bloc_diag[jloc + iloc * Mesh.ndof]) > 1E-10) {

	  nnz++;
	  
	}
      }
    }

    //Partie extradiagonale
    for(j = 0; j < 4; j++) {
      
      if (Mesh.elements[e].voisin[j] >= 0) {

	cplx *bloc_extra;
	
	bloc_extra = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + j+1].get_Aloc();
	
	for (iloc = 0; iloc < Mesh.ndof; iloc++) {
	  
	  for (jloc = 0; jloc < Mesh.ndof; jloc++) {
	    
	    if (abs(bloc_extra[jloc + iloc * Mesh.ndof]) > 1E-10) {
	      
	      nnz++;
	      
	    }
	  }
	}
	
	
      }
    }
    
  }
  
  return nnz;
}


void transforme_A_skyline_to_Sparse_CSR(A_skyline &A_glob, MKL_INT nnz, MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, Maillage &Mesh) {
   /*---------------------------------------------------- input ---------------------------------------
    A_glob : matrice en blocs contenant le systeme linéaire
    Mesh   : le maillage 
    /*-------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc, iglob, jglob, i, j, l, ibloc, jbloc, s;
  cplx *AA;

   /*--------------------------------------------------- intermédaire ----------------------------------
    ibloc : numero de l'élément suivant la ligne
    jbloc : .............................. colonne
    AA    : pour clôner ou pour pointer sur un bloc
    /*----------------------------------------------------- ouput ---------------------------------------------
    a     : tableau pour stocker les coefficients de la matrice sparse
    ia    : .................... les indices de lignes des coefficients non nuls de la matrice sparse
    ja    : .................... le nombre de coefficients non nuls de chaque ligne
    /*-------------------------------------------------------------------------------------------------*/
  

  cout<<"****************************************************** transformation en format CSR **************************** "<<endl;

  MKL_INT *tetra_voisin =  new MKL_INT[5];//pour stocker les voisins de chaque tetra
  MKL_INT *numero_local =  new MKL_INT[5];//pour stocker le numero local de chaque tetra

  for (i = 0; i < 5; i++){
    tetra_voisin[i] = 0;
    numero_local[i] = 0;
  }

  cplx val;

  //initialisation des tableaux a, ia et ja
  for (i = 0; i < nnz; i++){
    a[i] = cplx(0.0,0.0);
    ja[i] = 0;
  }
  
  for (i = 0; i < n+1; i++)
    ia[i] = 0;
  
  l = 0;
  ia[0] = 1;

  /*---------------------------------- remplissage du tableau ia ---------------------------*/
  
  /*for (iglob = 0; iglob < n; iglob++){
    global_to_local(Mesh.ndof, iglob, &ibloc, &iloc);
    ia[iglob+1] = ia[iglob] + (NVS[ibloc] + 1) * Mesh.ndof;
    }*/

  /*---------------------------------- remplissage des tableaux a et ja ----------------------*/
  for (iglob = 0; iglob < n; iglob++){
    global_to_local(A_glob.get_n_dof(), iglob, &ibloc, &iloc);
    
    tetra_voisin[0] = ibloc;
    tetra_voisin[1] = Mesh.elements[ibloc].voisin[0];
    tetra_voisin[2] = Mesh.elements[ibloc].voisin[1];
    tetra_voisin[3] = Mesh.elements[ibloc].voisin[2];
    tetra_voisin[4] = Mesh.elements[ibloc].voisin[3];
    
    tri_recursive(tetra_voisin, 0, 5);
   
    
    for (j = 0; j < 5; j++){
      
      if (tetra_voisin[j] == ibloc){
	numero_local[j] = 0;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[0]){
	numero_local[j] = 1;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[1]){
	numero_local[j] = 2;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[2]){
	numero_local[j] = 3;
      }
      
      else if (tetra_voisin[j] == Mesh.elements[ibloc].voisin[3]){
	numero_local[j] = 4;
      }
    }

   
    for (j = 0; j < 5; j++){
      
      jbloc = tetra_voisin[j];
      
      if (jbloc >= 0){
	
	for (jloc = 0; jloc < A_glob.get_n_dof(); jloc++){
	  
	  jglob = local_to_glob(jloc, jbloc, A_glob.get_n_dof());
	  
	  val = (A_glob.Tab_A_loc[ibloc * A_glob.get_n_inter() + numero_local[j]].get_Aloc())[iloc * A_glob.get_n_dof() + jloc];
	  
	  if (abs(val) > 1E-10){
	    
	    a[l] = val;
	    ja[l] = jglob+1;
	    
	    l++;
	  }
	}
      }
    }
    ia[iglob+1] = l+1;
    
  }
  cout<<" **************************************** fin de la transformation ******************************** "<<endl;
  cout<<" ia[n] = "<<ia[n]<<endl;
 
  delete [] tetra_voisin; tetra_voisin = 0;
  delete [] numero_local; numero_local = 0;
}
/************************************************ NORMES **************************************************************/

double norme_L2(MKL_INT e, cplx *X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I = cplx(0.0,0.0);
  cplx val ;
  cplx *x;
  double s;
  
  get_V__skyline_Block(X, e, MSph.ndof, &x);//restriction de X sur l'element e
    
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(MSph.elements[e].face[j], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
      }
      
      I += x[jloc] * conj(x[iloc]) * val;
    }
  }
  
  s = abs(I);
  
  return s;
}


double norme_L2(cplx *X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
 
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2(e, X, MSph, chaine);
  }

 return s;
}


void norme_L2(double *r, cplx *X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  s = 0.0;
  
#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2(e, X, MSph, chaine);
  }

 #pragma omp atomic
  *r += s;
}



double norme_L2_Vitesse(MKL_INT e, cplx *X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I, val ;
  cplx *x;
  double r, s;
  
  get_V__skyline_Block(X, e, MSph.ndof, &x);//restriction de X sur l'element e

  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(MSph.elements[e].face[j], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
      }
      
      r = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].OP[iloc].Ad, 1) ;

      r = r * MSph.elements[e].OP[jloc].ksurOmega * MSph.elements[e].OP[iloc].ksurOmega;
      
      I += x[jloc] * conj(x[iloc]) * r * val;
    }
  }
  
  s = abs(I);

  return s;
}


double norme_L2_Vitesse(cplx *X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_Vitesse(e, X, MSph, chaine);
  }

  //s = sqrt(s);

  return s;

}


void norme_L2_Vitesse(double *r, cplx *X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;

#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_Vitesse(e, X, MSph, chaine);
  }

  #pragma omp atomic
  *r += s;

}


double norme_L2(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I = cplx(0.0,0.0);
  cplx val ;
  cplx *x;
  double s;
  
  X.get_F_elem(MSph.ndof, e, &x);
  
    
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(MSph.elements[e].face[j], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
      }
      
      I += x[jloc] * conj(x[iloc]) * val;
    }
  }
 
  s = abs(I);

  return s;
}


double norme_L2(VecN &X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
 
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2(e, X, MSph, chaine);
  }

 return s;
}


void norme_L2(double *r, VecN &X, MailleSphere &MSph, const char chaine[]) {
  //initialiser *r = 0.0 en dehors de la fonction hors region parallele
  double s;
  MKL_INT e;
  s = 0.0;
  
#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2(e, X, MSph, chaine);
  }

 #pragma omp atomic
  *r += s;
}


double norme_L2_Vitesse(MKL_INT e, VecN &X, MailleSphere &MSph, const char chaine[]){
  MKL_INT iloc, jloc, j, l;
  cplx I, val ;
  cplx *x;
  double r, s;
  
  X.get_F_elem(MSph.ndof, e, &x);

  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){
      
      val = cplx(0.0,0.0);
      
      for (j = 0; j < 4; j++) {
	val += integrale_triangle(MSph.elements[e].face[j], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
      }
      
      r = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].OP[iloc].Ad, 1) ;

      r = r * MSph.elements[e].OP[jloc].ksurOmega * MSph.elements[e].OP[iloc].ksurOmega;
      
      I += x[jloc] * conj(x[iloc]) * r * val;
    }
  }
  
  s = abs(I);

  return s;
}


double norme_L2_Vitesse(VecN &X, MailleSphere &MSph, const char chaine[]) {
  double s;
  MKL_INT e;
  
  s = 0.0;
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_Vitesse(e, X, MSph, chaine);
  }

  //s = sqrt(s);

  return s;

}


void norme_L2_Vitesse(double *r, VecN &X, MailleSphere &MSph, const char chaine[]) {
  //initialiser *r = 0.0 en dehors de la fonction hors region parallele
  double s;
  MKL_INT e;
  
  s = 0.0;

#pragma omp for schedule(runtime)
  for(e = 0; e < MSph.get_N_elements(); e++){
    s += norme_L2_Vitesse(e, X, MSph, chaine);
  }

  #pragma omp atomic
  *r += s;
  //s = sqrt(s);

}


double norme_L2_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx P;
   double I = 0.0;
   
   for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
     for (j = 0; j < 3; j++) {
       Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
     }
     
     //Evaluation de alpha * j0 + beta * y_0 en Xquad
     SolexacteBessel(e, Xquad, &P, MSph);
     
     //Evaluation de la solution numérique en Xquad
     combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
     
     I = I + MSph.Qd.Wtetra[i] * abs((P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[e].Pexacte));
     
   }
   
   I = I * MSph.elements[e].Jac;
   
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  return I;
}



double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx *V = new cplx[3];

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx J, P;
   double I = 0.0;
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de alpha * j0 + beta * y_0 en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
    
    J = (V[0] - MSph.elements[e].Vexacte[0]) * conj(V[0] - MSph.elements[e].Vexacte[0])
      + (V[1] - MSph.elements[e].Vexacte[1]) * conj(V[1] - MSph.elements[e].Vexacte[1])
      + (V[2] - MSph.elements[e].Vexacte[2]) * conj(V[2] - MSph.elements[e].Vexacte[2]);
    
    I = I + MSph.Qd.Wtetra[i] * abs(J);
  }

  I = I * MSph.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  
  return I;
}


double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(MKL_INT e, MailleSphere &MSph) {

  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de Be en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    
    I = I + MSph.Qd.Wtetra[i] * abs(P * conj(P));
  }
  
  I = I * MSph.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  
  return I;
}


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MKL_INT e, MailleSphere &MSph) {

  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  
  
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de Be en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    
    I = I + MSph.Qd.Wtetra[i] * abs(V[0] * conj(V[0]) + V[1] * conj(V[1]) + V[2] * conj(V[2]));
  }
  
  I = I * MSph.elements[e].Jac;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  delete [] V; V  =  nullptr;
  
  return I;
}




double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    I +=  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph);

  }

  I = sqrt(I);
  
  return I;
}


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    I +=  norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);

  }

  I = sqrt(I);
  
  return I;
}
/*---------------------------------------------------------------------------------------------------------------*/
double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph);

  }

  I = sqrt(I);
  
  return I;
}



double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);

  }

  I = sqrt(I);
  
  return I;
}
/*---------------------------------------------------------------------------------------------------------------*/
double norme_inf_erreur(MKL_INT e, cplx *u_loc, MailleSphere &MSph){

  double s;
  
  cplx P;
  
  SolexacteBessel(e, MSph.elements[e].XG, &P, MSph);
  
  combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, MSph.elements[e].XG);

  s = abs(P - MSph.elements[e].Pexacte);

  return s;
}


double norme_inf_erreur(cplx *U, MailleSphere &MSph){
  
  double s, r;

  r = 0.0; 
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {
    cplx *u_loc;

    get_F_elem(MSph.ndof, e, &u_loc, U);

    s = norme_inf_erreur(e, u_loc, MSph);

    r = max(r, s);
  }

  return r;
}


double norme_inf_Bessel(MailleSphere &MSph){
  
  double s = 0.0;
  
  cplx P;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {
    
    SolexacteBessel(e, MSph.elements[e].XG, &P, MSph);
    
    s = max(s, abs(P));
  }

  return s;
}
/*-------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------- ERREUR EN PRESSION ------------------------------------------------------------------------*/
double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  
  x1 = MSph.elements[e].face[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].face[l].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].face[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
    
    I = I + MSph.Qd2D.Wtriangle[i] * abs((P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[e].Pexacte));
    
  }
  
  I = I * MSph.elements[e].face[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x2x3; x2x3 = 0;
  
  return I;
}


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph) {
  
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    I += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_une_face(e, l, u_loc, MSph);
  }
  
  return I;
  
}


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {
    cplx *u_loc;
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    
  }

  I = sqrt(I);
  return I;
}
/*--------------------------------------------------------------------------------- ERREUR EN VITESSE ------------------------------------------------------------------------*/

double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  cplx *W = new cplx[3];
  
  x1 = MSph.elements[e].face[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].face[l].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].face[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  
  double I = 0.0;
  cplx Vn_l;
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);

    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);

    for (j = 0; j < 3; j++) {
      W[j] = V[j] - MSph.elements[e].Vexacte[j];
    }
  
    if(l == 0){     
      Vn_l = dot_u(W, MSph.elements[e].n1);
    }
    else if(l == 1){
      Vn_l = dot_u(W, MSph.elements[e].n2);
    }
    else if(l == 2){
      Vn_l = dot_u(W, MSph.elements[e].n3);
    }
    else if(l == 3){
      Vn_l = dot_u(W, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * abs(Vn_l * conj(Vn_l));
    
  }
  
  I = I * MSph.elements[e].face[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x1x3; x1x3  =  nullptr;
  delete [] x2x3; x2x3  =  nullptr;
  
  delete [] V; V  =  nullptr;
  delete [] W; W  =  nullptr;
  
  return I;
}


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph) {
  
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    I += norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_une_face(e, l, u_loc, MSph);
  }
  
  return I;
  
}


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {
    
    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    
  }
  I = sqrt(I);
  return I;
}


double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph) {
  
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] < 0){
      I += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_une_face(e, l, u_loc, MSph);
    }
  }
  return I;
  
}


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph) {
  
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] < 0){
      I += norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_une_face(e, l, u_loc, MSph);
    }
  }
  
  return I;
  
}


/*------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------- POUR PRESSION ---------------------------------------------------------------------------*/
double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, MailleSphere &MSph) {

  
  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  
  x1 = MSph.elements[e].face[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].face[l].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].face[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }

  
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de Be en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    
    I = I + MSph.Qd2D.Wtriangle[i] * abs(P * conj(P));
  }
  
  I = I * MSph.elements[e].face[l].Aire;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x1x3; x1x3  =  nullptr;
  delete [] x2x3; x2x3  =  nullptr;
  
  return I;
}


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_un_element(MKL_INT e, MailleSphere &MSph) {
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    I += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_une_face(e, l, MSph);
  }

  return I;
}


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique(MailleSphere &MSph) {
  
  MKL_INT e;
  
  double I = 0.0;

  for (e = 0; e < MSph.get_N_elements(); e++) {
    
    I += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_un_element(e, MSph);

  }

  I = sqrt(I);

  return I;
}


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, MailleSphere &MSph) {
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    if(MSph.elements[e].voisin[l] < 0){
      I += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_une_face(e, l, MSph);
    }
  }
  return I;
}

/*---------------------------------------------------------------------------- POUR VITESSE ---------------------------------------------------------------------------*/

double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_une_face(MKL_INT e, MKL_INT l, MailleSphere &MSph) {

  
  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  
  
  x1 = MSph.elements[e].face[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].face[l].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].face[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }

  cplx Vn_l;
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de Be en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    if(l == 0){     
      Vn_l = dot_u(V, MSph.elements[e].n1);
    }
    else if(l == 1){
      Vn_l = dot_u(V, MSph.elements[e].n2);
    }
    else if(l == 2){
      Vn_l = dot_u(V, MSph.elements[e].n3);
    }
    else if(l == 3){
      Vn_l = dot_u(V, MSph.elements[e].n4);
    }   
       
    I = I + MSph.Qd2D.Wtriangle[i] * abs(Vn_l * conj(Vn_l));
  }
  
  I = I * MSph.elements[e].face[l].Aire;

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x1x3; x1x3  =  nullptr;
  delete [] x2x3; x2x3  =  nullptr;
 
  delete [] V; V  =  nullptr;
  
  return I;
}


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_un_element(MKL_INT e, MailleSphere &MSph) {
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    I += norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_une_face(e, l, MSph);
  }

  return I;
}

double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique(MailleSphere &MSph) {
  
  MKL_INT e;
  
  double I = 0.0;

  for (e = 0; e < MSph.get_N_elements(); e++) {
    
    I += norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_un_element(e, MSph);

  }

  I = sqrt(I);

  return I;
}


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(MKL_INT e, MailleSphere &MSph) {
  MKL_INT l;
  
  double I = 0.0;
  
  for( l = 0; l < 4; l++) {
    if (MSph.elements[e].voisin[l] < 0){
      I += norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_une_face(e, l, MSph);
    }
  }
  return I;
}


void debugg_integrale_numerique(MailleSphere &MSph){

  VecN U;
  U.n = MSph.get_N_elements() * MSph.ndof;
  U.Allocate();
   double maxx_Pe, minn_Pe, maxx_Ve, minn_Ve;
   double maxx_P, minn_P, maxx_V, minn_V;

   maxx_P = 0.0; minn_P = 1000.0; maxx_V = 0.0; minn_V = 1000.0;

   MKL_INT iloc, jloc;
   // for(MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

     cplx *u;

     auto t1 = std::chrono::high_resolution_clock::now();
     double IP = norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique(MSph);
     auto t2 = std::chrono::high_resolution_clock::now();
     std::chrono::duration<double, std::milli> float_S1 = t2 - t1;

     auto t3 = std::chrono::high_resolution_clock::now();
     double JP = norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
     auto t4 = std::chrono::high_resolution_clock::now();
     std::chrono::duration<double, std::milli> float_S2 = t4 - t3;

     auto t5 = std::chrono::high_resolution_clock::now();
     double IV = norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique(MSph);
     auto t6 = std::chrono::high_resolution_clock::now();
     std::chrono::duration<double, std::milli> float_S3 = t6 - t5;

     auto t7 = std::chrono::high_resolution_clock::now();
     double JV = norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
     auto t8 = std::chrono::high_resolution_clock::now();
     std::chrono::duration<double, std::milli> float_S4 = t8 - t7;
     
     maxx_P = max(maxx_P, abs(IP - JP));
     minn_P = min(minn_P, abs(IP - JP));
     
     maxx_V = max(maxx_V, abs(IV - JV));
     minn_V = min(minn_V, abs(IV - JV));
     //cout<<" e = "<<e<<" maxx_P = "<<maxx_P<<" minn_P = "<<minn_P<<" maxx_V = "<<maxx_V<<" minn_V = "<<minn_V<<endl;
      //}
   
   cout<<" maxx_P = "<<maxx_P<<" minn_P = "<<minn_P<<" maxx_V = "<<maxx_V<<" minn_V = "<<minn_V<<endl;
   cout<<" temps calcul norme pression en sequentiel : "<<float_S1.count()<<endl;
   cout<<" temps calcul norme velocite en sequentiel : "<<float_S3.count()<<endl;
   cout<<" temps calcul norme pression en parallel : "<<float_S2.count()<<endl;
   cout<<" temps calcul norme velocite en parallel : "<<float_S4.count()<<endl;
   
}
/*------------------------------------------------------------------------------------------------*/


double norme_L2_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx P;
   double I = 0.0;
   
   for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
     for (j = 0; j < 3; j++) {
       Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
     }
     
     
     //Evaluation de la solution numérique en Xquad
     combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
     
     I = I + MSph.Qd.Wtetra[i] * abs(MSph.elements[e].Pexacte * conj(MSph.elements[e].Pexacte));
     
   }
   
   I = I * MSph.elements[e].Jac;
   
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  return I;
}


double  norme_L2_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph);

    }
#pragma omp atomic
    I += s; 
  }
  I = sqrt(I);
  
  return I;
}

double norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  I = sqrt(I);
  
  return I;
}


double norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph);

    }
#pragma omp atomic
    I += s; 
  }
  I = sqrt(I);
  
  return I;
}



double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  I = sqrt(I);
  
  return I;
}
/*------------------------------------------------------------------------------------------------*/
double norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      cplx *u_loc;
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    }
  #pragma omp atomic
    I += s;
  }

  I = sqrt(I);
  return I;
}


double norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    }
    #pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  return I;
}


double norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_un_element(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}


double norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s += norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_un_element(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  I = sqrt(I);
  
  return I;
}


double norme_L2_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, cplx *v_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx P;
   double I = 0.0;
   
   for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
     for (j = 0; j < 3; j++) {
       Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
     }
     
     
     //Evaluation de la solution numérique en Xquad
     combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);

     //Evaluation de la solution numérique en Xquad
     combinaison_solution_projection(MSph.elements[e], MSph.ndof, v_loc, Xquad);
     
     I = I + MSph.Qd.Wtetra[i] * abs((MSph.elements[e].Pro_P - MSph.elements[e].Pexacte) * conj(MSph.elements[e].Pro_P - MSph.elements[e].Pexacte));
     
   }
   
   I = I * MSph.elements[e].Jac;
   
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  return I;
}


double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(cplx *U, cplx *V, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc, *v_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);

      get_F_elem(MSph.ndof, e, &v_loc, V);
      
      s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, v_loc, MSph);

    }
#pragma omp atomic
    I += s; 
  }
  I = sqrt(I);
  
  return I;
}



double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, cplx *v_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx *V = new cplx[3];

  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx J, P;
   double I = 0.0;
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution_projection(MSph.elements[e], MSph.ndof, v_loc, Xquad);
    
    J = (MSph.elements[e].Pro_V[0] - MSph.elements[e].Vexacte[0]) * conj(MSph.elements[e].Pro_V[0] - MSph.elements[e].Vexacte[0])
      + (MSph.elements[e].Pro_V[1] - MSph.elements[e].Vexacte[1]) * conj(MSph.elements[e].Pro_V[1] - MSph.elements[e].Vexacte[1])
      + (MSph.elements[e].Pro_V[2] - MSph.elements[e].Vexacte[2]) * conj(MSph.elements[e].Pro_V[2] - MSph.elements[e].Vexacte[2]);
    
    I = I + MSph.Qd.Wtetra[i] * abs(J);
  }

  I = I * MSph.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  
  return I;
}


double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(cplx *U, cplx *V, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc, *v_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);

      get_F_elem(MSph.ndof, e, &v_loc, V);
      
      s += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, v_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  I = sqrt(I);
  
  return I;
}




double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  
  x1 = MSph.face_double[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.face_double[l].noeud[1]->get_X();//................ 2
  x3 = MSph.face_double[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  double J;
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    //Evaluation de la solution numérique en Xquad dans l'element e
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc_e, Xquad);

    //Evaluation de la solution numérique en Xquad dans l'element f
    combinaison_solution(MSph.elements[f], MSph.ndof, u_loc_f, Xquad);

    J = abs((P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[e].Pexacte)) + abs((P - MSph.elements[f].Pexacte) * conj(P - MSph.elements[f].Pexacte)) - 2 * real((P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[f].Pexacte));

    I = I + MSph.Qd2D.Wtriangle[i] * J;
    
  }
  
  I = I * MSph.face_double[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x2x3; x2x3 = 0;
  
  return I;
}



double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  cplx *We = new cplx[3];
  cplx *Wf = new cplx[3];
  
  x1 = MSph.face_double[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.face_double[l].noeud[1]->get_X();//................ 2
  x3 = MSph.face_double[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  
  double I = 0.0;
  cplx Vn_e, Vn_f;
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);

    //Evaluation de la solution numérique en Xquad dans l'élément e
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc_e, Xquad);

    //Evaluation de la solution numérique en Xquad dans l'élément f
    combinaison_solution(MSph.elements[f], MSph.ndof, u_loc_f, Xquad);

    for (j = 0; j < 3; j++) {
      We[j] = V[j] - MSph.elements[e].Vexacte[j];
      Wf[j] = V[j] - MSph.elements[f].Vexacte[j];
    }

  
    if(l == 0){     
      Vn_e = dot_u(We, MSph.elements[e].n1);
      Vn_f = dot_u(Wf, MSph.elements[f].n1);
    }
    else if(l == 1){
      Vn_e = dot_u(We, MSph.elements[e].n2);
      Vn_f = dot_u(Wf, MSph.elements[f].n2);
    }
    else if(l == 2){
      Vn_e = dot_u(We, MSph.elements[e].n3);
      Vn_f = dot_u(Wf, MSph.elements[f].n3);
    }
    else if(l == 3){
      Vn_e = dot_u(We, MSph.elements[e].n4);
      Vn_f = dot_u(Wf, MSph.elements[f].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * (abs(Vn_e * conj(Vn_e)) + abs(Vn_f * conj(Vn_f)) + 2 * real(Vn_e * conj(Vn_f)));
    
  }
  
  I = I * MSph.face_double[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x1x3; x1x3  =  nullptr;
  delete [] x2x3; x2x3  =  nullptr;
  
  delete [] V; V  =  nullptr;
  delete [] Wf; Wf  =  nullptr;
  delete [] We; We  =  nullptr;
  
  return I;
}


double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  
  x1 = MSph.face_double[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.face_double[l].noeud[1]->get_X();//................ 2
  x3 = MSph.face_double[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  
  double I = 0.0;
  
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, MSph);
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
    
    I = I + MSph.Qd2D.Wtriangle[i] * abs((P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[e].Pexacte));
    
  }
  
  I = I * MSph.face_double[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x1x3; x1x3 = 0;
  delete [] x2x3; x2x3 = 0;
  
  return I;
}



double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph) {

  MKL_INT i, j;
  double *x1, *x2, *x3;
  double *Xquad = new double[3];
  double *x2x3 = new double[3];
  double *x1x3 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  cplx *W = new cplx[3];
  
  x1 = MSph.face_double[l].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.face_double[l].noeud[1]->get_X();//................ 2
  x3 = MSph.face_double[l].noeud[2]->get_X();//................ 3
  
  for (j = 0; j < 3; j++) {
    x2x3[j] = x2[j] - x3[j];//x2 - x3
    x1x3[j] = x1[j] - x3[j];//X1 - x3
  }
  
  
  double I = 0.0;
  cplx Vn_l;
  for (i = 0; i < MSph.Qd2D.ordre * MSph.Qd2D.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x3[j] + MSph.Qd2D.Xtriangle[2*i] * x1x3[j] + MSph.Qd2D.Xtriangle[2*i+1] * x2x3[j];
    }
    
    //Evaluation de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);

    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);

    for (j = 0; j < 3; j++) {
      W[j] = V[j] - MSph.elements[e].Vexacte[j];
    }
  
    if(l == 0){     
      Vn_l = dot_u(W, MSph.elements[e].n1);
    }
    else if(l == 1){
      Vn_l = dot_u(W, MSph.elements[e].n2);
    }
    else if(l == 2){
      Vn_l = dot_u(W, MSph.elements[e].n3);
    }
    else if(l == 3){
      Vn_l = dot_u(W, MSph.elements[e].n4);
    }   
   
    
    I = I + MSph.Qd2D.Wtriangle[i] * abs(Vn_l * conj(Vn_l));
    
  }
  
  I = I * MSph.face_double[l].Aire;
  
  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x1x3; x1x3  =  nullptr;
  delete [] x2x3; x2x3  =  nullptr;
  
  delete [] V; V  =  nullptr;
  delete [] W; W  =  nullptr;
  
  return I;
}



double norme_L2_erreur_du_saut_en_pression_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  MKL_INT e, f, ee;
  
  for (MKL_INT l = 0; l < MSph.N_Face_double; l++) {
    
    cplx *u_loc_e, *u_loc_f, *u_loc;

    e = MSph.face_double[l].voisin1;
    f = MSph.face_double[l].voisin2;
    
    if ((e >= 0) and (f >= 0)){
      
      get_F_elem(MSph.ndof, e, &u_loc_e, U);
      get_F_elem(MSph.ndof, f, &u_loc_f, U);
      
      I += norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph);
    }

    else{
      if ((e >= 0) and (f < 0)){
	ee = e;
      }
      else if ((e < 0) and (f >= 0)){
	ee = f;
      }

      get_F_elem(MSph.ndof, ee, &u_loc, U);

      I += norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_exterieure(ee, l, u_loc, MSph);
    }
    
  }

  I = sqrt(I);
  return I;
}



double norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  MKL_INT e, f, ee;
  
  for (MKL_INT l = 0; l < MSph.N_Face_double; l++) {
    
    cplx *u_loc_e, *u_loc_f, *u_loc;

    e = MSph.face_double[l].voisin1;
    f = MSph.face_double[l].voisin2;
    
    if ((e >= 0) and (f >= 0)){
      
      get_F_elem(MSph.ndof, e, &u_loc_e, U);
      get_F_elem(MSph.ndof, f, &u_loc_f, U);
      
      I += norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph);
    }

    else{
      if ((e >= 0) and (f < 0)){
	ee = e;
      }
      else if ((e < 0) and (f >= 0)){
	ee = f;
      }

      get_F_elem(MSph.ndof, ee, &u_loc, U);

      I += norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_exterieure(ee, l, u_loc, MSph);
    }
    
  }

  I = sqrt(I);
  return I;
}



double norme_H1_du_saut_de_l_erreur_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  MKL_INT e, f, ee;
  
  for (MKL_INT l = 0; l < MSph.N_Face_double; l++) {
    
    cplx *u_loc_e, *u_loc_f, *u_loc;

    e = MSph.face_double[l].voisin1;
    f = MSph.face_double[l].voisin2;
    
    if ((e >= 0) and (f >= 0)){
      
      get_F_elem(MSph.ndof, e, &u_loc_e, U);
      get_F_elem(MSph.ndof, f, &u_loc_f, U);
      
      I += norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph) +  norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph);
    }

    else{
      if ((e >= 0) and (f < 0)){
	ee = e;
      }
      else if ((e < 0) and (f >= 0)){
	ee = f;
      }

      get_F_elem(MSph.ndof, ee, &u_loc, U);

      I += norme_L2_erreur_du_saut_en_pression_avec_integ_numerique_sur_une_face_exterieure(ee, l, u_loc, MSph) + norme_L2_erreur_du_saut_en_velocite_avec_integ_numerique_sur_une_face_exterieure(ee, l, u_loc, MSph);
    }
    
  }
  
  I = sqrt(I);

  return I;
}


double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(cplx *U, MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph) + norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }
  
  
  return I;
}
  
  
 double norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures(cplx *U, MailleSphere &MSph) {
  
  double I = 0.0;
 
  MKL_INT e;

  for (e = 0; e < MSph.get_N_elements(); e++) {
    
    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph) + norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, u_loc, MSph);
  }
    return I;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ASSEMBLAGE_DE_A1_plus_A4_plus_A5_plus_A8_parallel_version(A_skyline &A_glob, MailleSphere &MSph, const char chaine[]){
  //Cette fonction construit la matrice A_skyline des termes int_{F}[P][P']+[V][V']
  //F étant une face 
  /*---------------------------------------------------------------------------------- input ---------------------------------------------------
    A_glob    : tableau skyline ou stocké la matrice
    MSph      : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------
                                              A_glob la matrice du systeme A U = F
    /*------------------------------------------------------------------------------ intermédiaire --------------------------------------------
    e         : indice global d'un élément
    f         : indice local d'une face
    B1        : pour stocker un bloc d'interaction element-element sur une face l;
                 int_{l} P_T P'_T

    B2        : .................................. voisin-element ............. l;
                 int_{l} P_K P'_T

    AA1       : pour stocker l'adresse de A_glob dans l'emplacement correspondant à l'interaction 
                element-element pour un element e quelconque

    AA2       : pour stocker A_glob dans l'emplacement correspondant à l'interaction
                 voisin-element pour un voisin quelconque de l'element e quelconque
    /*-----------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e;

  cplx *B1, *B2;
#pragma omp parallel private(B1, B2, e)
  {
    B1 = new cplx[MSph.ndof * MSph.ndof];
    B2 = new cplx[MSph.ndof * MSph.ndof];
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *AA1 = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + 0].get_Aloc();
      
      for (MKL_INT f = 0; f < 4; f++) {
	
	
	if (MSph.elements[e].voisin[f] >= 0) {//Sur les faces intérieures
	  
	  //stockage de int_{l} P_T P'_T  dans B1 et int_{l} P_K P'_T dans B2
	  Build_interior_planeBloc(e, f, B1, B2, MSph, chaine);
	  
	  //--------------------------------------  interaction element-element ------------------------------------------------
	  //ajout du bloc P_T P'_T associé à la face f
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B1, AA1, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face
	  Ajouter_BlocIntV_TxV_T(e, f, 1.0, B1, AA1, MSph);
	  	  
	  //--------------------------------------  interaction voisin-element ------------------------------------------------
	  cplx *AA2 = A_glob.Tab_A_loc[e * A_glob.get_n_inter() + (f+1)].get_Aloc();
	  
	  //ajout du bloc P_K P'_T associé à la face
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B2, AA2, MSph);
	  
	  //ajout du bloc V_Kn_K V'_Tn_T associé à la face f
	  Ajouter_BlocIntV_KxV_T(e, f, 1.0, B2, AA2, MSph);
	  
	}//Fin du if
      }//Fin boucle sur les faces
    }//Fin boucle sur les éléments
    delete [] B1; B1 = 0;
    delete [] B2; B2 = 0;
  }//Fin de la région parallele
  
  
}


double norme_H1_du_saut_d_un_vecteur(MKL_INT n, MKL_INT *ia, MKL_INT *ja, cplx *a, cplx *U){
  cplx *V = nullptr;
  V = new cplx[n];

  initialize_tab(n, V);

  produit_MatSparse_vecteur_parallel_version(n, ia, ja, a, U, V);
  
  double r = abs(compute_XdotY(n, U, V));

  delete [] V; V = nullptr;

  return r;
}



double norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]){
  //Cette fonction construit la matrice A_skyline des termes int_{F}[P][P']+[V][V']
  //F étant une face 
  /*---------------------------------------------------------------------------------- input ---------------------------------------------------
    A_glob    : tableau skyline ou stocké la matrice
    MSph      : le maillage
    /*-------------------------------------------------------------------------------- output -------------------------------------------------
                                              A_glob la matrice du systeme A U = F
    /*------------------------------------------------------------------------------ intermédiaire --------------------------------------------
    e         : indice global d'un élément
    f         : indice local d'une face
    B1        : pour stocker un bloc d'interaction element-element sur une face l;
                 int_{l} P_T P'_T

    B2        : .................................. voisin-element ............. l;
                 int_{l} P_K P'_T

    AA1       : pour stocker l'adresse de A_glob dans l'emplacement correspondant à l'interaction 
                element-element pour un element e quelconque

    AA2       : pour stocker A_glob dans l'emplacement correspondant à l'interaction
                 voisin-element pour un voisin quelconque de l'element e quelconque
    /*-----------------------------------------------------------------------------------------------------------------------------------------*/
  MKL_INT e, taille = MSph.ndof * MSph.ndof;

  double S1 = 0.0;
  double S2 = 0.0;
  double s1, s2;
  cplx *B1 = nullptr, *B2 = nullptr, *bloc_diag = nullptr, *bloc_extra = nullptr;
#pragma omp parallel private(B1, B2, e, bloc_diag, bloc_extra, s1, s2)
  {
    B1 = new cplx[taille];
    B2 = new cplx[taille];
    bloc_diag = new cplx[taille];
    bloc_extra = new cplx[4 * taille];
   
    s1 = 0.0; s2 = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      cplx I = cplx(0.0, 0.0);
      MKL_INT iloc, jloc;
      initialize_tab(taille, bloc_diag);
      
      initialize_tab(4 * taille, bloc_extra);
      
      for (MKL_INT f = 0; f < 4; f++) {
	
	
	if (MSph.elements[e].voisin[f] >= 0) {//Sur les faces intérieures
	  
	  //stockage de int_{l} P_T P'_T  dans B1 et int_{l} P_K P'_T dans B2
	  Build_interior_planeBloc(e, f, B1, B2, MSph, chaine);
	  
	  //--------------------------------------  interaction element-element ------------------------------------------------
	  //ajout du bloc P_T P'_T associé à la face f
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face
	  Ajouter_BlocIntV_TxV_T(e, f, 1.0, B1, bloc_diag, MSph);
	  	  
	  //--------------------------------------  interaction voisin-element ------------------------------------------------
	  cplx *D = bloc_extra + f * taille;
	  
	  //ajout du bloc P_K P'_T associé à la face
	  Ajouter_BlocP_XP_Y(e, f, 1.0, B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K V'_Tn_T associé à la face f
	  Ajouter_BlocIntV_KxV_T(e, f, 1.0, B2, D, MSph);

	}//Fin du if
      }//Fin boucle sur les faces

      cplx *x;
      get_F_elem(MSph.ndof, e, &x, U);
      
      for(iloc = 0; iloc < MSph.ndof; iloc++){
	for(jloc = 0; jloc < MSph.ndof; jloc++){
	  I +=  conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
      
      for(MKL_INT f = 0; f < 4; f++){
	if (MSph.elements[e].voisin[f] >=0){
	  
	  cplx *D = bloc_extra + f * taille;
	  
	  cplx *y;
	  
	  get_F_elem(MSph.ndof, MSph.elements[e].voisin[f], &y, U);
	  
	  
	  for(iloc = 0; iloc < MSph.ndof; iloc++){
	    for(jloc = 0; jloc < MSph.ndof; jloc++){
	      I +=  conj(x[iloc]) * D[jloc * MSph.ndof + iloc] * y[jloc];
	    }
	  }

	}
      }

      s1 += real(I);
      s2 += imag(I);
      
    }//Fin boucle sur les éléments
    delete [] B1; B1 = nullptr;
    delete [] B2; B2 = nullptr;
    delete [] bloc_diag; bloc_diag = nullptr;
    delete [] bloc_extra; bloc_extra = nullptr;

#pragma omp atomic
    S1 += s1;

#pragma omp atomic
    S2 += s2;
  }//Fin de la région parallele
  
  cplx J = cplx(S1, S2);
  
  double r = abs(J);
  return r;
}



double norme_H1_de_l_erreur_avec_integ_mixed(MKL_INT n, cplx *U, MailleSphere &MSph, const char chaine[]){
  double I = 0.0;

  MKL_INT n_R = n * MSph.ndof * 5;
  cplx *R_full = nullptr;
  R_full = new cplx[n_R];
  A_Block *array_R_loc = nullptr;
  array_R_loc = new A_Block[MSph.get_N_elements() * 5];
  A_skyline R_glob;
  R_glob.set_n_elem(MSph.get_N_elements()); // fixe le nombre d'elements 
  R_glob.set_n_inter(5); // fixe le nombre d'interactions 
  R_glob.set_n_dof(MSph.ndof);  // fixe le nombre d'ondes planes a ndof
  R_glob.set_n_A(); // calcule le nombre de coeff a stocker
  R_glob.build_mat(R_full, array_R_loc); // Construit les blocks et la structure en mémoire continue

  ASSEMBLAGE_DE_A1_plus_A4_plus_A5_plus_A8_parallel_version(R_glob, MSph, chaine);

  MKL_INT nnz = compute_nnz_for_Sparse_COO_format(R_glob, MSph);
  
  MKL_INT *ia = nullptr, *ja = nullptr;
  cplx *a = nullptr;
  ia = new MKL_INT[n+1]; ja = new MKL_INT[nnz]; a = new cplx[nnz];
  
  cout<<" transformation en sparse CSR... "<<endl;
  transforme_A_skyline_to_Sparse_CSR(R_glob, nnz, n, ia, ja, a, MSph);
  
  I =  norme_H1_du_saut_d_un_vecteur(n, ia, ja, a, U) + norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(U, MSph);


  delete [] R_full; R_full = nullptr;
  delete [] array_R_loc; array_R_loc = nullptr;
  delete [] ia; ia = nullptr;
  delete [] ja; ja = nullptr;
  delete [] a; a = nullptr;
  
  I = sqrt(I);
  return I;
}




cplx norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph, const char chaine[]){

   /*---------------------------------------------------- input ------------------------------------------------------------
    e, f    : numeros globaux de voisins de la face l
    l       : face d'un élément du maillage
    u_loc_e : coordonnées d'un vecteur U sur la base de Trefftz de e
    u_loc_f : coordonnées du meme vecteur U sur la base de Trefftz de f
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc;

  cplx s = cplx(0.0, 0.0);

  cplx Ie, If, Ief, vale, valf; 
  Ie = cplx(0.0,0.0); If = cplx(0.0, 0.0);;
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){     
      
      vale =  integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      Ie += u_loc_e[jloc] * conj(u_loc_e[iloc]) * vale;

      valf = integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[f].OP[jloc], chaine);      
      
      If += u_loc_f[jloc] * conj(u_loc_e[iloc]) * valf;
      
    }
  }
  
  s = Ie - If;
  
  return s;
}



cplx norme_L2_du_saut_en_pression_avec_integ_analytique_sur_les_faces_interieures_d_un_element(MKL_INT e, cplx *U, MailleSphere &MSph, const char chaine[]) {
  
  MKL_INT l, f;
  
  cplx I = cplx(0.0, 0.0);

  double s = 0.0;

  for( l = 0; l < 4; l++) {

    if (MSph.elements[e].voisin[l] >= 0){

      f = MSph.elements[e].voisin[l];

      cplx *u_loc_e, *u_loc_f;
      
      get_F_elem(MSph.ndof, e, &u_loc_e, U);
      get_F_elem(MSph.ndof, f, &u_loc_f, U);
      
      I += norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph, chaine);
    }
    
  }
  
  return I;
  
}




double norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]){

   /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numeros globaux de voisins de la face l
    l       : face d'un élément du maillage
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT iloc, jloc;

  double s = 0.0;

  cplx I, val; 
  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){     
      
      val =  integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      I += u_loc[jloc] * conj(u_loc[iloc]) * val;
      
    }
  }
  
  s = abs(I);
  
  return s;
}



cplx norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_interieure(MKL_INT e, MKL_INT f, MKL_INT l, cplx *u_loc_e, cplx *u_loc_f, MailleSphere &MSph, const char chaine[]){

  /*---------------------------------------------------- input ------------------------------------------------------------
    e, f    : numeros globaux de voisins de la face l
    l       : face d'un élément du maillage
    u_loc_e : coordonnées d'un vecteur U sur la base de Trefftz de e
    u_loc_f : coordonnées du meme vecteur U sur la base de Trefftz de f
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT iloc, jloc;
  

  double din_e;//la direction de l'onde plane iloc scalaire avec la normale à la face l sur e
  double djn_e;//............................ jloc ........................................ e
  double djn_f;//............................ jloc ........................................ f

  cplx s = cplx(0.0, 0.0);

  cplx Ie, If, Ief, vale, valf, valef; 
  Ie = cplx(0.0,0.0); If = cplx(0.0, 0.0); Ief = cplx(0.0, 0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){

      if (l == 0) {
	din_e = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn_e = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn_e = djn_e * MSph.elements[e].OP[jloc].ksurOmega;
	
	din_e = din_e * MSph.elements[e].OP[iloc].ksurOmega;;
	
	djn_f = cblas_ddot(3, MSph.elements[f].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn_f = djn_f * MSph.elements[f].OP[jloc].ksurOmega;	
      }
      else if (l == 1) {
	din_e = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn_e = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn_e = djn_e * MSph.elements[e].OP[jloc].ksurOmega;
	
	din_e = din_e * MSph.elements[e].OP[iloc].ksurOmega;
	
	djn_f = cblas_ddot(3, MSph.elements[f].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn_f = djn_f * MSph.elements[f].OP[jloc].ksurOmega;	
      }
      else if (l == 2) {
	din_e = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn_e = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn_e = djn_e * MSph.elements[e].OP[jloc].ksurOmega;
	
	din_e = din_e * MSph.elements[e].OP[iloc].ksurOmega;
	
	djn_f = cblas_ddot(3, MSph.elements[f].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn_f = djn_f * MSph.elements[f].OP[jloc].ksurOmega;		
      }
      else if (l == 3) {
	din_e = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn_e = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn_e = djn_e * MSph.elements[e].OP[jloc].ksurOmega;
	
	din_e = din_e * MSph.elements[e].OP[iloc].ksurOmega;
	
	djn_f = cblas_ddot(3, MSph.elements[f].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn_f = djn_f * MSph.elements[f].OP[jloc].ksurOmega;
      }
      
      vale =  integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      Ie += u_loc_e[jloc] * conj(u_loc_e[iloc]) * din_e * djn_e * vale;
      
      valf = integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[f].OP[jloc], chaine);      
      
      If += u_loc_f[jloc] * conj(u_loc_e[iloc]) * din_e * djn_f * valf;
      
    }
  }
  
  s = Ie - If;
  
  return s;
}


cplx norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_les_faces_interieures_d_un_element(MKL_INT e, cplx *U, MailleSphere &MSph, const char chaine[]) {
  
  MKL_INT l, f;
  
  double s = 0.0;
  cplx I = cplx(0.0, 0.0);
  
  for( l = 0; l < 4; l++) {

    if (MSph.elements[e].voisin[l] >= 0){

      f = MSph.elements[e].voisin[l];
      
      cplx *u_loc_e, *u_loc_f;
      
      get_F_elem(MSph.ndof, e, &u_loc_e, U);
      get_F_elem(MSph.ndof, f, &u_loc_f, U);
      
      I += norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_interieure(e, f, l, u_loc_e, u_loc_f, MSph, chaine);
    }
  }

  //s = abs(I);

  return I;
  
}


double norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_exterieure(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]){
  
   /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numero global de voisins de la face l
    l       : face d'un élément du maillage
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT iloc, jloc;
  

  double din;//la direction de l'onde plane iloc scalaire avec la normale à la face l sur e
  double djn;//............................ jloc ........................................ e

  double s = 0.0;

  cplx I, val; 
  I = cplx(0.0,0.0);
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){ 

      if (l == 0) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 1) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 2) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 3) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;      
	
      }
      
      val =  integrale_triangle(MSph.face_double[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      I += u_loc[jloc] * conj(u_loc[iloc]) * din * djn * val;
      
    }
  }
  
  s = abs(I);
  
  return s;
}






double norme_H1_du_saut_avec_integ_analytique(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  MKL_INT e, f, ee;
  
  for (MKL_INT l = 0; l < MSph.N_Face_double; l++) {
    
    cplx *u_loc;

    if ((e >= 0) and (f < 0)){
      ee = e;
    }
    else if ((e < 0) and (f >= 0)){
      ee = f;
    }
    
    get_F_elem(MSph.ndof, ee, &u_loc, U);
    
    I += norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine) + norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine);
  }
  
  I = sqrt(I);

  return I;
}



double norme_H1_du_saut_avec_integ_analytique_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I1 = 0.0;
  double I2 = 0.0;
  double I, s1, s2;
  MKL_INT e, f, l, ee;
  
  
#pragma omp parallel private(s2, e, f, l, ee)
  {
    s2 = 0.0;
#pragma omp for schedule(runtime)
    for (l = 0; l < MSph.N_Face_double; l++) {
      
      cplx *u_loc;

      e = MSph.face_double[l].voisin1;
      f = MSph.face_double[l].voisin2;
      
      if ((e >= 0) and (f < 0)){
	ee = e;
	
	get_F_elem(MSph.ndof, ee, &u_loc, U);
	
	s2 += norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine) + norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine);
	
      }
      else if ((e < 0) and (f >= 0)){
	ee = f;
	
	get_F_elem(MSph.ndof, ee, &u_loc, U);
	
	s2 += norme_L2_du_saut_en_pression_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine) + norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_une_face_exterieure(ee, l, u_loc, MSph, chaine);

      }
      
    }
#pragma omp atomic
    I2 += s2;
  }
  
  I = I1 + I2;
  
  I = sqrt(I);
  
  return I;
}



double norme_L2_surfacique_en_pression_avec_integ_analytique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]){


   /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numeros globaux de voisins de la face l
    l       : face d'un élément du maillage
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT iloc, jloc;
  cplx I = cplx(0.0, 0.0);
  double s = 0.0;
  cplx val;
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){     
      
      val =  integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      I += u_loc[jloc] * conj(u_loc[iloc]) * val;
    }
  }
  
  s = abs(I);
  return s;
} 




double norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_une_face(MKL_INT e, MKL_INT l, cplx *u_loc, MailleSphere &MSph, const char chaine[]){


   /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numeros globaux de voisins de la face l
    l       : face d'un élément du maillage
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT iloc, jloc;
  cplx I = cplx(0.0, 0.0);
  double s = 0.0;
  cplx val;

  double din;//la direction de l'onde plane iloc scalaire avec la normale à la face l sur e
  double djn;//............................ jloc ........................................ e
  
  for(iloc = 0; iloc < MSph.ndof; iloc++){
    for(jloc = 0; jloc < MSph.ndof; jloc++){ 
      
      if (l == 0) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n1, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 1) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n2, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 2) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n3, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;
	
      }
      else if (l == 3) {
	din = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn = cblas_ddot(3, MSph.elements[e].OP[jloc].Ad, 1, MSph.elements[e].n4, 1) ;
	
	djn = djn * MSph.elements[e].OP[jloc].ksurOmega;
	
	din = din * MSph.elements[e].OP[iloc].ksurOmega;      
	
      }
      
      val =  integrale_triangle(MSph.elements[e].face[l], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);      
      
      I += u_loc[jloc] * conj(u_loc[iloc]) * val;
    }
  }
  
  s = abs(I);

  return s;
  
} 


double norme_L2_surfacique_en_pression_avec_integ_analytique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]){
  /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numeros globaux de voisins de la face l
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT l;
  double s = 0.0;
  for(l = 0; l < 4; l++){

    s += norme_L2_surfacique_en_pression_avec_integ_analytique_sur_une_face(e, l, u_loc, MSph, chaine);

  }

  return s;
}


double norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_un_element(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]){
  /*---------------------------------------------------- input ------------------------------------------------------------
    e       : numeros globaux de voisins de la face l
    u_loc   : coordonnées d'un vecteur U sur la base de Trefftz de e
    MSph    : le maillage
    chaine  : chaine de caractères pour déterminer la précisioo des intégrales
              "double" pour double précision
	      "quad" pour la précision quad
    /*-------------------------------------------------- output ------------------------------------------------------------
    /*---------------------------------------------------------------------------------------------------------------------*/

  MKL_INT l;
  double s = 0.0;
  for(l = 0; l < 4; l++){

    s += norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_une_face(e, l, u_loc, MSph, chaine);

  }

  return s;
}


double norme_H1_surfacique_avec_integ_analytique(cplx *U, MailleSphere &MSph, const char chaine[]){

  double I = 0.0;
  MKL_INT e;
  
  for(e = 0; e < MSph.get_N_elements(); e++){

    cplx *u_loc;
    get_F_elem(MSph.ndof, e, &u_loc, U);
    I +=  norme_L2_surfacique_en_pression_avec_integ_analytique_sur_un_element(e, u_loc, MSph, chaine) +  norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_un_element(e, u_loc, MSph, chaine);
    
  }
  I = sqrt(I);
  return I;
}



double norme_H1_surfacique_avec_integ_analytique_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]){

  double s, I = 0.0;
  MKL_INT e;
  
#pragma omp parallel private(s, e)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++){

      cplx *u_loc;
      get_F_elem(MSph.ndof, e, &u_loc, U);
      s +=  norme_L2_surfacique_en_pression_avec_integ_analytique_sur_un_element(e, u_loc, MSph, chaine) +  norme_L2_surfacique_en_velocite_avec_integ_analytique_sur_un_element(e, u_loc, MSph, chaine);
      
    }
#pragma omp atomic
    I += s;
  }

  I = sqrt(I);

  return I;
}


/*&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&*/
double norme_H1_volumique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) {

  double I, J;
  I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    J =  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
    I += J;

  }

  I = sqrt(I);
  
  return I;
}



double  norme_H1_erreur_volumique_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I, J;
  I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    J = norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);

    I += J;

  }

  I = sqrt(I);
  
  return I;
}



double norme_H1_erreur_surfacique_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I, J;
  I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {
    
    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    J =  norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(e, u_loc, MSph) + norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    
    I += J;
  }

  I = sqrt(I);
  return I;
}


double norme_H1_surfacique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) {
  
  MKL_INT e;
  
  double I, J;
  I = 0.0;

  for (e = 0; e < MSph.get_N_elements(); e++) {
    
    J = norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_un_element(e, MSph) + norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_un_element(e, MSph);
    
    I += J;
  }
  
  I = sqrt(I);

  return I;
}


double norme_H1_surfacique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph) {
  
  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s += norme_L2_surfacique_pour_Bessel_en_pression_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, MSph) + norme_L2_surfacique_pour_Bessel_en_velocite_avec_integ_numerique_sur_les_faces_exterieures_d_un_element(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  
  I = sqrt(I);
  
  return I;
}



double norme_H1_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=  norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}


double  norme_H1_erreur_volumique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  I = sqrt(I);
  
  return I;
}


double norme_H1_erreur_surfacique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_surfacique_en_pression_avec_integ_numerique_sur_un_element(e, u_loc, MSph) + norme_L2_erreur_surfacique_en_velocite_avec_integ_numerique_sur_un_element(e, u_loc, MSph);
    }
    #pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  return I;
}
/******************************************************************************************************************/
double norme_H1_du_saut_d_un_vecteur(cplx *U, MailleSphere &MSph, const char chaine[]) {
  
  cplx I = cplx(0.0, 0.0);
  double s = 0.0;
  MKL_INT e;
  
  for (e = 0; e < MSph.get_N_elements(); e++) {
    
    I +=  norme_L2_du_saut_en_pression_avec_integ_analytique_sur_les_faces_interieures_d_un_element(e, U, MSph, chaine)  +  norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_les_faces_interieures_d_un_element(e, U, MSph, chaine);
    
  }
  
  s = abs(I);
  return s;
}


double norme_H1_du_saut_d_un_vecteur_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) {
  
  double I = 0.0;
  MKL_INT e, f, l;
  //double s ;
  cplx s = cplx(0.0, 0.0);

#pragma omp parallel private(s)
  {
    s = cplx(0.0, 0.0);
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=  norme_L2_du_saut_en_pression_avec_integ_analytique_sur_les_faces_interieures_d_un_element(e, U, MSph, chaine)  +  norme_L2_du_saut_en_velocite_avec_integ_analytique_sur_les_faces_interieures_d_un_element(e, U, MSph, chaine);
      
    }
    
#pragma omp atomic
    I += abs(s);
  }
  
  return I;
}


double norme_H1_de_l_erreur_avec_integ_mixed(cplx *U, MailleSphere &MSph, const char chaine[]){
  double I = 0.0;
  
  //I = norme_H1_du_saut_d_un_vecteur(U, MSph, chaine);
  I = norme_H1_du_saut_d_un_vecteur_par_assemblage_parallel_version(U, MSph,chaine);
  I += norme_H1_de_l_erreur_avec_integ_numerique_sur_les_faces_exterieures_parallel_version(U, MSph);
  
  I = sqrt(I);

  return I;
}

/*===========================================================================================================================*/
double norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]){
   
   double I = 0.0;
   double s1, s2, s3;

   cplx J;
   J = cplx(0.0, 0.0);

   MKL_INT iloc, jloc;
   cplx val;

   //int_{e} |P_ex|^2
   s1 = abs(integrale_tetra(MSph.elements[e], MSph.OP_inc, MSph.OP_inc, chaine));

   //int_{e} |P_uloc|^2
   for(iloc = 0; iloc < MSph.ndof; iloc++){
     for(jloc = 0; jloc < MSph.ndof; jloc++){
       val = integrale_tetra(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
       J = J + u_loc[jloc] * conj(u_loc[iloc]) * val;
     } 
   }

   s2 = abs(J);

   J = cplx(0.0, 0.0);

   //int_{e} P_ex conj(P_uloc)
   for(iloc = 0; iloc < MSph.ndof; iloc++){
     val =  integrale_tetra(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
     J = J + conj(u_loc[iloc]) * val;
   }

   s3 = - 2.0 * real(J);
  
   I = s1 + s2 + s3;
   
  return I;
}



double norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(MKL_INT e, cplx *u_loc, MailleSphere &MSph, const char chaine[]){
   
   double I = 0.0;
   double s1, s2, s3;

   cplx J;
   J = cplx(0.0, 0.0);

   MKL_INT iloc, jloc;
   cplx val;

   //int_{e} |P_ex|^2
   s1 = abs(integrale_tetra_VV(MSph.elements[e], MSph.OP_inc, MSph.OP_inc, chaine));

   //int_{e} |P_uloc|^2
   for(iloc = 0; iloc < MSph.ndof; iloc++){
     for(jloc = 0; jloc < MSph.ndof; jloc++){
       val = integrale_tetra_VV(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc], chaine);
       J = J + u_loc[jloc] * conj(u_loc[iloc]) * val;
     } 
   }

   s2 = abs(J);

   J = cplx(0.0, 0.0);

   //int_{e} P_ex conj(P_uloc)
   for(iloc = 0; iloc < MSph.ndof; iloc++){
     val =  integrale_tetra_VV(MSph.elements[e], MSph.elements[e].OP[iloc], MSph.OP_inc, chaine);
     J = J + conj(u_loc[iloc]) * val;
   }

   s3 = - 2.0 * real(J);
  
   I = s1 + s2 + s3;
   
  return I;
  
}



double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  double I = 0.0;
  
  I = abs(integrale_tetra(MSph.elements[e], MSph.OP_inc, MSph.OP_inc, chaine));
  
  return I;
}



double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(MKL_INT e, MailleSphere &MSph, const char chaine[]) {
  double I = 0.0;

  I = abs(integrale_tetra_VV(MSph.elements[e], MSph.OP_inc, MSph.OP_inc, chaine));
  
  return I;
}



double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    I +=  norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}


double norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  double s = 0.0;
  MKL_INT e ;
  
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=  norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}


double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    I +=  norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}


double norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  double s = 0.0;
  MKL_INT e;
  
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
  for (e = 0; e < MSph.get_N_elements(); e++) {

    s +=  norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

  }
#pragma omp atomic
  I += s;
  }

  I = sqrt(I);
  
return I;
}
// /*---------------------------------------------------------------------------------------------------------------*/
double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}

double  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
  for (e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);

  }
#pragma omp atomic
  I += s;
  }
  I = sqrt(I);
  
  return I;
}



double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}



double  norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) {
  
  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
    
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s += norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);
      
    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

double norme_H1_volumique_pour_OndePlaneIncidente_avec_integ_numerique(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    I = I + norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine) + norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}


double norme_H1_volumique_pour_OndePlaneIncidente_avec_integ_numerique_parallel_version(MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  double s = 0.0;
  MKL_INT e;
  
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
  for (e = 0; e < MSph.get_N_elements(); e++) {

    s += norme_L2_volumique_pour_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine) + norme_L2_volumique_pour_velocite_OndePlaneIncidente_avec_integ_numerique(e, MSph, chaine);

  }
#pragma omp atomic
  I += s;
  }

  I = sqrt(I);
  
return I;
}

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
double  norme_H1_erreur_volumique_avec_integ_numerique_SourceOndePlane(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    I += norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine) + norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);

  }

  I = sqrt(I);
  
  return I;
}

double  norme_H1_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane_parallel_version(cplx *U, MailleSphere &MSph, const char chaine[]) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
  for (e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    s += norme_L2_erreur_volumique_en_pression_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine) + norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_SourceOndePlane(e, u_loc, MSph, chaine);

  }
#pragma omp atomic
  I += s;
  }
  I = sqrt(I);
  
  return I;
}


/*-----------------------------------------------------------------------------------------------*/

void prodMatreal_Vectcomplex_ColMajor(double *A, MKL_INT m, MKL_INT n, cplx *V, MKL_INT p, cplx *b){

  if (p != n){
    cout<<" le nombre de colonne de la matrice est different du nombre de ligne du vecteur "<<endl;
  }
  else{
    MKL_INT i, j;
    for (i = 0; i < n; i++)
      b[i] = cplx(0.0, 0.0);

    for (i = 0; i < m; i++) {
      for (j = 0; j < n; j++) {
	b[i] += A[i + j * m] * V[j];
      }
    }
  }
}
/*******************************************************************************************************************/
cplx integrale_tetra_VV_ponderee(Element &E, MKL_INT iloc, MKL_INT jloc, const char chaine[]) {
  cplx I, J;
  double dd;
 
  double *W = nullptr; W = new double[3];

  //W = A * Ad_jloc
  cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, 1.0, E.Ac.value, 3, E.OP[jloc].Ad, 1, 0.0, W, 1);

  //dd = ((A * k * diloc)/omega) \cdot (k W / omega) 
  dd = cblas_ddot(3, E.OP[iloc].Ad, 1, W, 1) ;
  dd = dd * E.OP[iloc].ksurOmega * E.OP[jloc].ksurOmega;
  
  /* integrale de conj(P_iloc) P_jloc */
  J = integrale_tetra(E, iloc, jloc, chaine);
  
  I = dd * J;

  delete [] W; W = 0;

  return I;
  
}


cplx integrale_tetra_VV_ponderee(Element &E, Onde_plane &P1, Onde_plane &P2, const char chaine[]) {
  cplx I, J;
  double dd;
  MKL_INT lda = 3, inc = 1;
  
  double *W = nullptr; W = new double[3];
  
  //W = A * Ad_jloc
  cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, 1.0, E.Ac.value, 3, P2.Ad, 1, 0.0, W, 1);

  //dd = ((A * k * P1.d)/omega) \cdot (k W / omega) 
  dd = cblas_ddot(lda, P1.Ad, inc, W, inc) ;
  dd = dd * P1.ksurOmega * P2.ksurOmega;
  
  /* integrale de conj(P1) P2 */
  J = integrale_tetra(E, P1, P2, chaine);
  
  I = dd * J;

  delete [] W; W = 0;

  return I;
  
}


cplx quad_tetra_VV_ponderee(Element &Elem, Quad &Qd, Onde_plane &P1, Onde_plane &P2) {
  //Cette fonction calcule l'intégrale du produit de deux vitesses d'onde planes sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    Elem   : objet de type Element. Elle contient tout ce qui est en rapport à un element
    Qd     : objet de type Quad. Il contient la quadratute de Gauss-Legendre
    P1     : objet de type Onde_plane. C'est une fonction-test
    P2     : ......................... C'est la solution
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx *W = nullptr; W = new cplx[3];
  x1 = Elem.noeud[0]->get_X();// le noeud numero 1
  x2 = Elem.noeud[1]->get_X();//................ 2
  x3 = Elem.noeud[2]->get_X();//................ 3
  x4 = Elem.noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < Qd.ordre * Qd.ordre * Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + Qd.Xtetra[3*i] * x2x1[j] + Qd.Xtetra[3*i+1] * x3x1[j] + Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calcule de P et V pour l'onde P1
    P1.compute_P_and_V(Xquad);

    //calcule de P et V pour l'onde P2
    P2.compute_P_and_V(Xquad);

    //W = A P2.V
    prodMatreal_Vectcomplex_ColMajor(Elem.Ac.value, 3, 3, P2.V, 3, W);

    J = dot_c(P1.V, W);
    
    I = I + Qd.Wtetra[i] * J;
  }

  I = I * Elem.Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad = 0;
  delete [] x2x1; x2x1 = 0;
  delete [] x3x1; x3x1 = 0;
  delete [] x4x1; x4x1 = 0;
  delete [] W; W = 0;
  return I;
}



void buildVolumeMetrique_ponderee(MKL_INT ndof, cplx *M, Element &E, const char chaine[]){
  //Cette fonction calcul sur chaque l'element de structure E, la matrice associée associée
  // à la métrique volumique  (p, p') + (v, v') = int_{E} p p' + v v'
   /*-------------------------------------------------------------------- input -------------------------------------------
    ndof     : la dimension maximale de la base de Trefftz dans chaque élément du maillage
    M        : objet définissant la matrice de notre métrique volumique
    E        : structure de type Element, elle renforme les infos sur l'element correspondant
    chaine   : pour définir la précision des intégrales analytiques
               chaine = "double" pour une double précision
	       sinon c'est la précision en quad
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT iloc, jloc; //boucle sur les fonctions de bases
    /*------------------------------------------------------------------------------------------------------------------*/

  //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
  for(iloc = 0; iloc < ndof; iloc++) {
    for(jloc = 0; jloc < ndof; jloc++) {
      M[jloc * ndof + iloc] =  E.xi * integrale_tetra(E, iloc, jloc, chaine) + integrale_tetra_VV_ponderee(E, iloc, jloc, chaine);
    }
  }
}


void buildVolumeMetrique_ponderee(MKL_INT ndof, cplx *M, MailleSphere &MSph, const char chaine[]){
  //Cette fonction calcul sur chaque l'element de structure E, la matrice associée associée
  // à la métrique volumique  (p, p') + (v, v') = int_{E} p p' + v v'
   /*-------------------------------------------------------------------- input -------------------------------------------
    ndof     : la dimension maximale de la base de Trefftz dans chaque élément du maillage
    M        : objet définissant la matrice de notre métrique volumique
    E        : structure de type Element, elle renforme les infos sur l'element correspondant
    chaine   : pour définir la précision des intégrales analytiques
               chaine = "double" pour une double précision
	       sinon c'est la précision en quad
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT iloc, jloc; //boucle sur les fonctions de bases
    /*------------------------------------------------------------------------------------------------------------------*/
   //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
  for(iloc = 0; iloc < ndof; iloc++) {
    for(jloc = 0; jloc < ndof; jloc++) {
      M[jloc * ndof + iloc] =  MSph.fixe_element.xi * quad_tetra(MSph.fixe_element, MSph.Qd, MSph.fixe_element.OP[iloc], MSph.fixe_element.OP[jloc]) + quad_tetra_VV_ponderee(MSph.fixe_element, MSph.Qd, MSph.fixe_element.OP[iloc], MSph.fixe_element.OP[jloc]);
    }
  }
}



void buildVolumeMetrique_ponderee_parallel_version(MKL_INT ndof, cplx *M, MailleSphere &MSph, const char chaine[]){
  //Cette fonction calcul sur chaque l'element de structure E, la matrice associée associée
  // à la métrique volumique  (p, p') + (v, v') = int_{E} p p' + v v'
   /*-------------------------------------------------------------------- input -------------------------------------------
    ndof     : la dimension maximale de la base de Trefftz dans chaque élément du maillage
    M        : objet définissant la matrice de notre métrique volumique
    E        : structure de type Element, elle renforme les infos sur l'element correspondant
    chaine   : pour définir la précision des intégrales analytiques
               chaine = "double" pour une double précision
	       sinon c'est la précision en quad
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
 
    /*------------------------------------------------------------------------------------------------------------------*/
   //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
#pragma omp parallel
  {
    MKL_INT iloc, jloc; //boucle sur les fonctions de bases
#pragma omp for schedule(runtime) 
    for(iloc = 0; iloc < ndof; iloc++) {
      for(jloc = 0; jloc < ndof; jloc++) {
	M[jloc * ndof + iloc] =  MSph.fixe_element.xi * quad_tetra(MSph.fixe_element, MSph.Qd, MSph.fixe_element.OP[iloc], MSph.fixe_element.OP[jloc]) + quad_tetra_VV_ponderee(MSph.fixe_element, MSph.Qd, MSph.fixe_element.OP[iloc], MSph.fixe_element.OP[jloc]);
      }
    }
  }
}



void BuildVolumeMetricMatrix_ponderee(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
    /*------------------------------------------------------------------------------------------------------------------*/

  //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
  for(e = 0; e < MSph.get_N_elements(); e++) {

    cplx *M =  M_glob.Tab_A_loc[e].get_Aloc(); // pour stocker l'adresse du bloc associé à e

    MKL_INT iloc, jloc; //boucle sur les fonctions de bases
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      for(jloc = 0; jloc < MSph.ndof; jloc++) {
	M[jloc * MSph.ndof + iloc] =  MSph.elements[e].xi * integrale_tetra(MSph.elements[e], iloc, jloc, chaine) + integrale_tetra_VV_ponderee(MSph.elements[e], iloc, jloc, chaine);
      }
    }
  }
}


void BuildVolumeMetricMatrix_ponderee_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
    /*------------------------------------------------------------------------------------------------------------------*/
#pragma omp parallel private(e)
  {
#pragma omp for schedule(runtime) 
    for(e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *M =  M_glob.Tab_A_loc[e].get_Aloc(); // pour stocker l'adresse du bloc associé à e
      
      MKL_INT iloc, jloc; //boucle sur les fonctions de bases
      for(iloc = 0; iloc < MSph.ndof; iloc++) {
	for(jloc = 0; jloc < MSph.ndof; jloc++) {
	  M[jloc * MSph.ndof + iloc] =   MSph.elements[e].xi * integrale_tetra(MSph.elements[e], iloc, jloc, chaine) + integrale_tetra_VV_ponderee(MSph.elements[e], iloc, jloc, chaine);
	}
      }
    }
  }
}


void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
    /*------------------------------------------------------------------------------------------------------------------*/
#pragma omp parallel private(e)
  {
#pragma omp for schedule(runtime) 
    for(e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *M =  M_glob.Tab_A_loc[e].get_Aloc(); // pour stocker l'adresse du bloc associé à e
      
      MKL_INT iloc, jloc; //boucle sur les fonctions de bases
      for(iloc = 0; iloc < MSph.ndof; iloc++) {
	for(jloc = 0; jloc < MSph.ndof; jloc++) {
	  M[jloc * MSph.ndof + iloc] =   MSph.elements[e].xi * quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]) + quad_tetra_VV_ponderee(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
	}
      }
    }
  }
}


double norme_L2_ponderee_erreur_volumique_en_pression_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx P;
   double I = 0.0;
   cplx J = cplx(0.0, 0.0);
   for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
     for (j = 0; j < 3; j++) {
       Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
     }
     
     //Evaluation de alpha * j0 + beta * y_0 en Xquad
     SolexacteBessel(e, Xquad, &P, MSph);
     
     //Evaluation de la solution numérique en Xquad
     combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);
     
     //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
     // Pour P_{inc} on n'a pas de 1 / sqrt(2.0);
     //Avec le prouit scalaire pondere xi p overline(p') + A v overline(v')
     //inutile de multiplié par xi car il se simplifie
     J = J + MSph.Qd.Wtetra[i] * (P - MSph.elements[e].Pexacte) * conj(P - MSph.elements[e].Pexacte);
     
   }
   
   I = real(J * MSph.elements[e].Jac);
   
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  return I;
}



double norme_L2_ponderee_erreur_volumique_en_velocite_avec_integ_numerique(MKL_INT e, cplx *u_loc, MailleSphere &MSph){
   MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx *V = new cplx[3];
  cplx *W = nullptr; W = new cplx[3];
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
   cplx J, P;
   double I = 0.0;
   cplx T = cplx(0.0, 0.0);
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de alpha * j0 + beta * y_0 en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    //Evaluation de la solution numérique en Xquad
    combinaison_solution(MSph.elements[e], MSph.ndof, u_loc, Xquad);

    //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
    for(j = 0; j < 3; j++){
      V[j] = V[j] -  MSph.elements[e].Vexacte[j];
    }
    
    //W = A V
    prodMatreal_Vectcomplex_ColMajor(MSph.elements[e].Ac.value, 3, 3, V, 3, W);
    
    J = W[0] * conj(V[0]) + W[1] * conj(V[1]) + W[2] * conj(V[2]);
    
    T = T + MSph.Qd.Wtetra[i] * J;
  }

  I = real(T * MSph.elements[e].Jac);

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  delete [] W; W  =  nullptr;
  
  return I;
}


double norme_L2_ponderee_volumique_pour_Bessel_en_velocite_avec_integ_numerique(MKL_INT e, MailleSphere &MSph) {

  
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];
  cplx P;
  cplx *V = new cplx[3];
  cplx *W = nullptr; W = new cplx[3];
  
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4
  
  for (j = 0; j < 3; j++) {
    x2x1[j] = x2[j] - x1[j];//x2 - x1
    x3x1[j] = x3[j] - x1[j];//X3 - x1
    x4x1[j] = x4[j] - x1[j];//x4 - x1
  }

  
  double I = 0.0;
  cplx J = cplx(0.0, 0.0);
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }
    
    //Evaluation de Be en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);
    
    //W = A V
    prodMatreal_Vectcomplex_ColMajor(MSph.elements[e].Ac.value, 3, 3, V, 3, W);
    
    J = J + MSph.Qd.Wtetra[i] * (W[0] * conj(V[0]) + W[1] * conj(V[1]) + W[2] * conj(V[2]));
  }
  
  I = real(J * MSph.elements[e].Jac);

  //I = sqrt(I);
  
  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
 
  delete [] V; V  =  nullptr;
  delete [] W; W  =  nullptr;
  
  return I;
}


double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique(cplx *U, MailleSphere &MSph) {

  double I, J;
  I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    cplx *u_loc;
    
    get_F_elem(MSph.ndof, e, &u_loc, U);
    
    J =  MSph.elements[e].xi * norme_L2_ponderee_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_ponderee_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);

    I += J;

  }

  I = sqrt(I);
  
  return I;
}


double  norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(cplx *U, MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *u_loc;
      
      get_F_elem(MSph.ndof, e, &u_loc, U);
      
      s +=  MSph.elements[e].xi * norme_L2_ponderee_erreur_volumique_en_pression_avec_integ_numerique(e, u_loc, MSph) + norme_L2_ponderee_erreur_volumique_en_velocite_avec_integ_numerique(e, u_loc, MSph);
    }
#pragma omp atomic
    I += s;
  }

  I = sqrt(I);
  
  return I;
}



double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique(MailleSphere &MSph) {

  double I, J;
  I = 0.0;
  
  for (MKL_INT e = 0; e < MSph.get_N_elements(); e++) {

    J =   MSph.elements[e].xi * norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_ponderee_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
    I += J;

  }

  I = sqrt(I);
  
  return I;
}


double norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MailleSphere &MSph) {

  double I = 0.0;
  double s;
  MKL_INT e;
#pragma omp parallel private(s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {
      
      s +=   MSph.elements[e].xi * norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique(e, MSph) + norme_L2_ponderee_volumique_pour_Bessel_en_velocite_avec_integ_numerique(e, MSph);
      
    }
#pragma omp atomic
    I += s;
  }
  I = sqrt(I);
  
  return I;
}



cplx quad_tetra_Bessel_VV_ponderee(MKL_INT e, MKL_INT iloc, MailleSphere &MSph) {
  //Cette fonction calcule l'intégrale du produit de deux fonction de bases w_jloc * conj(w_iloc) sur un tetra
  /*------------------------------------------------------------ input -----------------------------------------------------
    e      : indice du tetra
    iloc   : numero d'une onde plane
    MSph   : le maillage
    /*---------------------------------------------------------- ouput ----------------------------------------------------
    I      : le résultat de l'intégrale
    /*----------------------------------------------------------------------------------------------------------------------*/
  MKL_INT i, j;
  double *x1, *x2, *x3, *x4;
  double *Xquad = new double[3];
  double *x2x1 = new double[3];
  double *x3x1 = new double[3];
  double *x4x1 = new double[3];

  cplx P;
  cplx *V = new cplx[3];
  cplx *W = nullptr; W = new cplx[3];
  x1 = MSph.elements[e].noeud[0]->get_X();// le noeud numero 1
  x2 = MSph.elements[e].noeud[1]->get_X();//................ 2
  x3 = MSph.elements[e].noeud[2]->get_X();//................ 3
  x4 = MSph.elements[e].noeud[3]->get_X();//................ 4

   for (j = 0; j < 3; j++) {
     x2x1[j] = x2[j] - x1[j];//x2 - x1
     x3x1[j] = x3[j] - x1[j];//X3 - x1
     x4x1[j] = x4[j] - x1[j];//x4 - x1
   }
   
  
  cplx I = cplx(0.0,0.0);
  cplx J;
  
  for (i = 0; i < MSph.Qd.ordre * MSph.Qd.ordre * MSph.Qd.ordre; i++) {
    for (j = 0; j < 3; j++) {
      Xquad[j] = x1[j] + MSph.Qd.Xtetra[3*i] * x2x1[j] + MSph.Qd.Xtetra[3*i+1] * x3x1[j] + MSph.Qd.Xtetra[3*i+2] * x4x1[j];
    }

    //calul de la solution analytique en Xquad
    SolexacteBessel(e, Xquad, &P, V, MSph);

    //calcul de P et V pour l'onde OP[iloc]
    MSph.elements[e].OP[iloc].compute_P_and_V(Xquad);

    //W = A V
    //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
    prodMatreal_Vectcomplex_ColMajor(MSph.elements[e].Ac.value, 3, 3, V, 3, W);

    //J = A V \cdot OP[iloc].V
    J = dot_c(MSph.elements[e].OP[iloc].V, W);
    
    I = I + MSph.Qd.Wtetra[i] * J;
  }

  I = I * MSph.elements[e].Jac;

  //liberation de la mémoire
  delete [] Xquad; Xquad  =  nullptr;
  delete [] x2x1; x2x1  =  nullptr;
  delete [] x3x1; x3x1  =  nullptr;
  delete [] x4x1; x4x1  =  nullptr;
  delete [] V; V  =  nullptr;
  delete [] W; W  =  nullptr;
  
  return I;
}


void BuildVector_Of_Volume_Projection_ponderee(cplx *B, MailleSphere &MSph){
  MKL_INT e, iloc;
  for(e = 0; e < MSph.get_N_elements(); e++) {
    cplx *b = B + e * MSph.ndof;

    //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
    for(iloc = 0; iloc < MSph.ndof; iloc++) {
      b[iloc] =  MSph.elements[e].xi * quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV_ponderee(e, iloc, MSph);
    }
  }
}


void BuildVector_Of_Volume_Projection_ponderee_parallel_version(cplx *B, MailleSphere &MSph){
  MKL_INT e;

  //une onde plane P = 1 / sqrt(2xi) exp(ik d_j x), V = A^{-1} d /sqrt(d A^{-1} d)
#pragma omp parallel
  {
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++) {
      cplx *b = B + e * MSph.ndof;
      
      for(MKL_INT iloc = 0; iloc < MSph.ndof; iloc++) {
	b[iloc] =  MSph.elements[e].xi * quad_tetra_Bessel(e, iloc, MSph) + quad_tetra_Bessel_VV_ponderee(e, iloc, MSph);
      }
    }
  }
}



void BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_and_optimized_version(A_skyline &M_glob, MailleSphere &MSph, const char chaine[]){
  /*-------------------------------------------------------------------- input -------------------------------------------
    M_glob   : objet définissant la matrice de notre métrique volumique
    Msph     : le maillage
    /*----------------------------------------------------------------- output -----------------------------------------
                                             M_glob
  /*-------------------------------------------------------------- autres variables ----------------------------------------*/
  MKL_INT e; //boucle sur les blocs
    /*------------------------------------------------------------------------------------------------------------------*/

  double *W = nullptr;
  double dd;
#pragma omp parallel private(e, W, dd)
  {
    W = new double[3];
#pragma omp for schedule(runtime)
    for(e = 0; e < MSph.get_N_elements(); e++) {
      
      cplx *M =  M_glob.Tab_A_loc[e].get_Aloc(); // pour stocker l'adresse du bloc associé à e
      
      MKL_INT iloc, jloc; //boucle sur les fonctions de bases
      for(iloc = 0; iloc < MSph.ndof; iloc++) {
	for(jloc = 0; jloc < MSph.ndof; jloc++) {

	  //W = A * Ad_jloc
	  cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 3, 1.0, MSph.elements[e].Ac.value, 3, MSph.elements[e].OP[jloc].Ad, 1, 0.0, W, 1);
	  
	  //dd = ((A * k * P1.d)/omega) \cdot (k W / omega) 
	  dd = cblas_ddot(3, MSph.elements[e].OP[iloc].Ad, 1, W, 1) ;
	  dd = dd * MSph.elements[e].OP[iloc].ksurOmega * MSph.elements[e].OP[jloc].ksurOmega;

	  M[jloc * MSph.ndof + iloc] =  (MSph.elements[e].xi + dd) * quad_tetra(MSph.elements[e], MSph.Qd, MSph.elements[e].OP[iloc], MSph.elements[e].OP[jloc]);
	}
      }
    }
    delete [] W; W = 0;
  }
}
/*##################################################################################################################*/
void store_P_and_transpose_P_local_to_process(MKL_INT indice_glob, MKL_INT indice_loc, cplx *a, cplx *b, A_skyline &P_red,  MailleSphere &MSph) {

   /*-------------------------------------------------------------------------- input ------------------------------------------------------------
    e         : le numero de l'élément
    a         : tableau complex de taille ndof * Nred, il contiendra la restriction de P_red 
                sur l'élément e
    b         : ......................... Nred * ndof, ............................ de
                 l'adjoint de P sur l'élément e 
    P_red     : Tableau contenant les N_red vecteurs propres associées aux Nred 
                plus grandes valeurs propres
    MSph      : le maillage
    /*------------------------------------------------------------------------ ouput ------------------------------------------------------------
    a, b     :
    /*---------------------------------------------------------------- variables intermédiaires -----------------------------------------
    PP_red    : contient la restriction de P_red sur l'élément e mais pas en taille réelle
    iloc, jloc: boucle sur les fonctions de bases
    JLOC      : 
    Nred      : les plus grandes valeurs propres par ordre croissant
    /*-----------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT iloc, jloc;
  
  cplx *PP_red = P_red.Tab_A_loc[indice_loc].get_Aloc();
  
  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
   
    for (jloc = 0; jloc < MSph.elements[indice_glob].Nred; jloc++) { // numero de la colonne	  	
      
      a[jloc * MSph.ndof + iloc] = PP_red[jloc * MSph.ndof + iloc];
      
      b[iloc * MSph.elements[indice_glob].Nred + jloc] = conj(PP_red[jloc * MSph.ndof + iloc]);
   
    }      
  }
}



void compute_A_red_ColMajor(MKL_INT indice_glob, MKL_INT indice_loc, A_skyline &M_glob, cplx *M_red, MailleSphere &MSph, cplx *MP, cplx *P, cplx *adjtP){

   /*------------------------------------------------------------------------- input ---------------------------------------------------------
    e       : indice de l'élément
    M_glob  : structure définissant la matrice de la projection
    P_red   : contient les Nred_Pro plus grands vecteurs propres associés au plus grandes valeurs propres de la matrice M
    M_red   : conj(trans(P_red)) *  M * P_red
    MSph    : le maillage
    /*----------------------------------------------------------------------- ouput ---------------------------------------------------------
                                                           A_red
    /*----------------------------------------------------------------------- intermedaire -------------------------------------------------
    P       :=  P_red
    AdjtP   := conj(trans(P_red))
    MP       := M * b
    /*-------------------------------------------------------------------------------------------------------------------------------------*/
  
  cplx *M = M_glob.Tab_A_loc[indice_loc].get_Aloc();

  //MP = M  P_red[e]
  prodAB_ColMajor(M, MSph.ndof, MSph.ndof, P, MSph.ndof, MSph.elements[indice_glob].Nred, MP) ;
  
  //M_red = P_red[e]^* MP = P_red[e]^* M P_red 
  prodAB_ColMajor(adjtP, MSph.elements[indice_glob].Nred, MSph.ndof, MP, MSph.ndof, MSph.elements[indice_glob].Nred, M_red) ;
  
}



void collection_Solutions_locales_parallel_version(int rang, int *counts_elem, int *displs_elem, cplx *U, A_skyline &M_glob, A_skyline &P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, taille =  MSph.ndof *  MSph.ndof;

  MKL_INT debut, fin, l = 0;
  
  debut = displs_elem[rang];
  fin = debut  + counts_elem[rang];
  
  cplx *MP, *P, *adjtP, *B;
  MatMN M_red;
  VecN u_red;
  
#pragma omp parallel private(MP, P, adjtP, e, M_red, u_red, B, l)
  {
    
    MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 
    
    P = new cplx[taille];//Pour stocker la matrice P_red[e]
    
    adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])
    
    M_red.m = MSph.ndof;
    M_red.n = MSph.ndof;
    
    M_red.Allocate_MatMN();
    
    
    u_red.n = MSph.ndof;
    u_red.Allocate();
    
#pragma omp for schedule(runtime)
    for(e = debut; e < fin; e++){

      l = e - debut;
      
      initialize_tab(taille, M_red.value);
      initialize_tab(MSph.ndof, u_red.value);
      
      
      M_red.m = MSph.elements[e].Nred;
      M_red.n = MSph.elements[e].Nred;
      
      u_red.n = MSph.elements[e].Nred;
    
      //on clône l'emplacement U correspondant à l'élément e
      get_F_elem(MSph.ndof, l, &B, U); 
      
      MSph.elements[e].Nred_Pro = MSph.elements[e].Nred;
      
      MSph.elements[e].Nres_Pro = MSph.elements[e].Nres;
      
      //stcokage sur e de P et son adjoint
      store_P_and_transpose_P_local_to_process(e, l, P, adjtP, P_red, MSph);
     
      //Calcul du bloc réduit M_red.value
      compute_A_red_ColMajor(e, l, M_glob, M_red.value, MSph, MP, P, adjtP);
      
      //calcul de u_red = conj(trans(PP_red)) * B;    
      compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP); 
      
      //résolution du système linéaire réduit par la méthode de CHOLESKY
      //La Matrice et le vecteur seront écrasés
      M_red.Solve_by_CHOLESKY_ColMajor(u_red);          
  
       //On retrouve la solution dans l'ancienne base B = P_red u_red 
      reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P);
    }
    cout<<"Libération finale de la mémoire :"<<endl; 
    delete [] M_red.value; M_red.value =  nullptr;
    delete [] u_red.value; u_red.value =  nullptr;
    delete [] MP ; MP =  nullptr;
    delete [] P ; P =  nullptr;
    delete [] adjtP ; adjtP =  nullptr;
    
  }
}



void collection_Solutions_locales_parallel_version(int rang, int *counts_elem, int *displs_elem, cplx *U, A_skyline &M_glob, cplx *P_red, MailleSphere &MSph) {
  //Cette fonction calcule et stocke les solutions locales dans un vecteur par la methode de la projection.
  //Chaque solution est calculée dans une base réduite obtenue en décomposant
  //LA MATRICE DE LA PROJECTION, puis on exprime la solution dans la base pleine.

  /*-------------------------------------------------------------------------------- input -------------------------------------------------------------------------------
    U       : vecteur pour stocker toutes les solutions locales
    choice  : chaîne de caractère pour définir la taille des blocs selon les blocs 
              à décomposer.
              choice = "one" C'est la matrice M de Cessenat-Desprès qu'on décompose
              Alors, Nred = Nred_Pro
	      choice = "two" C'est la matrice de la projection qu'on décompose.
              Dans ce cas Nred != Nred_Pro
	      Nred taille de chaque bloc qui en dehors de la projection
	      Nred_Pro taille de chaque bloc en projection
    P_red   : Tableau A_skyline qui contiendra les vecteurs propres réduits
    MSph    : le maillage
  /*------------------------------------------------------------------------------ ouput ------------------------------------------------------------------------------
         U     : Collection des solutions locales
    /*------------------------------------------------------------------------------ intermédiares ----------------------------------------------------------------------
    e          : numero d'un élément
    iloc       : boucle sur les composant de chaque locale
    MP         : objet de type MatMN. Il contient la matrice de la projection sur e
    u          : objet de type VecN. Il contient le vecteur de la projection sur e
    taille     : la somme des tailles des blocs
    MP_red     : la matrice réduite de la projection sur e
    u_red      : vecteur réduite de la projection sur e. Il contiendra la solution réduite après résolution
    B          : On clône le vecteur U sur chaque élément
    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  
  MKL_INT e, taille =  MSph.ndof *  MSph.ndof, taille_red = MSph.fixe_element.Nred * MSph.fixe_element.Nred;

  MKL_INT debut, fin, l = 0;
  
  debut = displs_elem[rang];
  fin = debut  + counts_elem[rang];
  
  cplx *MP, *adjtP, *B, *G, *v;
  MatMN M_red;
  VecN u_red;

  adjtP = nullptr;
  adjtP = new cplx[taille];//Pour stocker la matrice conj(P_red[e])

  store_P_and_transpose_P(P_red, adjtP, MSph);
  
#pragma omp parallel private(MP, e, l, M_red, u_red, B, G, v)
  {
    MP = nullptr; v = nullptr; G = nullptr;
    MP = new cplx[taille];//Pour stocker la matrice M_glob[e] * P_red[e] 
    G = new cplx[taille_red];//Pour stocker la matrice réduite à chaque itération e
    v = new cplx[MSph.fixe_element.Nred];//Pour stocker la solution réduite à chaque itération e
    
    M_red.m = MSph.fixe_element.Nred;
    M_red.n = MSph.fixe_element.Nred;
    
    M_red.value = G;
    
    
    u_red.n = MSph.fixe_element.Nred;
    u_red.value = v;
    
#pragma omp for schedule(runtime)
    for(e = debut; e < fin; e++){
      
      l = e - debut;

      initialize_tab(taille_red, M_red.value);
      initialize_tab(MSph.fixe_element.Nred, u_red.value);
      
      
      M_red.m = MSph.fixe_element.Nred;
      M_red.n = MSph.fixe_element.Nred;
      
      u_red.n = MSph.fixe_element.Nred;
    
      //on clône l'emplacement U correspondant à l'élément e
      get_F_elem(MSph.ndof, l, &B, U);  
      
      MSph.elements[e].Nred_Pro = MSph.fixe_element.Nred;
      
      MSph.elements[e].Nres_Pro = MSph.fixe_element.Nres;
      
      //cout<<" Calcule de la matrice réduite    
      compute_A_red_ColMajor(e, l, M_glob, M_red.value, MSph, MP, P_red, adjtP);
      
      //calcul de u_red = conj(trans(PP_red)) * B    
      compute_F_red_ColMajor(e, B, u_red.value, MSph, adjtP);
   
      //résolution du système linéaire par la méthode de CHOLESKY : "<<endl;
      //La Matrice et le vecteur seront écrasés
      M_red.Solve_by_CHOLESKY_ColMajor(u_red);           
  
       //cout<<" On retrouve la solution dans l'ancienne base "<<endl;
      reverseTo_originalVector_ColMajor(e, B, u_red.value, MSph, P_red);
      }
    cout<<"Libération finale de la mémoire :"<<endl; 
    delete [] M_red.value; M_red.value =  nullptr;
    delete [] v; v =  nullptr;
    delete [] MP ; MP =  nullptr;    
  }
  delete [] adjtP ; adjtP =  nullptr;
}


void TEST_the_distributed_projection(char *argv[]){
  
  double divide = 1.0 / 1000.0;
  MKL_INT np, N_elements, ndof, n_elem_loc, n_glob;

  double *points = nullptr;
  MKL_INT *tetra = nullptr, *voisin = nullptr;
  double *Dir = nullptr;
  double *X1d = nullptr, *W1d = nullptr;
  double *X2d = nullptr, *W2d = nullptr;
  double *X3d = nullptr, *W3d = nullptr;
  double *third_coord = nullptr;
  MKL_INT *new2old = nullptr, *old2new = nullptr;
  MKL_INT *NewListeVoisin = nullptr, *newloc2glob = nullptr;
  Sommet *sommets = nullptr;
  Element *elements = nullptr;
  Face *face_double = nullptr;
  Face *face = nullptr;
  MKL_INT *Tri = nullptr;
  MKL_INT *cumul_NRED = nullptr;
  double *virtual_points = nullptr;
  Sommet *virtual_sommets = nullptr;
  Onde_plane *virtual_plane_wave = nullptr;

  int* counts_elem = nullptr;
  int* displs_elem = nullptr;

  int* counts_n = nullptr;
  int* displs_n = nullptr;

  MKL_INT n_b = 0, n_P = 0, size_A_red = 0, n_Q = 0, size_Z_red = 0;
  MKL_INT n_red = 0, n_red_loc = 0, e, nnz_loc = 0, nnz_glob=0;

  cplx *Q_full = nullptr;
  A_Block *array_Q_loc = nullptr;
  A_skyline Q_glob;

  cplx *P_full = nullptr;
  A_Block *array_P_loc = nullptr;
  A_skyline P_glob;

  MKL_INT *local_cumul_Nred = nullptr;
  int *somme_Nred_local = nullptr;

  cplx *lambda = nullptr;
  VecN LAMBDA;

  cplx *sigma = nullptr;
  VecN SIGMA;

  cplx *tab_A_red = nullptr;
  A_Block *array_A_red = nullptr;
  A_skylineRed A_red;

  MKL_INT *ia = nullptr, *ja = nullptr;
  cplx *a = nullptr;

  cplx *b = nullptr;
  cplx *b_red = nullptr;

  cplx *x_red = nullptr;

  MKL_INT imin, imax;

  
  cplx *tab_Z_red = nullptr;
  A_Block *array_Z_red = nullptr;
  A_skylineRed Z_red;

  MKL_INT *IA = nullptr, *JA = nullptr;
  cplx *A = nullptr;

  cplx *B = nullptr;
  cplx *B_red = nullptr;

  cplx *X = nullptr, *X_red = nullptr;
  
  VecN vectB, vectB_red, vectb_red, vectb;

  get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);

  int rank ;
  int nb_procs ;

  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &nb_procs );

  MKL_INT N_Face_double = N_elements * 4;

 
  auto t1 = std::chrono::high_resolution_clock::now();
  
  double epsilon = strtod(argv[8], nullptr);
  double epsilon_reg_tikhonov = strtod(argv[9], nullptr);
  MKL_INT filtre = strtod(argv[10], nullptr);
  
  cout<<" valeur du filtre constant epsilon = "<<epsilon<<" type de filtre = "<<filtre<<endl;
  
 
  
  points = new double[3 * np];
  
  tetra = new MKL_INT[4 * N_elements];
  
  voisin = new MKL_INT[4 * N_elements];
  
  build_mesh(argv[1], argv[2], argv[3], points, tetra, voisin);
  
 
  Dir = new double[3 * ndof];
  
  build_direction_of_space_phases(argv[5], argv[6], ndof, Dir);
  
  MKL_INT n_gL = 12;
 
  X1d = new double[n_gL];
  W1d = new double[n_gL];
  quad_Gauss_1D(n_gL, X1d, W1d);
  
  MKL_INT n_quad2D = n_gL * n_gL;
  X2d = new double[2 * n_quad2D];
  W2d = new double[n_quad2D];
  
  MKL_INT n_quad3D = n_gL * n_gL * n_gL;
  X3d = new double[3 * n_quad3D];
  W3d = new double[n_quad3D];
  
  third_coord = new double[N_elements];
  
  new2old = new MKL_INT[N_elements];
  old2new = new MKL_INT[N_elements];
  
  NewListeVoisin = new MKL_INT[N_elements * 4];
  
  newloc2glob = new MKL_INT[4 * N_elements];
  
  sommets = new Sommet[np];
  
  elements = new Element[N_elements];
 
  face_double = new Face[N_Face_double];
  
  face = new Face[N_Face_double];
  
  Tri = new MKL_INT[N_Face_double];
  
  for(MKL_INT e = 0; e < N_elements; e++) {
    elements[e].OP = new Onde_plane[ndof];
  }
  
  cumul_NRED = new MKL_INT[N_elements];
  
 
  virtual_points = new double[12];
  
 
  virtual_sommets = new Sommet[4];
  
  virtual_plane_wave = new Onde_plane[ndof];
  
  MailleSphere MSph(epsilon, epsilon_reg_tikhonov, np, points, N_elements, tetra, voisin, sommets, ndof, Dir, n_gL, X1d, W1d, X2d, W2d, X3d, W3d);
  MSph.build_elements_and_faces(argv[4], argv[7], filtre, third_coord, new2old, old2new, NewListeVoisin, newloc2glob, elements, face_double, Tri, face, cumul_NRED, virtual_points, virtual_sommets, virtual_plane_wave, argv[15]);
   
  delete [] old2new; old2new =  nullptr;
  delete [] new2old; new2old =  nullptr;
  delete [] NewListeVoisin; NewListeVoisin =  nullptr;
  delete [] newloc2glob; newloc2glob =  nullptr;
  delete [] voisin; voisin =  nullptr;
  delete [] tetra; tetra =  nullptr;
  delete [] Tri; Tri =  nullptr;
  delete [] third_coord; third_coord =  nullptr;
  delete [] Dir; Dir =  nullptr;
  auto t2 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_t_MSph = t2 - t1;
  
  if (rank == 0){
  cout<<" temps de creation du maillage : "<<float_t_MSph.count() * divide<<" secs"<<endl;
  
  cout<<"************************************************ Eps_max_filtre = "<<epsilon<<" Eps_max_Tikhonov = "<<epsilon_reg_tikhonov<<endl;
  }

  counts_elem = new int[nb_procs];
  displs_elem = new int[nb_procs];


  Distribute_mesh_to_processes(nb_procs, rank, N_elements, &n_elem_loc, counts_elem, displs_elem);
 
  n_glob = N_elements * ndof;

  n_P = counts_elem[rank] * ndof * ndof;
  P_full = new cplx[n_P];
  array_P_loc = new A_Block[counts_elem[rank]];
 
  P_glob.set_n_elem(counts_elem[rank]); // fixe le nombre d'elements 
  P_glob.set_n_inter(1); // fixe le nombre d'interactions 
  P_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  P_glob.set_n_A(); // calcule le nombre de coeff a stocker
  P_glob.build_mat(P_full, array_P_loc); // Construit les blocks et la structure en mémoire continue

  Q_full = new cplx[n_P];
  array_Q_loc = new A_Block[counts_elem[rank]];
 
  Q_glob.set_n_elem(counts_elem[rank]); // fixe le nombre d'elements 
  Q_glob.set_n_inter(1); // fixe le nombre d'interactions 
  Q_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  Q_glob.set_n_A(); // calcule le nombre de coeff a stocker
  Q_glob.build_mat(Q_full, array_Q_loc); // Construit les blocks et la structure en mémoire continue

  
  double z1 = MPI_Wtime();
  BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(rank, counts_elem, displs_elem, P_glob, MSph, "double");
  double z2 = MPI_Wtime();
  if (rank == 0){
    cout<<" temps construction matrice produit scalaire pondere = "<<z2-z1<<" secs"<<endl;
  }
  
  cblas_zcopy(n_P, P_full, 1, Q_full, 1);
  
  local_cumul_Nred = new MKL_INT[counts_elem[rank]];
  
  for(e = 0; e < counts_elem[rank]; e++){
    local_cumul_Nred[e] = 0;
  }
  
  
   //cout<<" definition des dimensions des bases reduites "<<endl;
   define_and_collecte_dimension_of_new_trefftz_basis(rank, counts_elem, displs_elem, local_cumul_Nred, P_glob, &n_red_loc, &n_red, MSph);
   
   cout<<" rank = "<<rank<<" n_red_loc = "<<n_red_loc<<" n_red = "<<n_red<<endl;
   //cout<<" allocation des tableaux où stocker les dimensions "<<endl;
  
   lambda = new cplx[n_red_loc];
   LAMBDA.n = n_red_loc; LAMBDA.value = lambda;
   
   sigma = new cplx[n_red];
   SIGMA.n = n_red; SIGMA.value = sigma;

   //cout<<" decomposition et normalisation des blocs "<<endl;
   DecompositionGlobaleDesBlocs_ET_Normalisation(rank, counts_elem, displs_elem, local_cumul_Nred, P_glob, LAMBDA, MSph);

   MPI_Barrier(MPI_COMM_WORLD);

  size_A_red = compute_sizeForA_red(rank, counts_elem, displs_elem, MSph);

  size_Z_red = compute_sizeForA_red(MSph);


  //collecte_diagA_glob_with_MPI_routine(rank, nb_procs, n_P, P_full, Q_full);

   
  collecte_Vector_with_MPI_routine(rank, nb_procs, n_red_loc, lambda, sigma);
  
  cout<<" rank = "<<rank<<" size_A_red = "<<size_A_red<<" size_Z_red = "<<size_Z_red<<endl;
 
  double t9 = MPI_Wtime();
   compute_max_and_min_of_all_blocks_eigenvalues(SIGMA, MSph);
   
   double Cond = MSph.lambda_max_normalisee / (double) MSph.lambda_min_normalisee;
   
   cout<<"lambda_max_normalisee = "<<MSph.lambda_max_normalisee<<" lambda_min_normalisee = "<<MSph.lambda_min_normalisee<<" Cond = "<<Cond<<endl;
   cout<<"Nred_mean = "<<MSph.Nred_mean<<endl;
   double t10 = MPI_Wtime();
   
   if (rank == 0){
     cout<<" temps de calcul du max et du min de toutes les valeurs propres: "<<t10-t9<<" secs "<<endl;
   }
   
   delete [] lambda; lambda = nullptr;
  
  MPI_Barrier(MPI_COMM_WORLD);

  n_b = counts_elem[rank] * ndof;
  b = new cplx[n_b];

  for(e = 0; e < n_b; e++){
    b[e] = cplx(0.0, 0.0);
  }

  BuildVector_Of_Volume_Projection_ponderee_parallel_version(rank, counts_elem, displs_elem, b, MSph);

  n_glob = N_elements * ndof;

  MPI_Barrier(MPI_COMM_WORLD);
 

  double start_time = MPI_Wtime();
  collection_Solutions_locales_parallel_version(rank, counts_elem, displs_elem, b, Q_glob, P_glob, MSph) ;
  double end_time = MPI_Wtime();

  B = new cplx[n_glob];
  
  for(e = 0; e < n_glob; e++) {
    B[e] = cplx(0.0, 0.0);
  }
  
  collecte_Vector_with_MPI_routine(rank, nb_procs, n_b, b, B);
  
  if (rank == 0){
    
    cout<<" temps de resolution par LU distribuee "<<end_time - start_time<< " secs "<<endl;

    VecN vectB;
    vectB.n = n_glob;
    vectB.value = B;

    transform_hetero_ScalarAndVelocityError_to_VTK(vectB, MSph);
    //MSph.transform_hetero_analytique_ProjectionOfPressureAndVelocitySolution_to_VTK(vectB);
    cout<<"Liberation de la mémoire de U_sky "<<endl;
    
  }
  
 
  delete [] B; B = nullptr;
   
  delete [] b; b = nullptr;
  
  delete [] P_full; P_full = nullptr;
  delete [] array_P_loc; array_P_loc = nullptr;

  delete [] Q_full; Q_full = nullptr;
  delete [] array_Q_loc; array_Q_loc = nullptr;

  delete [] lambda; lambda = nullptr;
  delete [] sigma; sigma = nullptr;
  
  delete [] local_cumul_Nred; local_cumul_Nred = nullptr;
 
  delete [] points; points =  nullptr;
  delete [] X1d; X1d =  nullptr;
  delete [] W1d; W1d =  nullptr;
  delete [] X2d; X2d =  nullptr;
  delete [] W2d; W2d =  nullptr;
  delete [] X3d; X3d =  nullptr;
  delete [] W3d; W3d =  nullptr;
  delete [] cumul_NRED; cumul_NRED =  nullptr;
  for(MKL_INT e = 0; e < N_elements; e++) {
    delete [] elements[e].OP; elements[e].OP =  nullptr;
    for(MKL_INT j = 0; j < 4; j++){
      elements[e].noeud[j] = nullptr;
      for(MKL_INT l = 0; l < 3; l++) {
	elements[e].face[j].noeud[l] = nullptr;
      }
      
    }
  }
  
  for(MKL_INT i = 0; i < np; i++) {
    sommets[i].set_X(nullptr);
  }
  
  for(MKL_INT i = 0; i < N_Face_double; i++) {
    for(MKL_INT l = 0; l < 3; l++) {
      face_double[i].noeud[l] = nullptr;
      face[i].noeud[l] = nullptr; 
    }
  }
  delete [] sommets; sommets = nullptr;
  delete [] face_double; face_double = nullptr;
  delete [] face; face = nullptr;
  delete [] elements; elements =  nullptr;
  delete [] virtual_points; virtual_points =  nullptr;
  for(MKL_INT l = 0; l < 4; l++) {
    virtual_sommets[l].set_X(nullptr);
  }
  delete [] virtual_sommets; virtual_sommets =  nullptr;
  delete [] virtual_plane_wave; virtual_plane_wave =  nullptr;
}
/*==========================================================================================================================*/

void TEST_the_distributed_projection_regulated_with_Virtual_Element(char *argv[]){
  
  double divide = 1.0 / 1000.0;
  MKL_INT np, N_elements, ndof, n_elem_loc, n_glob;

  double *points = nullptr;
  MKL_INT *tetra = nullptr, *voisin = nullptr;
  double *Dir = nullptr;
  double *X1d = nullptr, *W1d = nullptr;
  double *X2d = nullptr, *W2d = nullptr;
  double *X3d = nullptr, *W3d = nullptr;
  double *third_coord = nullptr;
  MKL_INT *new2old = nullptr, *old2new = nullptr;
  MKL_INT *NewListeVoisin = nullptr, *newloc2glob = nullptr;
  Sommet *sommets = nullptr;
  Element *elements = nullptr;
  Face *face_double = nullptr;
  Face *face = nullptr;
  MKL_INT *Tri = nullptr;
  MKL_INT *cumul_NRED = nullptr;
  double *virtual_points = nullptr;
  Sommet *virtual_sommets = nullptr;
  Onde_plane *virtual_plane_wave = nullptr;

  int* counts_elem = nullptr;
  int* displs_elem = nullptr;

  int* counts_n = nullptr;
  int* displs_n = nullptr;

  MKL_INT n_b = 0, n_P = 0, size_A_red = 0, n_Q = 0, size_Z_red = 0;
  MKL_INT n_red = 0, n_red_loc = 0, e, nnz_loc = 0, nnz_glob=0;

  cplx *P_full = nullptr;
  A_Block *array_P_loc = nullptr;
  A_skyline P_glob;

  MKL_INT *local_cumul_Nred = nullptr;
  int *somme_Nred_local = nullptr;

   cplx *P = nullptr,*AA = nullptr, *M = nullptr;
  double *lambda = nullptr, *w = nullptr;

  cplx *tab_A_red = nullptr;
  A_Block *array_A_red = nullptr;
  A_skylineRed A_red;

  MKL_INT *ia = nullptr, *ja = nullptr;
  cplx *a = nullptr;

  cplx *b = nullptr;
  cplx *b_red = nullptr;

  cplx *x_red = nullptr;

  MKL_INT imin, imax;

  
  cplx *tab_Z_red = nullptr;
  A_Block *array_Z_red = nullptr;
  A_skylineRed Z_red;

  MKL_INT *IA = nullptr, *JA = nullptr;
  cplx *A = nullptr;

  cplx *B = nullptr;
  cplx *B_red = nullptr;

  cplx *X = nullptr, *X_red = nullptr;
  
  VecN vectB, vectB_red, vectb_red, vectb;

  get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);

  int rank ;
  int nb_procs ;

  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &nb_procs );

  MKL_INT N_Face_double = N_elements * 4;

 
  auto t1 = std::chrono::high_resolution_clock::now();
  
  double epsilon = strtod(argv[8], nullptr);
  double epsilon_reg_tikhonov = strtod(argv[9], nullptr);
  MKL_INT filtre = strtod(argv[10], nullptr);
  
  cout<<" valeur du filtre constant epsilon = "<<epsilon<<" type de filtre = "<<filtre<<endl;
  
 
  
  points = new double[3 * np];
  
  tetra = new MKL_INT[4 * N_elements];
  
  voisin = new MKL_INT[4 * N_elements];
  
  build_mesh(argv[1], argv[2], argv[3], points, tetra, voisin);
  
 
  Dir = new double[3 * ndof];
  
  build_direction_of_space_phases(argv[5], argv[6], ndof, Dir);
  
  MKL_INT n_gL = 12;
 
  X1d = new double[n_gL];
  W1d = new double[n_gL];
  quad_Gauss_1D(n_gL, X1d, W1d);
  
  MKL_INT n_quad2D = n_gL * n_gL;
  X2d = new double[2 * n_quad2D];
  W2d = new double[n_quad2D];
  
  MKL_INT n_quad3D = n_gL * n_gL * n_gL;
  X3d = new double[3 * n_quad3D];
  W3d = new double[n_quad3D];
  
  third_coord = new double[N_elements];
  
  new2old = new MKL_INT[N_elements];
  old2new = new MKL_INT[N_elements];
  
  NewListeVoisin = new MKL_INT[N_elements * 4];
  
  newloc2glob = new MKL_INT[4 * N_elements];
  
  sommets = new Sommet[np];
  
  elements = new Element[N_elements];
 
  face_double = new Face[N_Face_double];
  
  face = new Face[N_Face_double];
  
  Tri = new MKL_INT[N_Face_double];
  
  for(MKL_INT e = 0; e < N_elements; e++) {
    elements[e].OP = new Onde_plane[ndof];
  }
  
  cumul_NRED = new MKL_INT[N_elements];
  
 
  virtual_points = new double[12];
  
 
  virtual_sommets = new Sommet[4];
  
  virtual_plane_wave = new Onde_plane[ndof];
  
  MailleSphere MSph(epsilon, epsilon_reg_tikhonov, np, points, N_elements, tetra, voisin, sommets, ndof, Dir, n_gL, X1d, W1d, X2d, W2d, X3d, W3d);
  MSph.build_elements_and_faces(argv[4], argv[7], filtre, third_coord, new2old, old2new, NewListeVoisin, newloc2glob, elements, face_double, Tri, face, cumul_NRED, virtual_points, virtual_sommets, virtual_plane_wave, argv[15]);
   
  delete [] old2new; old2new =  nullptr;
  delete [] new2old; new2old =  nullptr;
  delete [] NewListeVoisin; NewListeVoisin =  nullptr;
  delete [] newloc2glob; newloc2glob =  nullptr;
  delete [] voisin; voisin =  nullptr;
  delete [] tetra; tetra =  nullptr;
  delete [] Tri; Tri =  nullptr;
  delete [] third_coord; third_coord =  nullptr;
  delete [] Dir; Dir =  nullptr;
  auto t2 = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double, std::milli> float_t_MSph = t2 - t1;
  
  if (rank == 0){
  cout<<" temps de creation du maillage : "<<float_t_MSph.count() * divide<<" secs"<<endl;
  
  cout<<"************************************************ Eps_max_filtre = "<<epsilon<<" Eps_max_Tikhonov = "<<epsilon_reg_tikhonov<<endl;
  }

  counts_elem = new int[nb_procs];
  displs_elem = new int[nb_procs];


  Distribute_mesh_to_processes(nb_procs, rank, N_elements, &n_elem_loc, counts_elem, displs_elem);
 
  n_glob = N_elements * ndof;

  n_P = counts_elem[rank] * ndof * ndof;
  P_full = new cplx[n_P];
  array_P_loc = new A_Block[counts_elem[rank]];
 
  P_glob.set_n_elem(counts_elem[rank]); // fixe le nombre d'elements 
  P_glob.set_n_inter(1); // fixe le nombre d'interactions 
  P_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  P_glob.set_n_A(); // calcule le nombre de coeff a stocker
  P_glob.build_mat(P_full, array_P_loc); // Construit les blocks et la structure en mémoire continue

  M = new cplx[ndof * ndof];
  P = new cplx[ndof * ndof];
  lambda = new double[ndof];
  
  AA = new cplx[ndof * ndof];
  w = new double[ndof];
  
  double z1 = MPI_Wtime();
  BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(rank, counts_elem, displs_elem, P_glob, MSph, "double");
  double z2 = MPI_Wtime();
  if (rank == 0){
    cout<<" temps construction matrice produit scalaire pondere = "<<z2-z1<<" secs"<<endl;
  }
  
  if (filtre == 0){
    
    double t5 = MPI_Wtime();
    
    buildArealMetrique(MSph.ndof, M, MSph.fixe_element, "double");
    
    double t6 = MPI_Wtime();
    
    if (rank == 0){
      cout<<" temps de construction de la matrice surfacique de l'element virtuel: "<<t6-t5<<" secs"<<endl;
    }
  }
  else{
    double t5 = MPI_Wtime();
    
    buildVolumeMetrique_ponderee(MSph.ndof, M, MSph, "quad");
    
    double t6 = MPI_Wtime();
    
    if (rank == 0){
      cout<<" temps de construction de la matrice M associee au metrique volumique de l'element virtuel: "<<t6-t5<<" secs"<<endl;
    }
  }
  
  
  local_cumul_Nred = new MKL_INT[counts_elem[rank]];
  
  for(e = 0; e < counts_elem[rank]; e++){
    local_cumul_Nred[e] = 0;
  }
  
  
   //cout<<" definition des dimensions des bases reduites "<<endl;
   define_and_collecte_dimension_of_new_trefftz_basis(rank, counts_elem, displs_elem, local_cumul_Nred, M, &n_red_loc, &n_red, MSph);
   
   cout<<" rank = "<<rank<<" n_red_loc = "<<n_red_loc<<" n_red = "<<n_red<<endl;
   //cout<<" allocation des tableaux où stocker les dimensions "<<endl;
  
   //cout<<" decomposition et normalisation des blocs "<<endl;
   DecompositionDeBloc_ET_Normalisation_TSVD_version(ndof, M, P, lambda, MSph.fixe_element, AA, w) ;
   MPI_Barrier(MPI_COMM_WORLD);

   double Cond = lambda[MSph.fixe_element.Nred-1] / (double) lambda[0];
   
   MSph.lambda_max_normalisee = lambda[MSph.fixe_element.Nred-1] / (double) MSph.fixe_element.facto_volume;
   
   MSph.lambda_min_normalisee = lambda[0] / (double) MSph.fixe_element.facto_volume;
   
   if (rank == 0){
   
     cout<<" max et min des valeurs propres non reduites et non normalisées "<<endl;
     cout<<" max = "<<w[MSph.ndof-1] <<" min = "<<w[0]<<endl;  
     
     cout<<" max et min des valeurs propres reduites et normalisées"<<endl;
     cout<<" max = "<<MSph.lambda_max_normalisee<<" min = "<<MSph.lambda_min_normalisee<<" Cond = "<<Cond<<" Nred = "<<MSph.fixe_element.Nred<<endl;
   }
   
   
   delete [] lambda; lambda = nullptr;
   MPI_Barrier(MPI_COMM_WORLD);
   
   n_b = counts_elem[rank] * ndof;
   b = new cplx[n_b];
   
   for(e = 0; e < n_b; e++){
     b[e] = cplx(0.0, 0.0);
   }

  BuildVector_Of_Volume_Projection_ponderee_parallel_version(rank, counts_elem, displs_elem, b, MSph);

  n_glob = N_elements * ndof;

  MPI_Barrier(MPI_COMM_WORLD);
 

  double start_time = MPI_Wtime();
  collection_Solutions_locales_parallel_version(rank, counts_elem, displs_elem, b, P_glob, P, MSph) ;
  double end_time = MPI_Wtime();

  B = new cplx[n_glob];
  
  for(e = 0; e < n_glob; e++) {
    B[e] = cplx(0.0, 0.0);
  }
  
  collecte_Vector_with_MPI_routine(rank, nb_procs, n_b, b, B);
  
  if (rank == 0){
    
    cout<<" temps de resolution par LU distribuee "<<end_time - start_time<< " secs "<<endl;

    VecN vectB;
    vectB.n = n_glob;
    vectB.value = B;

    transform_hetero_ScalarAndVelocityError_to_VTK(vectB, MSph);
    //MSph.transform_hetero_analytique_ProjectionOfPressureAndVelocitySolution_to_VTK(vectB);
    cout<<"Liberation de la mémoire de U_sky "<<endl;
    
  }
  
 
  delete [] B; B = nullptr;
   
  delete [] b; b = nullptr;
  
  delete [] P_full; P_full = nullptr;
  delete [] array_P_loc; array_P_loc = nullptr;

  delete [] lambda; lambda = nullptr;
  
  delete [] local_cumul_Nred; local_cumul_Nred = nullptr;
 
  delete [] P; P = nullptr;
  delete [] w; w = nullptr;
  delete [] M; M = nullptr;
  delete [] points; points =  nullptr;
  delete [] X1d; X1d =  nullptr;
  delete [] W1d; W1d =  nullptr;
  delete [] X2d; X2d =  nullptr;
  delete [] W2d; W2d =  nullptr;
  delete [] X3d; X3d =  nullptr;
  delete [] W3d; W3d =  nullptr;
  delete [] cumul_NRED; cumul_NRED =  nullptr;
  for(MKL_INT e = 0; e < N_elements; e++) {
    delete [] elements[e].OP; elements[e].OP =  nullptr;
    for(MKL_INT j = 0; j < 4; j++){
      elements[e].noeud[j] = nullptr;
      for(MKL_INT l = 0; l < 3; l++) {
	elements[e].face[j].noeud[l] = nullptr;
      }
      
    }
  }
  
  for(MKL_INT i = 0; i < np; i++) {
    sommets[i].set_X(nullptr);
  }
  
  for(MKL_INT i = 0; i < N_Face_double; i++) {
    for(MKL_INT l = 0; l < 3; l++) {
      face_double[i].noeud[l] = nullptr;
      face[i].noeud[l] = nullptr; 
    }
  }
  delete [] sommets; sommets = nullptr;
  delete [] face_double; face_double = nullptr;
  delete [] face; face = nullptr;
  delete [] elements; elements =  nullptr;
  delete [] virtual_points; virtual_points =  nullptr;
  for(MKL_INT l = 0; l < 4; l++) {
    virtual_sommets[l].set_X(nullptr);
  }
  delete [] virtual_sommets; virtual_sommets =  nullptr;
  delete [] virtual_plane_wave; virtual_plane_wave =  nullptr;
}


void compute_pressure_and_velocity_norms(int rank, int nb_procs, int *counts_elem, int *displs_elem, cplx *U, cplx *P, const char path_to_repertory[], const char method[], MKL_INT filtre, MailleSphere &MSph){

  MKL_INT n = MSph.get_N_elements() * MSph.ndof;
  
  

  MKL_INT n_b = 0, n_P = 0, N_elements, ndof;
  MKL_INT n_red = 0, n_red_loc = 0, e, n_A;
  N_elements = MSph.get_N_elements();
  ndof = MSph.ndof;
  /*
  cplx *P_full = nullptr;
  A_Block *array_P_loc = nullptr;
  A_skyline P_glob;

   cplx *A_full = nullptr;
  A_Block *array_A_loc = nullptr;
  A_skyline A_glob;

  cplx *b = nullptr;

  cplx *PhU = nullptr;

 
  
  n_P = counts_elem[rank] * ndof * ndof;
  P_full = new cplx[n_P];
  array_P_loc = new A_Block[counts_elem[rank]];
 
  P_glob.set_n_elem(counts_elem[rank]); // fixe le nombre d'elements 
  P_glob.set_n_inter(1); // fixe le nombre d'interactions 
  P_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  P_glob.set_n_A(); // calcule le nombre de coeff a stocker
  P_glob.build_mat(P_full, array_P_loc); // Construit les blocks et la structure en mémoire continue
 
  double z1 = MPI_Wtime();
  // BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(rank, counts_elem, displs_elem, P_glob, MSph, "double");
  BuildVolumeMetricMatrix_ponderee_parallel_version(rank, counts_elem, displs_elem, P_glob, MSph, "double");
  double z2 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps construction matrice produit scalaire pondere = "<<z2-z1<<" secs"<<endl;
  }
  
  
  n_b = counts_elem[rank] * ndof;
  b = new cplx[n_b];
  
  for(e = 0; e < n_b; e++){
    b[e] = cplx(0.0, 0.0);
  }
  
  double z3 = MPI_Wtime();
  BuildVector_Of_Volume_Projection_ponderee_parallel_version(rank, counts_elem, displs_elem, b, MSph);
  double z4 = MPI_Wtime();
  if (rank == 0){
    cout<<" temps construction du vecteur de la projection pondere = "<<z4-z3<<" secs"<<endl;
  }
  
  
  double z5 = MPI_Wtime();
  collection_Solutions_locales_parallel_version(rank, counts_elem, displs_elem, b, P_glob, P, MSph) ;
  
  PhU = new cplx[n];
  
  for(e = 0; e < n; e++) {
    PhU[e] = cplx(0.0, 0.0);
  }

  double z6 = MPI_Wtime();

  MPI_Barrier(MPI_COMM_WORLD);
  collecte_Vector_with_MPI_routine(rank, nb_procs, n_b, b, PhU);

  if (rank == 0){
    
    cout<<" temps de projection de la solution exacte "<<z6 - z5<< " secs "<<endl;
  }

  delete [] P_full; P_full = nullptr;
  delete [] array_P_loc; array_P_loc = nullptr;

  double a3 = MPI_Wtime();
  n = N_elements * ndof;
  
  n_A = counts_elem[rank] * ndof * ndof * 5;
  A_full = new cplx[n_A];
  array_A_loc = new A_Block[counts_elem[rank] * 5];
 
  A_glob.set_n_elem(counts_elem[rank]); // fixe le nombre d'elements 
  A_glob.set_n_inter(5); // fixe le nombre d'interactions 
  A_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
  A_glob.set_n_A(); // calcule le nombre de coeff a stocker
  A_glob.build_mat(A_full, array_A_loc); // Construit les blocks et la structure en mémoire continue

  ASSEMBLAGE_DE_Askyline_parallel_version(rank, counts_elem, displs_elem, A_glob, MSph, "double");

  double a4 = MPI_Wtime();
 
  if (rank == 0){
    cout<<" temps d'assemblage et d'allocation de la matrice A_glob "<<a4-a3<<" secs "<<endl;
  }
*/
  double L2P_ex, L2V_ex, L2P_err, L2V_err, HV_err, HV_ex, DG_ex, DG_err;
  L2P_ex = 0.0; L2V_ex = 0.0;
  L2P_err = 0.0; L2V_err = 0.0;
  HV_err = 0.0; HV_ex = 0.0;
  DG_ex = 0.0; DG_err = 0.0;

  cplx H1S_err_int = cplx(0.0, 0.0);
  double H1S_err_ext = 0.0;
  
  double t1 = MPI_Wtime();
  double local_L2P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t2 = MPI_Wtime();
  if (rank == 0){
    cout<<" temps de calcul de ||P_ex||_{L^2} : "<<t2-t1<<" secs"<<endl;
  }
  
  double t3 = MPI_Wtime();
  double local_L2V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t4 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||V_ex||_{L^2} : "<<t4-t3<<" secs"<<endl;
  }
  double t5 = MPI_Wtime();
  double local_L2P_err =  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t6 = MPI_Wtime();
  
  if (rank == 0){
    
    cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<t6-t5<<" secs"<<endl;
  }
  
  double t7 = MPI_Wtime();
  double local_L2V_err = norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t8 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<t8-t7<<" secs"<<endl;
  }
  double t9 = MPI_Wtime();
  double local_HV_ex = norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, MSph);
  double t10 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex||_{H^1} volumique: "<<t10-t9<<" secs"<<endl;
  }
  double t11 = MPI_Wtime();
  double local_HV_err = norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(rank, counts_elem, displs_elem, U, MSph);
  double t12 = MPI_Wtime();
  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} volumique: "<<t12-t11<<" secs"<<endl;
  }
  
  double t13 = MPI_Wtime();
  //double local_DG_ex = normeDG(rank, counts_elem, displs_elem, A_glob, PhU, MSph);
  double local_DG_ex = 1.0;
  /*
  //PhU = PhU - U
  cplx alpha = cplx(-1.0, 0.0);
  int incX = 1, incY = 1;

  cblas_zaxpy(n, &alpha, U, incX, PhU, incY);
  
  double local_DG_err = normeDG(rank, counts_elem, displs_elem, A_glob, PhU, MSph) ;
*/
  double local_DG_err=1.0;
  double t14 = MPI_Wtime();


  
  if (rank == 0){
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} modifiée: "<<t14-t13<<" secs"<<endl;
  }

  MPI_Datatype resized_Complex = create_mpi_complex_type();
  
  MPI_Allreduce(&local_L2P_ex ,&L2P_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2V_ex ,&L2V_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_HV_ex ,&HV_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_DG_ex ,&DG_ex,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2P_err ,&L2P_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_L2V_err ,&L2V_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_HV_err ,&HV_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Allreduce(&local_DG_err ,&DG_err,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  
  MPI_Barrier(MPI_COMM_WORLD);
  
  L2P_err = sqrt(L2P_err);
  L2V_err = sqrt(L2V_err);
  
  L2P_ex = sqrt(L2P_ex);
  L2V_ex = sqrt(L2V_ex);
  
  HV_err = sqrt(HV_err);
  HV_ex = sqrt(HV_ex);
  
  DG_ex = sqrt(DG_ex);
  
  DG_err = sqrt(DG_err);
  
  cout<<"norme Volumique "<<endl;
  
  double L2P_err_rela = L2P_err / (double) L2P_ex;
  double L2V_err_rela = L2V_err / (double) L2V_ex;
  double HV_err_rela = HV_err / (double) HV_ex;
  double DG_err_rela = DG_err / (double) DG_ex;
  
  if (rank == 0){
    cout<<" ||P_ex - P_h||_{L2} / ||P_ex[[_{L^2} = "<<L2P_err_rela<<" ||V_ex - V_h||_{L2} / ||V_ex||_{L^2}= "<<L2V_err_rela<<endl;
    
    cout<<" norme H1 volumique ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<HV_err_rela<<endl;
    
    cout<<" ||U_ex - U_h||_{DG}/ ||U_ex||_{DG} = "<<DG_err_rela<<endl;
    
    char *file_absolue = NULL;
    file_absolue = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
    strcpy(file_absolue, path_to_repertory);
    strcat(file_absolue,"_norme_absolue.txt");
      
    char *file_relative = NULL;
    file_relative = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
    strcpy(file_relative, path_to_repertory);
    strcat(file_relative,"_norme_relative.txt");
      
    FILE* fic_absolue;
    
    FILE* fic_relative;
    
    if( !(fic_absolue = fopen(file_absolue,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
    
    if( !(fic_relative = fopen(file_relative,"a")) ) {
      fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
      exit(EXIT_FAILURE);
    }
 
    cout<<"norme Volumique "<<endl;
    
    if (strcmp(method, "cess-des") == 0){
      
      fprintf(fic_absolue,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err, L2V_err, HV_err, DG_err);
      
      fprintf(fic_relative,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err_rela, L2V_err_rela, HV_err_rela, DG_err_rela);
    }
    else{
      
      FILE* fic_Cond;
      char *file_Cond = NULL;
      file_Cond = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
      strcpy(file_Cond, path_to_repertory);
      strcat(file_Cond,"_Cond_Vol.txt");
      
      if( !(fic_Cond = fopen(file_Cond,"a")) ) {
	fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
	    exit(EXIT_FAILURE);
      }
      
      fprintf(fic_absolue,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err, L2V_err, HV_err, DG_err);
      
      fprintf(fic_relative,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err_rela, L2V_err_rela, HV_err_rela, DG_err_rela);
      
      fprintf(fic_Cond,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, MSph.lambda_min, MSph.lambda_max, MSph.lambda_max_normalisee / (double) MSph.lambda_min_normalisee);
      
      
      fclose(fic_Cond); free(file_Cond); file_Cond = NULL;
      
    }
    
    
    fclose(fic_absolue); fclose(fic_relative);
    free(file_relative); file_relative = NULL;
    free(file_absolue); file_absolue = NULL;
  }
  /*
  delete [] A_full; A_full = nullptr;
  delete [] array_A_loc; array_A_loc = nullptr;
  delete [] b; b = nullptr;
  delete [] PhU; PhU = nullptr;*/
}




double normeDG_parallel_version(A_skyline &A_glob, cplx *X, MailleSphere &MSph) {
  //Cette fonction calcule la norme DG associée au vecteur X
  /*------------------------------------------------------------------------- input -----------------------------------------------------------------
    A_glob    : objet de type A_skyline, c'est la matrice du système linéaire
    X         : objet de type VecN, il contient un vecteur
    MSph      : le maillage
    /*----------------------------------------------------------------------- ouput -----------------------------------------------------------------
    s         : sqrt(real(X^* A_glob X)), la norme DG de X
    /*-------------------------------------------------------------- variables intermédiares -------------------------------------------------------
    i         : boucle sur les éléments
    j         : .......... les numéros locals des voisins des des éléments
    iloc      : .......... les fonctions-tests
    jloc      : .......... les fonctions solutions
    AA        : pour clôner la matrice locale sur chaque élément
    x         : pour clôner le vecteur local sur chaque élément
    y         : ................................ le voisin de chaque élément
    I         : X^* A_glob X
    /*----------------------------------------------------------------------------------------------------------------------------------------------*/  
  MKL_INT i, taille = MSph.ndof * MSph.ndof;
  
  double r = 0.0;
  double s;
  
#pragma omp parallel private(i, s)
  {
    s = 0.0;
#pragma omp for schedule(runtime)
    for (i = 0; i < MSph.get_N_elements(); i++) {
      cplx I = cplx(0.0, 0.0);
      MKL_INT iloc, jloc;
      
      cplx *bloc_diag = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + 0].get_Aloc();//bloc associé à l'élément i
      cplx *x;
      get_F_elem(MSph.ndof, i, &x, X);//clônage de X sur l'élément i
      
      for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
	for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
	  
	  I += conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
      
      
      for (int j = 0; j < 4; j++) {
	
	if (MSph.elements[i].voisin[j] >= 0) {
	  
	  cplx *y;
	  
	  cplx *bloc_extra = A_glob.Tab_A_loc[i * A_glob.get_n_inter() + (j+1)].get_Aloc();//bloc associé au voisin j de l'élément i
	  
	  get_F_elem(MSph.ndof, MSph.elements[i].voisin[j], &y, X);//clônage de X sur le voisin j de l'élément i
	  
	  for (iloc = 0; iloc < MSph.ndof; iloc++) { // numero de la ligne
	    for (jloc = 0; jloc < MSph.ndof; jloc++) { // numero de la colonne
	      
	      I += conj(x[iloc]) * bloc_extra[jloc * MSph.ndof + iloc] * y[jloc];
	    }
	  }
	}
      }
      s += real(I);
    }//fin boucle sur les éléments
    
#pragma omp atomic
    r += s;
    
  }//Fin de la région parallele
  
  return sqrt(r);
  }
    


double normeDG_en_Desamblee_parallel_version(cplx *X, MailleSphere &MSph, const char precision[]) {
  //Cette fonction calcule la norme DG associée au vecteur X
  /*------------------------------------------------------------------------- input -----------------------------------------------------------------
    A_glob    : objet de type A_skyline, c'est la matrice du système linéaire
    X         : objet de type VecN, il contient un vecteur
    MSph      : le maillage
    /*----------------------------------------------------------------------- ouput -----------------------------------------------------------------
    s         : sqrt(real(X^* A_glob X)), la norme DG de X
    /*-------------------------------------------------------------- variables intermédiares -------------------------------------------------------
    i         : boucle sur les éléments
    j         : .......... les numéros locals des voisins des des éléments
    iloc      : .......... les fonctions-tests
    jloc      : .......... les fonctions solutions
    AA        : pour clôner la matrice locale sur chaque élément
    x         : pour clôner le vecteur local sur chaque élément
    y         : ................................ le voisin de chaque élément
    I         : X^* A_glob X
    /*----------------------------------------------------------------------------------------------------------------------------------------------*/  
  MKL_INT e, taille = MSph.ndof * MSph.ndof;
  
  double r = 0.0;
  double s;
  cplx *B1 = nullptr, *B2 = nullptr, *bloc_diag = nullptr, *bloc_extra = nullptr;
#pragma omp parallel private(B1, B2, e, bloc_diag, bloc_extra, s)
  {
    B1 = new cplx[taille];
    B2 = new cplx[taille];
    bloc_diag = new cplx[taille];
    bloc_extra = new cplx[4 * taille];
    
    s = 0.0;
#pragma omp for schedule(runtime)
    for (e = 0; e < MSph.get_N_elements(); e++) {

      cplx I = cplx(0.0, 0.0);

      MKL_INT iloc, jloc;

      initialize_tab(taille, bloc_diag);
      
      initialize_tab(4 * taille, bloc_extra);
      
      for (MKL_INT f = 0; f < 4; f++) {
	
	
	if (MSph.elements[e].voisin[f] >= 0) {//Sur les faces intérieures
	  
	  //stockage de int_{l} P_T P'_T  dans B1 et int_{l} P_K P'_T dans B2
	  Build_interior_planeBloc(e, f, B1, B2, MSph, precision);
	  
	  //--------------------------------------  interaction element-element ------------------------------------------------
	  //ajout du bloc P_T P'_T associé à la face f 
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face
	  Ajouter_BlocIntV_TxV_T(e, f, MSph.elements[e].T_VV[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T P'_T associé à la face
	  Ajouter_BlocIntV_TxP_T(e, f, MSph.elements[e].T_VP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc P_T V'_Tn_T associé à la face
	  Ajouter_BlocIntP_TxV_T(e, f, MSph.elements[e].T_PV[f], B1, bloc_diag, MSph);
	  
	  //--------------------------------------  interaction voisin-element ------------------------------------------------
	  cplx *D = bloc_extra + f * taille;
	  
	  //ajout du bloc P_K P'_T associé à la face
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K V'_Tn_T associé à la face f
	  Ajouter_BlocIntV_KxV_T(e, f, MSph.elements[e].T_VV[f], B2, D, MSph);
	  
	  //ajout du bloc V_Kn_K P'_T associé à la face f
	  Ajouter_BlocIntV_KxP_T(e, f, MSph.elements[e].T_VP[f], B2, D, MSph);
	  
	  //ajout du bloc P_K V'_Tn_T associé à la face
	  Ajouter_BlocIntP_KxV_T(e, f, MSph.elements[e].T_PV[f], B2, D, MSph);

	}//Fin du if
	else {
	  
	  //stockage de int_{l} P_T P'_T  dans B1
	  Build_exterior_planeBloc(e, f, B1, MSph, precision);
	  
	  //ajout du bloc P_T P'_T associé à la face f
	  Ajouter_BlocP_XP_Y(e, f, MSph.elements[e].T_PP[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T V'_Tn_T associé à la face f
	  Ajouter_BlocExtV_TxV_T(e, f, MSph.elements[e].T_VV[f], B1, bloc_diag, MSph);
	  
	  //ajout du bloc V_Tn_T P'_T associé à la face f
	  Ajouter_BlocExtV_TxP_T(e, f, MSph.elements[e].T_VP[f], B1, bloc_diag, MSph);
	  
	//ajout du bloc P_T V'_Tn_T associé à la face f
	  Ajouter_BlocExtP_TxV_T(e, f, MSph.elements[e].T_PV[f], B1, bloc_diag, MSph);
	  
	}//Fin du else
      }//Fin boucle sur les faces
      
      cplx *x;
      get_F_elem(MSph.ndof, e, &x, X);
      
      for(iloc = 0; iloc < MSph.ndof; iloc++){
	for(jloc = 0; jloc < MSph.ndof; jloc++){
	  I +=  conj(x[iloc]) * bloc_diag[jloc * MSph.ndof + iloc] * x[jloc];
	}
      }
      
      for(MKL_INT f = 0; f < 4; f++){
	if (MSph.elements[e].voisin[f] >=0){
	  
	  cplx *bloc_voisin_elem = bloc_extra + f * taille;
	  
	  cplx *y;
	  
	  get_F_elem(MSph.ndof, MSph.elements[e].voisin[f], &y, X);
	  
	  
	  for(iloc = 0; iloc < MSph.ndof; iloc++){
	    for(jloc = 0; jloc < MSph.ndof; jloc++){
	      I +=  conj(x[iloc]) * bloc_voisin_elem[jloc * MSph.ndof + iloc] * y[jloc];
	    }
	  }
	  
	}
      }
      
      s += real(I);    
    }//Fin boucle sur les éléments
    delete [] B1; B1 = nullptr;
    delete [] B2; B2 = nullptr;
    delete [] bloc_diag; bloc_diag = nullptr;
    delete [] bloc_extra; bloc_extra = nullptr;

#pragma omp atomic
    r += s;

  }//Fin de la région parallele

  return sqrt(r);
}

    
    
void compute_pressure_and_velocity_norms(cplx *U, cplx *P, MKL_INT filtre, const char path_to_repertory[], const char method[], MailleSphere &MSph){
   char *file_absolue = NULL;
   file_absolue = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
   strcpy(file_absolue, path_to_repertory);
   strcat(file_absolue,"_norme_absolue.txt");
     
   char *file_relative = NULL;
   file_relative = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
   strcpy(file_relative, path_to_repertory);
   strcat(file_relative,"_norme_relative.txt");
     
   FILE* fic_absolue;
   
   FILE* fic_relative;
   
   if( !(fic_absolue = fopen(file_absolue,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }
   
   if( !(fic_relative = fopen(file_relative,"a")) ) {
     fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
     exit(EXIT_FAILURE);
   }

   MKL_INT n = MSph.get_N_elements() * MSph.ndof;
   MKL_INT n_P, n_A, N_elements, ndof;
   N_elements = MSph.get_N_elements();
   ndof = MSph.ndof;
   /*
   cplx *M_full = nullptr;
   A_Block *array_M_loc = nullptr;

   A_skyline M_glob;
   
   cplx *A_full = nullptr;
   A_Block *array_A_loc = nullptr;
   
   A_skyline A_glob;
   
   MKL_INT n_M = n * ndof;
   M_full = new cplx[n_M];
   array_M_loc = new A_Block[N_elements];
   
   M_glob.set_n_elem(N_elements); // fixe le nombre d'elements 
   M_glob.set_n_inter(1); // fixe le nombre d'interactions 
   M_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
   M_glob.set_n_A(); // calcule le nombre de coeff a stocker
   M_glob.build_mat(M_full, array_M_loc); // Construit les blocks et la structure en mémoire continue

   double z1 = omp_get_wtime();
   //BuildVolumeMetricMatrix_ponderee_parallel_version(M_glob, MSph, "quad");
   BuildVolumeMetricMatrix_ponderee_numerical_integ_parallel_version(M_glob, MSph, "quad");
   double z2 = omp_get_wtime();

   cout<<" temps de construction de la matrice produit scalaire volumique variable "<<z2-z1<<" secs"<<endl;

   cplx *Uex = nullptr;
   Uex = new cplx[n];
   initialize_tab(n, Uex);

   VecN PhUex;
   PhUex.n = n;
   PhUex.value = Uex;
   
   double z3 = omp_get_wtime();
   BuildVector_Of_Volume_Projection_ponderee_parallel_version(PhUex.value, MSph);
   double z4 = omp_get_wtime();

   cout<<" temps de construction du vecteur de la projection "<<z4-z3<<" secs "<<endl;

   double z5 = omp_get_wtime();
   collection_Solutions_locales_parallel_version(PhUex, M_glob, P, MSph); 
   double z6 = omp_get_wtime();

   cout<<" temps de la projection de la solution analytique "<<z6-z5<<" secs "<<endl;

   delete [] M_full; M_full = nullptr;
   delete [] array_M_loc; array_M_loc = nullptr;
   
   n_A =  n_M * 5;
   A_full = new cplx[n_A];
   array_A_loc = new A_Block[N_elements * 5];
   
   A_glob.set_n_elem(N_elements); // fixe le nombre d'elements 
   A_glob.set_n_inter(5); // fixe le nombre d'interactions 
   A_glob.set_n_dof(ndof);  // fixe le nombre d'ondes planes a ndof
   A_glob.set_n_A(); // calcule le nombre de coeff a stocker
   A_glob.build_mat(A_full, array_A_loc); // Construit les blocks et la structure en mémoire continue

   double z7 = omp_get_wtime();
    ASSEMBLAGE_DE_Askyline_parallel_version(A_glob, MSph, "double");
   double z8 = omp_get_wtime();

   cout<<" assemblage de la matrice A_sky de Cessenat-Despres "<<z8-z7<<" secs "<<endl;
   
   */
   double divide = 1.0 /1000;
   
   auto t1 = std::chrono::high_resolution_clock::now();
   double L2P_ex = norme_L2_volumique_pour_Bessel_en_pression_avec_integ_numerique_parallel_version(MSph);
   auto t2 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_L2P_ex = t2 - t1;
   
   cout<<" temps de calcul de ||P_ex||_{L^2} : "<<float_L2P_ex.count() * divide<<" secs"<<endl;
   
   auto t3 = std::chrono::high_resolution_clock::now();
   double L2V_ex = norme_L2_volumique_pour_Bessel_en_velocite_avec_integ_numerique_parallel_version(MSph);
   auto t4 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_L2V_ex = t4 - t3;
   cout<<" temps de calcul de ||V_ex||_{L^2} : "<<float_L2V_ex.count() * divide<<" secs"<<endl;
   
   auto t5 = std::chrono::high_resolution_clock::now();
   double L2P_err =  norme_L2_erreur_volumique_en_pression_avec_integ_numerique_parallel_version(U, MSph);
   auto t6 = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> float_L2P_err = t6 - t5;
   cout<<" temps de calcul de ||P_ex - P_h||_{L^2} : "<<float_L2P_err.count() * divide<<" secs"<<endl;
   
   auto t7 = std::chrono::high_resolution_clock::now();
   double L2V_err = norme_L2_erreur_volumique_en_velocite_avec_integ_numerique_parallel_version(U, MSph);
   auto t8 = std::chrono::high_resolution_clock::now();
     std::chrono::duration<double, std::milli> float_L2V_err = t8 - t7;
     cout<<" temps de calcul de ||V_ex - V_h||_{L^2} : "<<float_L2V_err.count() * divide<<" secs"<<endl;
     auto t9 = std::chrono::high_resolution_clock::now();
    double HV_ex = norme_H1_ponderee_volumique_pour_Bessel_avec_integ_numerique_parallel_version(MSph);
    auto t10 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_HV_ex = t10 - t9;
    cout<<" temps de calcul de ||U_ex||_{H^1} volumique: "<<float_HV_ex.count() * divide<<" secs"<<endl;
    
    auto t11 = std::chrono::high_resolution_clock::now();
    double HV_err = norme_H1_ponderee_erreur_volumique_avec_integ_numerique_parallel_version(U, MSph);
    auto t12 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_HV_err = t12 - t11;
    cout<<" temps de calcul de ||U_ex - U_h||_{H^1} volumique: "<<float_HV_err.count() * divide<<" secs"<<endl;

    auto t13 = std::chrono::high_resolution_clock::now();
    //double DG_ex = normeDG_en_Desamblee_parallel_version(PhUex.value, MSph, "double");
    double DG_ex = 1.0;
    /*
    cout<<" verification de U_h "<<endl;
    //check_vector(U, n);

    
    

    cplx alpha = cplx(-1.0, 0.0);
    int incX = 1, incY = 1;
    
    //PhUex = PhUex + alpha  * U
    cblas_zaxpy(n, &alpha, U, incX, PhUex.value, incY);

    cout<<" verification de U_ex - U_h "<<endl;
    //check_vector(PhUex.value, n) ;
    
    double DG_err = normeDG_en_Desamblee_parallel_version(PhUex.value, MSph, "double");
    */
    double DG_err = 1.0;
    auto t14 = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_Hmodif_err = t14 - t13;
    cout<<" temps de calcul de ||U_ex - U_h||_{DG} : "<<float_Hmodif_err.count() * divide<<" secs"<<endl;
    cout<<" ||U_ex||_{DG} = "<<DG_ex<<" ||U_ex - U_h||_{DG} = "<<DG_err<<endl;
    
    cout<<" L2P_ex = "<<L2P_ex<<" L2V_ex = "<<L2V_ex<<" H1V_ex = "<<HV_ex<<endl;
     cout<<"norme Volumique "<<endl;

     double L2P_err_rela = L2P_err / (double) L2P_ex;
     double L2V_err_rela = L2V_err / (double) L2V_ex;
     double HV_err_rela = HV_err / (double) HV_ex;
     double DG_err_rela = DG_err / (double) DG_ex;
     
     cout<<" ||P_ex - P_h||_{L2} / ||P_ex[[_{L^2} = "<<L2P_err_rela<<" ||V_ex - V_h||_{L2} / ||V_ex||_{L^2}= "<<L2V_err_rela<<endl;
     
     cout<<" norme H1 volumique ||U_ex - U_h||_{H^1}/ ||U_ex||_{H^1} = "<<HV_err_rela<<endl;
     
     cout<<" ||U_ex - U_h||_{DG}/ ||U_ex||_{DG} = "<<DG_err_rela<<endl;
     
     if (strcmp(method, "cess-des") == 0){
    
     fprintf(fic_absolue,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err, L2V_err, HV_err, DG_err);
    
     fprintf(fic_relative,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, L2P_err_rela, L2V_err_rela, HV_err_rela, DG_err_rela);
     }
     else{
       
       FILE* fic_Cond;
       char *file_Cond = NULL;
       file_Cond = (char *) calloc(strlen(path_to_repertory) + 50, sizeof(char));
       strcpy(file_Cond, path_to_repertory);
       strcat(file_Cond,"_Cond_Vol.txt");

       if( !(fic_Cond = fopen(file_Cond,"a")) ) {
         fprintf(stderr,"  ** UNABLE TO OPEN OUTPUT MESH FILE.\n");
         exit(EXIT_FAILURE);
        }

       fprintf(fic_absolue,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err, L2V_err, HV_err, DG_err);
       
       fprintf(fic_relative,"%d %d %g %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, L2P_err_rela, L2V_err_rela, HV_err_rela, DG_err_rela);
       
       fprintf(fic_Cond,"%d %d %g %g %g %g\n",MSph.get_N_elements(), MSph.ndof, MSph.Nred_mean, MSph.lambda_min, MSph.lambda_max, MSph.lambda_max_normalisee / (double) MSph.lambda_min_normalisee);
       
       
       fclose(fic_Cond); free(file_Cond); file_Cond = NULL;
       
     }
     
     fclose(fic_absolue); fclose(fic_relative);
     free(file_relative); file_relative = NULL;
     free(file_absolue); file_absolue = NULL;

     //delete [] Uex; Uex = nullptr;
     //delete [] A_full; A_full = nullptr;
     //delete [] array_A_loc; array_A_loc = nullptr;
  
}
/*##################################################################################################################*/
// void TEST_threaded_Sparse_COO_product(char *argv[], const char precision[]){

//   double divide = 1.0 / 1000.0;
//   MKL_INT np, N_elements, ndof;
//   get_mesh_size_and_Trefftz_basis_dimension(argv[1], argv[2], argv[5], argv[6], &np, &N_elements, &ndof);
//   auto t1 = std::chrono::high_resolution_clock::now();
  
//   double epsilon = strtod(argv[8], nullptr);
//   double epsilon_reg_tikhonov = strtod(argv[9], nullptr);
//   MKL_INT filtre = strtod(argv[10], nullptr);
  
//   cout<<" valeur du filtre constant epsilon = "<<epsilon<<" type de filtre = "<<filtre<<endl;
  
//   double *points = nullptr;
//   MKL_INT *tetra = nullptr, *voisin = nullptr;
  
//   points = new double[3 * np];
  
//   tetra = new MKL_INT[4 * N_elements];
  
//   voisin = new MKL_INT[4 * N_elements];
  
//   build_mesh(argv[1], argv[2], argv[3], points, tetra, voisin);
  
//   double *Dir = nullptr;
//   Dir = new double[3 * ndof];
  
//   build_direction_of_space_phases(argv[5], argv[6], ndof, Dir);
  
//   MKL_INT n_gL = 12;
//   double *X1d = nullptr, *W1d = nullptr;
//   X1d = new double[n_gL];
//   W1d = new double[n_gL];
//   quad_Gauss_1D(n_gL, X1d, W1d);
  
//   double *X2d = nullptr, *W2d = nullptr;
  
//   MKL_INT n_quad2D = n_gL * n_gL;
//   X2d = new double[2 * n_quad2D];
//   W2d = new double[n_quad2D];
  
//   double *X3d = nullptr, *W3d = nullptr;
  
//   MKL_INT n_quad3D = n_gL * n_gL * n_gL;
//   X3d = new double[3 * n_quad3D];
//   W3d = new double[n_quad3D];
  
//   double *third_coord = nullptr;
//   third_coord = new double[N_elements];
  
//   MKL_INT *new2old = nullptr, *old2new = nullptr;
//   new2old = new MKL_INT[N_elements];
//   old2new = new MKL_INT[N_elements];
  
//   MKL_INT *NewListeVoisin = nullptr, *newloc2glob = nullptr;
  
//   NewListeVoisin = new MKL_INT[N_elements * 4];
  
//   newloc2glob = new MKL_INT[4 * N_elements];
  
//   Sommet *sommets = nullptr;
  
//   sommets = new Sommet[np];
  
//   Element *elements = nullptr;
//   elements = new Element[N_elements];
  
//   MKL_INT N_Face_double = N_elements * 4;
//   Face *face_double = nullptr;
//   face_double = new Face[N_Face_double];
  
//   Face *face = nullptr;
//   face = new Face[N_Face_double];
  
//   MKL_INT *Tri = nullptr;
//   Tri = new MKL_INT[N_Face_double];
  
//   for(MKL_INT e = 0; e < N_elements; e++) {
//     elements[e].OP = new Onde_plane[ndof];
//   }
  
//   MKL_INT *cumul_NRED = nullptr;
//   cumul_NRED = new MKL_INT[N_elements];
  
//   double *virtual_points = nullptr;
//   virtual_points = new double[12];
  
//   Sommet *virtual_sommets = nullptr;
//   virtual_sommets = new Sommet[4];
  
//   Onde_plane *virtual_plane_wave = nullptr;
//   virtual_plane_wave = new Onde_plane[ndof];
  
//   MailleSphere MSph(epsilon, epsilon_reg_tikhonov, np, points, N_elements, tetra, voisin, sommets, ndof, Dir, n_gL, X1d, W1d, X2d, W2d, X3d, W3d);
//   MSph.build_elements_and_faces(argv[4], argv[7], filtre, third_coord, new2old, old2new, NewListeVoisin, newloc2glob, elements, face_double, Tri, face, cumul_NRED, virtual_points, virtual_sommets, virtual_plane_wave, argv[15]);
   
//   delete [] old2new; old2new =  nullptr;
//   delete [] new2old; new2old =  nullptr;
//   delete [] NewListeVoisin; NewListeVoisin =  nullptr;
//   delete [] newloc2glob; newloc2glob =  nullptr;
//   delete [] voisin; voisin =  nullptr;
//   delete [] tetra; tetra =  nullptr;
//   delete [] Tri; Tri =  nullptr;
//   delete [] third_coord; third_coord =  nullptr;
//   delete [] Dir; Dir =  nullptr;
//   auto t2 = std::chrono::high_resolution_clock::now();
  
//   std::chrono::duration<double, std::milli> float_t_MSph = t2 - t1;
  
//   cout<<" temps de creation du maillage : "<<float_t_MSph.count() * divide<<" secs"<<endl;

//   cout<<"************************************************ Eps_max_filtre = "<<epsilon<<" Eps_max_Tikhonov = "<<epsilon_reg_tikhonov<<endl;

//   cout<<" Allocation des tableau:\n";
//   cout<<" -- M matrice associee a associe au produit scalaire ";
//   cout<<" -- w les valeurs propres de la diagonalisation de M\n";
//   cout<<" -- AA les vecteurs propres de la diagonalisation de M\n";
//   cout<<" -- lambda les valeurs propres filtrees de la diagonalisation de M\n";
 
 
//   auto t3 = std::chrono::high_resolution_clock::now();

//   MKL_INT n = ndof * N_elements, e;
 
//   cplx *M = new cplx[ndof * ndof];
//   cplx *P = new cplx[ndof * ndof];
//   double *lambda = new double[ndof];
  
//   cplx *AA = new cplx[ndof * ndof];
//   double *w = new double[ndof];
  
//   auto t4 = std::chrono::high_resolution_clock::now();
  
//   std::chrono::duration<double, std::milli> float_MPL = t4 - t3;
  
//   cout<<" temps Allocation des matrices P: "<<float_MPL.count() * divide<<" secs"<<endl;

//   if (filtre == 0){

//     auto t5 = std::chrono::high_resolution_clock::now();
    
//     buildArealMetrique(MSph.ndof, M, MSph.fixe_element, precision);
    
//     auto t6 = std::chrono::high_resolution_clock::now();
    
//     std::chrono::duration<double, std::milli> float_BM = t6 - t5;
    
//     cout<<" temps de construction de la matrice surfacique de l'element virtuel: "<<float_BM.count() * divide<<" secs"<<endl;
//   }
//   else{
//     auto t5 = std::chrono::high_resolution_clock::now();
    
//      buildVolumeMetrique_ponderee(MSph.ndof, M, MSph, precision);
    
//     auto t6 = std::chrono::high_resolution_clock::now();
    
//     std::chrono::duration<double, std::milli> float_BM = t6 - t5;
    
//     cout<<" temps de construction de la matrice M associee au metrique volumique de l'element virtuel: "<<float_BM.count() * divide<<" secs"<<endl;
//   }

 
//   auto t7 = std::chrono::high_resolution_clock::now();
//   Definition_des_nouvelles_dimensions_des_bases_Trefftz_par_element_virtuel(M, MSph);
//   auto t8 = std::chrono::high_resolution_clock::now();
  
//   std::chrono::duration<double, std::milli> float_Dim = t8 - t7;
  
//   cout<<" temps de construction de la dimension de la base fixe: "<<float_Dim.count() * divide<<" secs"<<endl;
  
//   cout<<" Nred = "<<MSph.fixe_element.Nred<<" Nres = "<<MSph.fixe_element.Nres<<endl;
  
//   auto t_diag_metrique_1 = std::chrono::high_resolution_clock::now();
//   DecompositionDeBloc_ET_Normalisation_TSVD_version(MSph.ndof, M, P, lambda, MSph.fixe_element, AA, w) ;
//   auto t_diag_metrique_2 = std::chrono::high_resolution_clock::now();
  
//   std::chrono::duration<double, std::milli> float_diag_metrique = t_diag_metrique_2 - t_diag_metrique_1;
  
//   cout<<" temps de construction des bases reduites : "<<float_diag_metrique.count() * divide<<" secs"<<endl;
  
//   cout<<" max et min des valeurs propres non reduites et non normalisées "<<endl;
//   cout<<" max = "<<w[MSph.ndof-1] <<" min = "<<w[0]<<endl;
  
//   double Cond = lambda[MSph.fixe_element.Nred-1] / (double) lambda[0];
  
//   cout<<" max et min des valeurs propres reduites et normalisées"<<endl;
//   cout<<" max = "<<lambda[MSph.fixe_element.Nred-1] / (double) MSph.fixe_element.facto_volume<<" min = "<<lambda[0] / (double) MSph.fixe_element.facto_volume<<" Cond = "<<Cond<<endl;
  
//   MKL_INT sizeR_red = MSph.get_N_elements() * MSph.fixe_element.Nred * MSph.fixe_element.Nred * 5;
  
//   MKL_INT n_red = N_elements * MSph.fixe_element.Nred;
  
//   auto t7x = std::chrono::high_resolution_clock::now();

//   cplx *tab_R_red = nullptr; tab_R_red = new cplx[sizeR_red];
//   A_Block *array_R_red_loc = nullptr; array_R_red_loc = new A_Block[N_elements * 5];
  
//   A_skylineRed R_red;
//   R_red.set_n_elem(N_elements); // fixe le nombre d'elements 
//   R_red.set_n_inter(5); // fixe le nombre d'interactions 
//   R_red.set_n_A(sizeR_red); // calcule la taille totale de la matrice
//   R_red.build_mat(MSph, tab_R_red, array_R_red_loc); // Construit les blocks et la structure en mémoire continue
//   R_red.print_info();

 
//   cplx *B_red = nullptr; B_red = new cplx[n_red];
//   cplx *U_red = nullptr; U_red = new cplx[n_red];
  
//   for(i = 0; i < n_red; i++) {
//     B_red[i] = cplx(i, i+1);
//     U_red[i] = cplx(0.0 ,0.0);
//   }
  
//   auto t8x = std::chrono::high_resolution_clock::now();
  
//   std::chrono::duration<double, std::milli> float_A_glob = t8x - t7x;
  
//   cout<<" temps d'allocation de R_red, B et B_red: "<<float_A_glob.count() * divide<<" secs"<<endl;
  
//   auto t9 = std::chrono::high_resolution_clock::now();
  
//   ASSEMBLAGE_DE_LA_Reciprocite_parallel_version(R_red, P, MSph, precision);
  
//   auto t10 = std::chrono::high_resolution_clock::now();

//    std::chrono::duration<double, std::milli> float_BA = t10 - t9;
  
//   cout<<" temps d'assemblage de la matrice R_red : "<<float_BA.count() * divide<<" secs"<<endl;

 
//   double t11x = omp_get_wtime();
  
//   MKL_INT nnz = compute_nnz_for_Sparse_COO_format(R_red, MSph);
//   MKL_INT *rows = nullptr, *cols = nullptr;
//   cplx *values = nullptr;
//   rows = new MKL_INT[nnz]; cols = new MKL_INT[nnz]; values = new cplx[nnz];
  
//   cout<<" transformation en sparse COO... "<<endl;
//   transforme_A_skyline_to_Sparse_COO_format(R_red, rows, cols, values, MSph);
//   double t12x = omp_get_wtime();
  
//   cout<<" temps de transformation de la matrice en Sparse : "<<t12x - t11x<<" secs"<<endl;

//   sparse_matrix_t COO;
  
//   struct matrix_descr Descr;
  
//   Descr.type = SPARSE_MATRIX_TYPE_GENERAL;
  
//   double t13 = omp_get_time();
//   //produit général
//   preprocessing_of_Sparse_COO_MV_product(&Descr, &COO, nnz, n_red, n_red, rows, cols, values);
  
//   prodM_local_Vector_Sparse_COO_distribue(nnz, cplx(1.0,0.0), Descr, COO, B_red, cplx(0.0,0.0), U_red);
  
//   double t14 = omp_get_wtime();
//   if (rank == 0){
//     cout<<" temps de calcul du produit global sparse coo"<<t14-t13<<" secs "<<endl;
//   }
 
  
 
//   delete [] rows; rows = nullptr;
//   delete [] cols; cols = nullptr;
//   delete [] values; values = nullptr;
  
//   cout<<" destruction de tous les tableaux ..."<<endl;
//   delete [] array_R_red_loc; array_R_red_loc = nullptr;
//   delete [] tab_R_red; tab_R_red = nullptr;
//   delete [] P; P = nullptr;
//   delete [] w; w = nullptr;
//   delete [] M; M = nullptr;
//   delete [] B; B = nullptr;
//   delete [] B_red; B_red = nullptr;
//   delete [] points; points =  nullptr;
//   delete [] X1d; X1d =  nullptr;
//   delete [] W1d; W1d =  nullptr;
//   delete [] X2d; X2d =  nullptr;
//   delete [] W2d; W2d =  nullptr;
//   delete [] X3d; X3d =  nullptr;
//   delete [] W3d; W3d =  nullptr;
//   delete [] cumul_NRED; cumul_NRED =  nullptr;
//   for(MKL_INT e = 0; e < N_elements; e++) {
//     delete [] elements[e].OP; elements[e].OP =  nullptr;
//     for(MKL_INT j = 0; j < 4; j++){
//       elements[e].noeud[j] = nullptr;
//       for(MKL_INT l = 0; l < 3; l++) {
// 	elements[e].face[j].noeud[l] = nullptr;
//       }
      
//     }
//   }
  
//   for(MKL_INT i = 0; i < np; i++) {
//     sommets[i].set_X(nullptr);
//   }
  
//   for(MKL_INT i = 0; i < N_Face_double; i++) {
//     for(MKL_INT l = 0; l < 3; l++) {
//       face_double[i].noeud[l] = nullptr;
//       face[i].noeud[l] = nullptr; 
//     }
//   }
//   delete [] sommets; sommets = nullptr;
//   delete [] face_double; face_double = nullptr;
//   delete [] face; face = nullptr;
//   delete [] elements; elements =  nullptr;
//   delete [] virtual_points; virtual_points =  nullptr;
//   for(MKL_INT l = 0; l < 4; l++) {
//     virtual_sommets[l].set_X(nullptr);
//      }
//   delete [] virtual_sommets; virtual_sommets =  nullptr;
//   delete [] virtual_plane_wave; virtual_plane_wave =  nullptr;
// }
